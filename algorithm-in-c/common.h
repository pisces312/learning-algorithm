#ifndef COMMON_H_INCLUDED
#define COMMON_H_INCLUDED

#include <stdio.h>
//#include <stdlib.h>
#include <string.h>
#include <limits.h>
//#include <time.h>
//#include <assert.h>
//#include <math.h>
//#include <memory.h>
//
//#include <cstdlib>
//#include <cmath>
//#include <iostream>
//#include <iomanip>
//#include <queue>
//#include <vector>
//#include <list>
//#include <set>
//#include <stack>

//min,max
#include <algorithm>
//#include <numeric>
//#include <bits/stdc++.h>
//#include <math>


#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(x, y) (((x) > (y)) ? (x) : (y))

inline void swapInt(int* a,int* b) {
    int tmp=*a;
    *a=*b;
    *b=tmp;
}
//inline void swap(int* x,int i, int j)
//{ int t = x[i];
//  x[i] = x[j];
//  x[j] = t;
//}
inline void swap(int* x,const int i, const int j) {
    int t = x[i];
    x[i] = x[j];
    x[j] = t;
}

inline int min(int a, int b, int c) {
    return std::min(std::min(a,b),c);

    //way 2
//    if(a <= b && a <= c)
//        return a;
//    if(b <= a && b <= c)
//        return b;
//    return c;
}


inline int max(int a, int b, int c) {
    return std::max(std::max(a,b),c);
//    if(a >= b && a >= c)
//        return a;
//    if(b >= a && b >= c)
//        return b;
////    if(c >= a && c >= b) //can be ignored
//    return c;
}
/* getbits:  get n bits from position p */
inline unsigned getbits(unsigned x, int p, int n) {
    return (x >> (p+1-n)) & ~(~0 << n);
}
inline int isEven(int n) {
    return !(n&0x1);
}
/**
By default, it will set 0
**/
template <typename T>
T** malloc_Array2D(int row, int col, bool initZero=true) {
    size_t size = sizeof(T);
    size_t point_size = sizeof(T*);
    //allocate memory first, point_size * row stores pointers of each row
    T **arr = (T **) malloc(point_size * row + size * row * col);
    if(arr != NULL) {
        if(initZero)
            memset(arr, 0, point_size * row + size * row * col);
        T *head = (T*)((int)arr + point_size * row);
        while(row--)
            arr[row] = (T*)((int)head + row * col * size);
    }
    return (T**)arr;
}

template <typename T>
void free_Array2D(T **arr) {
    if(arr != NULL)
        free(arr);
}
//void free_Array2D(void **arr) {
//    if(arr != NULL)
//        free(arr);
//}

inline int lessInt(const void* x,const void* y) {
    return *(int*)x-*(int*)y;
}
inline int greaterInt(const void* x,const void* y) {
    return *(int*)y-*(int*)x;
}
//template<class T>
//void display(T a[],int n) {
//    for(int i=0; i<n; i++)
//        std::cout<<a[i]<<" ";
//    std::cout<<std::endl;
//}
//template<class T>
//void display(T* a,int n);
//void display(T a[],int n);
#endif // COMMON_H_INCLUDED
