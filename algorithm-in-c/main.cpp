#include "test_utils.h"
#include "bitmasks.h"

//void function
#define TEST_FUNC(F) { printf("\n%s()\n",#F); void F(); F(); }

//T is argument type
//V is argument value
#define TEST_FUNC_1(F,T,V) { printf("\n%s(%s)\n",#F,#V); void F(T); F(V); }


int main() {
    /**Test all tested cases**/
//    TEST_FUNC(testAll);

    /** Single Test   **/


    TEST_FUNC(testSortMain)
//    TEST_FUNC(testKMP);
//    TEST_FUNC(testHashmap2);

//TEST_FUNC(testHashmap);

//    TEST_FUNC(testHashset);
//    TEST_FUNC(testQsortIndex);
//TEST_FUNC(testTwoSumCPP);

//    TEST_FUNC_1(testTwoSum,int,10);


//void testTwoSum();
//void testTwoSum(int n);
//testTwoSum(2);
//    TEST_FUNC(testTwoSum);
//    TEST_FUNC(testBitMasks)
//    printf("%d\n",isPowOf2(2));

//    TEST_FUNC(testLIS);
//    TEST_FUNC(testShortestPathAlgos);

//    TEST_FUNC(testDFSGraph);
//
//    TEST_FUNC(testTopologySort);
//
//    TEST_FUNC(testEdgeListGraph);
//    TEST_FUNC(testKruskal);
//    TEST_FUNC(testAdjListGraph);

//    TEST_FUNC(testCircleInGraph);
//    TEST_FUNC(testPrim);


//Tree
//TEST_FUNC(testBiTreeTraverseStack);

//Cutedge::testCutedge();

//    testSortMain();
    return 0;
}
void testBitMasks() {
    printf("%d\n",isPowOf2(2));
    printf("%d\n",isPowOf2(3));
    printf("%d\n",isPowOf2(128));
    printf("%d\n",isPowOf2(127));

    printf("%d\n",getNumOnlyContainRightMost1(8));
    printf("%d\n",getNumOnlyContainRightMost1(24));
}
void testCFeature() {


//
    typedef unsigned int            uintptr_t;
    typedef uintptr_t uintx;
    const uintx max_uintx = (uintx)-1;
    //???
    printf("%u\n",max_uintx);



}
namespace simplegraph {
void testDFSGraph();
}
namespace search_mid_node {
void testSearchMidList();
}
namespace Cutedge {
void testCutedge();
}
void testAll() {
    TEST_FUNC(testPrime);
    TEST_FUNC(testMaze);
    TEST_FUNC(testCombination);
    TEST_FUNC(testPermutation);
    TEST_FUNC(testUglyNum);
    TEST_FUNC(testFibonacci);
    TEST_FUNC(testSubArrayByNum);
    TEST_FUNC(testSortMain);
    TEST_FUNC(testLoop);
    TEST_FUNC(testStringAlg);
    TEST_FUNC(testLCM);
    TEST_FUNC(testGCD);
    TEST_FUNC(testBitCount);

    //DP
    TEST_FUNC(testMaxContinuousSeqSum);
    TEST_FUNC(testLIS);
    TEST_FUNC(testAssemblyLineScheduling);
    TEST_FUNC(testLCS);
    TEST_FUNC(testEditDistance);
    TEST_FUNC(testHanoi);
    TEST_FUNC(testBinoCo);





    /**Tree**/
    TEST_FUNC(testAVLTree);
    TEST_FUNC(testBSTree);

    /**Search**/
    TEST_FUNC(testBinarySearch);
    TEST_FUNC(testSearch);



    search_mid_node::testSearchMidList();



    TEST_FUNC(testUnionFind);
//
    TEST_FUNC(testKMax);
//
    /** Graph **/
    TEST_FUNC(testAdjListGraph);
    TEST_FUNC(testShortestPathAlgos);
    TEST_FUNC(testPrim);



}
