# AlgorithmInC
My C programs for learning algorithm.

#Compile
Use "-msse4.2" for SSE4 instructions

#Link
Must change *.c->*.cpp, otherwise cannot link

#Online IDE
http://ide.geeksforgeeks.org/index.php

#Issues for Codeblocks project configuration
Remove .layout if some files are not updated in it. It will be recreated automatically.
