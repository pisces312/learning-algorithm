#include "search.h"
#include"../test_utils.h"
#include<functional>
/**********************************************************************

Permutation

a rearrangement of the elements of an ordered list S into a one-to-one correspondence with S itself.
A string of length n has n! permutation.

Problem: Print all permutations of a given string

Below are the permutations of string ABC.
ABC ACB BAC BCA CBA CAB

http://www.geeksforgeeks.org/?p=767
http://www.geeksforgeeks.org/?p=134901
http://dongxicheng.org/structure/permutation-combination/

***********************************************************************/

/**

Space: O(n)
a is tmp array which stores each permutation

for n number permutation, total step is n

**/
static int _permuteByDFS(bool* visited, int*a, int step,int n) {
    int c=0;
    if(step==n+1) { //in step n, last digit is set. So n+1 step exits
        for(int i=1; i<=n; ++i)
            printf("%d",a[i]);
        printf("\n");
        return 1;
    }
    for(int i=1; i<=n; ++i) {//max i is n, there are [1,n] candidates
        if(!visited[i]) {//if remove, it will print duplicate numbers
            a[step]=i;//the value is set by i
            visited[i]=true;
            c+=_permuteByDFS(visited,a,step+1,n);
            visited[i]=false;
        }
    }
    return c;
}
/**

Get permutation of 1,...,n

**/
int permuteByDFS(int n) {
    bool* visited=(bool*)calloc(n+1,sizeof(bool));
    int* a=(int*)malloc((n+1)*sizeof(int));
    int c=_permuteByDFS(visited,a,1,n);
    free(visited);
    free(a);
    return c;
}

/**

Dictionary order version 字典序法

!Support duplicate values

对给定的字符集中的字符规定了一个先后关系，在此基础上按照顺序依次产生每个排列。

排列中每个字符只出现一次

!similar to increasing a number
When it has high bit carry, all lower bits construct a smallest value and are increasing order
e.g. 165432, next is 2|13456, 2 is original 1+carry, 13456 is the smallest value without 2

[例]字符集{1,2,3},较小的数字较先,这样按字典序生成的全排列是:123,132,213,231,312,321。

生成给定全排列的下一个排列 所谓一个的下一个就是这一个与下一个之间没有字典顺序中相邻的字符串。这就要求这一个与下一个有尽可能长的共同前缀，也即变化限制在尽可能短的后缀上。

算法思想：
设P是[1,n]的一个全排列。
P=P1P2…Pn=P1P2…Pj-1PjPj+1…Pk-1PkPk+1…Pn , j=max{i|Pi<Pi+1}, k=max{i|Pi>Pj} ,对换Pj，Pk，将Pj+1…Pk-1PjPk+1…Pn翻转，
P’= P1P2…Pj-1PkPn…Pk+1PjPk-1…Pj+1即P的下一个

例子:839647521的下一个排列.
从最右开始,找到第一个比右边小的数字4(因为4<7，而7>5>2>1),再从最右开始,找到4右边比4大的数字5(因为4>2>1而4<5),交换4、5,此时5右边为7421,倒置为1247,即得下一个排列:839651247.用此方法写出全排列的非递归算法如下



**/
int permuteDict(int *a, int n) {
    int c=0;
    int i,j;

    //increasing sort
    qsort(a,n,sizeof(int),lessInt);
//    printArray(a,n);
    while(true) {
        // print this permutation
        for(i=0; i<n; ++i)
            printf("%d",a[i]);
        printf("\n");
        ++c;

        // Find the rightmost character which is smaller than its next
        // character. Let us call it 'first char'
        for(i=n-2; i>=0; --i)
            if(a[i]<a[i+1])
                break;
        j=i;
//        printf("j:%d, ",j);

        // If there is no such chracter, all are sorted in decreasing order,
        // means we just printed the last permutation and we are done.
        if(j>=0) {
            // Find first a[k]>a[j]('first char') from rightmost
            for(i=n-1; i>j; --i)
                if(a[j]<a[i])
                    break;
//            printf("i:%d\n",i);
// Swap first and second characters
            swap(a,j,i);
            // Sort the string from right of 'first char'
            qsort(a+j+1,n-j-1,sizeof(int),lessInt);
        } else
            break;
    }
    return c;
}

/**

Backtracking version

Note : The above solution prints duplicate permutations if there are repeating characters in input string.

Time Complexity: O(n*n!) There are n! permutations and it requires O(n) time to print a a permutation.

Space: No need extra space


设一组数p = {r1, r2, r3, … ,rn}, 全排列为perm(p)，pn = p – {rn} (pn表示不包含rn的一组数)
则perm(p) = r1perm(p1), r2perm(p2), r3perm(p3), … , rnperm(pn)。当n = 1时perm(p} = r1。

如：求{1, 2, 3, 4, 5}的全排列
1、首先看最后两个数4, 5。 它们的全排列为4 5和5 4, 即以4开头的5的全排列和以5开头的4的全排列。
由于一个数的全排列就是其本身，从而得到以上结果。
2、再看后三个数3, 4, 5。它们的全排列为3 4 5、3 5 4、 4 3 5、 4 5 3、 5 3 4、 5 4 3 六组数。
即以3开头的和4,5的全排列的组合、以4开头的和3,5的全排列的组合和以5开头的和3,4的全排列的组合.



b: The first element
e: The last element

**/
int permute(char *s, int b, int e) {
    if(b == e+1) {
        printf("%s\n", s);
        return 1;
    }
    int c=0;
    for(int i = b; i <= e; i++) {
        std::swap(s[b],s[i]);//location b can be s[b],...,s[e]
        c+=permute(s, b + 1, e);//go to next position
        std::swap(s[b],s[i]);
    }
    return c;
}
/**
n is always the total length of overall string
it's only used to print permutation
**/
int permute2(char *s, int n) {
    if(*s == '\0') {
        printf("%s\n", s-n);
        return 1;
    }
    int c=0;
    for(char* p=s,*b=s; *p!='\0'; ++p) {
        std::swap(*b,*p);
        c+=permute2(s + 1, n);
        std::swap(*b,*p);
    }
    return c;
}
//int permute2(char *s) {
//    int n=strlen(s);
//    return permute2(s,n);
//}

void testPermutation() {
    char a[]="abcd";

    int n=strlen(a);
    int c=permute(a, 0, n-1);
    printf("count:%d\n",c);

    c=permute2(a,n);
    printf("count:%d\n",c);

    c=permuteByDFS(4);
    printf("count:%d\n",c);

    int dict[]= {3,2,1,4};
    c=permuteDict(dict,4);
    printf("count:%d\n",c);

    int dict2[]= {3,2,2,4};
    c=permuteDict(dict2,4);
    printf("count:%d\n",c);

}
