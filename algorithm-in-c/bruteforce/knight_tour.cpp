#include"../test_utils.h"
//#include<rand.h>
/*******************************************************

The Knight’s tour problem
马踏棋盘算法(骑士周游问题)

The knight is placed on the first block of an empty board and,
moving according to the rules of chess, must visit each square exactly once.
Path followed by Knight to cover all the cells



Existence:
For any m × n board with m ≤ n, a closed knight's tour is always possible unless one or more of these three conditions are met:
1） m and n are both odd
2） m = 1, 2, or 4
3） m = 3 and n = 4, 6, or 8.
On any rectangular board whose smaller dimension is at least 5, there is a (possibly open) knight's tour.



!Types:
1) Open/Close
If the knight ends on a square that is one knight's move from the beginning square (so that it could tour the board again immediately, following the same path), the tour is closed, otherwise it is open.
2) Directed/Undrected closed tour



!!Number of tours:

On an 8 × 8 board, there are exactly 26,534,728,821,064 directed closed tours (i.e. two tours along the same path that travel in opposite directions are counted separately, as are rotations and reflections).

The number of undirected closed tours is half this number, since every tour can be traced in reverse. There are 9,862 undirected closed tours on a 6 × 6 board.

The number of directed open tours on an n × n board for n = 1, 2, … are:
n= 1, 2, 3, 4, 5,     6,         7,               8
   1; 0; 0; 0; 1,728; 6,637,920; 165,575,218,320; 19,591,828,170,979,904
(sequence A165134 in the OEIS).


http://www.geeksforgeeks.org/backtracking-set-1-the-knights-tour-problem/
http://www.geeksforgeeks.org/warnsdorffs-algorithm-knights-tour-problem/
https://en.wikipedia.org/wiki/Knight%27s_tour


********************************************************/

static const int EMPTY=-1;
static const int DIR_NUM=8;
static const int dirs[DIR_NUM][2]= {
    {1,-2},{1,2},{-1,2},{-1,-2},
    {2,1},{2,-1},{-2,1},{-2,-1}
};


/*********************************************************************************************


Warnsdorff’s heuristic
!!Cannot find all solutions because it's heuristic
!不能保证每次都找到，需要不断尝试
!Extremely fast


Warnsdorff’s Rule:
We can start from any initial position of the knight on the board.
We always move to an adjacent, unvisited square with minimal degree (minimum number of unvisited adjacent).
This algorithm may also more generally be applied to any graph.

Some definitions:
A position Q is accessible from a position P if P can move to Q by a single Knight’s move, and Q has not yet been visited.
The accessibility of a position P is the number of positions accessible from P.

Algorithm:
Set P to be a random initial position on the board
Mark the board at P with the move number “1”
Do following for each move number from 2 to the number of squares on the board:
let S be the set of positions accessible from P.
Set P to be the position in S with minimum accessibility
Mark the board at P with the current move number
Return the marked board — each square will be marked with the move number on which it is visited.



*********************************************************************************************/
static int countDegree(int** board,const int m,const int n,const int x,const int y) {
    int c=0;
    for(int d=0; d<DIR_NUM; ++d) {
        int tx=x+dirs[d][0];
        int ty=y+dirs[d][1];
        if(tx>=0&&tx<m&&ty>=0&&ty<n&&board[tx][ty]==EMPTY) ++c;
    }
    return c;
}

static bool nextMove(int** board,const int m,const int n,int& x, int& y) {
    int minDegree=INT_MAX;
    int minDegreeIdx=-1;
    //! Use random start dir
    int offset=rand()%DIR_NUM;
    for(int i=0; i<DIR_NUM; ++i) {
        int d=(i+offset)%DIR_NUM;
        int tx=x+dirs[d][0];
        int ty=y+dirs[d][1];
        if(tx>=0&&tx<m&&ty>=0&&ty<n&&board[tx][ty]==EMPTY) {
            int degree=countDegree(board,m,n,tx,ty);
            if(degree<minDegree) {//!the last element, degree is 0
                minDegree=degree;
                minDegreeIdx=d;
            }
        }
    }
    if(minDegree<INT_MAX) {
        x=x+dirs[minDegreeIdx][0];
        y=y+dirs[minDegreeIdx][1];
        return true;
    }
    return false;
}
/* checks its neighbouring sqaures */
/* If the knight ends on a square that is one
knight's move from the beginning square,
then tour is closed */
static bool neighbour(int x, int y, int sx, int sy) {
    for(int d=0; d<DIR_NUM; ++d)
        if((x+dirs[d][0])==sx&&(y+dirs[d][1])==sy)
            return true;
    return false;
}

bool knightTourWarnsdorff(int** board, const int m,const int n,bool isClosed=false,int tryTimes=5) {
    int totalSteps=m*n;
    int tx,ty,sx,sy,i;
    bool found=false;
    int c=0;
    while(c<tryTimes) {
        ++c;
        fillMatrix(board,m,n,EMPTY);
        sx=rand()%m;
        sy=rand()%n;
        tx=sx;
        ty=sy;
        board[tx][ty]=0;
        for(i=1; i<totalSteps; ++i) {
            if(!nextMove(board,m,n,tx,ty))
                break;
            board[tx][ty]=i;
        }
        if(i==totalSteps)
            if(isClosed) {
                if(neighbour(tx,ty,sx,sy)) {
                    found=true;
                    break;
                }
            } else {
                found=true;
                break;
            }
    }
    printf("Tried: %d time(s)\n",c);
    return found;
}


/********************************************************

Backtracking

For n<=5, n>5 very slow

board: initial values are all -1.store one final result.
step: next step, start from 1
x,y: start entry, value is 0


********************************************************/

static bool _knightOpenTourBacktracking(int ** board,const int m,const int n,int x,int y,int step) {
    if(step==(m*n))
        return true;

    for(int d=0; d<DIR_NUM; ++d) {
        int tx=x+dirs[d][0];
        int ty=y+dirs[d][1];
        if(tx>=0&&tx<m&&ty>=0&&ty<n&&board[tx][ty]==EMPTY) {
            board[tx][ty]=step;//!not only a flag, but step count

            if(_knightOpenTourBacktracking(board,m,n,tx,ty,step+1))
                return true;

            board[tx][ty]=EMPTY;//!backtracking
        }
    }
    return false;
}

bool knightOpenTourBacktracking(int** board,const int m,const int n,int x,int y) {
    fillMatrix(board,m,n,EMPTY);//!set -1 by default
    board[x][y]=0;//!start entry, step is 0
    return _knightOpenTourBacktracking(board,m,n,x,y,1);
}

/**

All open tours for a certain start entry

board: temp space

No need to specify start entry, because it goes through all cases

!when go through all cases, board become initial values, all -1

**/
unsigned long long _knightOpenTourBacktrackingAll(int ** board,const int m,const int n,int x,int y,int c,bool isShown) {
    if(c==(m*n)) {
        if(isShown) printMatrix<int>(board,m,n,printIntWithTab);
        return 1;
    }
    unsigned long long sum=0;
    for(int d=0; d<DIR_NUM; ++d) {
        int tx=x+dirs[d][0];
        int ty=y+dirs[d][1];
        if(tx>=0&&tx<m&&ty>=0&&ty<n&&board[tx][ty]==EMPTY) {
            board[tx][ty]=c;
            sum+=_knightOpenTourBacktrackingAll(board,m,n,tx,ty,c+1,isShown);
            board[tx][ty]=EMPTY;//!backtracking
        }
    }
    return sum;
}

unsigned long long knightOpenTourBacktrackingAll(int** board,const int m,const int n,int x,int y,bool isShown=false) {
    fillMatrix(board,m,n,EMPTY);//!set -1 by default
    board[x][y]=0;//!start entry, step is 0
    return _knightOpenTourBacktrackingAll(board,m,n,x,y,1,isShown);
}
/**

!Return the cases that from all possible start entries

m=n=5, sum=1728

**/
unsigned long long knightOpenTourBacktrackingAll(int** board,const int m,const int n,bool isShown=false) {
    unsigned long long sum=0;
    for(int i=0; i<m; ++i)
        for(int j=0; j<n; ++j)
            sum+=knightOpenTourBacktrackingAll(board,m,n,i,j,isShown);
    return sum;
}
/*********************************************************************************************

Knight close tour

/*********************************************************************************************/
bool _knightClosedTourBacktracking(int ** board,const int m,const int n,int x,int y,int c) {
    int totalSteps=m*n;
    for(int d=0; d<DIR_NUM; ++d) {
        int tx=x+dirs[d][0];
        int ty=y+dirs[d][1];
        if(tx>=0&&tx<m&&ty>=0&&ty<n)
            if(c==totalSteps) {//last step
                if(board[tx][ty]==0)//connect to start entry
                    return true;
            } else if(board[tx][ty]==EMPTY) {
                board[tx][ty]=c;
                if(_knightClosedTourBacktracking(board,m,n,tx,ty,c+1))
                    return true;
                board[tx][ty]=EMPTY;//!backtracking
            }
    }
    return false;
}
bool knightClosedTourBacktracking(int** board,const int m,const int n,int x,int y) {
    fillMatrix(board,m,n,EMPTY);//!set -1 by default
    board[x][y]=0;//!start entry, step is 0
    return _knightClosedTourBacktracking(board,m,n,x,y,1);
}
unsigned long long _knightClosedTourBacktrackingAll(int ** board,const int m,const int n,int x,int y,int c,bool isShown) {
    int totalSteps=m*n;
    unsigned long long sum=0;
    for(int d=0; d<DIR_NUM; ++d) {
        int tx=x+dirs[d][0];
        int ty=y+dirs[d][1];
        if(tx>=0&&tx<m&&ty>=0&&ty<n) {
            if(c==totalSteps) {//last step
                if(board[tx][ty]==0) {//connect to start entry
                    if(isShown) {
                        printMatrix<int>(board,m,n,printIntWithTab);
                    }
                    return 1;
                }
            } else {
                if(board[tx][ty]==EMPTY) {
                    board[tx][ty]=c;
                    sum+=_knightClosedTourBacktrackingAll(board,m,n,tx,ty,c+1,isShown);
                    board[tx][ty]=EMPTY;//!backtracking
                }
            }
        }
    }
    return sum;
}
unsigned long long knightClosedTourBacktrackingAll(int** board,const int m,const int n,int x,int y,bool isShown=false) {
    fillMatrix(board,m,n,EMPTY);//!set -1 by default
    board[x][y]=0;//!start entry, step is 0
    return _knightClosedTourBacktrackingAll(board,m,n,x,y,1,isShown);
}
unsigned long long knightClosedTourBacktrackingAll(int** board,const int m,const int n,bool isShown=false) {
    unsigned long long sum=0;
    for(int i=0; i<m; ++i)
        for(int j=0; j<n; ++j)
            sum+=knightClosedTourBacktrackingAll(board,m,n,i,j,isShown);
    return sum;
}



/*********************************************************************************************
permutation 64! -> very very slow

Naive Algorithm for Knight’s tour
The Naive Algorithm is to generate all tours one by one and check if the generated tour satisfies the constraints.
while there are untried tours
{
   generate the next tour
   if this tour covers all squares
   {
      print this path;
   }
}
*********************************************************************************************/

static void setKnightTour(int** board,int n, int* p) {
    for(int i=0,k=0; i<n; ++i)
        for(int j=0; j<n; ++j,++k)
            board[i][j]=p[k];
}
static bool isFoundNext(int** board,int n,int& x,int& y,const int expectStep) {
    int d=0;
    for(; d<DIR_NUM; ++d) {
        int tx=x+dirs[d][0];
        int ty=y+dirs[d][1];
        if(tx>=0&&tx<n&&ty>=0&&ty<n&&board[tx][ty]==expectStep) {
            x=tx;
            y=ty;
//            printf("(%d): <%d,%d>\n",expectStep,x,y);
            return true;
        }
    }
    return false;
}
static bool isKnightTourValid(int** board,int n) {
    const int step=board[0][0];
    const int lastStep=n*n-1;

    int x=0;
    int y=0;
    int s=step;

    while(s<lastStep)
        if(isFoundNext(board,n,x,y,s+1))
            ++s;
        else
            return false;
    x=0;
    y=0;
    s=step;
    while(s>0)
        if(isFoundNext(board,n,x,y,s-1))
            --s;
        else
            return false;
    return true;
}

static int knightTourGenerator(int** board,int n,int* p,int b,int e) {
    //3 for 8*8
    static const int d=1;

    if(b==e) {
        setKnightTour(board,n,p);
//        printMatrix<int>(board,n,n,printIntWithTab);//debug
        if(isKnightTourValid(board,n)) {
            printMatrix(board,n,n);
            return 1;
        }
        return 0;
    }
    int c=0;
    int t;

    for(int i=b; i<=e; ++i) {
        //Prune
        bool flag=false;
        if(b>0) {
            //compare left
            t=p[i]-p[b-1];
            if(t>=d||t<=-d) {
                flag=true;
                //compare up
                if(b-n>0) {
                    t=p[i]-p[b-n];
                    if(t<d&&t>-d)
                        flag=false;
                }
            }
        } else
            flag=true;

        //Go to next
        if(flag) {
            swap(p,b,i);
            c+=knightTourGenerator(board,n,p,b+1,e);
            swap(p,b,i);
        }
    }
    return c;
}

int knightTourNaive(int n) {
    int pn=n*n;
    int* p=new int[pn];
    for(int i=0; i<pn; ++i)
        p[i]=i;

    int** board=malloc_Array2D<int>(n,n);
    int c=knightTourGenerator(board,n,p,0,pn-1);
    delete[] p;
    return c;
}

/**************************************************************************
For test, only generate tours
**************************************************************************/
static bool isHasValidMove(int* p,int cpn,int n,int i) {
    int x=i/n;
    int y=i%n;
    int maxX=cpn/n;
    int maxY=cpn%n;
    for(int d=0; d<DIR_NUM; ++d) {
        int tx=x+dirs[d][0];
        int ty=y+dirs[d][1];
        if(tx>=0&&tx<maxX&&ty>=0&&ty<maxY) {
            int t=tx*n+ty;
            if((p[t]==p[i]+1)||(p[t]==p[i]-1))
//        if(t>=0&&t<cpn&&((p[t]==p[i]+1)||(p[t]==p[i]-1)))
                return true;
        }
    }
    return false;
}

static bool pruneTour(int* p,int n,int b,int i,int d) {
    bool flag=false;
    if(b>0) {
        //compare left
        int t=p[i]-p[b-1];
        if(t>=d||t<=-d) {
            flag=true;
            //compare up
            if(b-n>0) {
                t=p[i]-p[b-n];
                if(t<d&&t>-d)
                    flag=false;
            }
        }
    } else
        flag=true;


    if(flag) {
        int last=(b/n-3)*n;
        if(last>=0) {
            last+=n;
            flag=false;
            for(int j=0; j<last; ++j) {
                if(isHasValidMove(p,b+1,n,j)) {
                    flag=true;
                } else {
                    flag=false;
                    break;
                }
            }
        }

    }
    return flag;
}
static int knightTourGeneratorOnly(int** board,int n,int* p,int b,int e,int d,bool isShown) {
    if(b==e) {
        if(isShown) {
            setKnightTour(board,n,p);
            printMatrix<int>(board,n,n,printIntWithTab);//debug
        }
        return 1;
    }
    int c=0;
    int t;

    for(int i=b; i<=e; ++i) {
        //Prune
        bool flag=pruneTour(p,n,b,i,d);

        //Go to next
        if(flag) {
            swap(p,b,i);
            c+=knightTourGeneratorOnly(board,n,p,b+1,e,d,isShown);
            swap(p,b,i);
        }
    }
    return c;
}


void testKnightTourNaive() {
    //no solution for 3*3 chessboard
    int n=3;
    int c=knightTourNaive(n);
    printf("knightTourNaive:%d\n",c);

    int result[8][8]= {{ 0,  59,  38,  33,  30,  17,   8,  63},
        {37,  34,  31,  60,   9,  62,  29,  16},
        {58,   1,  36,  39,  32,  27,  18,   7},
        {35,  48,  41,  26,  61,  10,  15,  28},
        {42,  57,   2,  49,  40,  23,   6,  19},
        {47,  50,  45,  54,  25,  20,  11,  14},
        {56,  43,  52,   3,  22,  13,  24,   5},
        {51,  46,  55,  44,  53,   4,  21,  12}
    };

    n=8;
    int** board=createMatrix(n,n,result);
    printMatrix<int>(board,n,n,printIntWithTab);
    bool valid=isKnightTourValid(board,n);
    printf("isKnightTourValid:%d\n",valid);


//very very slow
//    n=4;
//    int pn=n*n;
//    int* p=new int[pn];
//    for(int i=0; i<pn; ++i)
//        p[i]=i;
//    int** board=malloc_Array2D<int>(n,n);
//    int d=3;//for prune
//    c=knightTourGeneratorOnly(board,n,p,0,pn-1,d,true);
//    printf("%d\n",c);


}

static void testOneKnightOpenTour(int m,int n,int x=0,int y=0) {

    if(m<1||n<1) return;

    printf("Chessboard <%d,%d>\n",m,n);

    int c=0;
    bool found=false;
    int** board=malloc_Array2D<int>(m,n);

    BEGIN_TIMING();
    found=knightOpenTourBacktracking(board,m,n,x,y);
    STOP_TIMING();
    printf("knightOpenTourBacktracking: %s\n", found ? "true" : "false");
    if(found)
        printMatrix<int>(board,m,n,printIntWithTab);

    free(board);

    printf("-------------------------------------------\n");
}

static void testOneKnightTour(int m,int n,int x=0,int y=0) {

    if(m<1||n<1) return;

    printf("Chessboard <%d,%d>\n",m,n);

    int c=0;
    bool found=false;
    int** board=malloc_Array2D<int>(m,n);

    BEGIN_TIMING();
    found=knightOpenTourBacktracking(board,m,n,x,y);
    STOP_TIMING();
    printf("knightOpenTourBacktracking: %s\n", found ? "true" : "false");
    if(found)
        printMatrix<int>(board,m,n,printIntWithTab);

    BEGIN_TIMING();
    found=knightClosedTourBacktracking(board,m,n,x,y);
    STOP_TIMING();
    printf("knightCloseTourBacktracking: %s\n", found ? "true" : "false");
    if(found)
        printMatrix<int>(board,m,n,printIntWithTab);

    free(board);

    printf("-------------------------------------------\n");
}

//May very slow when n>5
static void assertAllKnightOpenTour(int m,int n,unsigned long expect=-1,bool isShownAll=false) {
    if(m<1||n<1) return;

    unsigned long c=0;
    bool found=false;
    int** board=malloc_Array2D<int>(m,n);

    BEGIN_TIMING();
    c=knightOpenTourBacktrackingAll(board,m,n,isShownAll);
    STOP_TIMING();
    printf("knightOpenTourBacktrackingAll:%lu\n",c);
    if(expect>=0) assert(c==expect);

    free(board);
}
//May very slow when n>5
static void testAllKnightCloseTour(int m,int n,bool isShownAll=false) {
    if(m<1||n<1) return;

    unsigned long c=0;
    bool found=false;
    int** board=malloc_Array2D<int>(m,n);

    BEGIN_TIMING();
    c=knightClosedTourBacktrackingAll(board,m,n,isShownAll);
    STOP_TIMING();
    printf("knightOpenTourBacktrackingAll:%lu\n",c);

    free(board);
}


void testKnightTourWarnsdorff(int m=8,int n=8,bool isClosed=false,int tries=5) {
    printf("Find Knight %s Tour for Chessboard <%d,%d>\n",(isClosed?"Closed":"Open"),m,n);

    unsigned long c=0;
    bool found=false;
    int** board=malloc_Array2D<int>(m,n);

    BEGIN_TIMING();
    found=knightTourWarnsdorff(board,m,n,isClosed,tries);
    STOP_TIMING();

    if(found)
        printMatrix<int>(board,m,n,printIntWithTab);
    printf("knightTourWarnsdorff: %s\n", found ? "true" : "false");


//Test countDegree
//    for(int i=0; i<m; ++i)
//        for(int j=0; j<n; ++j) {
//            c=countDegree(board,m,n,i,j);
//            board[i][j]=c;
//        }
//
//    printMatrix(board,m,n);


    free(board);

}
void testKnightTour() {
//    int n=8;
    int times=100;
    for(int n=8; n<=10; ++n) {
        testKnightTourWarnsdorff(n,n,false,times);
        testKnightTourWarnsdorff(n,n,true,times);
    }


    //n=7, 2.5s
    //n=6, 1.6s
//    for(int i=1; i<=8; ++i) {
//        testOneKnightOpenTour(i,i);
//    }


//n=5, 6s
//    assertAllKnightOpenTour(5,5,1728);

//n<=6
//    for(int i=1; i<=6; ++i) {
//        testOneKnightTour(i,i);
//    }

}
