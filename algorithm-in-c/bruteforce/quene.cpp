#include"../test_utils.h"
/**********************************************************************

N Queen Problem

The N Queen is the problem of placing N chess queens on an N×N chessboard so that no two queens attack each other.

The expected output is a binary matrix which has 1s for the blocks where queens are placed. For example following is the output matrix for above 4 queen solution.
              { 0,  1,  0,  0}
              { 0,  0,  0,  1}
              { 1,  0,  0,  0}
              { 0,  0,  1,  0}


在8*8=64个格子中取出8个的组合，C(64,8) = 4426165368


http://www.geeksforgeeks.org/backtracking-set-3-n-queen-problem/
http://www.geeksforgeeks.org/printing-solutions-n-queen-problem/

***********************************************************************/
namespace quene {
/**

The idea is to place queens one by one in different columns, starting from the leftmost column. When we place a queen in a column, we check for clashes with already placed queens. In the current column, if we find a row for which there is no clash, we mark this row and column as part of the solution. If we do not find such a row due to clashes then we backtrack and return false.

1) Start in the leftmost column
2) If all queens are placed
    return true
3) Try all rows in the current column.  Do following for every tried row.
    a) If the queen can be placed safely in this row then mark this [row,
       column] as part of the solution and recursively check if placing
       queen here leads to a solution.
    b) If placing queen in [row, column] leads to a solution then return true.
    c) If placing queen doesn't lead to a solution then umark this [row,column]
       (Backtrack) and go to step (a) to try other rows.
4) If all rows have been tried and nothing worked, return false to trigger backtracking.

**/
//Check if conflict with [0,y-1]
static bool isValidForLeft(int** board,const int n,const int row,const int col) {
//check row
    for(int i=0; i<col; ++i)
        if(board[row][i])
            return false;
//check col
    for(int i=0; i<row; ++i)
        if(board[i][col])
            return false;
//check main diagonal (left top to right bottom)
    for(int i=row,j=col; i>=0&&j>=0; --i,--j)
        if(board[i][j])
            return false;
//check secondary diagonal (right top to left bottom)
    for(int i=row,j=col; i<n&&j>=0; ++i,--j)
        if(board[i][j])
            return false;
    return true;
}
/**
Incease col and check every row

Find one solution and return the solution by input array

**/
bool queneDFSOne(int** board,const int n,int col=0) {
    if(col==n)//! col>n-1
        return true;
    for(int r=0; r<n; ++r)
        if(isValidForLeft(board,n,r,col)) {
            board[r][col]=1;
            if(queneDFSOne(board,n,col+1))//! find one solution, just return
                return true;
            board[r][col]=0;
        }
    return false;
}
int** queneDFSOne(const int q) {
    int** board=malloc_Array2D<int>(q,q);
    queneDFSOne(board,q);
    return board;
}

/**
Incease col and check every row

Find one solution
**/
int queneDFSAll(int** board,const int n,int col=0,bool isShow=false) {
    if(col==n) {
        if(isShow)
            printMatrix(board,n,n);
        return 1;//!find one solution
    }
    int c=0;//!not use static, it will keep the result of last call
    for(int r=0; r<n; ++r)
        if(isValidForLeft(board,n,r,col)) {
            board[r][col]=1;
            c+=queneDFSAll(board,n,col+1,isShow);//!not return, print all solutions
            board[r][col]=0;
        }
    return c;
}

/*******************************************************************

!!Time and space optimized version

No need to iterate to validate
Much faster

Use 1D flag arrays: rows,mds,sds
(md: main diagonal； sd: secondary diagonal)
Use array to store quene's position
No need 2D matrix




*******************************************************************/
static void printQuene(int* q,int n) {
    int** a=malloc_Array2D<int>(n,n);
    for(int i=0; i<n; ++i)
        a[q[i]][i]=1;
    printMatrix(a,n,n);
    free(a);
}
int queneDFSAllOpt(int* quene,int* rows,int* mds,int* sds,const int n,int c,bool isShow) {
    if(c==n) {
        if(isShow)
            printQuene(quene,n);
        return 1;
    }
    int sum=0;
    for(int r=0; r<n; ++r)
        if(!rows[r]&&!mds[n+r-c-1]&&!sds[r+c]) {//!validation
            rows[r]=mds[n+r-c-1]=sds[r+c]=1;
            quene[c]=r;//!place one quene
            sum+=queneDFSAllOpt(quene,rows,mds,sds,n,c+1,isShow);
            rows[r]=mds[n+r-c-1]=sds[r+c]=0;//!backtracking
        }
    return sum;
}

int queneDFSAllOpt(const int n,bool isShow=false) {
    /**
    !Each element of flags array indicates one row, col, md or rd
    rows[0] means the first row
    initialize to 0
    **/
    int* rows=(int*)calloc(n,sizeof(int));
    //!no need col, because we iterate col, there must only have one quene
//    int* cFlags=(int*)calloc(n*sizeof(int));
    int* mds=(int*)calloc(2*n-1,sizeof(int));
    int* sds=(int*)calloc(2*n-1,sizeof(int));
    //!temp store soltion
    int* q=(int*)calloc(n,sizeof(int));
    int c=0;
    BEGIN_TIMING();
    c= queneDFSAllOpt(q,rows,mds,sds,n,0,isShow);
    STOP_TIMING();
    free(rows);
    free(mds);
    free(sds);
    free(q);
    return c;

}


static void assertQuene(int n, int e=-1,bool isShow=false) {
    int c=0;
    int** board=malloc_Array2D<int>(n,n);

    //Find one solution
    int** b=queneDFSOne(n);
    printf("queneDFSOne\n");
    printMatrix(b,n);
    free(b);
    b=NULL;

    fillMatrix<int>(board,n,n,0);
    BEGIN_TIMING();
    c=queneDFSAll(board,n,0,isShow);
    STOP_TIMING();
    printf("queneDFSAll:%d\n",c);
    if(e>=0) assert(c==e);

    c=queneDFSAllOpt(n,isShow);
    printf("queneDFSAllOpt:%d\n",c);
    if(e>=0) assert(c==e);

    free(board);

}

void testQuene() {
    assertQuene(4,2,true);
    assertQuene(8,92);
    assertQuene(12);
}

}
