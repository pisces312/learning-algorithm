#include "search.h"
#include"../test_utils.h"
#include<vector>
#include<queue>
#include<stack>
/****************************************************************

Maze


Backtracking

Need flag matrix to backtracking.
Otherwise it has to reuse original matrix


1. Recursion version of DFS
Path is stored in matrix, only maintain one path

2. Iteration version of DFS -> Use stack
Path is stored in stack, only maintain one path
Note: For #1 and #2, need extra space to record the min path,
when found smaller min, copy it

3. BFS->queue (not recommended)
Use Point struct to save previous point


*****************************************************************/
namespace maze {
/**

Find any feasible way

!Backtrackng Algorithm


If destination is reached
    print the solution matrix
Else
   a) Mark current cell in solution matrix as 1.
   b) Move forward in horizontal direction and recursively check if this
       move leads to a solution.
   c) If the move chosen in the above step doesn't lead to a solution
       then move down and check if  this move leads to a solution.
   d) If none of the above solutions work then unmark this cell as 0
       (BACKTRACK) and return false.


1 is block
0 is way

Start:(x,y)
End:(m-1,n-1)


!if only move down and move right, it may not able to find a path in some cases

Will include start node

 **/

static int BLOCK=1;
static int EMPTY=0;


bool mazeBacktracking(int** maze,int m,int n, int x, int y,int px,int py, int** sol) {
// if (x,y is goal) return true
    if(x == px && y == py) {
        if(sol[x][y]==1)
            return false;
        sol[x][y] = 1;
        return true;
    }

// Check if maze[x][y] is valid
    if(x >= 0 && x < m && y >= 0 && y < n && maze[x][y] == EMPTY && sol[x][y]==0) {
// mark x,y as part of solution path
        sol[x][y] = 1;

        /* Move forward in x direction */
        if(mazeBacktracking(maze,m,n, x+1, y,px,py, sol))
            return true;//!return if find one

        /* If moving in x direction doesn't give solution then
        Move down in y direction */
        if(mazeBacktracking(maze,m,n, x, y+1,px,py, sol))
            return true;

        if(mazeBacktracking(maze,m,n, x-1, y,px,py, sol))
            return true;

        if(mazeBacktracking(maze,m,n, x, y-1,px,py, sol))
            return true;

        if(mazeBacktracking(maze,m,n, x-1, y-1,px,py, sol))
            return true;

        if(mazeBacktracking(maze,m,n, x+1, y+1,px,py, sol))
            return true;

        if(mazeBacktracking(maze,m,n, x+1, y-1,px,py, sol))
            return true;

        if(mazeBacktracking(maze,m,n, x-1, y+1,px,py, sol))
            return true;

        /* If none of the above movements work then BACKTRACK:
        unmark x,y as part of solution path */
        sol[x][y] = 0;
    }
    return false;
}

/**

Iteration version of DFS based backtracking

https://software.intel.com/zh-cn/blogs/2014/03/03

**/
bool mazeDFSIteration(int** maze,const int m,const int n, int x, int y,const int px,const int py,std::vector<int>& path) {
    bool found=false;
    std::stack<int> s;
    maze[x][y] = 2;
    s.push(y);
    s.push(x);
    while(!s.empty()) {
        x=s.top();
        s.pop();//!Pop top to get second top
        y=s.top();
        s.push(x);//!Then have to push back

        if(x == px && y == py) { /*found*/
            found=true;
            break;
        }

        if(y + 1 < n && maze[x][y+1] == 0) {/*right*/
            maze[x][y+1]=2;
            s.push(y+1);
            s.push(x);
        } else if(x + 1 < m && maze[x+1][y] == 0) {/*down*/
            maze[x+1][y]=2;
            s.push(y);
            s.push(x+1);
        } else if(y - 1 >= 0 && maze[x][y-1] == 0) {/*left*/
            maze[x][y-1]=2;
            s.push(y-1);
            s.push(x);
        } else if(x - 1 >= 0 && maze[x-1][y] == 0) {/*top*/
            maze[x-1][y]=2;
            s.push(y);
            s.push(x-1);
        } else {
            s.pop();
            s.pop();
            //!cannot set empty here, otherwise endless loop
        }
//        printMatrix(maze,m,n);
    }
    if(found) {
        while(!s.empty()) {
            x=s.top();
            s.pop();
            y=s.top();
            s.pop();
            path.push_back(x);
            path.push_back(y);
//            printf("<%d,%d>\n",x,y);
        }
    }
    return found;
}

/**

!1. Maze matrix with border (sentinel) to avoid move validation
!No need to know row and column count

{2, 2, 2, 2, 2, 2, 2},
{2, 0, 0, 0, 0, 0, 2},
{2, 0, 2, 0, 2, 0, 2},
{2, 0, 0, 2, 0, 2, 2},
{2, 2, 0, 2, 0, 2, 2},
{2, 0, 0, 0, 0, 0, 2},
{2, 2, 2, 2, 2, 2, 2}

!2. No flag matrix but reuse original matrix, original matrix will be changed
0: Empty
2: Block
1: Visited

3. Set flag at the beginning of function

http://yuncode.net/code/c_5093587f5dbaf9

**/
bool mazeWithBorder (int** maze, int x, int y, int px,int py) {
    maze[x][y] = 1;

    if ( x == px && y == py )
        return true;

    if (maze[x][y+1] == 0 && mazeWithBorder(maze,x,y+1,px,py)) return true;
    if (maze[x+1][y] == 0 && mazeWithBorder(maze,x+1,y,px,py)) return true;
    if (maze[x][y-1] == 0 && mazeWithBorder(maze,x,y-1,px,py)) return true;
    if (maze[x-1][y] == 0 && mazeWithBorder(maze,x-1,y,px,py)) return true;

    maze[x][y] = 0;//!backtracking

    return false;
}

/**

!Find a minimum way to a point in a maze

Return both min step and path

0: empty
1: barrier

Four-direction move

Matrix:
a,m,n

!cannot define minStep as static in the function,
because the value can be changed in first call,
it will affect the further call

**/
static bool mazeBacktracing3(int** a,int m,int n,int x,int y,int px,int py,int step,int* minStep,int** sol,int** minSol) {
    static const int next[4][2]= {{0,1},{1,0},{0,-1},{-1,0}};//fixed values
    bool found=false;
    for(int i=0; i<4; ++i) { //4 way to go
        int tx=x+next[i][0];//Calc for next step point
        int ty=y+next[i][1];
        //valid, empty and not visted
        if(tx>=0&&tx<m&&ty>=0&&ty<n&&a[tx][ty]==EMPTY&&!sol[tx][ty]) {
            sol[tx][ty]=1;
            if(tx==px&&ty==py) {
                if(step+1<*minStep) {
                    found=true;
                    *minStep=step+1;
                    //!copy matrix and then backtracking for next path
                    copyMatrix(minSol,m,n,sol);
                    printf("Found one for <%d,%d> with %d step(s).\n",px,py,*minStep);
                    printMatrix(minSol,m,n);
                }
            } else//go to next step
                mazeBacktracing3(a,m,n,tx,ty,px,py,step+1,minStep,sol,minSol);
            sol[tx][ty]=0;//!backtracking
        }
    }
    return found;
}
int mazeBacktracing3(int** a,int m,int n,int x,int y,int px,int py,int** minSol) {
    int** sol=malloc_Array2D<int>(m,n);//tmp
    fillMatrix(sol,m,n,0);
    int steps=INT_MAX;
    sol[x][y]=1;
    mazeBacktracing3(a,m,n,x,y,px,py,1,&steps,sol,minSol);
    free(sol);
    return steps;
}




/************************************************************

complex, need extra space

rely on prior node info, have to store it

path: store path <x,y>

?only can check if exists a path,cannot find it

*************************************************************/

typedef struct Point {
    int x;
    int y;
    Point* pre;//!to trace back previous node in the path
    int step;//to find the min step
} Point;

static void printPathInMatrix(std::vector<int>& path,int m,int n) {
    int** a=malloc_Array2D<int>(m,n);
    for(std::vector<int>::iterator itr=path.begin(); itr!=path.end();) {
        int x=*itr;
        ++itr;
        int y=*itr;
        ++itr;
        a[x][y]=1;
        printf("<%d,%d>\n",x,y);
    }
    printMatrix(a,m,n);
    free(a);
}

static void buildMazePath(Point* lastP,std::vector<int> & path) {
    if(lastP==NULL) return;
    std::stack<Point*> s;
    Point* p=lastP;
    while(p) {
        s.push(p);
        p=p->pre;
    }
    while(!s.empty()) {
        p=s.top();
        s.pop();
        path.push_back(p->x);
        path.push_back(p->y);
    }
}

static bool mazebyBFS(int** a,int m,int n, int x,int y,int px,int py,bool** visited,std::vector<int> & path) {
    static const int next[4][2]= {
        {0,1},
        {1,0},
        {0,-1},
        {-1,0}
    };

    std::vector<Point*> gc;
    std::queue<Point*> q;
    //Start point
    Point *p=(Point*)malloc(sizeof(Point));
    p->x=x;
    p->y=y;
    p->pre=NULL;//no pre
    q.push(p);
    gc.push_back(p);

    visited[x][y]=true;
    bool found=false;
    while(!q.empty()&&!found) { //queue is not empty
        p=q.front();
        q.pop();

        for(int i=0; i<4; ++i) { //try 4 directions
            int cx=p->x+next[i][0];//!cannot cx+=next[i][0]
            int cy=p->y+next[i][1];
            if(cx<0||cx>=m||cy<0||cy>=n) //check border condition
                continue;
            if(a[cx][cy]==EMPTY && !visited[cx][cy]) { // go next step if not visited
                visited[cx][cy]=true;
                Point* nextP=(Point*)malloc(sizeof(Point));
                nextP->x=cx;
                nextP->y=cy;
                nextP->pre=p;//to backtrack path
                q.push(nextP);
                gc.push_back(nextP);
                if(cx==px&&cy==py) {
                    found=true;
                    break;
                }
            }
        }
    }
    //Construct path
    if(found)
        buildMazePath(q.back(),path);

    //free
    for(std::vector<Point*>::iterator itr=gc.begin(); itr!=gc.end(); ++itr)
        free(*itr);
    return found;
}

bool mazebyBFS(int** a,int m,int n,int x,int y,int px,int py,std::vector<int> & path) {
    bool** visited=malloc_Array2D<bool>(m,n);
    fillMatrix<bool>(visited,m,n,false);
    bool found=mazebyBFS(a,m,n,x,y,px,py,visited,path);
    free(visited);
    return found;
}

/**
By array queue
**/
static bool mazebyBFS2(int** a,int m,int n, int x,int y,int px,int py,bool** visited,std::vector<int> & path) {
    static const int next[4][2]= {
        {0,1},
        {1,0},
        {0,-1},
        {-1,0}
    };

    int head=0;
    int tail=0;
    //max size is m*n, the min size is the empty blocks
    const int maxQueueSize=m*n;
    Point* q[maxQueueSize];
    Point *p=(Point*)malloc(sizeof(Point));
    p->x=x;
    p->y=y;
    p->pre=NULL;//no pre
    q[tail++]=p;

    visited[x][y]=true;
    bool found=false;
    while(head<tail&&!found) { //queue is not empty
        p=q[head++];//pop
        for(int i=0; i<4; ++i) { //try 4 directions
            int cx=p->x+next[i][0];//!cannot cx+=next[i][0]
            int cy=p->y+next[i][1];
            if(cx<0||cx>=m||cy<0||cy>=n) //check border condition
                continue;
            if(a[cx][cy]==EMPTY && !visited[cx][cy]) { // go next step if not visited
                visited[cx][cy]=true;
                Point* nextP=(Point*)malloc(sizeof(Point));
                nextP->x=cx;
                nextP->y=cy;
                nextP->pre=p;//to backtrack path
                q[tail++]=nextP;//push
                if(cx==px&&cy==py) {
                    found=true;
                    break;
                }
            }
        }
    }
    //Construct path
    if(found)
        buildMazePath(q[tail-1],path);
    //Free
    for(int i=0; i<tail; ++i)
        free(q[i]);
    return found;
}

bool mazebyBFS2(int** a,int m,int n,int x,int y,int px,int py,std::vector<int> & path) {
    bool** visited=malloc_Array2D<bool>(m,n);
    fillMatrix<bool>(visited,m,n,false);
    bool found=mazebyBFS2(a,m,n,x,y,px,py,visited,path);
    free(visited);
    return found;
}

/**

Find min step by array queue

**/
static bool mazebyBFS3(int** a,int m,int n, int x,int y,int px,int py,bool** visited,std::vector<int> & path) {
    static const int next[4][2]= {
        {0,1},
        {1,0},
        {0,-1},
        {-1,0}
    };


    Point *last=NULL;

    int head=0;
    int tail=0;
    //max size is m*n, the min size is the empty blocks
    const int maxQueueSize=m*n;
    Point* q[maxQueueSize];
    Point *p=(Point*)malloc(sizeof(Point));
    p->x=x;
    p->y=y;
    p->pre=NULL;//no pre
    p->step=1;
    q[tail++]=p;

    visited[x][y]=true;
    int minStep=INT_MAX;
    bool found=false;
    //!Continue even if found one, iterate all possibilities
    while(head<tail) { //queue is not empty
        p=q[head++];//pop
        for(int i=0; i<4; ++i) { //try 4 directions
            int cx=p->x+next[i][0];//!cannot cx+=next[i][0]
            int cy=p->y+next[i][1];
            if(cx<0||cx>=m||cy<0||cy>=n) //check border condition
                continue;
            if(a[cx][cy]==EMPTY && !visited[cx][cy]) { // go next step if not visited
                visited[cx][cy]=true;
                Point* nextP=(Point*)malloc(sizeof(Point));
                nextP->x=cx;
                nextP->y=cy;
                nextP->pre=p;//to backtrack path
                nextP->step=p->step+1;
                q[tail++]=nextP;//push
                if(cx==px&&cy==py) { //!Not break, continue searching other path
                    found=true;
                    if(nextP->step<minStep) {
                        last=nextP;//!!Have to store current tail
                        minStep=nextP->step;
                    }
                }
            }
        }
    }
    //Construct path
    if(found)
        buildMazePath(last,path);
    //Free
    for(int i=0; i<tail; ++i)
        free(q[i]);
    return found;
}

bool mazebyBFS3(int** a,int m,int n,int x,int y,int px,int py,std::vector<int> & path) {
    bool** visited=malloc_Array2D<bool>(m,n);
    fillMatrix<bool>(visited,m,n,false);
    bool found=mazebyBFS3(a,m,n,x,y,px,py,visited,path);
    free(visited);
    return found;
}
}



using namespace maze;
static void assertMaze(int**a, int m,int n,int destX,int destY,int srcX=0,int srcY=0) {
    printMatrix(a,m,n);
    printf("<%d,%d> -> <%d,%d>\n",srcX,srcY,destX,destY);

    std::vector<int> path;
    int steps;
    bool found=false;

    int** sol=malloc_Array2D<int>(m,n);

    fillMatrix(sol,m,n,0);
    found=mazeBacktracking(a,m,n,srcX,srcY,destX,destY,sol);
    printf("mazeBacktracking:%d\n",found);
    printMatrix(sol,m,n);

    fillMatrix(sol,m,n,0);
    steps=mazeBacktracing3(a,m,n,srcX,srcY,destX,destY,sol);
    printf("mazeBacktracking3=%d\n",steps);
    printMatrix(sol,m,n);

    found=mazebyBFS(a,m,n,srcX,srcY,destX,destY,path);
    printf("mazebyBFS:%d\n",found);
    printPathInMatrix(path,m,n);


    path.clear();
    found=mazebyBFS2(a,m,n,srcX,srcY,destX,destY,path);
    printf("mazebyBFS2:%d\n",found);
    printPathInMatrix(path,m,n);

    path.clear();
    found=mazebyBFS3(a,m,n,srcX,srcY,destX,destY,path);
    printf("mazebyBFS3:%d\n",found);
    printPathInMatrix(path,m,n);


    //!Dup matrix since this alg will change it
    int** a2=malloc_Array2D<int>(m,n);
    copyMatrix(a2,m,n,a);
    path.clear();
    found=mazeDFSIteration(a2,m,n,srcX,srcY,destX,destY,path);
    printf("mazeDFSIteration=%d\n",found);
    printPathInMatrix(path,m,n);
    free(a2);


    printf("--------------------------\n");
}

static void assertMazeWithBorder(int**a,int m,int n,int destX,int destY,int srcX=0,int srcY=0) {
    printMatrix(a,m,n);

    bool found=false;

    found=mazeWithBorder(a,srcX,srcY,destX,destY);

    printf("mazeWithBorder:%d\n",found);
    printMatrix(a,m,n);
}
void testMaze() {
    int m,n;
    int** sol;
    int x,y;
    int px,py;
    int steps;

    m=5,n=4;
    int** a=malloc_Array2D<int>(m,n);
    /**
    0 0 1 0
    0 0 0 0
    0 0 1 0
    0 1 0 0
    0 0 0 1
    **/
    a[0][2]=1;
    a[2][2]=1;
    a[3][1]=1;
    a[4][3]=1;
    //Common case
    assertMaze(a,m,n,3,2);
    //<0,3>, need move back, need at least 4 directions
    assertMaze(a,m,n,0,3);
    //Have several paths, choose min one
    assertMaze(a,m,n,4,1);



    int matrix[4][4] = {
        {0, 1, 1, 1},
        {0, 0, 1, 0},
        {1, 0, 1, 1},
        {0, 0, 0, 0}
    };
    m=4,n=4;
    int** maze=createMatrix<int>(m,n,matrix);
    assertMaze(a,m,n,3,3);


    /** Test maze with border **/

    int matrix2[7][7] = {
        {2, 2, 2, 2, 2, 2, 2},
        {2, 0, 0, 0, 0, 0, 2},
        {2, 0, 2, 0, 2, 0, 2},
        {2, 0, 0, 2, 0, 2, 2},
        {2, 2, 0, 2, 0, 2, 2},
        {2, 0, 0, 0, 0, 0, 2},
        {2, 2, 2, 2, 2, 2, 2}
    };
    m=7,n=7;
    int** maze2=createMatrix<int>(m,n,matrix2);
    assertMazeWithBorder(maze2,m,n,5,5,1,1);

}
