#include"../test_utils.h"
/****************************************************************

Combination->Set

不考虑次序，只考虑出现的元素集合

全组合算法，m个数选n个组合算法

*****************************************************************/

/**

C(n,0)+C(n,1)+....+C(n,n-1)+C(n,n)

在此介绍二进制转化法，即将每个组合与一个二进制数对应起来，枚举二进制的同时，枚举每个组合。
如字符串：abcde
00000 <– –> null
00001<– –> e
00010 <– –> d
… …
11111 <– –> abcde

C(5,0)+C(5,1)+C(5,2)+C(5,3)+C(5,4)+C(5,5)
=1+5+10+10+5+1
=32

**/

int combinationByBits(const char* arr,int b,int e) {
    int n=1<<(e-b+1);
    printf("null\n");//empty set
    for(int i=1; i<n; ++i) {
        int t=i;
        int j=0;//reverse order
        while(t) {
            if(t&1)
                printf("%c",arr[j]);
            t>>=1;
            ++j;
        }
        printf("\n");
    }
    return n;
}
template<typename T>
int combinationByBits2(T* arr,int b,int e,PrintFunc func) {
    const int n=1<<(e-b+1);
    const int mask=n>>1;//highest bit mask
    printf("null\n");//for empty set
    for(int i=1; i<n; ++i) {
        for(int j=0,m=mask; m; m>>=1,++j)
            if(i&m)
                func((void*)(arr+j));
        printf("\n");
    }
    return n;
}
int combinationByBits2(const char* arr) {
    int n=strlen(arr);
    return combinationByBits2<const char>(arr,0,n-1,printAsChar);
}





/**
!TODO
C(n,m)

从n中选m个数

a. 首先从n个数中选取编号最大的数，然后在剩下的n-1个数里面选取m-1个数，直到从n-(m-1)个数中选取1个数为止。
b. 从n个数中选取编号次小的一个数，继续执行1步，直到当前可选编号最大的数为m。

**/
//int combination()
void testCombination() {
    int c=combinationByBits("abcde",0,4);
    printf("%d\n",c);

    c=combinationByBits2("abcde");
//    c=combinationByBits2<const char>("abcde",0,4,printAsChar);
    printf("%d\n",c);


    int a[]={1,2,3,4,5,6};
    c=combinationByBits2<int>(a,0,5,printAsInt);
    printf("%d\n",c);


}
