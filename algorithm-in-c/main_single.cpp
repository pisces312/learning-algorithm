#include "common.h"
#include "test_utils.h"
#include <stdlib.h>
#define TEST_FUNC(F) { printf("\n%s()\n",#F); void F(); F(); }


#include "quiz/leetcode.h"


//include cpp
//#include "quiz/add_two_nums.cpp"
//#include "quiz/long_substr_no_dup_char.cpp"


/**

26. Remove Duplicates from Sorted Array

Given a sorted array, remove the duplicates in place such that each element appear only once and return the new length.

Do not allocate extra space for another array, you must do this in place with constant memory.

For example,
Given input array nums = [1,1,2],

Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively. It doesn't matter what you leave beyond the new length.


Time: O(n); Space: O(1)


**/

int removeDuplicates(int* nums, int numsSize) {
    if(numsSize==0)
        return 0;
    int j=0;
    for(int i=1; i<numsSize; ++i) {
        if(nums[i]!=nums[j]) {
            nums[++j]=nums[i];
        }
    }
    return j+1;//+1 for len
}

int removeDuplicatesSol2(int* nums, int numsSize) {
    if(numsSize==0)
        return 0;
    int len=1;
    for(int i=1; i<numsSize; ++i) {
        if(nums[i]!=nums[len-1]) {
            nums[len++]=nums[i];
        }
    }
    return len;
}



void testRemoveDup() {
//    int a[]={1,1,2,3,4,4,5,6,7,7,7};
    int a[]= {1,1,2};

    int n=removeDuplicates(a,ARRAY_SIZE(a));

    printArray(a,n);


    printf("%d\n",n);
}


/**

80. Remove Duplicates from Sorted Array II

Follow up for "Remove Duplicates":
What if duplicates are allowed at most twice?

For example,
Given sorted array nums = [1,1,1,2,2,3],

Your function should return length = 5, with the first five elements of nums being 1, 1, 2, 2 and 3. It doesn't matter what you leave beyond the new length.

**/
int removeDuplicatesII(int* nums, int numsSize) {
    const int DUP_NUM=2;
    if(numsSize<=DUP_NUM-1)
        return numsSize;
    int len=DUP_NUM;
    for(int i=DUP_NUM; i<numsSize; ++i) {
        if(nums[i]!=nums[len-DUP_NUM]) {
            nums[len++]=nums[i];
        }
    }
    return len;
}
//! allow any duplicate times
int removeDuplicatesGeneric(int* nums, int numsSize,int dup) {
    if(numsSize<=dup-1)
        return numsSize;
    int j=dup;
    for(int i=dup; i<numsSize; ++i) {
        if(nums[i]!=nums[j-dup]) {
            nums[j++]=nums[i];
        }
    }
    return j;
}



void testRemoveDupII() {
//    int a[]={1,1,2,3,4,4,5,6,7,7,7};
//    int a[]= {1,1,1,2,2,3};
    int a[]= {1,1,1,2,2,3,3,3,3,4,4,4,5};

    int n=removeDuplicatesII(a,ARRAY_SIZE(a));

    printArray(a,n);


    printf("%d\n",n);
}

/**

33. Search in Rotated Sorted Array

Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.
(i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).
You are given a target value to search. If found in the array return its index, otherwise return -1.
You may assume no duplicate exists in the array.

**/
//Time: O(logn); Space: O(1)
int searchInRotatedSortedArray(int* nums, int numsSize, int target) {
    int b=0,e=numsSize-1,m=0;
    while(b<=e) {
        m=b+(e-b)/2;
//        printf("b=%d,m=%d,e=%d\n",b,m,e);
        if(nums[m]==target)
            return m;
        if(nums[b]<=nums[m]) { //normal case of b,m
            if(nums[b]<=target&&target<nums[m]) {
                //target in [b,m)
                e=m-1;
            } else {
                //target<nums[b], nums[m]<target
                //=>target in (m,e]
                b=m+1;
            }
        } else { //nums[b]>nums[m], rotate index is between b and m
            if(nums[m]<target&&target<=nums[e]) {
                //target in (m,e]
                b=m+1;
            } else {
                //target<nums[m], nums[e]<target
                //=>target in [b,m)
                e=m-1;
            }
        }
    }
    return -1;

}



int _searchInRotatedSortedArrayRecursive(int* nums, int b,int e, int target) {
    if(b>e)
        return -1;
    int m=b+(e-b)/2;
//        printf("b=%d,m=%d,e=%d\n",b,m,e);
    if(nums[m]==target)
        return m;
    if(nums[b]<=nums[m]) { //normal case of b,m
        if(nums[b]<=target&&target<nums[m])
            //target in [b,m)
            return _searchInRotatedSortedArrayRecursive(nums,b,m-1,target);
        //target<nums[b], nums[m]<target
        //=>target in (m,e]
        return _searchInRotatedSortedArrayRecursive(nums,m+1,e,target);
    } else { //nums[b]>nums[m], rotate index is between b and m
        if(nums[m]<target&&target<=nums[e])
            //target in (m,e]
            return _searchInRotatedSortedArrayRecursive(nums,m+1,e,target);
        //target<nums[m], nums[e]<target
        //=>target in [b,m)
        return _searchInRotatedSortedArrayRecursive(nums,b,m-1,target);
    }
}

int searchInRotatedSortedArrayRecursive(int* nums, int numsSize, int target) {
    return _searchInRotatedSortedArrayRecursive(nums,0,numsSize-1,target);
}
//Wrong version
//int searchInRotatedSortedArray(int* nums, int numsSize, int target) {
//    int b=0,e=numsSize-1,m=0;
//    while(b<=e) {
//        m=b+(e-b)/2;
//        printf("b=%d,m=%d,e=%d\n",b,m,e);
//        if(nums[m]==target)
//            return m;
//        if(nums[m]<target)
//            if(target<=nums[e]) //nums[m]<target<=nums[e]
//                b=m+1;
//            else // nums[e]<target, nums[m]<target
//                e=m-1;
//        else if(nums[b]<=target) // nums[b]<=target<nums[m]
//            e=m-1;
//        else  //target<nums[b], target<nums[m]
//            b=m+1;
//
//    }
//    return -1;
//
//}

void testSearchInRotatedSortedArray() {
//    int a[]= {4,5,6,7,8,1,2,3};
//
//    int idx=searchInRotatedSortedArray(a,ARRAY_LENGTH(a),8);
//    printf("%d\n",idx);
//
    int a[]= {4, 5, 6, 7, 0, 1, 2};
    for(int i=0; i<ARRAY_SIZE_INT(a); ++i) {
        int idx=searchInRotatedSortedArrayRecursive(a,ARRAY_SIZE_INT(a),a[i]);
//        int idx=searchInRotatedSortedArray(a,ARRAY_LENGTH(a),a[i]);
        printf("%d\n",idx);
    }
}

/**

4. Median of Two Sorted Arrays (diff size)

There are two sorted arrays nums1 and nums2 of size m and n respectively.
Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
Example 1:
nums1 = [1, 3]
nums2 = [2]

The median is 2.0

Example 2:
nums1 = [1, 2]
nums2 = [3, 4]

The median is (2 + 3)/2 = 2.5


http://www.geeksforgeeks.org/median-of-two-sorted-arrays-of-different-sizes/

**/

/**

k starts with 1

k is different during recursing

**/




double _findMedianSortedArrays(int* a, int m, int* b, int n,int k) {
    //make sure a is smaller than b, just reuse some logic
    if(m>n)
        return _findMedianSortedArrays(b,n,a,m,k);
//a will become 0 first (m==0)
//all elements are in b, so just return kth element of b
    if(m==0)
        return b[k-1];
    //get first element
    if(k==1) {
        printf("k=%d, m=%d,n=%d\n",k,m,n);
        return MIN(a[0],b[0]);
    }

    //m is smaller one, k/2 may larger than m
    //! (k/2)th or the last element of a (m-th element)
    //! here is why need m<n
    int pa=MIN(k/2,m);
    int pb=k-pa;
    printf("k=%d; m=%d, a[%d]; n=%d, b[%d]\n",k,m,pa,n,pb);
    // -1 to get array index
    if(a[pa-1]<b[pb-1]) {
        //remove first 'pa' length of a
        //so now we need to find (k-pa)th num
        //
        //when pa=m, it means all elements in a smaller than b,
        //new length of a is m-pa=0
        //
        //!k is changed here
        return _findMedianSortedArrays(a+pa,m-pa,b,n,k-pa);
    } else if(a[pa-1]>b[pb-1]) {
        //remove first 'pb' length of b
        //so now we need to find (k-pb)th num
        return _findMedianSortedArrays(a,m,b+pb,n-pb,k-pb);
    }

    return a[pa-1];// or b[pb-1]
}

/**

!No swap version

**/
int getKth(int* a, int aL, int* b, int bL, int k) {
    if (aL == 0)
        return b[k - 1];
    if (bL == 0)
        return a[k - 1];
    if (k == 1)
        return MIN(a[0], b[0]);

    int i = MIN(aL, k >> 1);
    int j = MIN(bL, k >> 1);
    //if (a[i - 1] == b[j - 1]) return a[i - 1];
    if (a[i - 1] < b[j - 1])
        return getKth(a + i, aL - i, b, bL, k - i);// Check: aRight + bLeft
    else
        return getKth(a, aL, b + j, bL - j, k - j);//otherwise
}

double findMedianSortedArrays2(int* nums1, int nums1Size, int* nums2, int nums2Size) {
    int l = (nums1Size + nums2Size + 1) >> 1;//if length of both is even, l + 1 == r, else l == r;
    int r = (nums1Size + nums2Size + 2) >> 1;

    return (getKth(nums1, nums1Size, nums2, nums2Size, l) + getKth(nums1, nums1Size, nums2, nums2Size, r)) / 2.0;
}


double findMedianSortedArrays(int* nums1, int nums1Size, int* nums2, int nums2Size) {
    int total=nums1Size+nums2Size;
    if(total&0x1) {//odd, k=total/2+1
        return _findMedianSortedArrays(nums1,nums1Size,nums2,nums2Size,total/2+1);
    } else {//even, k=total/2, k+1=total/2+1
        return (_findMedianSortedArrays(nums1,nums1Size,nums2,nums2Size,total/2)+_findMedianSortedArrays(nums1,nums1Size,nums2,nums2Size,total/2+1))/2;
    }
}


void tesetMedianSortedArrays() {
//    int a[]={3};
//    int b[]={1,2};


//    int a[]={1,3};
//    int b[]={2};

//    int a[]={1,2};
//    int b[]={3,4};

//    int a[]= {1,1,3,3};
//    int b[]= {1,1,3,3};
//    int a[]= {2,3,4};
//    int b[]= {1};
//    double m=findMedianSortedArrays2(a,ARRAY_SIZE_INT(a),b,ARRAY_SIZE_INT(b));
//    printf("%f\n",m);



}

/**

28. Implement strStr()

Returns the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.

**/
//Brute force version
int strStr(char* haystack, char* needle) {
    if(haystack==NULL||needle==NULL)
        return -1;
    if(*needle=='\0')
        return 0;
    //handle haystack's empty string here
    for(int k=0; haystack[k]; ++k) {
        int i=0;
        int j=0;
        while(haystack[k+i]&&needle[j]&&(haystack[k+i]==needle[j])) {
            ++i;
            ++j;
        }
        if(needle[j]=='\0') {
            return k;
        }
    }
    return -1;
}


int main() {

    int idx=strStr("abcdef","cde");
    printf("%d\n",idx);

    idx=strStr("","");//0
    printf("%d\n",idx);

//    i=searchInRotatedSortedArray(a,ARRAY_LENGTH(a),4);
//    printf("%d\n",i);

//    testRemoveDupII();
//char* a="abc";
//printf("%d\n",a[3]);

//    printf("%d\n",'A');
//    TEST_FUNC(testLongestSubstr);

//    TEST_FUNC(testAddTwoNums)

    return 0;
}


