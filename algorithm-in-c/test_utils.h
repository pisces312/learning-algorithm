#ifndef TEST_UTILS_H_INCLUDED
#define TEST_UTILS_H_INCLUDED
#include"common.h"
//#include <string.h>
//#include <limits.h>
#include <time.h>
#include <assert.h>
//#include <math.h>
//#include <memory.h>


//Test performance
#define BEGIN_TIMING() {\
    clock_t start = clock();
//ms
#define STOP_TIMING() \
    start = clock() - start; \
    printf("clicks=%lu\ttime=%lums\n", start, \
    1e3*start/((float) CLOCKS_PER_SEC));}
//ns with 1e style
#define STOP_TIMING_NS() \
    start = clock() - start; \
    printf("clicks=%lu\ttime=%gns\n", start, \
    1e9*start/((float) CLOCKS_PER_SEC));}

//ms
#define END_TIMING(S) \
    start = clock() - start;\
    printf("%s\tclicks=%d\ttime=%gns\n",(S),start,1e3*start/((float) CLOCKS_PER_SEC));}




//定义打印宏，并在打印信息前加入文件名、行号、函数名
#define PRINTFUNC() \
    printf("<%s>: ",__FUNCTION__)
//此宏展开后，类似于printf("123"),printf("456");
#define TRACE1 (printf("%s(%d)-<%s>: ",__FILE__, __LINE__, __FUNCTION__), printf)

//此宏展开后，类似于printf("%d""%d", 1, 2);
#define TRACE2(fmt,...) \
    printf("%s(%d)-<%s>: "##fmt, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)

#define PRINT_INT(N) printf("%d\n",(N))
#define PRINT_STRING(s) printf("%s\n",(s))


#define ARRAY_LENGTH(a) (sizeof(a)/sizeof(a[0]))
#define ARRAY_SIZE(a) ARRAY_LENGTH(a)
#define ARRAY_SIZE_INT(a) ((int)ARRAY_LENGTH(a))



typedef int(*PrintFunc)(void*);
int printAsChar(void* data);
int printAsInt(void* data);
int printIntWithTab(void* data);

template <typename T>
void printArray(T*a,int low,int high,PrintFunc func) {
    if(!a) return;
    if(low<0) return;
    for(int i=low; i<=high; ++i)
        func((void*)&a[i]);
    printf("\n");
}

template <typename T>
void printArray(T*a,int n,PrintFunc func) {
    printArray(a,0,n-1,func);
}

void printArray(int*a,int n) ;
void printArray(int*a,int low,int high) ;

#define PRINT_INT_ARRAY(a) printArray(a,ARRAY_SIZE(a))


template <typename T>
void printMatrix(T**a,int m,int n,PrintFunc func) {
    if(!a) return;
    if(m<1||n<1) return;
    for(int i=0; i<m; ++i) {
        for(int j=0; j<n; ++j)
            func((void*)&a[i][j]);
        printf("\n");
    }
    printf("\n");
}
template <typename T>
void printMatrix(T**a,int n,PrintFunc func) {
    printMatrix(a,n,n,func);
}
void printMatrix(int**a,int m,int n);
void printMatrix(int**a,int n);

template <typename T>
void fillMatrix(T**a,int m,int n,T val) {
    if(!a) return;
    if(m<1||n<1) return;
    for(int i=0; i<m; ++i)
        for(int j=0; j<n; ++j)
            a[i][j]=val;
}
/**

e.g.
int matrix[4][4] = {...};
setMatrix<int,4,4>(maze,matrix);

**/
template <typename T,int m,int n>
void setMatrix(T** dest, T (*src)[n]) {
    if(!dest) return;
    if(m<1||n<1) return;
    for(int i=0; i<m; ++i)
        for(int j=0; j<n; ++j)
            dest[i][j]=src[i][j];
}
/**

e.g.
int matrix[4][4] = {...};
setMatrix<int>(maze,m,n,matrix);
Or
setMatrix<int,4,4>(maze,matrix);

**/
template <typename T,int N>
void setMatrix(T** dest,int m,int n, T (*src)[N]) {
    if(!dest) return;
    if(m<1||n<1) return;
    for(int i=0; i<m; ++i)
        for(int j=0; j<n; ++j)
            dest[i][j]=src[i][j];
}

template <typename T,int N>
T** createMatrix(int m,int n, T (*src)[N]) {
    T** matrix=malloc_Array2D<T>(m,n,false);
    setMatrix<T,N>(matrix,m,n,src);
    return matrix;
}

template <typename T>
void copyMatrix(T** dest,int m,int n,T** src) {
    if(!dest) return;
    if(m<1||n<1) return;
    for(int i=0; i<m; ++i)
        for(int j=0; j<n; ++j)
            dest[i][j]=src[i][j];
}


void assertSorted(int *x,int n) ;
void createRandomData(int* keys,int n);
void createRandomUniqPostiveData(int* keys,int n);


#endif // TEST_UTILS_H_INCLUDED
