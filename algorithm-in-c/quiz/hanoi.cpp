#include "../test_utils.h"
#include <stack>
/****************************************************
Hanoi tower


Iteration version summary
http://blog.csdn.net/QiaoRuoZhuo/article/details/41623633?locationNum=1&fps=1

*****************************************************/

//Move A to C by B
void hanoi(int n,char A,char B,char C) {
    if(n==1) {
        printf("Move disk %d from %c to %c\n",n,A,C);
    } else {
        hanoi(n-1,A,C,B);//A->B
        printf("Move disk %d from %c to %c\n",n,A,C);
        hanoi(n-1,B,A,C);//B->C
    }
}

//Calculate total steps
int hanoi2(int n,char A,char B,char C) {
    static int steps=0;
    if(n==1) {
        printf("[%d] Move disk %d from %c to %c\n",++steps,n,A,C);
    } else {
        hanoi2(n-1,A,C,B);//A->B
        printf("[%d] Move disk %d from %c to %c\n",++steps,n,A,C);
        hanoi2(n-1,B,A,C);//B->C
    }
    return steps;
}

/**
Iteration version

Refer to iteration version of tree in order travel
**/
typedef struct HanoiMove {
    int n;
    char x;
    char y;
    char z;
} HanoiMove;

int hanoi3(int n,char a,char b,char c) {
    int steps=0;
    std::stack<HanoiMove*> st;
    char x=a,y=b,z=c;
    while(true) {
        if(n>1) {//hanoi2(n-1,a,c,b);
            HanoiMove* move=(HanoiMove*)malloc(sizeof(HanoiMove));
            move->n=n-1;
            move->x=x;
            move->y=z;
            move->z=y;

            st.push(move);

            printf("---Push %d,%c,%c,%c\n",move->n,move->x,move->y,move->z);

            //Build parameters
            --n;
            //swap y,z
            y=move->y;
            z=move->z;
        } else if(n==1) {
            ++steps;
            printf("[%d] Move disk %d from %c to %c\n",steps,n,x,z);

            if(st.empty())
                break;

            HanoiMove* move=st.top();
            st.pop();
            printf("---Pop %d,%c,%c,%c\n",move->n,move->x,move->y,move->z);
            //hanoi2(n-1,b,a,c);
            ++steps;
            printf("[%d] Move disk %d from %c to %c\n",steps,move->n+1,move->x,move->y);

            //reset parameters from stack top
            n=move->n;
            x=move->z;//b
            y=move->x;//a
            z=move->y;//c
        }
    }

    return steps;
}


int hanoi4(int n,char a,char b,char c) {
    int steps=0;
    std::stack<HanoiMove*> st;
    while(true) {
        if(n>1) {//hanoi2(n-1,a,c,b);
            HanoiMove* move=(HanoiMove*)malloc(sizeof(HanoiMove));
            move->n=n-1;
            move->x=a;
            move->y=c;
            move->z=b;

            st.push(move);

            printf("---Push %d,%c,%c,%c\n",move->n,move->x,move->y,move->z);

            //Build parameters
            --n;
            b=move->y;
            c=move->z;
        } else  if(n==1) {
            ++steps;
            printf("[%d] Move disk %d: %c -> %c\n",steps,n,a,c);

            if(st.empty()) break;

            HanoiMove* move=st.top();
            st.pop();
            printf("---Pop %d,%c,%c,%c\n",move->n,move->x,move->y,move->z);

            ++steps;
            printf("[%d] Move disk %d: %c -> %c\n",steps,move->n+1,move->x,move->y);

            //hanoi2(n-1,b,a,c);
            //reset parameters from stack top
            //hanoi2(n-1,a,c,b)=>hanoi2(n-1,b,a,c)
            //a=>x=>b
            //c=>y=>c
            //b=>z=>a
            n=move->n;
            a=move->z;
            b=move->x;
            c=move->y;
        }
    }

    return steps;
}



int hanoiFullBTree(int n, char a, char b, char c) { //根据满二叉树的规律输出
    int i, level, k, s;
    int m = 1<<n;
    int steps=0;

    for(i=1; i<m; i++) {//2^n-1 steps
        for(s=2,level=n; i%s==0; s=s<<1)  //判断是第几层的结点
            --level;

        k = i / s; //(k+1)th node of level
        printf("%d: %d ", i, n+1-level);
        ++steps;
        if(level & 1) { //odd level
            switch(k % 3) {
            case 0:
                printf("A -> C\n");
                break;
            case 1:
                printf("C -> B\n");
                break;
            case 2:
                printf("B -> A\n");
            }
        } else { //even level
            switch(k % 3) {
            case 0:
                printf("A -> B\n");
                break;
            case 1:
                printf("B -> C\n");
                break;
            case 2:
                printf("C -> A\n");
            }
        }
    }

    return steps;
}


typedef int (*HanoiFunc)(int,char,char,char) ;

static void assertHanoi(int n,HanoiFunc func=hanoi3) {

    for(int i=1,b=2,steps=0; i<=n; ++i,b<<=1) {
        steps=func(i,'a','b','c');
        printf("Total move: %d\n",steps);
        assert((b-1)==steps);
        printf("------------------------------\n");
    }
}

void testHanoi() {


    assertHanoi(4,hanoiFullBTree);

//    assertHanoi(4,hanoi3);

//    assertHanoi(4,hanoi4);

//    assertHanoi(3);

//    for(int i=1; i<5; ++i) {
//        hanoi3(4,'a','b','c');
//    }
//2^n-1
//    hanoi3(3,'a','b','c');

//    hanoi(3,'a','b','c');

}
