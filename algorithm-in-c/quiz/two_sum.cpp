#include"../test_utils.h"
#include"../search/binary_search.h"
//#include"../hash/hashset.h"
#include"../sort/sort.h"
#include <map>
#include <vector>
/**

LeetCode 001: Two Sum


Given an array of integers, return indices of the two numbers such that they add up to a specific target.
You may assume that each input would have exactly one solution, and you may not use the same element twice.

Example:
Given nums = [2, 7, 11, 15], target = 9,
Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].


Tips:

Pay attention to sort. It will break original array.


**/


//O(n^2)
void twoSumBasic(int *x,int n,const int sum,int& i1,int& i2) {
    for(int i=0; i<n; ++i)
        for(int j=i+1; j<n; ++j)
            if(x[i]+x[j]==sum) {
                //Or if(x[j] == sum-x[i])
                i1=i;
                i2=j;
                return;
            }
}


//?Slower than seq
void twoSumBiSearch(int *x,int n,const int sum,int& i1,int& i2) {
    qsort(x,n,sizeof(int),lessInt);//sort first
    for(int i=0; i<n; ++i) {
        int t=sum-x[i];
        int r=binarySearch(x,0,n-1,t);
        if(r>=0&&r!=i) {//not the same indx as x[i]
            i1=i;
            i2=r;
            return;
        }
    }
}


/**
! it will break original data

O(nlogn+n)=O(nlogn)

**/
void twoSumSortFirst(int *x,int n,const int sum,int& i1,int& i2) {
    qsort(x,n,sizeof(int),lessInt);//sort first
    for(int i=0,j=n-1; i<j;) { // iterate from begin and end
        int t=x[i]+x[j];
        if(t<sum) {
            ++i;
        } else if(t>sum) {
            --j;
        } else {
            i1=i;
            i2=j;
            return;
        }
    }
}
int* twoSumSort(int* nums, int numsSize, int target) {
    int* indices=(int*)malloc(2*sizeof(int));
    qsort(nums,numsSize,sizeof(int),lessInt);
    for(int i=0,j=numsSize-1; i<j;) {
        int t=nums[i]+nums[j];
        if(t<target) {
            ++i;
        } else if(t>target) {
            --j;
        } else {
            indices[0]=i;
            indices[1]=j;
            break;
        }
    }
    return indices;
}

/**
!Good sort version
But need to allocate extra memory
Do not break original array

**/
int* twoSumSortEx(int* nums, int numsSize, int target) {
    int* indices=(int*)malloc(2*sizeof(int));
    int* sortedIndices=qsortIndex(nums,numsSize);
    for(int i=0,j=numsSize-1; i<j;) {
        int numIdx1=sortedIndices[i];
        int numIdx2=sortedIndices[j];
        int t=nums[numIdx1]+nums[numIdx2];
        if(t<target) {
            ++i;
        } else if(t>target) {
            --j;
        } else {
            indices[0]=numIdx1;
            indices[1]=numIdx2;
            break;
        }
    }
    free(sortedIndices);
    return indices;
}



/**

!TODO C map

**/
//int* twoSumCMap(int* nums, int numsSize, int target) {
//}


/**
!![recommended] STL map

**/
int* twoSumHash(int* nums, int numsSize, int target) {
    int* indices=(int*)malloc(2*sizeof(int));
    std::map <int, int> m;//<sum,index>
    for(int i=0; i<numsSize; ++i) {
        int remaining=target-nums[i];
        std::map<int,int>::iterator itr=m.find(remaining);
        if(itr==m.end()) {
            m[nums[i]]=i;
        } else {
            indices[0]=itr->second;
            indices[1]=i;
            printf("%d[%d]+%d[%d]=%d\n",
                   nums[indices[0]],indices[0],nums[indices[1]],indices[1],target);
            break;
        }
    }
    return indices;
}

/**
STL map+vector

**/
std::vector<int> twoSum(std::vector<int>& nums, int target) {
    std::vector<int> indices(2);
    std::map <int, int> m;
    for(int i=0; i<nums.size(); ++i) {
        int remaining=target-nums[i];
        std::map<int,int>::iterator itr=m.find(remaining);
        if(itr==m.end()) {
            m[nums[i]]=i;
        } else {
            indices[0]=itr->second;
            indices[1]=i;
            break;
        }
    }
    return indices;
}

#define TEST_FIND_TWO_NUM(F) {\
int i1,i2;\
(F)(keys,n,sum,i1,i2);\
printf("%d+%d=%d\n",keys[i1],keys[i2],sum);\
assert(keys[i1]+keys[i2]==sum);}

#define TEST_FIND_TWO_NUM2(F) {\
int *indices= (F)(keys,n,sum);\
printf("%d[%d]+%d[%d]=%d\n",keys[indices[0]],indices[0], \
keys[indices[1]],indices[1],sum);\
assert(keys[indices[0]]+keys[indices[1]]==sum); \
free(indices); }


void testTwoSumCPP() {
    int target=6;

    std::vector<int> nums(3);
    nums[0]=3;
    nums[1]=2;
    nums[2]=4;


    std::vector<int> result=twoSum(nums, target);

    printf("%d[%d]+%d[%d]=%d\n",
           nums[result[0]],result[0],nums[result[1]],result[1],target);

}

void testTwoSum(int times=1) {


    /**custom data**/
//    const int n=3;
//    int keysOrig[n]= {3,2,4};
//    int sum=6;
    //

    for(int c=0; c<times; ++c) {
        /**random data**/
        const int n=10;
        int keysOrig[n];
        createRandomData(keysOrig,n);
        int sum=0;
        for(int i=0; i<2; ++i)
            sum+=keysOrig[rand()%n];

        //
        if(n<100)
            printArray(keysOrig,n);


        int keys[n];

//    memcpy(keys,keysOrig,n*sizeof(int));
//    TEST_FIND_TWO_NUM(twoSumBasic);
//    memcpy(keys,keysOrig,n*sizeof(int));
//    TEST_FIND_TWO_NUM(twoSumBiSearch);
//    memcpy(keys,keysOrig,n*sizeof(int));
//    TEST_FIND_TWO_NUM(twoSumSortFirst);

//    memcpy(keys,keysOrig,n*sizeof(int));
//    TEST_FIND_TWO_NUM2(twoSumHash);

//    memcpy(keys,keysOrig,n*sizeof(int));
//    TEST_FIND_TWO_NUM2(twoSumSort);

        memcpy(keys,keysOrig,n*sizeof(int));
        TEST_FIND_TWO_NUM2(twoSumSortEx);
    }
}
