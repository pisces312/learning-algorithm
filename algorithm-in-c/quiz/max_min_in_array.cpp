#include"../test_utils.h"
/*******************************************************

TODO

2.10 寻找数组中的最大值和最小值

1，可以将者看成两个独立的问题。
个需要比较N次，需要比较2*N次。

2，我们可以两两分组。
每一对中较小的放左边，较大放右边。 N/2次。
奇数位比较N/2次，找到最小值。
偶数位比较N/2次，找到最大值。
总的比较次数：1.5N。
缺点：破坏了原数组。

3，维持两个变量min和max。
取出两个数，相比较1次。
较小的和min比较，决定是否更新min。
同理，更新max。
可见处理两个数，比较3次。
总的比较次数：1.5N
优点：不会破坏原来的数组。



*******************************************************/


/**
4. Divide and Conquer
Recursive version
**/
void maxMinRec(int a[],int low,int high,int& max,int& min) { //引用作为参数的强大作用
    int k, max1,min1,max2,min2;
    if(high-low==1||high-low==0) //Handle 1 and 2 elements
        a[low]>a[high]? (max = a[low], min = a[high]):(max = a[high], min = a[low]);
    else {
        k=(high+low)/2;
        maxMinRec(a,low,k,max1,min1);
        maxMinRec(a,k+1,high,max2,min2);
        //Compare twice in one time
        max=max1>max2? max1:max2;
        min=min1<min2? min1:min2;
    }
}

void maxMin2(int *a,int n,int& max,int& min) {
    max=min=a[n-1];
    if((n&0x1)!=0)
        --n;

    for(int i=0; i<n-1; i+=2)
        if(a[i]>a[i+1]) {
            max=a[i]>max?a[i]:max;
            min=a[i+1]<min?a[i+1]:min;
        } else {
            max=a[i+1]>max?a[i+1]:max;
            min=a[i]<min?a[i]:min;
        }

}

static void assertMaxMin(int* a,int n,int expMax,int expMin) {
    int maxVal,minVal;
    maxMinRec(a,0,n-1,maxVal,minVal);
    printf("maxMinRec: max(%d), min(%d)\n",maxVal,minVal);
    assert(expMax==maxVal&&expMin==minVal);

    maxMin2(a,n,maxVal,minVal);
    printf("maxMin2: max(%d), min(%d)\n",maxVal,minVal);
    assert(expMax==maxVal&&expMin==minVal);
}
void testMaxMin() {
    int data[]= {8,3,6,2,1,9,4,5,7};
    int num=sizeof(data)/sizeof(data[0]);

    assertMaxMin(data,num,9,1);
}
//
//复杂度分析：
//
//f(2) = 1;
//f(n) = 2*f(n/2) + 2; (注：一层需要比较两次)
//可以推出f(n) = 1.5*n -2;
//可见总的比较次数仍然没有减少。
