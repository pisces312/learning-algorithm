#include"../test_utils.h"
#include <bits/stdc++.h>
/*****************************************************************

Find subarray with given sum


Given an unsorted array of integers, find a continous subarray which adds to a given number.

Examples:
Input: arr[] = {1, 4, 20, 3, 10, 5}, sum = 33
Ouptut: Sum found between indexes 2 and 4

Input: arr[] = {1, 4, 0, 0, 3, 10, 5}, sum = 7
Ouptut: Sum found between indexes 1 and 4

Input: arr[] = {1, 4}, sum = 0
Output: No subarray found


Negative cases:

Input: arr[] = {10, 2, -2, -20, 10}, sum = -10
Ouptut: Sum found between indexes 0 to 3

Input: arr[] = {-10, 0, 2, -2, -20, 10}, sum = 20
Ouptut: No subarray with given sum exists

http://www.geeksforgeeks.org/find-subarray-with-given-sum/
http://www.geeksforgeeks.org/find-subarray-with-given-sum-in-array-of-integers/

******************************************************************/




/**
[Both positive and negative]

Time: O(n^2)
Space: O(n)

Return: found

**/
void subArrayByNum(const int expect,int *x,int n,int *b,int *e) {
    int* cumvec=new int[n+1];
    int* cumarr=cumvec+1;
    cumarr[-1] = 0;
    for (int i = 0; i < n; i++)
        cumarr[i] = cumarr[i-1] + x[i];

    for (int i = 0; i < n; i++)
        for (int j = i; j < n; j++) {
            int sum = cumarr[j] - cumarr[i-1];
            if(sum==expect) {
                *b=i;
                *e=j;
                delete[] cumvec;
                return;
            }
        }

    *b=*e=-1;
    delete[] cumvec;
}


/**
[Positive only]

A simple solution is to consider all subarrays one by one
and check the sum of every subarray.

We run two loops:
the outer loop picks a starting point i
and the inner loop tries all subarrays starting from i.

Time: O(n^2)
Space: O(1)

**/
void subArrayByNumOnlyPos2(const int sum,int *arr,int n,int *b,int *e) {
    int curr_sum, i, j;

// Pick a starting point
    for (i = 0; i < n; i++) {
        curr_sum = arr[i];

// try all subarrays starting with 'i'
        for (j = i+1; j <= n; j++) {
            if (curr_sum == sum) {
                *b=i;
                *e=j-1;
                return;
            }
            //!This is optimization for all positive numbers
            //!Remove then it can support negtive numbers
            if (curr_sum > sum || j == n)
                break;
            curr_sum = curr_sum + arr[j];
        }
    }

    *b=*e=-1;
}

void subArrayByNum2(const int sum,int *arr,int n,int *b,int *e) {
    int curr_sum, i, j;

// Pick a starting point
    for (i = 0; i < n; i++) {
        curr_sum = arr[i];

// try all subarrays starting with 'i'
        for (j = i+1; j <= n; j++) {
            if (curr_sum == sum) {
                *b=i;
                *e=j-1;
                return;
            }
            curr_sum = curr_sum + arr[j];
        }
    }

    *b=*e=-1;
}

/**
Initialize a variable curr_sum as first element.
curr_sum indicates the sum of current subarray.
Start from the second element and add all elements one by one to the curr_sum.
If curr_sum becomes equal to sum, then print the solution.
If curr_sum exceeds the sum, then remove trailing elemnents while curr_sum is greater than sum.

!Time: O(n)
Spacee: O(1)

**/
void subArrayByNumOnlyPos3(const int sum,int *arr,int n,int *b,int *e) {
    /* Initialize curr_sum as value of first element
    and starting point as 0 */
    int curr_sum = arr[0], start = 0, i;

    /* Add elements one by one to curr_sum and if the curr_sum exceeds the
    sum, then remove starting element */
    for (i = 1; i <= n; i++) {
// If curr_sum exceeds the sum, then remove the starting elements
        while (curr_sum > sum && start < i-1) {//!optimization for only positive numbers
            curr_sum = curr_sum - arr[start];
            start++; //!start is increased every outer loop
        }

// If curr_sum becomes equal to sum, then return true
        if (curr_sum == sum) {
            *b=start;
            *e=i-1;
            return;
        }

// Add this element to curr_sum
        if (i < n)
            curr_sum = curr_sum + arr[i];
    }
// If we reach here, then no subarray
    *b=*e=-1;
}


/**

An efficient way is to use a map. The idea is to maintain sum of elements encountered
so far in a variable (say curr_sum). Let the given number is sum. Now for each element,
we check if curr_sum �C sum exists in the map or not. If we found it in the map that means,
we have a subarray present with given sum, else we insert curr_sum into the map and proceed
to next element. If all elements of the array are processed and we didn��t find any subarray
 with given sum, then subarray doesn��t exists.

Time: O(n)
Space: O(1)

**/

void subArrayByNum3(const int sum,int *arr,int n,int *b,int *e) {
// create an empty map
//C++11 standard
    std::unordered_map<int, int> map;

// Maintains sum of elements so far
    int curr_sum = 0;

    for (int i = 0; i < n; i++) {
// add current element to curr_sum
        curr_sum = curr_sum + arr[i];

// if curr_sum is equal to target sum
// we found a subarray starting from index 0
// and ending at index i
        if (curr_sum == sum) {
            *b=0;
            *e=i;
            return;
        }

// If curr_sum - sum already exists in map
// we have found a subarray with target sum
        if (map.find(curr_sum - sum) != map.end()) {
            *b=map[curr_sum - sum] + 1;
            *e=i;
            return;
        }

        map[curr_sum] = i;
    }

// If we reach here, then no subarray exists
    *b=*e=-1;
}



////////////////////////////////////////////////////////////////////////////////

static void assertSubArrayByNum(const int num,int *x,int n,const int expB,const int expE) {
    int b,e;
    subArrayByNum(num,x,n,&b,&e);
    printf("subArrayByNum:%d\n",b!=-1);
    if(b!=-1) printArray(x,b,e);
    assert(expB==b&&expE==e);

    subArrayByNum2(num,x,n,&b,&e);
    printf("subArrayByNum2:%d\n",b!=-1);
    if(b!=-1) printArray(x,b,e);
    assert(expB==b&&expE==e);

    subArrayByNum3(num,x,n,&b,&e);
    printf("subArrayByNum3:%d\n",b!=-1);
    if(b!=-1) printArray(x,b,e);
    assert(expB==b&&expE==e);


}

static void assertSubArrayByNumForPos(const int num,int *x,int n,const int expB,const int expE) {
    int b,e;

    subArrayByNumOnlyPos2(num,x,n,&b,&e);
    printf("subArrayByNumOnlyPos2:%d\n",b!=-1);
    if(b!=-1) printArray(x,b,e);
    assert(expB==b&&expE==e);

    subArrayByNumOnlyPos3(num,x,n,&b,&e);
    printf("subArrayByNumOnlyPos3:%d\n",b!=-1);
    if(b!=-1) printArray(x,b,e);
    assert(expB==b&&expE==e);

    //For both positive and negative
    assertSubArrayByNum(num,x,n,expB,expE);
}

void testSubArrayByNum() {
    int n=6,sum;
//    int a1[]= {1,-2,3,5,-3,2};
    int b,e;
    int arr1[] = {1, 4, 20, 3, 10, 5};
    assertSubArrayByNumForPos(33,arr1,6,2,4);

    int arr2[] = {1, 4, 0, 0, 3, 10, 5};
    assertSubArrayByNumForPos(7,arr2,7,1,4);

    int arr3[] = {1, 4};
    assertSubArrayByNumForPos(0,arr3,2,-1,-1);

    int arr4[] = {15, 2, 4, 8, 9, 5, 10, 23};
    n = sizeof(arr4)/sizeof(arr4[0]);
    sum = 23;
    assertSubArrayByNumForPos(sum,arr4, n, 1,4);

    int arr5[] = {10, 2, -2, -20, 10};
    sum = -10;
//Ouptut: Sum found between indexes 0 to 3
    assertSubArrayByNum(sum,arr5, 5, 0,3);

}
