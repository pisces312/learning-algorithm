#include <stdlib.h>
#include <stdio.h>


namespace add_two_num {

/**

2. Add Two Numbers [Linked list]

You are given two "non-empty" linked lists representing two non-negative integers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8



Tips:

Use dummy header to simplify logic



**/

/**
 * Definition for singly-linked list.
 */
struct ListNode {
    int val;
    struct ListNode *next;
    //ctor is only for C++
    ListNode(int x) : val(x), next(NULL) { }
    ListNode():val(0),next(NULL) {}
};

/**

!Recommended


**/
ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
    ListNode result(0),*p=&result;
    bool carry=false;
    while(l1||l2||carry) { //!cover 3 cases together
        int sum=(l1?l1->val:0)+(l2?l2->val:0)+carry;
        p->next=new ListNode(sum%10);
        carry=(sum>=10);
        p=p->next;
        l1=l1?l1->next:NULL;
        l2=l2?l2->next:NULL;
    }
    return result.next;
}


/**

! C++ + dummy header + less code(more compare)


**/
ListNode* addTwoNumsCPP2(ListNode* l1, ListNode* l2) {
    ListNode dummy(-1);//use dummy.next to store result
    ListNode* p=&dummy;//ptr for result list
    ListNode *p1=l1, *p2=l2;//cursor
    bool carry=false;
    int sum;
    const int dec=10;
    //1. calculate common part
    for(; p1||p2; p=p->next) {
        sum=carry;
        if(p1) {
            sum+=p1->val;
            p1=p1->next;
        }
        if(p2) {
            sum+=p2->val;
            p2=p2->next;
        }
        carry=(sum>=dec);
        p->next=new ListNode(sum%dec);
    }
    //2. Handle the last digit, may need to create new node
    if(carry)
        p->next=new ListNode(1);
    return dummy.next;
}

/**

C++ + dummy header


**/
ListNode* addTwoNumsCPP(ListNode* l1, ListNode* l2) {
    ListNode dummy(-1);//use dummy.next to store result
    ListNode* p=&dummy;//ptr for result list
    ListNode *p1=l1, *p2=l2, *pList;//cursor
    int carry=0;
    int sum;
    const int dec=10;
    //1. calculate common part
    for(; p1&&p2; p1=p1->next,p2=p2->next,p=p->next) {
        sum=p1->val+p2->val+carry;
        carry=(sum>=dec)?1:0;
        p->next=new ListNode(sum%dec);
    }
    //2. remaining part
    for(pList=p1?p1:p2; pList; pList=pList->next,p=p->next) {
        sum=pList->val+carry;
        carry=(sum>=dec)?1:0;
        p->next=new ListNode(sum%dec);
    }
    //3. !Handle the last digit, may need to create new node
    if(carry>0)
        p->next=new ListNode(1);

    return dummy.next;
}


//42ms
ListNode* addTwoNumsCPP3(ListNode* l1, ListNode* l2) {
    ListNode dummy(-1);//use dummy.next to store result
    ListNode* p=&dummy;//ptr for result list
    ListNode *p1=l1, *p2=l2, *pList;//cursor
    int carry=0;
    int sum;
    const int dec=10;
    //1. calculate common part
    for(; p1&&p2; p1=p1->next,p2=p2->next,p=p->next) {
        sum=p1->val+p2->val+carry;
        if(sum>=dec) {
            sum-=dec; //no need %
            carry=1;
        } else
            carry=0;
//or
//        carry=(sum>=dec)?1:0;
//
        p->next=new ListNode(sum);
    }

    //2. remaining part
    for(pList=p1?p1:p2; pList; pList=pList->next,p=p->next) {
        sum=pList->val+carry;
        if(sum>=dec) {
            sum-=dec;
            carry=1;
        } else
            carry=0;
        p->next=new ListNode(sum);
    }

    //3. !Handle the last digit, may need to create new node
    if(carry>0)
        p->next=new ListNode(1);

    return dummy.next;

}


/**
C version

Initialize "next" manually

**/

struct ListNode* addTwoNumbers2(struct ListNode* l1, struct ListNode* l2) {
    struct ListNode *resultList=NULL,*p=NULL,*t=NULL;
    struct ListNode *p1=l1, *p2=l2;
    struct ListNode *pList;
    int carry=0;
    int sum;
    const int dec=10;
    //1. calculate common part
    for(; p1&&p2; p1=p1->next,p2=p2->next) {
        //Calculate
        sum=p1->val+p2->val+carry;
        carry=(sum>=dec)?1:0;
        //Create
        t= (struct ListNode*)malloc(sizeof(struct ListNode));
        t->val=sum%dec;
        t->next=NULL; //!must initialize here
        if(p==NULL) {//first time
            p=t;
            resultList=t;
        } else {
            p->next=t;
            p=p->next;
        }
    }

    //2. remaining part
    for(pList=p1?p1:p2; pList; pList=pList->next,p->next=t,p=p->next) {
        //Calculate
        sum=pList->val+carry;
        carry=(sum>=dec)?1:0;
        //Create
        t= (struct ListNode*)malloc(sizeof(struct ListNode));
        t->val=sum%dec;
        t->next=NULL;
    }

    //3. !Handle the last digit, may need to create new node
    if(carry>0) {
        t= (struct ListNode*)malloc(sizeof(struct ListNode));
        t->val=carry;
        t->next=NULL;
        p->next=t;
    }

    return resultList;

}


ListNode* addTwoNumsCheat(ListNode* l1, ListNode* l2) {
    ListNode dummy(-1);//use dummy.next to store result
    ListNode* p=&dummy;//ptr for result list
    ListNode *p1=l1, *p2=l2, *pList,*t=NULL;//cursor


    const int cacheCount=16;
    ListNode* cache=new ListNode[cacheCount];
    int cacheIdx=0;

    int carry=0;
    int sum;
    const int dec=10;
    //1. calculate common part
    for(; p1&&p2; p1=p1->next,p2=p2->next,p=p->next) {
        sum=p1->val+p2->val+carry;
        carry=(sum>=dec)?1:0;
        if(cacheIdx<cacheCount) {
            t=&cache[cacheIdx];
            t->val=sum%dec;
            p->next= t;
            ++cacheIdx;
        } else {
            p->next= new ListNode(sum%dec);
        }
    }

    //2. remaining part
    for(pList=p1?p1:p2; pList; pList=pList->next,p=p->next) {
        sum=pList->val+carry;
        carry=(sum>=dec)?1:0;
        if(cacheIdx<cacheCount) {
            t=&cache[cacheIdx];
            t->val=sum%dec;
            p->next= t;
            ++cacheIdx;
        } else {
            p->next= new ListNode(sum%dec);
        }
    }

    //3. !Handle the last digit, may need to create new node
    if(carry>0) {
        p->next=new ListNode(1);
    }

    return dummy.next;

}


struct ListNode* addTwoNumsCheatC(struct ListNode* l1, struct ListNode* l2) {
    struct ListNode dummy;//use dummy.next to store result
    dummy.val=-1;
    dummy.next=NULL;
    struct ListNode* p=&dummy;//ptr for result list
    struct ListNode *p1=l1, *p2=l2, *pList,*t=NULL;//cursor


    const int cacheCount=16;
    struct ListNode* cache=(struct ListNode*)malloc(cacheCount*sizeof(struct ListNode));
    int cacheIdx=0;

    int carry=0;
    int sum;
    const int dec=10;
    //1. calculate common part
    for(; p1&&p2; p1=p1->next,p2=p2->next,p=p->next) {
        sum=p1->val+p2->val+carry;
        if(sum>=dec) {
            sum-=dec;
            carry=1;
        } else {
            carry=0;
        }
        if(cacheIdx<cacheCount) {
            t=&cache[cacheIdx];
            ++cacheIdx;
        } else {
            t= (struct ListNode*)malloc(sizeof(struct ListNode));
        }
        t->val=sum;
        t->next=NULL;
        p->next= t;
    }

    //2. remaining part
    for(pList=p1?p1:p2; pList; pList=pList->next,p=p->next) {
        sum=pList->val+carry;
        if(sum>=dec) {
            sum-=dec;
            carry=1;
        } else {
            carry=0;
        }
        if(cacheIdx<cacheCount) {
            t=&cache[cacheIdx];
            ++cacheIdx;
        } else {
            t= (struct ListNode*)malloc(sizeof(struct ListNode));
        }
        t->val=sum;
        t->next=NULL;
        p->next= t;
    }

    //3. !Handle the last digit, may need to create new node
    if(carry>0) {
        if(cacheIdx<cacheCount) {
            t=&cache[cacheIdx];
            ++cacheIdx;
        } else {
            t= (struct ListNode*)malloc(sizeof(struct ListNode));
        }
        t->val=1;
        t->next=NULL;
        p->next= t;
    }

    return dummy.next;

}



}


void testAddTwoNums() {
    using namespace add_two_num;
//[1],[9,9]
    struct ListNode* l1=(struct ListNode*)malloc(sizeof(struct ListNode));
    l1->val=1;
    l1->next=NULL;

    struct ListNode* l2=(struct ListNode*)malloc(sizeof(struct ListNode));
    l2->val=9;
    l2->next=NULL;

    struct ListNode* l22=(struct ListNode*)malloc(sizeof(struct ListNode));
    l22->val=9;
    l22->next=NULL;

    l2->next=l22;


//    struct ListNode* r=addTwoNumsCheatC(l1,l2);
//    struct ListNode* r=addTwoNumsCheat(l1,l2);
    struct ListNode* r=addTwoNumsCPP(l1,l2);
//    struct ListNode* r=addTwoNumbers(l1,l2);
    while(r) {
        printf("%d ",r->val)    ;
        r=r->next;
    }
    printf("\n");

}
