#ifndef LEETCODE_H_INCLUDED
#define LEETCODE_H_INCLUDED
/******************************************************

Tips:

Use STL container

structure's member must also be initialized

Use uthash.h for C, included by default



Submission:

Create two sessions. One for test, and the other for submit
can remove old submissions

Submission cannot be removed or modified

Can visit other's submission by clicking xx ms

Performance is not stable. Same code may run slowly

******************************************************/
#include <vector>



//002
namespace add_two_num {
struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2);
}
//001
std::vector<int> twoSum(std::vector<int>& nums, int target);

#endif // LEETCODE_H_INCLUDED
