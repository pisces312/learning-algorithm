
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/**

3. Longest Substring Without Repeating Characters

Given a string, find the length of the longest substring without repeating characters.
Examples:
Given "abcabcbb", the answer is "abc", which the length is 3.
Given "bbbbb", the answer is "b", with the length of 1.
Given "pwwkew", the answer is "wke", with the length of 3. Note that the answer must be a substring, "pwke" is a subsequence and not a substring.

https://leetcode.com/problems/longest-substring-without-repeating-characters/
http://www.geeksforgeeks.org/must-coding-questions-company-wise/


**/
/**

This solution uses extra space to store the last indexes of already visited characters. The idea is to scan the string from left to right, keep track of the maximum length Non-Repeating Character Substring (NRCS) seen so far. Let the maximum length be max_len. When we traverse the string, we also keep track of length of the current NRCS using cur_len variable. For every new character, we look for it in already processed part of the string (A temp array called visited[] is used for this purpose). If it is not present, then we increase the cur_len by 1. If present, then there are two cases:

a) The previous instance of character is not part of current NRCS (The NRCS which is under process). In this case, we need to simply increase cur_len by 1.
b) If the previous instance is part of the current NRCS, then our current NRCS changes. It becomes the substring staring from the next character of previous instance to currently scanned character. We also need to compare cur_len and max_len, before changing current NRCS (or changing cur_len).

**/
int lengthOfLongestSubstring(char* s) {
    if(s==NULL||*s=='\0') //!empty string should be considered
        return 0;
    int maxLen=0;
    int visited[128]= {0}; //Use array since size is fixed, default is 0, because i starts with 0
    for(int i=0,j=0; ; ++j) { //i and j are left and right of window
        char c=s[j];
        if(c=='\0')
            break;
        //!if previous char is int current window, update left side
        //(window should not contain previous duplicate char)
        //e.g. "abba"
        if(visited[c]>i)
            i=visited[c];
        //calculate window size
        if(j-i+1>maxLen)
            maxLen=j-i+1;
        //point to next index for current char
        visited[c]=j+1;
    }
    return maxLen;
}

int lengthOfLongestSubstring2(char* s) {
    int maxLen=0;
    int* visited=NULL;
    if(s==NULL||*s=='\0') //!empty string should be considered
        return 0;
    visited=(int*)malloc(128*sizeof(int));
    memset(visited,-1,128*sizeof(int));

    for(int i=0,j=0;; ++j) { //i and j are left and right of window
        char c=*(s+j);
        if(c=='\0')
            break;
        if(visited[c]!=-1) {
            //move window to right most
            //e.g. "abba"
            i=(i>visited[c]?i:visited[c]);//previous same char
        }
        if(j-i+1>maxLen)
            maxLen=j-i+1;
        visited[c]=j+1;//update, move to next char
    }

    free(visited);
    return maxLen;
}


void testLongestSubstr() {

    printf("%d\n",lengthOfLongestSubstring("abcabcbb"));//3
    printf("%d\n",lengthOfLongestSubstring(""));//0
    printf("%d\n",lengthOfLongestSubstring("au"));//2
    printf("%d\n",lengthOfLongestSubstring("dvdf"));//3

    printf("%d\n",lengthOfLongestSubstring("abba"));//3

}
