#ifndef BITMASKS_H_INCLUDED
#define BITMASKS_H_INCLUDED

/**
clear the last 1
**/
inline int clearLast1Bit(int n) {
    return n&(n-1);
}

/**
(n&(n-1))==0 make sure there is only one '1'
n>0 make sure it's a positive number
**/
inline bool isPowOf2(int n) {
    return n>0 && ((n&(n-1))==0);
}

/**
e.g.
8->8
24->8
**/
inline int getNumOnlyContainRightMost1(int n) {
    return n&(-n);
}

inline int clearLast1Bit2(int n) {
    return n-n&(-n);
}


#endif // BITMASKS_H_INCLUDED
