#ifndef TEST_UTILS_CPP_H_INCLUDED
#define TEST_UTILS_CPP_H_INCLUDED
#include"test_utils.h"
#include<stack>
#include<vector>
/**
First output element is stack top

**/
template <typename T>
void printStack(std::stack<T> &s,PrintFunc func) {
    if(s.empty()) {
        printf("\n");
        return;
    }
    T x= s.top();
    s.pop();
    func(x);
    printStack(s,func);
    s.push(x);
//    func(x);
}
//void printStackInt(std::stack<int> &s) {
//    printStack<int>(s,printAsInt);
//}

template <typename T>
void printVector(std::vector<T>& v,PrintFunc func) {
    for(int i=0,n=v.size(); i<n; ++i)
        func((void*)v[i]);
    printf("\n");
//    for(std::vector<T>::iterator itr=v.begin(); itr!=v.end(); ++itr)
//        func((void*)*itr);
}

#endif // TEST_UTILS_CPP_H_INCLUDED
