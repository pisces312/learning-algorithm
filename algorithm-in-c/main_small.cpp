#include"test_utils.h"
#include"test_utils.cpp"

// #include"string/kmp.cpp"

//#include "bruteforce/maze.cpp"
//#include"math/factorial.cpp"
//#include"bruteforce/quene.cpp"
//#include"bruteforce/knight_tour.cpp"
//#include"list/max_in_queue.cpp"

//#include"quiz/find_two_nums.cpp"
//#include"search/binary_search.cpp"

//#include"string/del_certain_chars.cpp"

//For sort.cpp
//#include"random/random.h"
//#include"search/binary_search.cpp"
//#include"bitree/heap.cpp"
//#include"sort/sort.cpp"
//#include"sort/sort_test.cpp"
//#include"sort/3_way_qsort.cpp"

//#include "dp/knapsack_01.cpp"


//#include"dp/min_cost_path.cpp"

//Tree
//#include"bitree/bitree.cpp"
//#include"bitree/bitree_test.cpp"
//#include"bitree/bitree_p.cpp"
//#include"bitree/bstree.cpp"
//#include"bitree/bstree_p.cpp"
//#include"bitree/avltree_p.cpp"
//Non-recursive traverse
//#include"bitree/non_recursive_traverse.cpp"
//#include"bitree/level_order_traversal.cpp"
//#include"bitree/lowest_common_ancestor.cpp"


//#include"quiz/max_min_in_array.cpp"

/** Graph **/
#include"graph/graph.cpp"
#include"graph/sample_graphs.cpp"
//#include"graph/graph_test.cpp"
//!DFS & Connectivity
#include"graph/dfs_graph.cpp"
//!BFS
//#include"graph/bfs.cpp"
//!MST
//#include"tree/union_find.cpp"
//#include"graph/mst_prim.cpp"
//#include"graph/mst_kruskal.cpp"
//
//#include"graph/cycle_detection.cpp"

#include"graph/shortestpath.cpp"


/**
- Simple test for "small" build target
- Include cpp files directly to compile
- No need to declare test method first
**/
int main() {

testShortestPathAlgos();
// testKMP();

//Graph algs:
//testAdjListGraph();
//testCircleInGraph();
//    testPrim();
//testDFSGraph();
//testBFSGraph();


//    testMaxMin();

//    testLCA();


//Tree algs:
//    testBSTree();
//    testAVLTree();
//    testBiTree();



//testMinCostPath();
//    testKnapSack01();

//test3WayParition();

//    testSortMain();


//    testDelChars();
//    FindTwoNum::testFindTwoNums();
//    FindMaxInQueue::testFindMaxInQueue();
//    testKnightTourNaive();
//testKnightTour();
//    quene::testQuene();

//    int a[]={1,2,3,4,5};
//    int len=ARRAY_LENGTH(a);
//    printf("%d\n",len);
//    testFactorial();
//    maze::testMaze();
    return 0;
}
