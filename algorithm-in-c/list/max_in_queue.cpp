#include"../test_utils.h"
#include <list>
/**

Maintain the max value of queue

!!Not priority queue, this queue is still FIFO, only maintain a max value

**/
namespace FindMaxInQueue {
/**
使用一个变量缓存最大元素
！！入队和出队时都要更新最大元素
入队只需比较一次，出队要比较多次（可以存储一个排序的序列）
**/
class ListQueue {
private:
    int maxValue;
    std::list<int> intList;
public:
    ListQueue() {
        maxValue=INT_MIN;
    }
    bool push(int value) {
        intList.push_back(value);
        maxValue=std::max(value,maxValue);
        return true;
    }
    int pop() {
        if (intList.size()==0)
            return -1;
        int value=intList.front();
        intList.pop_front();
        //!find max when pop
        if (value!=maxValue)
            maxValue=*max_element(intList.begin(),intList.end());
        return value;
    }
    int getMaxValue() {
        return maxValue;
    }
};



class Stack {
private:
    int size;
    int *array;
    int *maxArray;
    int stackTop;
    int maxStackItemIndex;//store index of max value

public:
    Stack(int s):size(s) {
        stackTop=-1;
        maxStackItemIndex=-1;
        array=new int[size];
        maxArray=new int[size];
    }
    int getMaxValue() {
        if (maxStackItemIndex>=0)
            return array[maxStackItemIndex];
        return INT_MIN;
    }
    bool push(int value) {
        stackTop++;
        if (stackTop>=size)
            return false;
        array[stackTop]=value;
        //Update index of max val
        if (value>getMaxValue()) {
            maxArray[stackTop]=maxStackItemIndex;
            maxStackItemIndex=stackTop;
        } else
            maxArray[stackTop]=-1;
        return true;
    }
    int pop() {
        if (stackTop<0)
            throw stackTop;
        int ret=array[stackTop];
        //!if top is max, need update after pop
        if (stackTop==maxStackItemIndex)
            maxStackItemIndex=maxArray[stackTop];
        stackTop--;
        return ret;
    }
    bool empty() {
        return stackTop<0;
    }


};


/**

Two stacks way

Need double space

**/
class Queue {
private:
    Stack stackA;
    Stack stackB;
public:
    Queue(int size):stackA(size),stackB(size) {}
    bool enQueue(int v) {
        return stackB.push(v);
    }
    int deQueue() {
        if (stackA.empty())
            while (!stackB.empty())
                stackA.push(stackB.pop());
        return stackA.pop();
    }
    bool empty() {
        return stackA.empty()&&stackB.empty();
    }
    int getMaxValue() {
        return std::max(stackA.getMaxValue(),stackB.getMaxValue());
    }
};


void testFindMaxInQueueByListQueue() {
    int val;
    ListQueue queue;
    queue.push(3);
    queue.push(1);
    queue.push(13);
    queue.push(6);
    queue.push(2);

    queue.push(5);
    val=queue.getMaxValue();
    printf("%d\n",val);
    assert(val==13);

    queue.pop();
    queue.pop();
    queue.pop();
    queue.pop();
    queue.push(5);
    val=queue.getMaxValue();
    printf("%d\n",val);
    assert(val==5);

}

void testFindMaxInQueueByTwoStacks() {
    int val;
    int len=10;
    Stack stack(len);
    stack.push(3);
    stack.push(1);
    stack.push(2);
    stack.push(4);
    stack.push(8);
    stack.push(5);
    val=stack.getMaxValue();
    printf("%d\n",val);
    assert(val==8);

    Queue queue(len);
    queue.enQueue(6);
    queue.enQueue(3);
    queue.enQueue(8);
    queue.enQueue(2);
    queue.enQueue(20);
    queue.enQueue(7);
    queue.enQueue(1);

    val=queue.getMaxValue();
    printf("%d\n",val);
    assert(val==20);
    while(!queue.empty())
        printf("%d\n",queue.deQueue());
}
void testFindMaxInQueue() {
    FindMaxInQueue::testFindMaxInQueueByListQueue();
    FindMaxInQueue::testFindMaxInQueueByTwoStacks();
}
};
