#include "../test_utils.h"
#include "list.h"
/*****************************************************
1. The condition of meet
If we move two pointers, one with speed 1 and another with speed 2,
they will end up meeting if the linked list has a loop.

2. Where to meet
Two people racing around a track, one running twice as fast as the other.
If they start off at the same place,
they will next meet at the start of the next lap

3. The case that not start from the same place
Suppose Fast Runner had a head start of k meters on an n step lap
They will meet k meters before the start of the next lap
Why? Fast Runner would have made k + 2(n - k) steps,
including its head start, and Slow Runner would have made n - k steps
Both will be k steps before the start of the loop
So (k+2(n-k))-(n-k)=n
a-b=n
a=2vt+k
b=vt
k+vt=n
vt=n-k=b
So slow runner run n-k meter


*****************************************************/

//where to meet

Node* meetInLoop(Node *node) {
    Node *pFast=node;
    Node *pSlow=node;
    while(pFast&&pFast->next) {
        pFast = pFast->next->next;
        pSlow = pSlow->next;

        if(pFast != NULL)
            printf("fast=%d ",pFast->element);
        if(pSlow != NULL)
            printf("slow=%d",pSlow->element);
        printf("\n");

        if(pFast==pSlow)
            return pSlow;
    }
    return NULL;
}


//whether has loop
bool hasLoop(Node *node) {
    return meetInLoop(node)!=NULL;
}
/**
1. At the beginning, Fast Runner (n2) and Slow Runner (n1) are
at the head of list (not loop entry).
Both are not in the loop!!

2. When n1 reaches LoopStart, suppose it's k distance from list head,
n2 has twice speed as n1, so n2 is 2k distance from list head.

3. Now n1 and n2 are both in the loop.
And n1 is at LoopStart.
n2 is k distance ahead of LoopStart.

4. MeetingPoint for n1 and n2 is k nodes behind LoopStart
Thus, if we move n1 back to Head and keep n2 at MeetingPoint,
and move them both at the same pace, they will meet at LoopStart

**/
Node* findLoopStart(Node *head) {
    if(head==NULL) return NULL;
    Node* meet=meetInLoop(head);
    if(meet==NULL) return NULL;
    //Go k steps together
    //The meet point is the loop entry
    while(head!=meet) {
        head=head->next;
        meet=meet->next;

        if(head != NULL)
            printf("head=%d ",head->element);
        if(meet != NULL)
            printf("meet=%d",meet->element);
        printf("\n");
    }
    return head;
}

//////////////////////////////////////////////////////////

static void assertLoopList(int nodesNum, int loopStartEl) {
    if(nodesNum<1) return;
    if(loopStartEl>nodesNum) return;


    Node *pList=NULL;
    for(int i=1; i<=nodesNum; ++i)
        insertLastList(&pList,i);
    printList(pList);

    //Build loop
    Node* tail= getNode(pList,nodesNum);
    Node* loopStart=getNode(pList,loopStartEl);
    tail->next=loopStart;

    int flag=hasLoop(pList);
    printf("Has loop: %d\n",flag);

    Node* meetNode=meetInLoop(pList);
    printf("Meet at %d\n",meetNode->element);

    Node* loopStart2=findLoopStart(pList);
    printf("Loop start: %d\n",loopStart2->element);
    assert(loopStart2->element==loopStart->element);
}

void testLoop() {

    assertLoopList(10,3);

}
