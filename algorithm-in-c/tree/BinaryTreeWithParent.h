#ifndef BINARYTREEWITHPARENT_H_INCLUDED
#define BINARYTREEWITHPARENT_H_INCLUDED


namespace BinaryTreeWithParent {
void PrintString(const char* str,int len);
struct Node {
    //basic members
    Node* left;
    Node* right;
    Node* parent;
    int data;
    //
    //for post traverse
    bool flag;
    Node(char d):left(NULL),right(NULL),parent(NULL),data(d),flag(false) {
//        Reset();
    }
    Node(int d):left(NULL),right(NULL),parent(NULL),data(d),flag(false) {
//        Reset();
    }
};
class BinaryTree {
    Node *root;
    int m_nodeNum;
public:
    int GetTreeNodeNum(){
        return m_nodeNum;
    }
    //二叉树找任意两个节点的第一个祖先
    Node* GetFirstAncestor(Node* n1, Node*n2);
};

}

#endif // BINARYTREEWITHPARENT_H_INCLUDED
