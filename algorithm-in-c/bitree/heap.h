#ifndef HEAP_H_INCLUDED
#define HEAP_H_INCLUDED

//index from 0
void siftdown(int* x,int i, int n);
void siftdownMin(int* x,int i, int n);

//index from 1
void siftdown2(int a[], int i, int n) ;
#endif // HEAP_H_INCLUDED
