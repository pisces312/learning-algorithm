#include "../common.h"


//Top down
//index starts from 0
//2*i+1, 2*i+2
//n is total nodes of current heap
void siftdown(int* x,int i, int n) {
    int c=2*i+1; //left child
    while(c<n) {
        //Find the larger one of children
        if(c+1 <n && x[c+1] > x[c])
            c++;
        //!
        if(x[i] >= x[c])
            break;
        //
        swap(x,i, c);
        i = c;
        c = 2*i+1;
    }
}
void siftdownMin(int* x,int i, int n) {
    int c=2*i+1; //left child
    while(c<n) {
        if(c+1 <n && x[c+1] < x[c])
            c++;
        if(x[i] < x[c])
            break;
        swap(x,i, c);
        i = c;
        c = 2*i+1;
    }
}
//Place max value as the last element of current heap
void siftdown2(int a[], int i, int n) {
    int temp = a[i];
    int j = 2 * i + 1;
    while(j < n) {
        if(j + 1 < n && a[j + 1] > a[j])
            j++;

        if(a[j] <= temp)
            break;

        a[i] = a[j];     //把较小的子结点往上移动,替换它的父结点
        i = j;
        j = 2 * i + 1;
    }
    a[i] = temp; //move to final place
}
