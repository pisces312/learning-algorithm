#include "../bitree/bitree.h"
#include "../bitree/bstree.h"
#include "../common.h"
/**
Binary search tree with parent version

**/


//Allow duplicate values
BSTNode* insertToBSTree_p(BSTNode* root, BSTNode* node) {
    if(!root) return node;
    if(!node) return root;
    BSTNode* pre=NULL;
    BSTNode* p=root;
    while(p) {
        pre=p;
        if(node->data<p->data)
            p=p->lchild;
        else
            p=p->rchild;
    }
    if(node->data<pre->data)
        pre->lchild=node;
    else
        pre->rchild=node;
    node->parent=pre;
    return root;
}

BSTNode* insertToBSTree_p(BSTNode* root, int data) {
    return insertToBSTree_p(root,createBSTNode(data));
}
