#include "../bitree/bitree.h"
#include "../bitree/bstree.h"
#include "../test_utils.h"
#include <vector>
/**

AVL tree
One kind of self-balanced binary tree

Balance factor based, node with parent implementation

BalanceFactor(N):= -Height(LeftSubtree(N)) + Height(RightSubtree(N))

-1 means left subtree is heavy
+1 means right subtree is heavy


Reference:

Invariant loop version
https://en.wikipedia.org/wiki/AVL_tree

http://interactivepython.org/runestone/static/pythonds/Trees/AVLTreeImplementation.html


**/

static AVLNode* createAVLNode(int data,int bf=0) {
    AVLNode*  p= (AVLNode*)malloc(sizeof(AVLNode));
    p->data=data;
    p->lchild=NULL;
    p->rchild=NULL;
    p->parent=NULL;
    p->bf=bf;
    return p;
}

/**




    B            D
   / \          / \
  A   D   =>   B   E
     / \      / \
    C   E    A   C


Subtree A, C and E's heights are not changed before and after rotate
So use hA,hC and hE to stands for newBal

newBal(B)=-hA+hC
oldBal(B)=-hA+hD=-hA+(1+max(hC,hE))

newBal(B)-oldBal(B)
=(-hA+hC)-(-hA+(1+max(hC,hE)))
=-1-max(hC,hE)+hC

newBal(B)
=oldBal(B)-1-max(hC,hE)+hC
=oldBal(B)-1+min(-hC,-hE)+hC
=oldBal(B)-1+min(-hC+hC,-hE+hC)
=oldBal(B)-1+min(0,-oldBal(D))
=oldBal(B)-1-max(0,oldBal(D))


newBal(D)=-hB+hE=-(1+max(hA,hC))+hE
oldBal(D)=-hC+hE

newBal(D)-oldBal(D)=-1-max(hA,hC)+hC

newBal(D)
=oldBal(D)-1+min(-hA,-hC)+hC
=oldBal(D)-1+min(newBal(B),0)


Parent link is also adjusted

return new root of this subtree

**/
AVLNode* rotateLeft(AVLNode* rotRoot) {
    AVLNode* newRoot=rotRoot->rchild;
    rotRoot->rchild=newRoot->lchild;
    if(newRoot->lchild)
        newRoot->lchild->parent=rotRoot;

    newRoot->parent=rotRoot->parent;
    if(rotRoot->parent)//rotRoot is not root
        if(rotRoot==rotRoot->parent->lchild)
            rotRoot->parent->lchild=newRoot;
        else
            rotRoot->parent->rchild=newRoot;

    newRoot->lchild=rotRoot;
    rotRoot->parent=newRoot;

//Update BF, only affect two nodes, rotRoot and newROot
    //rotRoot->bf: Old B.bf
    //newRoot->bf: Old D.bf
    //newBal(B)=oldBal(B)-1-max(0,oldBal(D))
    rotRoot->bf=rotRoot->bf-1-std::max(newRoot->bf,0);
    //rotRoot->bf: New B.bf
    //newRoot->bf: Old D.bf
    //newBal(D)=oldBal(D)-1+min(newBal(B),0)
    newRoot->bf=newRoot->bf-1+std::min(rotRoot->bf,0);
    return newRoot;
}



/**
LL shape=>right rotate

case 1: no left child's right child

       4(-2)              4(-1)
     /   \               /  \
    3(-2) 5             2(0) 5
   /           =>      /  \
  2(-1)               1    3(0)
 /
1

case 2: have left child's right child

       5(-2)               3(0)
     /   \                /    \
    3(-1) 6              2      5(0)
   /   \        =>      /      /  \
  2     4              1      4    6
 /
1


    D            B
   / \          / \
  B   E    =>  A   D
 / \              / \
A   C            C   E


Subtree A, C and E's heights are not changed before and after rotate
So use hA,hC and hE to stands for newBal



newBal(D)=-hC+hE
oldBal(D)=-hB+hE=-(1+max(hA,hC))+hE

newBal(D)-oldBal(D)=1+max(hA,hC)-hC

newBal(D)
=oldBal(D)+1+max(hA-hC,0)
=oldBal(D)+1-min(oldBal(B),0)


newBal(B)=-hA+hD=-hA+(1+max(hC,hE))
oldBal(B)=-hA+hC

newBal(B)-oldBal(B)
=(1+max(hC,hE)-hC

newBal(B)
=oldBal(B)+1+max(hC,hE)-hC
=oldBal(B)+1+max(0,newBal(D))


**/
AVLNode* rotateRight(AVLNode* rotRoot) {
    AVLNode* newRoot=rotRoot->lchild;
    rotRoot->lchild=newRoot->rchild;
    if(newRoot->rchild)
        newRoot->rchild->parent=rotRoot;

    newRoot->parent=rotRoot->parent;
    if(rotRoot->parent)//rotRoot is not root
        if(rotRoot==rotRoot->parent->rchild)
            rotRoot->parent->rchild=newRoot;
        else
            rotRoot->parent->lchild=newRoot;

    newRoot->rchild=rotRoot;
    rotRoot->parent=newRoot;

    //Update BF, only affect two nodes, rotRoot and newROot
    //rotRoot->bf: Old D.bf
    //newRoot->bf: Old B.bf
//    newBal(D)=oldBal(D)+1-min(oldBal(B),0)
    rotRoot->bf=rotRoot->bf+1-std::min(newRoot->bf,0);
    //rotRoot->bf: New D.bf
    //newRoot->bf: Old B.bf
    //newBal(B)=oldBal(B)+1+max(0,newBal(D))
    newRoot->bf=newRoot->bf+1+std::max(rotRoot->bf,0);
    return newRoot;
}



/**

!1) Left left case: Right rotate left left
Left node is new root




!2) RR case



3) Left right case:
Left rotate right right
Right rotate left left

       5              5           5
     /   \           / \         / \
    4(-2) 6   =>    4   6  =>   3   6
   /               /           /  \
  2(+1)           3           2(0) 4(0)
   \             /
    3           2


4) Right left case:
Right rotate left left
Left rotate right right

      3            3           3
     / \          / \         / \
    2   4    =>  2   4  =>   2   5
         \            \         / \
          6            5       4   6
         /              \
        5                6


Precondition: node->bf>1||node->bf<-1

So if bf<0, means bf=-2

return the new root

**/
AVLNode* rebalance(AVLNode* node) {
    if(node->bf<0) { //left
        if(node->lchild->bf>0)
            rotateLeft(node->lchild);//LR needs two rotates
        return rotateRight(node);
    } else {
        /**node->rchild->bf==0, which means
        node has lchild and rchild, it's LL shape
        So only rotate left**/
        if(node->rchild->bf<0) //only if no rchild of rchild
            rotateRight(node->rchild);//RL needs two rotates
        return rotateLeft(node);
    }
}

/**
Back trace until bf=0
Recursive version

original version

Return: root of adjusted subtree

**/
AVLNode* updateBalance(AVLNode* node) {
    if(node->bf>1||node->bf<-1)
        return rebalance(node);//Top root may be changed
    if(node->parent) { //If parent is null, nothing to do
        if(node==node->parent->lchild)
            --node->parent->bf;
        else
            ++node->parent->bf;
        if(node->parent->bf!=0)
            return updateBalance(node->parent);
    }
    return node;
}
/**
???for deletion, the node itself may need rebalance

Another recursive version, the place of rebalance is changed

Actually it's to update parent's balance
The new inserted node.bf is always 0
**/
//AVLNode* updateBalance2(AVLNode* node) {
//    if(node->parent) { //If parent is null, nothing to do
//        if(node==node->parent->lchild)
//            --node->parent->bf;
//        else
//            ++node->parent->bf;
//        if(node->parent->bf!=0) {
//            if(node->parent->bf>1||node->parent->bf<-1)
//                return rebalance(node->parent);//Top root may be changed
//            return updateBalance2(node->parent);
//        }
//    }
//    return node;
//}

//Recursive version
AVLNode* insert(AVLNode* root, AVLNode* node) {
    if(root==NULL) return node;
    AVLNode* newRoot=NULL;
    if(node->data<root->data) {
        if(root->lchild) {
            newRoot=insert(root->lchild,node);
        } else {
            root->lchild=node;
            node->parent=root;
            newRoot=updateBalance(node);
        }
    } else {
        if(root->rchild) {
            newRoot=insert(root->rchild,node);
        } else {
            root->rchild=node;
            node->parent=root;
            newRoot=updateBalance(node);
        }
    }
    //If overall root changed
    if(newRoot&&newRoot->parent==NULL)
        return newRoot;
    return root;
}


///////////////////////////////////////////////////////
//Iteration version
//Bottom up, from inserted node to the parent which bf is 0
//If bf >1 ||<-1, rebalance
//Return the root of this subtree, not the root of the whole tree
AVLNode* updateBalanceItr(AVLNode* root) {
//!Cannot check p->bf==0 in for loop,
//!It must first calculate, then check
//!Before calculation, p->bf may be 0 and will ignore loop
    for(AVLNode* p=root->parent,*q=root; p; q=p,p=p->parent) {
        if(q==p->lchild)
            --p->bf;
        else
            ++p->bf;
        if(p->bf==0)
            break;
        if(p->bf>1||p->bf<-1) //may return new root of this subtree
            return rebalance(p);
    }
    return root;//keep original root
}

/***********************

Delete

***********************/
//node is the parent of deleted node
//return current tree root
AVLNode* updateBalanceForDeletion(AVLNode* root,AVLNode* node) {
    if(node==NULL) return root;
    AVLNode* newRoot=NULL;
    if(node->bf>1||node->bf<-1) { //may return new root of this subtree
        newRoot= rebalance(node);
        if(newRoot&&newRoot->parent==NULL)
            return newRoot;
        return root;
    }
    for(AVLNode* p=node->parent; p; node=p,p=p->parent) {
        if(node->bf!=0)
            break;
        if(node==p->lchild)
            ++p->bf;
        else
            --p->bf;
        if(p->bf>1||p->bf<-1) { //may return new root of this subtree
            newRoot= rebalance(p);
            break;
        }
    }
    if(newRoot&&newRoot->parent==NULL)
        return newRoot;
    return root;
}
//return parent of deleted node
//only update parent's bf here, not go futher
AVLNode* deleteThisNode(AVLNode** root, AVLNode* node) {
    AVLNode* nodeToBalance=node->parent;
    //1) Have two children
    if(node->lchild&&node->rchild) {
        AVLNode* succ=node->rchild;
        AVLNode* succp=node;
        //!Have to get parent of succ, cannot use findMin function
        while(succ->lchild) {
            succp=succ;//Keep parent
            succ=succ->lchild;
        }



        succ->lchild=node->lchild;//lchild of deleted node
        if(succ->lchild)
            succ->lchild->parent=succ;
        succ->bf=node->bf;

        /**succ can be node->rchild, if node->rchild doesn't
        have any left child, so it has to stop succ->rchild
        point to itself(node->rchild)**/
        if(succ!=node->rchild) {
            succp->lchild=succ->rchild;//lchild of succ'sparent
            succ->rchild=node->rchild;
            if(succ->rchild)
                succ->rchild->parent=succ;
            ++succp->bf;
            nodeToBalance=succp;//start adjust from succp
        } else {
            --succ->bf;
            nodeToBalance=succ;//start adjust from succ
        }
        //Handle succ's parent
        if(node->parent) {
            succ->parent=node->parent;
            if(node==succ->parent->lchild) {
                succ->parent->lchild=succ;
            } else {
                succ->parent->rchild=succ;
            }
        } else {
            *root=succ;//!case 2.1
        }

    } else if(node->lchild) { //Only left child
        if(node->parent) {
            if(node==node->parent->lchild) {
                node->parent->lchild=node->lchild;
                ++node->parent->bf;
            } else {
                node->parent->rchild=node->lchild;
                --node->parent->bf;
            }
        } else
            *root=node->lchild;//!case 2.2
    } else if(node->rchild) { //Only right child
        if(node->parent) {
            if(node==node->parent->lchild) {
                node->parent->lchild=node->rchild;
                ++node->parent->bf;
            } else {
                node->parent->rchild=node->rchild;
                --node->parent->bf;
            }
        } else
            *root=node->rchild;//!case 2.3
    } else {
        if(node->parent) {
            if(node==node->parent->lchild) {
                node->parent->lchild=NULL;
                ++node->parent->bf;
            } else {
                node->parent->rchild=NULL;
                --node->parent->bf;
            }
        } else *root=NULL; //!case 3: all removed
    }
    free(node);
    return nodeToBalance;
}


//return current tree root
AVLNode* deleteNode(AVLNode* root,int data) {
    //Search
    AVLNode* node=(AVLNode*)searchByBSTree((BSTNode*)root,data);
    if(node==NULL) return root;//not found
    //Delete node and only update its parent's bf
    //!Tree root may be changed
    AVLNode* parent=deleteThisNode(&root,node);
    //Bottom up update ancestor's bf
    //!Tree root may also be changed here
    return updateBalanceForDeletion(root, parent);
}


////////////////////////////////////////////////
//    AVL tree Wiki version
///////////////////////////////////////////////
/**

Left rotate for RR case

Insertion case:

   2(+2) [x]            4(0) [z]
  /  \                  /  \
 1    4(+1) [z]    [x] 2(0) 5
[t1] / \         =>   / \    \
    3   5            1   3    6
 [t23]   \
          6 [t4]
  2                  2
 / \                / \
1   3(+2)          1   4(0)
     \      =>        /  \
      4(+1)          3(0) 5
       \
        5


Deletion case: Node is removed from t1

   2(+2) [x]            5(-1) [z]
  /  \                  /   \
 1    5(0) [z]    [x] 2(+1)  5
[t1] / \         =>   / \     \
    4   6            1   4     6
   /     \              /
  3       7            3
 [t23]   [t4]


Input:
X = root of subtree to be rotated left

Z = its right child, not left-heavy

 with height == Height(LeftSubtree(X))+2
Result:
new root of rebalanced subtree
**/

AVLNode* rotateLeft2(AVLNode* x,AVLNode* z) {
    // Z is by 2 higher than its sibling
    AVLNode* t23 = z->lchild; // Inner child of Z
    x->rchild = t23;
    if(t23 != NULL)
        t23->parent = x;

    z->lchild = x;
    x->parent = z;

    // 1st case, BalanceFactor(Z) == 0, only happens with deletion, not insertion:
    if(z->bf == 0) {  // t23 has been of same height as t4
        x->bf = +1;   // t23 now higher
        z->bf = -1;   // t4 now lower than X
    } else {// 2nd case happens with insertion or deletion:
        x->bf = 0;
        z->bf = 0;
    }

    return z; // return new root of rotated subtree
}

/**

Right rotate for LL case

Insertion case:

      5(-2) [x]
     /  \
[y] 3(-1) 6
   / \    [t4]
  2   4          =>
 /    [t23]
1
[t1]

**/
AVLNode* rotateRight2(AVLNode* x,AVLNode* y) {
    // Z is by 2 higher than its sibling
    AVLNode* t23 = y->rchild; // Inner child of Z
    x->lchild = t23;
    if(t23 != NULL)
        t23->parent = x;

    y->rchild = x;
    x->parent = y;


    if(y->bf == 0) {
        x->bf = -1;
        y->bf = +1;
    } else {// 2nd case happens with insertion or deletion:
        x->bf = 0;
        y->bf = 0;
    }

    return y; // return new root of rotated subtree
}

AVLNode* parent(AVLNode* n) {
    return n->parent;
}
AVLNode* leftChild(AVLNode* n) {
    return n->lchild;
}
AVLNode* rightChild(AVLNode* n) {
    return n->rchild;
}
int BalanceFactor(AVLNode* n) {
    return n->bf;
}

AVLNode* updateBalance2(AVLNode* Z) {
    for(AVLNode* X = parent(Z), *N, *G; X != NULL; X = parent(Z)) {  // Loop (possibly up to the root)
        // BalanceFactor(X) has to be updated:
        if(Z == X->rchild) {  // The right subtree increases
            if(BalanceFactor(X) > 0) {  // X is right-heavy
                // ===> the temporary BalanceFactor(X) == +2
                // ===> rebalancing is required.
                G = parent(X); // Save parent of X around rotations
                if(BalanceFactor(Z) < 0)  {  // Right Left Case     (see figure 5)
                    //  N = rotateRightLeft2(X,Z); // Double rotation: Right(Z) then Left(X)
                } else                           // Right Right Case    (see figure 4)
                    N = rotateLeft2(X,Z);      // Single rotation Left(X)
                // After rotation adapt parent link
            } else {
                if(BalanceFactor(X) < 0) {
                    X->bf = 0; // Z��s height increase is absorbed at X.
                    return NULL; // Leave the loop
                }
                X->bf = +1;
                Z=X; // Height(Z) increases by 1
                continue; //Root is not changed (no rebalance)
            }
        } else { // Z == left_child(X): the left subtree increases
            if(BalanceFactor(X) < 0) {  // X is left-heavy
                // ===> the temporary BalanceFactor(X) == �C2
                // ===> rebalancing is required.
                G = parent(X); // Save parent of X around rotations
                if(BalanceFactor(Z) > 0)  {   // Left Right Case
                    // N = rotateLeftRight2(X,Z); // Double rotation: Left(Z) then Right(X)
                } else                           // Left Left Case
                    N = rotateRight2(X,Z);     // Single rotation Right(X)
                // After rotation adapt parent link
            } else {
                if(BalanceFactor(X) > 0) {
                    X->bf = 0; // Z��s height increase is absorbed at X.
                    return NULL; // Leave the loop since X.bf is 0 now
                }
                X->bf = -1;
                Z=X; // Height(Z) increases by 1
                continue; //Root is not changed (no rebalance)
            }
        }
        // After a rotation adapt parent link:
        // N is the new root of the rotated subtree
        // Height does not change: Height(N) == old Height(X)
        N->parent = G;
        if(G != NULL) {
            if(X == G->lchild)
                G->lchild= N;
            else
                G->rchild = N;
            return NULL;
        }
        return N; // N is the new root of the total tree
        // There is no fall thru, only break; or continue;
    }
// Unless loop is left via break, the height of the total tree increases by 1.
}

//AVLNode* updateBalance2(AVLNode* n) {
//    AVLNode* g=NULL;
//     for (AVLNode* x = n->parent; x; x = g) { // Loop (possibly up to the root)
//     G = parent(X); // Save parent of X around rotations
//     // BalanceFactor(X) has not yet been updated!
//     if (N == left_child(X)) { // the left subtree decreases
//         if (BalanceFactor(X) > 0) { // X is right-heavy
//             // ===> the temporary BalanceFactor(X) == +2
//             // ===> rebalancing is required.
//             Z = right_child(X); // Sibling of N (higher by 2)
//             b = BalanceFactor(Z);
//             if (b < 0)                     // Right Left Case     (see figure 5)
//                 N = rotate_RightLeft(X,Z); // Double rotation: Right(Z) then Left(X)
//             else                           // Right Right Case    (see figure 4)
//                 N = rotate_Left(X,Z);      // Single rotation Left(X)
//             // After rotation adapt parent link
//         }
//         else {
//             if (BalanceFactor(X) == 0) {
//                 BalanceFactor(X) = +1; // N��s height decrease is absorbed at X.
//                 break; // Leave the loop
//             }
//             N = X;
//             BalanceFactor(N) = 0; // Height(N) decreases by 1
//             continue;
//         }
//     }
//     else { // (N == right_child(X)): The right subtree decreases
//         if (BalanceFactor(X) < 0) { // X is left-heavy
//             // ===> the temporary BalanceFactor(X) == �C2
//             // ===> rebalancing is required.
//             Z = left_child(X); // Sibling of N (higher by 2)
//             b = BalanceFactor(Z);
//             if (b > 0)                     // Left Right Case
//                 N = rotate_LeftRight(X,Z); // Double rotation: Left(Z) then Right(X)
//                else                        // Left Left Case
//                 N = rotate_Right(X,Z);     // Single rotation Right(X)
//             // After rotation adapt parent link
//         }
//         else {
//             if (BalanceFactor(X) == 0) {
//                 BalanceFactor(X) = �C1; // N��s height decrease is absorbed at X.
//                 break; // Leave the loop
//             }
//             N = X;
//             BalanceFactor(N) = 0; // Height(N) decreases by 1
//             continue;
//         }
//     }
//     // After a rotation adapt parent link:
//     // N is the new root of the rotated subtree
//     parent(N) = G;
//     if (G != null) {
//         if (X == left_child(G))
//             left_child(G) = N;
//         else
//             right_child(G) = N;
//         if (b == 0)
//             break; // Height does not change: Leave the loop
//     }
//     else {
//         tree->root = N; // N is the new root of the total tree
//         continue;
//     }
//     // Height(N) decreases by 1 (== old Height(X)-1)
// }
//}


/**

!Iteration version insert
Separate binary search tree insertion and AVL tree rebalance

**/
AVLNode* insertItr(AVLNode* root, AVLNode* node, UpdateBalanceFunc updateBalance=updateBalanceItr) {
    if(!root) return node;
    if(!node) return root;
    //[Top down] Find subtree to insert
    AVLNode* pre=NULL;
    AVLNode* p=root;
    while(p) {
        pre=p;
        if(node->data<p->data)
            p=p->lchild;
        else
            p=p->rchild;
    }
    //Insert
    if(node->data<pre->data)
        pre->lchild=node;
    else
        pre->rchild=node;
    node->parent=pre;
    //[Bottom up] Rebalance
    AVLNode* newRoot=updateBalanceItr(node);
    //Tree root update: If overall root changed
    if(newRoot&&newRoot->parent==NULL)
        return newRoot;
    return root;
}








///////////////////////////////////////////////
// Test
///////////////////////////////////////////////


static void printBF(AVLNode* p, std::vector<AVLNode*>* nodes=NULL) {
    if(p) {
        printBF(p->lchild,nodes);
//        printf("%d,%d,",p->data,p->bf);
//printf("%d,",p->bf);
        printf("%d(%d) ",p->data,p->bf);
        if(nodes)
            nodes->push_back(p);
        printBF(p->rchild,nodes);
    }

}
//static std::list<AVLNode*>* printBF(AVLNode* p) {
//    std::list<AVLNode*>* nodes=new std::list<AVLNode*>();
//    printBF(p,nodes);
//    return nodes;
//}


static void assertAVLTree(int* a, int n,int* expectedBFArr=NULL) {

    AVLNode* p=NULL;

    for(int i=0; i<n; ++i) {
        printf("------adding %d --------\n",a[i]);
//        p=insert(p,createAVLNode(a[i]));
        p=insertItr(p,createAVLNode(a[i]));
//        inOrderTraverse((BiTNode*)p,printNodeAsInt);
//        printf("\n");
        if(i!=n-1) {
            printBF(p);
            printf("\n");
        }
    }
    preOrderTraverse((BiTNode*)p,printAsInt);
    printf("\n");
    std::vector<AVLNode*> nodes;
    printBF(p,&nodes);
    printf("\n");
    bool flag=isBST((BiTNode*)p);
    printf("BST: %d\n",flag);
    assert(flag);
    printf("-----------------\n");
    if(expectedBFArr) {
//        printf("%d\n",nodes.size());
//        for(std::vector<AVLNode*>::iterator itr=nodes.begin();itr!=nodes.end();++itr){
//            printf("%d\n",(*itr)->bf);
//        }
        for(int i=0; i<n; ++i) {
//                printf("%d\n",nodes[i]->bf);
            assert(expectedBFArr[i]==(nodes[i]->bf));
        }
    }


    BiTNode* pre=NULL;
    BiTNode* succ=NULL;
    for(int i=0; i<n; ++i) {
        printf("searching %d, ",a[i]);
        BSTNode* result=searchByBSTree((BSTNode*)p,a[i]);
        assert(result);
        printf("Found %d\n",result->data);
        assert(result->data==a[i]);

        printf("In order predecessor of %d is ",a[i]);
        pre=inOrderPredecessor((BiTNode*)result);
        if(pre) {
            printf("%d\n",pre->data);
            assert(pre->data<=a[i]);
        } else printf("null\n");


        printf("In order successor of %d is ",a[i]);
        succ=inOrderSuccessor((BiTNode*)result);
        if(succ) {
            printf("%d\n",succ->data);
            assert(succ->data>=a[i]);
        } else printf("null\n");
    }

    for(int i=0; i<n; ++i) {
        printf("deleting %d \n",a[i]);
//        p=(AVLNode*)deleteNode2((BSTNode**)&p,a[i]);
        p=(AVLNode*)deleteNode((BSTNode*)p,a[i]);
        if(p) {
            printf("cur root: %d\n",p->data);
//            inOrderTraverse((BiTNode*)p,printNodeAsInt);
        } else {
            printf("all deleted\n");
            assert(i==n-1);
        }
    }

    printf("====================\n");
}

static void testAVLInsert() {

    int n;
//    int a[]= {'5','1','8','5','3'};


//    int a[]= {'5','1','8','5','3'};
//    n=sizeof(a)/sizeof(int);
//    testAVLTreeCore(a,n);

//LL case
    int ll[]= {4,3,5,2,1};
    //bf in order verification array
    int llInOrderBF[]= {0,0,0,-1,0};
    n=sizeof(ll)/sizeof(int);
    assertAVLTree(ll,n,llInOrderBF);

//LL case
    int ll2[]= {3,2,1};
    int ll2bf[]= {0,0,0};
    n=sizeof(ll2)/sizeof(int);
    assertAVLTree(ll2,n,ll2bf);



    //RR case
    int rr[]= {2,1,3,4,5};
    int rrbf[]= {0,1,0,0,0};
    n=sizeof(rr)/sizeof(int);
    assertAVLTree(rr,n,rrbf);

    //RR case, root change
    int rr2[]= {1,2,3};
    int rr2bf[]= {0,0,0};
    n=sizeof(rr2)/sizeof(int);
    assertAVLTree(rr2,n,rr2bf);

    printf("LR\n");
    //LR case
    int lr[]= {5,4,6,2,3};
    int lrbf[]= {0,0,0,-1,0};
    n=sizeof(lr)/sizeof(int);
    assertAVLTree(lr,n,lrbf);

    //RL case
    int rl[]= {3,2,4,6,5};
    int rlbf[]= {0,1,0,0,0};
    n=sizeof(rl)/sizeof(int);
    assertAVLTree(rl,n,rlbf);


    int t1[]= {6,4,8,3,5,7,9,2,1};
    int t1bf[]= {0,0,0,-1,0,-1,0,0,0};
    n=sizeof(t1)/sizeof(int);
    assertAVLTree(t1,n,t1bf);

}


static void assertAVLTreeDel(int* a, int n, int key,int* expectedBFArr=NULL) {

    AVLNode* p=NULL;
    BSTNode* pre=NULL;
    BSTNode* succ=NULL;
    for(int i=0; i<n; ++i) {
        p=insertItr(p,createAVLNode(a[i]));
    }




    printf("deleting %d\n",key);
    printBF(p);
    printf("\n");
    p=deleteNode(p,key);

    std::vector<AVLNode*> nodes;
    printBF(p,&nodes);
    printf("\n");

    if(p) printf("new root: %d\n",p->data);

    if(expectedBFArr) {

        for(int i=0; i<n-1; ++i) {
//                printf("%d\n",nodes[i]->bf);
            assert(expectedBFArr[i]==(nodes[i]->bf));
        }
    }

    printf("-------------------------\n");
}

void testAVLTree() {

    testAVLInsert();


    int n;
    int del1[]= {1,2,3,4,5};
    n=sizeof(del1)/sizeof(int);
    //no children node del: no balance, no propogate
    int del1bf[]= {0,1,0,-1};
    assertAVLTreeDel(del1,n,5,del1bf);
    //no children node del: no balance, propogate
    int del2bf[]= {0,1,1,0};
    assertAVLTreeDel(del1,n,3,del2bf);
    //no children node del: balance, propogate
    int del3bf[]= {1,0,-1,0};
    assertAVLTreeDel(del1,n,1,del3bf);



    int del2[]= {3,2,4,1,5};
    n=sizeof(del2)/sizeof(int);
    //only left child node del
    int del21bf[]= {0,1,1,0};
    assertAVLTreeDel(del2,n,2,del21bf);
    //only right child node del
    int del22bf[]= {0,-1,-1,0};
    assertAVLTreeDel(del2,n,4,del22bf);



    ////////////////////////////////////////
    //two children node del cases
    n=sizeof(del2)/sizeof(int);
    //two children node del case:no rebalance
    int del23bf[]= {0,-1,-1,0};
    assertAVLTreeDel(del2,n,3,del23bf);

    n=sizeof(del1)/sizeof(int);
    int del13bf[]= {0,1,0,-1};
    assertAVLTreeDel(del1,n,4,del13bf);

    int del3[]= {4,2,1,3,5};
    n=sizeof(del3)/sizeof(int);
    int del31bf[]= {0,1,1,0};
    assertAVLTreeDel(del3,n,2,del31bf);



    int t1[]= {6,4,8,3,5,7,9,2,1};
    n=sizeof(t1)/sizeof(int);
    for(int i=0; i<n; ++i)
        assertAVLTreeDel(t1,n,t1[i]);

//    assertAVLTreeDel(t1,n,4);
}






