#include "../bitree/bitree.h"
#include "../bitree/bstree.h"
#include "../test_utils.h"


BSTNode* createBSTNode(int data) {
    BSTNode* n=(BSTNode*)malloc(sizeof(BSTNode));
    n->data=data;
    n->lchild=NULL;
    n->rchild=NULL;
    n->parent=NULL;
    return n;
}


//!Used for both sort and search
BSTNode* searchByBSTree(BSTNode* p, int key) {
    while(p) {
        if(key==p->data)
            return p;
        if(key<p->data)
            p=p->lchild;
        else
            p=p->rchild;
    }
    return NULL;
}

BSTNode* searchByBSTree2(BSTNode* p, int key) {
    if(!p) return NULL;
    if(key==p->data)
        return p;
    if(key<p->data)
        return searchByBSTree2(p->lchild,key);
    return searchByBSTree2(p->rchild,key);
}

//Left most node
BSTNode* findMin(BSTNode* p) {
    BSTNode* pre=NULL;
    while(p) {
        pre=p;
        p=p->lchild;
    }
    return pre;
}

//Right most node
BSTNode* findMax(BSTNode* p) {
    BSTNode* pre=NULL;
    while(p) {
        pre=p;
        p=p->rchild;
    }
    return pre;
}


//!TODO insert recursive version

//Allow duplicate values
BSTNode* insertToBSTree(BSTNode* root, BSTNode* node) {
    if(!root) return node;
    if(!node) return root;
    BSTNode* pre=NULL;
    BSTNode* p=root;
    while(p) {
        pre=p;
        if(node->data<p->data)
            p=p->lchild;
        else
            p=p->rchild;
    }
    if(node->data<pre->data)
        pre->lchild=node;
    else
        pre->rchild=node;
    return root;
}

BSTNode* insertToBSTree(BSTNode* root, int data) {
    return insertToBSTree(root,createBSTNode(data));
}


//No duplicate value
BSTNode* insertToBSTreeUnique(BSTNode* root, BSTNode* node) {
    if(!root) return node;
    if(!node) return root;
    BSTNode* pre=NULL;
    BSTNode* p=root;
    while(p) {
        if(node->data==p->data) //If existed, just return original one
            return root;
        pre=p;
        if(node->data<p->data)
            p=p->lchild;
        else
            p=p->rchild;
    }
    if(node->data<pre->data)
        pre->lchild=node;
    else
        pre->rchild=node;
    return root;
}




//!Sort by BSTree
BSTNode* sortByBSTree(int* x,int n) {
    BSTNode* p=NULL;
    for(int i=0; i<n; ++i)
        p=insertToBSTree(p,x[i]);
    return p;
}

//Verify if it's BST
bool isBST(BiTNode* node,int minKey,int maxKey) {
    if(node==NULL) return true;
    if(node->data<minKey||node->data>maxKey) return false;
    return isBST(node->lchild,minKey,node->data)&&isBST(node->rchild,node->data,maxKey);
}
bool isBST(BiTNode* node) {
    return isBST(node,INT_MIN,INT_MAX);
}

/**
node:
The one to be deleted which has been found by search

Return:
It can be
1) original root
2) new root
2.1) succ
2.2) left child
2.3) right child
3) NULL if it's last node

**/
BSTNode* deleteThisNode(BSTNode* root, BSTNode* parent,BSTNode* node) {
    BSTNode* newRoot=root;
    //1) Have two children
    if(node->lchild&&node->rchild) {
        BSTNode* succ=node->rchild;
        BSTNode* p=node;
        //!Have to get parent of succ, cannot use findMin function
        while(succ->lchild) {
            p=succ;//Keep parent
            succ=succ->lchild;
        }

        succ->lchild=node->lchild;//lchild of deleted node
        p->lchild=succ->rchild;//lchild of succ'sparent
        /**succ can be node->rchild, if node->rchild doesn't
        have any left child, so it has to stop succ->rchild
        point to itself(node->rchild)**/
        if(succ!=node->rchild)
            succ->rchild=node->rchild;

        if(parent) {
            if(node==parent->lchild)
                parent->lchild=succ;
            else
                parent->rchild=succ;
        } else
            newRoot=succ;//!case 2.1
    } else if(node->lchild) { //Only left child
        if(parent) {
            if(node==parent->lchild)
                parent->lchild=node->lchild;
            else
                parent->rchild=node->lchild;
        } else
            newRoot=node->lchild;//!case 2.2
    } else if(node->rchild) { //Only right child
        if(parent) {
            if(node==parent->lchild)
                parent->lchild=node->rchild;
            else
                parent->rchild=node->rchild;
        } else
            newRoot=node->rchild;//!case 2.3
    } else {
        if(parent)
            if(node==parent->lchild)
                parent->lchild=NULL;
            else
                parent->rchild=NULL;
        else newRoot=NULL; //!case 3: all removed
    }
    free(node);
    return newRoot;
}

//Iteration version
BSTNode* deleteNode(BSTNode* root,int data) {
    if(root==NULL) return NULL;

    //1) Search
    BSTNode* node=root;
    BSTNode* parent=NULL;
    while(node) {
        if(data==node->data)
            break;    //Found the node to delete
        parent=node;
        if(data<node->data)
            node=node->lchild;
        else
            node=node->rchild;
    }
    if(node==NULL) return root;//Not found
    //2) Handle deletion
    return deleteThisNode(root,parent,node);
}



//Return:
//empty tree will return NULL, cannot distinguish not removed and empty tree
//xxx If tree root is removed, return new tree root, otherwise NULL

void deleteNode2(BSTNode** treeRoot, BSTNode* parent, BSTNode* node, int data) {
    if(node==NULL) return; //Finally, not found the node to remove
    if(data<node->data)
        deleteNode2(treeRoot,node,node->lchild,data);
    else if(data>node->data)
        deleteNode2(treeRoot,node,node->rchild,data);
    else
        *treeRoot=deleteThisNode(*treeRoot,parent,node);
}

/**
!?if dupcliate nodes for the same value, just pick one

Return: root of new tree, if tree is empty, return NULL

**/
BSTNode* deleteNode2(BSTNode** root, int data) {
    deleteNode2(root,NULL,*root,data);
    return *root;
}


/**
!TODO
Delete by replace

**/

//static Node* bstree_delete(BSTree tree, Node *z)
//{
//    Node *x=NULL;
//    Node *y=NULL;
//
//    if ((z->left == NULL) || (z->right == NULL) )
//        y = z;
//    else
//        y = bstree_successor(z);
//
//    if (y->left != NULL)
//        x = y->left;
//    else
//        x = y->right;
//
//    if (x != NULL)
//        x->parent = y->parent;
//
//    if (y->parent == NULL)
//        tree = x;
//    else if (y == y->parent->left)
//        y->parent->left = x;
//    else
//        y->parent->right = x;
//
//    if (y != z)
//        z->key = y->key;
//
//    if (y!=NULL)
//        free(y);
//
//    return tree;
//}


void testIsBST() {
    int pre[]= {'a','b','d','h','e','i','c','f','g','j','k'};
    int in[]= {'h','d','b','e','i','a','f','c','g','k','j'};
    BiTNode* root=createByPreAndIn(pre,in,11);
    bool flag=isBST(root);
    printf("BST: %d\n",flag);
}

void testBSTree() {
    BSTNode* root=NULL;
    BSTNode* node=NULL;

    int a[]= {5,1,8,5,3};
    int n=sizeof(a)/sizeof(int);

    root=sortByBSTree(a,n);

    inOrderTraverse((BiTNode*)root,printAsInt);

    printf("\n");

    node=findMin(root);
    printf("Min:%d\n",node->data);

    node=findMax(root);
    printf("Max:%d\n",node->data);


    for(int i=0; i<n; ++i) {
        node=searchByBSTree(root,a[i]);
        printf("node value:%d\n",node->data);

        node=searchByBSTree2(root,a[i]);
        printf("node value:%d\n",node->data);
    }



    //Unique value
    root=NULL;
    for(int i=0; i<n; ++i)
        root=insertToBSTreeUnique(root,createBSTNode(a[i]));
    inOrderTraverse((BiTNode*)root,printAsInt);
    printf("\n");


    //Force cast pointer
    int b[]= {'5','1','8','5','3'};
    n=5;
    root=NULL;
    for(int i=0; i<n; ++i)
        root=insertToBSTreeUnique(root,createBSTNode(b[i]));

    preOrderTraverse((BiTNode*)root);
    printf("\n");
    inOrderTraverse((BiTNode*)root,printAsInt);
    printf("\n");
    levelOrderTraByBFS((BiTNode*)root);
    printf("\n");
    int d=getTreeDepth((BiTNode*)root);
    printf("%d\n",d);
    destroyTree((BiTNode*)root);

    testIsBST();

}

