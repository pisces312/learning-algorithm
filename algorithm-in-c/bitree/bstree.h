#ifndef BSTREE_H_INCLUDED
#define BSTREE_H_INCLUDED


/************************************************
BST - structure definitions

*************************************************/
//For search tree
//Can be cast to basic binary tree node "BiTNode"
typedef struct BSTNode {
    struct BSTNode *lchild, *rchild;
    int data;
    struct BSTNode * parent;//for impl with parent
} BSTNode;

BSTNode* createBSTNode(int data);

/*****************************
BST - Algs without parent link
******************************/
//Non-recursive version
BSTNode* searchByBSTree(BSTNode* root, int key);
bool isBST(BiTNode* node);
BSTNode* insertToBSTree(BSTNode* root, int data);
BSTNode* insertToBSTree(BSTNode* root, BSTNode* node);
//!Iteration version
BSTNode* deleteNode(BSTNode* root,int data);
//Recursive version
BSTNode* deleteNode2(BSTNode** root, int data);


/*****************************
BST - Algs with parent link
******************************/
BSTNode* insertToBSTree_p(BSTNode* root, BSTNode* node);
BSTNode* insertToBSTree_p(BSTNode* root, int data);

/*****************************

AVL tree
A kind of BST

******************************/

typedef struct AVLNode {
    struct AVLNode *lchild, *rchild;
    int data;
    struct AVLNode *parent;//for impl with parent
    int bf;//for impl based on balanceFactor
    int height;//for impl based on height
} AVLNode;


typedef AVLNode*(*UpdateBalanceFunc)(AVLNode*);

////////////////////////
// Implementation 1
////////////////////////
AVLNode* rotateLeft(AVLNode* rotRoot);
AVLNode* rotateRight(AVLNode* rotRoot) ;
AVLNode* rebalance(AVLNode* node) ;
AVLNode* updateBalance(AVLNode* node) ;
AVLNode* updateBalanceItr(AVLNode* root) ;
//Recursive version
AVLNode* insert(AVLNode* root, AVLNode* node) ;


AVLNode* rotateLeft(AVLNode* x,AVLNode* z);
AVLNode* rotateRight(AVLNode* x,AVLNode* y) ;
AVLNode* updateBalance2(AVLNode* node) ;


//!Common bstree insertion part
AVLNode* insertItr(AVLNode* root, AVLNode* node, UpdateBalanceFunc) ;
//AVLNode* insertItr(AVLNode* root, AVLNode* node) ;













#endif // BSTREE_H_INCLUDED
