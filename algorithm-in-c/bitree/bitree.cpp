#include "bitree.h"
#include "../test_utils.h"

BiTNode* createBiTNode(int data) {
    BiTNode* n=(BiTNode*)malloc(sizeof(BiTNode));
    n->data=data;
    n->lchild=NULL;
    n->rchild=NULL;
    n->parent=NULL;
    return n;
}
/**
! Recursively construct tree by pre and in order
**/
BiTNode* createByPreAndIn(int* pre,int* in,int nodeCount) {
    if(nodeCount==0) return NULL;

    BiTNode *root=(BiTNode*)malloc(sizeof(BiTNode));
    root->data=pre[0];

    //Find current root pos in InOrderSeq
    int i=0;
    while(pre[0]!=in[i])
        ++i;

    root->lchild=createByPreAndIn(pre+1,in,i);
    root->rchild=createByPreAndIn(pre+1+i,in+i+1,nodeCount-i-1);
    return root;
}

BiTNode* createByPostAndIn(int* post,int* in,int nodeCount) {
    if(nodeCount==0)
        return NULL;

    BiTNode *root=(BiTNode*)malloc(sizeof(BiTNode));
    root->data=post[nodeCount-1];

    int i=0;
    while(post[nodeCount-1]!=in[i])  ++i;

    root->lchild=createByPostAndIn(post,in,i);
    root->rchild=createByPostAndIn(post+i,in+i+1,nodeCount-i-1);
    return root;
}


void preOrderTraverse(BiTNode* root, NodeVisitor visit) {
    if(root != NULL) {
        visit(&root->data);
        //Push root to stack and go to left child
        preOrderTraverse(root->lchild,visit);
        //Pop root from stack and go to right
        preOrderTraverse(root->rchild,visit);
    }
}

void inOrderTraverse(BiTNode* root,NodeVisitor visit) {
    if(root != NULL) {
        inOrderTraverse(root->lchild,visit);
        visit(&root->data);
        inOrderTraverse(root->rchild,visit);
    }
}

void preOrderTraverse(BiTNode* root) {
    preOrderTraverse(root,printAsChar);
}

void inOrderTraverse(BiTNode* root) {
    inOrderTraverse(root,printAsChar);
}

void postOrderTraverse(BiTNode* root) {
    if(root != NULL) {
        postOrderTraverse(root->lchild);
        postOrderTraverse(root->rchild);
        printf("%c ", root->data);
    }
}

BiTNode* findNode(BiTNode *root, int k) {
    if (root == NULL)
        return NULL;
// If key is present at root, or in left subtree or right subtree,
// return true;
    if (root->data == k || findNode(root->lchild, k) || findNode(root->rchild, k))
        return root;
    return NULL;
}


/**

Depth starts with 0
Depth of empty tree: 0
Depth of root: 1

**/
int getTreeDepth(BiTNode* p) {
    if(p==NULL) return 0;
    int left=getTreeDepth(p->lchild);
    int right=getTreeDepth(p->rchild);
    return (left>right?left:right)+1;
}


/**

Post order to free node

Have to visit two children first
then free parent

**/
void destroyTree(BiTNode* p) {
    if(!p) return;
    destroyTree(p->lchild);
    destroyTree(p->rchild);
    printf("free %c\n",p->data);
    free(p);
}

void printBiTree(BiTNode *p,NodeVisitor visitor,int n) {
    if(p==NULL) return;
    printBiTree(p->lchild,visitor,n+1);
    for(int i=0; i<n; ++i)
        printf("\t");
    visitor(&p->data);
//        for(int i=0; i<=n+1; ++i)
    printf("\n\n");

    printBiTree(p->rchild,visitor,n+1);
}
void printBiTree(BiTNode *p,NodeVisitor visitor=printAsChar) {
    printBiTree(p,visitor,0);
}

int printBiTNodeAsChar(void* p) {
    return printf("%c ",((BiTNode*)p)->data);
}
int printBiTNodeAsInt(void* p) {
    return printf("%d ",((BiTNode*)p)->data);
}

