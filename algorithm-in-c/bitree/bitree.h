#ifndef BITREE_H_INCLUDED
#define BITREE_H_INCLUDED

typedef struct BiTNode {
    struct BiTNode *lchild, *rchild;
    int data;
    struct BiTNode *parent;//only for bitree_p.cpp
} BiTNode, *BiTree;

BiTNode* createBiTNode(int data);


typedef int(*NodeVisitor)(void*);
//typedef struct BiTNodeWithFlag {
//    struct BiTNodeWithFlag *lchild, *rchild;
//    int data;
//    int flag;
//} BiTNodeWithFlag;

void printBiTree(BiTNode *p,NodeVisitor visitor);

int printBiTNodeAsChar(void* p);
int printBiTNodeAsInt(void* p);
/////////////////////////////////
//Tree creation
//This is only used for BiTNode
BiTNode* createByPreAndIn(int* pre,int* in,int nodeCount);
BiTNode* createByPostAndIn(int* post,int* in,int nodeCount);
//!No pre and post

/////////////////////////////////
//Tree traverse recursive version
void preOrderTraverse(BiTNode* root, NodeVisitor visit);
void inOrderTraverse(BiTNode* root,NodeVisitor visit);

void preOrderTraverse(BiTNode* root);
void inOrderTraverse(BiTNode* root);
void postOrderTraverse(BiTNode* root);

/////////////////////////////////////////////
//Tree traverse stack version
void preOrderIterByArrayStack(BiTNode* root);
void preOrderIterByStackLib2(BiTNode* p);
void preOrderIterByStackLib(BiTNode* p);

void preOrderStackWay3ByLib(BiTNode* root);
void preOrderStackWay4ByLib(BiTNode* p);

void inOrderTraverseStack(BiTNode* p);
void postOrderIter(BiTNode* p);
void postOrderTraverseStack2(BiTNode* p);
//void postOrderTraverseStackWithFlag1(BiTNodeWithFlag* p);
//void postOrderTraverseStackWithFlag2(BiTNodeWithFlag* p);
//void postOrderTraverseStackWithFlag3(BiTNodeWithFlag* p);

////////////////////////////////
//Layer traverse
//Top down and left to right
void levelOrderTraByBFS(BiTNode* p);
void levelOrderTraByOneQueue(BiTNode* p);
void levelOrderTraByTwoQueue(BiTNode* p);
void levelOrderTraRecursive(BiTNode* p);
int traverseOneLevel(BiTNode* root,int level);
void levelOrderTraByArray(BiTNode* p);
//Top down and right to left
int traverseFromR2L(BiTNode* root,int level);
void levelOrderTraByQueueR2L(BiTNode* p);
void levelOrderTraByQueueR2L2(BiTNode* p);
//Bottom up and right to left
void levelOrderTraBottomUpAndR2L(BiTNode* p);
void levelOrderTraBottomUpAndR2L2(BiTNode* p);

//Get kth nodes of level m (m,k starts from 0)
int getKthValueOfLevel(BiTNode* p,int m,int k);
int getKthValueOfLevel2(BiTNode* root,int m,int k);

int getTreeDepth(BiTNode* p);

void destroyTree(BiTNode* p);


BiTNode* findNode(BiTNode *root, int k);


/***************************************************************

Algorithms with parent pointer

***************************************************************/
BiTNode* inOrderPredecessor(BiTNode* node);
BiTNode* inOrderSuccessor(BiTNode* node);
bool isTreeRoot(BiTNode* node);



#endif // BITREE_H_INCLUDED
