#include "bitree.h"
#include "bstree.h"
#include "../test_utils.h"
#include "../test_utils_c++.h"
#include<vector>
#include <map>

/***************************************************************

Lowest Common Ancestor (LCA)

Assumption: two nodes must in tree


1. Lowest Common Ancestor in a Binary Search Tree.

Given values of two nodes in a Binary Search Tree, find the Lowest Common Ancestor (LCA).

http://www.geeksforgeeks.org/lowest-common-ancestor-in-a-binary-search-tree/



2. Lowest Common Ancestor in a Binary Tree

Given a binary tree (not a binary search tree) and two values say n1 and n2, write a program to find the least common ancestor.

Following is definition of LCA from Wikipedia:
Let T be a rooted tree. The lowest common ancestor between two nodes n1 and n2 is defined as the lowest node in T that has both n1 and n2 as descendants (where we allow a node to be a descendant of itself).

The LCA of n1 and n2 in T is the shared ancestor of n1 and n2 that is located farthest from the root.

http://www.geeksforgeeks.org/lowest-common-ancestor-binary-tree-set-1/



Scenario:
As part of a procedure for determining the distance between pairs of nodes in a tree:
the distance from n1 to n2 can be computed as the distance from the root to n1, plus the distance from the root to n2, minus twice the distance from the root to their lowest common ancestor.


***************************************************************/

/**

[Recommended] Using BST properties

While traversing from top to bottom, the first node n we encounter with value between n1 and n2, i.e., n1 < n < n2 or same as one of the n1 or n2, is LCA of n1 and n2 (assuming that n1 < n2).

So just recursively traverse the BST in, if node's value is greater than both n1 and n2 then our LCA lies in left side of the node, if it's smaller than both n1 and n2, then LCA lies on right side. Otherwise root is LCA (assuming that both n1 and n2 are present in BST)

**/
BSTNode* lcaOfBSTree(BSTNode* p,const int n1,const int n2) {
    if(p==NULL)
        return NULL;
    if(n1>n2) //! swap, make sure n1<=n2
        return lcaOfBSTree(p,n2,n1);
    if(p->data<n1)
        return lcaOfBSTree(p->rchild,n1,n2);
    if(p->data>n2)
        return lcaOfBSTree(p->lchild,n1,n2);
    return p;
}

BSTNode* lcaOfBSTree2(BSTNode* p,const int n1,const int n2) {
    if(p==NULL)
        return NULL;
    if(p->data>n1&&p->data>n2)
        return lcaOfBSTree2(p->lchild,n1,n2);
    if(p->data<n1&&p->data<n2)
        return lcaOfBSTree2(p->rchild,n1,n2);
    return p;
}

BSTNode* lcaOfBSTree3(BSTNode* p,const int n1,const int n2) {
    while(p)
        if(p->data>n1&&p->data>n2)
            p=p->lchild;
        else if(p->data<n1&&p->data<n2)
            p=p->rchild;
        else
            break;
    return p;
}




/**

Method1 for binary tree (By Storing root to n1 and root to n2 paths)

1) Find path from root to n1 and store it in a vector or array.
2) Find path from root to n2 and store it in another vector or array.
3) Traverse both paths till the values in arrays are same. Return the common element just before the mismatch.


Time Complexity: Time complexity of the above solution is O(n). The tree is traversed twice, and then path arrays are compared.

**/


//From leaf to root
bool findLeafToRootPath(BiTNode* p,int data,std::vector<BiTNode*>& path) {
    if(p==NULL) return false;
    bool flag=false;
    if(p->data==data) {
        flag=true;
    } else {
        flag=findLeafToRootPath(p->lchild,data,path);
        if(!flag)
            flag=findLeafToRootPath(p->rchild,data,path);
    }
    if(flag)
        path.push_back(p);
    return flag;
}

BiTNode* lcaOfBiTree(BiTNode* p,const int n1,const int n2) {
    std::vector<BiTNode*> path1,path2;
    if(findLeafToRootPath(p,n1,path1)
            &&findLeafToRootPath(p,n2,path2)) {
        int i=path1.size()-1,j=path2.size()-1;
        for(; i>=0&&j>=0; --i,--j)
            if(path1[i]!=path2[j])
                break;
        return path1[i+1];
//        if(i>=0&&j>=0)
//            return path1[i+1];
//        if(i<0)//8,20; 14,8,20
//            return path1[0];
//        if(j<0)//14,8,20; 8,20
//            return path2[0];
    }
    return NULL;
}


//From root to leaf
bool findRootToLeafPath(BiTNode* p,int data,std::vector<BiTNode*>& path) {
    if(p==NULL) return false;
    path.push_back(p);//add first, later may pop
    if(p->data==data)
        return true;
    bool flag=findRootToLeafPath(p->lchild,data,path);
    if(!flag)
        flag=findRootToLeafPath(p->rchild,data,path);
    if(!flag)
        path.pop_back();
    return flag;
}


BiTNode* lcaOfBiTree2(BiTNode* p,const int n1,const int n2) {
    std::vector<BiTNode*> path1,path2;
    if(findRootToLeafPath(p,n1,path1)
            &&findRootToLeafPath(p,n2,path2)) {
        int i=0,j=0;
        int size1=path1.size(),size2=path2.size();
        for(; i<size1&&j<size2; ++i,++j)
            if(path1[i]!=path2[j])
                break;
        return path1[i-1];//!path has at least one element
//        if(i<size1&&j<size2)
//            return path1[i-1];
//        //Return the last element of shorter path
//        if(i==size1)
//            return path1[size1-1];
//        if(j==size2)
//            return path2[size2-1];
    }
    return NULL;
}


/**

[Recommended] Method 2 for binary tree (Using Single Traversal)

!Precondition: both n1 and n2 are in the tree

The method 1 finds LCA in O(n) time, but requires three tree traversals plus extra spaces for path arrays. If we assume that the keys n1 and n2 are present in Binary Tree, we can find LCA using single traversal of Binary Tree and without extra storage for path arrays.
The idea is to traverse the tree starting from root. If any of the given keys (n1 and n2) matches with root, then root is LCA (assuming that both keys are present). If root doesn��t match with any of the keys, we recur for left and right subtree. The node which has one key present in its left subtree and the other key present in right subtree is the LCA. If both keys lie in left subtree, then left subtree has LCA also, otherwise LCA lies in right subtree.

Time Complexity: Time complexity of the above solution is O(n) as the method does a simple tree traversal in bottom up fashion.
Note that the above method assumes that keys are present in Binary Tree. If one key is present and other is absent, then it returns the present key as LCA (Ideally should have returned NULL).


**/
BiTNode* lcaOfBiTreeOneTra(BiTNode* p,const int n1,const int n2) {
    if(p==NULL) return NULL;
    if(p->data==n1||p->data==n2)
        return p;
    BiTNode* left=lcaOfBiTreeOneTra(p->lchild,n1,n2);
    BiTNode* right=lcaOfBiTreeOneTra(p->rchild,n1,n2);
    if(left&&right)
        return p;
    return left?left:right;//n1 is child of n2 or n2 is child of n1
}

/**

More general

Consider n1 and n2 may not in the tree

Extend method2


**/

static BiTNode* lcaOfBiTreeUtils(BiTNode* p,const int n1,const int n2,bool& v1,bool& v2) {
    if(p==NULL) return NULL;
    if(p->data==n1) {
        v1=true;
        return p;//! this only checks one node
    }
    if(p->data==n2) {
        v2=true;
        return p;
    }
    BiTNode* left=lcaOfBiTreeUtils(p->lchild,n1,n2,v1,v2);
    BiTNode* right=lcaOfBiTreeUtils(p->rchild,n1,n2,v1,v2);
    if(left&&right)
        return p;
    return left?left:right;
}

BiTNode* lcaOfBiTreeOneTra2(BiTNode* p,const int n1,const int n2) {
    // Initialize n1 and n2 as not visited
    bool v1 = false, v2 = false;

// Find lca of n1 and n2 using the technique discussed above
    BiTNode *lca = lcaOfBiTreeUtils(p, n1, n2, v1, v2);

// Return LCA only if both n1 and n2 are present in tree
    if (v1 && v2 || v1 && findNode(lca, n2) || v2 && findNode(lca, n1))
        return lca;

// Else return NULL
    return NULL;
}
/**

Lowest Common Ancestor in a Binary Tree | Set 2 (Using Parent Pointer)

If we are given a BST where every node has parent pointer, then LCA can be easily determined by traversing up using parent pointer and printing the first intersecting node.

Finding LCA becomes easy when parent pointer is given as we can easily find all ancestors of a node using parent pointer.
1. Create an empty hash table.
Insert n1 and all of its ancestors in hash table.
2. Check if n2 or any of its ancestors exist in hash table, if yes return the first existing ancestor.


http://www.geeksforgeeks.org/lowest-common-ancestor-in-a-binary-tree-set-2-using-parent-pointer/

**/
BiTNode* lcaOfBiTreeUsingParentPtr(BiTNode* p,BiTNode* n1,BiTNode* n2) {
    // Creata a map to store ancestors of n1
    std::map <BiTNode *, bool> ancestors;

// Insert n1 and all its ancestors in map
    while (n1 != NULL) {
        ancestors[n1] = true;
        n1 = n1->parent;
    }

// Check if n2 or any of its ancestors is in map.
    while (n2 != NULL) {
        if (ancestors.find(n2) != ancestors.end())
            return n2;
        n2 = n2->parent;
    }

    return NULL;
}


/**

TODO

Find LCA in Binary Tree using RMQ


http://www.geeksforgeeks.org/find-lca-in-binary-tree-using-rmq/

**/



/**********************************************************************************

Test


**********************************************************************************/

static BSTNode * bstreeForLCATest() {
    BSTNode *root        = createBSTNode(20);
    root->lchild               = createBSTNode(8);
    root->rchild              = createBSTNode(22);
    root->lchild->lchild         = createBSTNode(4);
    root->lchild->rchild        = createBSTNode(12);
    root->lchild->rchild->lchild  = createBSTNode(10);
    root->lchild->rchild->rchild = createBSTNode(14);
    return root;
}

static BSTNode * bstreeWithParentForLCATest() {
    BSTNode * root = NULL;

    root = insertToBSTree_p(root, 20);
    root = insertToBSTree_p(root, 8);
    root = insertToBSTree_p(root, 22);
    root = insertToBSTree_p(root, 4);
    root = insertToBSTree_p(root, 12);
    root = insertToBSTree_p(root, 10);
    root = insertToBSTree_p(root, 14);
    return root;
}
static BiTNode * bitreeForLCATest() {
    BiTNode *root        = createBiTNode(1);
    root->lchild = createBiTNode(2);
    root->rchild = createBiTNode(3);
    root->lchild->lchild = createBiTNode(4);
    root->lchild->rchild = createBiTNode(5);
    root->rchild->lchild = createBiTNode(6);
    root->rchild->rchild = createBiTNode(7);
    return root;
}


static void assertLCAOfBSTree(BSTNode* p,const int n1,const int n2,int e=INT_MIN) {
    BSTNode* t;

    t=lcaOfBSTree(p,n1,n2);
    printf("LCA of %d and %d is %d \n", n1, n2, t->data);
    if(e!=INT_MIN) assert(t->data==e);

    t=lcaOfBSTree2(p,n1,n2);
    printf("LCA of %d and %d is %d \n", n1, n2, t->data);
    if(e!=INT_MIN) assert(t->data==e);

    t=lcaOfBSTree3(p,n1,n2);
    printf("LCA of %d and %d is %d \n", n1, n2, t->data);
    if(e!=INT_MIN) assert(t->data==e);
}


void testLCAOfBSTree() {
    BSTNode* root=bstreeForLCATest();
    printBiTree((BiTNode*)root,printAsInt);
    assertLCAOfBSTree(root,10,14,12);
    assertLCAOfBSTree(root,14,8,8);
    assertLCAOfBSTree(root,8,14,8);
    assertLCAOfBSTree(root,10,22,20);

}

static void assertLCAOfBiTree(BiTNode* p,const int n1,const int n2,int e=INT_MIN) {
    BiTNode* t;
    bool flag;


    if(findNode(p,n1)&&findNode(p,n2)) {
        t=lcaOfBiTreeOneTra(p,n1,n2);
        if(t)
            printf("lcaOfBiTreeOneTra: LCA of %d and %d is %d \n", n1, n2, t->data);
        if(e!=INT_MIN) assert(t->data==e);
    }


    //! Support the case that not in the tree

    std::vector<BiTNode*> path1;
    flag=findLeafToRootPath(p,n1,path1);
    if(flag)
        printVector(path1,printAsInt);
    path1.clear();
    flag=findRootToLeafPath(p,n1,path1);
    if(flag)
        printVector(path1,printBiTNodeAsInt);


    std::vector<BiTNode*> path2;
    flag=findLeafToRootPath(p,n2,path2);
    if(flag)
        printVector(path2,printBiTNodeAsInt);
    path2.clear();
    flag=findRootToLeafPath(p,n2,path2);
    if(flag)
        printVector(path2,printBiTNodeAsInt);


    t=lcaOfBiTree(p,n1,n2);
    if(t)
        printf("lcaOfBiTree: LCA of %d and %d is %d \n", n1, n2, t->data);
    else
        printf("lcaOfBiTree: LCA of %d and %d is not found \n", n1, n2);
    if(e!=INT_MIN) assert(t->data==e);

    t=lcaOfBiTree2(p,n1,n2);
    if(t)
        printf("lcaOfBiTree2: LCA of %d and %d is %d \n", n1, n2, t->data);
    else
        printf("lcaOfBiTree2: LCA of %d and %d is not found \n", n1, n2);
    if(e!=INT_MIN) assert(t->data==e);


    t=lcaOfBiTreeOneTra2(p,n1,n2);
    if(t)
        printf("lcaOfBiTreeOneTra2: LCA of %d and %d is %d \n", n1, n2, t->data);
    else
        printf("lcaOfBiTreeOneTra2: LCA of %d and %d is not found \n", n1, n2);
    if(e!=INT_MIN) assert(t->data==e);

    printf("-----------------------------\n");
}


void testLCAOfBiTree() {
    BiTNode* root=(BiTNode*)bstreeForLCATest();
    printBiTree((BiTNode*)root,printAsInt);

    assertLCAOfBiTree(root,10,14,12);
    assertLCAOfBiTree(root,14,8,8);
    assertLCAOfBiTree(root,8,14,8);
    assertLCAOfBiTree(root,10,22,20);

    BiTNode* root2=(BiTNode*)bitreeForLCATest();
    printBiTree((BiTNode*)root2,printAsInt);

    assertLCAOfBiTree(root2,4,5,2);
    assertLCAOfBiTree(root2,4,6,1);
    assertLCAOfBiTree(root2,3,4,1);
    assertLCAOfBiTree(root2,2,4,2);
    assertLCAOfBiTree(root2,7,8);//not all in the tree
}

void testLCAUsingParent() {
    BSTNode *root= bstreeWithParentForLCATest();
    printBiTree((BiTNode*)root,printAsInt);

    BiTNode *n1 = (BiTNode*)root->lchild->rchild->lchild;
    BiTNode *n2 = (BiTNode*)root->rchild;
    BiTNode *lca = lcaOfBiTreeUsingParentPtr((BiTNode*)root,n1, n2);

    printf("LCA of %d and %d is %d \n", n1->data, n2->data, lca->data);
}

void testLCA() {
    testLCAOfBSTree();
    testLCAOfBiTree();
    testLCAUsingParent();
}
