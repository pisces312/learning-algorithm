#include "bitree.h"
#include "../test_utils.h"
#include "../test_utils_c++.h"
#include <stack>
#include <queue>

/**************************************************************************

Non-recursive traverse


Performance:

The stack used in this implementation probably requires dynamic memory allocation,
so it's unclear whether the iterative implementation would be more or less efficient than the recursive solution


**************************************************************************/



/**
!!Recommended

!The stack's push and pop sequence is opposite of recursive version
Make sure that the left node is always popped before the right node

Time: O(n)

**/
void preOrderIterByStackLib(BiTNode* p) {
    if(p==NULL)
        return;
    std::stack<BiTNode*> s;
    s.push(p);
    while(!s.empty()) {
        p=s.top();
        s.pop();

        //Debug
        printf("--pop <%c>: ",p->data);
        printStack<BiTNode*>(s,printBiTNodeAsChar);

        //Visit
        printf("Visit <%c> \n",p->data);
        if(p->rchild) {
            s.push(p->rchild);

            //Debug
            printf("--push right <%c>: ",p->rchild->data);
            printStack<BiTNode*>(s,printBiTNodeAsChar);
        }
        if(p->lchild) {
            s.push(p->lchild);

            //Debug
            printf("--push left <%c>: ",p->lchild->data);
            printStack<BiTNode*>(s,printBiTNodeAsChar);
        }

    }
}


/**
!!Recommended

!Simulate the stack's push and pop sequence of recursive version

**/
void preOrderIterByStackLib2(BiTNode* p) {
    std::stack<BiTNode*> s;
    while(p || !s.empty()) //p here is just for entering the loop first time
        if(p != NULL) {
            //if non-leaf node, go to left child
            //!-> if(p!=NULL)
            printf("Visit <%c> \n",p->data);
            s.push(p);

            //Debug
            printf("Stack after push <%c>: ",p->data);
            printStack<BiTNode*>(s,printBiTNodeAsChar);

            p = p->lchild; //!-> preOrder(p->lchild)
        } else {
            //if leaf node, go to parent
            p = s.top();
            s.pop();
            // here p is parent again and it cannot be NULL
            //!-> if(p!=NULL)

            //Debug
            printf("Stack after pop <%c>: ",p->data);
            printStack<BiTNode*>(s,printBiTNodeAsChar);

            p = p->rchild; //!-> preOrder(p->rchild)
        }
}

void preOrderStackWay3ByLib(BiTNode* p) {
    if(p==NULL)
        return;
    std::stack<BiTNode*> s;
    while(true)
        if(p!=NULL) {
            printf("%c ",p->data);
            s.push(p);
            p = p->lchild;
        } else {
            if(s.empty())//Only check stack when pop action
                break;
            p = s.top();
            s.pop();
            p= p->rchild;
        }
}


void preOrderStackWay4ByLib(BiTNode* p) {
    if(p==NULL)
        return;
    std::stack<BiTNode*> s;
    while(true) {
        //1. Visit root
        //2. Traverse left child
        while(p) {
            printf("%c ",p->data);
            s.push(p);
            p=p->lchild;
        }
        //3. Pop and traverse right child until stack is empty
        if(!s.empty()) { //check empty first, can avoid the root is NULL
            p=s.top();
            s.pop();
            p=p->rchild;
        } else
            break;
    }
}


/**

Drawback: Don't know stack size. Just use a supposed max size

**/
#define MAXSIZE 100
void preOrderIterByArrayStack(BiTNode* p) {
    if(p == NULL)
        return;
    BiTNode* s[MAXSIZE];
    int top = -1;
//p!=NULL is for entering the loop in first time
//because top<0 at that time
    while(p || top >= 0)
        if(p!=NULL) {
            printf("%c ",p->data);
            ++top;
            s[top] = p;
            p = p->lchild;
        } else {//visit right child
            p = s[top];
            --top;
            p= p->rchild;
        }
}


void inOrderTraverseStack(BiTNode* p) {
    BiTNode* stack[MAXSIZE];
    int top = -1;

    if(p == NULL)
        return;

//p!=NULL is for entering the loop in first time
//because top<0 at that time
    while(p || top >= 0) {
        if(p!=NULL) {
            ++top;
            stack[top] = p;
            p = p->lchild;
        } else {
            p = stack[top];
            --top;
            printf("%c ",p->data); //visit data
            p= p->rchild;
        }
    }
}

/*******************************************************

Post order iteration version

*******************************************************/


//Easy to understand
//Pop and push back
void postOrderTraverseStackWithFlag3(BiTNodeWithFlag* p) {
    std::stack<BiTNodeWithFlag*> s;
    while(true) {
        while(p) {
            p->flag=false;
            s.push(p); //1st push
            p=p->lchild;
        }
        if(!s.empty()) {
            //Pop, no matter the flag
            p=s.top();
            s.pop();
            if(p->flag) {
                printf("%c ",p->data);
                p=NULL; //Set back to NULL
            } else {
                p->flag=true;
                s.push(p); //2nd push
                p=p->rchild;
            }
        } else
            break;
    }
    printf("\n");
}


void postOrderIter(BiTNode* p) {
    BiTNode* s[MAXSIZE];
    //indicate whether two children are visited
    // 0 for unvisited, 1 for visited
    int tag[MAXSIZE] = {0};
    int top = -1;

    while(p || top >= 0) {
        if(p != NULL) {
            s[++top] = p;
            tag[top] = 0;
            p = p->lchild;
        } else if(tag[top] == 0) {
            //if right child is not visited, visit and update flag
            p=s[top];
            p=p->rchild;
            tag[top]= 1;
        } else {
            //! current p must be NULL,
            //! so next iteration will visit parent
            //! s[top] doesn't change p's value
            //!! [tricky] if use 'p=s[top--]', it has to set p to NULL back
            // top-- means pop
            printf("%c ",s[top--]->data);
        }
    }
}

//No need to check whether root is NULL for "while(true)"
void postOrderTraverseStack2(BiTNode* p) {
    BiTNode* s[MAXSIZE];
    int tag[MAXSIZE];
    int top = -1;
    while(true) {
        while(p) {
            s[++top] = p;
            tag[top] = 0;
            p = p->lchild;
        }
        if(top>=0) {
            if(tag[top] == 0) {
                p= s[top];
                p= p->rchild;
                tag[top]= 1;
            } else {
                printf("%c ",s[top]->data);
                top--;
            }
        } else
            break;
    }
}

//Avoid set p=NULL and push back
void postOrderTraverseStackWithFlag1(BiTNodeWithFlag* p) {
    std::stack<BiTNodeWithFlag*> s;
    while(true) {
        while(p) {
            p->flag=false;
            s.push(p);
            p=p->lchild;
        }
        if(!s.empty()) {
            if(s.top()->flag) {
                //!Not change p here
                //!Still use s.top()
                printf("%c ",s.top()->data);
                s.pop();
            } else {
                p=s.top();
                p->flag=true;
                p=p->rchild;
            }
        } else
            break;
    }
}


void postOrderTraverseStackWithFlag2(BiTNodeWithFlag* p) {
    std::stack<BiTNodeWithFlag*> s;
    while(true) {
        while(p) {
            p->flag=false;
            s.push(p);
            p=p->lchild;
        }
        if(!s.empty()) {
            //After while, p must be NULL
            //! But p's value is changed unexpectedly here
            p=s.top();
            if(p->flag) {
                printf("%c ",p->data);
                s.pop();
                p=NULL;//! so have to restore it to original value(NULL)
            } else {
                p->flag=true;
                p=p->rchild;
            }
        } else
            break;
    }
}


