#include "bitree.h"
#include "../test_utils.h"


/**
    a(b,c)
    b(d,e)
    c(f,g)
    d(h,)
    e(,i)
    f()
    g(,j)
    h()
    i()
    j(,k)
    k()
                 a
             /       \
            b         c
          /   \     /   \
        d      e   f     g
       /        \         \
      h          i         j
                          /
                         k

   pre: a b d h e i c f g j k
    in: h d b e i a f c g k j
  post: h d i e b f k j g c a
 layer: a b c d e f g h i j k

**/
BiTNode* testTreeCreationByPreAndIn() {
    int pre[]= {'a','b','d','h','e','i','c','f','g','j','k'};
    int in[]= {'h','d','b','e','i','a','f','c','g','k','j'};
    BiTNode* root=createByPreAndIn(pre,in,11);
    return root;
}

BiTNode* testTreeCreationByPostAndIn() {
    int post[]= {'h','d','i','e','b','f','k','j','g','c','a'};
    int in[]= {'h','d','b','e','i','a','f','c','g','k','j'};

    BiTNode* root=createByPostAndIn(post,in,11);
    preOrderTraverse(root);
    printf("\n");
    inOrderTraverse(root);
    printf("\n");
    postOrderTraverse(root);
    printf("\n");
    return root;

}



void testTreeTraverse() {
    BiTNode* root= testTreeCreationByPreAndIn();
    preOrderTraverse(root);
    printf("\n");
    inOrderTraverse(root);
    printf("\n");
    postOrderTraverse(root);
    printf("\n");
}

//BiTNodeWithFlag* createByPreAndIn2(int* pre,int* in,int nodeCount) {
//    if(nodeCount==0)
//        return NULL;
//    BiTNodeWithFlag *root=(BiTNodeWithFlag*)malloc(sizeof(BiTNodeWithFlag));
//    root->data=pre[0];
//    int i=0;
//    while(pre[0]!=in[i])
//        ++i;
//    root->lchild=createByPreAndIn2(pre+1,in,i);
//    root->rchild=createByPreAndIn2(pre+1+i,in+i+1,nodeCount-i-1);
//    return root;
//}

static void preOrderRec(BiTNode* p) {
    if(p != NULL) {
        printf("Visit <%c>\n",p->data);
        printf("--Push <%c>\n",p->data);
        preOrderRec(p->lchild);
        printf("--Pop <%c>\n",p->data);
    }
    if(p != NULL) {
        //don't need to care push and pop
        //no further function needs p any more
        preOrderRec(p->rchild);
    }
}

void testBiTreeTraverseStack() {
    BiTNode* root= testTreeCreationByPreAndIn();

    printf("\nCompare the stack order of recursive pre order: \n");
    preOrderRec(root);
    printf("\n");

    printf("\npreOrderIterByStackLib: \n");
    preOrderIterByStackLib(root);
    printf("\n");


    printf("\npreOrderIterByStackLib2: \n");
    preOrderIterByStackLib2(root);
    printf("\n");

    printf("\npreOrderStackWay3ByLib: ");
    preOrderStackWay3ByLib(root);
    printf("\n");

    printf("\npreOrderStackWay4ByLib: ");
    preOrderStackWay4ByLib(root);
    printf("\n");

    preOrderIterByArrayStack(root);
    printf("\n");

    inOrderTraverseStack(root);
    printf("\n");
    postOrderIter(root);
    printf("\n");
    postOrderTraverseStack2(root);
    printf("\n");

//    int pre[]= {'a','b','d','h','e','i','c','f','g','j','k'};
//    int in[]= {'h','d','b','e','i','a','f','c','g','k','j'};
//
//    BiTNodeWithFlag* root2=createByPreAndIn2(pre,in,11);
//    postOrderTraverseStackWithFlag1(root2);
//    printf("\n");
//    postOrderTraverseStackWithFlag2(root2);
//    printf("\n");
//    postOrderTraverseStackWithFlag3(root2);
//    printf("\n");

}

void testLayerTraverse() {
    BiTNode* root= testTreeCreationByPreAndIn();
    levelOrderTraByBFS(root);
    printf("\n");
    levelOrderTraByOneQueue(root);
    printf("\n");
    levelOrderTraRecursive(root);
    printf("\n");
    levelOrderTraByArray(root);
    printf("\n");
    levelOrderTraByTwoQueue(root);
    printf("\n");

    //Right to left
    levelOrderTraByQueueR2L(root);
    printf("\n");
    levelOrderTraByQueueR2L2(root);
    printf("\n");
    //
    levelOrderTraBottomUpAndR2L(root);
    printf("\n");
    levelOrderTraBottomUpAndR2L2(root);
    printf("\n");

    //
    int v=getKthValueOfLevel(root,2,2);
    printf("%c\n",v);
    v=getKthValueOfLevel2(root,2,2);
    printf("%c\n",v);
}


void testBiTree() {
    testTreeCreationByPostAndIn();

    BiTNode* root= testTreeCreationByPreAndIn();
    int depth=getTreeDepth(root);
    printf("%d\n",depth);

    destroyTree(root);

    testLayerTraverse();
    testBiTreeTraverseStack();
    testTreeTraverse();

//    testTreeCreationByPreAndIn();
}
