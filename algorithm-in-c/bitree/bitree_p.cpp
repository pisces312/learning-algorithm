#include"bitree.h"
#include"../test_utils.h"



bool isTreeRoot(BiTNode* node) {
    return node&&(node->parent==NULL);
}

BiTNode* inOrderPredecessor(BiTNode* node) {
    BiTNode* left=node->lchild;
    BiTNode* p=NULL;
    //1) Have left child, get right most of left child
    if(left) {
        p=left;
        while(p->rchild)
            p=p->rchild;
        return p;
    }

    p=node;
    while(p->parent&&p->parent->lchild==p)
        p=p->parent;
    //2) No left child. It's always the left child of parents.
    if(p->parent==NULL)
        return NULL;
    //3) No left child. At least one ancestor,
    //it's the right child of this ancestor
    //just return this one
    return p->parent;
}

BiTNode* inOrderSuccessor(BiTNode* node) {
    BiTNode* right=node->rchild;
    BiTNode* p=NULL;
    //1) Have right child, get right most of right child
    if(right) {
        p=right;
        while(p->lchild)
            p=p->lchild;
        return p;
    }

    p=node;
    while(p->parent&&p->parent->rchild==p)
        p=p->parent;
    //2) No right child. It's always the right child of parents.
    if(p->parent==NULL)
        return NULL;
    //3) No right child. At least one ancestor,
    //it's the left child of this ancestor
    //just return this one
    return p->parent;
}


//Use flag
//BiTNode* GetFirstAncestor(BiTNode* n1, BiTNode*n2) {
//    if(n1==NULL||n2==NULL||n1==n2) return NULL;
//    //
//    BiTNode* p=n1->parent;
//    while(p) {
//        p->flag=true;
//        p=p->parent;
//    }
//    p=n2->parent;
//    while(p&&!p->flag) {
//        p=p->parent;
//    }
//
//    bool found=p&&p->flag;
//
//    Node* q=n1->parent;
//    while(q) {
//        q->flag=false;
//        q=q->parent;
//    }
//
//
//    if(found) {
//        return p;
//    }
//    return NULL;
//}
