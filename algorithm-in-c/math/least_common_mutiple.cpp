#include"../common.h"
extern int gcd(int,int);
//x=gcd*a, y=gcd*b
//lcm=a*b*gcd=a*gcd*b*gcd/gcd=x*y/gcd

int lcm(int x,int y) {
    return x*y/gcd(x,y);
}
int lcmOfArray(int* a,int n) {
    if(n<=0)
        return -1;
    int t=a[0];
    int i=1;
    while(i<n) {
        t=lcm(t,a[i]);
        ++i;
    }
    return t;

}
void testLCM() {
    int n=lcm(42,30);
    printf("%d\n",n);

    n=lcm(30,42);
    printf("%d\n",n);
}
