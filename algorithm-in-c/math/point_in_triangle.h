#ifndef POINTINTRIANGLE_H_INCLUDED
#define POINTINTRIANGLE_H_INCLUDED
#include <math.h>
/**
判断点是否在三角形内

**/
namespace PointInTriangle {
    //误差
    const double e=1e-5;
    struct point {
        double x,y;

    };
    void swap(point& a,point& b) {
        cout<<"swap\n";
        point c=a;
        a=b;
        b=c;
    }
    double getSideLen(const point& a,const point& b) {
        return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
    }
    void calcTriangleLen(const point& A,const point& B,const point& C,double &a,double &b,double &c) {
        a=getSideLen(B,C);
        b=getSideLen(A,C);
        c=getSideLen(A,B);

    }
    double getTriangleArea(const point& A,const point& B,const point& C) {
        double a,b,c=0;
        calcTriangleLen(A,B,C,a,b,c);
        double p=(a+b+c)/2;
        return sqrt((p-a)*(p-b)*(p-c)*p);


    }
    /**
    法一
    使用面积，由一点和三边构成的三个面积，如果和为三角形本身的面积则点
    在三角形内，否则在外
    D是要判断的点
    isOn判断是否在线上
    **/
    bool isInTriangle(point A,point B,point C,point D,bool isOn) {
        if (isOn) {
            return !(getTriangleArea(A,B,D)+getTriangleArea(B,C,D)+getTriangleArea(A,C,D)>getTriangleArea(A,B,C));
        }
        return !(getTriangleArea(A,B,D)+getTriangleArea(B,C,D)+getTriangleArea(A,C,D)>=getTriangleArea(A,B,C));

//        return !(getTriangleArea(A,B,D)+getTriangleArea(B,C,D)+getTriangleArea(A,C,D)>getTriangleArea(A,B,C));

//        if (getTriangleArea(A,B,D)+getTriangleArea(B,C,D)+getTriangleArea(A,C,D)-getTriangleArea(A,B,C)>e) {
//            return false;
//        }
//        return true;

    }
    /**
    法二

    三角形内部的点，沿三角形的边界逆时针走，则一定保持在边界的左边！！
    即判断一个点p3是否在射线p1p2的左边，可以通过p1p2，p1p3两个向量叉积的正负来判断
    (ax,ay) (bx,by),(cx,cy)
    (bx-ax,by-ay)
    (cx-ax,cy-ay)

    **/
    double product(const point& A,const point& B,const point& C) {
        return (B.x-A.x)*(C.y-A.y)-(C.x-A.x)*(B.y-A.y);
//        return (B.x-A.x)*(C.x-A.x)-(C.y-A.y)*(B.y-A.y);
    }
    //!!!依赖于A，B，C的位置关系，输入顺序有关！！！！
    //!即以A B C的逆时针顺序传入参数
    //???如何将三个点按逆时针顺序摆放,可以固定第一个点，调整后两个点的位置，即swap两个点
// 摆放点的位置
//(C.y-B.y)*(B.x-A.x)>0
//逆时针分两种情况
//C.y>B.y&&A.x<=B.x  || B.y>=C.y&&B.x<A.x

    bool isInTriangle2(point A,point B,point C,point D,bool isOn) {
//        if(
//!判断点的传入顺序是否符合逆时针！
        double flag=(C.y-B.y)*(B.x-A.x);
        if (flag<0) {
            swap(B,C);
        } else if (flag==0) {
            //       A
            //   C   B
            //
            if (A.x==B.x) {
                if (A.y>B.y) {
                    if (C.x<A.x) {
                        swap(B,C);
                    }

                } else if (A.y<B.y) {
                    if (C.x>A.x) {
                        swap(B,C);
                    }

                }
            } else if (C.y==B.y) {
                if (C.x>B.x) {
                    if (C.y>A.y) {
                        swap(B,C);
                    }

                } else if (C.x<B.x) {
                    if (C.y<A.y) {
                        swap(B,C);
                    }

                }
            }

//            else if(C.y==B.y&&
        }
//        cout<<product(A,B,D)<<endl;
//        cout<<product(C,B,D)<<endl;
//        cout<<product(A,C,D)<<endl;
        if (isOn)
            return product(A,B,D)>=0&&product(B,C,D)>=0&&product(C,A,D)>=0;
        return product(A,B,D)>0&&product(B,C,D)>0&&product(C,A,D)>0;
    }
// TODO (Administrator#1#): 如何判断一个店在一个凸多边形内 p278
// TODO (Administrator#1#): 如何判断一个点是否在一个不自交多边形（不保证为凸的）内 怎样判断一个点是否在一个四面体内 p278

    void testPointInTriangle() {
        //
        //        B
        //D2 A  D C
        point a={-1,0};
        point b={1,2};
        point c={1,0};
//        point c={1,0};
        point d={0,0};
        point d2={-1,1};
        point d3={0,0.5};
//        //
//        cout<<isInTriangle(a,b,c,d,false)<<endl;
//        //在线上的情况
//        cout<<isInTriangle(a,b,c,d,true)<<endl;
//        //
//        cout<<isInTriangle(a,b,c,d2,false)<<endl;
//        cout<<isInTriangle(a,b,c,d2,true)<<endl;
//        //!!!依赖于A，B，C的位置关系，输入顺序有关！！！！
//        //!即以A B C的逆时针顺序传入参数
//        cout<<isInTriangle2(b,a,c,d,false)<<endl;
        cout<<isInTriangle2(b,a,c,d,true)<<endl;
        cout<<isInTriangle2(a,b,c,d,false)<<endl;
//        //
        cout<<isInTriangle2(b,a,c,d2,false)<<endl;
        cout<<isInTriangle2(b,a,c,d2,true)<<endl;
        //
        cout<<isInTriangle2(b,a,c,d3,false)<<endl;
        cout<<isInTriangle2(b,a,c,d3,true)<<endl;
//        int a
    }
};


#endif // POINTINTRIANGLE_H_INCLUDED
