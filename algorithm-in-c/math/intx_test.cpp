#include "Intx.h"
Intx factorialBigInt(int x) {
    Intx ret;
    return x<=0 ? ret.set(0):x==0||x==1 ? ret.set(1) : ret.multiple(factorialBigInt(x-1),ret.set(x));
//    return x<=0 ? ret.set(0):x==0||x==1 ? ret.set(1) : (factorial(x-1)*ret.set(x));
}
void testIntx() {
    factorialBigInt(50).get();
}
