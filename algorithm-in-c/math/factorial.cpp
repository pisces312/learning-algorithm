#include"../test_utils.h"
/**
Not tail recursive, recursive is not the last step
it needs to multiply n.
**/
int factorial(int n) {
    if(n==1) return 1;
    return n*factorial(n-1);
}
/**
Tail recursive

**/
static int _factorialTailRecursive(int n,int f) {
    if(n==1) return f;
    return _factorialTailRecursive(n-1,f*n);
}
int factorialTailRecursive(int n) {
    return _factorialTailRecursive(n,1);
}

int factorialIteration(int n) {
    int f=1;
    for(; n; --n)
        f*=n;
    return f;
}

void testFactorial() {
    int r=factorial(5);
    printf("%d\n",r);

    r=factorialTailRecursive(5);
    printf("%d\n",r);

    r=factorialIteration(5);
    printf("%d\n",r);
}
