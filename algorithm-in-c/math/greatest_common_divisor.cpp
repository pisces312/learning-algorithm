#include "../test_utils.h"
/**********************************************
Greatest common divisor

所谓求整数a、b的最大公约数，
就是求同时满足a%c=0、b%c=0的最大正整数c，
即求能够同时整除a和b的最大正整数c。

若a、b其中有一个为0，那么最大公约数即为a、b中非零的那个；
若a、b均为0，则最大公约数不存在（任意数均可同时整除它们）。

!! x>0||y>0, cannot both be 0

***********************************************/

/**
Basic-碾转相除 %
f(x,y)=f(y,x%y) (x>=y>0) 直到x、y中的一个为零

Recursive version

!!No need to make sure x>=y
Because if x<=y, gcd(y,x%y)=gcd(y,x)
just equivalent to swap x and y

**/
int gcd(int x,int y) {
    if(x==0&&y==0) return 0;//0 for error
    return (!y)?x:gcd(y,x%y);
}
//iteration version
int gcd4(int a,int b) {
    if(a==0&&b==0) return 0;//0 for error
    while(b) {
        int r=a%b;
        a=b;
        b=r;
    }
    return a;
}


/**
取模运算开销大，采用减法避免
原因：如果一个数能同时整除x和y，则必能同时整除x-y和y；而能够同时整除x-y和y的数也必能同时整除x和y，即x和y的公约数与x-y和y的公约数是相同的，其最大公约数也相同
公式：f(x,y)=f(x-y,y)
证明：z为一个数

if x%z==0, y%z==0
即x可恰好由m个z组成，同理y由n个z组成
所以(x-y)=(m-n)z，当然以下式子成立
(x-y)%z==0

Use "-" instead of "%"
Make sure x>=y

**/
int gcdByMinus(int x,int y) {
    if(x==0&&y==0) return 0;//0 for error
    if(x<y) //swap x and y by argument order adjust
        return gcdByMinus(y,x);
    if(y==0)
        return x;
    return gcdByMinus(y,x-y); //Not consider argument order
//    return gcd2(x-y,y); //ok
}


//iteration version
int gcdByMinus2(int a,int b) {
    if(a==0&&b==0) return 0;//0 for error
    while(true) {
        if(a<b)
            swapInt(&a,&b);
        if(b==0)
            break;
        int r=a-b;
        a=b;
        b=r;
    }
    return a;
}


/**
四种情况讨论
1. x,y even 即都能提出公因数2
f(x,y)=2*f(x/2,y/2)=2*f(x>>1,y>>1)=f(x>>1,y>>1)<<1
2. x even,y odd , 2一定不在公约数中，可以将x除2变小
f(x,y)=2*f(x/2,y)=f(x>>1,y)
3. x odd, y even, 同理
f(x,y)=2*f(x,y/2)=f(x,y>>1)
4. x,y odd,用一般方法，可以用减或取余
f(x,y)=f(y,x-y)
奇数之间相减，结果一定是偶数，即一奇一偶的情况
优点：利用移位提高效率

Use ">>" to avoid big integer division
**/

int gcdDivBy2(int x,int y) {
    if(x==0&&y==0) return 0;//0 for error
    if(x<y)
        return gcdDivBy2(y,x);
    if(y==0)
        return x;
    if(isEven(x)) {
        if(isEven(y)) //must *2 if both are divided by 2
            return gcdDivBy2(x>>1,y>>1)<<1;
        return gcdDivBy2(x>>1,y);
    }
    if(isEven(y))
        return gcdDivBy2(x,y>>1);
    return gcdDivBy2(y,x-y);
}


/**
Get GDC of multiple numbers

**/
int gcdOfArray(int* a,int n) {
    if(n<=0)
        return -1;
    int t=a[0];
    int i=1;
    while(i<n) {
        t=gcdByMinus2(t,a[i]);
        ++i;
    }
    return t;

}

static void assertGCD(int x,int y) {
    int expect=gcd(x,y);
    printf("gcd=%d\n",expect);

    int n=gcdByMinus(x,y);
    printf("gcdByMinus=%d\n",n);
    assert(expect==n);

    n=gcdByMinus2(x,y);
    printf("gcdByMinus2=%d\n",n);
    assert(expect==n);

    n=gcdDivBy2(x,y);
    printf("gcdDivBy2=%d\n",n);
    assert(expect==n);

    n=gcd4(x,y);
    printf("gcd4=%d\n",n);
    assert(expect==n);


}
void testGCD() {
    assertGCD(42,30);
    assertGCD(30,42);
    assertGCD(30,0);
    assertGCD(0,30);

    int a[]= {4,12,16};
//    int a[]={1,2,3,4,5,6};
    PRINT_INT(gcdOfArray(a,3));

//    PRINT_INT(lcmOfArray(a,3));
}
