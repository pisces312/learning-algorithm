#include"intx.h"
#include <iostream>
#include <vector>
using namespace std;

Intx::~Intx() {
    data.clear();
}
Intx Intx::set(int n) {//将整数n放置在vector<int> data中
    while (n) {
        data.insert(data.begin(), n%10);
        n /= 10;
    }
    return *this;
}
void Intx::get() { //打印这个整数
    for (unsigned int i=0; i<data.size(); i++) {
        cout<<(int)data.at(i);
    }
//        for each(int i in data) cout << i;
}
int Intx::length() {
    return data.size();
}
Intx Intx::multiple(Intx a,Intx b) {
    Intx ret;
    for (int i = 0; i < a.length()+b.length(); i++) // 先为返回值分配足够的空间
        ret.data.push_back(0);
    for (int i = 0; i < a.length(); i++)
        for (int j = 0; j < b.length(); j++) {
            int pos = ret.length()-1-(i+j);
            int temp = a.data[a.length()-1-i] * b.data[b.length()-1-j];
            ret.data[pos] += temp%10;
            if (ret.data[pos] > 9) {
                ret.data[pos] %= 10;
                ret.data[pos-1]++;
            }
            ret.data[pos-1] += temp/10;
            if (ret.data[pos-1] > 9) {
                ret.data[pos-1] %= 10;
                ret.data[pos-2]++;
            }
        }
    while (ret.data.front()==0) { // 删除开头的0
        ret.data.erase(ret.data.begin());
    }
    return ret;
}

