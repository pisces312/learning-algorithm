#include "../test_utils.h"

/**
C(n,k)=C(n-1,k)+C(n-1,k-1)
**/
int calcCombineByFormula(int n,int k) {
    if(n==k||k==0)
        return 1;
    return calcCombineByFormula(n-1,k)+calcCombineByFormula(n-1,k-1);
}
/**
By definition
**/
int calcCombineByDefinition(int n,int k) {
    int divisor=n-k;
    for(int i=divisor-1; i>0; --i)
        divisor*=i;
    int dividend=divisor;
    for(int i=n-k+1; i<=n; ++i)
        dividend*=i;
    return dividend/divisor;
}
