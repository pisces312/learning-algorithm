#include"../test_utils.h"
/****************************************************************

Ugly Numbers

Ugly numbers are numbers whose only prime factors are 2, 3 or 5. The sequence 1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, �� shows the first 11 ugly numbers. By convention, 1 is included.
Given a number n, the task is to find n��th Ugly number.
Input  : n = 7
Output : 8

Input  : n = 10
Output : 12

Input  : n = 15
Output : 24

Input  : n = 150
Output : 5832

http://www.geeksforgeeks.org/?p=753


****************************************************************/

/**

Memo version

Need many space

not best solution

**/
long uglyNumMemo(int* lookup,int n) {
    static int primes[]= {2,3,5};
//    if(n==1) return 1;
    for(int i=1,c=0; ; ++i) {
        if(lookup[i]==-1) {
            bool flag=false;
            int t=i;
            for(int j=0; j<3&&!flag; ++j)
                while((t%primes[j])==0) {
                    t/=primes[j];
                    if(lookup[t]==1) {
                        ++c;
                        flag=true;
                        lookup[i]=1;
//                        printf("set %d\n",i);
                        break;
                    }
                }
        } else
            ++c;
        if(c==n)
            return i;
    }
    return 0;
}

void testUglyNum() {
    int maxLookup=1024*1024;
    int* lookup=new int[maxLookup];
    for(int i=0; i<maxLookup; ++i)
        lookup[i]=-1;
    lookup[1]=1;
    lookup[2]=1;
    lookup[3]=1;
    lookup[5]=1;
    for(int i=1; i<160; ++i) {
        int len=uglyNumMemo(lookup,i);
        printf("%d th is %d\n",i,len);
        printf("-----------------------------\n");
    }
    delete[] lookup;
}
