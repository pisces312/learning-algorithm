#include "sort.h"
#include "../test_utils.h"
#include<iostream>
#include<iomanip>
/*****************************************************************

Statistic

The count of comparision and swap times

******************************************************************/

void createHeap(int* a,int n,int h,int &compare,int &move) {
    int i=h,j=2*h+1;
    int temp=a[h];
    move+=2;
    while(j<n) {
        if(j<n-1&&a[j]<a[j+1]) {
            compare++;
            j++;
        }
        if(temp>=a[j]) {
            compare++;
            break;
        } else {
            move++;
            a[i]=a[j];
            i=j;
            j=2*j+1;
        }
    }
    a[i]=temp;
}
void heapSortShowingDetail(int * table, int n) {
    int compare=0,move=0,i;
    for(i=(n-1)/2; i>=0; i--)
        createHeap(table,n,i,compare,move);
    for(i=n-1; i>0; i--) {
        swap(table,0,i);
        move+=3;
        createHeap(table,i,0,compare,move);
    }
    std::cout<<"  堆排序的比较次数为:"<<std::setw(5)<<compare<<"    移动次数为:"<<std::setw(5)<<move<<std::endl;
    printArray(table,n);
}


void quickSortPass(int *a,const int low,const int high,int &compare,int &move) {
    int i=low,j=high;
    int temp;

    if(i>=j) {
        return;
    }
    temp=a[low];
    while(i<j) {
        while(i<j&&temp<a[j]) {
            j--;
            compare++;
        }
        if(i<j) {
            move++;
            a[i]=a[j];
            i++;
        }
        while(i<j&&a[i]<temp) {
            i++;
            compare++;
        }
        if(i<j) {
            move++;
            a[j]=a[i];
            j--;
        }
    }
    a[i]=temp;
    move++;
    quickSortPass(a,low,i-1,compare,move);
    quickSortPass(a,i+1,high,compare,move);
}
void quickSortShowingDetail(int * table, int n) {
    int compare=0,move=0;
    quickSortPass(table,0,n-1,compare,move);
    std::cout<<"快速排序的比较次数为:"<<std::setw(5)<<compare<<"    移动次数为:"<<std::setw(5)<<move<<"     "<<std::setw(5)<<compare+move<<std::endl;
//    display(table,n);
}
void bubbleSortShowingDetail(int * table, int n) {
    int compare=0,move=0,i,j;
    for(i=0; i<n-1; i++)
        for(j=0; j<n-1-i; j++) {
            compare++;
            if(table[j]>table[j+1]) {
                swap(table,j,j+1);
                move+=3;
            }
        }
    std::cout<<"冒泡排序的比较次数为:"<<std::setw(5)<<compare<<"    移动次数为:"<<std::setw(5)<<move<<"     "<<std::setw(5)<<compare+move<<std::endl;
//    display(table,n);
}

void selectSortShowingDetail(int * table, int n) {
    int compare=0,move=0,i,j;
    for(i=0; i<n-1; i++) {
        int minIdx=i;
        for(j=i; j<n; j++) {
            compare++;
            if(table[j]<table[minIdx])
                minIdx=j;
        }
        if(i!=minIdx) {
            swap(table,i,minIdx);
            move+=3;
        }
    }
    std::cout<<"选择排序的比较次数为:"<<std::setw(5)<<compare<<"    移动次数为:"<<std::setw(5)<<move<<"     "<<std::setw(5)<<compare+move<<std::endl;
//    display(table,n);
}

void shellSortShowingDetail(int* table, int n) {
    int compare=0,move=0,i;

    int j,m,k,span,numOfd=0,nt=n;
    int *d=new int[n/2];
    while(nt>1) {
        nt/=2;
        d[numOfd++]=nt;
    }
    int temp;
    for(m=0; m<numOfd; m++) {
        span=d[m];
        for(k=0; k<span; k++) {
            for(i=k; i<n-span; i+=span) {
                temp=table[i+span];
                j=i;
                while(j>-1&&temp<=table[j]) {
                    compare++;
                    move++;
                    table[j+span]=table[j];
                    j-=span;
                }
                table[j+span]=temp;
                move+=2;
            }
        }
    }
//    std::cout<<"希尔排序的比较次数为:"<<compare<<"    移动次数为:"<<std::setw(5)<<move<<"     "<<std::setw(5)<<compare+move<<std::endl;
    std::cout<<"希尔排序的比较次数为:"<<std::setw(5)<<compare<<"    移动次数为:"<<std::setw(5)<<move<<"     "<<std::setw(5)<<compare+move<<std::endl;
//    display(table,n);
}


void insertSortShowingDetail(int *table, int n) {
    int compare=0,move=0,i,j;

    int temp;
    for(i=0; i<n-1; i++) {
        temp=table[i+1];
        j=i;
        compare++;//在while中必有一次比较
        while(j>=0&&table[j]>temp) {
            compare++;
            move++;
            table[j+1]=table[j];
            j--;
        }
        table[j+1]=temp;
        move+=2;
    }
    std::cout<<"插入排序的比较次数为:"<<std::setw(5)<<compare<<"    移动次数为:"<<std::setw(5)<<move<<"     "<<std::setw(5)<<compare+move<<std::endl;
//    display(table,n);
}

void testShowingDetail(int n=100) {
    int* org=new int[n];
    int* keys=new int[n];
    createRandomUniqPostiveData(org,n);
    memcpy(keys,org,sizeof(int)*n);
    printArray(org,n);

    insertSortShowingDetail(keys,n);
    assertSorted(keys,n);
//    printArray(keys,n);
    memcpy(keys,org,sizeof(int)*n);

    shellSortShowingDetail(keys,n);
    assertSorted(keys,n);
//    printArray(keys,n);
    memcpy(keys,org,sizeof(int)*n);

    selectSortShowingDetail(keys,n);
    assertSorted(keys,n);
//    printArray(keys,n);
    memcpy(keys,org,sizeof(int)*n);

    quickSortShowingDetail(keys,n);
    assertSorted(keys,n);
//    printArray(keys,n);
    memcpy(keys,org,sizeof(int)*n);

    bubbleSortShowingDetail(keys,n);
    assertSorted(keys,n);
//    printArray(keys,n);
    memcpy(keys,org,sizeof(int)*n);


    delete[] keys;
    delete[] org;
}
