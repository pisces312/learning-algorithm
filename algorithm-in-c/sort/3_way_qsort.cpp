#include"../test_utils.h"
#include"../sort/sort.h"
/**

3-Way QuickSort (Dutch National Flag) 荷兰国旗问题

In simple QuickSort algorithm, we select an element as pivot, partition the array around pivot and recur for subarrays on left and right of pivot.
Consider an array which has many redundant elements. For example, {1, 4, 2, 4, 2, 4, 1, 2, 4, 1, 2, 2, 2, 2, 4, 1, 4, 4, 4}. If 4 is picked as pivot in Simple QuickSort, we fix only one 4 and recursively process remaining occurrences.
The idea of 3 way QuickSort is to process all occurrences of pivot and is based on Dutch National Flag algorithm.
In 3 Way QuickSort, an array arr[l..r] is divided in 3 parts:
a) arr[l..i] elements less than pivot.
b) arr[i+1..j-1] elements equal to pivot.
c) arr[j..r] elements greater than pivot.



将乱序的红白蓝三色小球排列成有序的红白蓝三色的同颜色在一起的小球组

数组排序问题，这个数组分为前部，中部和后部三个部分，每一个元素（红白蓝分别对应0、1、2）必属于其中之一。由于红、白、蓝三色小球数量并不一定相同，所以这个三个区域不一定是等分的。

思路如下：将前部和后部各排在数组的前边和后边，中部自然就排好了。
设置两个标志位begin和end分别指向这个数组的开始和末尾，然后用一个标志位current从头开始进行遍历：
1）若遍历到的位置为0，则说明它一定属于前部，于是就和begin位置进行交换，然后current向前进，begin也向前进（表示前边的已经都排好了）。
2）若遍历到的位置为1，则说明它一定属于中部，根据总思路，中部的我们都不动，然后current向前进。
3）若遍历到的位置为2，则说明它一定属于后部，于是就和end位置进行交换，由于交换完毕后current指向的可能是属于前部的，若此时current前进则会导致该位置不能被交换到前部，所以此时current不前进。而同1），end向后退1。


http://www.geeksforgeeks.org/3-way-quicksort-dutch-national-flag/
http://www.geeksforgeeks.org/sort-an-array-of-0s-1s-and-2s/
http://www.cnblogs.com/gnuhpc/archive/2012/12/21/2828166.html

**/

/**

Dutch National Flag

Sort the input array, the array is assumed to
have values in {0, 1, 2}
**/
void dutchNationalFlag(int a[], int arr_size) {
    int lo = 0;
    int hi = arr_size - 1;
    int mid = 0;

    while (mid <= hi) {
        switch (a[mid]) {//!just use swith case
        case 0:
            swapInt(&a[lo++], &a[mid++]);
            break;
        case 1:
            mid++;
            break;
        case 2:
            swapInt(&a[mid], &a[hi--]);
            break;
        }
    }
}


/**

After partition
<mid: [...,b-1]
=mid: [b,...,m-1(=e)]
>mid: [m(=e+1),...]

So two types of return values
1. p1=b, p2=e
The range of mid
2. p1=b-1, p2=m
The last element of <mid and the first element of >mid
It's convenient for quicksort since don't need -1 and +1
    quickSort3Way2(a, low, p1);
    quickSort3Way2(a, p2, high);

!This function covers the case that mid is an element of array,

**/
void partition3Way(int*x, int b, int e, const int midVal,int& p1,int&p2) {
    int m=b;
    while (m <= e) //e=m-1 exit
        if (x[m] < midVal) {
            swap(x,b,m);
            ++b;//[...,b-1]
            ++m;//[b,...,m-1(=e)]
        } else if (x[m] > midVal) {
            swap(x,m,e);
            --e;
        } else
            ++m;
    //Handle return values
    if(e<b) {
        //!when midVal is not an element of array
        //!e.g. all larger/smaller than mid val
        p1=-1;
        p2=-1;
    } else {//common case for quick sort
        p1=b;
        p2=e;
    }
}
inline void partition3Way(int*x, int b, int e,int& p1,int&p2) {
    partition3Way(x,b,e,x[b],p1,p2);
}

void quickSort3Way(int* x,const int l, const int u) {
    int p1=-1,p2=-1;
    partition3Way(x,l,u,p1,p2);
//    printf("%d,%d\n",p1,p2);
    if(p1<0) return;
    quickSort3Way(x,l, p1-1);
    quickSort3Way(x,p2+1, u);
}
inline void quickSort3Way(int* x,int n) {
    quickSort3Way(x,0,n-1);
}

/////////////////////////////////////////////////////////////////

/* This function partitions a[] in three parts
a) a[l..i] contains all elements smaller than pivot
b) a[i+1..j-1] contains all occurrences of pivot
c) a[j..r] contains all elements greater than pivot */

//It uses Dutch National Flag Algorithm
void partition3Way2(int a[], int low, int high, int &i, int &j) {
//! To handle 2 elements
    if (high - low <= 1) {
        if (a[high] < a[low])
            swapInt(&a[high], &a[low]);
        i = low;
        j = high;
        return;
    }

    int mid = low;
    int pivot = a[high];
    while (mid <= high) {
        if (a[mid]<pivot)
            swapInt(&a[low++], &a[mid++]);
        else if (a[mid]==pivot)
            mid++;
        else if (a[mid]>pivot)
            swapInt(&a[mid], &a[high--]);
    }

//update i and j
    i = low-1;
    j = mid; //or high-1
}

// 3-way partition based quick sort
void quickSort3Way2(int a[], int low, int high) {
    if (low>=high) //! 1 or 0 elements
        return;
    int i, j;
// Note that i and j are passed as reference
    partition3Way2(a, low, high, i, j);

// Recur two halves
    quickSort3Way2(a, low, i);
    quickSort3Way2(a, j, high);
}
void quickSort3Way2(int* x,int n) {
    quickSort3Way2(x,0,n-1);
}

////////////////////////////////////////////////////////////////

void testQuickSort3Way() {
    int* ptr=NULL;
    int n=0;

    int a[]= {99,5,36,2,19,1,46,12,7,22,25,28,17,92};
    ptr=a;
    n=sizeof(a)/sizeof(int);
    printArray(ptr,n);
    quickSort3Way(ptr,n);
    printArray(ptr,n);
    printf("\n");

    int b[]= {2,1,1,1,1,1,1};
    ptr=b;
    n=sizeof(b)/sizeof(int);
    printArray(ptr,n);
    quickSort3Way(ptr,n);
    printArray(ptr,n);
    printf("\n");

}


void test3WayParition() {

    testQuickSort3Way();

    int p1,p2;

    //All smaller than mid
    int x0[]= {0};
    partition3Way(x0,0,ARRAY_SIZE(x0)-1,2,p1,p2);
    printf("%d,%d\n",p1,p2);
    PRINT_INT_ARRAY(x0);
    assert(p1==-1&&p2==-1);


    //All smaller than mid
    int x1[]= {1,1,1,1,1};
    partition3Way(x1,0,ARRAY_SIZE(x1)-1,2,p1,p2);
    printf("%d,%d\n",p1,p2);
    PRINT_INT_ARRAY(x1);
    assert(p1==-1&&p2==-1);


    //All larger than mid
    int x2[]= {1,1,1,1,1};
    partition3Way(x2,0,ARRAY_SIZE(x2)-1,0,p1,p2);
    printf("%d,%d\n",p1,p2);
    PRINT_INT_ARRAY(x2);
    assert(p1==-1&&p2==-1);

    int x3[]= {1,-1,0,5,0,-1,0,2,0,7,-1,1,-2,1,0,0,-3};
    partition3Way(x3,0,ARRAY_SIZE(x3)-1,0,p1,p2);
    printf("%d,%d\n",p1,p2);
//    partition3Way(x3,0,ARRAY_SIZE(x3)-1,0);
    PRINT_INT_ARRAY(x3);
    assert(p1==5&&p2==10);

    int x4[]= {0,-1,0,1,0,-1,0,1,0,0,-1,1,1,1,0,0,-1};
    partition3Way(x4,0,ARRAY_SIZE(x4)-1,0,p1,p2);
    printf("%d,%d\n",p1,p2);
//    partition3Way(x4,0,ARRAY_SIZE(x4)-1,0);
    PRINT_INT_ARRAY(x4);
//    printf("%d\n",i);//-1
    assert(p1==4&&p2==11);
}
