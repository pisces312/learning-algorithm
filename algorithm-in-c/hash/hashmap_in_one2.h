#ifndef HASHMAP_IN_ONE2_H_INCLUDED
#define HASHMAP_IN_ONE2_H_INCLUDED


#include <stdlib.h>
#include <string.h>

#define MIN_SLOTS	16


struct hash_node {
    size_t hash;
    struct hash_node *next;
};

//User-defined hash func
typedef size_t (*hash_func_t)(void *key);
//User-defined cmp func
typedef int (*cmp_func_t)(struct hash_node *node, void *key);

struct hashmap {
    struct hash_node **table;
    size_t len, count;
    hash_func_t hash;
    cmp_func_t cmp;
};

void hashmap_init(struct hashmap *map, hash_func_t hash, cmp_func_t cmp) {
    map->len = MIN_SLOTS;
    map->table = (hash_node**)calloc(map->len, sizeof(struct hash_node *));
    map->count = 0;
    map->hash = hash;
    map->cmp = cmp;
}

static int hashmap_grow(struct hashmap *map) {
    size_t i;

    /* first, allocate more room for the table */
    struct hash_node **newtable = (hash_node **)realloc(map->table, map->len * 2 *
                                  sizeof(struct hash_node *));
    if (newtable == NULL)
        return -1;
    map->table = newtable;

    /* then, split all nodes from the lower half of the table
       to either lower or upper half of the table */
    for (i = 0; i < map->len; ++i) {
        struct hash_node *node = map->table[i], *next;
        struct hash_node *a = NULL, *b = NULL;
        while (node) {
            next = node->next;
            if (node->hash & map->len) {
                /* upper half */
                node->next = b;
                b = node;
            } else {
                /* lower half */
                node->next = a;
                a = node;
            }
            node = next;
        }
        map->table[i] = a;
        map->table[i + map->len] = b;
    }
    map->len *= 2;
    return 0;
}

static int hashmap_shrink(struct hashmap *map) {
    size_t i;

    /* first, fold the upper half of the table to top of the lower half */
    map->len /= 2;
    for (i = 0; i < map->len; ++i) {
        struct hash_node *prev = map->table[i];
        struct hash_node *next = map->table[i + map->len];
        if (prev == NULL)
            map->table[i] = next;
        else {
            while (prev->next)
                prev = prev->next;
            prev->next = next;
        }
    }
    /* then, release unneeded memory */
    struct hash_node **newtable = (hash_node**)realloc(map->table, map->len *
                                  sizeof(struct hash_node *));
    if (newtable == NULL)
        return -1;
    map->table = newtable;
    return 0;
}



void hashmap_free(struct hashmap *map) {
    free(map->table);
}

struct hash_node *hashmap_get(struct hashmap *map, void *key) {
    struct hash_node *node = map->table[map->hash(key) & (map->len - 1)];
    while (node) {
        if (map->cmp(node, key))
            return node;
        node = node->next;
    }
    return NULL;
}

int hashmap_insert(struct hashmap *map, struct hash_node *node, void *key) {
    size_t slot;
    node->hash = map->hash(key);
    slot = node->hash & (map->len - 1);
    node->next = map->table[slot];
    map->table[slot] = node;
    map->count++;

    if (map->count > map->len * 3)
        hashmap_grow(map);
    return 0;
}

struct hash_node *hashmap_remove(struct hashmap *map, void *key) {
    size_t slot = map->hash(key) & (map->len - 1);
    struct hash_node *node = map->table[slot], *prev = NULL;
    while (node) {
        if (map->cmp(node, key)) {
            if (prev != NULL)
                prev->next = node->next;
            else
                map->table[slot] = node->next;
            map->count--;

            if (map->count < map->len / 4 && map->len > MIN_SLOTS)
                hashmap_shrink(map);
            return node;
        }
        prev = node;
        node = node->next;
    }
    return NULL;
}


#endif // HASHMAP_IN_ONE2_H_INCLUDED
