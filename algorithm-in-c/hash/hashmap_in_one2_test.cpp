/**
Need to write hash function manually

**/
#include"hashmap_in_one2.h"

#include <stdio.h>
#include <assert.h>

#ifndef offsetof
#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#endif

#define container_of(ptr, type, member) \
	((type *) ((char *) (ptr) - offsetof(type, member)))

struct test {
    struct hash_node node;
    int i, j;
};

static size_t hash_test(void *key) {
    int *i = (int*)key;
    return *i;
}

static int cmp_test(struct hash_node *node, void *key) {
    struct test *t = container_of(node, struct test, node);
    int *i = (int*)key;
    return t->i == *i;
}

#define COUNT		1000000
#define GET_COUNT	10000000

void testHashmap2() {
    int i;
    struct test *t;
    struct hashmap map;

    hashmap_init(&map, hash_test, cmp_test);

    for (i = 0; i < COUNT; ++i) {
        t = (test*)calloc(1, sizeof *t);
        t->i = i;
        t->j = i + 123;

        hashmap_insert(&map, &t->node, &i);
    }

    for (i = 0; i < GET_COUNT; ++i) {
        int k = rand() % COUNT;
        struct test *t;
        struct hash_node *node = hashmap_get(&map, &k);
        if (node == NULL) {
            printf("%d not found\n", k);
            assert(0);
        }
        t = container_of(node, struct test, node);
        assert (t->i == k && t->j == k + 123);
    }

    for (i = 0; i < COUNT; ++i) {
        int k = COUNT - 1 - i;
        struct hash_node *node = hashmap_remove(&map, &k);
        if (node == NULL) {
            printf("%d not found\n", k);
            assert(0);
        }
        t = container_of(node, struct test, node);;
        assert (t->i == k && t->j == k + 123);
    }

    assert(map.len == MIN_SLOTS);

}
