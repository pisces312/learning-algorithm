#include"../test_utils.h"
/**

有字符串s1和s2，要求在s1中删除所有s2的字符，算法要快

例如：
str1为 "asdfsa123fasdf123452345fasfasdf182734891624389sadfaklsjfklj"
str2为 "0123456789"
删除后
str1为 "asdfsafasdffasfasdfsadfaklsjfklj"
**/

//str为s1，chars为s2
char* delChars(const char *str, const char *chars) {
    int charCount[256] = {0};//table to count, 0 by default
    while (*chars) {
        charCount[*chars]=1;
        ++chars;
    }

    int n=strlen(str)+1;
    char* buf=(char*)malloc(n*sizeof(char));
    char* pBuf=buf;
    while(*str) {
        if(!charCount[*str]) {
            *pBuf=*str;
            ++pBuf;
        }
        ++str;
    }
    *pBuf='\0';
    return buf;
}

void delCharsInPlace(char *str, const char *chars) {
    int charCount[256] = {0};//table to count, 0 by default
    while (*chars) {
        charCount[*chars]=1;
        ++chars;
    }

    char* src=str;
    char* dest=str;
    while(*src) {
        if(!charCount[*src]) {
            *dest=*src;
            ++dest;
        }
        ++src;
    }
    *dest='\0';
}

void testDelChars() {
    char str[]="asdfsa123fasdf123452345fasfasdf182734891624389sadfaklsjfklj";
    char chars[]="0123456789";

    char* result=delChars(str,chars);
    printf("%s\n",result);
    free(result);


    delCharsInPlace(str,chars);
    printf("%s\n",str);

}
