#include"../test_utils.h"
/*******************************************
KMP

*******************************************/

/**
failure function / partial match table

next array:
store the next position in search string to compare
not about the string to be searched
so it can be pre-calculated and fixed
the value depends on the length of prefix/suffix array.
It ranges from [0,n-1]

index stands for the length of considered string
so it starts with 1

**/
void buildPartialMatchTable(int* next, const char *p) {
    int n = strlen(p);
    //both next's index and value are lengths
    next[1] = next[0] = 0;//[0] means len=0
    //both len and k are lengths
    for(int len = 2,k=next[1]; len <= n; len++) { //Start from len=2
        //i-1: the last pos of i length elements
        //k  : next[i-1], the max prefix/suffix length of i-1 length elements
        //     p[k-1]==p[i-2]
        //     so if p[k]==p[i-1], next[i] is k+1
        while(k != 0 && p[k] != p[len-1])
            k = next[k];
        if(p[k] == p[len-1]) //if i=2 then k=0
            ++k;
        next[len]=k;
//way2
//        if (p[k] == p[i-1]) //
//            next[i]=++k;
//        else
//            next[i]=0;

    }
}

void showNextDetails(const char*p,int n,int* next) {
    printf("partial match table: \n");
    printf("     %s\n",p);
    printf("next ");
    for(int i=1; i<=n; ++i) {
        printf("%d",next[i]);
//        printf("len=%d\n",i);
//        printArray<const char>(p,i,printAsChar);
//        printf("next[%d]=%d\n",i,next[i]);
//        printArray<const char>(p,next[i],printAsChar);
    }
    printf("\n");
}

/**
!
p: pattern
**/
int kmp_match(const char *text, const char *p, int *next) {
    int m = strlen(p);
    int n = strlen(text);
    int pi = 0;// the length of matched string from last iteration
    int ti = 0;// the position of text to compare
    while(ti < n) { //one match iteration:
//pi starts from next[pi]
//ti always increases
        pi=next[pi];
        while(pi<m && p[pi] == text[ti])
            ++pi, ++ti;
        //handle match result of this iteration
        if(pi == 0)
            //if first char is not matched
            //move to next pos of text
            //make sure 's' will increase in each iteration
            //otherwise 's' will not change and lead endless loop
            ++ti;
        else if(pi == m)//all matched
            return ti-m;
    }
    return -1;
}

int kmp_match4(const char *text, const char *p, int *next) {
    int m = strlen(p);
    int n = strlen(text);

    //iterate text
    for(int pi=0,ti=0; ti<n; ++ti) {
        //find the index of pattern to compare
        while (pi>0 && p[pi]!=text[ti])
            pi = next[pi];
        //compare with text with one char in pattern
        if (text[ti]==p[pi])
            ++pi;
        //check result
        if (pi == m)
            return ti-pi+1; //pi is p's len here, so -1
    }
    return -1;
}


int kmp_match2(const char *text, const char *p, int *next) {
    int m = strlen(p);
    int n = strlen(text);
    int pi = 0;
    int ti = 0;
    while(ti < n) {
        pi=next[pi];
        if(pi == 0 && p[pi] != text[ti])
            ++ti;
        else {
            while(pi<m && p[pi] == text[ti])
                ++pi, ++ti;
            if(pi == m)
                return ti-m;
        }
    }
    return -1;
}

int kmp_match3(const char *text, const char *p, int *next) {
    int m = strlen(p);
    int n = strlen(text);
    int q = 0;
    int s = 0;
    while(s < n) {
        for(q = next[q]; q < m && p[q] == text[s]; q++, s++);
        if(q == 0)
            s++;
        else if(q == m)
            return s-m;
    }
    return -1;
}


/**
**/

void compute_prefix(const char* pattern, int* next) {
    int n=strlen(pattern);
    next[0] = -1;
    for (int i = 1,j=-1; i < n; i++) {
        while (j > -1 && pattern[j + 1] != pattern[i])
            j = next[j];

        if (pattern[i] == pattern[j + 1])
            j++;
        next[i] = j;
    }
}
//create next array internally
int kmp(const char* text, const char* pattern) {
    int n = strlen(text);
    int m = strlen(pattern);
    if (n == 0 && m == 0)
        return 0; /* "","" */
    if (m == 0)
        return 0;  /* "a","" */
    int* next = new int[m];

    compute_prefix(pattern, next);


    for (int i = 0,j=-1; i < n; i++) {
        while (j > -1 && pattern[j + 1] != text[i])
            j = next[j];

        if (text[i] == pattern[j + 1])
            j++;
        if (j == m - 1) {
            return i-j;
        }
    }

    delete[] next;

    return -1;
}

static void assertKMP(const char* p,const char* text, const int expect) {
    printf("Text: %s\n",text);
    int n=strlen(p);
//! n+1 size
    int* next=(int*)malloc((n+1)*sizeof(int));
    buildPartialMatchTable(next,p);
    showNextDetails(p,n,next);
    //
    int idx=kmp_match4(text, p,next);
    assert(idx==expect);
    idx=kmp(text, p);
    assert(idx==expect);
    idx=kmp_match(text, p, next);
    assert(idx==expect);
    idx=kmp_match2(text, p, next);
    assert(idx==expect);
    idx=kmp_match3(text, p, next);
    assert(idx==expect);
    //
    printf("pattern occurs with shift: %d\n", idx);
    printf("matched string: %s\n",text+idx);
    free(next);
    printf("------------------------------\n");
}

void testKMP() {
    assertKMP("cca","cacca",2);
    assertKMP("ABCDABD","ABC ABCDAB ABCDABCDABDE",15);
}
