#include "graph.h"
//#include "../sort/sort.h"
//#include "../tree/tree.h"
#include "../test_utils.h"
#include "sample_graphs.h"
//#include <queue>
//#include <list>

/************************************************************************

Test

************************************************************************/


void testAdjListGraph() {
    int v = 5;
    AdjListGraph* graph = createAdjListGraph(v);
    addEdge(graph, 0, 1);
    addEdge(graph, 0, 4);
    addEdge(graph, 1, 2);
    addEdge(graph, 1, 3);
    addEdge(graph, 1, 4);
    addEdge(graph, 2, 3);
    addEdge(graph, 3, 4);

    printAdjListGraph(graph);


    AdjListGraph* dirGraph = createAdjListGraph(v);
    addEdge(dirGraph, 0, 1,true);
    addEdge(dirGraph, 0, 4,true);
    addEdge(dirGraph, 1, 2,true);
    addEdge(dirGraph, 1, 3,true);
    addEdge(dirGraph, 1, 4,true);
    addEdge(dirGraph, 2, 3,true);
    addEdge(dirGraph, 3, 4,true);

    printAdjListGraph(dirGraph);
}

void testEdgeListGraph() {
    EdgeListGraphEx* g=unDirWeightedEdgeGraph1();
    printEdgeListGraphEx(g);
    //
    g=dirWeightedEdgeGraph1();
    printEdgeListGraphEx(g);
    //
    g=dirWeightedEdgeGraph2();
    printEdgeListGraphEx(g);
    //
    g=dirWeightedEdgeGraph3();
    printEdgeListGraphEx(g);


    AdjMatNetwork* adjMatG=directedNetwork1();
    printAdjMatGraph(adjMatG);
    g=adjMat2EdgeListGraph(adjMatG);
    printEdgeListGraphEx(g);
}




