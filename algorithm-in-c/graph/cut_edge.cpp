#include "graph.h"
#include "../common.h"


namespace Cutedge {
int index,root;
int n,m, *first,*next;
Edge* e;
int* flags,*low,*num;



void cutedgeDFS(int u,int parent) {
    int t;
    ++index;
    low[u]=index;//不考虑从parent走
    num[u]=index;

    t=first[u];
    while(t!=-1) {
        if(num[e[t].v]==0) {
            cutedgeDFS(e[t].v,u);
            low[u]=std::min(low[u],low[e[t].v]);

            if(low[e[t].v]==num[e[t].v])
//            if(low[e[t].v]>num[u])
                flags[t]=1;//edge flag
        } else if(e[t].v!=parent) //其他访问过的非父节点也要更新
            low[u]=std::min(low[u],num[e[t].v]);
        t=next[t];
    }
}
void testCutedge() {
    int i;
    n=6;

    low=(int*)calloc(n,sizeof(int));
    num=(int*)calloc(n,sizeof(int));
    first=(int*)calloc(n,sizeof(int));


    Edge e2[]= {
        {0,3,1},
        {0,2,1},
        {3,1,1},
        {2,1,1},
        {1,4,1},
        {1,5,1},
        //
        {3,0,1},
        {2,0,1},
        {1,3,1},
        {1,2,1},
        {4,1,1},
        {5,1,1}
    };




    m=sizeof(e2)/sizeof(Edge);
    next=(int*)calloc(m,sizeof(int));
    flags=(int*)calloc(m,sizeof(int));
    e=e2;

    simplegraph::buildLinkGraph(n,m,e,first,next);
    simplegraph::printLinkedGraph(n,first,next,e);


    index=0;
    root=0;
    cutedgeDFS(0,root);

    printf("low:");
    for(i=0; i<n; ++i)
        printf("%d ",low[i]);
    printf("\nnum:");
    for(i=0; i<n; ++i)
        printf("%d ",num[i]);
    printf("\n");
    //
    for(i=0; i<n; ++i)
        if(flags[i])
            printf("%d-%d\n",e[i].u,e[i].v);
}
}

