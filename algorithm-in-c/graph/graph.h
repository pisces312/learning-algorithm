#ifndef GRAPH_H_INCLUDED
#define GRAPH_H_INCLUDED



/****************************************************************

Adjacency Matrix

Adjacency Matrix is a 2D array of size V x V where V is the number of vertexes in a graph.
Let the 2D array be adj[][], a slot adj[i][j] = 1 indicates that there is an edge from vertex i to vertex j.

Adjacency matrix for undirected graph is always symmetric.

Adjacency Matrix is also used to represent weighted graphs (network).
If adj[i][j] = w, then there is an edge from vertex i to vertex j with weight w.


Pros: Representation is easier to implement and follow. Removing an edge takes O(1) time. Queries like whether there is an edge from vertex u to vertex v are efficient and can be done O(1).

Cons: Consumes more space O(V^2). Even if the graph is sparse(contains less number of edges), it consumes the same space. Adding a vertex is O(V^2) time.

****************************************************************/
struct AdjMatGraph {
    //a matrix, map, 2D array
    //  edge: for undirected graph
    //  arc: for directed graph
    //Value:
    //  Network: (indicate weight)
    //      INT_MAX is for not connected
    //      0 is for itself
    //  Unweighted graph: 0 for not connected and to itself; 1 for connected
    int** arc;
    int n;//vertex number
//    int* vertex;//assume the vertexes are from [0,n-1], no need to store
};

AdjMatGraph* emptyUnweightedGraph(const int verNum);
AdjMatGraph* createUnweightedGraph(int vertNum, int* arcs,int arrSize,int directed=0);

void printAdjMatGraph(AdjMatGraph* graph);

struct AdjMatNetwork : AdjMatGraph {

};

AdjMatNetwork* emptyNetwork(const int verNum);
//Each arc is triple <i,j,w>
AdjMatNetwork* createNetwork(int vertNum, int* arcsValue,int arrSize,int directed=0);






/****************************************************************

Adjacency List

An array of linked lists is used. Size of the array is equal to number of vertexes. Let the array be array[]. An entry array[i] represents the linked list of vertexes adjacent to the i-th vertex. This representation can also be used to represent a weighted graph. The weights of edges can be stored in nodes of linked lists. Following is adjacency list representation of the above graph.

****************************************************************/

// A structure to represent an adjacency list node
struct AdjListNode {
    int dest;
    struct AdjListNode* next;
    int w; //the weight for network
};

// A structure to represent an adjacency list
struct AdjList {
    //!head node,
    //pointer to head node of list
    //for easy remove an edge
    struct AdjListNode *head;
};

// A structure to represent a graph. A graph is an array of adjacency lists.
struct AdjListGraph {
    int n; // The number of vertexes in graph
    struct AdjList* array;
};

// A utility function to create a new adjacency list node
struct AdjListNode* newAdjListNode(int dest);

// A utility function that creates a graph of n vertexes
AdjListGraph* createAdjListGraph(int n);

// Adds an edge to an undirected graph
void addEdge(AdjListGraph* graph, int src, int dest, bool directed=false);

void addEdge(AdjListGraph* graph, int src, int dest, int weight, bool directed=false);

// A utility function to print the adjacency list representation of graph
void printAdjListGraph(AdjListGraph* graph);





/////////////////////////////////////////////////////////////////////////
// Utils

//if weight=INT_MAX, print "-"
int printNetworkIntVal(void* data);




/////////////////////////////////////////////////////////////////////////


struct Edge {
    int u;
    int v;
    int w;//for weighted graph, it cannot be INT_MAX, because it's an edge
};

void swapEdge(struct Edge* x,struct Edge* y);

// Compare two edges according to their weights.
// Used in qsort() for sorting an array of edges
int edgeComp(const void* a, const void* b);

/**
Support both directed/undirected graph
Easy to return edges
**/
struct EdgeListGraph {
    int nV;
    int nE;
    // for undirected graph, it needs to create two edges
    struct Edge* edges;
};

EdgeListGraph* createEdgeListGraph(const int n,const int m, struct Edge* e,bool copy=true);

//Extend directly from EdgeListGraph for common fields
//because fields don't contain point of the struct itself
struct EdgeListGraphEx :EdgeListGraph {
    //! first and next are used to find the neighbor edges
    // the header array, store the index of edge, "nV" elements
    int* first;
    // store the index of edge, "nE" elements
    int* next;
};

EdgeListGraphEx* createEdgeListGraphEx(const int m, struct Edge* e,bool copy=true);
EdgeListGraphEx* createEdgeListGraphEx(const int n,const int m, struct Edge* e,bool copy=true);

//Convert EdgeListGraph to EdgeListGraphEx
void setFirstAndNext(EdgeListGraphEx* g);

void printEdgeListGraphEx(EdgeListGraphEx* g);


AdjMatNetwork* edgeList2AdjMatGraph(EdgeListGraph* g);
EdgeListGraphEx* adjMat2EdgeListGraph(AdjMatNetwork* g);

/****************************************************************

Travel (DFS & BFS)

****************************************************************/


void dfsOnlyConnected(AdjMatGraph* g,int cur);
void dfs(AdjMatGraph* g,int cur);

void bfsOnlyConnected(AdjMatGraph* g,int cur);
void bfsOnlyConnected2(AdjMatGraph* g,int cur);
void bfs(AdjMatGraph* g,int cur);


/****************************************************************

Connectivity

****************************************************************/

bool isConnectedGraph(AdjMatGraph* g);
bool isDirectedNetworkConnected(AdjMatGraph* g,int startNodeIdx);



/****************************************************************

MST

****************************************************************/


int* mstByPrim(AdjMatGraph* g,int cur);
int* mstByPrim2(AdjMatGraph* g,int cur);
unsigned long mstByPrimCPP(AdjMatGraph* g);

int mstByKruskal(EdgeListGraph* g);

/****************************************************************

Shortest path

****************************************************************/

void shortestPathByDFSSimple();

int* shortestPathByDijkstra(AdjMatGraph* g,int v0);
void shortestPathByDijkstraSimple();
void showSPByDijkstra(AdjMatGraph* g,int *path,int v0,int v);

int** shortestPathByFloyd(AdjMatGraph* g);
void shortestPathByFloydSimple();
void showSPByFloyd(AdjMatGraph* g,int** path,int u,int v);

//void shortestPathByDijkstraLinkedGraph();
//void shortestPathByBellmanFoard();
//void shortestPathByBellmanFoardUsingQueue();
//void shortestPathByBellmanFoardCheckingNegativeCircle() ;
//



/////////////////////////////////////////////////////////////////////////

namespace simplegraph {
void buildLinkGraph(const int n,const int m, struct Edge* e,int* first, int *next);
void printLinkedGraph(int n,int*first,int*next,struct Edge* e);
}




#endif // GRAPH_H_INCLUDED
