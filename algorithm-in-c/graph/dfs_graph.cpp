#include"graph.h"
#include"../test_utils.h"
#include"sample_graphs.h"


/****************************************************************

DFS

Graph can contain cycle. Because we have visited array to know if
a node has been visited twice.

For unweighted graph:
    If has directed/undirected edge between i and j,
    arc[i][j]=1, otherwise is 0

For weighted graph:
    not connected: arc[i][j]=INT_MAX

Time Complexity:
O(V+E) where V is number of vertices in the graph and E is number of edges in the graph.

****************************************************************/

/**
! DFS for unweighted graph
**/
static void _dfs(AdjMatGraph* g,int cur,bool* visited) {
    printf("%d ",cur);
    visited[cur]=true;//!set flag at the beginning
    for(int i=0; i<g->n; ++i)
        //!different logic for different kinds of graph
        if(g->arc[cur][i]
                && !visited[i]) //have an edge and not visited
            _dfs(g,i,visited);
}

/**
For connected, unweighted, acyclic/cyclic, directed/undirected graph
**/
void dfsOnlyConnected(AdjMatGraph* g,int cur) {
    bool* visited=(bool*)calloc(g->n,sizeof(bool));
    _dfs(g,cur,visited);
    free(visited);
}
/**
For unweighted, acyclic/cyclic, directed/undirected, connected/unconnected graph
This also can be used to traverse a forrest
**/
void dfs(AdjMatGraph* g,int cur) {
    bool* visited=(bool*)calloc(g->n,sizeof(bool));
    _dfs(g,cur,visited);
    //!!Traverse unconnected parts.
    for(int i=0; i<g->n; ++i)
        if(!visited[i])
            _dfs(g,i,visited);
    free(visited);
}




/**

DFS - iteration version - adjacent matrix graph

For weighted graph

**/
void dfsIter(AdjMatNetwork* g,int v0) {
    const int n=g->n;
    int** e=g->arc;
    int* st=(int*)malloc(n*sizeof(int));
    int top=0;
    bool* visited=(bool*)calloc(n,sizeof(bool));

    st[top++]=v0;
    visited[v0]=true;
    printf("%d ",v0);

    while(top>0) {
        int cur=st[top-1];
        bool flag=false;
        for(int i=0; i<n; ++i)
            if(!visited[i]&&e[cur][i]!=INT_MAX) {
                printf("%d ",i);
                st[top++]=i;
                visited[i]=true;
                flag=true;
                break;
            }
        if(!flag)
            --top;
    }
    printf("\n");

    free(st);
    free(visited);
}


/**

DFS - iteration version - adjacent list graph

For weighted graph

http://blog.sina.com.cn/s/blog_4e66800901000aw1.html

**/
void dfsIter(AdjListGraph* g,int v0) {
    const int n=g->n;
    int* st=(int*)malloc(n*sizeof(int));
    int top=0;
    bool* visited=(bool*)calloc(n,sizeof(bool));

    st[top++]=v0;
    visited[v0]=true;
    printf("%d ",v0);

    AdjListNode* t=NULL;
    while(top>0) {
        int cur=st[top-1];
        for(t = g->array[cur].head; t; t=t->next) {
            int v=t->dest;
            if(!visited[v]) {
                printf("%d ",v);
                st[top++]=v;
                visited[v]=true;
                break;
            }
        }
        if(t==NULL)
            --top;
    }
    printf("\n");

    free(st);
    free(visited);
}



bool isConnectedGraph(AdjMatGraph* g) {
    bool* visited=(bool*)calloc(g->n,sizeof(bool));
    _dfs(g,0,visited);
    bool flag=true;
    for(int i=0; i<g->n; ++i)
        if(!visited[i]) {//if has not visit, break
            flag=false;
            break;
        }
    free(visited);
    return flag;
}

////////////////////////////////////////

static void _dfsNetwork(AdjMatGraph* g,int cur,bool* visited) {
    visited[cur]=true;//!set flag at the beginning
    for(int i=0; i<g->n; ++i)
        if(g->arc[cur][i]<INT_MAX //must smaller than max int
                && !visited[i]) //have an edge and not visited
            _dfsNetwork(g,i,visited);
}
/**
The connectivity of directed graph relates to start node
Different start node has different connectivity
**/
bool isDirectedNetworkConnected(AdjMatGraph* g,int startNodeIdx) {
    bool* visited=(bool*)calloc(g->n,sizeof(bool));
    _dfsNetwork(g,startNodeIdx,visited);
    bool flag=true;
    for(int i=0; i<g->n; ++i)
        if(!visited[i]) {//if has not visit, break
            flag=false;
            break;
        }
    free(visited);
    return flag;
}





//test the traverse which starts from every nodes
#define printDFSGraph(G,F) { \
    printf("\n<%s>\n", (#F)); \
    for(int i=0; i<g->n; ++i) { \
        F((G),i); \
        printf("\n--------------------------\n"); \
    } }

static void _testDFSGraph(AdjMatGraph* g) {
    printDFSGraph(g,dfsOnlyConnected);
    printDFSGraph(g,dfs);
}

void testDFSGraph() {
    AdjMatNetwork* network=directedNetwork2();
    dfsIter(network,0);

    network=unDirAdjMatNetwork1();
    dfsIter(network,0);

    network=unDirAdjMatNetwork2();
    dfsIter(network,0);


    AdjListGraph* alg=unDirAdjListGraph1();
    dfsIter(alg,0);

    alg=unDirAdjListGraph2();
    dfsIter(alg,0);
//    _testDFSGraph(testGraph1());
//    _testDFSGraph(connUweightedGraphForTest());
//
//    AdjMatGraph* unconnGraph=unconnGraphForTest();
//    bool flag=isConnectedGraph(unconnGraph);
//    printf("\nConnected:%d\n",flag);
}
