#include"graph.h"
#include"../test_utils.h"
#include"sample_graphs.h"
#include<queue>
/******************************************************************

Breadth First Traversal or BFS

Time Complexity:
O(V+E) where V is number of vertices in the graph
and E is number of edges in the graph.


http://www.geeksforgeeks.org/breadth-first-traversal-for-a-graph/

******************************************************************/

/**

Traverses only the vertices reachable from a given source vertex. All the vertices may not be reachable from a given vertex (example Disconnected graph). To print all the vertices, we can modify the BFS function to do traversal starting from all nodes one by one

**/
void bfsOnlyConnected(AdjMatGraph* g,int cur) {
    //init to 0
    bool* visited=(bool*)calloc(g->n,sizeof(bool));
    std::queue<int> q;
    //put root as first element
    q.push(cur);
    visited[cur]=true;
    while(!q.empty()) {
        cur=q.front();
        q.pop();
        printf("%d ",cur);
        for(int i=0; i<g->n; ++i)
            if(g->arc[cur][i]&&!visited[i]) {
                visited[i]=true;
                q.push(i);
            }
    }
    free(visited);
}
//!Using array as queue
void bfsOnlyConnected2(AdjMatGraph* g,int cur) {
    //Set flag to 0
    bool* visited=(bool*)calloc(g->n,sizeof(bool));
    int* q=(int*)malloc(g->n*sizeof(int));
    int front=0;
    int last=1;
    q[front]=cur;

    visited[cur]=true;
    int i=0;
    while(front<last) {
        cur=q[front++];
        printf("%d ",cur);
        for(i=0; i<g->n; ++i)
            if(g->arc[cur][i]&&!visited[i]) {
                visited[i]=true; //must set before push to queue
                q[last++]=i;
            }
    }
    free(visited);
}

void bfs(AdjMatGraph* g,int cur) {
    bool* visited=(bool*)calloc(g->n,sizeof(bool));
    std::queue<int> q;
    q.push(cur);
    visited[cur]=true;
    int i=0;
    while(true) {
        while(!q.empty()) {
            cur=q.front();
            q.pop();
            printf("%d ",cur);
            for(i=0; i<g->n; ++i)
                if(g->arc[cur][i]&&!visited[i]) {
                    visited[i]=true;
                    q.push(i);
                }
        }
        //For unconnected parts
        //Every time, push one element
        for(i=0; i<g->n; ++i)
            if(!visited[i]) {
                visited[i]=true;
                q.push(i);
                break;
            }
        if(i==g->n)//All visited
            break;
    }
}



//test the traverse which starts from every nodes
#define printBFSGraph(G,F) { \
    printf("\n<%s>\n", (#F)); \
    for(int i=0; i<g->n; ++i) { \
        F((G),i); \
        printf("\n--------------------------\n"); \
    } }

static void _testGraphTra(AdjMatGraph* g) {
    printBFSGraph(g,bfsOnlyConnected);
    printBFSGraph(g,bfs);
    printBFSGraph(g,bfsOnlyConnected2);
}

void testBFSGraph() {
    _testGraphTra(testGraph1());
    _testGraphTra(connUweightedGraphForTest());
}
