#include"../test_utils.h"
#include"graph.h"
#include "../tree/tree.h"
#include"../graph/sample_graphs.h"
/************************************************************

MST using Kruskal's algorithm (Greedy Algorithms)

1. Sort all the edges in non-decreasing order of their weight.
2. Pick the smallest edge. Check if it forms a cycle with the spanning tree
formed so far. If cycle is not formed, include this edge. Else, discard it.
3. Repeat step#2 until there are (V-1) edges in the spanning tree.

Time Complexity:
O(ElogE) or O(ElogV). Sorting of edges takes O(ELogE) time. After sorting, we iterate through all edges and apply find-union algorithm. The find and union operations can take atmost O(LogV) time. So overall complexity is O(ELogE + ELogV) time. The value of E can be atmost O(V2), so O(LogV) are O(LogE) same. Therefore, overall time complexity is O(ElogE) or O(ElogV)


Support:
Both directed and undirected

http://www.geeksforgeeks.org/greedy-algorithms-set-2-kruskals-minimum-spanning-tree-mst/


************************************************************/



static void quickSortForEdge(struct Edge* x,int l,int u) {
    if(l >= u)
        return;
    int pivot = x[l].w;
    int i = l;
    int j = u+1;
    while(true) {
        do
            i++;
        while(i <= u && x[i].w < pivot);
        do
            j--;
        //don't need check j>=l, because x[l]=pivot
        while(x[j].w > pivot);
        if(i >= j)
            break;
        swapEdge(&x[i], &x[j]);
    }
    if(l!=j)//can not use i here, because i may equal to u
        swapEdge(&x[l], &x[j]);
    quickSortForEdge(x,l, j-1);
    quickSortForEdge(x,j+1, u);
}


int mstByKruskal(EdgeListGraph* g) {
    int n=g->nV,m=g->nE;
    Edge* e=g->edges;
    int sum=0;

    // Step 1:  Sort all the edges in non-decreasing order of their weight
    // If we are not allowed to change the given graph, we can create a copy of
    // array of edges
    // option 1: with qsort
    qsort(e, m, sizeof(Edge), edgeComp);
    // option 2
//    quickSortForEdge(e,0,m-1);

    //Step 2. Core logic
    int* f=UnionFind::makeSet(n);
    //iterate edges and only pick n-1 edges
    for(int i=0,c=0; i<m&&c<n-1; ++i) {
        int s1=UnionFind::findSet(f,e[i].u);
        int s2=UnionFind::findSet(f,e[i].v);
        if(s1!=s2) { //! detect circle
            UnionFind::unionSet(f,e[i].u,e[i].v);
            ++c;
            sum+=e[i].w;
            printf("add edge <%d,%d>\n",e[i].u,e[i].v);
        } else {
            printf("Circle is detected if edge<%d,%d> is added.\n",e[i].u,e[i].v);
        }
    }

    free(f);
    printf("sum=%d\n",sum);
    return sum;
}


void testKruskal() {
    EdgeListGraphEx* g=dirWeightedEdgeGraph1();
    printEdgeListGraphEx(g);
    int sum=mstByKruskal((EdgeListGraph*)g);
    assert(sum==19);

    g=dirWeightedEdgeGraph2();
    printEdgeListGraphEx(g);
    sum=mstByKruskal((EdgeListGraph*)g);
    assert(sum==19);
}

