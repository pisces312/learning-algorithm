#ifndef SAMPLE_GRAPHS_H_INCLUDED
#define SAMPLE_GRAPHS_H_INCLUDED
#include"graph.h"

AdjMatGraph* testGraph1();

AdjMatGraph* connUweightedGraphForTest();

AdjMatGraph* unconnGraphForTest();

AdjMatGraph* cirUndirGraphForTest();

AdjMatGraph* acyclicUndirGraphForTest();


AdjMatNetwork* testNetwork1();

AdjMatNetwork* directedNetwork1();
AdjMatNetwork* directedNetwork2();

AdjMatNetwork* unDirAdjMatNetwork1();
AdjMatNetwork* unDirAdjMatNetwork2();

AdjMatNetwork* adjMatDAGNegWeight();

AdjListGraph* adjListDAG1();
AdjListGraph* unDirAdjListGraph1();
AdjListGraph* unDirAdjListGraph2();

EdgeListGraphEx* unDirWeightedEdgeGraph1();

EdgeListGraphEx* dirWeightedEdgeGraph1();
EdgeListGraphEx* dirWeightedEdgeGraph2();
EdgeListGraphEx* dirWeightedEdgeGraph3();
EdgeListGraphEx* dirWeightedEdgeGraph4();
EdgeListGraphEx* dirWeightedEdgeGraphWithCircle();
#endif // SAMPLE_GRAPHS_H_INCLUDED
