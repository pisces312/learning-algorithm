#include"graph.h"
#include"../test_utils.h"
#include"sample_graphs.h"
#include<stack>

/**********************************************************


Topological Sorting


Overview:
For Directed Acyclic Graph (DAG)
Not unique, can have multiple results



DAG:
A DAG has at least one vertex with in-degree 0 and one vertex with out-degree 0.
Proof: There��s a simple proof to the above fact is that a DAG does not contain a cycle which means that all paths will be of finite length. Now let S be the longest path from u(source) to v(destination). Since S is the longest path there can be no incoming edge to u and no outgoing edge from v, if this situation had occurred then S would not have been the longest path
=> indegree(u) = 0 and outdegree(v) = 0



http://www.geeksforgeeks.org/topological-sorting/

**********************************************************/


/**

Topology sort by DFS using adjacent list graph

Algorithm:
We use a temporary stack. We don��t print the vertex immediately, we first recursively call topological sorting for all its adjacent vertices, then push it to a stack.
Finally, print contents of stack.
Note that a vertex is pushed to stack only when all of its adjacent vertices (and their adjacent vertices and so on) are already in stack.


Get topology sort from back to forth


**/
static void tpSortDFS(AdjListGraph* g, int u,bool visited[], int* result, int& tail) {
    // Mark the current node as visited.
    visited[u] = true;
    //! the order must be to iterate all adjacent vertexes
    for(AdjListNode* p = g->array[u].head; p; p=p->next) {
        int v=p->dest;
        if (!visited[v])
            tpSortDFS(g,v,visited,result,tail);
    }
    // Store the result from back to forth
    result[--tail]=u;
}
//!
int* topologySortByDFS(AdjListGraph* g) {
    const int n=g->n;
    int* result=(int*)malloc(n*sizeof(int));
    int tail=n;
    bool* visited=(bool*)calloc(n,sizeof(bool));

    for(int i=0; i<n; ++i)
        if(!visited[i])
            tpSortDFS(g,i,visited,result,tail);

    free(visited);
    return result;
}

int* topologySortByDFS(AdjListGraph* g,int cur) {
    const int n=g->n;
    int* result=(int*)malloc(n*sizeof(int));
    int tail=n;
    bool* visited=(bool*)calloc(n,sizeof(bool));
    //! special for start point, cannot set visited true directly
    // it needs to traverse all adjacent vertexes first
    tpSortDFS(g,cur,visited,result,tail);
    //! core logic
    for(int i=0; i<n; ++i)
        if(!visited[i])
            tpSortDFS(g,i,visited,result,tail);

    free(visited);
    return result;
}


// Use std::stack lib
static void tpSortDFS2(AdjListGraph* g, int u,bool visited[], std::stack<int>& st) {
    // Mark the current node as visited.
    visited[u] = true;
    // Recur for all the vertices adjacent to this vertex
    AdjListNode* p = g->array[u].head;
    while(p) {
        int v=p->dest;
        if (!visited[v])
            tpSortDFS2(g,v,visited,st);
        p = p->next;
    }
    // Push current vertex to stack which stores result
    st.push(u);

}


int* topologySortByDFS2(AdjListGraph* g,int cur) {
    const int n=g->n;
    std::stack<int> st;
    bool* visited=(bool*)calloc(n,sizeof(bool));
    //! special for start point
    tpSortDFS2(g,cur,visited,st);
    //! core logic
    for(int i=0; i<n; ++i)
        if(!visited[i])
            tpSortDFS2(g,i,visited,st);

    free(visited);

    //Result
    int* sorted=(int*)malloc(n*sizeof(int));
    int i=0;
    while(!st.empty()) {
        sorted[i]=st.top();
        st.pop();
        ++i;
    }
    return sorted;
}

/**

Topology by Kahn (In degree)

Step-1: Compute in-degree (number of incoming edges) for each of the vertex present in the DAG and initialize the count of visited nodes as 0.

Step-2: Pick all the vertices with in-degree as 0 and add them into a queue (Enqueue operation)

Step-3: Remove a vertex from the queue (Dequeue operation) and then.

    Increment count of visited nodes by 1.
    Decrease in-degree by 1 for all its neighboring nodes.
    If in-degree of a neighboring nodes is reduced to zero, then add it to the queue.

Step 5: Repeat Step 3 until the queue is empty.

Step 5: If count of visited nodes is not equal to the number of nodes in the graph then the topological sort is not possible for the given graph.


http://www.geeksforgeeks.org/topological-sorting-indegree-based-solution/

**/

//Use visited array
int* topologySortByIndegree(AdjListGraph* g) {
    const int n=g->n;
    int* sorted=(int*)malloc(n*sizeof(int));
    int c=0;
    bool* visited=(bool*)calloc(n,sizeof(bool));
    int* inDegree=(int*)calloc(n,sizeof(int));
    //Calculate in-degree
    for(int i=0; i<n; ++i)
        for(AdjListNode* p = g->array[i].head; p; p=p->next)
            ++inDegree[p->dest];
    //Remove vertex and update in-degree
    while(c<n)
        for(int i=0; i<n; ++i)
            if(!visited[i]&&inDegree[i]==0) {
                visited[i]=true;
                sorted[c++]=i;
                for(AdjListNode* p = g->array[i].head; p; p=p->next)
                    --inDegree[p->dest];
            }

    free(inDegree);
    free(visited);
    return sorted;
}

//!Use queue
int* topologySortByIndegree2(AdjListGraph* g) {
    const int n=g->n;
    int* inDegree=(int*)calloc(n,sizeof(int));//initial values: 0
    int* queue=(int*)malloc(n*sizeof(int));//store vertex with 0 in-degree
    int qHead=0, qTail=-1;
    //Calculate in-degree
    for(int i=0; i<n; ++i)
        for(AdjListNode* p = g->array[i].head; p; p=p->next)
            ++inDegree[p->dest];
    for(int i=0; i<n; ++i)
        if(inDegree[i]==0)
            queue[++qTail]=i;
    //Remove vertex and update in-degree
    while(qHead<=qTail) {
        int u=queue[qHead++];
        for(AdjListNode* p = g->array[u].head; p; p=p->next)
            if(--inDegree[p->dest]==0)
                queue[++qTail]=p->dest;
    }

    free(inDegree);
    return queue;
}

/**

Find all topology sort by back-tracing


1. Initialize all vertices as unvisited.
2. Now choose vertex which is unvisited and has zero indegree and decrease indegree of all those vertices by 1 (corresponding to removing edges) now add this vertex to result and call the recursive function again and backtrack.
3. After returning from function reset values of visited, result and indegree for enumeration of other possibilities.



http://www.geeksforgeeks.org/all-topological-sorts-of-a-directed-acyclic-graph/


**/
static void tpSortAllByIndegree(AdjListGraph* g,int* inDegree,bool* visited,int result[],int tail) {
    const int n=g->n;
    if(tail==n) {
        printArray(result,n);
        return;
    }
    for(int i=0; i<n; ++i)
        if(!visited[i]&&inDegree[i]==0) {
            visited[i]=true;
            for(AdjListNode* p = g->array[i].head; p; p=p->next)
                --inDegree[p->dest];

            result[tail]=i;
            tpSortAllByIndegree(g,inDegree,visited,result,tail+1);

            visited[i]=false;
            for(AdjListNode* p = g->array[i].head; p; p=p->next)
                ++inDegree[p->dest];
        }
}
void topologySortAllByIndegree(AdjListGraph* g) {
    const int n=g->n;
    bool* visited=(bool*)calloc(n,sizeof(bool));
    int* inDegree=(int*)calloc(n,sizeof(int));
    //Calculate in-degree
    for(int i=0; i<n; ++i)
        for(AdjListNode* p = g->array[i].head; p; p=p->next)
            ++inDegree[p->dest];

    int result[n];
    tpSortAllByIndegree(g,inDegree,visited,result,0);

    free(inDegree);
    free(visited);
}

void testTopologySort() {
    int* sorted=NULL;
    AdjListGraph* g=adjListDAG1();
    printAdjListGraph(g);

    sorted=topologySortByDFS(g);
    printArray(sorted,g->n);

    for(int i=0; i<g->n; ++i) {
        printf("TP sort stated from %d\n",i);
        sorted=topologySortByDFS(g,i);
        printArray(sorted,g->n);
    }

    sorted=topologySortByIndegree(g);
    printArray(sorted,g->n);

    sorted=topologySortByIndegree2(g);
    printArray(sorted,g->n);

    printf("\ntopologySortAllByIndegree\n");
    topologySortAllByIndegree(g);
}
