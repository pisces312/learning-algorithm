#include "graph.h"
#include "../sort/sort.h"
#include "../tree/tree.h"
#include "../common.h"


namespace Cutpoint {
//void dfs(int cur, int** e,int* book,int n,int* sum) {
//    int i;
//    printf("%d ",cur);
//    if(++(*sum)==n) return;
//    for(i=0; i<n; ++i)
//        if(e[cur][i]==1&&book[i]==0) {
//            book[i]=1;
//            dfs(i,e,book,n,sum);
//        }
//}

void dfs(int cur, Edge* e,int* book,int n,int* sum,int* first,int*next) {
    int t;
    book[cur]=1;
    if(++(*sum)==n) return;
    t=first[cur];
    while(t!=-1) {
        if(book[e[t].v]==0)
            dfs(e[t].v,e,book,n,sum,first,next);
        t=next[t];
    }
}
int isCutpoint(int cur, int n, Edge*e, int*first, int*next) {
    int* book=(int*)calloc(n,sizeof(int));
    int sum=1;
    int start,i;

    book[cur]=1;
    for(i=0; i<n; ++i)
        if(cur!=i) {
            start=i;
            break;
        }

    dfs(start,e,book,n,&sum,first,next);

    delete book;
    return (sum!=n);

}
void testCutpoint() {
    int n=6,m=7*2,flag,i;

    int* first=(int*)calloc(n,sizeof(int));
    int* next=(int*)calloc(m,sizeof(int));

    Edge e[]= {
        {0,3,1},
        {0,2,1},
        {3,1,1},
        {2,1,1},
        {1,4,1},
        {1,5,1},
        {4,5,1},
        //
        {3,0,1},
        {2,0,1},
        {1,3,1},
        {1,2,1},
        {4,1,1},
        {5,1,1},
        {5,4,1}
    };
    simplegraph::buildLinkGraph(n,m,e,first,next);
    simplegraph::printLinkedGraph(n,first,next,e);
    for(i=0; i<n; ++i) {
        flag=isCutpoint(i,n,e,first,next);
        printf("%d: %d\n",i,flag);
    }
}
}

namespace Cutpoint2 {
int index,root;
int n,m, *first,*next;
Edge* e;
int* flags,*low,*num;

int min(int a,int b) {
    return a<b?a:b;
}

//遍历n-1个点
void cutpointDFS(int u,int parent) {
    int t;
    int i,old;
    int child=0;//记录树的儿子，即图中未访问的节点
    ++index;
    low[u]=index;//不考虑从parent走
    num[u]=index;


    printf("visit %d, index %d, n %d\n",u,index,n);

    t=first[u];
    while(t!=-1) {
        if(num[e[t].v]==0) {

            cutpointDFS(e[t].v,u);
            old=low[u];

            low[u]=min(low[u],low[e[t].v]);
            //
            //
            printf("low[%d]=min(low[%d](%d),low[%d](%d)=%d\n",u,u,old,e[t].v,low[e[t].v],low[u]);
            printf("low:");
            for(i=0; i<n; ++i)
                printf("%d ",low[i]);
            printf("\nnum:");
            for(i=0; i<n; ++i)
                printf("%d ",num[i]);
            printf("\n");
            //
            //
            //

            ++child;
            if(u!=root&&low[e[t].v]>=num[u]) {
                printf("low[%d](%d)>=num[%d](%d)\n",e[t].v,low[e[t].v],u,num[u]);

                flags[u]=1;
                printf("found %d\n",u);
            } else if(u==root&&child==2)
                flags[u]=1;








        } else if(e[t].v!=parent) { //其他访问过的非父节点也要更新
            old=low[u];
            low[u]=min(low[u],num[e[t].v]);
            //
            //
            printf("low[%d]=min(low[%d](%d),num[%d](%d)=%d\n",u,u,old,e[t].v,num[e[t].v],low[u]);
            printf("low:");
            for(i=0; i<n; ++i)
                printf("%d ",low[i]);
            printf("\nnum:");
            for(i=0; i<n; ++i)
                printf("%d ",num[i]);
            printf("\n");
            //
            //
        }
        t=next[t];
    }
}
void testCutpoint() {
    int i;
    n=6;
    flags=(int*)calloc(n,sizeof(int));
    low=(int*)calloc(n,sizeof(int));
    num=(int*)calloc(n,sizeof(int));
    first=(int*)calloc(n,sizeof(int));
    next=(int*)calloc(m,sizeof(int));

    Edge e2[]= {
        {0,3,1},
        {0,2,1},
        {3,1,1},
        {2,1,1},
        {1,4,1},
        {1,5,1},
        {4,5,1},
        //
        {3,0,1},
        {2,0,1},
        {1,3,1},
        {1,2,1},
        {4,1,1},
        {5,1,1},
        {5,4,1}
    };




    m=sizeof(e2)/sizeof(Edge);
    e=e2;

    simplegraph::buildLinkGraph(n,m,e,first,next);
    simplegraph::printLinkedGraph(n,first,next,e);


    index=0;
    root=0;
    cutpointDFS(0,root);

    printf("low:");
    for(i=0; i<n; ++i)
        printf("%d ",low[i]);
    printf("\nnum:");
    for(i=0; i<n; ++i)
        printf("%d ",num[i]);
    printf("\n");
    //
    for(i=0; i<n; ++i)
        if(flags[i])
            printf("(%d)\n",i);
}
}


