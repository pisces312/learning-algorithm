#include"graph.h"
#include"../test_utils.h"

/**
Connected, undirected, unweighted, acyclic graph

0____
|\   \
1 2---4
|
3

**/
AdjMatGraph* testGraph1() {
    int arcs[]= {0,1,0,2,0,4,1,3,2,4};
    return createUnweightedGraph(5,arcs,sizeof(arcs)/sizeof(int));
}
/**
Connected, unweighted graph
    0
   / \
  1   2
 / \  | \
3  4  5  6
 \  \/  /
  --7---

{0,1,0,2,1,3,1,4,2,5,2,6,3,7,4,7,5,7,6,7}

DFS
0 1 3 7 4 5 2 6
1 0 2 5 7 3 4 6
2 0 1 3 7 4 5 6
3 1 0 2 5 7 4 6
4 1 0 2 5 7 3 6
5 2 0 1 3 7 4 6
6 2 0 1 3 7 4 5
7 3 1 0 2 5 6 4
**/
AdjMatGraph* connUweightedGraphForTest() {
    int arcs[]= {0,1,0,2,1,3,1,4,2,5,2,6,3,7,4,7,5,7,6,7};
    return createUnweightedGraph(8,arcs,sizeof(arcs)/sizeof(int));
}



/**
Unconnected, unweighted, acyclic graph
      0
      |
  1   2
 / \  | \
3   4 5--6
 \ /
  7

{0,2,1,3,1,4,2,5,2,6,3,7,4,7,5,6}

**/
AdjMatGraph* unconnGraphForTest() {
    int arcs[]= {0,2,1,3,1,4,2,5,2,6,3,7,4,7,5,6};
    return createUnweightedGraph(8,arcs,sizeof(arcs)/sizeof(int));
}


/**
Circled undirected graph

0--1
|  |
3--2

**/
AdjMatGraph* cirUndirGraphForTest() {
    int arcs[]= {0,1,1,2,2,3,3,0};
    return createUnweightedGraph(4,arcs,ARRAY_SIZE(arcs));
}

/**
undirected acyclic graph

0--1
   |
3--2

**/
AdjMatGraph* acyclicUndirGraphForTest() {
    int arcs[]= {0,1,1,2,2,3};
    return createUnweightedGraph(4,arcs,ARRAY_SIZE(arcs));
}


/**
    1     11     3
(0)---(1)----(3)----(5)
 \     |     /|     /
  \    |   9/ |    /
   \  6|   /  |   /
   2\  |  /  7|  / 4
     \ | /    | /
      \|/  13 |/
      (2)----(4)

**/
AdjMatNetwork* testNetwork1() {
    int arcs[]= {1,3,11,
                 2,4,13,
                 3,5,3,
                 4,5,4,
                 1,2,6,
                 3,4,7,
                 0,1,1,
                 2,3,9,
                 0,2,2
                };
    return createNetwork(6,arcs,sizeof(arcs)/sizeof(int));
}
/**
          30
 +--------------------------+
 |  100          60         |
(0)------>(1)<------(2)<----+
 |         |         |
 |         |         |
 | 10      |         |
 +--->(4)--+--->(3)<-+


**/
AdjMatNetwork* directedNetwork1() {
    int arcs[]= {0,1,100,
                 0,2,30,
                 0,4,10,
                 2,1,60,
                 2,3,60,
                 3,1,10,
                 4,3,50
                };
    return createNetwork(5,arcs,sizeof(arcs)/sizeof(int),1);
}
/**
             10
       (0)------->(3)
        |         /|\
      5 |          |
        |          | 1
       \|/         |
       (1)------->(2)
            3

**/
AdjMatNetwork* directedNetwork2() {
    int arcs[]= {0,1,5,
                 0,3,10,
                 1,2,3,
                 2,3,1
                };
    return createNetwork(4,arcs,sizeof(arcs)/sizeof(int),1);
}

AdjMatNetwork* adjMatDAGNegWeight() {
    int arcs[]= {1,2,2,
                 0,1,3,
                 0,4,5,
                 3,4,2,
                 2,3,3
                };
    return createNetwork(5,arcs,sizeof(arcs)/sizeof(int),1);
}
AdjMatNetwork* unDirAdjMatNetwork1() {
    int arcs[]= {0,1,1,
                 0,2,5,
                 1,2,3,
                 1,3,7,
                 1,4,5,
                 2,4,1,
                 2,5,7,
                 3,4,2,
                 3,6,3,
                 4,5,3,
                 4,6,6,
                 4,7,9,
                 5,7,5,
                 6,7,2,
                 6,8,7,
                 7,8,4
                };
    return createNetwork(9,arcs,sizeof(arcs)/sizeof(int),0);
}
AdjListGraph* unDirAdjListGraph1() {
    AdjListGraph* g=createAdjListGraph(9);
    addEdge(g,0,1,1);
    addEdge(g,0,2,5);
    addEdge(g,1,2,3);
    addEdge(g,1,3,7);
    addEdge(g,1,4,5);
    addEdge(g,2,4,1);
    addEdge(g,2,5,7);
    addEdge(g,3,4,2);
    addEdge(g,3,6,3);
    addEdge(g,4,5,3);
    addEdge(g,4,6,6);
    addEdge(g,4,7,9);
    addEdge(g,5,7,5);
    addEdge(g,6,7,2);
    addEdge(g,6,8,7);
    addEdge(g,7,8,4);
    return g;
}


/**
          2    3
      (0)--(1)--(2)
       |   / \   |
      6| 8/   \5 |7
       | /     \ |
      (3)-------(4)
            9
**/
AdjMatNetwork* unDirAdjMatNetwork2() {
    int arcs[]= {0,1,2,
                 0,3,6,
                 1,2,3,
                 1,3,8,
                 1,4,5,
                 2,4,7,
                 3,4,9
                };
    return createNetwork(5,arcs,sizeof(arcs)/sizeof(int),0);
}
AdjListGraph* unDirAdjListGraph2() {
    AdjListGraph* g=createAdjListGraph(6);
    addEdge(g,0,1,2);
    addEdge(g,0,3,6);
    addEdge(g,1,2,3);
    addEdge(g,1,3,8);
    addEdge(g,1,4,5);
    addEdge(g,2,4,7);
    addEdge(g,3,4,9);
    return g;
}




AdjListGraph* adjListDAG1() {
    AdjListGraph* g=createAdjListGraph(6);
    addEdge(g,5, 2,true);
    addEdge(g,5, 0,true);
    addEdge(g,4, 0,true);
    addEdge(g,4, 1,true);
    addEdge(g,2, 3,true);
    addEdge(g,3, 1,true);
    return g;
}





//////////////////////////////////////////////////////////////////


EdgeListGraphEx* unDirWeightedEdgeGraph1() {
    int n=6;
    Edge e[]= {
        {0,3,1},
        {0,2,1},
        {3,1,1},
        {2,1,1},
        {1,4,1},
        {1,5,1},
        //
        {3,0,1},
        {2,0,1},
        {1,3,1},
        {1,2,1},
        {4,1,1},
        {5,1,1}
    };
    int m=sizeof(e)/sizeof(Edge);
    return createEdgeListGraphEx(n,m,e);
}

EdgeListGraphEx* dirWeightedEdgeGraph1() {
    struct Edge e[9]= {
        {1,3,11},
        {2,4,13},
        {3,5,3},
        {4,5,4},
        {1,2,6},
        {3,4,7},
        {0,1,1},
        {2,3,9},
        {0,2,2}
    };
    int m=sizeof(e)/sizeof(Edge);
    return createEdgeListGraphEx(6,m,e);
}

/**
 Let us create following weighted graph
       10
    0------1
    | \    |
   6|  \   |15
    |  5\  |
    |    \ |
    |     \|
    2------3
        4       **/
EdgeListGraphEx* dirWeightedEdgeGraph2() {


    int V = 5;  // Number of vertexes in graph
    int E = 5;  // Number of edges in graph
    EdgeListGraphEx* graph=createEdgeListGraphEx(V,E,NULL);

    graph->edges=(Edge*) malloc(sizeof(Edge)*E);
    // add edge 0-1
    graph->edges[0].u = 0;
    graph->edges[0].v = 1;
    graph->edges[0].w = 10;

    // add edge 0-2
    graph->edges[1].u = 0;
    graph->edges[1].v = 2;
    graph->edges[1].w = 6;

    // add edge 0-3
    graph->edges[2].u = 0;
    graph->edges[2].v = 3;
    graph->edges[2].w = 5;

    // add edge 1-3
    graph->edges[3].u = 1;
    graph->edges[3].v = 3;
    graph->edges[3].w = 15;

    // add edge 2-3
    graph->edges[4].u = 2;
    graph->edges[4].v = 3;
    graph->edges[4].w = 4;

    setFirstAndNext(graph);

    return graph;
}


EdgeListGraphEx* dirWeightedEdgeGraph3() {
    struct Edge e[]= {
        {0,3,9},
        {1,3,6},
        {0,1,5},
        {3,2,8},
        {0,2,7}
    };
    int m=sizeof(e)/sizeof(Edge);
    return createEdgeListGraphEx(m,e);
}

EdgeListGraphEx* dirWeightedEdgeGraph4() {
    struct Edge e[]= {
        {0,1,1},
        {0,2,12},
        {1,2,9},
        {1,3,3},
        {2,4,5},
        {3,2,4},
        {3,4,13},
        {3,5,15},
        {4,5,4}
    };
    int m=sizeof(e)/sizeof(Edge);
    return createEdgeListGraphEx(m,e);
}

// with negative weight
EdgeListGraphEx* dirWeightedEdgeGraphWithCircle() {
    struct Edge e[]= {
        {0,3,9},
        {1,3,6},
        {0,1,5},
        {3,2,8},
        {2,0,-23}
    };
    int m=sizeof(e)/sizeof(Edge);
    return createEdgeListGraphEx(m,e);
}



