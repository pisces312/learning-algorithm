#include"../test_utils.h"
#include"graph.h"
#include<numeric>
#include<list>
#include"../test_utils.h"
#include"sample_graphs.h"
/*****************************************************************

MST - Prim (Greedy algorithm)

Precondition:
1. Weighted graph
2. Connected graph

!For less vertexes and more edges graph

Support both directed and undirected graph

Properties:
After each step, current MST is connected


Time:
O(v^2), v is the node number
so no relationship with edge number, applied to edge dense graph



*****************************************************************/

/**

Implemented by adjacent matrix

bool array maintains MST node set

Return edge array

**/
int* mstByPrim(AdjMatGraph* g,int cur) {
    int i,j;

    const int n=g->n;

    //store previous node
    int *pre=(int *)malloc(n*sizeof(int));
    //store result. n-1 edges
    int *edges=(int *)malloc(2*(n-1)*sizeof(int));


    //Maintain the distance between current MST to remaining nodes
    int *dis=(int *)malloc(n*sizeof(int));
    int minVal;
    int minIdx;
    //Initial dis array
    for(i=0; i<n; ++i) {//if no edge, INT_MAX
        dis[i]=g->arc[cur][i];
        if(dis[i]==INT_MAX)
            pre[i]=-1;//No pre node for tree root
        else
            pre[i]=cur;
    }


    bool *mstNodeSet=(bool *)calloc(n,sizeof(bool));
    //At start, there is only one node in set
    mstNodeSet[cur]=true;


    for(i=0; i<n-1; ++i) { //at most n-1 edges
//1. Find the min of v->vi
        minVal=INT_MAX;
        for(j=0; j<n; ++j) {
            if(!mstNodeSet[j]&&dis[j]<minVal) {
                minVal=dis[j];
                minIdx=j;
            }
        }
        //Add the node to MST
        mstNodeSet[minIdx]=true;

        //Debug
        printf("add edge<%d,%d> with weight %d\n",pre[minIdx],minIdx,minVal);

//2. Update dis according to new added node j
        for(j=0; j<n; ++j)
            if(!mstNodeSet[j]&&dis[j]>g->arc[minIdx][j]) {
                dis[j]=g->arc[minIdx][j];
                pre[j]=minIdx;
            }
    }

    //prepare result
    for(i=0,j=0; i<n; ++i) {
        if(i!=cur) {
            edges[j++]=pre[i];
            edges[j++]=i;
        }
    }

    free(pre);
    free(dis);
    free(mstNodeSet);
    return edges;
}


// use while loop
int* mstByPrim2(AdjMatGraph* g,int cur) {
    const int n=g->n;

    //store previous node
    int *pre=(int *)malloc(n*sizeof(int));
    //store result. n-1 edges
    int *edges=(int *)malloc(2*(n-1)*sizeof(int));

    bool *visited=(bool *)calloc(g->n,sizeof(bool));

    int minVal;
    int i,k;
    int *dis=(int *)malloc(n*sizeof(int));

    //Initialize, all INT_MAX
    for(i=0; i<n; ++i)
        dis[i]=INT_MAX;


    visited[cur]=true;
    int c=1;
    int j=cur;

    while(c<n) { //at most n-1 edges
        //1. Update dis according to new added node
        for(k=0; k<n; ++k)
            if(!visited[k]&&dis[k] > g->arc[j][k]) {
                dis[k]=g->arc[j][k];
                pre[k]=j;
            }

        //2. Find the min of v->vi
        minVal=INT_MAX;
        for(i=0; i<n; ++i) {
            if(!visited[i]&&dis[i]<minVal) {
                minVal=dis[i];
                j=i;
            }
        }
        visited[j]=true;
        printf("add edge<%d,%d> with weight %d\n",pre[j],j,minVal);
        ++c;
    }

    //prepare result
    for(i=0,j=0; i<n; ++i) {
        if(i!=cur) {
            edges[j++]=pre[i];
            edges[j++]=i;
        }
    }

    free(pre);
    free(dis);
    free(visited);
    return edges;
}


unsigned long mstByPrimCPP(AdjMatGraph* g) {
    const int n=g->n;
    int** e=g->arc;
    //Store the distance array
    int* dis=new int[n];
    memcpy(dis, e[0], n*sizeof(int));
    // the set of nodes that not in MST
    std::list<int> L;
    for(int i=n; --i; L.push_back(i));
    //
    while(!L.empty()) {
        //find the edge with min weight
        int minVal=INT_MAX;
        std::list<int>::iterator minItr; //for remove
        for(std::list<int>::iterator p=L.begin(); p!=L.end(); ++p)
            if(dis[*p]<minVal) {
                minVal=dis[*p];
                minItr=p;
            }
        //remove the new node i from L
        int k = *minItr;
        L.erase(minItr);
        //update remaining nodes
        for(std::list<int>::iterator p=L.begin(); p!=L.end(); ++p)
            if(dis[*p] >= e[k][*p])
                dis[*p] = e[k][*p];
    }
    //Sum D and get the total weight of MST
    unsigned long sum= std::accumulate(dis+1, dis+n, 0LU);
    delete[] dis;
    return sum;
}




typedef int*(*PrimFunc)(AdjMatGraph*,int);
void testPrimDriver(PrimFunc func,AdjMatGraph* g,const int minWeight=-1) {
    //Get MST which starts from every node
    for(int start=0; start<g->n; ++start) {
        int sum=0;
        int* edges=func(g,start);
        for(int i=0; i<2*(g->n-1); i+=2) {
            printf("MST edge<%d,%d>=%d\n",edges[i],edges[i+1],g->arc[edges[i]][edges[i+1]]);
            sum+=g->arc[edges[i]][edges[i+1]];
        }
        if(minWeight>=0)
            assert(minWeight==sum);
        printf("Weight of MST: %d\n",sum);
        printf("---------------------------\n");
    }
}


void testPrim() {

    AdjMatGraph* g=testNetwork1();

    unsigned long sum=mstByPrimCPP(g);
    printf("%lu\n",sum);
    assert(19==sum);

    testPrimDriver(mstByPrim,g,19);
    testPrimDriver(mstByPrim2,g,19);

    g=unDirAdjMatNetwork2();
    testPrimDriver(mstByPrim,g,16);

}







