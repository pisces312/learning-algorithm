#include "graph.h"
#include "../common.h"

AdjMatGraph* emptyUnweightedGraph(const int verNum) {
    AdjMatGraph* g=new AdjMatGraph;
    g->n=verNum;
    g->arc=malloc_Array2D<int>(g->n,g->n);

    for(int i=0; i<g->n; ++i)
        for(int j=0; j<g->n; ++j)
            g->arc[i][j]=0;
//            if(i==j) g->arc[i][j]=0;
//            else g->arc[i][j]=INT_MAX;
    return g;
}

//arrSize is 2*arcNum
AdjMatGraph* createUnweightedGraph(int vertNum, int* arcs,int arrSize,int directed) {
    AdjMatGraph* g=emptyUnweightedGraph(vertNum);
    for(int i=0; i<arrSize; i+=2)
        g->arc[arcs[i]][arcs[i+1]]=1;
    if(!directed)
        for(int i=0; i<arrSize; i+=2)
            g->arc[arcs[i+1]][arcs[i]]=1;
    return g;
}

/*
Network, weighted graph
*/

//INT_MAX means no edge for weighted graph
//<i,i>=0
AdjMatNetwork* emptyNetwork(const int verNum) {
    AdjMatNetwork* g=new AdjMatNetwork;
    g->n=verNum;
    g->arc=malloc_Array2D<int>(g->n,g->n);

    for(int i=0; i<g->n; ++i)
        for(int j=0; j<g->n; ++j)
            if(i==j)
                g->arc[i][j]=0;
            else
                g->arc[i][j]=INT_MAX;
    return g;
}
// arcs will be copied
static AdjMatNetwork* createNetwork(AdjMatNetwork* g, int* arcs,int arrSize,int directed) {
    for(int i=0; i<arrSize; i+=3)
        g->arc[arcs[i]][arcs[i+1]]=arcs[i+2];
    if(!directed)
        for(int i=0; i<arrSize; i+=3)
            g->arc[arcs[i+1]][arcs[i]]=arcs[i+2];
    return g;
}


AdjMatNetwork* createNetwork(int vertNum, int* arcs,int arrSize,int directed) {
    AdjMatNetwork* g=emptyNetwork(vertNum);
    return createNetwork(g,arcs,arrSize,directed);
}

void printAdjMatGraph(AdjMatGraph* graph) {
    int** e=graph->arc;
    int n=graph->n;
    for(int i=0; i<n; ++i) {
        for(int j=0; j<n; ++j) {
            if(e[i][j]==INT_MAX)
                printf("%4c",'-');
            else
                printf("%4d",e[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

////////////////////////////////////////////////////////////
// Adjacent List Graph
////////////////////////////////////////////////////////////

// A utility function to create a new adjacency list node
struct AdjListNode* newAdjListNode(int dest) {
    AdjListNode* newNode = (AdjListNode*) malloc(sizeof(AdjListNode));
    newNode->dest = dest;
    newNode->next = NULL;
    return newNode;
}

AdjListGraph* createAdjListGraph(int n) {
    AdjListGraph* graph = (AdjListGraph*) malloc(sizeof(AdjListGraph));
    graph->n = n;
    // Create an array of adjacency lists. Size of array will be V
    graph->array = (AdjList*) malloc(n * sizeof(AdjList));
    // Initialize each adjacency list as empty by making head as NULL
    for(int i = 0; i < n; ++i)
        graph->array[i].head = NULL;
    return graph;
}

void addEdge(AdjListGraph* graph, int src, int dest, int weight, bool directed) {
    // Add an edge from src to dest. A new node is added to the adjacency
    // list of src. The node is added at the begining
    AdjListNode* newNode = newAdjListNode(dest);
    newNode->w=weight;
    newNode->next = graph->array[src].head;
    graph->array[src].head = newNode;

    if(!directed) {
        // Since graph is undirected, add an edge from dest to src also
        newNode = newAdjListNode(src);
        newNode->w=weight;
        newNode->next = graph->array[dest].head;
        graph->array[dest].head = newNode;
    }
}
// Adds an edge to an undirected graph
void addEdge(AdjListGraph* graph, int src, int dest, bool directed) {
    addEdge(graph,src,dest,0,directed);
}

// A utility function to print the adjacenncy list representation of graph
void printAdjListGraph(AdjListGraph* g) {
    printf("\nAdjacency list \n");
    for(int v = 0; v < g->n; ++v) {
        AdjListNode* pCrawl = g->array[v].head;
        printf("Node %d: ", v);
        while(pCrawl) {
            printf("-> %d", pCrawl->dest);
            pCrawl = pCrawl->next;
        }
        printf("\n");
    }
}



int printNetworkIntVal(void* data) {
    int v=*((int*)data);
    if(INT_MAX==v)
        printf("%4c",'-');
    else
        printf("%4d",v);
}



///////////////////////////////////////////////////////////////////////////////


void swapEdge(struct Edge* x,struct Edge* y) {
    struct Edge t=*x;
    *x=*y;
    *y=t;
}

// Compare two edges according to their weights.
// Used in qsort() for sorting an array of edges
int edgeComp(const void* a, const void* b) {
    struct Edge* a1 = (struct Edge*)a;
    struct Edge* b1 = (struct Edge*)b;
    return a1->w > b1->w;
}



static int getVertexNum(Edge* e,int m) {
    if(!e)
        return 0;
    int maxVal=0;
    for(int i=0; i<m; ++i) {
        if(e[i].u>maxVal)
            maxVal=e[i].u;
        if(e[i].v>maxVal)
            maxVal=e[i].v;
    }
    if(maxVal>0)
        ++maxVal;
    return maxVal;
}


//nE must be set first
static void setEdge(EdgeListGraph* g, Edge* e,bool copy) {
    int m=g->nE;
    Edge* edges=e;
    if(copy&&e) {
        edges = (Edge*) malloc(sizeof(Edge)*m);
        for(int i=0; i<m; ++i) {
            edges[i].u=e[i].u;
            edges[i].v=e[i].v;
            edges[i].w=e[i].w;
        }
    }
    g->edges=edges;
}

//Not update first and next array
EdgeListGraph* createEdgeListGraph(const int n,const int m, struct Edge* e,bool copy) {
    EdgeListGraph* g = (EdgeListGraph*) malloc(sizeof(EdgeListGraph));
    g->nV=n;
    g->nE=m;
    setEdge(g,e,copy);
    return g;
}

//nV,nE and edges must be set first
void setFirstAndNext(EdgeListGraphEx* g) {
    int n=g->nV;
    int m=g->nE;
    Edge* e=g->edges;
    //
    int* first=(int*)calloc(n,sizeof(int));
    int* next=(int*)calloc(m,sizeof(int));
    for(int i=0; i<n; ++i) //Vertex, initial first
        first[i]=-1;
    for(int i=0; i<m; ++i) { //Edge, update first and next
        next[i]=first[e[i].u];
        first[e[i].u]=i;
    }
    //
    g->first=first;
    g->next=next;
}

EdgeListGraphEx* createEdgeListGraphEx(const int m, struct Edge* e,bool copy) {
    int n=getVertexNum(e,m);
    return createEdgeListGraphEx(n,m, e,copy);
}

EdgeListGraphEx* createEdgeListGraphEx(const int n,const int m, struct Edge* e,bool copy) {
    EdgeListGraphEx* g = (EdgeListGraphEx*) malloc(sizeof(EdgeListGraphEx));
    g->nV=n;
    g->nE=m;
    setEdge((EdgeListGraph*)g,e,copy);
    if(e) {
        setFirstAndNext(g);
    }
    return g;
}


void printEdgeListGraphEx(EdgeListGraphEx* g) {
    for(int i=0; i<g->nV; ++i) {
        printf("%d: ",i);
        int eIdx=g->first[i];
        while(eIdx!=-1) {
            printf("-> %d(%d) ",g->edges[eIdx].v, g->edges[eIdx].w);
            eIdx=g->next[eIdx];
        }
        printf("\n");
    }
    printf("\n");
}

AdjMatNetwork* edgeList2AdjMatGraph(EdgeListGraph* edgeListGraph) {
    int n=edgeListGraph->nV;
    int m=edgeListGraph->nE;
    Edge* e=edgeListGraph->edges;
    AdjMatNetwork* g=emptyNetwork(n);
    for(int i=0; i<m; ++i) {
        int u=e[i].u;
        int v=e[i].v;
        int w=e[i].w;
        g->arc[u][v]=w;
    }
    return g;
}

EdgeListGraphEx* adjMat2EdgeListGraph(AdjMatNetwork* adjMatG) {
    const int n=adjMatG->n;
    int** e=adjMatG->arc;

    int m=0;
    for(int i=0; i<n; ++i)
        for(int j=0; j<n; ++j)
            if(i!=j&&e[i][j]!=INT_MAX)
                ++m;
    Edge* edges=(Edge*)malloc(m*sizeof(Edge));
    int k=0;
    for(int i=0; i<n; ++i)
        for(int j=0; j<n; ++j)
            if(i!=j&&e[i][j]!=INT_MAX) {
                edges[k].u=i;
                edges[k].v=j;
                edges[k].w=e[i][j];
                ++k;
            }
    EdgeListGraphEx* g= createEdgeListGraphEx(n,m,edges,false);
    setFirstAndNext(g);
    return g;
}

namespace simplegraph {
//TODO
void buildLinkGraph(const int n,const int m, struct Edge* e,int* first, int *next) {
    int i;
    for(i=0; i<n; ++i) //Vertex, initial first
        first[i]=-1;
    for(i=0; i<m; ++i) { //Edge, update first and next
        next[i]=first[e[i].u];
        first[e[i].u]=i;
    }
}


void printLinkedGraph(int n,int*first,int*next,struct Edge* e) {
    int i,t;
    for(i=0; i<n; ++i) {
        printf("%d:",i);
        t=first[i];
        while(t!=-1) {
            printf("%d(%d) ",e[t].v,e[t].w);
            t=next[t];
        }
        printf("\n");
    }
}


}
