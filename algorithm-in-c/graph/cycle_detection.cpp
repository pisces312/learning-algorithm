#include"graph.h"
#include"../test_utils.h"
#include"sample_graphs.h"
#include"../tree/tree.h"
/******************************************************************


TODO other algs

http://www.cnblogs.com/TenosDoIt/p/3644225.html

******************************************************************/



/**

Use dfs

Input:
directed/undirected, connected graph

**/
static bool _cycleDetectDFS(AdjMatGraph* g,int father,int cur,bool* visited) {
    visited[cur]=true;
    for(int i=0; i<g->n; ++i)
        if(i!=cur
                &&g->arc[cur][i]) {
            if(visited[i]&&i!=father)//! the visited one must be the father
                return true;
            if(!visited[i]&&_cycleDetectDFS(g,cur,i,visited))
                return true;
        }
    return false;
}

bool cycleDetectDFS(AdjMatGraph* g) {
    bool* visited=(bool*)calloc(g->n,sizeof(bool));
    //the father of start is -1
    //since for loop will ignore start itself,
    //it will not have any other node that has visited
    bool flag=_cycleDetectDFS(g,-1,0,visited);
    free(visited);
    return flag;
}

/**

Detect Cycle in an Undirected Graph

http://www.geeksforgeeks.org/union-find/

Time:
    Basic union find takes O(n), total is O(nE)
    Union find with rank takes O(logn), total is O(Elogn)

**/
bool isCycleInUndirGraph(EdgeListGraph* g) {
    int nE=g->nE;
    int nV=g->nV;
    Edge* e=g->edges;

    int* f=UnionFind::makeSet(nV);
    for(int i=0; i<nE; ++i) {
        int s1=UnionFind::findSet(f,e[i].u);
        int s2=UnionFind::findSet(f,e[i].v);
        if(s1==s2)
            return true;
        UnionFind::unionSet(f,e[i].u,e[i].v);
    }
    return false;
}


void testCircleInGraph() {
    AdjMatGraph* g=cirUndirGraphForTest();
    printMatrix(g->arc,g->n);
    bool flag=cycleDetectDFS(g);
    printf("%d\n",flag);
    assert(flag);

    g=connUweightedGraphForTest();
    printMatrix(g->arc,g->n);
    flag=cycleDetectDFS(g);
    printf("%d\n",flag);
    assert(flag);

    g=acyclicUndirGraphForTest();
    printMatrix(g->arc,g->n);
    flag=cycleDetectDFS(g);
    printf("%d\n",flag);
    assert(flag==false);

    EdgeListGraph* edgeListGraph=unDirWeightedEdgeGraph1();
    flag=isCycleInUndirGraph(edgeListGraph);
    printf("isCycleInUndirGraph:%d\n",flag);
    //    assert(flag==false);

}

