#include "../test_utils.h"
#include "graph.h"
#include "sample_graphs.h"

/**
DFS for shortest path

Pros:
    No need aux array
Cons:
    Only one shortest path for point to point

**/
static int e[100][100];
static int src, dest;
static int minDis = INT_MAX, n;
static void dfsForSP(int cur, int dis, int* visited) {
    if(dis > minDis)
        return;
    if(cur == dest) {  //Reach destination
        if(dis < minDis)
            minDis = dis;
        return;
    }
    for(int i = 0; i < n; ++i)
        if(e[cur][i] < INT_MAX && !visited[i]) {
            visited[i] = 1;
            dfsForSP(i, dis + e[cur][i], visited);
            visited[i] = 0;
        }
}
void shortestPathByDFSSimple() {
    int visited[100];
    src = 0, dest = 4, n = 5;
    for(int i = 0; i < n; ++i)
        for(int j = 0; j < n; ++j)
            if(i == j)
                e[i][j] = 0;
            else
                e[i][j] = INT_MAX;

    e[0][1] = 2;
    e[0][4] = 10;
    e[1][2] = 3;
    e[1][4] = 7;
    e[2][0] = 4;
    e[2][3] = 4;
    e[3][4] = 5;
    e[4][2] = 3;

    for(int i = 0; i < n; ++i)
        visited[i] = 0;

    visited[src] = 1;
    dfsForSP(src, 0, visited);
    printf("%d\n", minDis);
    assert(minDis == 9);
}

/**

 Dijkstra (Greedy)

 Description:
    ! Weight cannot be negative
    SSSP
    Support both directed and undirected graph
    Use adjacency matrix graph
 Precondition:
    All weights are non-negative values
    Weighted and connected graph
 Edge:
    <i,i>=0
    No edge between i and j, dis=INT_MAX
 Return path array

 http://www.geeksforgeeks.org/greedy-algorithms-set-6-dijkstras-shortest-path-algorithm/

**/
int* shortestPathByDijkstra(AdjMatGraph* g, int v0) {
    int i, j, u, v, minDis;
    int n = g->n;
    int** e = g->arc;
    //dis[i]: The total weights of v0->vi
    int* dis = (int*)malloc(n * sizeof(int));
    //The previous node index k of vk->vi
    int* path = (int*)malloc(n * sizeof(int));
    bool* sptSet = (bool*)malloc(n * sizeof(bool));

    //1. Initialization
    for(i = 0; i < n; ++i) {
        sptSet[i] = false;
        dis[i] = e[v0][i]; //can be INT_MAX if not connected
        if(dis[i] == INT_MAX)
            path[i] = -1;
        else //v0 is pre node of i if v0->i
            path[i] = v0;
    }
    //Make start node visited
    dis[v0] = 0;
    sptSet[v0] = true;

    //2. Core logic
    //! n-1 iterations, because v0 has already been considered, "i" is only used to count the number of iterations, not for node
    //! each iteration finds one shortest path for a "certain" vertex (v0->vx), vx can be any of n-1 nodes, no special order
    for(i = 0; i < n - 1; ++i) {
        minDis = INT_MAX;
        //! At least one edge for a connected graph, so after iteration, "minDis<INT_MAX"
        for(j = 0; j < n; ++j)
            if(!sptSet[j]
                    && dis[j] < minDis) {
                u = j;
                minDis = dis[u]; //Find u with min dis
            }

        sptSet[u] = true; //Add u to set P

        for(v = 0; v < n; ++v)
            // make sure no overflow for add operation
            // if(e[u][v]<INT_MAX&&minDis<INT_MAX
            //         &&dis[v]>minDis+e[u][v])
            // dis[u] is minDis, but it's more clear to
            // use dis[u] instead of minDis
            if(e[u][v] < INT_MAX
                    && !sptSet[v]
                    && dis[v] > dis[u] + e[u][v]) {
                dis[v] = dis[u] + e[u][v];
                path[v] = u;
            }
    }

    //Result
    printf("Path array: ");
    for(i = 0; i < n; ++i)
        printf("%d ", path[i]);
    printf("\n");

    free(sptSet);
    free(dis);

    return path;
}
// A utility function to find the vertex with minimum distance value, from
// the set of vertices not yet included in shortest path tree
static int minDistanceVertex(int n, int dist[], bool sptSet[]) {
    int minDis = INT_MAX, min_index;
    for(int v = 0; v < n; v++)
        if(!sptSet[v] && dist[v] <= INT_MIN)
            minDis = dist[v], min_index = v;
    return min_index;
}
//Show the shortest path of v0->,...,->v
void showSPByDijkstra(AdjMatGraph* g, int* path, int v0, int v) {
    int* st = (int*)malloc(g->n * sizeof(int));
    int top = -1;
    int total = 0;

    //Use a stack to reserve path
    while(v != v0 && path[v] != -1) {
        total += g->arc[path[v]][v];
        st[++top] = v;
        v = path[v];
    }

    if(path[v] == -1) {
        printf("No shortest path for %d->%d\n", v0, v);
    } else {
        st[++top] = v0;
        printf("Path with weights %d (", total);
        while(top >= 0) {
            printf("%d ", st[top]);
            --top;
        }
        printf(")\n");
    }

    free(st);
}

void shortestPathByDijkstraSimple() {
    static const int MAX_DIS = 999;
    static int e[10][10], visited[10], dis[10];
    int i, j, u, v, minDis;
    int src = 0, n = 6; //Specify any node

    for(i = 0; i < n; ++i)
        for(j = 0; j < n; ++j)
            if(i == j)
                e[i][j] = 0;
            else
                e[i][j] = MAX_DIS;

    //Initial data
    e[0][1] = 1;
    e[0][2] = 12;
    e[1][2] = 9;
    e[1][3] = 3;
    e[2][4] = 5;
    e[3][2] = 4;
    e[3][4] = 13;
    e[3][5] = 15;
    e[4][5] = 4;

    for(i = 0; i < n; ++i)
        dis[i] = e[src][i];

    for(i = 0; i < n; ++i)
        visited[i] = 0;
    visited[src] = 1; //Add src to set P

    for(i = 0; i < n - 1; ++i) {
        minDis = MAX_DIS;
        for(j = 0; j < n; ++j)
            if(!visited[j]
                    && dis[j] < minDis) {
                minDis = dis[j]; //Find u with min dis
                u = j;
            }
        visited[u] = 1; //Add u to set P
        for(v = 0; v < n; ++v)
            if(e[u][v] < MAX_DIS
                    && !visited[v]
                    && dis[v] > dis[u] + e[u][v])
                dis[v] = dis[u] + e[u][v];
    }

    for(i = 0; i < n; ++i)  //Result
        printf("%d ", dis[i]);
    printf("\n");
}

/***************************************************************

Floyd Warshall (Dynamic Programming)

 Description:
    Calculate all shortest paths
    No need "visited" array
    Use adjacency matrix graph

 Precondition:
    ! All weights have to be non-negative values
    Require weighted and connected graph

 Edge:
    <i,i>=0
    No edge between i and j, dis=INT_MAX

 Return:
    path array

 Steps:
 1. Initialize the solution matrix same as the input graph matrix as a first step.
 2. Update the solution matrix by considering all vertices as an intermediate vertex.
    !! When we pick vertex number k as an intermediate vertex, we already have considered  vertices {0, 1, 2, .. k-1} as intermediate vertices.

For every pair (i, j) of source and destination vertices respectively, there are two possible cases.
1) k is not an intermediate vertex in shortest path from i to j. We keep the value of dist[i][j] as it is.
2) k is an intermediate vertex in shortest path from i to j. We update the value of dist[i][j] as dist[i][k] + dist[k][j].

Time Complexity: O(V^3)

http://www.geeksforgeeks.org/dynamic-programming-set-16-floyd-warshall-algorithm/

***************************************************************/

int** shortestPathByFloyd(AdjMatGraph* g) {
    int n = g->n;
    int** e = g->arc;

    int** dis = malloc_Array2D<int>(n, n, false);
    int** path = malloc_Array2D<int>(n, n, false);

    for(int i = 0; i < n; ++i)
        for(int j = 0; j < n; ++j) {
            dis[i][j] = e[i][j];
            path[i][j] = i; //!initial value is the start value
        }

    for(int k = 0; k < n; ++k)
        for(int i = 0; i < n; ++i)
            for(int j = 0; j < n; ++j)
                if(dis[i][k] < INT_MAX && dis[k][j] < INT_MAX) {  //avoid overflow
                    if(dis[i][j] > dis[i][k] + dis[k][j]) {
                        dis[i][j] = dis[i][k] + dis[k][j];
                        //! record path from back to forth
                        path[i][j] = path[k][j];
                    }
                }

    printMatrix(dis, n, printNetworkIntVal);
    printMatrix(path, n);

    free_Array2D(dis);

    return path;
}

void showSPByFloyd(AdjMatGraph* g, int** path, int u, int v) {
    int* st = (int*)malloc(g->n * sizeof(int));
    int top = -1;

    int w = 0;
    int i = u, k = v;

    bool isConnected = true;

    //! Print both start and end nodes
    while(true) {
        ++top;
        st[top] = k;
        if(i == k)
            break;
        if(g->arc[path[i][k]][k] == INT_MAX) {
            isConnected = false;
            break;
        }
        w += g->arc[path[i][k]][k];
        k = path[i][k];
    }

    if(isConnected) {
        printf("SP of %d->%d (%d): ", u, v, w);
        while(top >= 0) {
            printf("%3d", st[top]);
            --top;
        }
        printf("\n");
    } else
        printf("No SP for %d->%d\n", u, v);

    free(st);
}

void shortestPathByFloydSimple() {
    static int MAX_DIS = 999;
    int e[100][100], i, j, k, n = 5;
    int dis[100][100];

    //build a directed connected graph
    for(i = 0; i < n; ++i)
        for(j = 0; j < n; ++j)
            if(i == j)
                e[i][j] = 0; //to itself
            else
                e[i][j] = MAX_DIS; //not connected
    e[0][1] = 2;
    e[0][2] = 6;
    e[0][3] = 4;
    e[1][2] = 3;
    e[2][0] = 7;
    e[2][3] = 1;
    e[3][0] = 5;
    e[3][2] = 12;

    //initial distance array
    for(i = 0; i < n; ++i)
        for(j = 0; j < n; ++j)
            dis[i][j] = e[i][j];

    //! The most outer loop must be for the intermediate node
    for(k = 0; k < n; ++k)
        for(i = 0; i < n; ++i)
            for(j = 0; j < n; ++j)
                if(dis[i][k] < MAX_DIS
                        && dis[k][j] < MAX_DIS
                        && dis[i][j] > dis[i][k] + dis[k][j])
                    dis[i][j] = dis[i][k] + dis[k][j];

    for(i = 0; i < n; ++i) {
        for(j = 0; j < n; ++j)
            printf("%3d ", e[i][j]);
        printf("\n");
    }
}

//
//
//
//void shortestPathByDijkstraLinkedGraph() {
//    static const int MAX_DIS=999;
//    int i,j,t,minDis;
//    int src=0, uMin;//Specify any node
//
//    int n=6;
//    int m=9;
//    int *visited=(int*)calloc(n,sizeof(int));
//    int *dis=(int*)malloc(n*sizeof(int));
//    int *u=(int*)malloc(m*sizeof(int));
//    int *v=(int*)malloc(m*sizeof(int));
//    int *w=(int*)malloc(m*sizeof(int));
//    int *first=(int*)malloc(n*sizeof(int));//store edge index
//    int *next=(int*)malloc(m*sizeof(int));
//    u[0]=0,u[1]=0,u[2]=1,u[3]=1,u[4]=2,u[5]=3,u[6]=3,u[7]=3,u[8]=4;
//    v[0]=1,v[1]=2,v[2]=2,v[3]=3,v[4]=4,v[5]=2,v[6]=4,v[7]=5,v[8]=5;
//    w[0]=1,w[1]=12,w[2]=9,w[3]=3,w[4]=5,w[5]=4,w[6]=13,w[7]=15,w[8]=4;
//
//    buildLinkGraph(n,m,u,first,next);
//    printLinkedGraph(n,first,next,v,w);
//
//    //Set dis
//    for(i=0; i<n; ++i)
//        dis[i]=MAX_DIS;
//    dis[src]=0;
//    t=first[src];
//    while(t!=-1) {
//        dis[v[t]]=w[t];
//        t=next[t];
//    }
//
//    visited[src]=1;//Add src to set P
//    for(i=0; i<n-1; ++i) {
//        minDis=MAX_DIS;
//        for(j=0; j<n; ++j)//Find the min dis from current table
//            if(visited[j]==0&&dis[j]<minDis) {
//                minDis=dis[j];//Find u with min dis
//                uMin=j;
//            }
//        visited[uMin]=1;//Add to set P
//        t=first[uMin];
//        while(t!=-1) {//Enhanced by linked graph
//            if(w[t]<MAX_DIS&&dis[v[t]]>dis[uMin]+w[t])
//                dis[v[t]]=dis[uMin]+w[t];
//            t=next[t];
//        }
//    }
//    for(i=0; i<n; ++i)//Result
//        printf("%d ",dis[i]);
//    printf("\n");
//
//}
//

/**

Bellman Ford (DP)

Description:
    ! Support negative weight
    SSSP：Single-Source Shortest Path
    Cannot cover negative cycle
Return:
    Only the weight of shortestpath s->v
Time complexity:
    O(V*E)
**/
int* shortestPathByBellmanFord(EdgeListGraph* g, int s) {
    const int n = g->nV;
    const int m = g->nE;
    Edge* e = g->edges;
    int* dis = (int*)malloc(n * sizeof(int));

    //Initial dis array
    for(int i = 0; i < n; ++i)
        dis[i] = INT_MAX;
    dis[s] = 0;

    //Relax
    for(int i = 0; i < n - 1; ++i) {
        bool relaxed = false;
        for(int j = 0; j < m; ++j) {
            int u = e[j].u;
            int v = e[j].v;
            int w = e[j].w;
            if(dis[u] != INT_MAX
                    && dis[v] > dis[u] + w) {
                dis[v] = dis[u] + w;
                relaxed = true;
            }
        }
        if(!relaxed) { //[Optional] Optimization
            printf("Relax finished at %d/%d round\n", i + 1, n - 1);
            break;
        }
    }

    //Check negative ring
    for(int j = 0; j < m; ++j) {
        int u = e[j].u;
        int v = e[j].v;
        int w = e[j].w;
        if(dis[u] != INT_MAX
                && dis[v] > dis[u] + w) {
            printf("Found negative cycle: dis[%d](%d)>dis[%d](%d)+%d\n",u,dis[u],v,dis[v],w);
            break;
        }
    }

    return dis;
}



/**

Bellman Ford - Optimized by queue


Check negative cycle:
    if the number of enqueue > n

http://www.cnblogs.com/lxt1105/p/6478108.html

**/
int* shortestPathByBellmanFordUsingQueue(AdjMatGraph* g, int s) {
    const int n=g->n;
    int** e=g->arc;

    //queue, for each node, max enqueue number is n-1 if no negative cycle
    //it will be n for negative cycle case
    //actually only need n*(n-1)+1, because once negative cycle detected, it will return
    int* q=(int*)malloc(n*n*sizeof(int));
    int head=0,tail=-1;

    //Initial dis
    int* dis=(int*)malloc(n*sizeof(int));
    for(int i=0; i<n; ++i)
        dis[i]=INT_MAX;
    dis[s]=0;

    q[++tail]=s;
    //Detect negative cycle
    int *enqueueCount=(int*)calloc(n,sizeof(int));//default is 0
    ++enqueueCount[s];
    bool hasNegCycle=false;
    while(head<=tail&&!hasNegCycle) {
        int u=q[head++];
        for(int v=0; v<n; ++v) {
            if(dis[u]<INT_MAX
                    &&e[u][v]<INT_MAX
                    &&dis[v]>dis[u]+e[u][v]) {
                dis[v]=dis[u]+e[u][v];//update dis
                q[++tail]=v; //if u->v is changed, enqueue v
                ++enqueueCount[v]; //count enqueue number
                if(enqueueCount[v]==n) { //check negative cycle
                    printf("Negative cycle is detected!\n");
                    hasNegCycle=true;
                    break;
                }
            }
        }
    }
    free(q);
    free(enqueueCount);
    return dis;
}

/**********************************************************

Test

**********************************************************/

static void testDijkstraCore(AdjMatGraph* g, int v0) {
    //Must connected graph
    if(isDirectedNetworkConnected(g, v0)) {
        int* path = shortestPathByDijkstra(g, v0);
        for(int j = 0; j < g->n; ++j) {
            if(v0 != j) {
                printf("%d->%d\n", v0, j);
                showSPByDijkstra(g, path, v0, j);
                printf("------------------------------\n");
            }
        }
        free(path);
    } else {
        printf("Not connected graph started with %d\n", v0);
    }
}

void testDijkstra() {
    //Test Dijkstra in one method
    shortestPathByDijkstraSimple();

    AdjMatGraph* g = NULL;
    //Sample 1
    g = unDirAdjMatNetwork1();
    testDijkstraCore(g, 0);

    //Sample 2
    //0->1: 0,4,3,1
    //0->2: 0,2
    //0->3: 0,4,3 (Part of 0->1)
    //0->4: 0,4 (Part of 0->1)
    g = directedNetwork1();
    for(int i = 0; i < g->n; ++i) {
        testDijkstraCore(g, i);
        printf("==============================\n");
    }
}

static void testFloydCore(AdjMatGraph* g) {
    int** path = shortestPathByFloyd(g);
    for(int i = 0; i < g->n; ++i)
        for(int j = 0; j < g->n; ++j)
            if(i != j)
                showSPByFloyd(g, path, i, j);
}

void testFloydWarshall() {
    shortestPathByFloydSimple();

    AdjMatGraph* g = NULL;

    g = unDirAdjMatNetwork1();
    testFloydCore(g);

    g = directedNetwork1();
    testFloydCore(g);

    g = directedNetwork2();
    testFloydCore(g);
}
void testBellmanFord() {
    int* dis;
    EdgeListGraphEx* g = dirWeightedEdgeGraph4();
    printEdgeListGraphEx(g);
    dis=shortestPathByBellmanFord(g, 0);
    printArray(dis,g->nV);


    EdgeListGraphEx* g2 = dirWeightedEdgeGraphWithCircle();
    printEdgeListGraphEx(g2);
    dis=shortestPathByBellmanFord(g2, 0);
    printArray(dis,g2->nV);

    //Use queue
    AdjMatGraph* adjMatGraph=adjMatDAGNegWeight();
    printAdjMatGraph(adjMatGraph);
    dis=shortestPathByBellmanFordUsingQueue(adjMatGraph,0);
    printArray(dis,adjMatGraph->n);

    adjMatGraph=edgeList2AdjMatGraph(g);
    printAdjMatGraph(adjMatGraph);
    dis=shortestPathByBellmanFordUsingQueue(adjMatGraph,0);
    printArray(dis,adjMatGraph->n);

    adjMatGraph=edgeList2AdjMatGraph(g2);
    printAdjMatGraph(adjMatGraph);
    dis=shortestPathByBellmanFordUsingQueue(adjMatGraph,0);
    printArray(dis,adjMatGraph->n);



}
void testShortestPathAlgos() {
    shortestPathByDFSSimple();
    testDijkstra();
    testFloydWarshall();
    testBellmanFord();
}
