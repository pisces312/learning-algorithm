#include"test_utils.h"



void printArray(int*a,int low,int high) {
    printArray(a,low,high,printAsInt);
//    printArray<int>(a,low,high,printAsInt);
}

void printArray(int*a,int n) {
    printArray(a,0,n-1,printAsInt);
}

int printAsChar(void* data) {
    return printf("%c ", *((char*)data));
}
int printAsInt(void* data) {
    return printf("%d ", *((int*)data));
}
int printIntWithTab(void* data) {
    return printf("%d\t", *((int*)data));
}

void printMatrix(int**a,int m,int n) {
    printMatrix<int>(a,m,n,printAsInt);
}
void printMatrix(int**a,int n) {
    printMatrix<int>(a,n,n,printAsInt);
}
void assertSorted(int *x,int n) {
    for(int i=1; i<n; ++i)
        if(x[i-1]>x[i])
            assert(false);
}
void createRandomData(int* keys,int n) {
    time_t seed=clock();
    //!! "time()" may generate same random data
//    time(&seed);
    srand(seed);
    for(int i=0; i<n; ++i)
        keys[i]=rand()%n;
}
void createRandomUniqPostiveData(int* keys,int n) {
    time_t seed;
    time(&seed);
    srand(seed);
    for(int i=0; i<n; ++i)
        keys[i]=i;
    int a=0,b=0;
    for(int i=0; i<n; ++i) {
        a=rand()%n;
        b=rand()%n;
        swap(keys,a,b);
    }
}
