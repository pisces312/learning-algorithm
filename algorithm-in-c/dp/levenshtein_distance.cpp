#include "../test_utils.h"
/*******************************************************

LD algorithm

Given two strings str1 and str2 and below operations that can performed on str1.
Find minimum number of edits (operations) required to convert ��str1�� into ��str2��.

Insert
Remove
Replace

All of the above operations are of equal cost.

Examples:
Input:   str1 = "geek", str2 = "gesek"
Output:  1
We can convert str1 into str2 by inserting a 's'.

Input:   str1 = "cat", str2 = "cut"
Output:  1
We can convert str1 into str2 by replacing 'a' with 'u'.

Input:   str1 = "sunday", str2 = "saturday"
Output:  3
Last three and first characters are same.  We basically
need to convert "un" to "atur".  This can be done using
below three operations.
Replace 'n' with 'r', insert t, insert a


�ַ��������㷨(string similarity)
ֵԽ��Խ������

�༭���룬�ֳ�Levenshtein���룬��ָ�����ִ�֮�䣬��һ��ת����һ����������ٱ༭������������ɵı༭����������һ���ַ��滻����һ���ַ�������һ���ַ���ɾ��һ���ַ���
���罫kittenһ��ת��sitting��
sitten ��k��s��
sittin ��e��i��
sitting ����g��

���⣺�ҳ��ַ����ı༭���룬����һ���ַ���s1���پ������ٲ�������ɱ���ַ���s2�����������֣����һ���ַ���ɾ��һ���ַ����޸�һ���ַ�


http://www.geeksforgeeks.org/dynamic-programming-set-5-edit-distance/
http://www.cnblogs.com/biyeymyhjob/archive/2012/09/28/2707343.html
http://www.cnblogs.com/yujunyong/articles/2004724.html


**********************************************************/


/**

Very basic recursive version without DP

The time complexity of above solution is exponential.
In worst case, we may end up doing O(3^m) operations.
The worst case happens when none of characters of two strings match.

The idea is process all characters one by one staring from either from left or right sides of both strings.
Let we traverse from right corner, there are two possibilities for every pair of character being traversed.

m: Length of str1 (first string)
n: Length of str2 (second string)

If last characters of two strings are same, nothing much to do. Ignore last characters and get count for remaining strings. So we recur for lengths m-1 and n-1.
Else (If last characters are not same), we consider all operations on ��str1��, consider all three operations on last character of first string, recursively compute minimum cost for all three operations and take minimum of three values.
Insert: Recur for m and n-1
Remove: Recur for m-1 and n
Replace: Recur for m-1 and n-1

**/
int editDistanceRec(const char *src, int m,const char* dest,int n) {
// If first/second string is empty, the only option is to
// insert all characters of second/first string into first
    if(m==0||n==0) return std::max(m,n);
    //!Compare first element, because the scan order is from left to right
    if(src[0]==dest[0]) return editDistanceRec(src+1,m-1,dest+1,n-1);

    //Add a char to source
    int ins=editDistanceRec(src,m,dest+1,n-1)+1;
    int del=editDistanceRec(src+1,m-1,dest,n)+1;
    //If use replace, compare next char then
    int rep=editDistanceRec(src+1,m-1,dest+1,n-1)+1;

    return min(ins,del,rep);
}

//!Compare from last element to first
int editDistanceRec2(const char *str1, int m,const char* str2,int n) {
// If first string is empty, the only option is to
// insert all characters of second string into first
    if (m == 0) return n;

// If second string is empty, the only option is to
// remove all characters of first string
    if (n == 0) return m;

// If last characters of two strings are same, nothing
// much to do. Ignore last characters and get count for
// remaining strings.
    if (str1[m-1] == str2[n-1])
        return editDistanceRec2(str1,m-1, str2,  n-1);

// If last characters are not same, consider all three
// operations on last character of first string, redcursively
// compute minimum cost for all three operations and take
// minimum of three values.
    return 1 + min ( editDistanceRec2(str1, m,str2,  n-1), // Insert
                     editDistanceRec2(str1,  m-1,str2, n), // Remove
                     editDistanceRec2(str1, m-1,str2,  n-1)); // Replace
}

/////////////////////////////////////////////////////////////////

typedef struct EditDistMem {
    int d;
    int refcnt;//just for test
} EditDistMem;

//Memento based recursive version
int editDistanceMementoCore(EditDistMem** mem,const char *src, int m,const char* dest,int n) {
    if(mem[m][n].refcnt) {
        ++mem[m][n].refcnt;
        return mem[m][n].d;
    }
    int d=0;
    if(m==0||n==0) {
        d=std::max(m,n);
    } else if(src[0]==dest[0]) {
        d=editDistanceMementoCore(mem,src+1,m-1,dest+1,n-1);
    } else {
        //Add a char to source
        int ins=editDistanceMementoCore(mem,src,m,dest+1,n-1)+1;
        int del=editDistanceMementoCore(mem,src+1,m-1,dest,n)+1;
        //If use replace, compare next char then
        int rep=editDistanceMementoCore(mem,src+1,m-1,dest+1,n-1)+1;
        d= min(ins,del,rep);
    }
    ++mem[m][n].refcnt;
    mem[m][n].d=d;
    return d;
}

int editDistanceMemento(const char *src,int m, const char* dest,int n) {
    int d=0;
    EditDistMem** mem=malloc_Array2D<EditDistMem>(m+1,n+1);
    for(int i=0; i<=m; ++i) {
        for(int j=0; j<=n; ++j) {
            mem[i][j].d=0;
            mem[i][j].refcnt=0;
        }
    }
    d=editDistanceMementoCore(mem,src,m,dest,n);

    //Show memento
    for(int i=1,c=0; i<=m; ++i) {
        for(int j=1; j<=n; ++j) {
            if(mem[i][j].d) {
                ++c;
                printArray(src,i,printAsChar);
                printArray(dest,j,printAsChar);
                printf("[%d] d=%d,ref=%d\n",c,mem[i][j].d,mem[i][j].refcnt);
            }
        }

    }

    free_Array2D((void**)mem);
    return d;
}

//////////////////////////////////////////////////////////////

int editDistanceMementoCore2(int** mem,const char *src, int m,const char* dest,int n) {
    if(mem[m][n])
        return mem[m][n];
    int d=0;
    if(m==0||n==0) {
        d=std::max(m,n);
    } else if(src[0]==dest[0]) {
        d=editDistanceMementoCore2(mem,src+1,m-1,dest+1,n-1);
    } else {
        //Add a char to source
        int ins=editDistanceMementoCore2(mem,src,m,dest+1,n-1)+1;
        int del=editDistanceMementoCore2(mem,src+1,m-1,dest,n)+1;
        //If use replace, compare next char then
        int rep=editDistanceMementoCore2(mem,src+1,m-1,dest+1,n-1)+1;
        d= min(ins,del,rep);
    }
    mem[m][n]=d;
    return d;
}

int editDistanceMemento2(const char *src,int m,const char* dest,int n) {
    int d=0;
    int** mem=malloc_Array2D<int>(m+1,n+1);
    for(int i=0; i<=m; ++i)
        for(int j=0; j<=n; ++j)
            mem[i][j]=0;
    d=editDistanceMementoCore2(mem,src,m,dest,n);
    free_Array2D((void**)mem);
    return d;
}


///////////////////////////////////////////////////////////////////////
//!backtracing to output how to edit source to target
void backtracingForEditDistance(int** matrix,const char* src,int m,const char* dest,int n) {
    int size=std::max(m,n);
    char* p=(char*)calloc(size,sizeof(char));
    char* q=(char*)calloc(size,sizeof(char));
    int k=0;
    for(int i=m+1,j=n+1; i>0&&j>0; ++k) {
        if(src[i-1]==dest[j-1]) {
            p[k]=src[i-1];
            q[k]=dest[j-1];
            --i;
            --j;
        } else {
            int del=matrix[i-1][j];
            int ins=matrix[i][j-1];
            int rep=matrix[i-1][j-1];
//            printf("del:%d,ins:%d,rep:%d\n",del,ins,rep);
            if(del<=ins&&del<=rep) { //del
                p[k]=src[i-1];
                q[k]='-';
                --i;
//                printf("del\n");
            } else if(ins<=del&&ins<=rep) { //ins
                p[k]='-';
                q[k]=dest[j-1];
                --j;
//                printf("ins\n");
            } else { //rep
                p[k]=src[i-1];
                q[k]=dest[j-1];
                --i;
                --j;
//                printf("rep\n");
            }
        }
    }


    //Output
    for(int i=k-1; i>=0; --i) {
        printf("%c",p[i]);
    }
    printf("\n");
    for(int i=k-1; i>=0; --i) {
        printf("%c",q[i]);
    }
    printf("\n");


    free(p);
    free(q);

}


/**

!DP

������
���ȶ�������һ����������edit(i, j)��
����ʾ��һ���ַ����ĳ���Ϊi���Ӵ����ڶ����ַ����ĳ���Ϊj���Ӵ��ı༭���롣
��Ȼ���������¶�̬�滮��ʽ��
if i == 0 �� j == 0��edit(i, j) = 0
if i == 0 �� j > 0��edit(i, j) = j
if i > 0 ��j == 0��edit(i, j) = i
if i �� 1 ��j �� 1 ��edit(i,j)= min{edit(i-1,j)+1,edit(i,j-1)+1,edit(i-1,j-1)+f(i,j)},
����һ���ַ����ĵ�i���ַ������ڵڶ����ַ����ĵ�j���ַ�ʱ��f(i, j) = 1��
����f(i, j) = 0��


Time Complexity: O(m x n)
Auxiliary Space: O(m x n)

Applications:
There are many practical applications of edit distance algorithm,
refer Lucene API for sample. Another example, display all the words in a dictionary
that are near proximity to a given word\incorrectly spelled word.

Compare gene

**/
int editDistanceDP(const char *src,int m,const char* dest,int n) {
    // Create a table to store results of subproblems
    int** dp=malloc_Array2D<int>(m+1,n+1);

    // Fill d[][] in bottom up manner
    //i and j are length, not index, so they can be m and n
    for (int i=0; i<=m; ++i)  {
        for (int j=0; j<=n; ++j) {
            // If first string is empty, only option is to
            // insert all characters of second string
            if (i==0)
                dp[i][j] = j;  // Min. operations = j

            // If second string is empty, only option is to
            // remove all characters of second string
            else if (j==0)
                dp[i][j] = i; // Min. operations = i

            // If last characters are same, ignore last char
            // and recur for remaining string
            else if (src[i-1] == dest[j-1])
                dp[i][j] = dp[i-1][j-1];

            // If last character are different, consider all
            // possibilities and find minimum
            else
                dp[i][j] = 1 + min(dp[i][j-1],  // Insert
                                   dp[i-1][j],  // Remove
                                   dp[i-1][j-1]); // Replace
        }
    }


    //show details
    backtracingForEditDistance(dp,src,m,dest,n);

    int d=dp[m][n];
    free_Array2D((void**)dp);
    return d;

}

/**********************************************************************

Test

***********************************************************************/
typedef int(*LDFun)(const char *,int,const char*,int);
static void assertLD(LDFun func, const char *src,const char* dest,int expect) {
    int m=strlen(src);
    int n=strlen(dest);
    int d=func(src,m,dest,n);
    printf("%d\n",d);
    if(expect>=0)
        assert(d==expect);
}
static void assertLDs(const char *src,const char* dest,int expect) {
    assertLD(editDistanceRec,src,dest,expect);
    assertLD(editDistanceRec2,src,dest,expect);
    assertLD(editDistanceMemento,src,dest,expect);
    assertLD(editDistanceMemento2,src,dest,expect);
    assertLD(editDistanceDP,src,dest,expect);
}
void testEditDistance() {

    assertLDs("snowy","sunny",3);
    //Different length
    assertLDs("GGATCGA","GAATTCAGTTA",5);
    assertLDs("geek","gesek",1);
    //only replace case
    assertLDs("cut","cat",1);
    //only insert case
    assertLDs("sunday","saturday",3);

}
