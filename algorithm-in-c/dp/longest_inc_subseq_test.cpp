#include"longest_inc_subseq.h"
#include "../test_utils.h"

static void assertLIS(int* a,int n,int expect) {
    int len;
    printArray(a,n);

    //!TODO
    len=lisRec(a,n);
    printf("%d\n",len);
//    assert(expect==len);

    len = lisDP(a,n);
    printf("lisDP %d\n",len);
    assert(expect==len);


    int* inc=lisDP2(a,n,&len);
    printf("lisDP2 %d\n",len);
    printArray(inc,len);
    assert(expect==len);
    delete[] inc;

    len=lisFast(a,n);
    printf("%d\n",len);
    assert(expect==len);

    len=lisFast2(a,n);
    printf("%d\n",len);
    assert(expect==len);

    len=lisByLCS(a,n);
    printf("%d\n",len);
    assert(expect==len);

    len=lisByBiSearch(a,n);
    printf("%d\n",len);
    assert(expect==len);


    printf("---------------------------------------\n");
}

void testLongestIncDecSeq() {
    const int size=7;
    int a[][size]= {
        {1,5,3,4,2,7,8},
        //inc(5) 1,3,4,7,8
        //dec(3) 5,3,2
        //inc+dec=inc(5)
        {1,2,3,4,3,2,1},
        //inc+dec(7)
        {6,5,4,3,2,1,0},
        //inc+dec(7)
        {9,5,3,4,2,7,6},
        //inc(3) 3,4,7
        //dec(4) 9,5,3,2
        //inc+dec(4) 3,4,7,6
    };


    int m[size];
    for(int i=0; i<sizeof(a)/sizeof(a[0]); ++i)
        printf("len=%d\n",longestIncDecSeq(a[i],size));
//    printf("%d\n",MaxIncreaseSeq(a[0],size,m));
//    printf("%d\n",MaxDecreaseSeq(a[0],size,m));


//    printf("MaxIncreaseDecreaseSeqLen=%d\n",MaxIncreaseDecreaseSeq(a[1],size));


}


void testLIS() {
    int len;
    int arr[]= {3, 10, 2, 1, 20};
    int n=sizeof(arr)/sizeof(int);
    assertLIS(arr,n,3);


    int arr2[] = {10, 22, 9, 33, 21, 50, 41, 60};
    n = sizeof(arr2)/sizeof(arr2[0]);
    assertLIS(arr2,n,5);

    int arr3[]= { 2, 5, 3, 7, 11, 8, 10, 13, 6 };
    n = sizeof(arr3)/sizeof(arr3[0]);
    assertLIS(arr3,n,6);

    testLongestIncDecSeq();
}
