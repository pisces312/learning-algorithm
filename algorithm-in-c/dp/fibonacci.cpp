#include "../test_utils.h"
/***************************************************************

Fibonacci problem

Given n, find nth Fibonacci Number

f(n)=f(n-1)+f(n-2)
f(0)=0,f(1)=1

0,1,2,3,4,5,6, 7, 8, 9,10
0,1,1,2,3,5,8,13,21,34,55

Time taken by Recursion method is much more than the two Dynamic Programming techniques mentioned above - Memorization and Tabulation

***************************************************************/
/**

Recursive version

Recursion tree for execution of fib(5)

                         fib(5)
                     /             \
               fib(4)                fib(3)
             /      \                /     \
         fib(3)      fib(2)         fib(2)    fib(1)
        /     \        /    \       /    \
  fib(2)   fib(1)  fib(1) fib(0) fib(1) fib(0)
  /    \
fib(1) fib(0)


**/
long fibonacci(long n) {
    if(n<=0)
        return 0;
    if(n==1)
        return 1;
    return fibonacci(n-1)+fibonacci(n-2);
}

/**

Memorization version

**/
long fibMemo(long* lookup,long n) {
    if (lookup[n] == -1)
        if (n <= 0)
            lookup[n] = 0;
        else if (n == 1)
            lookup[n] = 1;
        else
            lookup[n] = fibMemo(lookup,n-1) + fibMemo(lookup,n-2);
    return lookup[n];
}

long fibMemo(long n) {
    if(n<=0) return 0;
    long* lookup=new long[n+1];
    for (int i = 0; i <= n; i++)
        lookup[i] = -1;
    int ret= fibMemo(lookup,n);
    delete[] lookup;
    return ret;
}

/**

Tabulated version

**/
long fibTab(long n) {
    if(n<0) return 0;
    int f[n+1];
    f[0] = 0;
    f[1] = 1;
    for (int i = 2; i <= n; i++)
        f[i] = f[i-1] + f[i-2];
    return f[n];
}

/**

Iteration version

**/
long fibonacci2(long n) {
    int pre1=1,pre2=0,f=0;
    if(n<=0) return 0;
    if(n==1) return 1;
    for(int i=2; i<=n; ++i) {
        f=pre1+pre2;
        pre2=pre1;
        pre1=f;
    }
    return f;
}

//!TODO matrix version

static void assertFib(int n, int e=-1) {
    printf("%d\n",n);
    long f;
    BEGIN_TIMING();
    f=fibonacci(n);
    STOP_TIMING();
    printf("fibonacci:%ld\n",f);
    if(e>=0) assert(f==e);

    BEGIN_TIMING();
    f=fibMemo(n);
    STOP_TIMING();
    printf("fibMemo:%ld\n",f);
    if(e>=0) assert(f==e);

    if(n>=0) {
        long lookup[n+1];
        for(int i=0; i<=n; ++i)
            lookup[i]=-1;
        BEGIN_TIMING();
        f=fibMemo(lookup,n);
        STOP_TIMING();
        printf("fibMemo2:%ld\n",f);
        if(e>=0) assert(f==e);
    }

    BEGIN_TIMING();
    f=fibTab(n);
    STOP_TIMING();
    printf("fibTab:%ld\n",f);
    if(e>=0) assert(f==e);

    BEGIN_TIMING();
    f=fibonacci2(n);
    STOP_TIMING();
    printf("fibonacci2:%ld\n",f);
    if(e>=0) assert(f==e);

    printf("-------------------------\n");
}

void testFibonacci() {
    assertFib(-1,0);
    assertFib(0,0);
    assertFib(1,1);
    assertFib(2,1);
    assertFib(3,2);
    assertFib(4,3);
    assertFib(10,55);
    assertFib(40);//test performance
}
