#include"../test_utils.h"
//#include<queue>
/**************************************************

0-1 Knapsack Problem

Given weights and values of n items, put these items in a knapsack of capacity W to get the maximum total value in the knapsack.

In other words, given two integer arrays val[0..n-1] and wt[0..n-1] which represent values and weights associated with n items respectively.

Also given an integer W which represents knapsack capacity, find out the maximum value subset of val[] such that sum of the weights of this subset is smaller than or equal to W.

You cannot break an item, either pick the complete item, or don't pick it (0-1 property).



http://www.geeksforgeeks.org/dynamic-programming-set-10-0-1-knapsack-problem/

**************************************************/

/**

!TODO

DP can only handle integer weight


A simple solution is to consider all subsets of items and calculate the total weight and value of all subsets. Consider the only subsets whose total weight is smaller than W. From all such subsets, pick the maximum value subset.

1) Optimal Substructure:
To consider all subsets of items, there can be two cases for every item:
 (1) the item is included in the optimal subset
 (2) not included in the optimal set.
Therefore, the maximum value that can be obtained from n items is max of following two values.
 (1) Maximum value obtained by n-1 items and W weight (excluding nth item).
 (2) Value of nth item plus maximum value obtained by n-1 items and W minus weight of the nth item (including nth item).

If weight of nth item is greater than W, then the nth item cannot be included and case 1 is the only possibility.

2) Overlapping Subproblems
Following is recursive implementation that simply follows the recursive structure mentioned above.



f[i,j]:表示在前i件物品中选择若干件放在承重为 j 的背包中，可以取得的最大价值。
!!前i指的不是结果中的前i，结果可能根本不包含待考察的前i件
    i is i th item
    j is the capacity left
f[i,j]=max {f[i-1,j-wt[i]]+val[i], f[i-1,j]}
f[0,w]=0
f[0,0]=0


Time complexity of this naive recursive solution is exponential (2^n).


In the following recursion tree, K() refers to knapSack().
The two parameters indicated in the following recursion tree are n and W.

The recursion tree is for following sample inputs.
wt[] = {1, 1, 1}, W = 2, val[] = {10, 20, 30}
It should be noted that the above function computes the same subproblems again and again.
K(1, 1) is being evaluated twice.

                       K(3, 2)         ---------> K(n, W)
                   /            \
                 /                \
            K(2,2)                  K(2,1)
          /       \                  /    \
        /           \              /        \
       K(1,2)      K(1,1)        K(1,1)     K(1,0)
       /  \         /   \          /   \
     /      \     /       \      /       \
K(0,2)  K(0,1)  K(0,1)  K(0,0)  K(0,1)   K(0,0)
Recursion tree for Knapsack capacity 2 units and 3 items of 1 unit weight.



From last to first, no need extra parameter for current element

n is the number of sacks

Returns the maximum value that can be put in a knapsack of capacity W

**/
int knapSack01a(int W, int wt[], int val[], int n) {
    //! Base Cases:
    //1. All items are put into knapsack.
    //2. Or no space to store more
    if(n == 0 || W == 0)
        return 0;

    // If weight of the nth item is more than Knapsack capacity W, then
    // this item cannot be included in the optimal solution
    if(wt[n-1] > W)
        return knapSack01a(W, wt, val, n-1);

    // Return the maximum of two cases:
    // (1) nth item included
    // (2) not included
    return std::max(val[n-1] + knapSack01a(W-wt[n-1], wt, val, n-1),
                    knapSack01a(W, wt, val, n-1));
}

/**
From first to last, need i to stand for current element
**/
int knapSack01b(int W, int wt[], int val[], const int n,int i=0) {
    if(i == n || W == 0)
        return 0;
    if(wt[i] > W)
        return knapSack01b(W, wt, val, n, i+1);
    return std::max(val[i] + knapSack01b(W-wt[i], wt, val, n, i+1),
                    knapSack01b(W, wt, val, n, i+1));
}


int knapSack01Memo(int** K, int W, int wt[], int val[], int n) {
    if(K[n][W]>=0) {
        printf("Lookup memo: K[%d][%d]=%d\n",n,W,K[n][W]);
        return K[n][W];
    }
    if(n == 0 || W == 0) {
        K[n][W]=0;
        return 0;
    }
    if(wt[n-1] > W) {
        if(K[n-1][W]>=0)
            printf("Lookup memo: K[%d][%d]=%d\n",n-1,W,K[n-1][W]);
        else
            K[n-1][W]= knapSack01Memo(K,W, wt, val, n-1);
        return K[n-1][W];
    }

    if(K[n-1][W-wt[n-1]]>=0)
        printf("Lookup memo: K[%d][%d]=%d\n",n-1,W-wt[n-1],K[n-1][W-wt[n-1]]);
    else
        K[n-1][W-wt[n-1]]=knapSack01Memo(K,W-wt[n-1], wt, val, n-1);


    if(K[n-1][W]>=0)
        printf("Lookup memo: K[%d][%d]=%d\n",n-1,W,K[n-1][W]);
    else
        K[n-1][W]=knapSack01Memo(K,W, wt, val, n-1);


    return std::max(val[n-1] + K[n-1][W-wt[n-1]],K[n-1][W]);
}

int knapSack01Memo(int W, int wt[], int val[], int n) {
    int** K=malloc_Array2D<int>(n+1,W+1,false);
    fillMatrix(K,n+1,W+1,-1);

    int maxVal=knapSack01Memo(K,W,wt,val,n);
// backtrackForKnapSack2()
    free(K);
    return maxVal;
}


/**
Time Complexity: O(nW) where n is the number of items and W is the capacity of knapsack.



有编号分别为a,b,c,d,e的五件物品，它们的重量分别是2,2,6,5,4，它们的价值分别是6,3,5,4,6，现在给你个承重为10的背包，如何让背包里装入的物品具有最大的价值总和？

     w      1   2   3   4   5   6   7   8   9   10
name v
a   2   6   0   6   6   9   9   12  12  15  15  15
b   2   3   0   3   3   6   6   9   9   9   10  11
c   6   5   0   0   0   6   6   6   6   6   10  11
d   5   4   0   0   0   6   6   6   6   6   10  10
e   4   6   0   0   0   6   6   6   6   6   6   6

物品考虑的顺序为edcba

用e2单元格表示e行2列的单元格，这个单元格的意义是用来表示只有物品e时，有个承重为2的背包，那么这个背包的最大价值是0，因为e物品的重量是4，背包装不了。

对于d2单元格，表示只有物品e，d时,承重为2的背包,所能装入的最大价值，仍然是0，因为物品e,d都不是这个背包能装的。

对于承重为8的背包，a8=15,是怎么得出的呢？
根据01背包的状态转换方程，需要考察两个值，
1. f[i-1,j]表示我有一个承重为8的背包，当只有物品b,c,d,e四件可选时，这个背包能装入的最大价值,对于这个例子来说就是b8的值9
2. f[i-1,j-Wi]表示我有一个承重为6的背包（等于当前背包承重减去物品a的重量），当只有物品b,c,d,e四件可选时，这个背包能装入的最大价值,指单元格b6,值为9

Pi指a物品的价值，即6。由于f[i-1,j-Wi]+Pi = 9 + 6 = 15 大于f[i-1,j] = 9，所以物品a应该放入承重为8的背包

http://blog.csdn.net/mu399/article/details/7722810

**/
static void backtrackForKnapSack1(int** K,int W, int wt[], int val[], int n) {
    for(int i=n,t=W; i>=1&&t>0; --i) //the number of item, not index
        if(K[i][t]-K[i-1][t-wt[i-1]]==val[i-1]) {
            printf("%d(w:%d,v:%d)\n",i,wt[i-1],val[i-1]);
            t-=wt[i-1];
        }
}

int knapSack01Table(int W, int wt[], int val[], int n) {
    //! No need fully initialization
//    int K[n+1][W+1];//Stack may not have enough space for 2D array
    int** K=malloc_Array2D<int>(n+1,W+1);
    // Build table K[][] in bottom up manner
    for(int i = 0; i <= n; i++)
        for(int w = 0; w <= W; w++)
            if(i==0 || w==0)
                K[i][w] = 0;
            else if(wt[i-1] <= w)
                //!not only rely on K[i-1][w], cannot use space opt
                K[i][w] = std::max(val[i-1] + K[i-1][w-wt[i-1]],  K[i-1][w]);
            else
                K[i][w] = K[i-1][w];

    //Backtracking
    backtrackForKnapSack1(K,W,wt,val,n);

    int maxVal=K[n][W];
    free(K);
    return maxVal;
}

static void assertKnapSack01(int* wt,int* val,int W,int n,const int e=-1) {
    int maxVal;
    maxVal=knapSack01a(W, wt, val, n);
    printf("knapSack01a: %d\n", maxVal);
    if(e>0) assert(maxVal==e);

    maxVal=knapSack01b(W, wt, val, n);
    printf("knapSack01b: %d\n", maxVal);
    if(e>0) assert(maxVal==e);

    maxVal=knapSack01Memo(W, wt, val, n);
    printf("knapSack01Memo: %d\n", maxVal);
    if(e>0) assert(maxVal==e);

    maxVal=knapSack01Table(W, wt, val, n);
    printf("knapSack01Table: %d\n", maxVal);
    if(e>0) assert(maxVal==e);

    printf("--------------------------------------\n");
}

void testKnapSack01() {
    int W,n;

    int val[] = {60, 100, 120};
    int wt[] = {10, 20, 30};
    W = 50;
    n = ARRAY_SIZE(val);
    assertKnapSack01(wt,val,W,n,220);


    int wt2[]= {2,2,6,5,4};
    int val2[]= {6,3,5,4,6};
    W=10;
    n = ARRAY_SIZE(val2);
    assertKnapSack01(wt2,val2,W,n,15);


    int wt3[] = {1, 1, 1};
    W = 2;
    int val3[] = {10, 20, 30};
    n = ARRAY_SIZE(val3);
    assertKnapSack01(wt3,val3,W,n,50);



}
