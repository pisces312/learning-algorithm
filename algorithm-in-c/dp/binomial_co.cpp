#include "../test_utils.h"

/**
C(n,k)=C(n-1,k-1)+C(n-1,k), n>k>=0
C(i,i)=C(i,0)=1, 1<=i<=n
**/

/*********************************************
Using 2D array

**********************************************/
//Calculate all coefficient
int** binomialCoefficientAll(int n,int k) {
    if(n<1||k>n) return NULL;
    int** c=malloc_Array2D<int>(n+1,k+1);

    //No C(0,0), so n starts from 1
    for(int i=1; i<=n; ++i)
        for(int j=0; j<=i&&j<=k; ++j)
            if(i==j||j==0) {
                c[i][j]=1;
                printf("C(%d,%d)=%d\n",i,j,c[i][j]);
            } else {
                c[i][j]=c[i-1][j-1]+c[i-1][j];
                printf("C(%d,%d)=C(%d,%d)+C(%d,%d)=%d\n",i,j,i-1,j-1,i-1,j,c[i][j]);
            }

    return c;
}

int binomialCoefficient(int n,int k) {
    int** c=binomialCoefficientAll(n,k);
    int cnk=c[n][k];
    free_Array2D((void**)c);
    return cnk;
}



/*********************************************
Using two 1D arrays

**********************************************/

int* binomialCoefficientAll2(int n,int k) {
    if(n<1||k>n) return NULL;
    int* c1=(int*)malloc((k+1)*sizeof(int));
    int* c2=(int*)malloc((k+1)*sizeof(int));
    int* c=c1;
    int* cPre=NULL;

    //Need to swap two arrays every iteration
    for(int i=1; i<=n; ++i,cPre=c,c=(cPre==c1?c2:c1))
        for(int j=0; j<=i&&j<=k; ++j)
            if(i==j||j==0) {
                c[j]=1;
                printf("C(%d,%d)=%d\n",i,j,c[j]);
            } else {
                c[j]=cPre[j-1]+cPre[j];
                printf("C(%d,%d)=C(%d,%d)+C(%d,%d)=%d\n",i,j,i-1,j-1,i-1,j,c[j]);
            }

    return c;
}

int binomialCoefficient2(int n,int k) {
    int* c=binomialCoefficientAll2(n,k);
    int cnk=c[k];
    free(c);
    return cnk;
}
//////////////////////////////////////////////////////////////

void tesetBinoCo2D() {
    int c;

    c=binomialCoefficient(3,1);
//    printf("%d\n",c);
    assert(c==3);

    c=binomialCoefficient(3,2);
//    printf("%d\n",c);
    assert(c==3);

    int n=10;
    for(int i=0; i<=n; ++i) {
        c=binomialCoefficient(n,i);
//        printf("%d ",c);
    }
//    printf("%d\n",c);

    printf("---------------------------\n");
}

void testBinoCo() {

    tesetBinoCo2D();

    binomialCoefficientAll2(10,6);


}
