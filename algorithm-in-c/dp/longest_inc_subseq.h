#ifndef LIS_H_INCLUDED
#define LIS_H_INCLUDED

//Basic recursive version
int lisRec(int* arr, int n);

//DP
//O(n^2) by searching index from [0,j],j<i
int lisDP(int* s,int n);
//backtracing increasing sequence
int* lisDP2(int* s,int n,int* len);
//O(n^2) by linear search from back to front
int lisByLinearSearch(int * L,int n);;
//O(nlogn) by binary search
int lisByBiSearch(int * L,int n) ;


//O(nlogn) algorithm by number characteristic
int lisFast(int* v, int n);
//Output increasing sequence
int lisFast2(int* v, int n);


int lisByLCS(int* v, int n);

//first inc then dec seq
int longestIncDecSeq(int* a,int size) ;

#endif
