#include "../test_utils.h"
/****************************************************************

Assembly Line Scheduling


问题描述

某个汽车工厂共有两条装配线,每条有 n 个装配站。
装配线 i 的第 j个装配站表示为 S(i,j) ,在该站的装配时间为 a(i,j) 。

一个汽车底盘进入工厂,然后进入装配线 i(i 为 1 或 2),花费时间为 ei 。
在通过一条线的第 j 个装配站后,这个底盘来到任一条装配线的第(j+1)个装配站。
如果它留在相同的装配线,则没有移动开销。
但是,如果它移动到另一条线上,则花费时间为 t(i,j) 。
在离开一条装配线的第 n 个装配站后,完成的汽车底盘花费时间为 xi 离开工厂。

待求解的问题是,确定应该在装配线 1 内选择哪些站,在装配线 2 内选择哪些站,
才能使汽车通过工厂的总时间最短。

A car factory has two assembly lines, each with n stations.
A station is denoted by Si,j where i is either 1 or 2
and indicates the assembly line the station is on,
and j indicates the number of the station.
The time taken per station is denoted by ai,j.
Each station is dedicated to some sort of work like engine fitting,
body fitting, painting and so on. So, a car chassis must pass through
each of the n stations in order before exiting the factory.
The parallel stations of the two assembly lines perform the same task.
After it passes through station Si,j, it will continue to station Si,j+1
unless it decides to transfer to the other line. Continuing on the same
line incurs no extra cost, but transferring from line i at station j–1
to station j on the other line takes time ti,j. Each assembly line takes
an entry time ei and exit time xi which may be different for the two lines.
Give an algorithm for computing the minimum time it will take to build a car chassis.


e[i]: Each assembly line takes an entry time
x[i]: Each assembly line takes an exit time
a[i][j]: The time taken per station
t[i][j]: transfer from line i at station j–1 to station j on the other line


S[1][j]=min{S[1][j-1]+a[1][j],t[1][j-1]+S[2][j-1]+a[2][j]}

S[2][j]=min{S[2][j-1]+a[2][j],t[2][j-1]+S[1][j-1]+a[1][j]}



http://www.cnblogs.com/lienhua34/p/4008344.html
http://www.geeksforgeeks.org/dynamic-programming-set-34-assembly-line-scheduling/

*****************************************************************/

void backtracingForAssemblyLine(int* t1,int *t2,int** a, int** t,int stationNum, int *x) {
    int* s=new int[2*stationNum];//save schedule
    int sp=0;

    //Special for last station because it considers exit time
    int tt= (t1[stationNum-1] + x[0]<=t2[stationNum-1] + x[1])?0:1;
    s[sp++]=tt;
    s[sp++]=stationNum-1;

    for(int i=stationNum-1; i>=1; s[sp++]=tt,s[sp++]=i-1,--i)
        if(tt==0)
            tt=(t1[i-1] + a[0][i]<=t2[i-1] + t[1][i] + a[0][i])?0:1;
        else
            tt=(t2[i-1] + a[1][i]<=t1[i-1] + t[0][i] + a[1][i])?1:0;

    //Output
    for(int i=sp-1; i>=0; i-=2)
        printf("station(%d,%d)\n",s[i-1],s[i]);

    delete[] s;
}

//Only two lines, it introduces two DP equations
int carAssembly(int** a, int** t,int stationNum, int *e, int *x) {
    int* t1=new int[stationNum];
    int* t2=new int[stationNum];

    t1[0] = e[0] + a[0][0]; // time taken to leave first station in line 1
    t2[0] = e[1] + a[1][0]; // time taken to leave first station in line 2

    // Fill tables T1[] and T2[] using the above given recursive relations
    for(int i = 1; i < stationNum; ++i) {
        t1[i] = std::min(t1[i-1] + a[0][i], t2[i-1] + t[1][i] + a[0][i]);
        t2[i] = std::min(t2[i-1] + a[1][i], t1[i-1] + t[0][i] + a[1][i]);
    }

    // Consider exit times and retutn minimum
    int tmin= std::min(t1[stationNum-1] + x[0], t2[stationNum-1] + x[1]);

    backtracingForAssemblyLine(t1,t2,a,t,stationNum,x);

    delete[] t1;
    delete[] t2;
    return tmin;
}

/***************************************************************

10                          18
--->(4)--->(5)-->(3)--->(2)---->
     \     /\     /\     /
     7\   / 4\   / 5\   /
       \ /    \ /    \ /
        \      /      /
       / \    / \    / \
     9/   \ 2/   \ 8/   \
12   /     \/     \/     \  7
--->(2)-->(10)-->(1)--->(4)---->

***************************************************************/
void testAssemblyLineScheduling() {
    int NUM_STATION=4;
//    int lineNum=2;
    int line1[]= {4, 5, 3, 2};
    int line2[]= {2, 10, 1, 4};
    int t1[]= {0, 7, 4, 5};
    int t2[]= {0, 9, 2, 8};
    int* a[]= {line1,line2};
    int* t[]= {t1,t2};
    int e[] = {10, 12};
    int x[] = {18, 7};
    int time=carAssembly((int**)a, (int**)t,NUM_STATION, e, x);
    printf("%d\n", time);
    assert(time==35);

}
