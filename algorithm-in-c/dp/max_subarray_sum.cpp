#include "max_subarray_sum.h"
#include "../common.h"



/*********************************************************

[DP] Find the max value of continuous subsequence

s[i]=max{s[i-1]+a[i],a[i]}


Note:
!Need to consider whether supports negative



http://blog.csdn.net/qiaoruozhuo/article/details/65630389

**********************************************************/



/**
Brute force

For all kinds of number including negative number

Time: O(N^3)
Space: O(1)

**/
int maxSubArrayBF(int *A,int n,int* start,int* end) {
    int maxSum=INT_MIN;
    for (int i=0; i<n; i++) //start index of subarray
        for (int j=i; j<n; j++) {//end index of subarray
            int sum=A[i];
            for (int k=i+1; k<=j; k++) //length of subarray
                sum+=A[k];
            if (sum>maxSum) {
                maxSum=sum;
                *start=i;
                *end=j;
            }
        }
    return maxSum;
}

/**

Time: O(N^2)
Space: O(1)

**/
int maxSubArrayBF2(int *A,int n,int* start,int* end) {
    int max=INT_MIN;
    for (int i=0; i<n; i++) {
        int sum=0;
        for (int j=i; j<n; j++) {//calculate sum[i,i],sum[i,i+1],...,sum[i,n-1]
            sum+=A[j];//add current number directly
            if (sum>max) {
                max=sum;
                //记录起止
                *start=i;
                *end=j;
            }
        }
    }
    return max;
}



/**
Use partial sum array

O(n^2)
**/
int maxSubArrayByPatialSum(int* x,int n) {
    //cumarr[i] stands for the sum of [0]~[i], arrayLen is i+1
    int* cumvec=new int[n+1];//! [-1] as sentinel, request n+1 mem
    int *cumarr=cumvec+1;//! make cumarr[-1] legal
    cumarr[-1] = 0;//! access -1 position

    int maxsofar = INT_MIN;

    //since sentinel is used, i starts from 0
    for (int i = 0; i < n; i++)
        cumarr[i] = cumarr[i-1] + x[i];

    for (int i = 0; i < n; i++)
        for (int j = i; j < n; j++) {
            //!Substract the first [0,...,i-1] elements of [0,...j], i<=j<n
            //the result is [i,j],i<=j<n
            int sum = cumarr[j] - cumarr[i-1];
            if (sum > maxsofar)
                maxsofar = sum;
        }

    delete[] cumvec;
    return maxsofar;
}


/**

!Tabulation version

Time: O(n)
Space: O(n)

**/
int maxSubArrayDP1(int* a,int n, int* outputLeft, int* outputRight) {
    //1) Calculate all s[i]
    //s[i] stores the sum of sub-array which ends with A[i]
    //s[i]=A[k]+...+A[i], 0<=k<i
    int* s=(int*)calloc(n,sizeof(int));
    s[0]=a[0];
    for(int i=1; i<n; ++i)
        //! "<" or "<=" will lead same sum but different subarray
        if(s[i-1]<0)
            s[i]=a[i];
        else
            s[i]=s[i-1]+a[i];

    //2) Find max value and right border
    int right=0;
    for(int i=1; i<n; ++i)
        if(s[i]>s[right])
            right=i;

//3)[Optional]Find left border separately

//Way 1
//    int left=0;
//    for(int i=right-1; i>=0; --i)
//        //If "=", the sequence may contain less elements
//        //will become shorter with same max value
//        if(s[i]<0) {
////        if(s[i]<=0) {
//            left=i+1;
//            break;
//        }

//Way 2
    int left = right;
    while(left > 0 && s[left-1] > 0)
        --left;

    //Set result
    *outputLeft=left;
    *outputRight=right;

    int sum=s[right];
    free(s);
    return sum;
}

//Find left border when calculating s[i]
int maxSubArrayDP2(int* a,int n, int* outputLeft, int* outputRight) {
    int* s=(int*)calloc(n,sizeof(int));
    s[0]=a[0];
    int max=s[0];
    int left=0;
    int right=0;
    for(int i=1; i<n; ++i)
        if(s[i-1]<0) {
            s[i]=a[i];
            if(s[i]>max) {
                max=s[i];
                left=i;
                right=i;
            }
        } else {
            s[i]=s[i-1]+a[i];
            if(s[i]>max) {
                max=s[i];
                right=i;
            }
        }

    //Set result
    *outputLeft=left;
    *outputRight=right;

    free(s);
    return max;
}

/**
Space: O(1)
Use only one variable to store state
have to record borders at the same time
**/
int maxSubArrayDP3(int* a,int n, int* outputLeft, int* outputRight) {
    int s=a[0];
    int max=s,left=0,right=0;
    for(int i=1; i<n; ++i)
        if(s<0) {
            s=a[i];
            if(s>max) {
                max=s;
                left=i;
                right=i;
            }
        } else {
            s+=a[i];
            if(s>max) {
                max=s;
                right=i;
            }
        }

    //Set result
    *outputLeft=left;
    *outputRight=right;
    return max;
}




/*****************************************************

Another DP solution

Time: O(n)
Space: O(n)

Assume the max subarray is A[i]...A[j]
1. 0=i=j,即元素A[0]本身构成最大一段
2. 0=i<j，即最大段以A[0]开始
3. 0<i，A[0]和最大段无关系

all[i]:
max sum within A[i,...,n-1], may not start with A[i]
not mean the range is from i to n-1, may part of them

start[i]:
max sum within A[i,...,n-1], must start with A[i]
the end element may not be n-1


max{A[i],A[i]+Start[i+1],All[i]}

******************************************************/
int maxSubArrayDP4(int *A,int n) {
    int* start=new int[n];
    int* all=new int[n];
    start[n-1]=A[n-1];//{n-1}, one element
    all[n-1]=A[n-1];//{n-1}
    for (int i=n-2; i>=0; i--) { //from back to front
        start[i]=std::max(A[i],A[i]+start[i+1]);
        all[i]=std::max(start[i],all[i+1]);
    }
    int sum=all[0];//the max subarray within [0,...,n-1]
    delete[] start;
    delete[] all;
    return sum;
}

/**
!Optimize space

Time: O(n)
Space: O(1)

**/
int maxSubArrayDP5(int *A,int n) {
    int start=A[n-1];
    int all=A[n-1];
    for (int i=n-2; i>=0; i--) {
        start=std::max(A[i],A[i]+start);
        all=std::max(start,all);
    }
    return all;
}


int maxSubArrayDP6(int *A,int n,int *beginIndex,int *endIndex) {
    int start=A[n-1];
    int all=A[n-1];
    *beginIndex=*endIndex=n-1;
    for (int i=n-2; i>=0; i--) {
        if (A[i]>=A[i]+start) {
            *beginIndex=*endIndex=i;
            start=A[i];
        } else
            start+=A[i];
        if (start>all) {
            all=start;
            *beginIndex=i;
        }
    }
    return all;
}

/********************************************************

Divide and conquer

分成大小相等的两段，即两个子问题来求解
1 左半部分 A[0],...,A[n/2]中的最大值
2 右半部分 A[n/2+1],...,A[n-1]中的最大值
3 跨界 左半部分以A[n/2]结尾，右半部分以A[n/2+1]开始

递归
O(N*log2N)


*********************************************************/
int _maxSubArrayDC(int* A, int left, int right) {
    int maxLeftSum, maxRightSum;
    int maxLeftBorderSum, maxRightBorderSum;
    int leftBorderSum, rightBorderSum;
    int mid, i;

    if(left == right)
        return (A[left] > 0) ? A[left] : 0;

    mid = (left + right) / 2;
    maxLeftSum = _maxSubArrayDC(A, left, mid); //递归计算左半部子序列最大和
    maxRightSum = _maxSubArrayDC(A, mid+1, right);//递归计算右半部子序列最大和

    maxLeftBorderSum = leftBorderSum = 0;
    for(i=mid; i>=left; i--) { //从中间开始向左计算包含A[mid]子序列的最大和
        leftBorderSum += A[i];
        if(leftBorderSum > maxLeftBorderSum)
            maxLeftBorderSum = leftBorderSum;
    }

    maxRightBorderSum = rightBorderSum = 0;
    for(i=mid+1; i<=right; i++) {  //从中间开始向右计算A[mid+1]子序列的最大和
        rightBorderSum += A[i];
        if(rightBorderSum > maxRightBorderSum)
            maxRightBorderSum = rightBorderSum;
    }

    return max(maxLeftSum, maxRightSum, maxLeftBorderSum+maxRightBorderSum);
}

int maxSubArrayDC(int* x, int n) { //分治算法
    return _maxSubArrayDC(x, 0, n-1);
}

int _maxSubArrayDC2(int* x,int l, int u) {
    if (l > u)  /* zero elements */
        return INT_MIN;
    if (l == u)  /* one element */
        return x[l];
    //下面保证有两个元素
    int i;
    int sum;
    int m = (l+u) / 2;
    //m一定在[l,u]之内
    /* find max crossing to left */
    int lmax = sum = x[m];
    //从一半处往前找，即左侧
    for (i = m-1; i >= l; i--) {
        sum += x[i];
        if (sum > lmax)
            lmax = sum;
    }
    /* find max crossing to right */
    int rmax = sum = x[m+1];
    //从一半处往后找，即右侧
    for (i = m+2; i <= u; i++) {
        sum += x[i];
        if (sum > rmax)
            rmax = sum;
    }
    if(lmax<0||rmax<0)
        sum=std::max(lmax,rmax);
    else
        sum=lmax+rmax;
    return std::max(sum,
                    std::max(_maxSubArrayDC2(x,l, m),
                             _maxSubArrayDC2(x,m+1, u)));
}
int maxSubArrayDC2(int* x,int n) {
    return _maxSubArrayDC2(x,0,n-1);
}



//range
int _maxSubArrayDC3(int* x,int l, int u,int* b,int* e) {
    if (l > u)  /* zero elements */
        return INT_MIN;
    if (l == u) { /* one element */
        *b=*e=u;
        return x[u];
    }
    int m = (l+u) / 2;

    int i;
    int sum;
    int a1=m,a2=m+1;
    /* find max crossing to left */
    int lmax = sum = x[m];
    //从一半处往前找，即左侧
    for (i = m-1; i >= l; i--) {
        sum += x[i];
        if (sum > lmax) {
            a1=i;
            lmax = sum;
        }
    }
    /* find max crossing to right */
    int rmax = sum = x[m+1];
    //从一半处往后找，即右侧
    for (i = m+2; i <= u; i++) {
        sum += x[i];
        if (sum > rmax) {
            a2=i;
            rmax = sum;
        }
    }
    if(lmax<0||rmax<0) {
        if(lmax>=rmax) {
            sum=lmax;
            a2=m;
        } else {
            sum=rmax;
            a1=m+1;
        }
    } else {
        sum=lmax+rmax;
    }
    int b1,e1,b2,e2;
    lmax=_maxSubArrayDC3(x,l, m,&b1,&e1);
    rmax=_maxSubArrayDC3(x,m+1, u,&b2,&e2);
    if(lmax>=rmax) {
        if(sum<=lmax) {
            *b=b1;
            *e=e1;
            return lmax;
        }

    } else {
        if(sum<=rmax) {
            *b=b2;
            *e=e2;
            return rmax;
        }
    }
    *b=a1;
    *e=a2;
    return sum;
}
int maxSubArrayDC3(int *A,int n,int* maxSubArrayStart,int* maxSubArrayEnd) {
    return _maxSubArrayDC3(A,0,n-1,maxSubArrayStart,maxSubArrayEnd);
}


/**
Find the closest subarray which close to a specified num

**/
int findClosestSubArrayByNum(const int expect,int *x,int n,int *beginIndex,int *endIndex) {
    int d = INT_MAX, result=0;
    *beginIndex=*endIndex=-1;

    int* cumvec=new int[n+1];
    int* cumarr=cumvec+1;
    cumarr[-1] = 0;
    for (int i = 0; i < n; i++)//计算部分和
        cumarr[i] = cumarr[i-1] + x[i];

    for (int i = 0; i < n; i++)
        for (int j = i; j < n; j++) {
            int sum = cumarr[j] - cumarr[i-1];
            int temp=std::abs(sum -expect);
            if (temp<d) {
                d=temp;
                result=sum;
                *beginIndex=i;
                *endIndex=j;
            }
        }

    delete[] cumvec;
    return result;
}

/**
Find a subarray with specified length and  sum

**/
int findClosestSubArrayByNumAndLen(const int expect,const int len,int *x,int n,int *beginIndex,int *endIndex) {
    if(len>n||len<1||n<=0)
        return INT_MIN;//int_min means error

    int* cumvec=new int[n+1];
    int *cumarr=cumvec+1;
    cumarr[-1] = 0;
    for (int i = 0; i < n; i++)//计算部分和
        cumarr[i] = cumarr[i-1] + x[i];

    int d = INT_MAX,res=x[0];
    *beginIndex=*endIndex=-1;
    for (int i = 0; i < n; i++) {
        //! just use length
        int sum = cumarr[i+len-1] - cumarr[i-1];
        int temp=std::abs(sum -expect);
        if (temp<d) {
            res=sum;
            d=temp;
            *beginIndex=i;
            *endIndex=i+len-1;
        }
    }
    delete[] cumvec;
    return res;
}




/**********************************************************************

!!循环数组的情况，即允许首尾连接
采用空间换时间的方式
复制一份数组，接在原数组后面即可!!!
!!只允许跨一次，即两份数组，否则可能无限循环！！！

e.g.
0,-2,3,5,-1,2
0,-2,[3,5,-1,2,0,-2,3,5,-1,2]
16

************************************************************************/
int maxSubArrayRing(int *A,int n) {
    int size=(n<<1);
    int *ringArray=new int[size];
    memcpy(ringArray,A,n*sizeof(int));
    memcpy(ringArray+n,ringArray,n*sizeof(int));
    int sum=maxSubArrayDP5(ringArray,size);
    delete[] ringArray;
    return sum;
}
/**
循环数组

时间复杂度O(N)+O(N)=O(N)
**/
int maxSubArrayRingByDC(int *A,int n) {

    int sum=A[0],sum2=A[n-1];
    int max1=sum,max2=sum2;
    for (int i=1,j=n-2; i<n; i++,j--) {
        sum+=A[i],sum2+=A[j];
        if (sum>max1)
            max1=sum;
        if (sum2>max2)
            max2=sum2;
    }

    //0~n-1，与原问题比较
    return std::max(max2+max1,maxSubArrayDP5(A,n));
}


