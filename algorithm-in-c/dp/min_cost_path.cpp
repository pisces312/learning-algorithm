#include"../test_utils.h"
/*********************************************************

Min cost path (MCP)

Given a cost matrix cost[][] and a position (m, n) in cost[][], write a function that returns cost of minimum cost path to reach (m, n) from (0, 0). Each cell of the matrix represents a cost to traverse through that cell. Total cost of a path to reach (m, n) is sum of all the costs on that path (including both source and destination).

You can only traverse down, right and diagonally lower cells from a given cell, i.e., from a given cell (i, j), cells (i+1, j), (i, j+1) and (i+1, j+1) can be traversed.
(!只能向下，向右，斜对角线三个方向移动)

You may assume that all costs are positive integers.



http://www.geeksforgeeks.org/dynamic-programming-set-6-min-cost-path/

**********************************************************/

/**

Recursive version

mC refers to minCost()
                                    mC(2, 2)
                          /            |           \
                         /             |            \
                 mC(1, 1)           mC(1, 2)             mC(2, 1)
              /     |     \       /     |     \           /     |     \
             /      |      \     /      |      \         /      |       \
       mC(0,0) mC(0,1) mC(1,0) mC(0,1) mC(0,2) mC(1,1) mC(1,0) mC(1,1) mC(2,0)


From dest to src
i: index of row
j: index of col

**/
int minCostPathRec(int** cost, int i,int j) {
    if(i<0||j<0)
        return INT_MAX;
    if(i==0&&j==0) //!the start point
        return cost[i][j];
    return cost[i][j]+min(minCostPathRec(cost,i,j-1),
                          minCostPathRec(cost,i-1,j),
                          minCostPathRec(cost,i-1,j-1));
}

int minCostPathDPMemeo(int** cost, int i,int j,int** tc) {
    if(i<0||j<0)
        return INT_MAX;
    if(tc[i][j]>=0)
        return tc[i][j];
    if(i==0&&j==0) {
        tc[i][j]=cost[i][j];
        return tc[i][j];
    }

    if(tc[i][j-1]<0)
        tc[i][j-1]=minCostPathRec(cost,i,j-1);
    if(tc[i-1][j]<0)
        tc[i-1][j]=minCostPathRec(cost,i-1,j);
    if(tc[i-1][j-1]<0)
        tc[i-1][j-1]=minCostPathRec(cost,i-1,j-1);

    return cost[i][j]+min(tc[i][j-1],tc[i-1][j],tc[i-1][j-1]);
}

/**

1) Optimal Substructure
The path to reach (m, n) must be through one of the 3 cells: (m-1, n-1) or (m-1, n) or (m, n-1). So minimum cost to reach (m, n) can be written as “minimum of the 3 cells plus cost[m][n]”.
minCost(m, n) = min (minCost(m-1, n-1), minCost(m-1, n), minCost(m, n-1)) + cost[m][n]

2) Overlapping Subproblems

Time: O(mn), O(n^2)

**/
int minCostPathDPTable(int** cost,const int m,const int n) {
    int** tc=malloc_Array2D<int>(m,n,false);//!no need to initialize

    tc[0][0]=cost[0][0];//!handle start point
    for(int i=1; i<m; ++i)//!handle first col
        tc[i][0]=tc[i-1][0]+cost[i][0];
    for(int i=1; i<n; ++i)//!handle first row
        tc[0][i]=tc[0][i-1]+cost[0][i];

    for(int i=1; i<m; ++i)
        for(int j=1; j<n; ++j)
            tc[i][j]=cost[i][j]+min(tc[i-1][j-1],tc[i][j-1],tc[i-1][j]);

    int minCost=tc[m-1][n-1];
    free(tc);
    return minCost;

}

void testMinCostPath() {
    int m=3,n=3;
    int c1[3][3] = { {1, 2, 3},
        {4, 8, 2},
        {1, 5, 3}
    };
    int** cost=createMatrix(m,n,c1);
    printMatrix(cost,m,n);

    printf(" %d ", minCostPathRec(cost, m-1,n-1));


    printf(" %d ", minCostPathDPTable(cost, m,n));

    int** tc=malloc_Array2D<int>(m,n,false);
    fillMatrix(tc,m,n,-1);
    printf(" %d ", minCostPathDPMemeo(cost, m-1,n-1,tc));
}
