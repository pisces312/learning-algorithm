#ifndef MAX_SEQ_SUM_H_INCLUDED
#define MAX_SEQ_SUM_H_INCLUDED
#include<vector>

//Brute force
int maxSubArrayBF(int *A,int n,int* start,int* end);
int maxSubArrayBF2(int *A,int n,int* start,int* end);
int maxSubArrayByPatialSum(int* x,int n);

//Divide and conquer
int maxSubArrayDC(int* a,int n);//not for negative
int maxSubArrayDC2(int* x,int n);
int maxSubArrayDC3(int* x,int n,int* outputLeft, int* outputRight);

//DP alg
int maxSubArrayDP1(int* a,int n, int* outputLeft, int* outputRight);
int maxSubArrayDP2(int* a,int n, int* outputLeft, int* outputRight);
int maxSubArrayDP3(int* a,int n, int* outputLeft, int* outputRight);

//DP alg 2,support negative
int maxSubArrayDP4(int *A,int n);
int maxSubArrayDP5(int *A,int n);
int maxSubArrayDP6(int *A,int n,int* outputLeft, int* outputRight);

//Extension
//寻找接近某一给定实数t的子数组之和
int findClosestSubArrayByNum(const int expect,int *x,int n,int *beginIndex,int *endIndex) ;
//给定m，数组x和其长度n，找到使x[i]+...+x[i+m]最接近t的i,即子数组长度为m+1
int findClosestSubArrayByNumAndLen(const int expect,const int len,int *x,int n,int *beginIndex,int *endIndex);

//Ring
int maxSubArrayRing(int *A,int n);
int maxSubArrayRingByDC(int *A,int n);


/*******************************************************************

Template version

*******************************************************************/

template<typename T>
T maxSubsequenceSum1(const std::vector<T> &a,int & seqStart,int & seqEnd) {
    int n=(int)a.size();
    T maxSum=0;
    int i,j,k;
    for(i=0; i<n; i++)
        for(j=i; j<n; j++) {
            T thisSum=0;
            for(k=i; k<=j; k++)
                thisSum+=a[k];
            if(thisSum>maxSum) {
                maxSum=thisSum;
                seqStart=i;
                seqEnd=j;
            }
        }
    return maxSum;
}
template<typename T>
T maxSubsequenceSum2(const std::vector<T> &a,int& seqStart,int& seqEnd) {
    int n=(int)a.size();
    T maxSum=0;
    for(int i=0; i<n; i++) {
        T thisSum=0;
        for(int j=i; j<n; j++) {
            thisSum+=a[j];
            if(thisSum>maxSum) {
                maxSum=thisSum;
                seqStart=i;
                seqEnd=j;
            }
        }
    }
    return maxSum;
}
template<typename T>
T maxSubsequenceSum3(const std::vector<T> &a,int& seqStart,int& seqEnd) {
    int n=(int)a.size();
    T maxSum=0,thisSum=0;
    for(int i=0,j=0; j<n; j++) {
        thisSum+=a[j];
        if(thisSum>maxSum) {
            maxSum=thisSum;
            seqStart=i;
            seqEnd=j;
        } else if(thisSum<0) {
            i=j+1;
            thisSum=0;
        }
    }
    return maxSum;
}
#endif // MAX_SEQ_SUM_H_INCLUDED
