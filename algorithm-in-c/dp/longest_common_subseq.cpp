#include"../test_utils.h"
#include"longest_common_subseq.h"
/*****************************************************

Longest common subsequence (LCS)

LCS Problem Statement:
Given two sequences, find the length of longest subsequence present in both of them.
A subsequence is a sequence that appears in the same relative order, but not necessarily
contiguous. For example, "abc", "abg", "bdf", "aeg", "acefg", .. etc are subsequences
of "abcdefg". So a string of length n has 2^n different possible subsequences.
It is a classic computer science problem, the basis of diff (a file comparison program
that outputs the differences between two files), and has applications in bioinformatics.

Examples:
LCS for input Sequences "ABCDGH" and "AEDFHR" is "ADH" of length 3.
LCS for input Sequences "AGGTAB" and "GXTXAYB" is "GTAB" of length 4.


http://www.geeksforgeeks.org/dynamic-programming-set-4-longest-common-subsequence/
http://www.geeksforgeeks.org/printing-longest-common-subsequence/
http://www.algorithmist.com/index.php/Longest_Common_Subsequence
http://www.cnblogs.com/huangxincheng/archive/2012/11/11/2764625.html

******************************************************/

/**

Naive Recursive implementation (from last to first)

Time complexity is O(2^n) in worst case and worst case happens when all characters of X and Y mismatch
i.e., length of LCS is 0.

Considering the above implementation, following is a partial recursion tree for input strings ��AXYT�� and ��AYZX��
                         lcs("AXYT", "AYZX")
                       /                 \
         lcs("AXY", "AYZX")            lcs("AXYT", "AYZ")
         /            \                  /               \
lcs("AX", "AYZX") lcs("AXY", "AYZ")   lcs("AXY", "AYZ") lcs("AXYT", "AY")

In the above partial recursion tree, lcs(��AXY��, ��AYZ��) is being solved twice.
If we draw the complete recursion tree, then we can see that there are many
subproblems which are solved again and again. So this problem has Overlapping
Substructure property and recomputation of same subproblems can be avoided by
either using Memorization or Tabulation.

**/
int lcsRec(const char* X,int m,const char* Y,int n) {
    if (m == 0 || n == 0)
        return 0;
    if (X[m-1] == Y[n-1])
        return 1 + lcsRec(X, m-1,Y,  n-1);
    return std::max(lcsRec(X, m,Y,  n-1), lcsRec(X, m-1,Y,  n));
}


//TODO from first to last, recursive



/**

Memo version

**/
static int lcsDPMemo(int** f, const char* X,int m,const char* Y,int n) {
    if(f[m][n]==-1)
        if(m==0||n==0)
            f[m][n]=0;
        else {
            if (X[m-1] == Y[n-1])
                f[m][n]= 1 + lcsDPMemo(f,X, m-1,Y,  n-1);
            else
                f[m][n]= std::max(lcsDPMemo(f,X, m,Y,  n-1), lcsDPMemo(f,X, m-1,Y,  n));
        }
    return f[m][n];
}
int lcsDPMemo(const char* X,int m,const char* Y,int n) {
    int** f=malloc_Array2D<int>(m+1,n+1);
    for(int i=0; i<=m; ++i)
        for(int j=0; j<=n; ++j)
            f[i][j]=-1;
    int ret=lcsDPMemo(f,X,m,Y,n);
    free_Array2D<int>(f);
    return ret;
}




/**

Printing Longest Common Subsequence

Given two sequences, print the longest subsequence present in both of them.

Traverse the 2D array starting from L[m][n]. Do following for every cell L[i][j]
a) If characters (in X and Y) corresponding to L[i][j] are same (Or X[i-1] == Y[j-1]),
then include this character as part of LCS.
b) Else compare values of L[i-1][j] and L[i][j-1] and go in direction of greater value.

The following table (taken from Wiki) shows steps (highlighted) followed by the above algorithm.


		0	1	2	3	4	5	6	7
		?	M	Z	J	A	W	X	U
0	?	0	0	0	0	0	0	0	0
1	X	0	0	0	0	0	0	1	1
2	M	0	1	1	1	1	1	1	1
3	J	0	1	1	2	2	2	2	2
4	Y	0	1	1	2	2	2	2	2
5	A	0	1	1	2	3	3	3	3
6	U	0	1	1	2	3	3	3	4
7	Z	0	1	2	2	3	3	3	4




Backtracing from bottom up, right left
(Can draw line from matrix)
Have to use 2D array, not optimized space

O(m+n)

**/
void backtracingForLCS(int** c,const char* x,int m,const char* y,int n) {
    char* a=(char*)calloc(std::max(m,n),sizeof(char));
    int k=0;
    for(int i=m,j=n; i>0&&j>0;)
        if(x[i-1]==y[j-1]) {//Go left up
            a[k]=x[i-1];//Set only if equal
            --i;
            --j;
            ++k;
        } else {
            if(c[i-1][j]>=c[i][j-1]) //Go up
                --i;
            else//Go left
                --j;
        }

    for(int i=k-1; i>=0; --i)
        printf("%c",a[i]);
    printf("\n");
}


/**
1) Optimal Substructure:
Let the input sequences be X[0..m-1] and Y[0..n-1] of lengths m and n respectively.
And let L(X[0..m-1], Y[0..n-1]) be the length of LCS of the two sequences X and Y.

Following is the recursive definition of L(X[0..m-1], Y[0..n-1]).
If last characters of both sequences match (or X[m-1] == Y[n-1]) then
L(X[0..m-1], Y[0..n-1]) = 1 + L(X[0..m-2], Y[0..n-2])
If last characters of both sequences do not match (or X[m-1] != Y[n-1]) then
L(X[0..m-1], Y[0..n-1]) = MAX ( L(X[0..m-2], Y[0..n-1]), L(X[0..m-1], Y[0..n-2])

(c[i][j]=
if xi==yj, c[i-1][j-1]+1
if xi!=yj, max(c[i-1][j],c[i][j-1])
c[i][0]=0,c[0][j]=0)


Examples:
1) Consider the input strings ��AGGTAB�� and ��GXTXAYB��. Last characters match for the strings.
So length of LCS can be written as:
L(��AGGTAB��, ��GXTXAYB��) = 1 + L(��AGGTA��, ��GXTXAY��)

2) Consider the input strings ��ABCDGH�� and ��AEDFHR. Last characters do not match for the strings.
So length of LCS can be written as:
L(��ABCDGH��, ��AEDFHR��) = MAX ( L(��ABCDG��, ��AEDFHR��), L(��ABCDGH��, ��AEDFH��) )
So the LCS problem has optimal substructure property as the main problem can be solved
using solutions to subproblems.

2) Overlapping Subproblems:
Following is simple recursive implementation of the LCS problem. The implementation simply
follows the recursive structure mentioned above.



ʱ�临�Ӷȣ�
�����������ǻ�����O(MN)��ʱ�䣬����ʱ���ǻ�����O��M+N)��ʱ�䣬
��������������ǻ�����O(MN)��ʱ�䡣
Time Complexity of the above implementation is O(mn)
which is much better than the worst case time complexity of Naive Recursive implementation.


�ռ临�Ӷȣ�
�����������ǻ�����O(MN)�Ŀռ䣬��Ǻ���Ҳ������O(MN)�Ŀռ䣬
��������������ǻ�����O(MN)�Ŀռ䡣
**/
int lcsDPTab(const char* x,int m,const char* y,int n) {
//    int c[m+1][n+1];
    int** c=malloc_Array2D<int>(m+1,n+1);

    //Initial border
    for(int i=0; i<=m; ++i)
        c[i][0]=0;
    for(int j=0; j<=n; ++j)
        c[0][j]=0;

    for(int i=1; i<=m; ++i)
        for(int j=1; j<=n; ++j)
            if(x[i-1]==y[j-1])//compare current char
                c[i][j]=c[i-1][j-1]+1;
            else
                c[i][j]=std::max(c[i-1][j],c[i][j-1]);

    //Output common part
    backtracingForLCS(c,x,m,y,n);

    int d=c[m][n];

    free_Array2D((void**)c);
    return d;
}


static void assertLCS(const char* x,const char*y,int e=-1) {
    int len;
    int m=strlen(x);
    int n=strlen(y);

    printf("LCS of %s, %s\n",x,y);

    BEGIN_TIMING();
    len=lcsRec(x,m,y,n);
    STOP_TIMING();
    printf("lcsRec:%d\n",len);
    if(e>=0) assert(len==e);

    BEGIN_TIMING();
    len=lcsDPTab(x,m,y,n);
    STOP_TIMING();
    printf("lcsDPTab:%d\n",len);
    if(e>=0) assert(len==e);

    BEGIN_TIMING();
    len=lcsDPMemo(x,m,y,n);
    STOP_TIMING();
    printf("lcsDPMemo:%d\n",len);
    if(e>=0) assert(len==e);

    BEGIN_TIMING();
    len=lcsSpaceOpt<const char>(x,m,y,n);
    STOP_TIMING();
    printf("lcsSpaceOpt:%d\n",len);
    if(e>=0) assert(len==e);

    printf("-----------------------------\n");

}

void testLCS() {

    assertLCS("AGGTAB","GXTXAYB",4);
    assertLCS("cnblog","belong",4);
    assertLCS("ABCDGH","AEDFHR",3);


    int a1[]= {3,10,2,1,20};
    int a2[]= {1,2,3,10,20};
    int len=lcsSpaceOpt<int>(a1,5,a2,5);
    printf("%d\n",len);
}


