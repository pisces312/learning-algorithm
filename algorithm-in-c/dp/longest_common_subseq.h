#ifndef LONGEST_COMMON_SUBSEQ_H_INCLUDED
#define LONGEST_COMMON_SUBSEQ_H_INCLUDED


/***************************************************
Space optimized C++ implementation of LCS problem

Space: O(2*n)

http://www.geeksforgeeks.org/space-optimized-solution-lcs/

***************************************************/
template<typename T>
int lcsSpaceOpt(T* X,int m,T* Y,int n) {
    int L[2][n+1];//!2 arrays, must use n as column number
    // Binary index, used to index current row and
    // previous row.
    bool bi;
    for (int i=0; i<=m; i++) {
        bi = i&1;//!Compute current binary index
        for (int j=0; j<=n; j++) {
            if (i == 0 || j == 0)
                L[bi][j] = 0;

            else if (X[i-1] == Y[j-1]) //i>=1,j>=1 here
                L[bi][j] = L[1-bi][j-1] + 1;

            else
                L[bi][j] = std::max(L[1-bi][j], L[bi][j-1]);
        }
    }

    /* Last filled entry contains length of LCS
       for X[0..n-1] and Y[0..m-1] */
    return L[bi][n];
}


#endif // LONGEST_COMMON_SUBSEQ_H_INCLUDED
