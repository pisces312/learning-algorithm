#include "../test_utils.h"
#include "../search/binary_search.h"
#include "../dp/longest_common_subseq.h"
#include "../sort/sort.h"
/***************************************************************

The Longest Increasing Subsequence (LIS) problem

Note: Only consider strictly increasing sequences. (No duplicate values)

Find the length of the longest subsequence of a given sequence
such that all elements of the subsequence are sorted in increasing order.

For example, the length of LIS for {10, 22, 9, 33, 21, 50, 41, 60, 80} is 6
and LIS is {10, 22, 33, 50, 60, 80}.

More Examples:
Input  : arr[] = {3, 10, 2, 1, 20}
Output : Length of LIS = 3
The longest increasing subsequence is 3, 10, 20

Input  : arr[] = {3, 2}
Output : Length of LIS = 1
The longest increasing subsequences are {3} and {2}

Input : arr[] = {50, 3, 10, 7, 40, 80}
Output : Length of LIS = 4
The longest increasing subsequence is {3, 7, 40, 80}


http://www.geeksforgeeks.org/dynamic-programming-set-3-longest-increasing-subsequence/
http://www.geeksforgeeks.org/longest-monotonically-increasing-subsequence-size-n-log-n/


***************************************************************/

/**

!By DP

Overlapping Subproblems:
Considering the above implementation, following is recursion tree for
an array of size 4. lis(n) gives us the length of LIS for arr[].

             lis(4)
         /       |    \
        /        |     \
      lis(3)   lis(2)   lis(1)
     /   \        |
   lis(2) lis(1) lis(1)
   /
lis(1)


We can see that there are many subproblems which are solved again and again.
So this problem has Overlapping Substructure property and recomputation of
same subproblems can be avoided by either using Memorization or Tabulation.

Let arr[0..n-1] be the input array and L(i) be the length of the LIS ending at index i such that arr[i] is the last element of the LIS.
Then, L(i) can be recursively written as:
L(i) = 1 + max( L(j) ) where 0 < j < i and arr[j] < arr[i]; or
L(i) = 1, if no such j exists.
To find the LIS for a given array, we need to return max(L(i)) where 0 < i < n.

Time complexity: O(n^2)


**/
int lisDP(int* s,int n) {
    if(n<1) return 0;
    int* lis=new int[n];
    //for non-zero input, at least 1
    int maxLen=1;
    lis[0]=1;
    for(int i=1; i<n; ++i) { //i is the length of LIS
        lis[i]=1;
        for(int j=0; j<i; ++j)
            if(s[i]>s[j] && lis[j]+1>lis[i]) {
                lis[i]=lis[j]+1;
                if(lis[i]>maxLen)
                    maxLen=lis[i];
            }
    }
    delete[] lis;
    return maxLen;
}


//Only calculate DP matrix
static int* lisDPArray(int* s,int n) {
    int* dp=new int[n];
    int i,j;
    for(i=1,dp[0]=1; i<n; ++i)
        for(j=0,dp[i]=1; j<i; ++j)
            if(s[i]>s[j])
                if(dp[j]+1>dp[i])
                    dp[i]=dp[j]+1;
    return dp;
}

//Get the increasing seq
static int* backtrackingForLIS(int* lis,int* s,int n, int* maxLen) {
    int len=1;//maintain max length
    int maxIdx=0;
    //Find max len and record the index
    //Start with 1 because always lis[0]=1
    for(int i=1; i<n; ++i)
        if(len<lis[i]) {
            len=lis[i];
            maxIdx=i;
        }

    //! backtracking
    int *inc=new int[len];//store the result
    //decrease the max LIS length when the first time match
    //print the
    for(int i=maxIdx,curLISLen=len; i>=0; --i)
        if(curLISLen==lis[i]) {
            inc[curLISLen-1]=s[i];
            --curLISLen;
        }
    *maxLen=len;
    return inc;
}

int* lisDP2(int* a,int n, int* len) {
    int* dp=lisDPArray(a,n);
    int* inc=backtrackingForLIS(dp,a,n,len);
    delete[] dp;
    return inc;
}



int lisByLinearSearch(int * L,int n) {
    int* B = new int[n+1];//����B
    B[0]=INT_MIN;//��B[0]��Ϊ��С
    B[1]=L[0];//��ʼʱ�������������г���Ϊ1����ĩԪ��Ϊa1
    int len = 1;//LenΪ��ǰ�����������г��ȣ���ʼ��Ϊ1��
    for(int i = 1; i<n; i++)
        if(L[i]>B[len])
            ++len;
        else
            for(int j=i; j>=1; --j) //compare from back to front
                if(B[j]>L[i]) {
                    B[j]=L[i];
                    break;//B[i] is also in increasing order, so just break
                }
    delete[] B;
    return len;
}

/**

Time: O(nlogn)

**/
int lisByBiSearch(int * L,int n) {
    int* B = new int[n+1];//����B
    B[0]=INT_MIN;//��B[0]��Ϊ��С
    B[1]=L[0];//��ʼʱ�������������г���Ϊ1����ĩԪ��Ϊa0
    int len = 1;//��ǰ�����������г��ȣ���ʼ��Ϊ1��
    for(int i = 1; i<n; i++) {
        //B's length is (len+1)
        //len<n, p=[0,len+1],len+1 means L[i]>max element of previous seq
        //extend
        int p= binarySearchFirstForInsert(B,0,len,L[i]);
        B[p] = L[i];//������Ϊp�������������еĵ�ǰ��ĩԪ����Ϊai;
        if(p>len) ++len;//���µ�ǰ�����������г��ȣ�
    }
    delete[] B;
    return len;
}

/////////////////////////////////////////////////////////////////

//longest decreasing sequence
static int maxIncSeq(int *a,int size,int* m) {
    if(size<=0)
        return 0;
    m[0]=1;
    int max=1;
    for(int i=1; i<size; ++i) {
        m[i]=1;////m[i]��СֵΪ1
        for(int j=0; j<i; j++)
            if(a[j]<a[i]&&m[j]+1>m[i]) {
                m[i]=m[j]+1;
                if(m[i]>max)
                    max=m[i];
            }
    }
    return max;
}
static int maxDecSeq(int *a,int size,int* m) {
    if(size<=0)
        return 0;
    m[0]=1;
    int max=1;
    for(int i=1; i<size; ++i) {
        m[i]=1;////m[i]��СֵΪ1
        for(int j=0; j<i; j++)
            if(a[j]>a[i]&&m[j]+1>m[i]) {
                m[i]=m[j]+1;
                if(m[i]>max)
                    max=m[i];
            }
    }
    return max;
}
//longest first inc then dec seq
int longestIncDecSeq(int* a,int size) {
    int maxLen=1;
    int *m=new int[size];
    for(int i=0; i<size; ++i) {
        int tmp=maxIncSeq(a,i,m)+maxDecSeq(a+i,size-i,m);
        if(tmp>maxLen)
            maxLen=tmp;
    }
    delete[] m;
    return maxLen;
}

//////////////////////////////////////////////////////////////

/**
!XXX incorrect for { 2, 5, 3, 7, 11, 8, 10, 13, 6 },
should be 6 but 4

Recursive implementation for calculating the LIS

Optimal Substructure:

Let arr[0..n-1] be the input array
and L(i) be the length of the LIS ending at index i
such that arr[i] is the last element of the LIS.

Then, L(i) can be recursively written as:
L(i) = 1 + max( L(j) ) where 0 < j < i and arr[j] < arr[i]; or
L(i) = 1, if no such j exists.

To find the LIS for a given array, we need to return max(L(i)) where 0 < i < n.
Thus, we see the LIS problem satisfies the optimal substructure property
as the main problem can be solved using solutions to subproblems.


**/
int _lis(int arr[], int n, int *max_lis_length) {
    if (n == 1)// Base case
        return 1;

    int current_lis_length = 1;//at least 1
    for (int i=0; i<n-1; i++) {
        // Recursively calculate the length of the LIS
        // ending at arr[i]
        int subproblem_lis_length = _lis(arr, i, max_lis_length);

        // Check if appending arr[n-1] to the LIS
        // ending at arr[i] gives us an LIS ending at
        // arr[n-1] which is longer than the previously
        // calculated LIS ending at arr[n-1]
        if (arr[i] < arr[n-1] &&
                current_lis_length < (1+subproblem_lis_length))
            current_lis_length = 1+subproblem_lis_length;
    }

    // Check if currently calculated LIS ending at
    // arr[n-1] is longer than the previously calculated
    // LIS and update max_lis_length accordingly
    if (*max_lis_length < current_lis_length)
        *max_lis_length = current_lis_length;

    return current_lis_length;
}

// The wrapper function for _lis()
int lisRec(int* arr, int n) {
    int max_lis_length = 1; // stores the final LIS

    // max_lis_length is passed as a reference below
    // so that it can maintain its value
    // between the recursive calls
    _lis( arr, n, &max_lis_length );

    return max_lis_length;
}



/*******************************************************************

Convert to LCS problem


********************************************************************/

int lisByLCS(int* a,int n) {
    int *b=new int[n];
    memcpy(b,a,sizeof(int)*n);
    quickSortFinal(b,n);
    printf("lisByLCS\n");
    printArray(b,n);
    int len=lcsSpaceOpt<int>(a,n,b,n);
    delete[] b;
    return len;
}

/*******************************************************************************

O(nlogn) algorithm

!Can be used for online (stream)

Our observation is, assume that the end element of largest sequence is E.
  We can add current element A[i] to the existing sequence
  if there is an element A[j] (j > i) such that E < A[i] < A[j]

  We can replace current element A[i] to the existing sequence
  if there is an element A[j] (j > i) such that E > A[i] < A[j]

In case of our original array {2, 5, 3}, note that we face same situation
when we are adding 3 to increasing sequence {2, 5}. I just created
two increasing sequences to make explanation simple.
Instead of two sequences, 3 can replace 5 in the sequence {2, 5}.



The question is, when will it be safe to add or replace an element in the existing sequence
Let us consider another sample A = {2, 5, 3}. Say, the next element is 1.
How can it extend the current sequences {2,3} or {2, 5}.
Obviously, it can't extend either.

Consider the array is {2, 5, 3, 1, 2, 3, 4, 5, 6}.
Making 1 as new sequence will create new sequence which is largest.
The observation is, when we encounter new smallest element in the array,
it can be a potential candidate to start new sequence.

From the observations, we need to maintain lists of increasing sequences.
In general, we have set of active lists of varying length.
We are adding an element A[i] to these lists. We scan the lists (for end elements)
in decreasing order of their length. We will verify the end elements of
all the lists to find a list whose end element is smaller than A[i] (floor value).



Our strategy determined by the following conditions,

1. If A[i] is smallest among all end candidates of active lists,
   we will start new active list of length 1.
2. If A[i] is largest among all end candidates of active lists,
   we will clone the largest active list, and extend it by A[i].
3. If A[i] is in between, we will find a list with
   largest end element that is smaller than A[i].
   Clone and extend this list by A[i]. We will discard all
   other lists of same length as that of this modified list.
Note that at any instance during our construction of active lists,
the following condition is maintained.



Implementation:

We need not to maintain all the lists.
We will use an auxiliary array to keep end elements.
The maximum length of this array is that of input.
In the worst case the array divided into N lists of size one

Discarding operation can be simulated with replacement,
To discard an element, we will trace ceil value of A[i] in auxiliary array,
and replace ceil value with A[i].

Extending a list is analogous to adding more elements to array.
We extend a list by adding element to auxiliary array.
We also maintain a counter to keep track of auxiliary array length.
The end element of new extended list is larger than previous list
So tail array is actually sorted as increase order


http://www.geeksforgeeks.org/longest-monotonically-increasing-subsequence-size-n-log-n/

********************************************************************************/

int lisFast(int* v, int n) {
    if (n == 0)
        return 0;

    int* tail=new int[n];
    int length = 1; // always points empty slot in tail

    //! tail[i] stands for the subseq that has i+1 length
    tail[0] = v[0];
    for (int i = 1; i < n; i++) {
        if (v[i] < tail[0]) {
            // new smallest value
            tail[0] = v[i];
            printf("Create a new list {%d}\n",v[i]);
        } else if (v[i] > tail[length-1]) {
            // v[i] extends largest subsequence
            printf("Extend to list {...,%d,%d}\n",tail[length-1],v[i]);
            tail[length++] = v[i];
        } else {
            // v[i] will become end candidate of an existing subsequence or
            // Throw away larger elements in all LIS, to make room for upcoming grater elements than v[i]
            // (and also, v[i] would have already appeared in one of LIS, identify the location and replace it)
            int discard=binarySearchFirstForInsert(tail, 0, length-1, v[i]);
            printf("Discard list end with %d and create new {...,%d}\n",tail[discard],v[i]);
            tail[discard] = v[i];
        }
    }

    delete[] tail;
    return length;
}

/*****************************************************************************

Construction of Longest Increasing Subsequence (N log N)

http://www.geeksforgeeks.org/?p=27614



*****************************************************************************/
int biSearchFirst(int* keys, int b,int e,int* indices,const int key) {
    --b;//!as sentinel
    while(e-b>1) {//! better understanding than "b<e-1"
        int m=b+((e-b)>>1);
        if(keys[indices[m]]>=key)//!not exit if "==", still move e
            e=m;
        else
            b=m;
    }
    return e;
}

int lisFast2(int* v, int n) {
    if (n == 0)
        return 0;

    int* tailIndices=new int[n];
    int* prevIndices=new int[n];//used when the value in tail is replaced
    for(int i=0; i<n; ++i)
        prevIndices[i]=-1;

    int length = 1; // always points empty slot in tail

    tailIndices[0] = 0;
    for (int i = 1; i < n; i++) {
        if (v[i] < v[tailIndices[0]]) {
            // new smallest value
            tailIndices[0] = i;
            printf("Create a new list {%d}\n",v[i]);
        } else if (v[i] > v[tailIndices[length-1]]) {
            prevIndices[i]=tailIndices[length-1];//i is next element of pre[i]
            // v[i] extends largest subsequence
            printf("Extend to list {...,%d,%d}\n",v[tailIndices[length-1]],v[i]);
            tailIndices[length++] = i;
        } else {
            // v[i] will become end candidate of an existing subsequence or
            // Throw away larger elements in all LIS, to make room for upcoming grater elements than v[i]
            // (and also, v[i] would have already appeared in one of LIS, identify the location and replace it)
            int discard=biSearchFirst(v, 0, length-1,tailIndices, v[i]);
            prevIndices[i]=tailIndices[discard-1];
            printf("Discard list end with %d and create new {...,%d}\n",v[tailIndices[discard]],v[i]);
            tailIndices[discard] = i;
        }
    }

    int* result=new int[length];
    //reverse
    for (int i = tailIndices[length-1],j=length-1; i >= 0; i = prevIndices[i],--j)
        result[j]=v[i];
    for(int i=0; i<length; ++i)
        printf("%d ",result[i]);
    printf("\n");

    delete[] tailIndices;
    delete[] prevIndices;
    delete[] result;
    return length;
}
