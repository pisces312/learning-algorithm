#include "max_subarray_sum.h"
#include "../test_utils.h"


typedef int(*MaxSeqSumFunc)(int*,int, int*, int*);
typedef int(*MaxSeqSumFunc2)(int*,int);

#define ASSERT_MAX_SUBARRAY_FUNC(F,A,N,E) {\
printf("%s: ",#F); \
testMaxSubArrayFunc(F,A,N,E); }

#define ASSERT_MAX_SUBARRAY_FUNC2(F,A,N,E) {\
printf("%s: ",#F); \
testMaxSubArrayFunc2(F,A,N,E); }


static void testMaxSubArrayFunc2(MaxSeqSumFunc2 func,int* a, int n,int expect=INT_MIN) {
    int sum;
    BEGIN_TIMING();
    sum=func(a,n);
    STOP_TIMING();
    printf("%d\n",sum);
    if(expect>INT_MIN)
        assert(expect==sum);
}

static void testMaxSubArrayFunc(MaxSeqSumFunc func,int* a, int n,int expect=INT_MIN) {
    int left,right;
    int sum;
    BEGIN_TIMING();
    sum=func(a,n,&left,&right);
    STOP_TIMING();
    //Check result
    printf("%d; ",sum);
    if(left==right)
        printf("[%d]; ",left);
    else
        printf("[%d~%d]; ",left,right);
    printArray(a,left,right);
    if(expect>INT_MIN)
        assert(expect==sum);
}
static bool isAllNegative(int* a,int n) {
    for(int i=0; i<n; ++i)
        if(a[i]>=0) return false;
    return true;
}
static void assertMaxSubArray(int* a, int n,int expect=INT_MIN) {
    int sum;
    bool isAllNeg=isAllNegative(a,n);

    printArray(a,n);

    //BF
    ASSERT_MAX_SUBARRAY_FUNC(maxSubArrayBF,a,n,expect);
    ASSERT_MAX_SUBARRAY_FUNC(maxSubArrayBF2,a,n,expect);
    ASSERT_MAX_SUBARRAY_FUNC2(maxSubArrayByPatialSum,a,n,expect);

    //DC
    if(!isAllNeg)
        ASSERT_MAX_SUBARRAY_FUNC2(maxSubArrayDC,a,n,expect);
    ASSERT_MAX_SUBARRAY_FUNC2(maxSubArrayDC2,a,n,expect);
    ASSERT_MAX_SUBARRAY_FUNC(maxSubArrayDC3,a,n,expect);

    //DP
    ASSERT_MAX_SUBARRAY_FUNC(maxSubArrayDP1,a,n,expect);
    ASSERT_MAX_SUBARRAY_FUNC(maxSubArrayDP2,a,n,expect);
    ASSERT_MAX_SUBARRAY_FUNC(maxSubArrayDP3,a,n,expect);

    //DP 2
    ASSERT_MAX_SUBARRAY_FUNC2(maxSubArrayDP4,a,n,expect);
    ASSERT_MAX_SUBARRAY_FUNC2(maxSubArrayDP5,a,n,expect);
    ASSERT_MAX_SUBARRAY_FUNC(maxSubArrayDP6,a,n,expect);


    int left,right,len;
    int e=10;
    sum=findClosestSubArrayByNum(e,a,n,&left,&right);
    printf("findSubArrayByNum: %d, expect=%d, d=%d\n",sum,e,std::abs(sum-e));
    if(left>=0)
        printArray(a,left,right);

    e=6;
    len=3;
    sum=findClosestSubArrayByNumAndLen(e,len,a,n,&left,&right);
    printf("findClosestSubArrayByNumAndLen: %d, expect=%d, len=%d, d=%d\n",sum,e,len,std::abs(sum-e));
    if(left>=0)
        printArray(a,left,right);


    printf("-------------------------------------\n");
}

void testFindSubArraySumCloseToNum() {
    int n=6;
    int a1[]= {1,-2,3,5,-3,2};
    int b,e;

    assert(findClosestSubArrayByNum(4,a1,n,&b,&e)==4);
    PRINT_INT(findClosestSubArrayByNum(6,a1,n,&b,&e));
    printArray(a1,b,e);
}
void testFindFixedLenSubArraySumCloseToNum() {
    int n=6;
    int a1[]= {1,-2,3,5,-3,2};
    int b,e,m,sum;

    m=4;
    sum=findClosestSubArrayByNumAndLen(4,m,a1,n,&b,&e);
    printArray(a1,b,b+m);

    m=2;
    sum=findClosestSubArrayByNumAndLen(3,m,a1,n,&b,&e);
    printArray(a1,b,b+m);

}

void testTemplateAlg() {
    std::vector<int> a(6);
    a[0]=-2;
    a[1]=11;
    a[2]=-4;
    a[3]=13;
    a[4]=-5;
    a[5]=2;

    int start=0,end=0;
    PRINT_INT(maxSubsequenceSum1<int>(a,start,end));
    PRINT_INT(maxSubsequenceSum2<int>(a,start,end));
    PRINT_INT(maxSubsequenceSum3<int>(a,start,end));
}



void testMaxSubArrayRandomSuite(int n) {
    int* keys=new int[n];
    createRandomData(keys,n);
    if(n<50) printArray(keys,n);
    int begin,end;
    int expect=maxSubArrayDP3(keys,n,&begin,&end);
    assertMaxSubArray(keys,n,expect);
    delete[] keys;
}

void testMaxSubArrayRing(){
    int a1[]={0,-2,3,5,-1,2};
    testMaxSubArrayFunc2(maxSubArrayRing,a1,6,16);
    testMaxSubArrayFunc2(maxSubArrayRingByDC,a1,6,16);
}

/////////////////////////////////////////////////////////
// Test entry
void testMaxContinuousSeqSum() {
    int n;

    int d1[]= {2,1,-3,7,1,9,-12,5,0,-1,7};
    n=sizeof(d1)/sizeof(int);
    assertMaxSubArray(d1,n,17);

    int d2[]= { -2, 11, -4, 13, -5, -2 };
    n=sizeof(d2)/sizeof(int);
    assertMaxSubArray(d2,n,20);

    int allNeg[]= { -21, -11, -4, -13, -5, -12 };
    n=sizeof(allNeg)/sizeof(int);
    assertMaxSubArray(allNeg,n,-4);

    testFindSubArraySumCloseToNum();
    testFindFixedLenSubArraySumCloseToNum();

    testTemplateAlg();

    testMaxSubArrayRandomSuite(100);

    int a1[]= {1,-2,3,5,-3,2};
    int a2[]= {0,-2,3,5,-1,2};
    //ȫ��
    int a3[]= {-9,-2,-3,-5,-3,-7};
    int a4[]= {-9,8,-28,4,9,-28,-14,
               2,15, -3, 16, -24, 19, 23, 0,
               -12, -26, 14, -18, 17, -6,
               -27, 28, 12, -20, -12, -14, -8, 9, -26
              };
    int a5[]= {-3,-4,-2,-5,1,-2}; //-1

    testMaxSubArrayRing();


}
