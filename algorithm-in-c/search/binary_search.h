#ifndef BINARYSEARCH_H_INCLUDED
#define BINARYSEARCH_H_INCLUDED


int binarySearch(int* keys, int b,int e,const int key);
int binarySearchR(int* keys, int b,int e,const int key);
int binarySearchLast(int* keys, int b,int e,const int key);
int binarySearchFirstEq(int* keys, int b,int e,const int key);
int binarySearchFirstEq2(int* keys, int b,int e,const int key);
int binarySearchFirstEq3(int* keys, int b,int e,const int key);

int binarySearchLess(int* keys, int b,int e,const int key);
int binarySearchGreater(int* keys, int b,int e,const int key);

//Use sentinel
int binarySearchFirstForInsert(int* keys, int b,int e,const int key);
int binarySearchForInsert1(int* keys, int b,int e,const int key);
int binarySearchForInsert3(int* a, int b, int e, const int key,bool* found);


void binarySearchForRange(int* a, int n, const int key, int* b, int* e);
//int binarySearch1(int* keys, int b,int e,const int key);
//int binarySearch2(int* keys, int b,int e,const int key);



void PrepareForBinarySearchFixedNum(int n);
int binarySearchFixedNum(int* keys,const int key);
int binarySearchThousand(int* x,int n,const int t);


#endif // BINARYSEARCH_H_INCLUDED
