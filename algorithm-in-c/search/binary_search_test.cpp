#include "binary_search.h"
#include "search.h"
#include"../test_utils.h"

typedef int (*SearchFunc)(int* keys, int minIndex,int maxIndex,const int key);

static int* createRandomSorted(int n) {
    int* keys=new int[n];
    createRandomData(keys,n);
    qsort(keys,n,sizeof(int),lessInt);
    return keys;
}
enum SortedDataType {
    Natual,Even,Odd
};
static int* createSortedData(int n, SortedDataType type=SortedDataType::Natual) {
    int* keys=new int[n];
    for(int i=0,num=0; i<n; ++num) {
        switch(type) {
        case SortedDataType::Natual:
            keys[i++]=num;
            break;
        case SortedDataType::Even:
            if((num&0x1)==0)
                keys[i++]=num;
            break;
        case SortedDataType::Odd:
            if((num&0x1)!=0)
                keys[i++]=num;
            break;
        }
    }
    return keys;
}


static int* createSeqSorted(int n) {
    return createSortedData(n);
}

/**
Search function return:
    Not found: -1
Parameters:
    isFirst: whether must return the first equal one
**/
static void assertSearchFunc(SearchFunc func,int*keys,int n,bool isFirst=false,bool isAssert=true) {
    int idx;
    for(int i=0; i<n; ++i) {
        idx=func(keys,0,n-1,keys[i]);
        printf("Found %d at %d\n",keys[i],idx);
        if(isAssert) {
            if(isFirst)
                assert(idx==linnearSearchFirst(keys,0,n-1,keys[i]));
            else
                assert(keys[idx]==keys[i]);
        }
    }
    idx=func(keys,0,n-1,-1);
    if(idx<0)
        printf("Not found: %d\n",idx);
    else
        printf("Found %d at %d\n",-1,idx);
    if(isAssert) assert(idx==-1);
    idx=func(keys,0,n-1,n);
    if(idx<0)
        printf("Not found: %d\n",idx);
    else
        printf("Found %d at %d\n",n,idx);
    if(isAssert) assert(idx<0||idx>=n);
    delete[] keys;

}

static void testRandom(SearchFunc func,int n=10,bool isFirst=false,bool flag=true) {
    printf("---------Test random values----------\n");
    int* keys=createRandomSorted(n);
    printArray(keys,n);
    assertSearchFunc(func,keys,n,isFirst,flag);
    delete[] keys;
}

static void testRandomFindFirst(SearchFunc func,int n=10,bool flag=true) {
    testRandom(func,n,true,flag);
}
static void testSeq(SearchFunc func,int n=10,bool isFirst=false,bool flag=true) {
    printf("---------Test seq values----------\n");
    int idx;
    int* keys=createSeqSorted(n);
    printArray(keys,n);
    assertSearchFunc(func,keys,n,isFirst,flag);
    delete[] keys;
}
static void testSame(SearchFunc func,int n=10,bool isFirst=false,bool flag=true) {
    printf("---------Test same values----------\n");
    int val=0;
    int idx;
    int* keys=new int[n];
    memset(keys,val,sizeof(int)*n);
    printArray(keys,n);
    assertSearchFunc(func,keys,n,isFirst,flag);
    delete[] keys;
}

#define assertBiSearch(F) { \
printf("----------------%s------------------\n",#F); \
testAllCommon((F)); }

static void testAllCommon(SearchFunc func,int n=10,bool isFirst=false,bool flag=true,int count=1) {
    for(int i=0; i<count; ++i) {
        testSame(func,n,isFirst,flag);
        testSeq(func,n,isFirst,flag);
        testRandom(func,n,isFirst,flag);
    }
    printf("-------------------all pass!-------------------\n");
}

/////////////////////////////////////////////////////


static void testSearchFixedNum(int n=10) {
    PrepareForBinarySearchFixedNum(n);
    int* keys=createRandomSorted(n);
    printArray(keys,n);
    int id;
    for(int i=0; i<n; ++i) {
        id=binarySearchFixedNum(keys,keys[i]);
        printf("%d ",id);
        assert(keys[id]==keys[i]);
        assert(id==linnearSearchFirst(keys,0,n-1,keys[i]));
    }
    printf("\n");
    assert(binarySearchFixedNum(keys,-1)==-1);
    assert(binarySearchFixedNum(keys,n)==-1);
    delete[] keys;
}

static void testSearchThousand() {
    int n=1000;
    int* keys=createRandomSorted(n);
    int pos;
    for(int i=0; i<n; ++i) {
        pos=binarySearchThousand(keys,n,keys[i]);
        if(keys[pos]!=keys[i]) {
            printf("keys[%d]=%d, keys[%d]=%d\n",pos,keys[pos],i,keys[i]);
            break;
        }
        assert(keys[pos]==keys[i]);
        assert(pos==linnearSearchFirst(keys,0,n-1,keys[i]));
    }
    assert(binarySearchThousand(keys,n,-1)==-1);
    assert(binarySearchThousand(keys,n,n)==-1);
    delete[] keys;
}


static void testBiSearchInsertForUnique(SearchFunc func=binarySearchFirstForInsert) {
    int i;
    int key;
    int n=10;
    int* keys=createSortedData(n,SortedDataType::Even);
    printArray(keys,n);
    //Existed one
    key=keys[0];
    i=func(keys,0,n-1,key);
    printf("seach key=%d\tfound at %d\n",key,i);
    assert(i==0);

    //Smaller than the first element
    key=-1;
    i=func(keys,0,n-1,key);
    printf("seach key=%d\tfound at %d\n",key,i);
    assert(i==0);


//Larger than the last element
    key=keys[n-1]+1;
    i=func(keys,0,n-1,key);
    printf("seach key=%d\tfound at %d\n",key,i);
    assert(i==n);

//Not existing one but within data range
    key=3;
    i=func(keys,0,n-1,key);
    printf("seach key=%d\tfound at %d\n",key,i);
    assert(i==2);
}


static void testBinarySearchInsertionWithFlag() {
    bool flag;
    int i;
    int key;
    int n=10;
    int b=0,e=n-1;
    int* keys=createSortedData(n,SortedDataType::Even);
    printArray(keys,n);
    //Existed one
    key=keys[0];
    i=binarySearchForInsert3(keys,b,e,key,&flag);
    if(flag)
        printf("seach key=%d\tfound at %d\n",key,i);
    else
        printf("seach key=%d\tinsert at %d\n",key,i);
    assert(i==0);

    //Smaller than the first element
    key=-1;
    i=binarySearchForInsert3(keys,b,e,key,&flag);
    if(flag)
        printf("seach key=%d\tfound at %d\n",key,i);
    else
        printf("seach key=%d\tinsert at %d\n",key,i);
    assert(i==b);


//Larger than the last element
    key=keys[n-1]+1;
    i=binarySearchForInsert3(keys,b,e,key,&flag);
    if(flag)
        printf("seach key=%d\tfound at %d\n",key,i);
    else
        printf("seach key=%d\tinsert at %d\n",key,i);
    assert(i==n);

//Not existing one but within data range
    key=3;
    i=binarySearchForInsert3(keys,b,e,key,&flag);
    if(flag)
        printf("seach key=%d\tfound at %d\n",key,i);
    else
        printf("seach key=%d\tinsert at %d\n",key,i);
    assert(i==2);
}
//////////////////////////////////////////


static void testBinarySearchGreater() {
    int i;
    int n=10;
    int* keys=createSortedData(n,SortedDataType::Even);
    printArray(keys,n);
//1.1 Contain key=keys[m]: key=keys[m]<keys[i], i is in the array, return i
    i=binarySearchGreater(keys,0,n-1,keys[0]);
    printf("%d\n",i);
    assert(i==1);

//1.2 Contain key=keys[m]: m is the last element, return e+1
    i=binarySearchGreater(keys,0,n-1,keys[n-1]);
    printf("%d\n",i);
    assert(i==n);

//2.1 No search key: key<keys[i], i is in the array, return i
    i=binarySearchGreater(keys,0,n-1,3);
    printf("%d\n",i);
    assert(i==2);
//2.2 No search key: key<keys[0], return 0
    i=binarySearchGreater(keys,0,n-1,-1);
    printf("%d\n",i);
    assert(i==0);

//2.3 No search key: key>=keys[e], all values are smaller than key,
//    return e+1
    i=binarySearchGreater(keys,0,n-1,100);
    printf("%d\n",i);
    assert(i==n);

}





static void testBinarySearchRange() {
    int val=0;
    int n=5;
    int* keys=new int[n];
    int b,e;
    memset(keys,val,sizeof(int)*n);
    printArray(keys,n);


    binarySearchForRange(keys,n,val,&b,&e);
    printf("[%d,%d]\n",b,e);

    int a[]= {5, 7, 7, 8, 8, 10};
    n=sizeof(a)/sizeof(int);
    printArray(a,n);
    binarySearchForRange(a,n,8,&b,&e);
    printf("[%d,%d]\n",b,e);

}


void testBinarySearch() {

    testBinarySearchInsertionWithFlag();
    testBiSearchInsertForUnique();

    assertBiSearch(binarySearch);

//find the keys[i]>key, not equal to
//not assert
    testAllCommon(binarySearchGreater,6,false,false);
    testBinarySearchGreater();
    testAllCommon(binarySearchLess,6,false,false);
    assertBiSearch(binarySearchR);
    assertBiSearch(binarySearchLast);


    //Test find first equal
    assertBiSearch(binarySearchFirstEq);
    testRandomFindFirst(binarySearchFirstEq);
    assertBiSearch(binarySearchFirstEq2);
    testRandomFindFirst(binarySearchFirstEq2);
    assertBiSearch(binarySearchFirstEq3);
    testRandomFindFirst(binarySearchFirstEq3);

    testSearchThousand();
    testSearchFixedNum();

    testBinarySearchRange();

}
