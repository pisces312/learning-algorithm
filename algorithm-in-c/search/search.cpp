#include "../common.h"
#include "../sort/sort.h"

template <typename T>
int linearSearchGeneric(const T* keys,int b,int e, const T key) {
    int i = b;
    while(i <= e && keys[i]!=key)
        ++i;
    if(i > e)
        return -1;
    return i;
}


int linearSearch(int* keys, int b,int e, int key) {
    int i = b;
    while(i <= e && keys[i]!=key)
        ++i;
    if(i > e)
        return -1;
    return i;
}

//!Faster sequential search: Sentinel使用哨卫
int seqSearchSentinel(int* x, int n,const int t) {
    int i;
    int hold = x[n];//Use an extra space after array
    x[n] = t;
    for(i = 0; ; i++) //减少了比较的次数,省了多次i<n的判断
        if(x[i] == t)
            break;
    x[n] = hold;
    if(i == n)
        return -1;
    return i;
}
/* Alg 23: Faster sequential search: loop unrolling */
int seqSearchLoopUnrolling(int* x, int n,const int t) {
    int i;
    int hold = x[n];
    x[n] = t;
    for(i = 0; ; i+=8) {
        if(x[i] == t)   {
            break;
        }
        if(x[i+1] == t) {
            i += 1;
            break;
        }
        if(x[i+2] == t) {
            i += 2;
            break;
        }
        if(x[i+3] == t) {
            i += 3;
            break;
        }
        if(x[i+4] == t) {
            i += 4;
            break;
        }
        if(x[i+5] == t) {
            i += 5;
            break;
        }
        if(x[i+6] == t) {
            i += 6;
            break;
        }
        if(x[i+7] == t) {
            i += 7;
            break;
        }
    }
    x[n] = hold;
    if(i == n)
        return -1;
    return i;
}


int linnearSearchFirst(int* keys, int b,int e,const int key) {
    for(int i=b; i<=e; ++i)
        if(keys[i]==key)
            return i;
    return -1;
}


//<keys[0], return -1
//>keys[n-1], return n
//==keys[i], return i
//keys[0]<keys[i-1]<=key<keys[n-1], return i
int linearSearchForInsertion(int* keys, int b,int e,const int key) {
    if(key<keys[b])
        return -1;
    int i=b;
    for(; i<=e; ++i)
        if(keys[i]>=key)
            return i;
    return i;
}



void testSearch() {
    int a[]= {4,2,7,3,8,4,2,1,9};
    int n=sizeof(a)/sizeof(int);

    int i=linearSearchGeneric<int>(a,0,n-1,1);
    printf("a[%d]=%d\n",i,a[i]);
//
//
//
//    quickSortFinal(a,n);
//    printArray(a,n);
//
//    i=binarySearch(a,0,n-1,1);
//    printf("a[%d]=%d\n",i,a[i]);
//
//
//    i=binarySearchRecursive(a,0,n-1,1);
//    printf("a[%d]=%d\n",i,a[i]);


}
