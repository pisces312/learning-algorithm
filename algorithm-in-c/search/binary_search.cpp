#include"binary_search.h"
#include "../common.h"

/**************************************************
Binary search

Precondition: array must have order

Time: O(logn)

边界条件容易出错，出现死循环等


***************************************************/

/**

!Basic version, returns the mid of same values
-1 if not found

while(b<=e)
b=m+1, make sure move one step every iteration, avoid endless loop
e=m-1

**/

int binarySearch(int* keys,int b, int e, const int key) {
    while(b<=e) {//!border condition
        int m = b + ((e - b)>>1); //!avoid overflow
//        printf("b:%d,m:%d,e:%d\n",b,m,e);
//m=b/2+e/2;
//m=(b+e)/2; //b+e may overflow
        if(keys[m]==key)
            return m;
        if(keys[m]<key)
            b=m+1;//!
        else
            e=m-1;//!
    }
    return -1;
}

//Recursive version
int binarySearchR(int* keys,int b, int e, int key) {
    if(b>e)
        return -1;
    int m = b + ((e - b) / 2);
    if(keys[m]==key)
        return m;
    if(keys[m]<key)
        return binarySearchR(keys,m+1,e,key);
    return binarySearchR(keys,b,m-1,key);
}


/**
!Return the right most idx if there are many same values,

while(b<e-1)
<=key, b=m
e=m

如果有很多待查找的数字，找出最大的，最大的一定是在最右边

为了能够把左右两个数进行对比，
循环结束的时候，左右指针对应的数要相隔1，
所以循环结束的条件left < right -1；

要想保留最大下标，当data[mid] == value时，不能结束，
因为data[mid+1]也可能等于value，所以要继续遍历，
这时让left = mid，不能让right=mid，
因为mid右边可能还有等于value的值，所以<=要合并。

**/
int binarySearchLast(int* keys, int b,int e,const int key) {
    //In most cases, keys[b]<=key and keys[e]>key
    //! better understanding than "b<e-1"
    //! when exits, e-b==1 which means b nexts to e
    while(e-b>1) {
        int m=b+((e-b)>>1);
//        printf("b:%d,m:%d,e:%d\n",b,m,e);
        //!cannot exit when ==, needs to find max
        if(keys[m]<=key)
            b=m;
        else
            e=m;
    }
//    printf("b:%d,e:%d\n",b,e);
    //for the case that the key to search is the last element
    if(keys[e]==key)
        return e;
    if(keys[b]==key)//common case
        return b;
    return -1;
}



/**

!Return the first of same values

while(b<e-1)
>=key, e=m
b=m

**/
int binarySearchFirstEq(int* keys, int b,int e,const int key) {
    //! better understanding than "b<e-1"
    while(e-b>1) {
        int m=b+((e-b)>>1);
//        printf("b:%d,m:%d,e:%d\n",b,m,e);
        if(keys[m]>=key)//!move e even it's equal to search key
            e=m;
        else
            b=m;
    }
//    printf("b:%d,e:%d\n",b,e);
    //The reason why check b
    //When the search key is found as the first element
    //b=0,e=1, so the result cannot be e but b
    //We can use sentinel
    if(keys[b]==key)
        return b;
    if(keys[e]==key)//common case
        return e;
    return -1;
}

int binarySearchFirstEq3(int* arr, int b, int e, const int value) {
    while (b < e) {
        int m = b + (e - b) / 2;
        if (arr[m] < value)
            b = m + 1;
        else
            e = m;
    }
    if (arr[b] == value)
        return b;
    return -1;
}

int binarySearchFirstEq2(int* keys, int b,int e,const int key) {
    int insIdx=binarySearchFirstForInsert(keys,b,e,key);
    if((insIdx==e+1)||keys[insIdx]!=key)
        return -1;
    return insIdx;
}

//!Use sentinel
//int binarySearch2(int* keys,int b, int e, const int key) {
//    ++e;
//    while(b<e) {//!
//        int m = b + ((e - b)>>1);
//        printf("b:%d,m:%d,e:%d\n",b,m,e);
//        if(keys[m]==key)
//            return m;
//        if(keys[m]<key)
//            b=m+1;
//        else
//            e=m;//!
//    }
//    return -1;
//}

//int binarySearchFirst2(int* keys, int b,int e,const int key) {
//    --b;
//    ++e;
//    int n=e;
//    while(b+1!=e) {
//        int m=b+((e-b)>>1);
//        if(keys[m]<key)
//            b=m;
//        else
//            e=m;
//    }
//    if(e>=n||keys[e]!=key)
//        return -1;
//    return e;
//}

/////////////////////////////////////////////////

static int GetMaxPowerOf2(int n) {
    int mask=0x40000000;
    do {
        if((n&mask)!=0)
            return mask;
        mask>>=1;
    } while(mask);
    return mask;
}
static int BINARY_SEARCH_FIRST_STEP;
static int BINARY_SEARCH_FIXED_NUM;
void PrepareForBinarySearchFixedNum(int n) {
    BINARY_SEARCH_FIXED_NUM=n;
    BINARY_SEARCH_FIRST_STEP=GetMaxPowerOf2(BINARY_SEARCH_FIXED_NUM);
}
int binarySearchFixedNum(int* keys, const int key) {
    int i=BINARY_SEARCH_FIRST_STEP;
    int b=-1;
    if(keys[i-1]<key)
        b=BINARY_SEARCH_FIXED_NUM-i;
    while(i!=1) {
        i>>=1;
        if(keys[b+i]<key)
            b+=i;
    }
    ++b;
    if(b>BINARY_SEARCH_FIXED_NUM||keys[b]!=key)
        return -1;
    return b;
}

/**
For a certain count of numbers

hard code all comparions

**/
int binarySearchThousand(int* x,int n,const int t) {
    if(n != 1000)
        return binarySearchFirstEq(x,0,n-1,t);
    int l = -1;
    if(x[511]   < t) l = 1000 - 512;
    if(x[l+256] < t) l += 256;
    if(x[l+128] < t) l += 128;
    if(x[l+64 ] < t) l += 64;
    if(x[l+32 ] < t) l += 32;
    if(x[l+16 ] < t) l += 16;
    if(x[l+8  ] < t) l += 8;
    if(x[l+4  ] < t) l += 4;
    if(x[l+2  ] < t) l += 2;
    if(x[l+1  ] < t) l += 1;
    ++l;
    if(l >= n || x[l] != t)
        return -1;
    return l;
}

/*******************************************************

Find an index that keys[i]<key

1. key may not existed in the array!!
2. -1 means all values are larger than key

*******************************************************/

int binarySearchLess(int* keys, int b,int e,const int key) {
    while(b<e-1) {
        int m=b+((e-b)>>1);
//        printf("b:%d,m:%d,e:%d\n",b,m,e);
        //cannot exit when ==, needs to find max
        if(keys[m]<key)
            b=m;
        else
            e=m-1;//keys[m]>=key, so key[m-1]<key
    }
//    printf("b:%d,e:%d\n",b,e);
    //Case: key is larger than the max value of the array
    //also means the array doesn't have this key
    if(keys[e]<key)
        return e;
    if(keys[b]<key)//common case
        return b;
    return -1;//all values are larger than key!!
}

/*******************************************************

! Find an index that key<keys[i]

Can be used for insert sort alg

Parameters:
e: last element index

Return: [b,e+1], if b=0, [0,n], n is not the index within array

1. Contain key=keys[i-1]
   1) key=keys[i-1]<keys[i], i-1 is the last matched one,
      i is in the array, return i
   2) m is the last element, return e+1
2. No search key
   1) key<keys[i], i is in the array, return i
   2) key<keys[b], return b
   3) key>=keys[e], all values are smaller than key,
    return e+1

E.g.
key=1; 2,2,2,2,2
 e+1 means
key=1; 0,0,0,0

*******************************************************/

int binarySearchGreater(int* keys, int b,int e,const int key) {
    while(b<e-1) {
        int m=b+((e-b)>>1);
//        printf("b:%d,m:%d,e:%d\n",b,m,e);
        //cannot exit when ==, needs to find max
        if(keys[m]>key)
            e=m;
        else
            b=m+1;
    }
//    printf("b:%d,e:%d\n",b,e);
    //Case: key is smaller than the max value of the array
    //also means the array doesn't have this key
    //so just return the first element
    if(keys[b]>key)//special case
        return b;//search key may not in the array, just return the first element
    if(keys[e]>key)//common case
        return e;
    //all values are smaller than search key!!
    //return next pos
    return e+1;//special case
}

/********************************************************

Search Insert Position


Given a sorted array and a target value

1. <keys[0], two solutions
    1) return -1
    2) return 0, keep consistent with #3
2. keys[0]<keys[i-1]<=key<keys[n-1], return i
3. >keys[n-1], return n
4. ==keys[i], return i

Here are few examples.
[1,3,5,6], 5 → 2
[1,3,5,6], 2 → 1 Not existed but within data range
[1,3,5,6], 7 → 4
[1,3,5,6], 0 → -1 or 0

*********************************************************/

/**
!1. <keys[0], return -1
2. keys[0]<keys[i-1]<=key<keys[n-1], return i
3. >keys[n-1], return n
4. ==keys[i], return i
**/
int binarySearchForInsert1(int* a, int b, int e, const int key) {
    while(b<e-1) {//(b<=e-2) a[b]<=key<=a[m-1]<a[e]
        int m=b+((e-b)>>1);
//        printf("b:%d,m:%d,e:%d\n",b,m,e);
        if(a[m]>key)
            e=m-1;
        else
            b=m;//! <=key, move even if equal to search key
    }//break when b>e-1 => b>=e => b==e
//    printf("b:%d,e:%d\n",b,e);
    if(a[e] == key) return e;//Found existing one
    if(a[e] < key) return e+1;//Larger than all data
//    if(a[b] == key) return b;//Found existing one
    if(a[b] > key) return -1;
    return b+1;//Not found but within data range
}


/**
 !!Make actual left boundary decrease as a sentinel (guard)
 to make the logic consistent

 Consider this case:
 b=0,e=n-1; The result should be first element

 if not decrease b, when exits loop, e-b=1, b can be 0,
 but e will be 1 at that time. It needs to add extra logic
 to check keys[b]

 If decrease, when exits, b=-1,e=0, so it just needs to always
 return e!!

 Return:
 Found->[b,e]
 Not found
    ->i, keys[e]>=keys[i]>key
    ->b, if key<keys[b]
    ->e+1, if key>keys[e]

 **/
int binarySearchFirstForInsert(int* keys, int b,int e,const int key) {
    if(key>keys[e])//optional, make it return fast
        return e+1;
    if(key<keys[b])//optional, make it return fast
        return b;//at least return b
    --b;//!as sentinel
    ++e;//!as sentinel, if not ++, if key>keys[e], will not return e+1
    //! better understanding than "b<e-1"
    while(e-b>1) {//b=[-1,e], e=[0,e+1]
        int m=b+((e-b)>>1);
        if(keys[m]<key)
            b=m;
        else
            e=m;//! key<=keys[m], not exit if "==", still move e
    }
    //!no need to check b now
    //return the first index of matched search key
    return e;//[0,e+1]
}

/**

!1. <keys[0], return 0, keep consistent with #3
2. keys[0]<keys[i-1]<=key<keys[n-1], return i
3. >keys[n-1], return n
4. ==keys[i], return i

Use found to distinguish the returned idx is found
or for insertion

**/
int binarySearchForInsert3(int* a, int b, int e, const int key,bool* found) {
    while(b<e-1) {//(b<=e-2) a[b]<=key<=a[m-1]<a[e]
        int m=b+((e-b)>>1);
//        printf("b:%d,m:%d,e:%d\n",b,m,e);
        if(a[m]>key)
            e=m-1;
        else
            b=m;//! <=key, move even if equal to search key
    }//break when b>e-1 => b>=e => b==e
//    printf("b:%d,e:%d\n",b,e);
    if(a[e] == key) {//Found existing one
        *found=true;
        return e;
    }
    *found=false;
    if(a[e] < key) return e+1;//Larger than all data
//    if(a[b] == key) return b;//Found existing one
    if(a[b] > key) return b;//return first
    return b+1;//Not found but within data range
}

/**************************************************************

Search for a Range

Given a sorted array of integers, find the starting and ending position of a given target value.

Your algorithm's runtime complexity must be in the order of O(log n).

If the target is not found in the array, return [-1, -1].

For example,
Given [5, 7, 7, 8, 8, 10] and target value 8,
return [3, 4].

**************************************************************/
void binarySearchForRange(int* a, int n, const int key, int* b, int* e) {
    *e=binarySearchLast(a,0,n-1,key);
    if(*e<0) {
        *b=-1;
        return;
    }
    *b=binarySearchFirstEq(a,0,*e,key);
}




