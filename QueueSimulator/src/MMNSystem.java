/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Administrator
 */
public class MMNSystem extends QSystem {

    public MMNSystem(String name, ArriveProcess arriveProcess, double miu, int serverNum) {
        this.name = name;
        this.arriveProcess = arriveProcess;
        this.serverNum = serverNum;
        arriveProcess.addSystem(this);
        serveProcess = new ServeProcess(serverNum);
        serveProcess.addListener(this);
        for (int i = 0; i < serverNum; i++) {
            serveProcess.addServer(new Server(String.valueOf(i), miu, serveProcess));
        }
        queueLengthLimited = -1;
//        Thread thread = new Thread(arriveProcess);
//        thread.start();
    }
}
