
import java.util.Random;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class RandomUtils {

    static Random random=new Random();

    public static double exponentialDistribution(double r, long t) {
        return 1 - (Math.exp(-r * t));
    }

    public static double exponentialDistributionP(double r, long t) {
        return (r * Math.exp(-r * t));
    }
//符合指数分布的随机数

    /**
     * 即到达强度越大，间隔越小
     * 这里设到达强度为每分钟到达的数目
     * @param lamda
     * @return
     */
    public static double randomE(double lamda) {
        return -Math.log(random.nextDouble()) / lamda;
    }
}
