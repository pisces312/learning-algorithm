
import java.util.LinkedList;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public abstract class QSystem implements ServerListener {

    String name;
    ArriveProcess arriveProcess;
    ServeProcess serveProcess;
    int serverNum;
    int queueLengthLimited;
    LinkedList<Client> clientsQueue = new LinkedList<Client>();
    long totalWaitTime = 0;
    int totalClientNum = 0;
    //当前到来的客户被服务完，不能用这种方式判断是否处理完所有客户，可能
    //下一个还没有到来

//    int finishedClient=0;
//    int totalFinishedClient = 0;
    //返回当前服务好的客户数
    public int currentFinishedClient = 0;

    public void setServeProcess(ServeProcess serveProcess) {
        this.serveProcess = serveProcess;
    }

    public void setArriveProcess(ArriveProcess arriveProcess) {
        this.arriveProcess = arriveProcess;
    }
//    public void finishClient(Client client){
//
//    }

    /**
     * 通知系统一个客户开始接受服务
     * 对所有客户进行等待时间的统计
     * @param client
     */
    public void notifyClientAccepted(Client client) {
        totalClientNum++;
        totalWaitTime += client.waitTime;
//        System.out.println("[ " + name + " ]: 平均等待时间:" + getEWs());
    }

    public void processClient(Client client) {

        client.arrived(this);
        if (serveProcess.canServeClient(client)) {
            System.out.println("[ " + name + " ]: 正在处理" + client);
        } else if (queueLengthLimited != -1 && queueLengthLimited < clientsQueue.size()) {
            System.out.println("[ " + name + " ]: 队列已满");

        } else {
            //先处理

            //没有服务员空闲则放到等待队列
            System.out.println("[ " + name + " ]: " + client + "等待");
            clientsQueue.add(client);
        }
    }

    /**
     * 给一个模拟结束的判断
     * @return
     */
    public boolean isFinished() {
        return arriveProcess != null && currentFinishedClient == arriveProcess.maxClientNum;
    }

    public void notifyReleased() {
        synchronized (this) {
            currentFinishedClient++;
        }
        //保证只在最后处理完所有客户显示一次平均等待时间
//        totalFinishedClient++;
        if (isFinished()) {
            serveProcess.closeServerProcess();
//        if (arriveProcess!=null&&currentFinishedClient == arriveProcess.maxClientNum) {
            System.out.println("====[ " + name + " ]: 平均等待时间:" + getEWs());
        }
        if (!clientsQueue.isEmpty()) {
            Client client = clientsQueue.getFirst();
            //需要移除处理完的
            clientsQueue.removeFirst();
            if (serveProcess.canServeClient(client)) {
                System.out.println("[ " + name + " ]:正在处理" + client);
            }
        } else {
            //客户没有全部处理完，但仍
//            System.out.println("平均等待时间:" + getEWs());
        }
    }

    public double getEWs() {
        return totalWaitTime / (double) totalClientNum;
    }
}
