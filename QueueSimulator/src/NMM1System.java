
import java.util.Random;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class NMM1System extends QSystem {

    int systemNum;
    QSystem[] mm1Systems;
    Random random = new Random();
//    int currentFinishedClientNum = 0;

    public NMM1System(String name, ArriveProcess arriveProcess, double miu,int systemNum) {
        this.systemNum = systemNum;
        this.name = name;
        this.arriveProcess = arriveProcess;
        mm1Systems = new MM1System[systemNum];
        arriveProcess.addSystem(this);
        for (int i = 0; i < mm1Systems.length; i++) {
            mm1Systems[i] = new MM1System("M|M|1-" + i, this,miu);
        }
    }

    @Override
    public void processClient(Client client) {
        //随机选择一个队伍排队
        int id = random.nextInt(systemNum);
//        System.out.println(id);
        mm1Systems[id].processClient(client);
//                System.out.println("[ n-M|M|1 ]:平均等待时间："+getEWs());
//        super.processClient(client);
    }

//    @Override
//    public void notifyClientAccepted(Client client) {
////        System.out.println("");
//        totalClientNum++;
////        super.notifyClientAccepted(client);
//    }
    @Override
    public void notifyReleased() {
//        super.notifyReleased();
//        System.out.println("cur:"+currentFinishedClientNum);
        currentFinishedClient++;
        if (currentFinishedClient == arriveProcess.getTotalClientNum()) {
            System.out.println("======[ n-M|M|1 ]:平均等待时间：" + getEWs());
            for(QSystem system:mm1Systems){
                system.serveProcess.closeServerProcess();
            }
            
        }
    }
//    @Override
//    public void notifyClientAccepted(Client client) {
//        super.notifyClientAccepted(client);
//        System.out.println("[ n-M|M|1 ]:平均等待时间："+getEWs());
//    }
//    @Override
//    public double getEWs() {
//
//        totalClientNum = 0;
//        totalWaitTime = 0;
//        for (QSystem system : mm1Systems) {
//            totalClientNum += system.totalClientNum;
//            totalWaitTime += system.totalWaitTime;
//        }
//        if (totalClientNum == 0) {
//            return 0;
//        }
//        return totalWaitTime / totalClientNum;
//    }
}
