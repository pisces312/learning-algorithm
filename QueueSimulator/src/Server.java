
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class Server implements Runnable {
//服务能力

    double miu = 1;
    //服务的对象
    Client client;
    ServeProcess serveProcess;
    String name;

    public long getServerProcessTime() {
        return (long) (500 * RandomUtils.randomE(miu));
//        system.

    }

    public void acceptClient(Client client) {
        this.client = client;
        client.accepted();
    }

    public Server(String name, double miu,ServeProcess serveProcess) {
        this.name = name;
        this.miu=miu;
        this.serveProcess = serveProcess;
    }

    public void run() {
        
        long time = getServerProcessTime();
        System.out.println("服务时间：" + time);
        try {
            Thread.sleep(time);
//        throw new UnsupportedOperationException("Not supported yet.");
        } catch (InterruptedException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("服务员" + name + "对客户" + client.name + "的服务完成");
//        client.finished();
        serveProcess.releaseServer(this);
//        throw new UnsupportedOperationException("Not supported yet.");
    }
//    public Server(Client client) {
//        this.client = client;
//        client.setAcceptedTime(System.currentTimeMillis());
//    }
}
