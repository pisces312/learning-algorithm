
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class ServeProcess {
    //一个服务过程只能对应一个系统
//    ServerListener listener;
    //解决由n个相同系统组成时的通知问题

    LinkedList<ServerListener> listeners = new LinkedList<ServerListener>();
//    int count = 0;
//    QSystem system;
//    double miu = 0.2;
    LinkedList<Server> freeServers = new LinkedList<Server>();
    LinkedList<Server> busyServers = new LinkedList<Server>();
//    Timer timer = new Timer();
    int maxServerNum = 1;
    ExecutorService executor;

    public ServeProcess(int maxServerNum) {
        this.maxServerNum = maxServerNum;
//        this.system = system;
//        this.miu=miu;
//        random = new Random();
//        timer.
        executor = Executors.newFixedThreadPool(maxServerNum);


    }

    /**
     * 必须要在最后结束线程池!!
     */
    public void closeServerProcess() {
        executor.shutdown();
    }

    public void addListener(ServerListener listener) {
//        this.listener=listener;
        listeners.add(listener);
    }

    public void releaseServer(Server server) {
//        public void releaseServer(Server server,Client client) {

        freeServers.add(server);
        busyServers.remove(server);
        for (ServerListener listener : listeners) {
            listener.notifyReleased();
        }
//        system.notifyServerReleased();
//        if(listener!=null){
//            listener.notifyReleased();
//        }
    }

    public void addServer(Server server) {
        if (freeServers.size() + busyServers.size() == maxServerNum) {
            System.out.println("超过服务员最多数目，不能添加！");
            return;
        }
        freeServers.add(server);


    }

    public boolean canServeClient(Client client) {
        if (!freeServers.isEmpty()) {
            Server s = freeServers.getFirst();
            freeServers.removeFirst();
            busyServers.add(s);
            s.acceptClient(client);
            executor.execute(s);            
            //告诉系统客户已经接受服务
            for (ServerListener listener : listeners) {
                listener.notifyClientAccepted(client);
            }
//            system.notifyClientAccepted(client);
            return true;
        }
        return false;
    }
}
