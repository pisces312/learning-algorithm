/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Administrator
 */
public class MM1System extends QSystem {
//供组合多个mm1系统时使用

    public MM1System(String name, ServerListener listener, double miu) {
        this(name, null, listener, miu);
    }

    public MM1System(String name, ArriveProcess arriveProcess, double miu) {
        this(name, arriveProcess, null, miu);
    }

    private MM1System(String name, ArriveProcess arriveProcess, ServerListener listener, double miu) {
        this.name = name;
        if (arriveProcess != null) {
            this.arriveProcess = arriveProcess;
            arriveProcess.addSystem(this);
        }
//        arriveProcess = new ArriveProcess(0.2, 10);
        serveProcess = new ServeProcess(1);
        //这里加入两个监视器，其中一个给外层
        if (listener != null) {
            serveProcess.addListener(listener);
        }
        serveProcess.addListener(this);

        serveProcess.addServer(new Server("1", miu, serveProcess));

        serverNum = 1;
//        servers.add(new Server());
        queueLengthLimited = -1;
    }
}
