
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *假设没有同时到来的情况
 * @author Administrator
 */
public class ArriveProcess extends Thread {

    int maxClientNum;
    int count = 0;
    double lamda;
    //同时作为多个系统的到达过程，提供相同的源
    LinkedList<QSystem> systems = new LinkedList<QSystem>();
//    Timer timer = new Timer();

    public ArriveProcess(double lamda, int max) {
//        this.system = system;
        this.lamda = lamda;
        this.maxClientNum = max;
//        random = new Random();
//        timer.

    }

    /**
     * 向从该到达过程得到顾客的所有系统进行通知
     */
//    public void notifyClientAccepted(){
//
//    }
    public int getTotalClientNum() {
        return maxClientNum;
    }
//    public void simulate(){
//
//    }

    /**
     * 可以同时给多个系统提供相同的顾客源
     * @param system
     */
    public void addSystem(QSystem system) {
        systems.add(system);
    }

    public long getClientArriveTimeInterval() {
        return (long) (500 * RandomUtils.randomE(lamda));
//        system.

    }

    @Override
    public void run() {
        while (count < maxClientNum) {
            count++;
            long time = getClientArriveTimeInterval();
            System.out.println("时间间隔:" + time);
            try {
//                wait(time);//不能用wait
                sleep(time);
            } catch (InterruptedException ex) {
                Logger.getLogger(ArriveProcess.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (QSystem system : systems) {
                //每个系统给一个新创建的客户
                //可以做一个顾客池
                system.processClient(new Client(String.valueOf(count)));
            }

            System.out.println("arrive client");
        }
    }
    //所有客户已经产生
}
