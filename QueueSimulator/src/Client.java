/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Administrator
 */
public class Client {

    long arriveTime;
    long acceptedTime = -1;
    long waitTime = -1;
    String name;
    //一个顾客实例只能给一个系统处理
    QSystem system;
    //保存是由哪个到达过程产生的
//    ArriveProcess arriveProcess;

    public Client(String name/**,ArriveProcess arriveProcess*/
            ) {
        this.name = name;
//        this.arriveProcess=arriveProcess;
//        arriveTime = System.currentTimeMillis();
    }

    /**
     * 到达一个系统
     * @param system
     */
    public void arrived(QSystem system) {
        this.system = system;
        this.arriveTime = System.currentTimeMillis();
    }

    public void accepted() {
        this.acceptedTime = System.currentTimeMillis();
        waitTime = acceptedTime - arriveTime;
//        arriveProcess.
        system.notifyClientAccepted(this);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("client[" + name + "] arriveTime:" + arriveTime);
        if (acceptedTime != -1) {
            sb.append("  接受服务时刻:" + acceptedTime + " 等待时间:" + waitTime);
        }
        return sb.toString();
    }
}
