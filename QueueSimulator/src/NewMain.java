
import java.util.Arrays;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class NewMain {

    public static void printLine(int len) {
        for (int i = 0; i < len; i++) {
            System.out.print("=");
        }
        System.out.println(" " + len);
    }

    public static void testExpRandom() {
        int num = 50;
        double lamda = 0.2;
        double[] array = new double[num];
//        PriorityQueue<Double> queue=new PriorityQueue<Double>();
        ArriveProcess arriveProcess = new ArriveProcess(lamda, num);
        QSystem system = new MM1System("M|M|1", arriveProcess, 0.2);

        arriveProcess.addSystem(system);
        for (int i = 0; i < num; i++) {
            double d = RandomUtils.randomE(lamda);
//            queue.add(d);
//            System.out.println(d);
            array[i] = d;
//            System.out.println(arriveProcess.randomE(0.1));
        }
        Arrays.sort(array);
        int[] count = new int[(int) (array[num - 1] + 1)];
        Arrays.fill(count, 0);
        for (int i = 0; i < num; i++) {
            System.out.println(array[i]);
            count[(int) array[i]]++;
        }
        System.out.println();
        for (int i = 0; i < count.length; i++) {
            printLine(count[i]);
//            System.out.println(count[i]);
        }
//        System.out.println(Arrays.toString(array));
//        double r = 0.1;
////        double r = 0.0001;
//        long t = System.currentTimeMillis();
//        System.out.println(r * t);
//        System.out.println(t);
//        System.out.println(exponentialDistribution(r, 10));

    }

    public static void test1() {
        double lamda = 3;//0.8;
        double miu = 0.5;
        int clientNum = 20;
        int n = 3;
        ArriveProcess arriveProcess1 = new ArriveProcess(lamda / n, clientNum);
        ArriveProcess arriveProcess2 = new ArriveProcess(lamda, clientNum);
        QSystem system1 = new MM1System("M|M|1", arriveProcess1, miu);
        QSystem system2 = new MMNSystem("M|M|n", arriveProcess2, miu, n);
        arriveProcess1.start();
        arriveProcess2.start();
    }

    public static void test2() {
//        double lamda = 0.8;//0.1;//0.2;//0.3;//0.5;
//        double miu = 0.667;
//        int clientNum = 10;
//        int n = 2;


        double lamda = 5;//0.8;
        double miu = 0.5;
        int clientNum = 20;
        int n = 3;
        //使用一个流
        ArriveProcess arriveProcess = new ArriveProcess(lamda, clientNum);
//        QSystem mm1System = new MM1System("M|M|1", arriveProcess);
        QSystem nmm1System = new NMM1System("n-M|M|1", arriveProcess, miu, n);
        QSystem mmnSystem = new MMNSystem("M|M|n", arriveProcess, miu, n);
        arriveProcess.start();

    }
    public static void testNMMN(){
        double lamda = 0.8;//0.1;//0.2;//0.3;//0.5;
        double miu = 0.667;
        int clientNum = 10;
        int n = 2;
        //使用一个流
        ArriveProcess arriveProcess = new ArriveProcess(lamda, clientNum);
//        QSystem mm1System = new MM1System("M|M|1", arriveProcess);
        QSystem nmm1System = new NMM1System("n-M|M|1", arriveProcess, miu, n);
        arriveProcess.start();
    }

    public static void testMM1() {
        double lamda = 3;
        double miu = 0.5;
        int clientNum = 20;
        ArriveProcess arriveProcess1 = new ArriveProcess(lamda, clientNum);
        QSystem system1 = new MM1System("M|M|1", arriveProcess1, miu);
        arriveProcess1.start();
    }

    public static void testMMN() {
        double lamda = 1;//0.1;//0.2;//0.3;//0.5;
        double miu = 0.667;
        int clientNum = 10;
        int n = 2;
        //使用一个流
        ArriveProcess arriveProcess = new ArriveProcess(lamda, clientNum);
        QSystem mmnSystem = new MMNSystem("M|M|n", arriveProcess, miu, n);
        arriveProcess.start();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        testNMMN();
//        testMMN();
//        testMM1();
//        test1();
        test2();
    }
}
