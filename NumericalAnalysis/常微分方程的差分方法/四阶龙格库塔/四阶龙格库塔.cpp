#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
double f(double x,double y)
{
	//y'=y-2*x/y;
	//y(0)=1;
	return y-2*x/y;
}
double yf(double x)
{
	return sqrt(1+2*x);
}
void RungeKutta(double x0,double y0,double h,int n)
{
	double k1,k2,k3,k4;
	cout<<"�Ľ����������ʽ\n";
	cout<<setw(3)<<"xn"<<setw(10)<<"yn"<<setw(10)<<"y(n)\n";
	for(int i=0;i<n;i++)
	{
		k1=f(x0,y0);
		x0+=0.5*h;
		k2=f(x0,y0+h*k1/2);
		k3=f(x0,y0+h*k2/2);
		x0+=0.5*h;
		k4=f(x0,y0+h*k3);
		y0=y0+h*(k1+2*k2+2*k3+k4)/6;
		cout<<setw(4)<<x0<<" "<<setw(10)<<y0<<setw(10)<<yf(x0)<<endl;
	}
}
void main()
{
	double x=0,y=1,h=0.1,n=10;
	RungeKutta(x,y,h,n);
}