#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
double f(double x,double y)
{
	//y'=y-2*x/y;
	//y(0)=1;
	return y-2*x/y;
}
double yf(double x)
{
	return sqrt(1+2*x);
}
void mEuler(double x0,double y0,double h,int n)
{
	double yp,yc;
	cout<<"改进的欧拉折线法\n";
	cout<<setw(3)<<"xn"<<setw(10)<<"yn"<<setw(10)<<"y(n)\n";
	for(int i=0;i<n;i++)
	{

		yp=y0+h*f(x0,y0);		
		x0+=h;
		yc=y0+h*f(x0,yp);
		y0=(yp+yc)/2;
		cout<<setw(4)<<x0<<" "<<setw(10)<<y0<<setw(10)<<yf(x0)<<endl;
	}

}
void main()
{
	double x=0,y=1,h=0.1,n=10;
	mEuler(x,y,h,n);

}