#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
double f(double x,double y)
{
	//y'=y-2*x/y;
	//y(0)=1;
	return y-2*x/y;
}
double yf(double x)
{
	return sqrt(1+2*x);
}
void mEuler(double x0,double y0,double h,int n,double r=0.5,double p=1)
{
	double k1,k2;
	cout<<"r="<<r<<"  p="<<p<<"时的二阶龙格库塔格式\n";
	cout<<setw(3)<<"xn"<<setw(10)<<"yn"<<setw(10)<<"y(n)\n";
	for(int i=0;i<n;i++)
	{
		k1=f(x0,y0);
		x0+=p*h;
		k2=f(x0,y0+p*h*k1);
		y0=y0+h*((1-r)*k1+r*k2);
		cout<<setw(4)<<x0<<" "<<setw(10)<<y0<<setw(10)<<yf(x0)<<endl;
	}

}
void main()
{
	double x=0,y=1,h=0.1,n=10;
	mEuler(x,y,h,n);
	mEuler(x,y,h,n,1,0.5); //?????????

}