#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
/*double f(double x)
{
	if(x==0)
		return 1;
	return sin(x)/x;
	
}*/
double f(double x)
{
	return 4.0/(1+x*x);
}
double integration(double a,double b,int n)
{
	double h=(b-a)/n,s=0;
	for(int i=1;i<n;i++)
		s+=f(a+i*h);
	s=h*(s+(f(a)+f(b))/2);
	return s;
}
double integration(double a,double b,double e)
{
	double h=sqrt(e),s=0;
	int n=(b-a)/h;
	for(int i=1;i<n;i++)
		s+=f(a+i*h);
	s=h*(s+(f(a)+f(b))/2);
	return s;
}
void main()
{
	double a=0,b=1,e=0.000001;
	int n=100;
	//cout<<setprecision(16)<<integration(a,b,n)<<endl;
	cout<<setprecision(16)<<integration(a,b,e)<<endl;
}