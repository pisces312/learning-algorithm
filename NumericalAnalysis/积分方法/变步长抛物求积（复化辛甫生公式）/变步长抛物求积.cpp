#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
double f(double x)
{
	if(x==0)
		return 1;
	return sin(x)/x;
}
double integration(double a,double b,double e)
{
	double h=b-a;
	double p=f(a)+f(b);
	double s2=p*h/2,s1,g,t=0,x;
	int count=1;
	cout<<"变步长抛物求积\n";
	do{
		s1=s2;
		g=0;
		for(h/=2,x=a+h;x<b-0.5*h;x+=2*h)
		{
			cout<<"x="<<x<<endl;
			g+=f(x);
		}
		s2=(p+4*g+2*t)*h/3;
		t+=g;
		cout<<"S"<<count<<"="<<setprecision(16)<<s2<<endl;
		count*=2;
	}while(fabs(s1-s2)>=e);

	/*for(;;)
	{
		s1=s2;
		h/=2;
		g=0;
		for(x=a+h;x<b-h/2;x+=2*h)
			g+=f(x);
		s2=(p+4*g+2*t)*h/3;
		t+=g;
		cout<<"S"<<count<<"="<<setprecision(16)<<s2<<endl;
		count*=2;
		if(fabs(s1-s2)<e) break;
	}*/
 	return s2;
}
double integration2(double a,double b,double e)
{
	double h=b-a;
	double p=f(b)-f(a);
	double s2=p*h/2,s1,g,t=f((a+b)/2),x;
	int count=1;
	cout<<"变步长抛物求积\n";
	do{
		s1=s2;
		g=0;
		for(h/=2,x=a+h;x<b-0.5*h;x+=2*h)
		{
			cout<<"x="<<x<<endl;
			g+=f(x);
		}
		//if(count==4) break;
		
		s2=(p+4*t+2*g)*h/3;
		t+=g;
		cout<<"S"<<count<<"="<<setprecision(16)<<s2<<endl;
		count*=2;
	}while(fabs(s1-s2)>=e);
	return s2;
}
void main()
{
	double a=0,b=1,e=0.001;
	cout<<integration(a,b,e)<<endl;
	//cout<<integration2(a,b,e)<<endl;
}