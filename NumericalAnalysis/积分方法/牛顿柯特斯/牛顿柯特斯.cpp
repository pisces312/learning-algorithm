#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
double f(double x)
{
	if(x==0)
		return 1;
	return sin(x)/x;
		//return 4.0/(1+x*x);
}
double integration(double a,double b)
{
	double h=b-a,p=f(a)+f(b),s=7*(f(a)+f(b));
	h/=4;
	s=4*h*(s+32*f(a+h)+12*f(a+2*h)+32*f(a+3*h))/90;  //4h!!!
	cout<<setprecision(16)<<s<<endl;
 	return s;

}
void main()
{
	double a=0,b=1,e=0.001;
	cout<<integration(a,b)<<endl;
}