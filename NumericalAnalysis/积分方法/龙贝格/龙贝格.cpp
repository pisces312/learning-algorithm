#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
/*double f(double x)
{
	if(x==0)
		return 1;
	return sin(x)/x;
}*/
double f(double x)
{
	return 4.0/(1+x*x);
}
/*double Romberg(double a,double b,double e)
{
	double x,s=0,h=b-a,p=(f(a)+f(b))/2;
	double t2=p*h;
	double s2=t2,c2=t2,r2=t2,t1,s1,c1,r1;
	//cout<<"T1="<<t1<<endl;
	int count=1;
	cout<<"龙贝格\n";
	do{
		t1=t2,s1=s2,c1=c2,r1=r2;
		for(h/=2,x=a+h;x<b-0.5*h;x+=2*h)//理论上为x<=b-h,但h为过剩值
			s+=f(x);
		t2=h*(p+s);
		s2=t2+(t2-t1)/3;
		c2=s2+(s2-s1)/15;
		r2=c2+(c2-c1)/63;
		cout<<"R"<<count<<"="<<setprecision(16)<<r2<<endl;
		count*=2;
	}while(fabs(r1-r2)>=e);

	return r2;
}*/
double Romberg(double a,double b,double e)
{
	double x,s=0,h=b-a,p=(f(a)+f(b))/2;
	double t2=p*h;
	double s2=t2,c2=t2,r2=t2,t1,s1,c1,r1;
	int tcount=1,scount=1,ccount=1,rcount=1,k=0;
	cout<<"龙贝格\n";
	cout<<"T"<<setprecision(16)<<tcount<<endl;
	for(;;)
	{
		tcount*=2;
		t1=t2;
		for(h/=2,x=a+h;x<b-0.5*h;x+=2*h)//理论上为x<=b-h,但h为过剩值
			s+=f(x);
		t2=h*(p+s);
		cout<<"T"<<tcount<<" ";
		if(k>=0)
		{
			
			cout<<"S"<<scount<<" ";
			scount*=2;
			s1=s2;
			s2=t2+(t2-t1)/3;
			if(k==0)
			{
				k++;
				cout<<endl;
				continue;
			}
		}
		if(k>=1)
		{
			
			cout<<"C"<<ccount<<" ";
			ccount*=2;
			c1=c2;
			c2=s2+(s2-s1)/15;
			if(k==1)
			{
				cout<<endl;
				k++;
				continue;
			}
		}
		if(k==2)
		{
			cout<<"R"<<rcount<<endl;
			rcount*=2;
			r1=r2;
			r2=c2+(c2-c1)/63;
			//cout<<"R"<<count<<"="<<setprecision(16)<<r2<<endl;
			if(fabs(r1-r2)<e)
				break;
		}
	}	
	return r2;
}
void main()
{
	double a=0,b=1,e=0.000001;
	cout<<"精度为:"<<e<<"的龙贝格积分值为:\n"<<Romberg(a,b,e)<<endl;
}