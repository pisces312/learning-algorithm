#include <iostream.h>
//定步长梯形求积分

//所求积分fx=x;
double Fnx(double x)
{
	return x;
}

double T(double a,double b,int n,int right)
{
	double h,s;
	double *y;
	y=new double[n];
	h=(b-a)/n;
	if(right==0)
	{
		s=(Fnx(a)+Fnx(b))/2;
		for(int i=1;i<n-1;i++)
			s=s+Fnx(a+h*i);
	}
	else
	{
		for(int i=0;i<=n;i++)
			cin>>y[i];
		s=(y[0]+y[n])/2;
		for(int j=1;j<n-1;j++)
			s=s+y[j];
	}
	return s*h;
}

void main()
{
	double a,b;
	int n,right;
	cout<<"输入积分上下限: ";
	cin>>a>>b;
	cout<<"输入步长: ";
	cin>>n;
	cout<<"判断列表(1)还是函数(0): ";
	cin>>right;
	cout<<"所求积分在["<<a<<","<<b<<"]的积分值为: "<<T(a,b,n,right)<<endl;
}