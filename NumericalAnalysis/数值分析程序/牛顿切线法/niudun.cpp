#include <iostream.h>
#include <math.h>

//牛顿切线法求方程的根！
//方程为x*exp(x)-1=0
//则求的Fx(x0)/Fx0(x0)=(x0-exp(-x0))/(1+x0)

double Fx(double x0)
{
	return (x0-exp(-x0));
}

double Fx0(double x0)
{
	return (1+x0);
}

double progress(double x0,double l,int n)
{
	double x1;
	int k=1,i=1;
	while(k!=n)
	{
		if(Fx0(x0)!=0)
		{
			x1=x0-Fx(x0)/Fx0(x0);
			if(fabs(x0-x1)<l)
			{
				cout<<"经过"<<i<<"次迭代"<<endl;
				return x1;
			}
			i++;
			k+=1;
			x0=x1;
		}
		else
		{
			return -1;
		}
	}
	if(k==n)
		return -2;
}

void main()
{
	double x0,e,y;
	int n;
	cout<<"请输入x0: ";
	cin>>x0;
	cout<<"请输入精度e: ";
	cin>>e;
	cout<<"请输入循环次数n: ";
	cin>>n;
	cout<<endl;
	y=progress(x0,e,n);
	if(y==-1)
		cout<<"error!"<<endl;
	else {
		if(y==-2)
			cout<<"failed!"<<endl;
		else
			cout<<"所得的根为: "<<y<<endl;
	}
}