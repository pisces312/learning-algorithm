#include <iostream.h>
#include <iomanip.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int n;
double *x,*y;
double Lagrange(double X)
{
	int i;double r=1,result=0;
	if(X<x[1]) 
		i=1;
	else if(X>x[n-1])
		i=n-1;
	else
		for(int j=1;j<n-2;j++)
		{
			if(X>x[j]&&X<x[j+1]) 
				if(fabs(X-x[j])<fabs(X-x[j+1]))
					i=j;
				else
					i=j+1;
		}
	for(int k=i-1;k<=i+1;k++)
	{	
		r=1;
		for(int j=i-1;j<=i+1;j++)
		{
			if(j!=k) 
				r*=(X-x[j])/(x[k]-x[j]);
		}
		result+=r*y[k];
	}
	return result;
}


void main()
{
	double tX;
	char opt;
	char *temp;
	int op;
	cout<<"已知几组数据? ";
	cin>>n;
	x=new double[n];
	y=new double[n];
	for(int i=0;i<n;i++)
	{
		cout<<"第"<<i+1<<"组x["<<i<<"] : "<<endl;
		cin>>x[i];
		cout<<"y["<<i<<"] : "<<endl;
		cin>>y[i];		
	}
	do{
		cout<<"数据输入是否正确? (y 正确; 1-n 需修改的数据组号)";
		cin>>opt;
		opt=tolower(opt);
		if(opt!='y') 
		{
			temp=new char[2];
			temp[0]=opt;
			temp[1]='\0';
			op=atoi(temp);
			if(op>0 && op<=n)
			{
				cout<<"第"<<op<<"组x["<<op-1<<"] : ";
				cin>>x[op-1];
				cout<<"y["<<op-1<<"] : ";
				cin>>y[op-1];
			}
		}
	}while(opt!='y');
	do{
		cout<<"\n(x TO y) x = ";
		cin>>tX;
		if(tX!=-1)
		{
			cout<<"运算结果: "
			<<setiosflags(ios::fixed)<<setprecision(8)
			<<Lagrange(tX)<<endl;
		}
	}while(tX!=-1);

}
