#include <iostream.h>
#include <math.h>
#include <iomanip.h>
double f1(double x)
 {
	return (x==0)?1.00:sin(x)/x;		
 }
void main()
{
	double a,b,eps,S,x,h,T1=0,T2=0;
	int e;
	cout<<"积分区间[a,b]= ";
	cin>>a>>b;
	cout<<"精确到小数几位? ";
	cin>>e;
	eps=pow(10,-e)/2;
	h=b-a;
	T1=h/2*(f1(a)+f1(b));
	do{
		T1=T2;
		S=0;
		x=a+h/2;
		do{
			S=f1(x)+S;
			x=h+x;
		}while(x<b);
		T2=T1/2+h*S/2;
		h/=2;
		cout<<setiosflags(ios::fixed)<<setprecision(e+1)
			<<"T1="<<T1<<"T2="<<T2<<endl;
	}while(fabs(T2-T1)>=eps);
	cout<<setiosflags(ios::fixed)<<setprecision(e+1)
		<<"结果: "<<T2<<endl;
}