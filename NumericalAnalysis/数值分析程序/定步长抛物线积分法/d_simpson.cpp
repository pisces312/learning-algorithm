//定步长抛物线积分法

#include <iostream.h>
#include <math.h>

double fx(double x)
{
	if(x==0)
		return 1;
	else
		return sin(x)/x;
}

double simpson(double a,double b,int n)
{
	double s=fx(a)+fx(b);
	double h=(b-a)/n;
	for(int i=1;i<n;i+=2)
		s=s+4*fx(a+i*h);
	for(int j=2;j<n;j+=2)
		s=s+2*fx(a+j*h);
	return s*h/3;
}

void main()
{
	double a,b;
	int n;
	cout<<"请输入函数的区间: ";
	cin>>a>>b;
	cout<<"请输入步长: ";
	cin>>n;
	while(n%2!=0)
	{
		cout<<"步长必须为偶数!请重新输入: ";
		cin>>n;
	}
	cout<<"积分结果为: "<<simpson(a,b,n)<<endl;
}