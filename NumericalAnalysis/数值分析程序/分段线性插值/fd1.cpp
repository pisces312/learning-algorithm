#include <iostream.h>
#include <iomanip.h>
int n;float *x,*y;
float fd1(float X)
{
	float result;int i;
	if(X<x[1]) 
		i=1;
	else if(X>x[n-1])
		i=n-1;
	else
		for(int j=1;j<n-1;j++)
		{if(X>x[j]&&X<x[j+1]) i=j+1;}
	result=y[i]+(y[i]-y[i-1])/(x[i]-x[i-1])*(X-x[i]);
	return result;
}
void main()
{
	float tX;
	cout<<"x = ";
	cin>>tX;
	cout<<"已知几组数据? ";
	cin>>n;
	x=new float[n];
	y=new float[n];
	for(int i=0;i<n;i++)
	{
		cout<<"第"<<i+1<<"组x["<<i<<"] : ";
		cin>>x[i];
		cout<<"y["<<i<<"] : ";
		cin>>y[i];
	}
	cout<<"运算结果: "
		<<setiosflags(ios::fixed)<<setprecision(8)
		<<fd1(tX)<<endl;
}