/*
  f(x)=x*x*x-x-1,则g(x)=(x+1)开3次方根,q取g(xk)的一阶导数.(xk为根的某个近似值)
  这里xk=1.5,则q=0.180961174
*/

#include <iostream.h>
#include <math.h>

double gx(double x0)
{
	double y;
	y=x0+1;
	y=log10(y);
	y=y/3;
	y=pow(10,y);
	return y;
}

double quick(double x0,double e,double q)
{
	double x,x1;
	do
	{
		x=gx(x0);
		x1=x+(q/(1-q))*(x-x0);
		x0=x1;
	}while(fabs(x1-x0)>e);
	return x1;
}

void main()
{
	double x0,e,q;
	cout<<"输入近似值x0: ";
	cin>>x0;
	cout<<"输入精度e: ";
	cin>>e;
	cout<<"输入g("<<x0<<")的一阶导数q值(q=0.180961174): ";
	cin>>q;
	cout<<quick(x0,l,q)<<endl;
}