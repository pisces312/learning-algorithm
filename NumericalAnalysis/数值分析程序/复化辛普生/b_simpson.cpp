//变步长抛物线积分法

#include <iostream.h>
#include <math.h>
#include <iomanip.h>

double fx(double x)
{
	return 4/(1+x*x);
}

double simpson(double a,double b,int e,int n,int m)
{
	double s1,s2,s,h,t,l;
	l=pow(10,-e);
	s1=0;
	h=(b-a)/n;
	s=fx(a)+fx(b);
	while(n<m)
	{
		t=0;
		for(int i=1;i<n;i+=2)
		{
			t=t+fx(a+i*h);
		}
		s=s+4*t;
		s2=s*h/3;
		cout<<"(s2-s1)="<<setiosflags(ios::fixed)<<setprecision(e)<<fabs(s2-s1)<<endl;
		if(fabs(s2-s1)<l)
			return s2;
		else
		{
			s1=s2;
			s=s-2*t;
			h=h/2;
			n=n*2;
		}
	}
	cout<<"步长越界!!!"<<endl;
	return s2;
}

void main()
{
	double a,b,y;
	int n,m,e;
	cout<<"请输入积分函数的区间: ";
	cin>>a>>b;
	cout<<"请输入精度的位数: ";
	cin>>e;
	cout<<"请输启始步长大小: ";
	cin>>n;
	while(n%2!=0)
	{
		cout<<"步长必须是偶数，请重新输入: ";
		cin>>n;
	}
	cout<<"请输入最大步长大小: ";
	cin>>m;
	y=simpson(a,b,e,n,m);
	cout<<"积分的值为: "<<setiosflags(ios::fixed)<<setprecision(e)<<y<<endl;
}