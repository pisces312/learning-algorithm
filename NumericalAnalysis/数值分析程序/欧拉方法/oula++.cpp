#include <iostream.h>
#include <iomanip.h>
#include <math.h>

double fx(double x,double y)
{
	return (y-2*x/y);
}

double fxn(double x)
{
	return sqrt(1+2*x);
}

void oula_adv(double x0,double y0,double h,int N)
{
	double yp,yc,x1,y1;
	for(int n=1;n<=N;n++)
	{
		x1=x0+h;
		yp=y0+h*fx(x0,y0);
		yc=y0+h*fx(x1,yp);
		y1=(yp+yc)/2;

		cout<<"x("<<n<<")= "<<setiosflags(ios::fixed)<<setprecision(1)<<x1<<'\t';
		cout<<"y("<<n<<")= "<<setiosflags(ios::fixed)<<setprecision(4)<<y1<<'\t'<<"  ";
		cout<<"yn("<<n<<")= "<<setiosflags(ios::fixed)<<setprecision(4)<<fxn(x1)<<endl;
		x0=x1;
		y0=y1;
	}
}

void main()
{
	double x0,y0,h;
	int N;
	cout<<"输入初始值(x0,y0): ";
	cin>>x0>>y0;
	cout<<"输入步长: ";
	cin>>h;
	cout<<"输入步数: ";
	cin>>N;
	oula_adv(x0,y0,h,N);
}