#include <iostream.h>
#include <iomanip.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int n;
double *x,*y;
double Lagrange(double X)
{
	double r=1,result=0;
	for(int k=0;k<n;k++)
	{	
		r=1;
		for(int j=0;j<n;j++)
		{if(j!=k) r*=(X-x[j])/(x[k]-x[j]);}
		result+=r*y[k];
	}
	return result;
}
double unLag(double Y)
{
	double a,b,result=-1;
	cout<<"输入a,b: ";	cin>>a>>b;
	for(double i=a;i<b;i+=0.0000001)
	{if(fabs(Lagrange(i)-Y)<0.00000005) result= i;}
	return result;
}

void main()
{
	double tX,tY;
	char opt;char *temp;int op;
	cout<<"已知几组数据? ";cin>>n;
	x=new double[n];
	y=new double[n];
	x[0]=0.46;x[1]=0.47;x[2]=0.48;x[3]=0.49;
	y[0]=0.4846555;y[1]=0.4937452;y[2]=0.5027498;y[3]=0.5116683;

	do{
		cout<<"\n数据输入是否正确? (y 正确; 1-n 需修改的数据组号)";
		cin>>opt;
		opt=tolower(opt);
		if(opt!='y') 
		{
			temp=new char[2];
			temp[0]=opt;
			temp[1]='\0';
			op=atoi(temp);
			if(op>0 && op<=n)
			{
				cout<<"第"<<op<<"组x["<<op-1<<"] : ";
				cin>>x[op-1];
				cout<<"y["<<op-1<<"] : ";
				cin>>y[op-1];
			}
		}
	}while(opt!='y');
	do{
		cout<<"(x TO y) x = ";
		cin>>tX;
		if(tX!=-1)
		{
			cout<<"运算结果: "
			<<setiosflags(ios::fixed)<<setprecision(8)
			<<Lagrange(tX)<<endl;
		}
	}while(tX!=-1);
	cout<<"(y TO x) y = ";
	cin>>tY;
	cout<<unLag(tY)<<endl;
}
