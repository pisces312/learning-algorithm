//拉格朗日插值插值公式求根

#include <iostream.h>

double la(int n,double x,double x1[],double y1[])//拉格朗日插值插值公式主函数
{
	int k;
	double y=0,t;
	for(k=0;k<=n;k++)
	{
		t=1;
		for(int j=0;j<=n;j++)
		{
			if(j!=k)
				t=t*((x-x1[j])/(x1[k]-x1[j]));
		}
		y=y+t*y1[k];
	}
	return y;
}

double rela(int n,double x,double x1[],double y1[],int p)//反向求解x
{
	double y=la(n,x,x1,y1);
	double c=y-p;
	while(c>=0.0005||c<=-0.0005)
	{

		x+=0.001;
		y=la(n,x,x1,y1);
		c=y-p;
	}
	return x;
}

void main()
{
	int n;
	cout<<"input the value of n : ";
	cin>>n;
	cout<<endl;
	double x;
	double *x1;
	double *y1;
    x1=new double[n+1];
	y1=new double[n+1];
	cout<<"input the value of x : ";
	cin>>x;
	cout<<endl;
	for(int i=0;i<=n;i++)
	{
		cout<<"input the value of x["<<i<<"] : ";
		cin>>x1[i];
		cout<<"input the value of y["<<i<<"] : ";
		cin>>y1[i];
		cout<<endl;
	}
	cout<<"when the value of x is "<<x<<" the result is "<<la(n,x,x1,y1)<<endl;
	cout<<endl;
	int p;
	cout<<"in put the result : "<<endl;
	cin>>p;
	cout<<"when the result is "<<p<<",the vaule of x is "<<rela(n,x,x1,y1,p)<<endl;
}