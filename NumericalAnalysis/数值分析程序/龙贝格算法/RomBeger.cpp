#include <iostream.h>
#include <iomanip.h>
#include <math.h>

double fx(double x)
{
	return 4/(1+x*x);
}

double rombeger(double a,double b,int e)
{
	double h,s,s1,s2,t1,t2,x,c1,c2,r1,r2,l;
	int k;
	l=pow(10,-e)/2;
	s2=t2=c2=r2=0;
	k=0;
	h=b-a;
	t1=h*(fx(a)+fx(b))/2;
	do
	{
		if(k>=3)
		{
			r1=r2;
			c1=c2;
			t1=t2;
			s1=s2;
			h/=2;
		}
		k++;
		
		s=0;
		x=a+h/2;
		while(x<b)
		{
			s=s+fx(x);
			x=x+h;
		}
		t2=t1/2+h*s/2;
		cout<<"t2="<<setiosflags(ios::fixed)<<setprecision(e)<<t2<<endl;
		s2=t2+(t2-t1)/3;
		cout<<"s2="<<setiosflags(ios::fixed)<<setprecision(e)<<s2<<endl;
		if(k==1)
		{
			t1=t2;
			s1=s2;
			h/=2;
		}
		if(k>1)
			c2=s2+(s2-s1)/15;
		cout<<"c2="<<setiosflags(ios::fixed)<<setprecision(e)<<c2<<endl;
		if(k==2)
		{
			c1=c2;
			t1=t2;
			s1=s2;
			h/=2;
		}
		if(k>2)
			r2=c2+(c2-c1)/63;
		cout<<"r2="<<setiosflags(ios::fixed)<<setprecision(e)<<r2<<endl;

		cout<<endl;
	}while((fabs(r1-r2)>=l)&&(k<8));
	return r2;
}

void main()
{
	double a,b;
	int e;
	cout<<"请输入积分区间: ";
	cin>>a>>b;
	cout<<"请输入计算精度: ";
	cin>>e;
	cout<<endl;
	cout<<"积分结果为: "<<setiosflags(ios::fixed)<<setprecision(e)<<rombeger(a,b,e)<<endl;
}