#include <iostream.h>
#include <math.h>
#include <iomanip.h>

double Fxn(double x)
{
	if(x==0)
		return 1;
	else
		return sin(x)/x;
}

double CT(double a,double b,int n,int l)
{
	double s,h,x,Tn,T2n,lx;
	lx=pow(10,-l);
	s=(Fxn(a)+Fxn(b))/2;
	h=(b-a)/n;
	x=a+h;
	for(int i=1;i<n;i++)
	{
		s=s+Fxn(x);
		x=x+h;
	}
	Tn=s*h;
	cout<<endl;
	while(1)
	{
		x=a+h/2;
		for(int i=1;i<=n;i++)
		{
			s=s+Fxn(x);
			x=x+h;
		}
		T2n=s*h/2;
		if(fabs(T2n-Tn)<lx)
		{
			break;
		}
		h=h/2;
		n=2*n;
		Tn=T2n;
	}
	return T2n;
}

void main()
{
	double a,b;
	int n,l;
	cout<<"输入积分上下限: ";
	cin>>a>>b;
	cout<<"输入积分的误差限: ";
	cin>>l;
	cout<<"输入积分步长: ";
	cin>>n;
	cout<<"所求积分值为: "<<setiosflags(ios::fixed)<<setprecision(l)<<CT(a,b,n,l)<<endl;
}