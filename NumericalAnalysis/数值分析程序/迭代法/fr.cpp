#include <iostream.h>
#include <math.h>
#include <iomanip.h>
double f1(double Xk)
{
	return exp(1.0000000/3.000000*log(Xk+1));
}
double f2(double Xk)
{
	return exp(-Xk);
}
double fr(double X0,double EPS,int n)
{
	int k=1;
	double X1,result;

	while(k<=n)
	{
		X1=f2(X0);
		if(fabs(X1-X0)<EPS)
		{
			result=X1;
			cout<<"经过"<<k<<"次迭代结果: ";
			k=n+1;
		}
		else
			if(k==n)
			{	
				cout<<"\n超过最大迭代次数, 迭代失败....."<<endl;
			    result=-1;k=n+1;
			}
			else
			{
				k++;
				X0=X1;
			}
	}
	return result;
}

void main()
{
	int tempTime,totalTime;
	double x0,eps,res;
	int N,digi;
	cout<<"初值x0 = ";
	cin>>x0;
	cout<<"精确到小数几位? ";
	cin>>digi;
	eps=pow(10,-digi)/2;
	cout<<"最大迭代次数N = ";
	cin>>N;
	cout<<setiosflags(ios::fixed)<<setprecision(digi)
		<<res<<endl;
	
}