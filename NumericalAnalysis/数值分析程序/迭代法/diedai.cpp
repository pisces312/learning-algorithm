//迭代法求方程的根

/*
  方程为x*x*x-x-1=0 区间[1,2]
  转化为迭代函数为x=(1+x)开3次方根,且一阶导数值小于1，收敛！
  再次转化为x=pow10(1/3log(x+1))
*/

#include <iostream.h>
#include <math.h>

double tx(double x)
{
	double y=x+1;
	y=log10(y);
	y=y/3;
	y=pow(10,y);
	return y;
}

double diedai(double x0,double l,int n)
{
	double x1;
	int k=1;
	while(k!=n)
	{
		x1=tx(x0);
		if(fabs(x1-x0)<l)
			return x1;
		else
		{
			k=k+1;
			x0=x1;
		}
	}
	return -1;
}

void main()
{
	double x0,l,y;
	int n;
	cout<<"输入初值x0: ";
	cin>>x0;
	cout<<"输入精度值l: ";
	cin>>l;
	cout<<"输入最大迭代次数n: ";
	cin>>n;
	cout<<endl;
	y=diedai(x0,l,n);
	if(y==-1)
		cout<<"Die dia is shi bai le!"<<endl;
	else
		cout<<"则方程的根为: "<<y<<endl;
}

