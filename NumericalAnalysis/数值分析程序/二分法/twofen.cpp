//二分法求方程的根

#include <iostream.h>

double fx(double x)
{
	double p=x*x*x-x-1;//待计算方程
//	cout<<"p="<<p<<endl;
	return p;
}

void twofen(double a,double b,double l)
{
	double x,y0,y;
	int i=0;
	while(b-a>=l||a-b>=l)
	{
		y0=fx(a);
		x=(a+b)/2;
		y=fx(x);
		if(y*y0<=0)
			b=x;
		else
			a=x;
//		cout<<"a="<<a<<"  "<<"b="<<b<<endl;
		i++;
//		cout<<"b-a="<<b-a<<endl;
	}
	cout<<"经过 "<<i<<" 次计算所得实根为："<<x<<endl;
	cout<<"此时Y值为："<<y<<endl;
}


void main()
{
	double a,b,l;
	cout<<"请输入方程区间：";
	cin>>a>>b;
	cout<<"请输入误差限：";
	cin>>l;
	twofen(a,b,l);
}

