#include <iostream.h>
#include <iomanip.h>
#include <math.h>

//四阶龙格-库塔方法

double fx(double x,double y)
{
	return (y-2*x/y);
}

double fxn(double x)
{
	return sqrt(1+2*x);
}

void Runge_Kutta(double x0,double y0,double h,int N)
{
	double k1,k2,k3,k4;
	for(int n=1;n<=N;n++)
	{
		double x1=x0+h;
		k1=fx(x0,y0);
		k2=fx(x0+h/2,y0+h*k1/2);
		k3=fx(x0+h/2,y0+h*k2/2);
		k4=fx(x1,y0+h*k3);
		double y1=y0+h*(k1+2*(k2+k3)+k4)/6;

		cout<<"x("<<n<<")= "<<setiosflags(ios::fixed)<<setprecision(1)<<x1<<'\t';
		cout<<"y("<<n<<")= "<<setiosflags(ios::fixed)<<setprecision(4)<<y1<<'\t'<<"  ";
		cout<<"yn("<<n<<")= "<<setiosflags(ios::fixed)<<setprecision(4)<<fxn(x1)<<endl;

		x0=x1;
		y0=y1;
	}
}

void main()
{
	double x0,y0,h;
	int N;
	cout<<"输入初始值(x0,y0): ";
	cin>>x0>>y0;
	cout<<"输入步长: ";
	cin>>h;
	cout<<"输入步数: ";
	cin>>N;
	Runge_Kutta(x0,y0,h,N);
}