#include<iostream>
#include<cmath>
#include<vector>

using namespace std;
class Item
{
public:
	vector<double> a;
	double x;
	int n;
	Item(vector<double> &a1,double x1,int n1)
	{
		
		x=x1;
		n=n1;
		a.resize(n);
		for(int i=0;i<n;i++)
		{
			a[i]=a1[i];
		}
	}
};

void p1(Item &n)
{
	double sum=0;
	cout<<"p1("<<n.x<<")=";
	for(int i=0;i<n.n;i++)
	{
		double temp=n.a[i]*pow(n.x,i);
		sum+=temp;
		cout<<temp;
		if(i!=n.n-1) cout<<"+";
	}
	cout<<"="<<sum<<endl;			
}
void p2(Item &n)
{
	double xn=1,sum=0;
	cout<<"p2("<<n.x<<")=";
	for(int i=0;i<n.n;i++)
	{
		double temp=n.a[i]*xn;
		sum+=temp;
		xn*=n.x;
		cout<<temp;
		if(i!=n.n-1) cout<<"+";
	}
	cout<<"="<<sum<<endl;			
}
void p3(Item &n)
{	
	double sum=n.a[n.n-1];
	cout<<"p3("<<n.x<<")=";
	for(int i=n.n-1;i>0;i--)		
		sum=(sum*n.x+n.a[i-1]);
	cout<<sum<<endl;
}
void main()
{

	int number=10;
	double x=3;
	//cin>>number;
	vector<double> num(number);
	double dim[10]={1,2,3,4,5,6,7,8,9,10};
	for(int i=0;i<number;i++)
	{
		num[i]=dim[i];
	}
	Item item(num,x,number);
	p1(item);
	p2(item);
	p3(item);
}