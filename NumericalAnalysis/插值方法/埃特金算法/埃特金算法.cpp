#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
double f(double x) //原函数
{
	return exp(-x)-x;
}
double g(double x)  //迭代函数
{
	return exp(-x);
}

bool findroot(double& a,double& b,double x0,double len,int n)
{
	double y0=f(x0),y1,y2;
	bool finished=true;
	if(y0==0)
		a=b=x0;
	else
	{
		for(int i=1;i<=n;i++)
		{
			finished=true;
			y1=f(x0+i*len);
			if(y1==0)
			{
				a=b=x0+i*len;
				break;
			}
			if(y1*y0<0)
			{
				a=x0+(i-1)*len;
				b=a+len;
				break;
			}
			y2=f(x0-i*len);
			if(y2==0)
			{
				a=b=x0-i*len;
				break;
			}
			if(y2*y0<0)
			{
				a=x0-i*len;
				b=a+len;
				break;
			}
			finished=false;
		}
	}
	if(!finished)
	{
		cout<<"根搜索失败!\n";
		return finished;
	}
	if(a==b)
		cout<<"根为"<<setprecision(16)<<a<<endl;
	else
		cout<<"根在"<<setprecision(16)<<a<<"～"<<b<<"范围"<<endl;
	return finished;
}
double Aitken(double x0,double e,int max=100)
{
	double xk1,xk2,temp;
	int n=0;
	do
	{
		temp=x0;
		xk1=g(x0);
		xk2=g(xk1);		
		x0=xk2-pow((xk2-xk1),2)/(xk2-2*xk1+x0);	
		cout<<setprecision(16)<<"X(k+1)="<<xk1<<"\nX(k+2)="<<xk2<<"\nX(k)="<<x0<<endl;
		cout<<"第"<<++n<<"次埃特金算法加速迭代得："<<x0<<endl;
	}
	while(fabs(temp-x0)>=e&&n<max);
	if(n==max) 
		cout<<"迭代函数发散或迭代次数超过最大次数限制！\n";
	else 
		cout<<"根为"<<x0<<endl;
	return x0;
}
void main()
{
	double a,b;
//	if(findroot(a,b,0,0.1,1000))
		Aitken(0.5,0.00001);

}