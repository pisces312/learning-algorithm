#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
double f(double x)
{
	//return exp(-x)-x+1;
	return x*x*x-x-1;
	
}
double g(double x)
{
	//return exp(-x)+1;
	return pow((x+1),1.0/3);
}
bool findroot(double& a,double& b,double x0,double len,int n)
{
	double y0=f(x0),y1,y2;
	bool finished=true;
	if(y0==0)
		a=b=x0;
	else
	{
		for(int i=1;i<=n;i++)
		{
			finished=true;
			y1=f(x0+i*len);
			if(y1==0)
			{
				a=b=x0+i*len;
				break;
			}
			if(y1*y0<0)
			{
				a=x0+(i-1)*len;
				b=a+len;
				break;
			}
			y2=f(x0-i*len);
			if(y2==0)
			{
				a=b=x0-i*len;
				break;
			}
			if(y2*y0<0)
			{
				a=x0-i*len;
				b=a+len;
				break;
			}
			finished=false;
		}
	}
	if(!finished)
	{
		cout<<"根搜索失败!\n";
		return finished;
	}
	if(a==b)
		cout<<"根为"<<setprecision(16)<<a<<endl;
	else
		cout<<"根在"<<setprecision(16)<<a<<"～"<<b<<"范围"<<endl;
	return finished;
}
double diedai(double x0,double e,int max=100)
{
	double x=g(x0);
	int n=0;
	do{
		x=x0;
		x0=g(x);
		cout<<"第"<<++n<<"次迭代得："<<setprecision(16)<<x0<<endl;
	}while(fabs(x-x0)>=e&&n<max);
	if(n==max) 
		cout<<"迭代函数发散或迭代次数超过最大次数限制！\n";
	else 
		cout<<"根为"<<x0<<endl;
	return x0;
}
void main()
{
	double a,b;
	if(findroot(a,b,0,0.1,1000))
		diedai(a,0.0001);
}
