#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
double g(double x,double c)
{
	return 0.5*(x+1.0*c/x);
}
double newtonsqrt(double x0,double c,double e,int max=100)
{
	double x1,temp;
	int n=0;
	do{
		temp=x0;
		x1=g(x0,c);
		x0=x1;
		cout<<"第"<<n+1<<"次迭代为"<<setprecision(16)<<x1<<endl;
	}while(fabs(temp-x0)>=e&&++n<max);
	if(n==max) 
		cout<<"迭代函数发散或迭代次数超过最大次数限制！\n";
	else 
		cout<<c<<"开方为"<<x0<<endl;
	return x1;
}
void main()
{
	newtonsqrt(10,115,0.0001);	
}