#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
double f(double x) //原函数
{
	//return exp(-x)-x+1;
	return x*x*x-x-1;
}
double g(double x)  //迭代函数
{
	//return exp(-x)+1;
	return pow(x+1,1.0/3);
}
double ga(double x) //迭代函数导数
{
	//return -exp(-x);
	return 1.0/3*pow(x+1,-2.0/3);
}
bool findroot(double& a,double x0,double len,int n)
{
	double y0=f(x0),y1,y2;
	bool finished=true;
	if(y0==0)
		a=x0;
	else
	{
		for(int i=1;i<=n;i++)
		{
			finished=true;
			y1=f(x0+i*len);
			if(y1==0)
			{
				a=x0+i*len;
				break;
			}
			if(y1*y0<0)
			{
				a=x0+(i-1)*len;
				break;
			}
			y2=f(x0-i*len);
			if(y2==0)
			{
				a=x0-i*len;
				break;
			}
			if(y2*y0<0)
			{
				a=x0-i*len;
				break;
			}
			finished=false;
		}
	}	
	if(!finished)
	{
		cout<<"根搜索失败!\n";
		return finished;
	}
	cout<<"在"<<a<<"附近加速迭代\n";
	return finished;
}
double exdd(double x0,double e,int max=100)
{
	double q=ga(x0);
	double x=g(x0);
	double x1=x+q*(x-x0)/(1-q);
	int n=0;	
	do{
		x1=x0;
		q=ga(x1);
		x=g(x1);
		x0=x+q*(x-x1)/(1-q);
		cout<<"第"<<++n<<"次加速迭代得："<<setprecision(16)<<x0<<endl;
	}while(fabs(x1-x0)>=e&&n<max);
	if(n==max) 
		cout<<"迭代函数发散或迭代次数超过最大次数限制！\n";
	else 
		cout<<"根为"<<x0<<endl;
	return x0;
}
void main()
{
	double x;
	if(findroot(x,0,0.1,1000))
		exdd(x,0.0001);

}