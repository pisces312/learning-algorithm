#include<iostream>
#include<stack>
#include<string>
#include<cmath>
#include<iomanip>
#include<algorithm>
using namespace std;
template<class T>
void outputStack(const stack<T> &s)
{
	stack<T> s1(s),s2;
	while(!s1.empty())
	{
		s2.push(s1.top());
		s1.pop();
	}
	while(!s2.empty())
	{
		cout<<s2.top()<<" ";
		s2.pop();
	}
	cout<<endl;
}
string toPostfix(string &expstr)
{
	cout<<"  后缀表达式构造过程:\n";
	stack<char> s1;
	string poststr;
	string operatorstr;
	char out=expstr[0];
	unsigned i=0,j=0;
	if(out=='-') expstr.insert(0,"0");
	while(i<expstr.length())
	{
		switch(expstr[i])
		{
		case '+':
		case '-':
			while(!s1.empty()&&s1.top()!='(')
			{
				cout<<setw(4)<<"\""<<s1.top()<<"\"出栈";
				poststr+=s1.top();
				s1.pop();
				cout<<setw(15)<<"运算符栈中为:";
				outputStack<char>(s1);
				cout<<setw(25)<<"后缀表达式为:"<<poststr<<endl;
			}
			cout<<setw(4)<<"\""<<expstr[i]<<"\"入栈";
			s1.push(expstr[i++]);
			cout<<setw(15)<<"运算符栈中为:";
			outputStack<char>(s1);
			break;
		case '*':
		case '/':
			while(!s1.empty()&&(s1.top()=='*'||s1.top()=='/'))
			{
				cout<<setw(4)<<"\""<<s1.top()<<"\"出栈";
				poststr+=s1.top();
				s1.pop();
				cout<<setw(15)<<"运算符栈中为:";
				outputStack<char>(s1);
				cout<<setw(25)<<"后缀表达式为:"<<poststr<<endl;
			}
			cout<<setw(4)<<"\""<<expstr[i]<<"\"入栈";
			s1.push(expstr[i++]);
			cout<<setw(15)<<"运算符栈中为:";
			outputStack<char>(s1);
			break;
		case '(':
			cout<<setw(4)<<"\""<<expstr[i]<<"\"入栈";
			s1.push(expstr[i++]);
			cout<<setw(15)<<"运算符栈中为:";
			outputStack<char>(s1);
			break;

		case ')':
			out=s1.top();
			cout<<setw(4)<<"\""<<out<<"\"出栈";
			s1.pop();
			cout<<setw(15)<<"运算符栈中为:";
			outputStack<char>(s1);
			while(!s1.empty()&&out!='(')
			{
				poststr+=out;
				out=s1.top();
				cout<<setw(4)<<"\""<<out<<"\"出栈";
				s1.pop();
				cout<<setw(15)<<"运算符栈中为:";
				outputStack<char>(s1);
				cout<<setw(25)<<"后缀表达式为:"<<poststr<<endl;
			}
			i++;
			break;
		default:
			while((expstr[i]>='0'&&expstr[i]<='9'&&i<expstr.length())||(expstr[i]=='.'))
			{
				poststr+=expstr[i++];
				cout<<setw(25)<<"后缀表达式为:"<<poststr<<endl;
			}
			poststr+=' ';
			break;		
		}		
	}	
	while(!s1.empty())
	{
		cout<<setw(4)<<"\""<<s1.top()<<"\"出栈";
		poststr+=s1.top();
		s1.pop();
		cout<<setw(15)<<"运算符栈中为:";
		outputStack<char>(s1);
		cout<<setw(25)<<"后缀表达式为:"<<poststr<<endl;
	}		
	return poststr;
}
void calc(string &p)
{
	cout<<"\n\n  求值过程:\n";
	stack<double> s2;
	double x=0,y=0,z=0,s=0;
	unsigned i=0;
	while(i<p.length())
	{
		s=0;
		if(p[i]>='0'&&p[i]<='9')
		{	
			z=0;
			int l=1;
			while(p[i]!=' '&&p[i]!='.')
			{
				z=z*10+p[i]-'0';
				i++;
			}
			if(p[i]=='.')
			{
				i++;
				while(p[i]!=' ')
				{
					double t=p[i++]-'0';
					for(int k=0;k<l;k++)
						t/=10;
					s+=t;
					l++;
				}
			}
			s2.push(z+s);
			cout<<setw(10)<<z+s<<"入栈";
			cout<<setw(15)<<"操作数栈中为:";
			outputStack<double>(s2);
		}
		else 
		{
			if(p[i]!=' ')
			{
				y=s2.top();
				s2.pop();
				cout<<setw(10)<<y<<"出栈";
				cout<<setw(15)<<"操作数栈中为:";
				outputStack<double>(s2);
				x=s2.top();
				s2.pop();
				cout<<setw(10)<<x<<"出栈";
				cout<<setw(15)<<"操作数栈中为:";
				outputStack<double>(s2);
				switch(p[i])
				{
					case '+':z=x+y;
							 cout<<x<<"+"<<y<<"="<<z<<endl;
						break;
					case '-':z=x-y;
						 cout<<x<<"-"<<y<<"="<<z<<endl;
						break;
					case '*':z=x*y;
						 cout<<x<<"×"<<y<<"="<<z<<endl;
						break;
					case '/':
						z=(double)x/(double)y;
						cout<<x<<"÷"<<y<<"="<<z<<endl;
						break;
				}
				s2.push(z);
				cout<<setw(10)<<z<<"入栈";
				cout<<setw(15)<<"操作数栈中为:";
				outputStack<double>(s2);
			}
		}
		i++;
	}	
	cout<<"表达式值为:"<<s2.top()<<endl;
}
string chartonum(string &exp)
{
	int i=0;
	string temp;	
	while(i<exp.size())
		if(isalpha(exp[i]))
		{
			string asc;
			int t=int(exp[i]);
			while(t!=0)
			{	
				asc+=(t%10+'0');
				t/=10;				
			}
			reverse(asc.begin(),asc.end());
			temp+=asc;
			i++;
		}
		else
			temp+=exp[i++];
	return temp;
}