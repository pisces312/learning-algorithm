#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
/*double f(double x)
{
//	return x*x*x-x-1;
	return x*x*x-3*x*x+2;
}*/
double f(double x)
{
	return exp(-x)-x+1;
}
bool findroot(double& a,double& b,double x0,double len,int n)
{
	double y0=f(x0),y1,y2;
	bool finished=true;
	if(y0==0)
		a=b=x0;
	else
	{
		for(int i=1;i<=n;i++)
		{
			finished=true;
			y1=f(x0+i*len);
			if(y1==0)
			{
				a=b=x0+i*len;
				break;
			}
			if(y1*y0<0)
			{
				a=x0+(i-1)*len;
				b=a+len;
				break;
			}
			y2=f(x0-i*len);
			if(y2==0)
			{
				a=b=x0-i*len;
				break;
			}
			if(y2*y0<0)
			{
				a=x0-i*len;
				b=a+len;
				break;
			}
			finished=false;
		}
	}
	if(!finished)
	{
		cout<<"根搜索失败!\n";
		return finished;
	}
	if(a==b)
		cout<<"根为"<<setprecision(16)<<a<<endl;
	else
		cout<<"根在"<<setprecision(16)<<a<<"～"<<b<<"范围"<<endl;
	return finished;
}
double erfenfa(double a,double b,double e)
{
	double x=(a+b)/2,x0=0;
	while(fabs(x-x0)>=e)
	{
		
		double t=f(x);
		if(t==0)
			return x;
		else 
		{
			if(f(a)*t<0)
				b=x;
			else a=x;
		}
		x0=x;
		x=(a+b)/2;
		cout<<x0<<" "<<x<<" "<<fabs(x-x0)<<endl;
	}
	return x;

}
void main()
{
	double a,b;
	if(findroot(a,b,-123,1,1000))
	//cout<<setprecision(16)<<erfenfa(1,1.5,0.005)<<endl;
	cout<<setprecision(16)<<erfenfa(a,b,0.0001)<<endl;

}