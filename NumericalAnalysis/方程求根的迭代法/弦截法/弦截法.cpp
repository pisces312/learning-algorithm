#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
double f(double x)
{
	//return exp(-x)-x+1;
	return x-cos(x);
}
bool findroot(double& a,double& b,double len,int n,double x0=0)
{
	double y0=f(x0),y1,y2;
	bool finished=true;
	if(y0==0)
		a=b=x0;
	else
	{
		for(int i=1;i<=n;i++)
		{
			finished=true;
			y1=f(x0+i*len);
			if(y1==0)
			{
				a=b=x0+i*len;
				break;
			}
			if(y1*y0<0)
			{
				a=x0+(i-1)*len;
				b=a+len;
				break;
			}
			y2=f(x0-i*len);
			if(y2==0)
			{
				a=b=x0-i*len;
				break;
			}
			if(y2*y0<0)
			{
				a=x0-i*len;
				b=a+len;
				break;
			}
			finished=false;
		}
	}
	if(!finished)
	{
		cout<<"根搜索失败!\n";
		return finished;
	}
	if(a==b)
		cout<<"根为"<<setprecision(16)<<a<<endl;
	else
		cout<<"根在"<<setprecision(16)<<a<<"～"<<b<<"范围"<<endl;
	return finished;
}
double xj(double x1,double x0,double e,int max=100)
{
	double x=x1-f(x1)*(x1-x0)/(f(x1)-f(x0));
	int n=0;
	do
	{
		x0=x1;
		x1=x;
		x=x-f(x)*(x-x0)/(f(x)-f(x0));
		cout<<setprecision(16)<<x<<endl;
	}while(fabs(x1-x0)>=e&&++n<max);
	if(n==max) 
		cout<<"迭代函数发散或迭代次数超过最大次数限制！\n";
	else 
		cout<<"根为"<<x0<<endl;
	return x;
}
void main()
{
	double a,b;
	if(findroot(a,b,0.01,10000))
		xj(a,b,0.0001);	
}