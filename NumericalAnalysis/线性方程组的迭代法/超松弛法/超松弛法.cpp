#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
bool test(double* matrix,int& xnum)
{
	for(int i=0;i<xnum;i++)
	{
		int t=(xnum+1)*i;
		double sum=0;
		for(int j=0;j<xnum;j++)
		{
			if(j!=i)
			{
				sum+=fabs(matrix[t+j]);
			}
		}
		if(fabs(matrix[t+i])<sum)
			return false;
	}
	return true;
}
void SOR(double* matrix,int &xnum,double *x,double w,double e)
{
	if(!test(matrix,xnum))
	{
		cout<<"此方程组发散！\n";
		return;
	}
	int i,j,count=0;
	double error=0;
	cout<<"方程组为：\n";
	for(i=0;i<xnum;i++)
	{
		for(j=0;j<xnum;j++)
			cout<<setiosflags(ios::showpos)<<matrix[(xnum+1)*i+j]<<resetiosflags(ios::showpos)<<"x"<<j+1<<" ";
		cout<<"="<<setiosflags(ios::showpos)<<matrix[(xnum+1)*i+j]<<endl;
	}
	cout<<resetiosflags(ios::showpos);
	do{
		for(i=0;i<xnum;i++)
		{	
			double sum=0,temp,xk=x[i];
			error=0;
			int t=(xnum+1)*i;
			for(int j=0;j<xnum;j++)
				if(j!=i)
					sum+=(matrix[t+j]*x[j]);
			temp=(matrix[t+xnum]-sum)/matrix[t+i];	
			x[i]=w*temp+(1-w)*xk;
			if(error<fabs(xk-x[i]))
			{
				error=fabs(xk-x[i]);
			}
		}
		cout<<"第"<<++count<<"次超松弛法迭代结果为：\n";
		for(i=0;i<xnum;i++)
		{
			cout<<"X"<<i+1<<"="<<setprecision(16)<<x[i]<<endl;
		}
		cout<<endl;
	}while(error>=e);
	cout<<"方程组的根为：\n";
	for(i=0;i<xnum;i++)
	{
		cout<<"X"<<i+1<<"="<<setprecision(16)<<x[i]<<endl;
	}
	
}

void main()
{
	int i;
	int xnum=0,totalnum=0;

	//可变参数
	//double temp[]={10,-1,-2,7.2,-1,10,-2,8.3,-1,-1,5,4.2};	
	//xnum=3;
	//double temp[]={3,1,2,1,2,-1};  /p171-5-1
	//xnum=2;

	double temp[]={5,-2,1,4,1,5,-3,2,2,1,-5,-11}; //p171-5-2
	xnum=3;

	double e=0.01,w=0.3;
	double* matrix,*x;
	
	totalnum=xnum*(xnum+1);	//留一个放系数
	matrix=new double[totalnum]; 
	x=new double[xnum];
	for(i=0;i<totalnum;i++)		//方程组矩阵
		matrix[i]=temp[i];
	for(i=0;i<xnum;i++)		//解初始化为0
		x[i]=0;
	SOR(matrix,xnum,x,w,e);
}
