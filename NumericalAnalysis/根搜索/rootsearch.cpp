#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
double f(double x)
{
	return x*x*x-3*x*x+2;
}


bool findroot(double& a,double& b,double x0,double len,int n)
{
	double y0=f(x0),y1,y2;
	bool finished=true;
	if(y0==0)
		a=b=x0;
	else
	{
		for(int i=1;i<=n;i++)
		{
			finished=true;
			y1=f(x0+i*len);
			if(y1==0)
			{
				a=b=x0+i*len;
				break;
			}
			if(y1*y0<0)
			{
				a=x0+(i-1)*len;
				b=a+len;
				break;
			}
			y2=f(x0-i*len);
			if(y2==0)
			{
				a=b=x0-i*len;
				break;
			}
			if(y2*y0<0)
			{
				a=x0-i*len;
				b=a+len;
				break;
			}
			finished=false;
		}
	}
	if(!finished)
	{
		cout<<"根搜索失败!\n";
		return finished;
	}
	if(a==b)
		cout<<"根为"<<setprecision(16)<<a<<endl;
	else
		cout<<"根在"<<setprecision(16)<<a<<"～"<<b<<"范围"<<endl;
	return finished;
}
/*void findroot(double x0,double len,int n)
{
	double y0=f(x0),y1,y2;
	if(y0==0)
	{
		cout<<"根为"<<setprecision(16)<<x0<<endl;
		return;
	}
	else
	{
		for(int i=1;i<=n;i++)
		{
			y1=f(x0+i*len);
			if(y1==0)
			{
				cout<<"根为"<<setprecision(16)<<x0+i*len<<endl;
				return;
			}
			if(y1*y0<0)
			{
				cout<<"根在"<<setprecision(16)<<x0+(i-1)*len<<"～"<<x0+i*len<<"范围"<<endl;
				return;
			}
			y2=f(x0-i*len);
			if(y2==0)
			{
				cout<<"根为"<<setprecision(16)<<x0-i*len<<endl;
				return;
			}
			if(y2*y0<0)
			{
				cout<<"根在"<<setprecision(16)<<x0-i*len<<"～"<<x0-(i-1)*len<<"范围"<<endl;
				return;
			}
		}
	}
}*/

/*double findroot(double x0,double len,int n)
{
	double y0=f(x0),y1,y2;
	if(y0==0)
		return x0;
	else
	{
		for(int i=1;i<=n;i++)
		{
			y1=f(x0+i*len);
			if(y1==0)
				return x0+i*len;
			if(y1*y0<0)
				return x0+(i-1)*len;
			y2=f(x0-i*len);
			if(y2==0)
				return x0-i*len;
			if(y2*y0<0)
				return x0-i*len;
		}
	}
}*/

				


void main()
{
	double a,b;
	//findroot(a,b,0,1,100);
	findroot(a,b,-123,1,1000);
//	findroot(a,b,2,0.1,100);
	//findroot(a,b,2,0.1,100);
//	findroot(2,0.1,100);
	

}