#include <iostream>
#include "ArriveProcess.h"
#include<ctime>
#include "QSystem.h"
#include<list>
#include<string>
#include "Client.h"
#include "MMNSystem.h"
#include "MM1System.h"
#include "NMM1System.h"
using namespace std;
void test2() {
    double lamda = 0.008;//0.005;//0.9;
//        double miu = 0.003;//0.6;
//        int n = 3;


    double miu = 0.001;
    int n = 10;
    long endTime = 1000;
    //m|m|1
    ArriveProcess* arrive=new ArriveProcess(lamda,endTime);
    ServeProcess* serveProcess=new ServeProcess(1);

    Server* server=new Server("1",miu);
    serveProcess->addServer(server);
    //ArriveProcess arrive(lamda,endTime);
    QSystem* system=new QSystem();
    serveProcess->setSystem(system);
    system->setArriveProcess(arrive);
    system->setServeProcess(serveProcess);
    arrive->addSystem(system);
    arrive->startStimulate();
    //arrive.startStimulate();

}
void test3() {
    list<QSystem*> systems;
    QSystem* system=new QSystem();
    system->name="a";
    systems.push_back(system);
    int count=0;
    for (list<QSystem*>::iterator itr = systems.begin(); itr != systems.end(); itr++) {
        //为每个系统创建一个新客户
        QSystem* system=*itr;
        count++;

        cout<<count<<" "<<system->name<<endl;
    }
}
void test() {
    srand( (unsigned)time( NULL ) );
    long max=0x7fff;
    for (int i=0;i<10;i++) {
        //cout<<  (double)   rand()   /   INT_MAX<<endl;
        //cout<<drand48()<<endl;
        //可行
        cout<<(rand()%max)/(double)max<<endl;
        //long a=-(long)(log(rand())/0.008);
        //cout<<a<<endl;
    }
}
void testMMNSystem() {
    double lamda = 0.008;//0.005;//0.9;
//        double miu = 0.003;//0.6;
//        int n = 3;


    double miu = 0.001;
    int n = 10;
    long endTime = 5000;//10000;
    //m|m|1
    ArriveProcess* arrive=new ArriveProcess(lamda,endTime);
    QSystem* system=new MMNSystem("M|M|n",arrive,miu,n);
    arrive->startStimulate();

}
void testMM1System() {
    double lamda = 0.008;//0.005;//0.9;
//        double miu = 0.003;//0.6;
//        int n = 3;


    double miu = 0.001;
    int n = 10;
    long endTime = 10000;
    //m|m|1
    ArriveProcess* arrive=new ArriveProcess(lamda,endTime);
    QSystem* system=new MM1System("M|M|1",arrive,miu);
    arrive->startStimulate();

}
void testNMM1System() {
    double lamda = 0.008;//0.005;//0.9;
//        double miu = 0.003;//0.6;
//        int n = 3;


    double miu = 0.001;
    int n = 10;
    long endTime = 10000;
    //m|m|1
    ArriveProcess* arrive=new ArriveProcess(lamda,endTime);
    QSystem* system=new NMM1System("N|M|M|1",arrive,miu,n);
    arrive->startStimulate();
}
void testCompare() {
    double lamda = 0.008;//0.005;//0.9;
//        double miu = 0.003;//0.6;
//        int n = 3;


    double miu = 0.001;
    int n = 10;
    long endTime = 10000;
    //m|m|1
    ArriveProcess* arrive=new ArriveProcess(lamda,endTime);
    QSystem* system=new NMM1System("N|M|M|1",arrive,miu,n);
    QSystem* system2=new MMNSystem("M|M|n",arrive,miu,n);

    arrive->startStimulate();
    cout<<system->ewq<<endl;
    cout<<system2->ewq<<endl;
    if (system2->ewq>0)
        cout<<(system->ewq/system2->ewq)<<endl;
}
int main() {
    testCompare();
    //testNMM1System();
    //testMMNSystem();
    //testMM1System();
    //test();
    //test2();
    //test3();
    return 0;
}
