#include "ArriveProcess.h"
#include "Client.h"
ArriveProcess::ArriveProcess(double lamda2,long endTime2) {
    lamda = lamda2;
    endTime = endTime2;
    curTime=0;
    srand( (unsigned)time( NULL ) );
}
void ArriveProcess::addSystem(QSystem* system) {
    cout<<"add a system\n";
    systems.push_back(system);
}


void ArriveProcess::startStimulate() {
    count=0;
//        long usedTime = 0;
    //cout<<curTime<<endl;
    while (true) {
        long clientDt = (long)(-log((rand()%0x7fff)/(double)0x7fff)/lamda);
        curTime += clientDt;
        if (curTime > endTime) {
            break;
        }
        count++;
//iterator<list<QSystem*>> itr
        //cout<<clientDt<<"  "<<curTime<<endl;

        for (list<QSystem*>::iterator itr = systems.begin(); itr != systems.end(); itr++) {
            //为每个系统创建一个新客户
            string id = "count";
            //顾客到达时间即为当前时间，同时携带到达间隔
            Client* client=new Client(id, curTime, clientDt);
            QSystem* system=*itr;
            //itr
            //cout<<"accept client\n";
            system->acceptClient(client);
        }
    }
    //对还在排队的顾客处理等待时间
    //到时
    //for (iterator<QSystem*> itr = systems.begin(); itr != systems.end(); itr++) {
    for (list<QSystem*>::iterator itr = systems.begin(); itr != systems.end(); itr++) {
        QSystem* system=*itr;
        system->stop(endTime);
        system->showResult();

    }
    cout<<"模拟结束，持续时间"<<endTime<<"  顾客到达数目:"<<count<<endl;
}
