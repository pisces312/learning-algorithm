#include"ServeProcess.h"
ServeProcess::ServeProcess(int maxServerNum2) {
    maxServerNum = maxServerNum2;

}
void ServeProcess::setSystem(QSystem* listener2) {
    system = listener2;
}
void ServeProcess::addServer(Server* server) {
    if (freeServers.size() == maxServerNum) {
        cout<<"超过服务员最多数目，不能添加！\n";
        return;
    }
    freeServers.push_back(server);
    cout<<"add a server success!\n";
}
/**
     * 获得一个服务员后必须马上使用！！
     * 输入应该为当前时刻
     * 必须取最小者！！！
     * @param curTime
     * @return
     */
Server* ServeProcess:: getCurrentFreeServer(long curTime) {

    Server* server=NULL;
    //!!!!
    long minTime=curTime;

    for (list<Server*>::iterator itr=freeServers.begin();itr!=freeServers.end();itr++) {
        Server* s=*itr;
        if (s->serverEndTime<minTime) {
            minTime=s->serverEndTime;
            server=s;
        }
    }
    //!!!!
    if (minTime<=curTime&&server!=NULL) {
        cout<<"find a server\n";
        freeServers.pop_front();
    } else {
        cout<<"does not have a free server\n";
    }
    return server;
    /**Server* s = freeServers.front();
    if (s == NULL) {
        return NULL;
    }
    if (s->serverEndTime <= curTime) {
        freeServers.pop_front();
        return s;
    }*/
    //return NULL;

}

void ServeProcess::processWaitingClient(long curTime) {
    //cout<<"process waiting client\n";
//!!!有问题，应该先判断服务器是否有空闲，再取等待顾客，因为取的同时会删除顾客
    Client* waitingClient = NULL;
    //int c=0;
    //cout<<system->hasWaitingClient()<<endl;
    while (system->hasWaitingClient()) {
        //cout<<c++<<endl;
        waitingClient = system->getWaitingClient();
        //如果有空闲的服务员，则将一定被用到
        Server* s = getCurrentFreeServer(curTime);

        if (s != NULL) {//保证了不多删除排队的顾客
            cout<<"process a client in queue\n";
            system->calcWaitTimeWhenQueueChanged(curTime);
            system->removeWaitingClient();
            s->serveClient(waitingClient);
            //服务结束
            freeServers.push_back(s);
            system->calcServedClientWaitTime(waitingClient);
        } else {
            break;
        }
    }
}
void ServeProcess::processClient(Client* client) {
    //cout<<"serveprocess accept a client\n";
    //这个time代表了当前模拟器的时间，即等于当前到达顾客的时刻
    long curTime = client->arriveTime;
//        Server s = null;//getFreeServer(time);
//????
    processWaitingClient(curTime);
//cout<<"process current client\n";
    //最后再服务刚到的顾客，直接服务
    Server* s = getCurrentFreeServer(curTime);
    if (s != NULL) {
        // cout<<"serve a client\n";
//            system.calcStateTime(curTime);
        s->serveClient(client);
//            system.calcStateTime(curTime);
        //必须重新加入才能进行优先的调整！！！！
        freeServers.push_back(s);
        system->calcServedClientWaitTime(client);
    } else {
        if (system->isQueueFull()) {

        } else {
            cout<<"go to queue\n";
            system->calcWaitTimeWhenQueueChanged(curTime);
            system->addWaitingClient(client);
        }
    }


}

