#include "MM1System.h"
#include "ServeProcess.h"
//MM1System::MM1System() {}
MM1System::MM1System(string name, double miu, string serverName) {
    //不能在内部再调用构造函数
    init(name, NULL, miu, serverName);
}

//MM1System::MM1System(string name2, ArriveProcess* arriveProcess2, double miu, string serverName) {
void MM1System::init(string name2, ArriveProcess* arriveProcess2, double miu, string serverName) {
    //cout<<"init mm1system\n";
    totalClientNum=0;
    totalWaitTime=0;
    totalWaitTime2=0;
    lastStateTime=0;
    name = name2;
    //cout<<"a "<<name<<endl;
    if (arriveProcess2 != NULL) {
        arriveProcess = arriveProcess2;
        arriveProcess->addSystem(this);
    }
//        arriveProcess = new ArriveProcess(0.2, 10);
    serveProcess = new ServeProcess(1);
    //这里加入两个监视器，其中一个给外层

    serveProcess->setSystem(this);
    Server* s=new Server(serverName, miu);
    serveProcess->addServer(s);

    serverNum = 1;
//        servers.add(new Server());
    queueLengthLimited = -1;
    //cout<<"a2 "<<name<<endl;
}

MM1System::MM1System(string name, ArriveProcess* arriveProcess, double miu) {
    init(name, arriveProcess, miu, "0");

}
//void MM1System::acceptClient(Client* client) {
//
//    cout<<"mm1 accept a client\n";
//    allArrivedClients.push_back(client);
//    //每到达一个顾客就计数
//    totalClientNum++;
////        System.out.println("顾客" + client.name + "到达:" + client.arriveTime);
//    //交给服务过程处理
//    serveProcess->processClient(client);
//}
//void MM1System::showResult() {
//    printTitle();
//    for (list<Client*>::iterator itr=allArrivedClients.begin();itr!=allArrivedClients.end();itr++) {
//        Client* client=*itr;
//        client->print();
//    }
//    //在这里输出所有顾客的情况
//    cout<<name<<" EWq="<<getEWq()<<endl;
//    cout<<name<<" ELq="<<getELq()<<endl;
//}
//void MM1System::stop(long endTime2) {
//    endTime = endTime2;
//    //????
//    serveProcess->processWaitingClient(endTime);
////计算最终队列中剩余的顾客的等待时间
//    for (list<Client*>::iterator itr=clientsWaitingQueue.begin();itr!=clientsWaitingQueue.end();itr++) {
//        Client* client=*itr;
//        totalWaitTime += (endTime - client->arriveTime);
//        calcWaitTimeWhenQueueChanged(endTime);
//    }
//}
