#ifndef QSYSTEM_H_INCLUDED
#define QSYSTEM_H_INCLUDED
#include "Client.h"
#include "Server.h"
#include<iostream>
#include<string>
#include<list>
#include "ArriveProcess.h"
#include "ServeProcess.h"
using namespace std;
class ArriveProcess;
class ServeProcess;
//class Client;
//class Server;
class QSystem {
public:
    string name;
    ArriveProcess* arriveProcess;
    ServeProcess* serveProcess;
    int serverNum;
    //限制排队的长度
    int queueLengthLimited;
    //顾客等待队列
    //队列按到达时刻从小到大排列
    list<Client*> clientsWaitingQueue;
    long totalWaitTime;
    //顾客到达总数
    int totalClientNum ;
    double ewq ;
    double elq ;
    long totalWaitTime2 ;
    long endTime ;
    //上次系统的状态
    long lastStateTime ;
    //记录所有到来的顾客，为方便显示结果
    list<Client*> allArrivedClients;
    //QSystem();
    void calcWaitTimeWhenQueueChanged(long time);

    bool isQueueFull();

    bool hasWaitingClient();

    bool addWaitingClient(Client* e);


    Client* getWaitingClient();

    void removeWaitingClient();

    void setServeProcess(ServeProcess* serveProcess2);
    void setArriveProcess(ArriveProcess* arriveProcess2);
    /**
     * 这里计算接收服务的顾客
     * @param client
     */
    void calcServedClientWaitTime(Client* client) ;

    void printTitle() ;

    double getEWq();

    double getELq() ;
//下面是抽象函数实现多态
    /**
     * 接收顾客
     * @param client
     */
    virtual void acceptClient(Client* client);
    /**
     * 模拟结束后对仍在队列中等待的顾客计算其等待时间
     * @param time
     */
    virtual void stop(long endTime2);



    virtual void showResult();
    //virtual ~QSystem();

};
#endif // QSYSTEM_H_INCLUDED
