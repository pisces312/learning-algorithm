#ifndef ARRIVEPROCESS_H_INCLUDED
#define ARRIVEPROCESS_H_INCLUDED



#include "QSystem.h"
#include<iostream>
#include<list>
#include<ctime>
#include<cmath>

using namespace std;
//�໥����
class QSystem;
class ArriveProcess{
    public:
    ArriveProcess(double lamda2,long endTime2);
    int count;
    double lamda;
    long curTime;
    long endTime;
    list<QSystem*> systems;


    void addSystem(QSystem* system);
    void startStimulate();
};
#endif // ARRIVEPROCESS_H_INCLUDED
