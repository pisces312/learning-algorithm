#include "QSystem.h"
/*QSystem::QSystem() {
    queueLengthLimited = -1;
    totalClientNum=0;
    totalWaitTime=0;
    totalWaitTime2=0;
    //cout<<"这里是正确的\n";
    //cout<<clientsWaitingQueue.size()<<endl;
    //cout<<clientsWaitingQueue.empty()<<endl;
    //cout<<"b\n";


}*/


void QSystem::calcWaitTimeWhenQueueChanged(long time) {
    long dt = time - lastStateTime;
    totalWaitTime2 += (clientsWaitingQueue.size() * dt);
    cout<<"totalWaitTime2="<<totalWaitTime2<<endl;
    lastStateTime = time;

}

bool QSystem::isQueueFull() {
    return !(queueLengthLimited == -1 || (signed int)clientsWaitingQueue.size() < queueLengthLimited);
}
//?????
bool QSystem::hasWaitingClient() {
    //???????????
    //cout<<"has waiting client\n";
    //cout<<clientsWaitingQueue.size()<<endl;
    //cout<<clientsWaitingQueue.empty()<<endl;
    //cout<<"aaaa\n";
    //return clientsWaitingQueue.size() > 0;
    return !clientsWaitingQueue.empty();
}

bool QSystem::addWaitingClient(Client* e) {
    //cout<<"add waiting client\n";
    if (isQueueFull()) {
        return false;
    }
    clientsWaitingQueue.push_back(e);
    return true;
}


Client* QSystem::getWaitingClient() {
    //cout<<"get waiting client\n";
    return clientsWaitingQueue.front();
}

void QSystem::removeWaitingClient() {
    clientsWaitingQueue.pop_front();
}

void QSystem::setServeProcess(ServeProcess* serveProcess2) {
    serveProcess = serveProcess2;
}

void QSystem::setArriveProcess(ArriveProcess* arriveProcess2) {
    arriveProcess = arriveProcess2;
}


void QSystem::calcServedClientWaitTime(Client* client) {
    totalWaitTime += client->waitTime;
}



void QSystem::printTitle() {
    cout<<"顾客序号 服务员 到达间隔  到达时刻 等待时间 接受服务时刻 服务时间 预计离开时刻\n";
}



double QSystem::getEWq() {
    ewq = totalWaitTime / (double) totalClientNum;
    return ewq;
}

double QSystem::getELq() {
    elq = totalWaitTime2 / (double) endTime;
    return elq;
}
void QSystem::acceptClient(Client* client) {

    //cout<<"mm1 accept a client\n";
    allArrivedClients.push_back(client);
    //每到达一个顾客就计数
    totalClientNum++;
//        System.out.println("顾客" + client.name + "到达:" + client.arriveTime);
    //交给服务过程处理
    serveProcess->processClient(client);
}
void QSystem::showResult() {
    printTitle();
    for (list<Client*>::iterator itr=allArrivedClients.begin();itr!=allArrivedClients.end();itr++) {
        Client* client=*itr;
        client->print();
    }
    //在这里输出所有顾客的情况
    cout<<name<<" EWq="<<getEWq()<<endl;
    cout<<name<<" ELq="<<getELq()<<endl;
    cout<<totalWaitTime2<<endl;
    //正确
    cout<<endTime<<endl;
}
void QSystem::stop(long endTime2) {
    endTime = endTime2;
    //????
    serveProcess->processWaitingClient(endTime);
//计算最终队列中剩余的顾客的等待时间
    for (list<Client*>::iterator itr=clientsWaitingQueue.begin();itr!=clientsWaitingQueue.end();itr++) {
        Client* client=*itr;
        totalWaitTime += (endTime - client->arriveTime);
        calcWaitTimeWhenQueueChanged(endTime);
    }
}


