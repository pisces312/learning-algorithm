#ifndef SERVER_H_INCLUDED
#define SERVER_H_INCLUDED
#include<string>
#include<iostream>
#include<ctime>
#include<cmath>
#include "Client.h"
using namespace std;
class Server {
public:
    long serverEndTime ;
//服务能力
    double miu;
    string name;
    /**
     *对顾客的服务过程
     * @param client
     */
    void serveClient(Client* client);

    Server(string name, double miu);
};


#endif // SERVER_H_INCLUDED
