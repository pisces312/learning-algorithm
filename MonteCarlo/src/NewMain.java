
import java.text.DecimalFormat;
import queue.QSystem;
//import queue.MM1System;
import queue.MMNSystem;
import queue.ArriveProcess;
//import queue.NMM1System;
import queue.MM1System;
import queue.NMM1System;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *服务员能力也要相同？
 * 这里让两个系统对每个顾客的服务时间都相同
 * @author Administrator
 */
public class NewMain {

    public static void printLine(int len) {
        for (int i = 0; i < len; i++) {
            System.out.print("=");
        }
        System.out.println(" " + len);
    }

//    public static void testExpRandom() {
//        int num = 50;
//        double lamda = 0.2;
//        double[] array = new double[num];
////        PriorityQueue<Double> queue=new PriorityQueue<Double>();
//        ArriveProcess arriveProcess = new ArriveProcess(lamda, num);
//        QSystem system = new MM1System("M|M|1", arriveProcess, 0.2);
//
//        arriveProcess.addSystem(system);
//        for (int i = 0; i < num; i++) {
//            double d = RandomUtils.randomE(lamda);
////            queue.add(d);
////            System.out.println(d);
//            array[i] = d;
////            System.out.println(arriveProcess.randomE(0.1));
//        }
//        Arrays.sort(array);
//        int[] count = new int[(int) (array[num - 1] + 1)];
//        Arrays.fill(count, 0);
//        for (int i = 0; i < num; i++) {
//            System.out.println(array[i]);
//            count[(int) array[i]]++;
//        }
//        System.out.println();
//        for (int i = 0; i < count.length; i++) {
//            printLine(count[i]);
////            System.out.println(count[i]);
//        }
////        System.out.println(Arrays.toString(array));
////        double r = 0.1;
//////        double r = 0.0001;
////        long t = System.currentTimeMillis();
////        System.out.println(r * t);
////        System.out.println(t);
////        System.out.println(exponentialDistribution(r, 10));
//
//    }
//    public static void testMethod(double lamda, double miu, int n, long endTime) {
//
//        ArriveProcess arriveProcess1 = new ArriveProcess(lamda, endTime);
////        ArriveProcess arriveProcess2 = new ArriveProcess(lamda, endTime);
//        QSystem system1 = new MM1System("M|M|1", arriveProcess1, n * miu);
//        QSystem system2 = new MMNSystem("M|M|n", arriveProcess1, miu, n);
//        arriveProcess1.startStimulate();
////        arriveProcess2.startStimulate();
//        double ews1 = system1.getEWq();
//        double ews2 = system2.getEWq();
//        System.out.println(ews1);
//        System.out.println(ews2);
//        System.out.println("n=" + n);
//        if (ews2 != 0) {
//            System.out.println(ews1 / ews2);
//        }
//    }
    public static void testMethod1(double lamda, double miu, int n, long endTime) {

        ArriveProcess arriveProcess1 = new ArriveProcess(lamda / n, miu, endTime);
        ArriveProcess arriveProcess2 = new ArriveProcess(lamda, miu, endTime);
        QSystem system1 = new MM1System("M|M|1", arriveProcess1, miu);
        QSystem system2 = new MMNSystem("M|M|n", arriveProcess2, miu, n);
        arriveProcess1.startStimulate();
        arriveProcess2.startStimulate();
        double ews1 = system1.getEWq();
        double ews2 = system2.getEWq();
        System.out.println(ews1);
        System.out.println(ews2);
        System.out.println("n=" + n);
        if (ews2 != 0) {
            System.out.println(ews1 / ews2);
        }
    }

    public static void testMethod2(double lamda, double miu, int n, long endTime) {
        System.out.println("λ=" + lamda + "，μ=" + miu + "，n=" + n + "，结束时间=" + endTime + "，比值为" + (lamda / (n * miu)) + "时的结果。");
//        System.out.println("");
        //使用一个流
        ArriveProcess arriveProcess = new ArriveProcess(lamda, miu, endTime);
        QSystem mmnSystem = new MMNSystem("M|M|n", arriveProcess, miu, n);
        QSystem mm1System = new NMM1System("n个M|M|1", arriveProcess, miu, n);
        arriveProcess.startStimulate();
        double ewq1 = mmnSystem.getEWq();
        double ewq2 = mm1System.getEWq();
        double elq1 = mmnSystem.getELq();
        double elq2 = mm1System.getELq();
//        System.out.println(ewq1);
//        System.out.println(ewq2);
//        System.out.println(elq1);
//        System.out.println(elq2);

        if (ewq1 != 0) {
            System.out.println("EWq2/EWq1 = " + ewq2 / ewq1);
        }
        System.out.println("ELq2/ELq1 = " + elq2 / elq1);
        System.out.println("理论n = " + n + " + C");

    }

//    public static void testNMMN() {
//        double lamda = 1;//0.1;//0.2;//0.3;//0.5;
//        double miu = 0.9;//0.667;
//        int clientNum = 10;
//        int n = 2;
//        //使用一个流
//        ArriveProcess arriveProcess = new ArriveProcess(lamda, clientNum);
////        QSystem mm1System = new MM1System("M|M|1", arriveProcess);
//        QSystem nmm1System = new NMM1System("n-M|M|1", arriveProcess, miu, n);
//        arriveProcess.start();
//    }

//    public static void testMM1() {
//        double lamda = 1;
//        double miu = 1;
//        int clientNum = 10;//-1;//10;
//        long time =-1;// 10000;
//        ArriveProcess arriveProcess1 = new ArriveProcess(lamda, time);
//        QSystem system1 = new MM1System("M|M|1", arriveProcess1, miu);
//        arriveProcess1.start();
//    }
    public static void testMMN() {
        double lamda = 1;//0.1;//0.2;//0.3;//0.5;
        double miu = 0.667;
//        int clientNum = 10;
        int n = 2;
        //使用一个流
        ArriveProcess arriveProcess = new ArriveProcess(lamda, miu, 10000);
        new MMNSystem("M|M|n", arriveProcess, miu, n);
//        QSystem mmnSystem = new MMNSystem("M|M|n", arriveProcess, miu, n);
//        arriveProcess.start();
    }

    public void testTest() {
        double lamda = 0.9;
        double miu = 0.2;
        int n = 3;
        long endTime = 100000;
//        while(lamda<0.9){
        testMethod2(lamda, miu, n, endTime);


    }

    public static void test3() {
        System.out.printf("%+8.3f", 3.14);
        System.out.println();
        System.out.printf("%+-8.3f\n", 3.14);
        System.out.printf("%08.3f\n", 3.14);
        System.out.printf("%(8.3f\n", -3.14);
        System.out.printf("%,f\n", 2356.34);
        System.out.printf("%x\n", 0x4a3b);
        System.out.printf("%#x\n", 0x4a3b);
        System.out.println("------------");
        System.out.printf("你好:%s,%3d岁,工资%-7.2f\n", "张三", 38, 15000.00);
//        System.out.printf("你好:%1$s,%2$3d岁,%2$#x岁\n", "张三", 38);
//        System.out.printf("%3d,%#<x", 38);
        System.out.println();
        DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
        df.setMaximumFractionDigits(2);
        System.out.println(df.format(12.3456789));
        df.setMaximumFractionDigits(2);
//        df.applyPattern("#####.##");
//        df.setMinimumIntegerDigits(4);
//        df.
        df.applyPattern("00000");
        System.out.println(df.format(12.3456789));
//        double a=12.323;
        int a = 234;
        System.out.printf("%10d", a);
        System.out.printf("%8c", "abc");


    }

    /**
     * 由于到达间隔已经确定，只需要服务员的能力和结束时间
     * @param miu
     * @param n
     * @param endTime
     */
//public static void testFromFile(double miu, int n, long endTime,String path) {
//        System.out.println("  μ=" + miu + "  n=" + n + "  结束时间=" + endTime);
//        //使用一个流
//        ArriveProcess arriveProcess = new ArriveProcess(path, endTime);
//        QSystem mmnSystem = new MMNSystem("M|M|n", arriveProcess, miu, n);
//        QSystem mm1System = new NMM1System("n-M|M|1", arriveProcess, miu, n);
//        arriveProcess.startStimulate();
//        double ewq1 = mmnSystem.getEWq();
//        double ewq2 = mm1System.getEWq();
//        double elq1 = mmnSystem.getELq();
//        double elq2 = mm1System.getELq();
//        System.out.println(ewq1);
//        System.out.println(ewq2);
//        System.out.println(elq1);
//        System.out.println(elq2);
//        System.out.println("理论n = " + n + " + C");
//        if (ewq1 != 0) {
//            System.out.println("EWq2/EWq1 = " + ewq2 / ewq1);
//        }
//        System.out.println("ELq2/ELq1 = " + elq2 / elq1);
//
//    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("请输入λ、μ、n、结束时间");
        //当lamda==n*miu时，n个mm1系统强，当lamda/n*miu远小于1时mmn强
        /**
         * 1 0.98 2
         *0.9 0.5 2
         * 0.8 0.5 2
         */
        double lambda = 0.003;//0.005;//0.9;
//        double miu = 0.003;//0.6;
//        int n = 3;

//和lambda与miu之间的比值也有关
        double miu = 0.002;
        int n = 5;
        long endTime = 30000;
        System.out.println("(默认为λ=" + lambda + "、μ=" + miu + "、n=" + n + "、结束时间=" + endTime);
        if (args != null) {
            switch (args.length) {
                case 4:
                    endTime = Integer.valueOf(args[3]);
                case 3:
                    n = Integer.valueOf(args[2]);
                case 2:
                    miu = Double.valueOf(args[1]);
                case 1:
                    lambda = Double.valueOf(args[0]);
                    break;

            }
        }
//        testMethod(lamda, miu, n, endTime);
//        testMethod1(lamda, miu, n, endTime);
        testMethod2(lambda, miu, n, endTime);
//        testFromFile(miu, n, endTime, "f:\\src.txt");
//        test3();
//        DataInputStream dis=new DataInputStream(System.in);
//        dis.readDouble();
//        dis.readDouble();
//        dis.readInt();
//        dis.readLong();
//
//        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
//        do{
//            String content=null;
//            try {
//                content = br.readLine();
//            } catch (IOException ex) {
//                Logger.getLogger(NewMain.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            if(content==null){
//                break;
//            }
//        }while(true);

//        System.in.
//        testMM1();
//            testMMN();
//        testNMMN();
//        test1();

//        }
    }
}
