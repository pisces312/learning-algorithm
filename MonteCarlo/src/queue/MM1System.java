package queue;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class MM1System extends QSystem {

    /**
     * 对n个mm1系统的支持
     * 并且给每个服务员编号
     * @param name
     * @param miu
     */
    public MM1System(String name, double miu, String serverName) {
        this(name, null, miu, serverName);
    }

    private MM1System(String name, ArriveProcess arriveProcess, double miu, String serverName) {
        super();
        this.name = name;
        if (arriveProcess != null) {
            this.arriveProcess = arriveProcess;
            arriveProcess.addSystem(this);
        }
//        arriveProcess = new ArriveProcess(0.2, 10);
        serveProcess = new ServeProcess(1);
        //这里加入两个监视器，其中一个给外层

        serveProcess.setSystem(this);

        serveProcess.addServer(new Server(serverName, miu));

//        serverNum = 1;
//        servers.add(new Server());
        queueLengthLimited = -1;
    }

    public MM1System(String name, ArriveProcess arriveProcess, double miu) {
        this(name, arriveProcess, miu, "0");

    }
}
