package queue;

import java.util.LinkedList;
import java.util.Random;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *假设没有同时到来的情况
 * @author Administrator
 */
public class ArriveProcess {

    int count = 0;
    double lambda;
    //一个系数，控制速度
//    int s = 100;
    //同时作为多个系统的到达过程，提供相同的源
    LinkedList<QSystem> systems = new LinkedList<QSystem>();
//    Timer timer = new Timer();
//    long timeSlice;
    //当前绝对时间
    private long curTime = 0;
    private long endTime;
//    Random random=new Random();
    public final static int SRC_FILE = 0;
    public final static int SRC_GENERATED = 1;
    int type;
//    long[] dt;
//    DataInputStream dis;
//BufferedReader br;
    double miu;

    /**
     * 通过指定顾客的数目结束到达过程
     * @param lambda
     * @param max
     */
    public ArriveProcess(double lamda, double miu, long endTime) {
        this.lambda = lamda;
        this.miu = miu;
        this.endTime = endTime;
        //由自己产生
        type = SRC_GENERATED;
//        this.s=speed;
    }

    public static long randomExpLong(double r, double lamda) {
        return (long) (-Math.log(r) / lamda);
    }
//    public ArriveProcess(String path, long endTime) {
//        type = SRC_FILE;
//        this.endTime = endTime;
//        try {
////        DataInputStream dis=null;
////            dis = new DataInputStream(new FileInputStream(path));
//
//            br=new BufferedReader(new InputStreamReader(new FileInputStream(path)));
//
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(ArriveProcess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//    }

    /**
     * 由文件读取到达流的间隔,结束时间
     * @param dt
     */
//    public ArriveProcess(long[] dt, long endTime) {
////        this.dt = dt;
//        type = SRC_FILE;
//    }
//    public long getCurrentTime() {
//        return curTime;
//    }

//    public long getEndTime() {
//        return endTime;
//    }
    /**
     * 可以同时给多个系统提供相同的顾客源
     * @param system
     */
    public void addSystem(QSystem system) {
        systems.add(system);
    }

//    private void fromFile() {
////        for(long clientDt:dt){
//        try {
//            while (true) {
//                String str=br.readLine();
//                if(str==null){
//                    break;
//                }
//
//                long clientDt =Long.valueOf(str) ;//dis.readLong();
//                System.out.println("dt="+clientDt);
//                curTime += clientDt;
//                if (curTime > endTime) {
//                    break;
//                }
//                count++;
////            System.out.println("顾客到达时间间隔:" + time);
//                for (QSystem system : systems) {
//                    //为每个系统创建一个新客户
//                    String id = String.valueOf(count);
//                    system.acceptClient(new Client(id, curTime, clientDt));
//                }
//
//            }
//        } catch (IOException ex) {
//            Logger.getLogger(ArriveProcess.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (br!= null) {
//                try {
//                    br.close();
//                } catch (IOException ex) {
//                    Logger.getLogger(ArriveProcess.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
////            if (dis != null) {
////                try {
////                    dis.close();
////                } catch (IOException ex) {
////                    Logger.getLogger(ArriveProcess.class.getName()).log(Level.SEVERE, null, ex);
////                }
////            }
//        }
//        for (QSystem system : systems) {
//            //to do 将stop放入showResult中！！
//            system.stop(endTime);
//            system.showResult();
//
//        }
//        System.out.println("模拟结束，持续时间" + endTime + "  顾客到达数目:" + count);
//    }
    private void generatedByItSelf() {
//        Random r1=new Random();
//        Random r2=new Random();
        while (true) {

//            long clientDt = randomExpLong(r1.nextDouble()/*Math.random()*/, lambda);//getClientArriveTimeInterval();
//            long serverDt = randomExpLong(r2.nextDouble()/*Math.random()*/, miu);//getClientArriveTimeInterval();
            long clientDt = randomExpLong(Math.random(), lambda);//getClientArriveTimeInterval();
            long serverDt = randomExpLong(Math.random(), miu);//getClientArriveTimeInterval();
            curTime += clientDt;
            if (curTime > endTime) {
                break;
            }
            count++;
//            System.out.println("顾客到达时间间隔:" + time);
            for (QSystem system : systems) {
                //为每个系统创建一个新客户
                String id = String.valueOf(count);
                system.acceptClient(new Client(id, curTime, clientDt, serverDt));
            }
//            System.out.println("\n");
        }
        //对还在排队的顾客处理等待时间
        //到时
        for (QSystem system : systems) {
            //to do 将stop放入showResult中！！
            system.stop(endTime);
            system.showResult();

        }
        System.out.println("模拟结束，持续时间" + endTime + "  顾客到达数目:" + count);
    }

    public void startStimulate() {
//        long usedTime = 0;
        switch (type) {
            case SRC_FILE:
//                fromFile();
                break;
            case SRC_GENERATED:
                generatedByItSelf();
                break;
        }

    }
}
