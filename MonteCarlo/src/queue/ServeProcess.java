package queue;

import java.util.Comparator;
import java.util.PriorityQueue;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class ServeProcess {
    //一个服务过程只能对应一个系统

    QSystem system;
    //????用优先队列？？，找服务器endTime最小的？？
    PriorityQueue<Server> servers;

    //存储当前可用的服务，供给随机选择使用，对应于同时有多个服务员空闲的情况
//    LinkedList<Server> freeServers = new LinkedList<Server>();
    int maxServerNum = 1;
//    Random random = new Random();

    public ServeProcess(int maxServerNum) {
        this.maxServerNum = maxServerNum;
        servers = new PriorityQueue<Server>(maxServerNum, new Comparator() {

            public int compare(Object o1, Object o2) {
                Server s1 = (Server) o1;
                Server s2 = (Server) o2;
                if (s1.serverEndTime < s2.serverEndTime) {
                    return -1;
                }
                if (s1.serverEndTime == s2.serverEndTime) {
                    return 0;
                }
                return 1;
            }
        });
    }

    public void setSystem(QSystem listener) {
        this.system = listener;
    }

    public void addServer(Server server) {
        if (servers.size() == maxServerNum) {
            System.out.println("超过服务员最多数目，不能添加！");
            return;
        }
        servers.add(server);
    }

    /**
     * ???有问题
     *采用随机选择空闲服务员的方法
     * 获得相对空闲的服务员
     * 获得一个服务员后必须马上使用！！
     * 输入应该为当前时刻
     * 必须取最小者！！！xxxx
     * @param curTime
     * @return
     */
    public Server getRelativeFreeServer(long curTime) {
        //清空空闲服务缓存
//        freeServers.clear();
//
//        Server s =null;
//        while ((s = servers.peek()) != null) {
//            if (s.serverEndTime <= curTime) {
//                servers.poll();
//                freeServers.add(s);
//            } else {
//                break;
//            }
//        }
////        System.out.println("服务员缓冲区大小:"+freeServers.size());
//        //只有一个，别无选择
//        switch (freeServers.size()) {
//            case 0:
//                //!!!!
//                s=null;
//                break;
//            case 1://xxxx不能用pop，这里不能删除freeServers中的服务
//                //应该删除被选中的，因为服务玩，它会被加回
//                //del choosen server
//                s = freeServers.pop();
//                break;
//            default:
//                int index = random.nextInt(freeServers.size());
//                s = freeServers.get(index);
//                //返回的是s前一个位置
//                //del choosen server
//                freeServers.remove(index);
//                //!!!!加回优先队列，否则服务员边少！！！
//                //只加回没被选中的其他所有
////                for (Server s2 : freeServers) {
////                    servers.offer(s2);
////                }
//                servers.addAll(freeServers);
//                break;
//        }
//
//
//        return s;
        //
        //始终取最小空闲服务时间的服务员，因为服务员自己可以知道谁的空闲时间小
        Server s = servers.peek();
        if (s == null) {
            return null;
        }
        if (s.serverEndTime <= curTime) {
            servers.poll();
            return s;
        }
        return null;

    }

    public void processWaitingClient(long curTime) {
//        Server s = null;//getFreeServer(time);
//!!!有问题，应该先判断服务器是否有空闲，再取等待顾客，因为取的同时会删除顾客
//        Client waitingClient = null;
        while (system.hasWaitingClient()) {
            Client waitingClient= system.getWaitingClient();
            //如果有空闲的服务员，则将一定被用到
            Server s = getRelativeFreeServer(curTime);

            if (s != null) {//保证了不多删除排队的顾客
//                s.time=curTime;
                //在删除之前
                //出队接受服务
                //???放在哪个位置
                system.calcWaitTimeWhenQueueChanged(curTime);
                system.removeWaitingClient();
//                system.calcWaitTimeWhenQueueChanged(curTime);
                s.serveClient(waitingClient);
                //服务结束
//                system.calcStateTime(curTime);
                //必须重新加入才能进行优先的调整！！！！
                servers.add(s);
                system.calcServedClientWaitTime(waitingClient);
            } else {
                break;
            }
        }
    }

    public void processClient(Client client) {
        //这个time代表了当前模拟器的时间，即等于当前到达顾客的时刻
        long curTime = client.arriveTime;
//        Server s = null;//getFreeServer(time);
        processWaitingClient(curTime);

        //最后再服务刚到的顾客，直接服务
        Server s = getRelativeFreeServer(curTime);
        if (s != null) {
//            system.calcStateTime(curTime);
            s.serveClient(client);
//            system.calcStateTime(curTime);
            //必须重新加入才能进行优先的调整！！！！
            servers.add(s);
            system.calcServedClientWaitTime(client);
        } else {
            if (system.isQueueFull()) {
            } else {
                system.calcWaitTimeWhenQueueChanged(curTime);
                system.addWaitingClient(client);
//                system.calcWaitTimeWhenQueueChanged(curTime);
            }
//            if (system.addWaitingClient(client)) {
//                System.out.println("顾客" + client.name + "进入等待队列");
//            } else {
//                System.out.println("损失顾客" + client.name);
//            }
        }


    }
}
