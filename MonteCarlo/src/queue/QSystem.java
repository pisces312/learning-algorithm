package queue;

import java.util.LinkedList;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public abstract class QSystem {

    String name;
    ArriveProcess arriveProcess;
    ServeProcess serveProcess;
    int serverNum;
    //限制排队的长度
    int queueLengthLimited = -1;
    //顾客等待队列
    //队列按到达时刻从小到大排列
    LinkedList<Client> clientsWaitingQueue = new LinkedList<Client>();
    long totalWaitTime = 0;
    //顾客到达总数
    int totalClientNum = 0;
    double ewq = 0;
    double elq = 0;
//    ExecutorService executor;
    //当前到来的客户被服务完，不能用这种方式判断是否处理完所有客户，可能
    //下一个还没有到来

//    int finishedClient=0;
//    int totalFinishedClient = 0;
    //返回当前服务好的客户数
//    public int currentFinishedClient = 0;
    long totalWaitTime2 = 0;
    long endTime = 0;
    //上次系统的状态
    long lastStateTime = 0;
    //记录所有到来的顾客，为方便显示结果
    protected LinkedList<Client> allArrivedClients = new LinkedList<Client>();

    public QSystem() {
    }

    /**
     * 排队改变时计算
     * @param time
     */
    public void calcWaitTimeWhenQueueChanged(long time) {
        long dt = time - lastStateTime;
        totalWaitTime2 += (clientsWaitingQueue.size() * dt);
        lastStateTime = time;
    }

    public boolean isQueueFull() {
        return !(queueLengthLimited == -1 || clientsWaitingQueue.size() < queueLengthLimited);
    }

    public boolean hasWaitingClient() {
        return clientsWaitingQueue.size() > 0;
    }

    public boolean addWaitingClient(Client e) {
        if (isQueueFull()) {
            return false;
        }
        clientsWaitingQueue.addLast(e);
        return true;
    }

    /**
     * 决定排队顾客接收服务的优先权
     * @return
     */
//    public Client getAndDelWaitingClient() {
////        return clientsWaitingQueue.pop();//pollFirst();//xxx
//        return clientsWaitingQueue.pollFirst();
////        return clientsWaitingQueue.pollLast();
//    }
    public Client getWaitingClient() {
//        return clientsWaitingQueue.pop();//pollFirst();//xxx
        return clientsWaitingQueue.getFirst();
//        return clientsWaitingQueue.pollLast();
    }

    public void removeWaitingClient() {
        clientsWaitingQueue.removeFirst();
    }

    public void setServeProcess(ServeProcess serveProcess) {
        this.serveProcess = serveProcess;
    }

    public void setArriveProcess(ArriveProcess arriveProcess) {
        this.arriveProcess = arriveProcess;
    }

    /**
     * 这里计算接收服务的顾客
     * @param client
     */
    public void calcServedClientWaitTime(Client client) {
//        totalClientNum++;
        totalWaitTime += client.waitTime;
    }

    /**
     * 模拟结束后对仍在队列中等待的顾客计算其等待时间
     * @param time
     */
    public void stop(long endTime) {
        this.endTime = endTime;
        //????
        serveProcess.processWaitingClient(endTime);
//计算最终队列中剩余的顾客的等待时间
        for (Client client : clientsWaitingQueue) {

//            client.waitTime = endTime - client.arriveTime;
//            calcServedClientWaitTime(client);
            totalWaitTime += (endTime - client.arriveTime);
            calcWaitTimeWhenQueueChanged(endTime);
        }
//        printTitle();
//        showResult();
    }

    public void printTitle() {
        System.out.println(name + "系统详细实验数据如下：");
        System.out.println("顾客序号 服务员 到达间隔 到达时刻 等待时间 接受服务时刻 服务时间 离开时刻");
//        System.out.println("顾客序号 服务员 到达间隔 到达时刻 等待时间 接受服务时刻 服务时间 预计离开时刻");
    }

    public void showResult() {
        printTitle();
        for (Client e : allArrivedClients) {
            e.print();
//            System.out.println(e);
        }
        //在这里输出所有顾客的情况
        System.out.println(name + " EWq=" + getEWq());
        System.out.println(name + " ELq=" + getELq());
    }

    public double getEWq() {
        ewq = totalWaitTime / (double) totalClientNum;
        return ewq;
    }

    public double getELq() {
        elq = totalWaitTime2 / (double) endTime;
        return elq;
    }

    /**
     * 接收顾客
     * @param client
     */
    public void acceptClient(Client client) {
        allArrivedClients.add(client);
        //每到达一个顾客就计数
        totalClientNum++;
//        System.out.println("顾客" + client.name + "到达:" + client.arriveTime);
        //交给服务过程处理
        serveProcess.processClient(client);
    }
}
