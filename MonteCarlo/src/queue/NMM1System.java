package queue;

import java.util.Random;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class NMM1System extends QSystem {

    int systemNum;
    QSystem[] mm1Systems;
    Random random = new Random();
//    int currentFinishedClientNum = 0;

    public NMM1System(String name, ArriveProcess arriveProcess, double miu, int systemNum) {
        super();
        this.systemNum = systemNum;
        this.name = name;
        this.arriveProcess = arriveProcess;
        mm1Systems = new MM1System[systemNum];
        arriveProcess.addSystem(this);
        for (int i = 0; i < mm1Systems.length; i++) {
            mm1Systems[i] = new MM1System("M|M|1-" + i, miu, String.valueOf(i));

        }
    }

    @Override
    public void acceptClient(Client client) {
        //随机选择一个队伍排队
        int id = random.nextInt(systemNum);
//        System.out.println(id);
        mm1Systems[id].acceptClient(client);
//                System.out.println("[ n-M|M|1 ]:平均等待时间："+getEWs());
//        super.processClient(client);
    }
//    long curTime = 0;

    /**
     * 要加上仍在排队的顾客的等待时间！！！
     */
    @Override
    public void showResult() {
//        totalClientNum = 0;
//        totalWaitTime = 0;
        //
//        totalWaitTime2=0;
        //不要忘了stop
        printTitle();
//        System.out.println("顾客序号 服务员 顾客到达时间间隔  到达时刻 等待时间 接受服务时刻 服务时间 预计离开时刻");
        for (QSystem s : mm1Systems) {
            s.stop(endTime);
            totalWaitTime2+=s.totalWaitTime2;
            //
            totalWaitTime += s.totalWaitTime;
            totalClientNum += s.totalClientNum;
            for (Client e : s.allArrivedClients) {
                e.print();
//                System.out.println(e);
            }
        }
        //在这里输出所有顾客的情况
        System.out.println(name + " EWq=" + getEWq());
        System.out.println(name + " ELq=" + getELq());
    }

    @Override
    public void stop(long time) {
        endTime = time;
//        showResult();
    }
}
