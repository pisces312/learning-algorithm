package queue;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *服务时间统一产生！！！！
 * @author Administrator
 */
public class Client {
    //记录该顾客到达的时间间隔

    long dt = 0;
    long arriveTime = 0;
    long acceptedTime = 0;
    long waitTime = 0;
    String name;
    //为显示结果方便，记录服务的时间
    //因为提前赋一个服务时间，所以实际的服务时间可能是零，即没有被服务到
    public long serveTime = 0;
    public String serverName = "-1";
    //一个顾客实例只能给一个系统处理
//    QSystem system;
    //保存是由哪个到达过程产生的
//    ArriveProcess arriveProcess;

    public Client(String name, long arriveTime, long dt, long serveTime) {
        this.name = name;
        this.arriveTime = arriveTime;
        this.serveTime = serveTime;
        this.dt = dt;
    }

    /**
     * 告诉系统自己已经在接受服务
     */
    public void accepted(long acceptedTime) {
        this.acceptedTime = acceptedTime;
        waitTime = acceptedTime - arriveTime;
    }

    public void print() {
        long actualServerTime = serveTime;
        if (serverName.startsWith("-")) {
            actualServerTime = 0;//没有服务
        }
        System.out.printf("%5d%5d%10d%10d%10d%10d%10d%10d\n", Integer.parseInt(name), Integer.parseInt(serverName), dt, arriveTime, waitTime, acceptedTime, actualServerTime, (acceptedTime + actualServerTime));
//        System.out.printf("%8d%8d%8d%8d%8d%8d%8d%8d\n", Integer.parseInt(name), Integer.parseInt(serverName), dt, arriveTime, waitTime, acceptedTime, serveTime, (acceptedTime + serveTime));
//        System.out.printf("%8d",a);
    }

//    @Override
//    public String toString() {
////System.out.println("顾客序号 服务员 顾客到达时间间隔  到达时刻 等待时间 接受服务时刻 服务时间 预计离开时刻");
////        StringBuffer sb = new StringBuffer(name + " "+dt +" "+ serveTime+" "+arriveTime+" "+waitTime+" "+acceptedTime);
////        return sb.toString();
////        return name + ","+dt +","+ serveTime+","+arriveTime+","+waitTime+","+acceptedTime;
//        return name + " " + serverName + " " + dt + " " + arriveTime + " " + waitTime + " " + acceptedTime + " " + serveTime + " " + (acceptedTime + serveTime);
//    }
}
