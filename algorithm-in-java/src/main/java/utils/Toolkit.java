/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.Random;

/**
 *
 * @author DELL
 */
public final class Toolkit {

    static Random random = new Random();

    public static void shuffle(int[] array) {
        for (int i = 0; i < 10; i++) {
            int r1 = random.nextInt(array.length);
            int r2 = random.nextInt(array.length);
            int temp = array[r1];
            array[r1] = array[r2];
            array[r2] = temp;
        }
    }
}
