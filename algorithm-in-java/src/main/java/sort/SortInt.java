package sort;

import java.util.Arrays;

public class SortInt {
    public static void main(String[] args) {
        int x[] = {4, 3, 6, 2, 1, 6, 5};
//        insertSort(x);
//        selectSort(x);
//        heapSort(x);
//        mergeSort(x);
        bubbleSort(x);

        System.out.println(Arrays.toString(x));
    }

    private static void swap(int[] array, int i, int j) {
        int t = array[i];
        array[i] = array[j];
        array[j] = t;
    }

    //! must pass the length, because heap's size is changing
    public static void heapSiftMax(int x[], int i, int n) {
        int c = 2 * i + 1;
        while (c < n) {
            if (c + 1 < n && x[c] < x[c + 1])
                ++c;
            if (x[i] > x[c])
                break;
            swap(x, i, c);
            i = c;
            c = 2 * i + 1;
        }
    }

    /**
    n=no+n1+n2
    n=n1+2n2+1
    no=n2+1
    n1=1 (For complete binary tree)
    =>
    n=no+1+n2=2n2+2
    n2=n/2-1
     */
    public static void heapSort(int x[]) {
        //bottom up to create heap
        for (int i = x.length / 2 - 1; i >= 0; --i)
            heapSiftMax(x, i, x.length);

        for (int i = x.length - 1; i > 0; --i) {
            swap(x, 0, i);
            //!the length has changed, the heap becomes small here
            heapSiftMax(x, 0, i);
        }
    }

    /**
     * x1[b,m]
     * x2[m+1,e]
     */
    public static void mergeSortedArray(int[] x, int b, int m, int e, int[] tmp) {
        int i = b, j = m + 1, k = b;

        while (i <= m && j <= e)
            if (x[i] <= x[j])
                tmp[k++] = x[i++];
            else
                tmp[k++] = x[j++];

        while (i <= m)
            tmp[k++] = x[i++];

        while (j <= e)
            tmp[k++] = x[j++];

        //! must copy back to original array
        for (k = b; k <= e; ++k)
            x[k] = tmp[k];

    }

    private static void _mergeSort(int[] x, int b, int e, int[] tmp) {
        if (b < e) {
            int m = (b + e) / 2;
            _mergeSort(x, b, m, tmp);
            _mergeSort(x, m + 1, e, tmp);
            mergeSortedArray(x, b, m, e, tmp);
        }
    }

    //in place sort
    public static void mergeSort(int[] x) {
        int[] tmp = new int[x.length];
        _mergeSort(x, 0, x.length - 1, tmp);
    }



    public static void insertSort(int[] x) {
        int i, j, t;
        //First element is treated as sorted
        //Just start with 1
        for (i = 1; i < x.length; ++i) {
            t = x[i];
            //Key: Find a place to insert
            for (j = i; j > 0; --j) {
                if (x[j - 1] > t) //">" make it stable
                    x[j] = x[j - 1];
                else
                    break; //exit loop when an element >= current element
            }
            x[j] = t;
        }
    }

    public static void selectSort(int[] x) {
        //Target index
        for (int i = 0; i < x.length - 1; ++i) {
            int minIdx = i;
            for (int j = i + 1; j < x.length; ++j) {
                if (x[j] < x[minIdx]) {
                    minIdx = j;
                }
            }
            swap(x, i, minIdx);
        }
    }

    public static void bubbleSort(int[] x) {
        for (int i = x.length - 1; i > 0; --i)  //n-1 iterations
            //! right border, j<=i
            //find one min/max value in one iteration
            for (int j = 1; j <= i; ++j) 
                if (x[j - 1] > x[j]) 
                    swap(x, j - 1, j);
    }

}
