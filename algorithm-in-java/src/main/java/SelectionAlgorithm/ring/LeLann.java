/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package SelectionAlgorithm.ring;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import utils.Toolkit;

/**
 *时间复杂度为O(n^2)
 * @author DELL
 */
public class LeLann {

    public static CyclicBarrier barrier;
//    public static CountDownLatch latch;
    static int countSend = 0;

    public static class Process implements Runnable {

        int uid;
        State state;
        Process nextProcess;

        public Process(int uid/*, Process nextProcess*/) {
            this.uid = uid;
            state=State.UNKNOWN;
//            this.nextProcess = nextProcess;
        }

        @Override
        public String toString() {
            return String.valueOf(uid);
        }

        public void setNextProcess(Process nextProcess) {
            this.nextProcess = nextProcess;
        }
//
//        public void send(Process p) {
//            p.accept(uid);
//        }
//
//        public void send(int id, Process p) {
//            p.accept(id);
//        }

        public void send(int id) {
            countSend++;
            System.out.println(uid + " send " + id + " to " + nextProcess);
            nextProcess.accept(id);

        }

        /**
         * 只要收到的id不是自己，就转发
         * @param v
         */
        public void accept(int v) {
            System.out.println(uid + " receives " + v);
            if (v == uid) {
                state = State.LEADER;
                System.out.println("leader is " + uid);
            } else {
                send(v);
            }

        }

        public void run() {
            try {
                barrier.await();
            } catch (InterruptedException ex) {
//                Logger.getLogger(LCR.class.getName()).log(Level.SEVERE, null, ex);
            } catch (BrokenBarrierException ex) {
//                Logger.getLogger(LCR.class.getName()).log(Level.SEVERE, null, ex);
            }
            send(uid);
//            try {
//                barrier.await();
//            } catch (InterruptedException ex) {
//                Logger.getLogger(NewMain.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (BrokenBarrierException ex) {
//                Logger.getLogger(NewMain.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int size = 10;
//        latch = new CountDownLatch(1);
        barrier = new CyclicBarrier(size + 1);
        ExecutorService exec = Executors.newCachedThreadPool();
        Process[] processes = new Process[size];

        int[] array = new int[size];

        for (int i = 0; i < size; i++) {
            array[i] = i;
        }
        Toolkit.shuffle(array);
        processes[0] = new Process(array[0]);
        System.out.println(array[0]);
        for (int i = 1, n = processes.length; i < n; i++) {
            processes[i] = new Process(array[i]);
            processes[i - 1].setNextProcess(processes[i]);
            System.out.println(array[i]);
            exec.execute(processes[i - 1]);
        }
        processes[size - 1].setNextProcess(processes[0]);
        exec.execute(processes[size - 1]);
        try {
//        latch.countDown();
            barrier.await();

//            barrier.await();
        } catch (InterruptedException ex) {
            Logger.getLogger(LCR.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BrokenBarrierException ex) {
            Logger.getLogger(LCR.class.getName()).log(Level.SEVERE, null, ex);
        }
        exec.shutdown();
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(LCR.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(size + "个进程发送了" + countSend + "条消息");
//        for(i=0;i<size;i++){
//
//        }

    }
}
