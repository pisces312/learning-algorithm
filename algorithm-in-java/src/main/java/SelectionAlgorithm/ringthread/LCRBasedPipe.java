/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package SelectionAlgorithm.ringthread;

import SelectionAlgorithm.ring.State;
import utils.Toolkit;

import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class LCRBasedPipe {

    public static CyclicBarrier barrier;
//    public static CountDownLatch latch;
    static int countSend = 0;

    public static class Process implements Runnable {

        int uid;
        State state;
        PipedReader pr = new PipedReader();
        PipedWriter pw = new PipedWriter();
        Process nextProcess;

        public Process(int uid/*, Process nextProcess*/) {
            this.uid = uid;
            state = State.UNKNOWN;
//            this.nextProcess = nextProcess;
        }

        public void connect(Process p) {
            nextProcess = p;
            try {
                pw.connect(p.getPipedReader());
            } catch (IOException ex) {
                Logger.getLogger(LCRBasedPipe.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        public PipedReader getPipedReader() {
            return pr;
        }

        @Override
        public String toString() {
            return String.valueOf(uid);
        }

        public void send(int id) {
            countSend++;
            try {
//                pw.
                System.out.println(uid + " send " + id + " to " + nextProcess);
                pw.write(id);
            } catch (IOException ex) {
                Logger.getLogger(LCRBasedPipe.class.getName()).log(Level.SEVERE, null, ex);
            }


        }

        public void run() {
            try {
                barrier.await();
            } catch (InterruptedException ex) {
                Logger.getLogger(LCRBasedPipe.class.getName()).log(Level.SEVERE, null, ex);
            } catch (BrokenBarrierException ex) {
                Logger.getLogger(LCRBasedPipe.class.getName()).log(Level.SEVERE, null, ex);
            }
            send(uid);
            try {
                while (true) {
                    int v = pr.read();
                    System.out.println(uid + " receives " + v);
                    if (v == -1) {
                        break;
                    } else if (v > uid) {
                        send(v);
                    } else if (v == uid) {
                        state = State.LEADER;
                        System.out.println("leader is " + uid);
//                        send(-1);
                        break;
                    }

                }
            } catch (IOException ex) {
                Logger.getLogger(LCRBasedPipe.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                //???只关闭管道一端是否可行
//                if (pr != null) {
//                    try {
//                        pr.close();
//                    } catch (IOException ex) {
//                        Logger.getLogger(LCR.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
                if (pw != null) {
                    try {
                        pw.close();
                    } catch (IOException ex) {
                        Logger.getLogger(LCRBasedPipe.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            try {
                barrier.await();
            } catch (InterruptedException ex) {
                Logger.getLogger(LCRBasedPipe.class.getName()).log(Level.SEVERE, null, ex);
            } catch (BrokenBarrierException ex) {
                Logger.getLogger(LCRBasedPipe.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int size = 10;
//        latch = new CountDownLatch(1);
        barrier = new CyclicBarrier(size + 1);
        ExecutorService exec = Executors.newCachedThreadPool();
        Process[] processes = new Process[size];

        int[] array = new int[size];

        for (int i = 0; i < size; i++) {
            array[i] = i;
        }
        Toolkit.shuffle(array);
        processes[0] = new Process(array[0]);
        System.out.println(array[0]);
        for (int i = 1, n = processes.length; i < n; i++) {
            processes[i] = new Process(array[i]);
            processes[i - 1].connect(processes[i]);
            System.out.println(array[i]);
            exec.execute(processes[i - 1]);
        }
        processes[size - 1].connect(processes[0]);
        exec.execute(processes[size - 1]);
        try {
//        latch.countDown();
            barrier.await();

            barrier.await();
            System.out.println(size + "个进程发送了" + countSend + "条消息");
            System.out.println(size * Math.log(size));
        } catch (InterruptedException ex) {
            Logger.getLogger(LCRBasedPipe.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BrokenBarrierException ex) {
            Logger.getLogger(LCRBasedPipe.class.getName()).log(Level.SEVERE, null, ex);
        }
        exec.shutdown();
//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException ex) {
//            Logger.getLogger(LCR.class.getName()).log(Level.SEVERE, null, ex);
//        }

//        for(i=0;i<size;i++){
//
//        }

    }
}
