package tree.bitree;

import java.util.HashMap;

public class FindMaxDistinctNode {


    //    class Tree {
    //        public int x;
    //        public Tree l;
    //        public Tree r;
    //    }
    //
    //    private HashSet<Integer> set;
    //    private int maxNum;
    //
    //    private void traverseTree(Tree t) {
    //        if (t == null)
    //            return;
    //        if (set.contains(t.x)) {
    //            if (t.l == null && t.r == null) {
    //                set.remove(t.x);
    //            }
    //        } else {
    //            set.add(t.x);
    //            if (maxNum < set.size()) {
    //                maxNum = set.size();
    //            }
    //        }
    //    }
    //
    //    public int solution(Tree T) {
    //        set = new HashSet<>();
    //        maxNum = 0;
    //        traverseTree(T);
    //        return maxNum;
    //    }



    //    class Tree {
    //        public int x;
    //        public Tree l;
    //        public Tree r;
    //    }
    //
    //    private HashMap<Integer, Integer> map;
    //    private int maxNum;
    //
    //    private void traverseTree(Tree t) {
    //        if (t == null)
    //            return;
    //        Integer c = map.get(t.x);
    //        if (c == null) {
    //            map.put(t.x, 1);
    //            if (maxNum < map.size()) {
    //                maxNum = map.size();
    //            }
    //        } else {
    //            if (t.l == null && t.r == null) {
    //                if (c == 1) {
    //                    map.remove(t.x);
    //                } else {
    //                    map.put(t.x, c - 1);
    //                }
    //            }
    //        }
    //        traverseTree(t.l);
    //        traverseTree(t.r);
    //
    //    }
    //
    //    public int solution(Tree T) {
    //        map = new HashMap<>();
    //        maxNum = 0;
    //        traverseTree(T);
    //        return maxNum;
    //    }


    private HashMap<Integer, Integer> map;
    private int maxNum;

    private void traverseTree(TreeNode t) {
        if (t == null) {
            if (maxNum < map.size()) {
                maxNum = map.size();
            }
            return;
        }

        Integer c = map.get(t.val);
        if (c == null) {
            map.put(t.val, 1);
        } else {
            map.put(t.val, c + 1);
        }

        traverseTree(t.left);
        traverseTree(t.right);

        c = map.get(t.val);
        if (c == 1) {
            map.remove(t.val);
        } else {
            map.put(t.val, c - 1);
        }

    }

    public int solution(TreeNode T) {
        map = new HashMap<>();
        maxNum = 0;
        traverseTree(T);
        return maxNum;
    }


}
