package tree.bitree;

import org.junit.Test;

public class BitreeSerializerTest {

    @Test
    public void testPreOrderSerializer() {
//        30       
//        / \   
//       /   \  
//      10   20   
//      /    / \ 
//     50   45 35 
        TreeNode root = new TreeNode(30);
        TreeNode node1 = new TreeNode(10);
        TreeNode node2 = new TreeNode(20);
        TreeNode node3 = new TreeNode(50);
        TreeNode node4 = new TreeNode(45);
        TreeNode node5 = new TreeNode(35);
        root.left = node1;
        root.right = node2;
        node1.left = node3;
        node2.left = node4;
        node2.right = node5;

        //30,10,50,#,#,#,20,45,#,#,35,#,#"
        String treeStr = BitreePreOrderSerializer.serialize(root);
        System.out.println(treeStr);

        root = BitreePreOrderSerializer.deserialize(treeStr);
        verifyTree(root);



    }

    @Test
    public void testPreOrderSerializer2() {
        TreeNode root = BitreePreOrderSerializer.deserialize("3,9,20,#,#,15,7,#,#,#,#");
        verifyTree(root);
        String treeStr = BitreePreOrderSerializer.serialize(root);
        System.out.println(treeStr);
    }


    private void verifyTree(TreeNode deser) {
        BitreePrinter.printTree(deser);
        BitreeTraversal.preOrder(deser);
        System.out.println();
        BitreeTraversal.inOrder(deser);
        System.out.println();
    }

    //
    //3,9,20,#,#,15,7
//    3
//    / \
//   9  20
//     /  \
//    15   7
//    
//    
//    
//  1
// / \
//2   3
/// \   \
//4   5   6
// / \
//7   8
//it will be serialized to {1,2,3,4,5,#,6,#,#,7,8}.
    //???incorrect
    @Test
    public void test() {

//        String treeStr="1,#,2";

//        String treeStr="1,2,3,4,5,#,6,#,#,7,8";

//        String treeStr = "3,9,20,#,#,15,7";

//        TreeNode root = BitreeBFSSerializer.deserialize(treeStr);
//        String str = BitreeBFSSerializer.serialize(root);
//        System.out.println(str);


//        BitreePrinter.printNode(root);
//
//        BitreeTraversal.preOrder(root);
//        System.out.println();
//        BitreeTraversal.inOrder(root);
//        System.out.println();



    }
}
