package tree.bitree;

import java.util.Stack;

public class BitreeTraversal {

    public static void preOrder(TreeNode p) {
        if (p != null) {
            System.out.print(p.val + " ");
            preOrder(p.left);
            preOrder(p.right);
        }
    }

    public static void inOrder(TreeNode p) {
        if (p != null) {
            inOrder(p.left);
            System.out.print(p.val + " ");
            inOrder(p.right);
        }
    }


    public static void preOrderItr(TreeNode p) {
        Stack<TreeNode> stack = new Stack<>();
        while (p != null || !stack.isEmpty()) {
            if (p != null) {
                System.out.print(p.val + " ");
                stack.push(p);
                p = p.left;
            } else {
                p = stack.pop();
                p = p.right;
            }
        }
        System.out.println();
    }

    public static void inOrderItr(TreeNode p) {
        Stack<TreeNode> stack = new Stack<>();
        while (p != null || !stack.isEmpty()) {
            if (p != null) {
                stack.push(p);
                p = p.left;
            } else {
                p = stack.pop();
                System.out.print(p.val + " ");
                p = p.right;
            }
        }
        System.out.println();
    }

    /**
     * 第二种思路：
     * 
     * 要保证根结点在左孩子和右孩子访问之后才能访问，
     * 
     * 因此对于任一结点P，先将其入栈。
     * 如果P不存在左孩子和右孩子，则可以直接访问它；
     * 或者P存在左孩子或者右孩子，但是其左孩子和右孩子都已被访问过了，则同样可以直接访问该结点。
     * 若非上述两种情况，则将P的右孩子和左孩子依次入栈，
     * 
     * 这样就保证了每次取栈顶元素的时候，
     *    左孩子在右孩子前面被访问，
     *    左孩子和右孩子都在根结点前面被访问。
     */
    public static void postOrderItr(TreeNode p) {
        System.out.println("postOrderItr(TreeNode p)");
        if (p == null) {
            return;
        }
        TreeNode cur = null; //当前结点 
        TreeNode pre = null; //前一次访问的结点 
        Stack<TreeNode> s = new Stack<>();
        s.push(p);
        while (!s.isEmpty()) {
            cur = s.peek();
            //1) no children
            if ((cur.left == null && cur.right == null) ||
            //2) all children are visited
            //Way 1:
//                (pre != null && (pre == cur.left || pre == cur.right))) {
            //Way 2:
                (cur.right != null && pre == cur.right) || (cur.left != null && pre == cur.left)) {

                //Debug - print stack content
                System.out.println(s);
                //Visit                
                System.out.print(cur.val + " ");
                s.pop();
                pre = cur;
            } else {
                //push right first, so it will visit left first
                if (cur.right != null)
                    s.push(cur.right);
                if (cur.left != null)
                    s.push(cur.left);
            }
        }
        System.out.println();
    }

    /**
     * 第一种思路：对于任一结点P，将其入栈，然后沿其左子树一直往下搜索，
     * 直到搜索到没有左孩子的结点，此时该结点出现在栈顶，但是此时不能将
     * 其出栈并访问，因此其右孩子还为被访问。所以接下来按照相同的规则对
     * 其右子树进行相同的处理，当访问完其右孩子时，该结点又出现在栈顶，
     * 此时可以将其出栈并访问。这样就保证了正确的访问顺序。可以看出，在
     * 这个过程中，每个结点都两次出现在栈顶，只有在第二次出现在栈顶时，
     * 才能访问它。因此需要多设置一个变量标识该结点是否是第一次出现在栈顶。
     */
    public static void postOrderWithFlagItr(TreeNodeWithFlag p) {
        Stack<TreeNodeWithFlag> s = new Stack<>();
        while (p != null || !s.isEmpty()) {
            while (p != null) {
                p.isFirst = true;
                s.push(p); //1st push
                //Debug - print stack content
                System.out.println("push " + p + " " + s);
                p = (TreeNodeWithFlag) p.left;
            }

            //check empty before 'pop'
            //optional here, because when comes to loop
            //it has at least one node
            if (!s.isEmpty()) {
                //Way 1: prefer
                TreeNodeWithFlag t = s.pop();
                //Debug - print stack content
                System.out.println("pop " + t + " " + s);
                if (t.isFirst) {
                    t.isFirst = false;
                    //Debug - print stack content
                    System.out.println("push " + t + " " + s);
                    s.push(t); //2nd push
                    p = (TreeNodeWithFlag) t.right;
                } else {
                    //Visit
                    System.out.println("visit " + t.val);
                    p = null;//!optional here, because we use 't' to check 
                }
                //
                //Way 2:
//                p = s.peek();
//                if (p.isFirst) {
//                    p.isFirst = false;
//                    p = (TreeNodeWithFlag) p.right;
//                } else {
//                    s.pop();
//                    System.out.print(p.val + " ");
//                    p = null;
//                }
            }
        }
//        System.out.println();

    }



    public static void postOrderItr2(TreeNodeWithFlag p) {
        Stack<TreeNodeWithFlag> s = new Stack<>();
        while (p != null || !s.isEmpty()) {
            if (p != null) {
                p.isFirst = true;
                s.push(p); //1st push
                p = (TreeNodeWithFlag) p.left;
            } else {
                //Way 1:
                TreeNodeWithFlag t = s.pop();
                if (t.isFirst) {
                    t.isFirst = false;
                    s.push(t); //2nd push
                    p = (TreeNodeWithFlag) t.right;
                } else {
                    System.out.print(t.val + " ");
                    p = null;
                }
                //
                //Way 2:
//                p = s.peek();
//                if (p.isFirst) {
//                    p.isFirst = false;
//                    p = (TreeNodeWithFlag) p.right;
//                } else {
//                    s.pop();
//                    System.out.print(p.val + " ");
//                    p = null;
//                }
            }
        }
        System.out.println();

    }


    public static void postOrderItr3(TreeNodeWithFlag p) {
        Stack<TreeNodeWithFlag> s = new Stack<>();
        while (p != null || !s.isEmpty()) {
            if (p != null) {
                p.isFirst = true;
                s.push(p); //1st push
                p = (TreeNodeWithFlag) p.left;
            } else {
                p = s.peek();
                if (p.isFirst) {
                    p.isFirst = false;
                    p = (TreeNodeWithFlag) p.right;
                } else {
                    s.pop();
                    System.out.print(p.val + " ");
                    //!!indicate that next iteration should check element in stack
                    //!must set null here, p is changed by 's.peek()'
                    p = null;
                }
            }
        }
        System.out.println();

    }


    //Add contidion to optimization
    public static void postOrderWithFlagItr4(TreeNodeWithFlag p) {
        Stack<TreeNodeWithFlag> s = new Stack<>();
        while (p != null || !s.isEmpty()) {
            while (p != null) {
                p.isFirst = true;
                s.push(p); //1st push
                //Debug - print stack content
                System.out.println("push " + p + " " + s);
                p = (TreeNodeWithFlag) p.left;
            }

            //check empty before 'pop'
            //optional here, because when comes to loop
            //it has at least one node
            if (!s.isEmpty()) {
                //Way 1: prefer
                TreeNodeWithFlag t = s.pop();
                //Debug - print stack content
                System.out.println("pop " + t + " " + s);
                if (t.isFirst) {
                    t.isFirst = false;
                    //Add condition for optimization
                    if (t.right != null) {
                        //Debug - print stack content
                        System.out.println("push " + t + " " + s);
                        s.push(t); //2nd push
                        p = (TreeNodeWithFlag) t.right;
                    } else {
                        //Visit
                        System.out.println("visit " + t.val);
                    }
                } else {

                    //Visit
                    System.out.println("visit " + t.val);
                    p = null;//!optional here, because we use 't' to check 
                }
                //
                //Way 2:
//                p = s.peek();
//                if (p.isFirst) {
//                    p.isFirst = false;
//                    p = (TreeNodeWithFlag) p.right;
//                } else {
//                    s.pop();
//                    System.out.print(p.val + " ");
//                    p = null;
//                }
            }
        }
//        System.out.println();

    }

}
