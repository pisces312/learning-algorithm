package tree.bitree;

public class MinDepth {

//correct version
    public int minDepth(TreeNode root) {
        if (root == null)
            return 0;
        int left = minDepth(root.left);
        int right = minDepth(root.right);
        if (left > 0 && right > 0)
            return Math.min(left, right) + 1;
        return Math.max(left, right) + 1;
    }



    //!incorrect
//    private int min = Integer.MAX_VALUE;
//
//    private void _minDepth(TreeNode root, int d) {
//        if (root == null) {
//            if (d < min)
//                min = d;
//            return;
//        }
//        _minDepth(root.left, d + 1);
//        _minDepth(root.right, d + 1);
//    }
//
//    public int minDepth2(TreeNode root) {
//        //Handle root and its direct children specially
//        if (root == null)
//            return 0;
//        if (root.left == null && root.right == null)
//            return 1;
//        if (root.left != null)
//            _minDepth(root.left, 1);
//        if (root.right != null)
//            _minDepth(root.right, 1);
//        return min;
//    }

}
