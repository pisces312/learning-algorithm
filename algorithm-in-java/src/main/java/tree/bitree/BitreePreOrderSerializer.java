package tree.bitree;

import java.util.StringTokenizer;

//preorder
public class BitreePreOrderSerializer {
    public static String serialize(TreeNode root) {

        if (root == null)
            return "#";

        return root.val + "," + serialize(root.left) + "," + serialize(root.right);


//        if (root == null)
//            return "# ";
//
//        return root.val + " " + serialize(root.left) + serialize(root.right);

    }

    public static TreeNode deserialize(String res) {
        return deserialize(res, TreeNode.class);
    }

    public static <T extends TreeNode> T deserialize(String res, Class<T> clazz) {
        StringTokenizer st = new StringTokenizer(res, ",");
        return deserialize(st, clazz);
    }


    private static <T extends TreeNode> T  deserialize(StringTokenizer st, Class<T> clazz) {
        if (!st.hasMoreTokens())
            return null;
        String val = st.nextToken();
        if (val.equals("#"))
            return null;

        T tree = null;
        try {
            tree = clazz.newInstance();
            tree.val = Integer.parseInt(val);
//          TreeNode tree = new TreeNode(Integer.parseInt(val));
            tree.left = deserialize(st, clazz);
            tree.right = deserialize(st, clazz);

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return tree;
    }


    public static void main(String[] args) {
        TreeNode root = new TreeNode(30);
        TreeNode node1 = new TreeNode(10);
        TreeNode node2 = new TreeNode(20);
        TreeNode node3 = new TreeNode(50);
        TreeNode node4 = new TreeNode(45);
        TreeNode node5 = new TreeNode(35);
        root.left = node1;
        root.right = node2;
        node1.left = node3;
        node2.left = node4;
        node2.right = node5;

        System.out.println(serialize(root));
        TreeNode node = deserialize("30 10 50 # # # 20 45 # # 35 # # ");
    }
}
