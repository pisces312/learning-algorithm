package tree.bitree;

import org.junit.Test;

public class BitreeTraversalTest {
    @Test
    public void testPostOrderIter() {

        TreeNodeWithFlag root = BitreePreOrderSerializer
            .deserialize("30,10,50,#,#,#,20,45,#,#,35,#,#", TreeNodeWithFlag.class);
        BitreePrinter.printTree(root);

//        BitreeTraversal.preOrderItr(root);

//        BitreeTraversal.inOrderItr(root);
//        BitreeTraversal.preOrder(root);
//        System.out.println();
        System.out.println("postOrderItr");
        BitreeTraversal.postOrderItr((TreeNode)root);
        
        System.out.println("postOrderWithFlagItr");
        BitreeTraversal.postOrderWithFlagItr(root);
//        BitreeTraversal.postOrderItr2(root);
//        BitreeTraversal.postOrderItr3(root);

    }
}
