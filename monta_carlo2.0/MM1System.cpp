#include "MM1System.h"
#include "ServeProcess.h"
//MM1System::MM1System() {}
MM1System::MM1System(string name, double miu, int serverName) {
    //不能在内部再调用构造函数
    init(name, NULL, miu, serverName);
}

void MM1System::init(string name2, ArriveProcess* arriveProcess2, double miu, int serverName) {
    //cout<<"init mm1system\n";
    totalClientNum=0;
    totalWaitTime=0;
    totalWaitTime2=0;
    lastStateTime=0;
    name = name2;
    //cout<<"a "<<name<<endl;
    if (arriveProcess2 != NULL) {
        arriveProcess = arriveProcess2;
        arriveProcess->addSystem(this);
    }
//        arriveProcess = new ArriveProcess(0.2, 10);
    serveProcess = new ServeProcess(1);
    //这里加入两个监视器，其中一个给外层

    serveProcess->setSystem(this);
    Server* s=new Server(serverName, miu);
    serveProcess->addServer(s);

    serverNum = 1;
//        servers.add(new Server());
    queueLengthLimited = -1;
    //cout<<"a2 "<<name<<endl;
}

MM1System::MM1System(string name, ArriveProcess* arriveProcess, double miu) {
    init(name, arriveProcess, miu, 0);

}
