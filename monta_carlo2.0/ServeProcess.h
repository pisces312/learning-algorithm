#ifndef SERVEPROCESS_H_INCLUDED
#define SERVEPROCESS_H_INCLUDED
#include "QSystem.h"
#include "Server.h"
#include "Client.h"
#include<iostream>
#include<list>
using namespace std;
class QSystem;
class ServeProcess {
    public:
    //一个服务过程只能对应一个系统

    QSystem *system;
    //????用优先队列？？，找服务器endTime最小的？？
    list<Server*> freeServers;
//    LinkedList<Server> freeServers = new LinkedList<Server>();
    unsigned int maxServerNum;

    ServeProcess(int maxServerNum);

    void setSystem(QSystem* listener);

    void addServer(Server* server) ;
    /**
     * 获得一个服务员后必须马上使用！！
     * 输入应该为当前时刻
     * 必须取最小者！！！
     * @param curTime
     * @return
     */
    Server* getCurrentFreeServer(long curTime);
    void processWaitingClient(long curTime) ;
    void processClient(Client* client);
};



#endif // SERVEPROCESS_H_INCLUDED
