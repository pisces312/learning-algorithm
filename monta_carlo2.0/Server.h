#ifndef SERVER_H_INCLUDED
#define SERVER_H_INCLUDED
#include<string>
#include<iostream>
#include<ctime>
#include<cmath>
#include "Client.h"
using namespace std;
class Server {
public:
    long serverEndTime ;
//服务能力
    double miu;
    int name;
    //string name;
    /**
     *对顾客的服务过程
     * @param client
     */
    void serveClient(Client* client);

    Server(int name, double miu);
};


#endif // SERVER_H_INCLUDED
