#ifndef CLIENT_H_INCLUDED
#define CLIENT_H_INCLUDED
//#include<stdio.h>
#include<iostream>
#include<string>
using namespace std;
class Client{
    public:
    long dt;
    long arriveTime;
    long acceptedTime;
    long waitTime;
    //string name;
    int name;
    //为显示结果方便，记录服务的时间
    long serveTime;
    //string serverName;
    int serverName;

    Client(int name2, long arriveTime2, long dt2);

    /**
     * 告诉系统自己已经在接受服务
     */
    void accepted(long acceptedTime);

    void print();
};


#endif // CLIENT_H_INCLUDED
