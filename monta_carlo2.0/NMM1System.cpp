#include "NMM1System.h"
#include "QSystem.h"
#include "ArriveProcess.h"
#include "ServeProcess.h"
#include<string>
NMM1System::NMM1System(string name2, ArriveProcess* arriveProcess2, double miu, int systemNum2) {
        totalClientNum=0;
        totalWaitTime=0;
        totalWaitTime2=0;
        lastStateTime=0;
        systemNum = systemNum2;
        name = name2;
        arriveProcess = arriveProcess2;

        mm1Systems = new MM1System*[systemNum];//("M|M|1-id", miu, "id");
        arriveProcess->addSystem(this);
        //QSystem* s=mm1Systems;
        for (int i = 0; i < systemNum; i++) {
            //MM1System* s=new MM1System("M|M|1-id", miu, "id");
            //cout<<"c "<<s->name<<endl;
            //mm1Systems[i]=s;
            mm1Systems[i]=new MM1System("M|M|1-id", miu, i);
            //(*mm1Systems)[i]=new MM1System("M|M|1-id", miu, "id");
            //mm1Systems[i].name="M|M|1";
            //mm1Systems[i].miu=miu;
            //mm1Systems[i].
            //s=new MM1System("M|M|1-id", miu, "id");
            //s++;
            //mm1Systems
            //cout<<"b "<<(*mm1Systems[i]).name<<endl;

        }
        srand( (unsigned)time( NULL ) );
    }


    void NMM1System::acceptClient(Client* client) {
        //随机选择一个队伍排队
        //cout<<"nmm1\n";
        int id = rand()%systemNum;
        //cout<<"id="<<id<<endl;
        //cout<<"b\n";
//        System.out.println(id);
        mm1Systems[id]->acceptClient(client);
        //cout<<"accepted!\n";
//                System.out.println("[ n-M|M|1 ]:平均等待时间："+getEWs());
//        super.processClient(client);
    }
//    long curTime = 0;

    /**
     * 要加上仍在排队的顾客的等待时间！！！
     */

    void NMM1System::showResult() {
        totalClientNum = 0;
        totalWaitTime = 0;
        //
        totalWaitTime2=0;
        //不要忘了stop
        printTitle();
//        System.out.println("顾客序号 服务员 顾客到达时间间隔  到达时刻 等待时间 接受服务时刻 服务时间 预计离开时刻");
        //QSystem* s= mm1Systems;
        for (int i=0;i<systemNum;i++) {
            //为每个系统创建一个新客户
            MM1System* s=mm1Systems[i];
            s->stop(endTime);
            totalWaitTime2+=s->totalWaitTime2;
            //
            totalWaitTime += s->totalWaitTime;
            totalClientNum += s->totalClientNum;


            for (list<Client*>::iterator itr=s->allArrivedClients.begin();itr!=s->allArrivedClients.end();itr++) {
                Client* client=*itr;
                client->print();
            }
            //s++;
        }
        //在这里输出所有顾客的情况
        cout<<name<<" EWq="<<getEWq()<<endl;
        cout<<name<<" ELq="<<getELq()<<endl;
    }


    void NMM1System::stop(long time) {
        endTime = time;
//        showResult();
    }
