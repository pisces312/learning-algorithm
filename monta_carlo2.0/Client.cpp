#include<iostream>
#include<string>
#include<iomanip>
#include"Client.h"
using namespace std;
Client::Client(int name2, long arriveTime2, long dt2) {
    name = name2;
    arriveTime = arriveTime2;
    dt = dt2;
    acceptedTime = 0;
    waitTime = 0;
    //为显示结果方便，记录服务的时间
    serveTime = 0;
    serverName = -1;
}

/**
 * 告诉系统自己已经在接受服务
 */
void Client::accepted(long acceptedTime2) {
    acceptedTime = acceptedTime2;
    waitTime = acceptedTime2 - arriveTime;
}

void Client::print() {
    cout<<setw(8)<<name<<setw(8)<<serverName<<setw(8)<<dt<<setw(8)<<arriveTime<<setw(8)<<waitTime<<setw(10)<<acceptedTime<<setw(10)<<serveTime<<setw(10)<<acceptedTime+serveTime<<endl;
    //printf("%5d%5d%10d%10d%10d%10d%10d%10d\n", Integer.parseInt(name), Integer.parseInt(serverName), dt, arriveTime, waitTime, acceptedTime, serveTime, (acceptedTime + serveTime));
}

