#ifndef MM1SYSTEM_H_INCLUDED
#define MM1SYSTEM_H_INCLUDED

#include "QSystem.h"
#include "ArriveProcess.h"

#include<string>
using namespace std;
class ArriveProcess;
class MM1System :public QSystem {
public:
    //MM1System();
    MM1System(string name, double miu, int serverName);


    MM1System(string name, ArriveProcess* arriveProcess, double miu);
    void init(string name2, ArriveProcess* arriveProcess2, double miu, int serverName);

};


#endif // MM1SYSTEM_H_INCLUDED
