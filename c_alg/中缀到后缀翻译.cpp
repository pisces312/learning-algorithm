#include<ctype.h>
#include<stdio.h>
#include<stdlib.h>
/**
将中缀表达式翻译成后缀表达式
**/
int lookahead;
void error() {
    printf("error\n");

    exit(1);
}
void match(int t) {
    if (lookahead==t) {
        lookahead=getchar();
    } else error();
}

void term() {
    if (isdigit(lookahead)) {
        putchar(lookahead);
        match(lookahead);
    } else error();
}
void expr() {
    term();
    while (true) {
        if (lookahead=='+') {
            match('+');
            term();
            putchar('+');
//            ungetc(lookahead,stdin);
//ungetc('+',stdin);
        } else if (lookahead=='-') {
            match('-');
            term();
            putchar('-');
//            ungetc(lookahead,stdin);
//  ungetc('-',stdin);
        } else break;

    }
}

//控制台输入9+5-4
int main() {
    //

//    putchar('9');
//printf("9+5-4\n");
//    printf("%s\n",getchar());
//    putchar('+');
//    putchar('5');
//    putchar('-');
//    putchar('4');
//    putchar('\0');
//    putchar('\n');
//
//    //
//char c;
//ungetc(c,stdin);
//    printf("%s\n",getchar());
//
    lookahead=getchar();
    expr();
    putchar('\n');
    return 0;
}
