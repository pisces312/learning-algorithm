//求一字符串中最大回文串的长度

//比如：
//"ABBA"      回文长度为: 4  回文子串为: "ABBA"
//"12abcba22" 回文长度为: 7  回文子串为: "2abcba2"


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
int FindMaxPalindrome(const char* s,int& beginIndex) {
    if(s==NULL||*s=='\0') {
        beginIndex=-1;//记录回文序列开始的下标
        return 0;
    }
    int     max = 1;
    beginIndex=0;
    int     i;
    for (const char* p = s; *p != '\0'; p++) {//遍历字符串
        //奇偶两种情况
        for (i = 1; (p - i) >= s && *(p + i) != '\0'; i++) {//向两侧查找
            if (*(p - i) != *(p + i))
                break;
        }

        if (max < (i * 2 - 1)) {
            max = (i * 2 - 1);
            beginIndex=p-i-s+1;
            printf("odd %d\n",beginIndex);
        }
///////////////////////////////////
//偶数情况
        for (i = 0; (p - i) >= s && *(p + i + 1) != '\0'; i++) {
            if (*(p - i) != *(p + i + 1))
                break;

        }
        if (max < (i * 2)) {
            max = (i * 2);
            beginIndex=p-i-s+1;
            printf("even %d\n",beginIndex);
        }
    }

    printf("max = %d\n", max);
    return max;
}
//void PrintRange(const char* a,int b,int e){
//    for(int i=b;i<=e;++i){
//        printf("%c",a[i]);
//    }
//    printf("\n");
//}
void PrintRange(const char* a,int b,int len){
    for(int i=b;i<b+len;++i){
        printf("%c",a[i]);
    }
    printf("\n");
}
int main(int argc, char **argv) {
    const char* a="ABBA";
    const char* b="12abcba22";
    int start;
    int len;
    len=FindMaxPalindrome(a,start);
//    printf("%d\n",start);
    PrintRange(a,start,len);
//    printf("%s\n",a+start);

    len=FindMaxPalindrome(b,start);
    PrintRange(b,start,len);
//    printf("%s\n",b+start);
    return 0;
}
