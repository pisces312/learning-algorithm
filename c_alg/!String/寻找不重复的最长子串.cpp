#include<stdio.h>

/*trend examination: find the longest substring's length with no duplicate char in a string*/

//All rights not reserved!
int max_len(char* str) {
    if(!str)return 0;
    char* records[256]= {0};
    char* fast = str;
    char* slow = str;
    int max=0;
    int tmp=0;

    while(*fast!='\0') {
        unsigned int index =(unsigned int) *fast;
        //there is a duplicate char
        if(records[index] && records[index]>=slow) {
            if(tmp > max)
                max = tmp;
            tmp-=records[index]-slow +1;
            slow=records[index]+1;
        }
        records[index]=fast;
        fast++;
        tmp++;
    }
    if(tmp>max)
        max = tmp;
    return max;
}

int main() {
    char* arr[]=        {"atom","ballsaint","wangmeng","zhanggongyuan","wansishuang","abcabc","abcdefgabcdefg","aaaaaaaaaa",NULL};
    int i;
    for(i=0; arr[i]; i++)
        printf("max of %s is %d\n",arr[i],max_len(arr[i]));

    return 0;
}
