

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
public class BoyerMoore {

    String pat;
    String text;
    int[] rpr;

    public BoyerMoore(String pat, String text) {
        this.pat = pat;
        this.text = text;

        init();
    }

    public boolean isInPat(char ch) {
        return pat.indexOf(ch) != -1;
    }

//    public int getLastEqualIndex(char ch) {
//        return pat.lastIndexOf(ch);
//    }

    /**
     * 最小移动1，最大移动模式串的长度！！
     * @param index
     * @return
     */
    public int shift(int index) {
//        return pat.length() - pat.lastIndexOf(ch) - 1;
        System.out.println(pat.length() - rpr[index]);
        return pat.length() - rpr[index];
    }

    /**
     * 这里生成辅助数组
     */
    public void init() {
        rpr = new int[pat.length()];
        rpr[0] = 0;//rpr.length-1;//0;
        //!!!!最后一位的处理！！
        rpr[rpr.length - 1] = rpr.length - 1;// 0;
        for (int i = pat.length() - 1; i >= 1; i--) {//子字符串取的长度
            for (int j = 1; j <= pat.length() - i; j++) {
                int backRight = pat.length() - 1;//始终在原字符串最后
                int frontRight = backRight - j;//错移j位
//                int t = frontRight;
                int c = 0;
                while (frontRight - i + 1 >= 0 && c < i) {
                    if (pat.charAt(backRight) != pat.charAt(frontRight)) {
                        break;
                    }
                    frontRight--;
                    backRight--;
                    c++;
                }
                if (c == i) {
//                    rpr[pat.length() - i-1] = t;
                    rpr[pat.length() - i - 1] = frontRight + 1;
                    break;
                }
            }
        }
//        System.out.println(Arrays.toString(rpr));
    }

    /**
     *原始，未改进
     * 正确
     * @return
     */
    public int match1() {
        //从后往前比较，k对应待比较的字符串的位置
        int k = pat.length() - 1;
        int n = text.length();
        int patIndex;
        while (k < n) {
            patIndex = pat.length() - 1;
            System.out.println("k=" + k);
//            System.out.println("patIndex="+patIndex);
            System.out.println();
            char ch = 0;
            while (patIndex >= 0 && (pat.charAt(patIndex) == (ch = text.charAt(k)))) {
                patIndex--;
                k--;
            }

            if (patIndex == -1) {
                System.out.println("match");
                return k+1;
            } else {
                //            char ch=pat.charAt(k);
                //不匹配则用下面的判断
                if (!isInPat(ch)) {
                    k += pat.length();
                } else {
                    k += shift(ch);
//                getLastEqualIndex(ch);
                }
            }
        }
        return -1;
    }

    /**
     *第二个版本
     * 正确
     * @return
     */
    public int match2() {
        //从后往前比较，k对应待比较的字符串的位置
        int k = pat.length() - 1;
        int n = text.length();
        int patIndex;
        while (k < n) {
            patIndex = pat.length() - 1;
            System.out.println("k=" + k);
            char ch = 0;
            while (patIndex >= 0 && (pat.charAt(patIndex) == (ch = text.charAt(k)))) {
                patIndex--;
                k--;
            }
            if (patIndex == -1) {
                System.out.println("match");
                return k+1;
            } else {
                k += (pat.length() - pat.lastIndexOf(ch) - 1);
            }
        }
        return -1;
    }

    /**
     *第三版，优化！
     * 正确
     * @return
     */
    public int match3() {
        //从后往前比较，k对应待比较的字符串的位置
        int k = pat.length() - 1;
        int n = text.length();
        int patIndex;
        while (k < n) {
            patIndex = pat.length() - 1;
            System.out.println("k=" + k);
            char ch = 0;
            while (patIndex >= 0 && (pat.charAt(patIndex) == (ch = text.charAt(k)))) {
                patIndex--;
                k--;
            }
            if (patIndex == -1) {
                System.out.println("match");
                return k+1;
            } else {
                System.out.println("patIndex=" + patIndex);
                k += Math.max(shift(patIndex), pat.length() - pat.lastIndexOf(ch) - 1);
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        BoyerMoore bm = new BoyerMoore("AT-THAT", "WHICH-FINALLY-HALTS,--AT-THAT-POINT");
//        BoyerMoore bm = new BoyerMoore("AT-THAT", "WHICH-FINALLY-HALTS,--AT-THAT-POINT");
//        System.out.println(bm.isInPat('A'));
        System.out.println(bm.match3());
//        bm.match1();
//        bm.match2();
//        bm.init();

    }
}
