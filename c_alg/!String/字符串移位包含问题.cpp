//3.1 字符串移位包含的问题：
//1，题意：给定s1和s2，判定s2是否能够被s1做循环移位得到的字符串包含。
//
//2，解决：

#include<iostream>
#include<cstring>
using namespace std;
//方法一： 一般的逐次移位再判断
//会修改原数组
bool isMatch(char* src, char* des) {
    int len = strlen(src);
    for (int i = 0; i < len; i++) {
        char tempchar = src[0];
        for (int j = 1; j < len; j++)
            src[j - 1] = src[j];
        src[len - 1] = tempchar;
        if (strstr(src, des) != NULL )
            return true;
    }
    return false;
}
//
//方法二： 空间换时间
//s2在s1的循环移位中，那么s2一定在s1+s1上。
//这时调用一次strstr即可。
bool isMatch2(const char* src, const char* des) {
//    cout<<src<<endl;
    int len = strlen(src);
    char* dsrc=new char[(len<<1)+1];
    memcpy(dsrc,src,len);
    memcpy(dsrc+len,src,len+1);
//    cout<<dsrc<<endl;

    if (strstr(dsrc, des) != NULL ) {
        delete[] dsrc;
        return true;
    }
    delete[] dsrc;
    return false;
}
int main() {
    char src[] = "AABBCD";
    char des[] = "CDAA";

    cout << isMatch2(src, des)<<endl;
    cout << isMatch(src, des)<<endl;
    return 0;
}

