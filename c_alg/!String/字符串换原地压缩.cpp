/*
* Copyright (c) 2010,东南大学数据库课题组
* All rights reserved.
*
* 文件名称：
* 摘    要：简要描述本文件的内容
*
* 当前版本：1.0
* 作    者：倪力
* 完成日期：2010年
*/
#include<stdio.h>
#include<stdlib.h>
//"eeeeeaaaff"
//"e5a3f2"
void CompressStrInPlace(char *str){
    char* p=str;
    char* pre=p;
    char* s=p;
    int rep=0;
    char buffer[16];
    char* sNum=NULL;
    while(*p||rep){//加rep判断，避免漏掉最后一个
        if(*p==*pre){
            ++rep;
            pre=p++;
        }else{
            *s++=*pre;
            //最后一位为进制
            sNum=itoa(rep,buffer,10);
            while(*sNum!='\0'){//避免复制'\0'
                *s++=*sNum++;
            }
            rep=0;
            pre=p;
        }
    }
    *s='\0';

}

int main(){
    char a[]="eeeeeaaaff";
    printf("%s\n",a);
    CompressStrInPlace(a);
    printf("%s\n",a);


    char b[]="sssssssssssssssssssssssssssssssslllllllllllllllllluyutuuuuuuuuuuuuuu";
    printf("%s\n",b);
    CompressStrInPlace(b);
    printf("%s\n",b);
    return 0;
}

