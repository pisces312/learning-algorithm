#include<stdio.h>
#include<math.h>
//防止溢出的乘法
unsigned long long multi(unsigned long long a,unsigned long long b,unsigned long long c) {
//    return (a*b)%c;
    if(a==0||b==0) return 0;
    unsigned long long  sum=0,add=a%c;
    while(b) {
        if(b&1) {
            if((sum+=add)>=c) sum=sum-c;
        }
        if((add<<=1)>=c) add-=c;
        b>>=1;
    }
    return sum;
}
//取最后c位
//logn次乘法
//最快O(logn)
unsigned long long mod(unsigned long long a,unsigned long long b,unsigned long long c) {
    unsigned long long  y=1;
    c=pow(10,c);
    while(b) {
        if(b&1) {//奇数情况
            y=multi(y,a,c);
        }
        a=multi(a,a,c);
        b>>=1;
    }
    return y;
}
int main() {
    unsigned long long int a=6,b=12,c=3;
//    for(; scanf("%llu%llu%llu",&a,&b,&c)!=EOF;) {
    printf("%llu\n",mod(a,b,c));
//    printf("%llu\n",mod(a,b,c)%c);
//    }
    return 0;
}
