#include<iostream.h>
#include<math.h>
#include<iomanip.h>
void main()
{
	int n=1;
	double pi=0,term=1,sign=1;
	while(fabs(term)>=1e-8)
	{
		term=sign*1.0/n;
		sign=-sign;
		pi=pi+term;
		n+=2;
	}
	pi=pi*4;
	cout<<setprecision(16)<<pi<<endl;
}