#include<stdio.h>
void Reverse(char* a,int b,int e){
    if(b>=e){
        return;
    }
    int t=a[b];
    a[b]=a[e];
    a[e]=t;
    Reverse(a,b+1,e-1);
}
int main()
{
    //int a[]={1,2,3,4,5,6};
    char a[]="123456";
    Reverse(a,0,5);
    printf("%s\n",a);
     return 0;
}
