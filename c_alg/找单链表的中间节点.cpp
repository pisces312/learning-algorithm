//一个单链表，不知道长度，写一个函数快速找到中间节点的位置

/* 假定这个链表只有奇数个节点 */
List *list_middle(List *l)
{
    List *fast;
    List *slow;

    fast = slow = l;

    while (fast != NULL) {
        if (fast->next)
            fast = fast->next->next;
        else
            return slow;
        slow = slow->next;
    }
    return slow;
}
