import java.util.Enumeration;
import java.util.Vector;

/*
 * OSPF.java
 * 
 * Created on 2007-6-3, 13:39:34
 * 
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author pisces312
 */
public class OSPF {
    final static int MAX=Integer.MAX_VALUE;
    Vector<Integer> tree=new Vector();
    Vector<Integer> others=new Vector();
    int[][] C={
        {0,2,1,MAX,MAX},
        {2,0,2,3,MAX},
        {1,2,0,9,MAX},
        {MAX,3,9,0,1},
        {MAX,MAX,MAX,1,0}
    };
    
    //路由由正整数唯一编号
    //到自身为0,不能改变!!
    int[][] S={
        {0,2,3,-1,-1},
        {1,0,3,4,-1},
        {1,2,0,4,-1},
        {-1,2,3,0,5},
        {-1,-1,-1,4,0}
        
    };
    int[][]D;

    /** Creates a new instance of OSPF */
    public OSPF() {
    }
    void SPF(){
        if(C.length<1)
            return;
        D=new int[C.length][];
        tree.addElement(0);
        int i,near;
        for(i=1;i<C.length;i++){
            others.addElement(i);
        }
        D[0]=new int[C[0].length];
        for(i=0;i<D[0].length;i++){
            D[0][i]=C[0][i];
        }        
        Vector<Integer> temp;
        do{
            temp=new Vector(tree);
            for(i=0;i<temp.size();i++){
                near=findNearNode(temp.get(i));
                tree.addElement(near);
                others.removeElement(near);                
            }
        }while(others.size()>0);
            
        
        
    }
    int findNearNode(int i){
        int min=Integer.MAX_VALUE,near=-1;
        for(Enumeration e=others.elements();e.hasMoreElements();){
            near=(Integer)e.nextElement();
            if(C[i][near]<min){
                min=C[i][near];
                
            }           
        }
        return near;
    }
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }

}
