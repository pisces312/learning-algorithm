#include <iostream>
//#include <list>
using namespace std;
//void JosephusRing(int n,int m){
//    list<int> ring;
//    for(int i=1;i<=n;i++){
//        ring.push_back(i);
//    }
//    while(!ring.empty()){
//        int size=ring.size();
//        list<int>::iterator itr=ring.begin();
//        int c=0;
//        int len=m%size;
//        while(itr!=ring.end()){
//            c++;
//            if(c==len){
//                break;
//            }
//            itr++;
//        }
//        if(itr!=ring.end()){
//            cout<<*itr<<endl;
//            ring.erase(itr);
////            ring.remove(itr);
//        }
//
//    }
//}
typedef struct LNode{
    int data;
    struct LNode *link;
}LNode,*LinkedList;
//从1开始编号
void JosephusRing(int n,int k,int m){
    LinkedList p,r,list,curr;
    p=(LinkedList)malloc(sizeof(LNode));
    p->data=1;
    p->link=p;
    curr=p;
    for(int i=1;i<n;i++){
        LinkedList t=(LinkedList)malloc(sizeof(LNode));
        t->data=i+1;
        t->link=curr->link;
        curr->link=t;
        curr=t;
    }
    r=curr;
    k--;
    while(k--){
        r=p;
        p=p->link;
    }
    while(n--){
        for(int s=m-1;s--;r=p,p=p->link);
        r->link=p->link;
        cout<<p->data<<"->";
        free(p);
        p=r->link;
    }
}
int main()
{
    //从第一个人开始 1,2,3,4,5
    //3,1,5,2,4
    JosephusRing(5,1,3);
//    cout << "Hello world!" << endl;
    return 0;
}
