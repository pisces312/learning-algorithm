/*
ID:zhaorui3
PROG:concom
LANG:C++
*/
# include <iostream>
# include <fstream>
# include <cstdlib>
using namespace std;

int stock[101][101] = {{0}};
bool boCon[101][101] = {false};

int main()
{

	ifstream fin("concom.in");
	ofstream fout("concom.out");
	int coms;
	fin >> coms;
	int tempA , tempB , st;
	for (int i = 1; i <= coms ; ++i)
	{
		fin >> tempA >> tempB >> st;
		stock[tempA][tempB] = st;

	}
	fin.close();


	///////////////////////////////


	for (int i = 1; i <= 100; ++i)
	{
		for(int j = 1 ; j <= 100 ; j++ )
		{
		    boCon[i][j] = false;
			if(stock[i][j] > 50)
			{
				boCon[i][j] = true;
			}
		}
	}

	bool flag = true;
	while(flag)
	{
		flag = false;
		for (int i = 1; i <= 100; ++i)
		{
			for (int j = 1; j <= 100; ++j)
			{
			    int temp = 0;
                for(int l = 1 ; l <= 100 ; l++ )
                {
                    if(boCon[i][l])
                        temp += stock[l][j];
                }
                if(stock[i][j] + temp > 50 && !boCon[i][j])
                {
                    boCon[i][j] = true;
                    flag = true;
                }
            }
		}
	}

	for(int i = 1 ; i <= 100 ; i++ )
    {
        for(int j = 1 ; j <= 100 ; j++ )
        {
            if(j == i)
                continue;
            if(boCon[i][j])
                fout << i << ' ' << j << endl;
        }

    }

    fin.close();
    fout.close();
    return 0;
}
