#ifndef BLACKWHITETREE_H_INCLUDED
#define BLACKWHITETREE_H_INCLUDED
#include<list>
#include<iostream>
using namespace std;
/**
done
黑白树
问题背景
在图论中，包含n个结点（结点编号为1~n）、n-1条边的无向连通图被称为树。在树中，任意一对结点间的简单路径总是惟一的。你拥有一棵白色的树——所有节点都是白色的。接下来，你需要处理c条指令：
1.修改指令(0v)：改变一个给定结点的颜色（白变黑，黑变白）；
2.查询指令(1v)：询问从结点1到一个给定结点所经过的第一个黑色结点编号（假设沿着简单路径走）。
注意，在查询指令中，必须从结点1而不是结点v出发。如果结点1本身就是黑色，查询指令应该返回1。
输入说明
第一行包含两个正整数n,c，即结点数和指令条数。以下n-1行，每行两个正整数(ui,vi)(1<=ui<=n)，表示结点ui到vi之间有一条无向边。以下c行，每行两个整数(c,v)。当c=0时表示修改指令，其中v代表被修改的结点编号；c=1时表示查询指令。你的程序需要输出结点1到结点v之间路径的第一个黑色结点编号。在第一条指令执行前，所有结点都是白色的。
输出格式
对于每个查询操作(c=1的操作)，输出一行，包含一个整数，即第一个黑色结点的编号。如果不存在黑色结点，输出-1。
样例输入
9 8 //n c
1 2
1 3
2 4
2 9
5 9
7 9
8 9
6 8
1 3 //从这开始时指令，即1X ,X表示操作的节点
0 8
1 6
1 7
0 2
1 9
0 2
1 9
样例输出
-1
8
-1
2
-1
评分说明
共有30个数据，分为3组，按组计分。这三组的满分分别为20,30,50分。
第一组:n=5000,m=4000000
第二组:n=100000,m=3000000
第三组:n=1000000,m=1000000
每组数据中只要有一个数据运行超过5秒或者答案错，该组计0分。
否则，该组数据的得分取决于其中运行时间最长的数据的运行时间：运行时间越短，得分越高。
**/

//!多叉树
class BWTreeNode {

    public:
    static int count;
    int id;
    BWTreeNode() {
//        child=NULL;

        id=count;
        count++;
        isWhite=true;
    }
    list<BWTreeNode*> neighours;
//    BWTreeNode* child;
    bool isWhite;
    //两者互为邻居,其中之一添加即可
    void addBothNeighour(BWTreeNode* neighour) {
        neighours.push_back(neighour);
        neighour->neighours.push_back(this);

    }
    void addBothNeighour(BWTreeNode& neighour) {
        neighours.push_back(&neighour);
        neighour.neighours.push_back(this);

    }
//    void addNeighour(BWTreeNode* neighour) {
//        neighours.push_back(neighour);
//
//    }
};
int BWTreeNode::count=0;
//该文件中使用的全局变量?
static bool* flags;
static int n;
static BWTreeNode* nodes;
//void addBothNeighour(BWTreeNode* n1,BWTreeNode* n2) {
//    n1->
//
//}
void printNode(BWTreeNode* node) {
    if (node==NULL) {
        return;
    }
    cout<<node->id<<" "<<node->isWhite<<endl;
    flags[node->id]=true;
    for (list<BWTreeNode*>::iterator itr=node->neighours.begin();itr!=node->neighours.end();itr++) {
        if (!flags[(*itr)->id])
            printNode(*itr);
    }

}
void printTree(BWTreeNode& root) {
    printNode(&root);

}
//相互转换，白变黑，黑变白
void changeInstruction(BWTreeNode* node) {
    node->isWhite=!node->isWhite;
}
void changeInstruction(BWTreeNode& node) {
    changeInstruction(&node);
}
//BWTreeNode getNextNerghour
//node为目标节点
int searchInstruction(BWTreeNode* node) {
    if (!nodes[1].isWhite) {
        return 1;
    }
//    cout<<"searching...\n";
    memset(flags,0,sizeof(bool)*(n+1));
    list<BWTreeNode*> stack;
    stack.push_back(&nodes[1]);
    flags[nodes[1].id]=true;
    while (!stack.empty()) {
        BWTreeNode* cur=stack.back();
//        cout<<"visit :"<<cur->id<<endl;
//        stack.pop_front();
//        if (!cur->isWhite) {
//            return cur->id;
//        }
        if (cur==node) {
            cout<<"找到到指定节点的路径！\n";
            break;
        }
        list<BWTreeNode*>::iterator itr=cur->neighours.begin();
        for (;itr!=cur->neighours.end();itr++) {
//            cout<<"neigh:"<<(*itr)->id<<endl;
            if (!flags[(*itr)->id]) {
//                cout<<"push :"<<(*itr)->id<<endl;
                stack.push_back(*itr);
                flags[(*itr)->id]=true;
                break;
            }
        }
        //邻居全部访问到,回溯
        if (itr==cur->neighours.end()) {
            stack.pop_back();
        }

    }



//        flags[node->id]=true;
    for (list<BWTreeNode*>::iterator itr=stack.begin();itr!=stack.end();itr++) {
        if (!(*itr)->isWhite){
            return (*itr)->id;
        }

        }
    return -1;

}
int searchInstruction(BWTreeNode& node) {
    return searchInstruction(&node);
}
void testBWTree() {

    n=9;
    int c=8;
    flags=new bool[n+1];
    memset(flags,0,sizeof(bool)*(n+1));
//    cout<<n<<endl;
    //多一个节点
    nodes=new BWTreeNode[n+1];
//    cout<<nodes[1].isWhite<<endl;
    //
    /**
    1 2
    1 3
    2 4
    2 9
    5 9
    7 9
    8 9
    6 8
    **/
    nodes[1].addBothNeighour(nodes[2]);
    nodes[1].addBothNeighour(nodes[3]);
    nodes[2].addBothNeighour(nodes[4]);
    nodes[2].addBothNeighour(nodes[9]);
    nodes[5].addBothNeighour(nodes[9]);
    nodes[7].addBothNeighour(nodes[9]);
    nodes[8].addBothNeighour(nodes[9]);
    nodes[6].addBothNeighour(nodes[8]);
    printTree(nodes[1]);
    /**
    1 3
    0 8
    1 6
    1 7
    0 2
    1 9
    0 2
    1 9**/
    int id=-1;
    id=searchInstruction(nodes[3]);
    cout<<id<<endl;
    changeInstruction(nodes[8]);
    id=searchInstruction(nodes[6]);
    cout<<id<<endl;
    id=searchInstruction(nodes[7]);
    cout<<id<<endl;
    changeInstruction(nodes[2]);
    id=searchInstruction(nodes[9]);
    cout<<id<<endl;
    changeInstruction(nodes[2]);
    id=searchInstruction(nodes[9]);
    cout<<id<<endl;






}
#endif // BLACKWHITETREE_H_INCLUDED
