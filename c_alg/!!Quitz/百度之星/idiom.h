#ifndef IDIOM_H_INCLUDED
#define IDIOM_H_INCLUDED
//#include "printgbk.h"
#include<iostream>
using namespace std;
/**
1.成语纠错（15分）
问题背景
成语是中华民族的文化瑰宝，作为历史的缩影、智慧的结晶、汉语言的精华，闪烁着睿智的光芒。
你的任务是给一个错误的四字成语进行纠错，找到它的正确写法。具体来说，你只允许修改四个汉字中的其中一个，使得修改后的成语在给定的成语列表中出现。原先的错误成语保证不在成语列表中出现。
有时，这样的“纠错”结果并不惟一。例如“一糯千金”可以改为“一字千金”也可以改成“一诺千金”。但由于“糯”和“诺”是同音字，“一糯千金”实为“一诺千金”的可能性比较大。
因此，我们还将提供一个汉字分类表，要求修改前后的两个字必须属于同一个分类。
在这样的限制下，我们保证成语纠错的结果惟一。
注意
1、汉字均采用GBK编码(参见FAQ)
2、每个汉字分类至少包含两个汉字，同一个汉字可能出现在多个类别中。
3、成语列表中的成语都是真实存在的四字成语。成语列表和待纠错成语中的所有汉字均在汉字分类表中的至少一个分类中出现。
输入格式
输入第一行包含两个整数n,m(1<=n<=200,1<=m<=20000)。n表示汉字类别的个数，m表示成语的个数。
??中间用空格隔开？
以下n行每行用一个无空白分隔符（空格、TAB）的汉字串表示一个分类中的所有汉字。
注意，该汉字串最多可能包含200个汉字。
以下m行为成语列表，每行一个成语，恰好四个汉字。
最后一行为待纠错的成语，恰好四个汉字，且不在成语列表中出现。
输出格式
仅一行，为一个四字成语。在“修改必须在同一分类中进行”的限制下，输入数据保证纠错结果惟一。
样例输入
7 3
糯诺挪喏懦
字自子紫籽
前钱千牵浅
进近今仅紧金斤尽劲
完万
水睡税
山闪衫善扇杉
一诺千金
一字千金
万水千山
一糯千金


样例输出
一诺千金
**/
static char** table;
static int n=0,m=0;
//!只有一个错
//返回-1表示有多个错或相同
//返回错误的汉字的第一个下标（两个）
int getWrongPos(char * idiom,char* toCompare) {
    int pos=-1;
    for (int i=0;i<9;i+=2) {
        if (idiom[i]!=toCompare[i]||idiom[i+1]!=toCompare[i+1]) {
            if (pos>=0)//多余一个错误的情况，直接返回
                return -1;
            pos=i;
        }
    }
    return pos;
}
//传入汉字的指针和所在位置
int findTableRow(char* ch,int startPos) {
    for (int i=0;i<n;i++) {
        char* str=table[i];
        for (int j=0;str[j]!='\0';j+=2) {
            if (ch[startPos]==str[j]&&ch[startPos+1]==str[j+1]) {
                return i;

            }

        }
    }
    return -1;
}
/**
!从控制台读入字符串，注意回车个数！！
!使用cout可以正确输出汉字
**/
void testIdiom() {

    cin>>n>>m;

    cout<<n<<m<<endl;
    //!把回车取下来
    cin.get();
    //!该汉字串最多可能包含200个汉字。
    table=new char*[n];
    for (int i=0;i<n;i++) {
        table[i]=new char[200];
        cin.getline(table[i],200);
        cout<<table[i]<<endl;
//        printChineseCharacter(table[i]);
//        cout<<i<<endl;
    }

//    for (int i=0;i<n;i++) {
//             printChineseCharacter(table[i]);
//    }
//保存正确的成语
    char** idioms=new char*[m];
    int size=9;
    for (int i=0;i<m;i++) {

        idioms[i]=new char[size];
        cin.getline(idioms[i],size);
        cout<<idioms[i]<<endl;
//        printChineseCharacter(idioms[i]);
        cout<<endl;
    }
    char* wrong=new char[size];
    cin.getline(wrong,size);
    cout<<"错误的成语："<<wrong<<endl;
//    int* pos=new int[m];
    int pos;
//    int min=5;
//    int index=-1;
    cout<<"result:\n";
    //找到差异最小的??
    for (int i=0;i<m;i++) {
        pos=getWrongPos(wrong,idioms[i]);
        if (pos>=0) {
//            index=i;
            if (findTableRow(wrong,pos)==findTableRow(idioms[i],pos)) {
                cout<<idioms[i]<<endl;
            }
        }
    }
}

#endif // IDIOM_H_INCLUDED
