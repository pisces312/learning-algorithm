#ifndef FAIRNUMBER_H_INCLUDED
#define FAIRNUMBER_H_INCLUDED
#include<set>
#include<iostream>
using namespace std;
/**
done
公平数（35分）
问题背景
如果一个整数的十六进制表示（不含前导0）中，前一半数字之和等于后一半数字之和，我们称它为公平数。
注意，如果该数的十六进制表示中包含奇数个数字，则正中间的数字既不属于前一半，又不属于后一半。
例如在十六进制下1+D=7+7，因此1DE77是公平数。数字E并不参与计算。
再例如，所有单个数字的十六进制数（即0~F）均为公平数，但F0不是（不能把F0补充前导0写成0F0，进而认为它是公平数）。
给出十六进制数X,Y,K和十六进制数字集合S，求区间[X,Y]之内，有多少个公平数满足：
? 十六进制表达式（不包含前导0）中每个数字均在集合S中
? 并且为K的倍数
输入格式
输入第一行为数字集S，包含0~9以及大写字母A~F。
每个数字或字母最多出现一次。
第二行包含3个十六进制正整数K,X,Y，均不超过10个数字。
输出格式
仅一行，包含一个整数，即满足条件的公平数个数。
样例输入

1 2 4 C
5 100 FFF

样例输出
4
样例解释
只有四个数满足条件：212，424，4C4，C1C。

!!??正数？
**/
//!第一种方法
void inputFromCon(set<int> &s) {
    char c;
    int num;
    while (true) {
        c=cin.get();
        if (c=='\n') {
            break;
        }
        cin.putback(c);
        //置基数为十六进制，使其可以输入十六进制
        cin>>hex>>num;
        s.insert(num);
    }
}
//返回第一个非零的位上的数
//数组从前往后先存低位再存高位
int devideHexNum(int num,int* digits,const int& size) {
    if (num==0)
        return 0;
    int i=0;
    for (i=0;i<size&&num;i++,num>>=4) {
        digits[i]=num&0xf;
    }
//    if(i>0){
//        return i-1;
//    }
    return i-1;
}
void testFairNumber() {
    //该子集是单个数字
    set<int> s;
//    inputFromCon(s);
//人造数据
    s.insert(1);
    s.insert(2);
    s.insert(4);
    s.insert(0xc);

//
    cout<<"set:\n";
    set<int>::iterator   it;
    for (it=s.begin();it!=s.end();++it)
        cout<<*it<<" ";
    cout<<endl;
    //整型int最多可以表示几位十六进制位
    int maxDigits=sizeof(int)*8/4;
    int* hexDigit=new int[maxDigits];
//    cout<<sizeof(int)*8/4<<endl;
    //!x,y,k均为十六进制
    int x=0x1DE77;
//    int y=0x100;//0xfff;
    int y=0x1DE77;
    int k=5;
    for (int i=x;i<=y;i++) {
        int j=devideHexNum(i,hexDigit,maxDigits);
        int mid=j/2;
        if (j%2!=0) {//偶数位
            mid++;
        }
        int lowSum=0,highSum=0;
        for (int k=0;k<mid;k++) {
            lowSum+=hexDigit[k];
            highSum+=hexDigit[j-k];
        }
        if (lowSum==highSum)
            cout<<hex<<i<<" "<<lowSum<<" "<<highSum<<endl;
//        for (;j>=0;j--) {
//            cout<<hex<<hexDigit[j];
//        }
//        cout<<endl;
//        for (int j=devideHexNum(i,hexDigit,maxDigits);j>=0;j--) {
//            cout<<hex<<hexDigit[j];
//        }
//        cout<<endl;

    }


}


//!第二种方法
//
void inputFromConEx(bool *s) {
    char c;
    int num;
    while (true) {
        c=cin.get();
        if (c=='\n') {
            break;
        }
        cin.putback(c);
        //置基数为十六进制，使其可以输入十六进制
        cin>>hex>>num;
        s[num]=true;
    }
}
void inputFromConEx2(bool *s) {
    char c;
    int num;
    while (true) {
        c=cin.peek();
        if (c=='\n') {
            break;
        }
        //置基数为十六进制，使其可以输入十六进制
        cin>>hex>>num;
        s[num]=true;
    }
}
//数组从前往后先存低位再存高位
//返回第一个非零的位上的数
//如果使用的数字不包括在数字子集中则返回-1
int devideHexNumEx(int num,int* digits,const int& size,bool* s,const int& k ) {
    if (num==0)
        return 0;
    if (num%k!=0)
        return -1;
    int i=0;
    for (i=0;i<size&&num;i++,num>>=4) {
        digits[i]=num&0xf;
        if (!s[digits[i]]) {//只要有一个数字不在数字子集中就输出错误
            return -1;
        }
    }
    return i-1;
}
void testFairNumberEx() {
    //该子集是单个数字的集合
//!十六进制，只有十六种字符，采用数组表示集合，用下标表示一个字符，快速设置

    bool* s=new bool[16];
    int maxDigits=sizeof(int)*8/4;
    memset(s,0,16*sizeof(bool));
    //从控制台输入
//    inputFromConEx2(s);
    //人造数据
    s[1]=true;
    s[2]=true;
    s[4]=true;
    s[0xc]=true;
//使用所有数字
//    memset(s,1,16*sizeof(bool));
//
//    //整型int最多可以表示几位十六进制位
    int* hexDigit=new int[maxDigits];
//    cout<<sizeof(int)*8/4<<endl;
    //!x,y,k均为十六进制
    int x=0x100;//0x1DE77;
    int y=0xfff;
//    int x=0xdd;//0x1DE77;
//    int y=0xdd;//0x1DE77;
    int k=5;//取1即忽略
    //从控制台读取x,y,z
//    cin>>hex>>k>>hex>>x>>hex>>y;
    //
    int c=0;
    for (int i=x;i<=y;i++) {
        int j=devideHexNumEx(i,hexDigit,maxDigits,s,k);
        if (j==-1)
            continue;
        int mid=j/2;
        if (j%2!=0) {//偶数位
            mid++;
        }
        int lowSum=0,highSum=0;
        for (int k=0;k<mid;k++) {
            lowSum+=hexDigit[k];
            highSum+=hexDigit[j-k];
        }
        if (lowSum==highSum){
            c++;
            cout<<hex<<i<<endl;//<<" "<<lowSum<<" "<<highSum<<endl;
        }
    }
    cout<<"个数:"<<c<<endl;


}
#endif // FAIRNUMBER_H_INCLUDED
