#include<iostream>
#include<cmath>
#include<time>
#include<clock>
using namespace std;

#define NULL 0
#define LEN sizeof(struct Stack) /*定义结构体的长度*/
#define MAX_X 50 /*定义迷宫的最大高度*/
#define MAX_Y 50 /*定义迷宫的最大宽度*/

struct Stack{
	int x; /*存放迷宫中的X坐标*/
	int y; /*存放迷宫中的Y坐标*/
	struct Stack *next;
	struct Stack *pre; /*定义向上的指针，为了最后正序输出堆栈*/
};

struct Stack *top; /*定义堆栈的头指针*/
struct Stack *base; /*定义堆栈的尾指针*/
struct Stack * initstack()
{ /*初始化堆栈，并在栈底放入0,0*/
	base = top = ( struct Stack *) malloc ( LEN );
	top->x = 0;
	top->y = 0;
	top->next = NULL;
	top->pre = NULL;
	return top;
}

struct Stack * push( int x , int y )
{ /*往堆栈中压入数据*/
	struct Stack *p;
	p = top;
	top = ( struct Stack *) malloc ( LEN );
	top->next = p;
	p->pre = top;
	top->x = x;
	top->y = y;
	return top;
};





int * pop(int array[2])
{ /*从堆栈中取出数据，并释放栈顶的空间*/
	struct Stack *p;
	if(top == base) return 0;
	else
	{
		array[0] = top->x;
		array[1] = top->y;
		p = top;
		top = top->next;
		top->pre = NULL;
		free(p);
	}
	return &array[0];
}

int * downtotop(int array[2])
{ /*为了最后正序输出堆栈定义的函数，从栈底向栈顶读数据，并释放栈底空间*/
	struct Stack *p;
	if(top == base) return 0;
	else
	{
		p = base;
		base = base->pre;
		base->next = NULL;
		array[0] = base->x;
		array[1] = base->y;
		free(p);
	}
	return &array[0];
}

void searchpath(int matrix[MAX_X][MAX_Y],int x_max,int y_max , int array_pre[2])
{ /*搜索迷宫中的路径*/
	int array[2];
	array[0] = top->x;
	array[1] = top->y;

//到达终点,则跳出
	if( top->x == x_max-1 && top->y == y_max-1 )
	{
		return;
	}

//向左走，递归
	if( ( top->y - 1 >=0 && matrix[top->x][top->y - 1] == 0) && ( top->x != array_pre[0] || top->y - 1 != array_pre[1] ) && !( top->x == x_max-1 && top->y == y_max-1 ) )
	{
		top = push( top->x , top->y - 1 );
		searchpath(matrix , x_max , y_max , array);
	}
//向下走，递归
	if( ( top->x + 1 < x_max && matrix[top->x + 1][top->y] == 0) && ( top->x + 1 != array_pre[0] || top->y != array_pre[1] ) && !( top->x == x_max-1 && top->y == y_max-1 ) )
	{
		top = push( top->x + 1 , top->y );
		searchpath(matrix , x_max , y_max , array);
	}
//向右走，递归
	if( ( top->y + 1 < y_max && matrix[top->x][top->y + 1] == 0) && ( top->x != array_pre[0] || top->y + 1 != array_pre[1] ) && !( top->x == x_max-1 && top->y == y_max-1 ) )
	{
		top = push( top->x , top->y + 1 );
		searchpath(matrix , x_max , y_max , array);
	}
//向上走，递归
	if( ( top->x - 1 >=0 && matrix[top->x - 1][top->y] == 0) && ( top->x - 1 != array_pre[0] || top->y != array_pre[1] ) && !( top->x == x_max-1 && top->y == y_max-1 ) )
	{
		top = push( top->x - 1 , top-> y );
		searchpath(matrix , x_max , y_max , array);
	}

//如果四个方向都不能走，则退回上一步，取出栈顶的值
	if( !(top->x == x_max-1 && top->y == y_max-1) ) pop( array );

	return;
}


void main()
{
	int array[2]; /*定义一个数组存放坐标，初始化的值没有意义*/
	int matrix[MAX_X][MAX_Y]; /*定义迷宫矩阵*/
	int * p; /*定义辅助的指针*/
	int i,j,x_max,y_max; /*定义其他辅助的变量*/

	do
	{ /*要求用户输入矩阵大小，如果小于等于1则再次要求用户输入*/
		printf("Please input the size of the maze (x,y)...( x>1 && y>1 ): \n");
		scanf("%d,%d",&x_max,&y_max);
	}while(x_max<=1 && y_max<=1);

	srand((unsigned) time(&i)); /*生成随机矩阵，经过测试，大于4以上的随机矩阵很难走出迷宫*/
	for(i=0;i<MAX_X;i++)
		for(j=0;j<MAX_Y;j++)
			matrix[j] = (rand()%2);
	matrix[0][0] = 0; /*定义起点为0*/
	matrix[x_max-1][y_max-1] = 0; /*定义终点为0*/
	printf("The randon maze is : \n\n");
	for(i=0;i<MAX_X;i++)
		for(j=0;j<MAX_Y;j++)
			printf("%2d ",matrix[j]);
	}
	printf("\n");
	}
	printf("\n\n");

	top = initstack(); /*初始话堆栈*/
	top = push(0,0); /*压入起点的坐标*/

	searchpath( matrix , x_max , y_max , array ); /*寻找路径走出迷宫*/

	if(top->x != x_max -1 || top->y != y_max -1)
	{ /*如果top指针没有指到终点，则是没有路出迷宫*/
		printf("No way to the terminal\n\n");
	}
	else
	{
		printf("The way to the terminal is :\n"); /*如果有路出迷宫则正序输出堆栈中的坐标*/
		while( base!=top )
		{
			p = downtotop(array);
			printf(" (%d,%d) ",p[0],p[1]);
			if( base!=top ) printf("-->");
		}
	}
	printf("\n");

	free(base); /*释放堆栈空间*/

} 