/*
//    　　当2b＞a+y时，使用模式一将Z和Y移动过桥；
//　　　　当2b＜a+y时，使用模式二将Z和Y移动过桥；
//　　　　当2b＝a+y时，使用模式一将Z和Y移动过桥。这样就使问题转变为N-2个旅行者的情形，从而递归解决之。
*/
#include<stdio.h>
#include<stdlib.h>
inline int compareInt(const void* x,const void* y) {
    return *(int*)x-*(int*)y;
}
void printArrayRange(int* keys,int b,int e,int limits) {
    if(limits>0) {
        e=limits+b-1;
    }
    for(int i=b; i<=e; ++i) {
        printf("%d ",keys[i]);
    }
    printf("\n");
}
void printArray(int* keys,int n,int limits) {
    printArrayRange(keys,0,n-1,limits);
}
////////////////////////////////////////////
//非递归
int GoOverBridge(int* t,int n) {
    qsort(t,n,sizeof(int),compareInt);
    printArray(t,n,0);
    if(n<=0) {
        return 0;
    }
    if(n<=2) {
        return t[n-1];
    }
    if(n==3) {
        return t[1]+t[0]+t[2];
    }
    int a=t[0],b=t[1],y,z,sum=0,c=n;
    while(c>3) {
        y=t[c-2],z=t[c-1];
        if((b<<1)>=a+y) {
            sum+=(z+a+y+a);
        } else {
            sum+=(b+a+z+b);
        }
        c-=2;
    }
    if(c==2) {
        return sum+t[1];
    }
    return sum+t[1]+t[0]+t[2];
}
//
//递归版

int GoOverBridgeCore(int* t,int n) {
    if(n<=0) {
        return 0;
    }
    if(n<=2) {
        return t[n-1];
    }
    if(n==3) {
        return t[1]+t[0]+t[2];
    }
    if((t[1]<<1)>=t[0]+t[n-2]) {
        return (t[n-1]+t[0]+t[n-2]+t[0])+GoOverBridgeCore(t,n-2);
    }
    return (t[1]+t[0]+t[n-1]+t[1])+GoOverBridgeCore(t,n-2);
}
int GoOverBridgeDriver(int* t,int n) {
    qsort(t,n,sizeof(int),compareInt);
    printArray(t,n,0);
    return GoOverBridgeCore(t,n);
}
////////////////////////////////////////////////
//打印过桥过程
int GoOverBridgeShowProcess(int* t,int n) {
    qsort(t,n,sizeof(int),compareInt);
    printArray(t,n,0);
    if(n<=0) {
        return 0;
    }
    if(n==1) {
        printf("t[  0]       -> %3d\t%3d\n",t[0],t[0]);
        return t[n-1];
    }
    if(n==2) {
        printf("t[  0],t[  1]-> %3d\t%3d\n",t[1],t[1]);
        return t[n-1];
    }
    int sum=0;
    if(n==3) {
        sum+=t[n-1];
        printf("t[  0],t[%3d]-> %3d\t%3d\n",n-1,t[n-1],sum);
        sum+=t[0];
        printf("t[  0]       <- %3d\t%3d\n",t[0],sum);
        sum+=t[n-2];
        printf("t[  0],t[%3d]-> %3d\t%3d\n",n-2,t[n-2],sum);
        return sum;
    }

    int a=t[0],b=t[1];
    int y,z;
//    　　　　当2b＞a+y时，使用模式一将Z和Y移动过桥；
//　　　　当2b＜a+y时，使用模式二将Z和Y移动过桥；
//　　　　当2b＝a+y时，使用模式一将Z和Y移动过桥。这样就使问题转变为N-2个旅行者的情形，从而递归解决之。
    int c=n;
    while(c>3) {
        y=t[c-2],z=t[c-1];
        if((b<<1)>=a+y) {
            sum+=z;
            printf("t[  0],t[%3d]-> %3d\t%3d\n",c-1,t[c-1],sum);
            sum+=a;
            printf("t[  0]       <- %3d\t%3d\n",t[0],sum);
            sum+=y;
            printf("t[  0],t[%3d]-> %3d\t%3d\n",c-2,t[c-2],sum);
            sum+=a;
            printf("t[  0]       <- %3d\t%3d\n",t[0],sum);
        } else {
            sum+=b;
            printf("t[  0],t[  1]-> %3d\t%3d\n",t[1],sum);
            sum+=a;
            printf("t[  0]       <- %3d\t%3d\n",t[0],sum);
            sum+=z;
            printf("t[%3d],t[%3d]-> %3d\t%3d\n",c-2,c-1,t[c-1],sum);
            sum+=b;
            printf("t[  1]       <- %3d\t%3d\n",t[1],sum);
        }
        c-=2;
    }
    if(c==2) {
        sum+=t[1];
        printf("t[  0],t[  1]-> %3d\t%3d\n",t[1],sum);
    } else {
        sum+=t[2];
        printf("t[  0],t[%3d]-> %3d\t%3d\n",2,t[2],sum);
        sum+=t[0];
        printf("t[  0]       <- %3d\t%3d\n",t[0],sum);
        sum+=t[1];
        printf("t[  0],t[%3d]-> %3d\t%3d\n",1,t[1],sum);
    }
    return sum;


}

int main() {
    int t[]= {4,7,2,3,2,4};
    int n=sizeof(t)/sizeof(int);
//    int c=GoOverBridge(t,n);
    int c=GoOverBridgeShowProcess(t,n);
    printf("%d\n",c);
    c=GoOverBridgeDriver(t,n);
    printf("%d\n",c);


    int a[]= {1,4,5,5,5,8,9};
    n=sizeof(a)/sizeof(int);
//    int c=GoOverBridge(t,n);
    c=GoOverBridgeShowProcess(a,n);
    printf("%d\n",c);




    n=sizeof(a)/sizeof(int);
    c=GoOverBridgeDriver(a,n);
    printf("%d\n",c);
    return 0;
}

