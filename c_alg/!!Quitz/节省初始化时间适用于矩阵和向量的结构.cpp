#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <memory.h>

#include <iostream>
//for cpp sort
#include <algorithm>
/**
编程珠玑中文第二版p8
第一章问题9
不需要全部初始化
**/
class Vector {
    int* from;

    int* to;
    int top;//指向to中的最后一个有元素的位置

    int* data;
    int size;

public:
    Vector(int n):
        size(n),
        top(0)
         {
        from=new int[size];
        to=new int[size];
        data=new int[size];


    }
    ~Vector() {
        delete[] from;
        delete[] to;
        delete[] data;
    }
    void SetElement(int i,int value) {
        from[i]=top;
        to[top]=i;
        data[i]=value;
        top++;
    }
    void printArray() {
        for(int i=0; i<top; ++i) {
            printf("%d ",data[to[i]]);
        }
        printf("\n");
    }
};
int main() {
    Vector v(10);
    v.SetElement(5,4);
    v.SetElement(4,1);
    v.SetElement(8,2);
    v.SetElement(1,6);
    v.SetElement(2,8);
    v.SetElement(6,3);
    v.SetElement(9,0);
//    v.SetElement(7,3);
//    v.SetElement(3,7);
//    v.SetElement(0,9);
    v.printArray();

    return 0;
}
