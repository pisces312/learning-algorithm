#ifndef BIGINTEGER_H_INCLUDED
#define BIGINTEGER_H_INCLUDED



#endif // BIGINTEGER_H_INCLUDED
#include<iostream>
#include<string>
#include<vector>
using namespace std;
/*
这里对正实数处理
*/
class BigInteger {


public:
//标记符号
    bool isPositive;
//只保存正数部分
    vector<char> number;
    BigInteger getNewReverseInteger() {
        vector<char> result;
        vector<char>::iterator itr=number.end();
        do {
            itr--;
            result.push_back(*itr);
        } while (itr!=number.begin());
        return BigInteger(result);
    }
    bool isPalindromic() {
        vector<char> result;
        vector<char>::iterator itr=number.end();
        do {
            itr--;
            result.push_back(*itr);
        } while (itr!=number.begin());
        itr=number.begin();
        vector<char>::iterator itr2=result.begin();
        while (itr!=number.end()) {
            if (*itr!=*itr2) {
                return false;
            }
            itr++;
            itr2++;
        }
        return true;
    }
    void reverse() {
        vector<char> result;
        vector<char>::iterator itr=number.end();
        do {
            itr--;
            result.push_back(*itr);
        } while (itr!=number.begin());
        number=result;
    }
    BigInteger(long num) {
        long temp=num;
        string str="";
        while (temp!=0) {
            int n=temp%10;
            str.push_back(char(n+'0'));
            temp/=10;
        }
        setNumber(str);
    }
    BigInteger(vector<char>& num) {
        number=num;
    }
    //根据数字的正反序初始化
    BigInteger(vector<char>& num,bool isReverse) {
        number=num;
        if (isReverse) {
            reverse();
        }
    }
    BigInteger() {
        number.push_back('0');

    }

    BigInteger(string num) {
//        cout<<"原始数字:"<<num<<endl;
        number=getVector(num);

    }
    BigInteger(char* num,int len) {
        number=getVector(num,len);
    }
    void setNumber(string num) {
        number=getVector(num);

    }
    void setBigInteger(BigInteger big) {
        number=big.number;
    }
    vector<char> getVector(char* num,int len) {
        vector<char> result;
        for (int i=0;i<len;i++) {
            result.push_back(num[i]);
        }
        return result;
    }
    vector<char> getVector(string num) {
        vector<char> result;
        string::iterator itr=num.begin();
        for (;itr!=num.end();itr++) {
            result.push_back(*itr);
        }
        return result;
    }

    int getLength() {
        return number.size();
    }
    int hasPoint(string num) {
        string::iterator itr=num.begin();
        int count=0;
        for (;itr!=num.end();itr++,count++) {
            if (*itr=='.') {
                return count;
            }
        }
        return -1;
//        return strchr(num.c_str());
    }
    int hasPoint() {
        vector<char>::iterator itr=number.begin();
        int count=0;
        for (;itr!=number.end();itr++,count++) {
            if (*itr=='.') {
                return count;
            }
        }
//        cout<<"没有小数点"<<endl;
        return -1;

    }
    //小数部分长度
    int decimalLength() {
        int point=hasPoint();
        if (point<0) {
            return 0;
        } else {
            return number.size()-point-1;
        }
    }
    void addAtEnd(char c) {
        number.push_back(c);
    }
    //根据一个数的格式规范化两者
    //必须用引用
    //如果都没有小数点，则不加小数点！！！
    void standardize(BigInteger& b) {
        int aDecLen=decimalLength();
        int bDecLen=b.decimalLength();
        if (aDecLen==0&&bDecLen==0) {
        } else {
            if (aDecLen==0) {
                number.push_back('.');
                number.push_back('0');
                aDecLen=1;
            }
            if (bDecLen==0) {
                b.addAtEnd('.');
                b.addAtEnd('0');
                bDecLen=1;
            }
        }
        //有一者存在小数部分，则规范化

        int maxLen=aDecLen>bDecLen?aDecLen:bDecLen;
        for (int i=0;i<maxLen-aDecLen;i++) {
            number.push_back('0');
        }
        for (int i=0;i<maxLen-bDecLen;i++) {
            b.addAtEnd('0');
        }
        maxLen=number.size()-b.getLength();
//        cout<<"maxlen="<<maxLen<<endl;
        if (maxLen>0) {
            b.standardizePrefix(maxLen);
        } else if (maxLen<0) {
            standardizePrefix(-maxLen);
        }

//        cout<<"完成规格化\n";
//        print();
//        b.print();
    }
    void standardizePrefix(int n) {
        for (int i=0;i<n;i++) {
            number.insert(number.begin(),'0');
        }
    }
    int addSingleChar(char a,char b) {
        return a-'0'+b-'0';
    }


    BigInteger add(string num) {
        vector<char> b=getVector(num);
//        BigInteger* big=new BigInteger(b,false);
        return add(BigInteger(b,false));
    }
    BigInteger add(BigInteger b) {
        standardize(b);
        vector<char>::iterator itrA=number.end();
        vector<char>::iterator itrB=b.number.end();
        vector<char> result;

        //进位
        int carry=0;
        do {
            itrA--;
            itrB--;
            if (*itrA=='.') {
                result.push_back('.');
                continue;
            }
//            cout<<*itrA<<"  "<<*itrB<<endl;
            int num=addSingleChar(*itrA,*itrB)+carry;
            result.push_back((char)(num%10+'0'));

            if (num>9) {
                carry=1;
            } else {
                carry=0;
            }


        } while (itrA!=number.begin());
        if (carry==1) {
            result.push_back('1');
        }


        //
//        cout<<endl;
//        for (int i=result.size()-1;i>=0;i--) {
//            cout<<result.at(i);
//
//        }
//        cout<<endl;


        return BigInteger(result,true);


    }
    void print() { //打印这个整数
        for (unsigned int i=0;i<number.size();i++) {
            cout<<number.at(i);
        }
//        cout<<endl;
//        for each(int i in data) cout << i;
    }
};
