#ifndef TOOLFUNC_H_INCLUDED
#define TOOLFUNC_H_INCLUDED



#endif // TOOLFUNC_H_INCLUDED
#include <iostream>
#include<cmath>
#include<string>
#include"prime.h"
#include <sstream>
#include<iomanip>
using namespace std;
//采用位的方法判断是否含有1~9所有的数字
bool isPandigital9(long num){
    unsigned int a[]={0,1,2,4,8,16,32,64,128,256};
    unsigned int t=0;
    while(num!=0){
        t|=a[num%10];
        num/=10;
    }
//    cout<<t<<endl;必须用无符号数！！
    for(unsigned int i=0x80000000;i>0;i>>=1){
//        cout<<i<<endl;
        if((t&i)==0){
            return false;
        }
    }
    return true;
}
//计算指定小数位数的除法
long* getDivArray(long dividend,long divisor,long size) {
    cout<<((double)dividend)/divisor<<endl;
    long p=0;
    //多计算5位！！保证最后一位的精度
    long* array=new long[size+5];
    long long m=100000,r=dividend;
    for (long i=0;i<size+5;i++,p++) {
        long long d=r*m;

        long q=d/divisor;
//        cout<<q<<" ";
        r=d%divisor;
//        cout<<r<<endl;
        array[p]=q;
//        if (q>=m) {
//            long c=p;
//            while (c>0&&array[c]>m) {
//                array[c]-=m;
//                array[c-1]++;
//                c--;
//            }
//
//        } else if (q<0) {
//            long c=p;
//            while (c>0&&array[c]<m) {
//                array[c-1]--;
//                array[c]+=m;
//                c--;
//            }
//
//        }
        cout<<array[p]<<endl;


    }
    //输出

    for (long i=0;i<size;i++) {
//        cout<<array[i];
        cout<<setw(5)<<setfill('0')<<array[i];
    }
    cout<<endl;
    return array;

}
//获得len长度的小数位数
string getFraction(int n,int d,int len) {
    string result;
    int q,r=-1;
    while(n>=d){
        q=n/d;
        r=n%d;
        n=r;
        result.push_back('0'+q);
    }
    if(r==-1){
//        cout<<"r==-1\n";
        result.push_back('0');
    }
//    if(r==0){
//        return result;
//    }else
//    {
        result.push_back('.');
//    }

    int c=0;
    while (c<len) {
        while (n<d&&n!=0) {
            n*=10;
            if (n<d) {
                //补零
                result.push_back('0');
                c++;
//                    cout<<'0';
            }

        }
        if(c==len){
            break;
        }
        c++;
        q=n/d;
        r=n%d;
        n=r;
        result.push_back('0'+q);
    }
    return result;
}
//即求延迟数
int getMaxPowerOf2And5(int num){
    int c2=0,c5=0;
    int num2=num;
    while(num2%2==0){
        c2++;
        num2/=2;
    }
    int num5=num;
    while(num5%5==0){
        c5++;
        num5/=5;
    }
    return c2>c5?c2:c5;
}
//确定字典排序的全排列的下一个
string nextLexicographicPermutations(string number) {
//    string number="839647521";
//    string number="0123456789";
//    cout<<number<<endl;
    int i,begin=0,end=0,len=number.size();
    for (i=0;i<len-1;i++) {
        if (number[i]<number[i+1]) {
            begin=i+1;//取最往后的一组
        }
    }
//    cout<<number[begin]<<endl;
    char max=number[begin-1];

    for (i=begin;i<len;i++) {
        if (max<number[i]) {
            end=i;
        }
    }
//    cout<<number[end]<<endl;
//    cout<<"swaping...\n";
    number[begin-1]=number[end];
    number[end]=max;
//    cout<<number<<endl;
//    cout<<"reversing...\n";
    string temp;
    i=len-1;
    while (i>=begin) {
        temp.push_back(number[i--]);
    }
//    cout<<temp<<endl;

//    cout<<number.replace(begin,len-begin,temp)<<endl;
    return number.replace(begin,len-begin,temp);
}
bool isPandigital(string num) {
    int n=num.size();
    int* a=new int[n+1];
    for (int i=1;i<=n;i++) {
        a[i]=0;
    }
    for (int i=0;i<n;i++) {
        int c=num[i]-'0';
        if (c>n+1) {
            return false;
        } else {
            a[c]=1;
        }
    }
    for (int i=1;i<=n;i++) {
        if (a[i]==0)
            return false;

    }
    return true;
}
//判断是否包含n个数字
bool isPandigital(unsigned long number,int n) {
    int* a=new int[n+1];
    for (int i=1;i<=n;i++) {
        a[i]=0;
    }
    unsigned long temp=number;
    int count=0;
    while (temp!=0) {
        int p=temp%10;
        temp/=10;
        if (p>n) {//数字不在前n个数字之内
            return false;
        }
        a[p]=1;
        count++;
    }
    if (count>n)
        return false;
    for (int i=1;i<=n;i++) {
        if (a[i]==0)
            return false;

    }
    return true;
}
int getDigitsCount(long number) {
    long temp=number;
    int count=0;
    while (temp!=0) {
        int n=temp%10;
        temp/=10;
        count++;
    }
    return count;
}
unsigned long stringToLong(string number) {
    stringstream stream;
    unsigned long n;
    stream << number;
    stream>>n;
    return n;

}
unsigned long reverseLong(unsigned long num) {
    unsigned long temp=num;
    string str;
    while (temp!=0) {
        int n=temp%10;
        str.push_back(char(n+'0'));
        temp/=10;
    }
    return stringToLong(str);
}
int stringToInteger(string number) {
    int n=0,cost=1;
    for (int i=number.size()-1;i>=0;i--,cost*=10) {
        n+=(number[i]-'0')*cost;

    }
    return n;

}
string powInt(int n,int max) {
    vector<int> digits;
    digits.push_back(1);
    int count=0,carry;
    while (count<max) {
        vector<int>::iterator itr=digits.begin();
        carry=0;
        while (itr!=digits.end()) {
            int p=(*itr)*n+carry;
            *itr=p%10;
            if (p>9) {
                carry=1;
            } else {
                carry=0;
            }
            itr++;
        }
        if (carry==1) {
            digits.push_back(1);
        }
        count++;
    }
    int sum=0;
    string result;
//    result.resize((digits.size());
    vector<int>::iterator itr=digits.end();
    do {
        itr--;
        result.push_back((char)(*itr+'0'));
    } while (itr!=digits.begin());
    return result;
}
bool isPalindromic2(unsigned long num) {
    unsigned long temp=num;
    string str="";
    while (temp!=0) {
        int n=temp%10;
        str.push_back(char(n+'0'));
        temp/=10;
    }
//    cout<<str<<endl;
    if (stringToLong(str)==num) {
        return true;
    }
    return false;
}
bool isPalindromic(long num) {
    int temp=num;
    string str="";
    while (temp!=0) {
        int n=temp%10;
        str.push_back(char(n+'0'));
        temp/=10;
    }
    string str2="";
    string::iterator itr=str.end();
    do {
        itr--;
        str2.push_back(*itr);
    } while (itr!=str.begin());
//    cout<<str2<<endl;
    return str.compare(str2)?false:true;
}
string integerToString2(unsigned long num) {
    stringstream stream;
    string str;
    stream << num;
    stream>>str;
    return str;

}
string integerToString(int num) {
    int temp=num;
    string str="";
    while (temp!=0) {
        int n=temp%10;
        str.push_back(char(n+'0'));
        temp/=10;
    }
    return str;
}
bool isPalindromic(string str) {
    string str2="";
    string::iterator itr=str.end();
    do {
        itr--;
        str2.push_back(*itr);
    } while (itr!=str.begin());
//    cout<<str2<<endl;
    return str.compare(str2)?false:true;
}
//求最大公约数
unsigned long long maxCommonDivisor(unsigned long long a,unsigned long long b) {
    unsigned long long n;
    do {
        n=a%b;
//        int temp=a;
        a=b;
        b=n;
    } while (n!=0);
    return a;
}
//求两数间的最小公倍数
int minCommonMultiple(int a,int b) {
    return a*b/maxCommonDivisor(a,b);
}
int minCommonMultiple(int array[],int len) {
    if (len<1)
        return 0;
    int temp=array[0];
    for (int i=1;i<len;i++) {
        temp=minCommonMultiple(temp,array[i]);
    }
    return temp;
}
//求前n项自然数和
int naturalSum(int n) {
    int sum=0;
    for (int i=1;i<=n;i++) {
        sum+=i;
    }
    return sum;

}
int naturalSquareSum(int n) {
    int sum=0;
    for (int i=1;i<=n;i++) {
        sum+=i*i;
    }
    return sum;
}

