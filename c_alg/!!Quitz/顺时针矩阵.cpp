/*
* Copyright (c) 2010,东南大学数据库课题组
* All rights reserved.
*
* 文件名称：
* 摘    要：简要描述本文件的内容
*
* 当前版本：1.0
* 作    者：倪力
* 完成日期：2010年
*/
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
//int GetElement(int* a,int i,int j){
//    a
//}
//m*n
void PrintClockwiseMatrix(int m,int n) {
    if(m<=0||n<=0){
        return;
    }
    int t=m*n;
    int* a=new int[t];
//    memset(a,0,t*sizeof(int));
    int i=0,j=-1;
    int c=0;
    int ce=n-1,re=m-1;//行的右边界，列的下边界
    int cb=0,rb=1;//行的左边界，列的上边界
    while(c<t) {

        ++j;
        while(j<=ce) {
            *(a+i*n+j)=++c;
            ++j;
        }
        j=ce--;

        ++i;
        while(i<=re) {
            *(a+i*n+j)=++c;
            ++i;
        }
        i=re--;

        --j;
        while(cb<=j) {
            *(a+i*n+j)=++c;
            --j;
        }
        j=cb++;

        --i;
        while(rb<=i) {
            *(a+i*n+j)=++c;
            --i;
        }
        i=rb++;

    }
    for(i=0; i<m; ++i) {
        for(j=0; j<n; ++j) {
//            if(*(a+i*n+j))
            printf("%4d ",*(a+i*n+j));
//            else{
//                printf("  ");
//            }
        }
        printf("\n");
    }

    delete[] a;
}
int main() {
    int m=4,n=5;
    PrintClockwiseMatrix(m,n);

    m=7,n=4;
    PrintClockwiseMatrix(m,n);


    m=5,n=5;
    PrintClockwiseMatrix(m,n);

    m=4,n=4;
    PrintClockwiseMatrix(m,n);
    return 0;
}
