//随机抽取某区间内的数，要求全部抽取但不能重复

#include <stdio.h>
#include <stdlib.h>
//#include <unistd.h>
#include<time.h>
int rand_no_overlap(int start, int end);

int main(int argc, char **argv) {
    rand_no_overlap(1, 10);
    exit(EXIT_SUCCESS);
}

int rand_no_overlap(int start, int end) {
    int     size = end - start + 1;
    int     arr[size];

    /* init array */
    int     i;
    for (i = 0; i < size; i++)
        arr[i] = start + i;

    int     p_cur;
    int     p_tail = size - 1;

    srand(time(NULL));
    for (i = 0; i < size; i++) {
        p_cur = rand() % (p_tail + 1);//产生的随机数范围不断缩小
        printf("%d\n", arr[p_cur]);

        // copy the last element to current position
        arr[p_cur] = arr[p_tail--];
    }
}
