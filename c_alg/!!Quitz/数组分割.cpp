#include <iostream>
#include <cstring>
#include <cstdio>
using namespace std;
//动态规划: isOK[i][v]表示是否可以找到i个数,使得他们之和等于v.
const int m = 0xffff;
bool isOK[100][m];

void HalfSum(int data[], int N) {
    //初始化数组
    memset(isOK, 0, sizeof(isOK));
    isOK[0][0] = true;//0个数，和为0！

    int sum = 0;
    int n = N/2;  //分割成一半
    for (int i = 0; i < N; ++i)  //计算总和
        sum += data[i];

    int halfsum=(sum>>1);
    for (int k = 1; k < N; ++k) {//对原始数组的每个元素进行遍历
        for (int i = 1; i <= min(k, n); ++i) {//个数
            for (int v = 1; v <=halfsum; ++v)
            //保证当前数组的和大于原始数组的第k个元素
                if ((v >= data[k]) && isOK[i - 1][v - data[k]])//i-1个数
                    isOK[i][v] = true;
        }
    }

    while (!isOK[n][ halfsum])
        -- halfsum;
    cout << halfsum << endl;
}

int main() {
    int data[] = {1, 5, 7, 8, 9, 6, 3, 11, 20, 17};
    HalfSum(data, sizeof(data) / sizeof(data[0]));
    return 0;
}
