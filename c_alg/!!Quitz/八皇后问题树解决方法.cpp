#include<iostream>
#include<iomanip>
using namespace std;
const int max=8;
int x=1,y=1;
typedef struct Node
{
	int qipan[max+1];//0号不用,qipan[i]存放第i行棋子的列位置,qipan[i]在(1,max)之间
    struct Node *next[max+1];//存放孩子指针
}*Tree;
int count_qipan(int* p)//返回棋盘已摆放的行数
{
    int z=0,i=1;
    while(p[i]!=0&&i<=max)
	{
        z++;
        i++;
    }
    return z;
}
bool illage(int *p)//判断棋盘摆放是否合法
{
    int count=count_qipan(p);
    for(int i=0;i<=count;i++)
        for(int j=i+1;j<=count;j++)
            if(p[i]==p[j]||p[j]==p[i]+(j-i)||p[j]==p[i]-(j-i)) 
				return 0;//不同行两棋子同列,不同行两棋子在对角线上
    return 1;
}
void print(int *p)//打印棋盘,用矩阵表示,1表示皇后放的位置,其余位置用0表示
{
    for(int i=1;i<=max;i++)
	{
		for(int j=1;j<=max;j++)
			if(p[i]==j) cout<<setw(2)<<1;
			else cout<<setw(2)<<0;
        cout<<"\n";
    }
	cout<<endl;
}
void create(Node * &t,int i,int j)//用递归建立棋盘树(在棋盘结点的第i行上棋子位置为j)
{
    int m,c;
    t->qipan[i]=j;//在(i,j)位置放置一个皇后
    for(c=i+1;c<=max;c++)
        t->qipan[c]=0;
    if(i==max)//如果放好了8个皇后就返回
	{
        for(m=1;m<=max;m++)
            t->next[m]=NULL;
        return;
    }
    if(illage(t->qipan))//合法才继续建立
        for(m=1;m<=max;m++)
		{
            t->next[m]=new Node;
            for(c=1;c<=i;c++)
                t->next[m]->qipan[c]=t->qipan[c];
            for(;c<=max;c++)
                t->next[m]->qipan[c]=0;
            create(t->next[m],i+1,m);
        }
}
void createtree(Tree &p)//建立树
{
    int i;
    p=new Node;
	for(i=1;i<=max;i++)
        p->qipan[i]=0;//设置空棋盘(把棋盘的数组元素全部赋0)
    for(i=1;i<=max;i++)
	{
        p->next[i]=new Node;
        create(p->next[i],1,i);
    }
}
bool isleaf(Node *p)//判断结点是否为叶子
{
    if(count_qipan(p->qipan)==max) 
        return 1;
    return 0;
}
int traverse(Node *p)//先根便历棋盘,打印所有可能
{
    static int num=0;
    if(isleaf(p))
	{
		if(illage(p->qipan)&&p->qipan[x]==y)
		{
	        print(p->qipan);
			num++;
		}
	}
    else
	{
        if(illage(p->qipan))
            for(int i=1;i<=max;i++)
                traverse(p->next[i]);
    }
    return num;
}
void main()
{
    Tree T;	
    cout<<"请设定一个皇后位置:(从1开始)";
	cin>>x>>y;
	createtree(T);
    cout<<"共有解法:"<<traverse(T)<<endl;
}
