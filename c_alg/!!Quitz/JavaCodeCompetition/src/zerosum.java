import java.util.LinkedList;

/**
 * BFS,
 * only handle <S9
 * 
 * @author pisces312
 */
public class zerosum {

    //    static int                   num;
    //    static int []                nums     = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    //    static int []                nums     = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    static int []                nums     = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9};
    static char []               operator = new char[] {' ', '+', '-'};
    //  static Scanner in;
    //    static PrintWriter           pw;
    //    static Scanner               in;

    static LinkedList<Character> operators;

    static StringBuilder         sb       = new StringBuilder();
    static boolean               isFirst  = true;

    public static void main(String [] args) throws Exception {
        System.out.println(Integer.MAX_VALUE);
        //        in = new Scanner(new BufferedReader(new FileReader(
        //                "zerosum.in")));
        //        pw = new PrintWriter(new BufferedWriter(new FileWriter(
        //                "zerosum.out")));
        //      in = new Scanner(System.in);
        //      pw = new PrintWriter(System.out);
        //        num = in.nextInt();
        //        System.out.println("3 calc and output " + (System.currentTimeMillis() - start));
        //
        //
        //
//        long start = System.currentTimeMillis();
//        operators = new LinkedList<Character>();//用一个linedlist记录每层添加操作数
//        dfs(1);//dfs从第一层开始
//        System.out.println("time: " + (System.currentTimeMillis() - start));
//        //        start = System.currentTimeMillis();
//        System.out.println(sb);
//        //        pw.close();
    }

    public static void dfs(int depth) {
        //        LinkedList<Character> temp = operators;
        if (depth == nums.length) {
            if (judge()) {//如果到达第八层，则判断和是否为0
                if (isFirst) {
                    isFirst = false;
                } else {
                    sb.append(';');
                }
                for (int i = 0; i < nums.length - 1; i++) {
                    sb.append(nums[i] + "" + operators.get(i));
                }
                sb.append(nums[nums.length - 1]);
                //              System.out.println(sb);
            }
        } else {
            for (int i = 0; i <= 2; i++) {//如果没有到达终止条件，则继续dfs
                operators.addLast(operator[i]);
                dfs(depth + 1);
                operators.removeLast();
            }
        }
    }

    public static boolean judge() {
        //        LinkedList<Character> temp = operators;
        LinkedList<Character> ops = new LinkedList<Character>(operators);//记录操作符
        LinkedList<Long> ots = new LinkedList<Long>();//记录操作数
        //!!!每次都重新放入操作数，由于有空格操作
        for (int i = 0; i < 9; i++) {
            ots.add((long)(i + 1));
        }
        for (int i = 0; i < ops.size();) {
            char op = ops.get(i);
            if (op == ' ') {//如果操作符是“ ”，则更新操作数
                ops.remove(i);
                long a = ots.remove(i);
                long b = ots.remove(i);
                ots.add(i, a * 10 + b);
            } else {
                i++;
            }
        }
        long a = ots.get(0);//计算总和
        for (int i = 0; i < ops.size(); i++) {
            char op = ops.get(i);
            if (op == '+') {
                a = a + ots.get(i + 1);
            } else {
                a = a - ots.get(i + 1);
            }
        }
        if (a == 0) {
            return true; //总和为0，返回true
        } else {
            return false;
        }
    }

}