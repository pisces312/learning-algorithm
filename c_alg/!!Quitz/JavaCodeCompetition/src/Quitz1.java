import java.util.Date;

/**
 * An arithmetic sequence is a sequence can be presented as:
 * a, a+b, a+2b, a+3b, ... , a+nb(n=0,1,2,3...)
 * a is non-negative integer and b is integer.
 * Number of bisquares is a number which can be presented by 2 numeric squares,
 * such as p^2+q^2.
 * Given 2 input integer. The first input integer is the length of the
 * arithmetic sequence. The 2nd input integer is the maximum integer in the
 * bisquare. Definition:
 * X: 3<=X<=25, Integer, the length of the sequence
 * Y: 1<=Y<=500, Integer, the maximum value of p and q, 0<=p, q<=Y
 * Calculate and output all the 'a' and 'b' that meet the requirement of: all
 * element in the arithmetic sequence constructed by 'a' and 'b' can be found in
 * the collection of p^2+q^2, which p and q are any number in the boundary of Y.
 * Sample Input: (Format: comma separated 2 digits, "X,Y")
 * 5,7
 * Sample Output:
 * (Format: comma separated 2 digits "a,b", each group is separated by
 * semicolon. If no answer found, or error occurred, output a "NOTFOUND". Order
 * by: b ascending, then a ascending)
 * 1,4; 37,4; 2,8; 29,8; 1,12; 5,12; 13,12; 17,12; 5,20; 2,24
 * 1,4;1,12;2,8;2,24;5,12;5,20;13,12;17,12;29,8;37,4;
 * amax>=37
 * bmax>=24
 * 
 * @author pisces312
 */
public class Quitz1 {

    public String runWithoutOtherfunction(String arg) {
        // parse parameters
        String [] pars = arg.split(",");
        //the length of the sequence
        int x = Integer.parseInt(pars[0]);
        int y = Integer.parseInt(pars[1]);
        ////////////////////////////////////////
        int qmax = y;
        int qmin = 0;
        int [] squ = new int[qmax + 1];
        for (int i = qmin; i <= qmax; ++i) {
            squ[i] = i * i;

        }
        //////////////////////////////////////

        int [] sumArray = new int[squ.length * (squ.length + 1) / 2];
        //        System.out.println(Arrays.toString(sumArray));
        int summax = 0;
        int k = 0;
        for (int i = 0; i < squ.length; ++i) {
            for (int j = i; j < squ.length; ++j) {
                sumArray[k] = squ[i] + squ[j];
                ++k;
                //
                //each p^2+q^2 is different
                summax += squ[i];
            }
        }
        //TODO better sort
        //        Arrays.sort(sumArray);
        for (int i = 0; i < sumArray.length - 1; i++) {
            for (int j = 0; j < sumArray.length - i - 1; j++) {
                if (sumArray[j] > sumArray[j + 1]) {
                    int temp = sumArray[j];
                    sumArray[j] = sumArray[j + 1];
                    sumArray[j + 1] = temp;
                }
            }
        }
        //        System.out.println(Arrays.toString(sumArray));
        //////////////////////////////////////
        int amin = 0;
        int bmin = 1;
        //a good one
        int amax = (summax - x * (x - 1) * bmin) / x + 1;
        int bmax = (summax / (x * (x - 1) / 2)) + 1;
        //not a good boundary
        //        int amax = 2 * squ[qmax - 1] - n * bmin;
        //        int bmax = 2 * squ[qmax - 1];
        //        System.out.println("amax:" + amax);
        //        System.out.println("bmax:" + bmax);
        StringBuilder sb = new StringBuilder();
        for (int b = bmin; b <= bmax; ++b) {
            for (int a = amin; a <= amax; ++a) {
                int i = 0;
                for (int key = a; i < x; ++i, key += b) {
                    ////////////////////////////////////////
                    //binary search
                    int idx = -1;
                    //
                    int low = 0;
                    int high = sumArray.length - 1;
                    while (low <= high) {
                        int mid = (low + high) >>> 1;
                        int midVal = sumArray[mid];
                        if (midVal < key)
                            low = mid + 1;
                        else if (midVal > key)
                            high = mid - 1;
                        else {
                            idx = mid; // key found
                            break;
                        }
                    }
                    ////////////////////////////////////////
                    if (idx < 0 || idx >= sumArray.length) {
                        break;
                    }
                    ////
                    //
                    //                    int idx = Arrays.binarySearch(sumArray, sum);
                    //                    int idx = Arrays.binarySearch(sumArray, sum);

                }
                if (i == x) {
                    if (sb.length() > 0) {
                        sb.append(";");
                    }
                    sb.append(a + "," + b);
                }
            }
        }

        //
        //
        //        System.out.println(x + "," + y);
        //        System.out.println(Arrays.toString(squ));
        //        System.out.println(Arrays.toString(sumArray));

        if (sb.toString().isEmpty()) {
            return "NOTFOUND";
        }
        return sb.toString();
    }

    public String run3(String arg) {
        // parse parameters
        String [] pars = arg.split(",");
        //the length of the sequence
        int x = Integer.parseInt(pars[0]);
        int y = Integer.parseInt(pars[1]);
        ////////////////////////////////////////
        //calc each p, q's square
        int [] squ = new int[y + 1];
        for (int i = 0; i < squ.length; ++i) {
            squ[i] = i * i;
        }
        //////////////////////////////////////
        int [] sumArray = new int[squ.length * (squ.length + 1) / 2];
        //        System.out.println(Arrays.toString(sumArray));
        int summax = 0;
        int k = 0;
        for (int i = 0; i < squ.length; ++i) {
            for (int j = i; j < squ.length; ++j) {
                sumArray[k] = squ[i] + squ[j];
                ++k;
                //
                //each p^2+q^2 is different
                summax += squ[i];
            }
        }
        java.util.Arrays.sort(sumArray);
        //////////////////////////////////////
        int amin = 0;
        int bmin = 1;
        int amax = (summax - x * (x - 1) * bmin) / x + 1;
        int bmax = (summax / (x * (x - 1) / 2)) + 1;
        StringBuilder sb = new StringBuilder();
        for (int b = bmin; b <= bmax; ++b) {
            for (int a = amin; a <= amax; ++a) {
                int i = 0;
                for (int key = a; i < x; ++i, key += b) {
                    ////////////////////////////////////////
                    int idx = java.util.Arrays.binarySearch(sumArray, key);
                    if (idx < 0 || idx >= sumArray.length) {
                        break;
                    }
                }
                if (i == x) {
                    if (sb.length() > 0) {
                        sb.append(";");
                    }
                    sb.append(a + "," + b);
                }
            }
        }
        if (sb.length() == 0) {
            return "NOTFOUND";
        }
        return sb.toString();
    }

    public String run4(String arg) {
        // parse parameters
        String [] pars = arg.split(",");
        //the length of the sequence
        int x = Integer.parseInt(pars[0]);
        int y = Integer.parseInt(pars[1]);
        ////////////////////////////////////////
        //calc each p, q's square
        int [] squ = new int[y + 1];
        for (int i = 0; i < squ.length; ++i) {
            squ[i] = i * i;
        }
        //////////////////////////////////////
        int [] sumArray = new int[squ.length * (squ.length + 1) / 2];
        //        System.out.println(Arrays.toString(sumArray));
        boolean [] mark = new boolean[y * y * 2 + 1];
        int summax = 0;
        int k = 0;
        for (int i = 0; i < squ.length; ++i) {
            for (int j = i; j < squ.length; ++j) {
                sumArray[k] = squ[i] + squ[j];
                mark[sumArray[k]] = true;
                ++k;
                //each p^2+q^2 is different
                summax += squ[i];
            }
        }
        //////////////////////////////////////
        int amin = 0;
        int bmin = 1;
        int amax = (summax - x * (x - 1) * bmin) / x;
        //        int bmax = (summax / (x * (x - 1) / 2)) + 1;
        int bmax = (sumArray[sumArray.length - 1] - sumArray[0]) / x;
        StringBuilder sb = new StringBuilder();
        for (int b = bmin; b <= bmax; ++b) {
            for (int a = amin; a <= amax; ++a) {
                int i = 0;
                for (int key = a; i < x; ++i, key += b) {
                    if (key >= mark.length) {
                        break;
                    }
                    if (!mark[key]) {
                        break;
                    }
                }
                if (i == x) {
                    if (sb.length() > 0) {
                        sb.append(";");
                    }
                    sb.append(a + "," + b);
                }
            }
        }
        if (sb.length() == 0) {
            return "NOTFOUND";
        }
        return sb.toString();
    }

    //    public String runOther(String arg) {
    //        String [] pars = arg.split(",");
    //        //the length of the sequence
    //        int n = Integer.parseInt(pars[0]);
    //        int m = Integer.parseInt(pars[1]);
    //        //int n=5;int m=7;
    //
    //        Boolean [] mark = new Boolean[m * m * 2 + 1];
    //        int i, j;
    //        for (i = 0; i < m * m * 2 + 1; i++)
    //            mark[i] = false;
    //
    //        for (i = 0; i <= m; i++)
    //            for (j = i; j <= m; j++) {
    //                mark[i * i + j * j] = true;
    //            }
    //        int max = m * m * 2 + 1;
    //        ArrayList<Integer> v = new ArrayList<Integer>();
    //
    //        //sort
    //        for (i = 0; i < max; i++) {
    //            if (mark[i] == true)
    //                v.add(i);
    //        }
    //
    //        int len;
    //        Boolean isfind = false;
    //        int size = v.size();
    //        int max_len = (v.get(size - 1) - v.get(0)) / (n - 1);
    //        for (len = 1; len <= max_len; len++)
    //            for (i = 0; i < size; i++) {
    //                int delen = n - 1;
    //                int num = v.get(i);
    //                while (delen > 0) {
    //                    num += len;
    //                    if (num > v.get(size - 1))
    //                        break;
    //                    if (!mark[num])
    //                        break;
    //                    delen--;
    //                }
    //                if (delen == 0) {
    //                    isfind = true;
    //                    System.out.print(v.get(i) + "," + len + ";");
    //                }
    //            }
    //        if (isfind == false)
    //            System.out.println("none");
    //
    //        return " ";
    //    }

    public String runOther2(String arg) {
        String [] pars = arg.split(",");
        //the length of the sequence
        int n = Integer.parseInt(pars[0]);
        int m = Integer.parseInt(pars[1]);
        //max p value
        //boolean array, default is false

        int i, j;
        int [] squ = new int[m + 1];
        for (i = 0; i < squ.length; ++i) {
            squ[i] = i * i;
        }

        //all the p^2+q^2 marked true
        boolean [] mark = new boolean[m * m * 2 + 1];
        for (i = 0; i <= m; ++i) {
            for (j = i; j <= m; ++j) {
                mark[squ[i] + squ[j]] = true;
            }
        }
        //!!!sort v using map
        //v.length:all possiable p^2+q^2
        int [] sum = new int[squ.length * (squ.length + 1) / 2];
        for (i = 0, j = 0; i < mark.length; ++i) {
            if (mark[i]) {
                sum[j] = i;
                ++j;
            }
        }
        //        int summax=v[j-1];
        //        System.out.println(Arrays.toString(v));
        //        System.out.println(Arrays.toString(squ));

        StringBuilder sb = new StringBuilder();
        //a+nb-(a)=nb
        //        int bmax = (v[v.length - 1] - v[0]) / (n - 1);
        //limit the b range
        int bmax = (sum[j - 1] - sum[0]) / (n - 1);
        for (int b = 1; b <= bmax; ++b)
            //!use subtraction
            for (i = 0; i < j; i++) {
                //check if all member is satisfied
                int delen = n - 1;
                //
                int num = sum[i];
                while (delen > 0) {
                    num += b;
                    //max value of p^2+q^w
                    if (num > sum[j - 1])
                        break;
                    if (!mark[num])
                        break;
                    delen--;
                }
                //
                if (delen == 0) {
                    if (sb.length() > 0) {
                        sb.append(";");
                    }
                    sb.append(sum[i] + "," + b);
                }
            }
        if (sb.length() == 0)
            return "NOTFOUND";

        return sb.toString();
    }

    /**
     * 0.3ms
     * 
     * @param args
     */
    public static void main(String [] args) {
        //        String result = null;
        //        Quitz1 quitz = new Quitz1();
        //        int c = 10;
        //        long start = System.currentTimeMillis();
        //        for (int i = 0; i < c; ++i) {
        //            //            result = quitz.run2("6,17");
        //            //            result = quitz.run3("7,7");
        //            //            result = quitz.run3("5,7");
        //            //                        result = quitz.run3("25,500");
        //            /**
        //             * 14123.0
        //             * 37,84;521,84;901,84;1217,84;1301,84;1385,84;1469,84;1553,84;1637,
        //             * 84;1721,84;74,168;41,252
        //             */
        //            //                        result = quitz.run3("12,50");
        //            //            result = quitz.runOther("12,50");
        //            result = quitz.runOther2("12,50");
        //            //            result = quitz.run4("12,50");
        //            //                        result = quitz.runOther("25,500");
        //        }
        //        System.out.println();
        //        long total=(System.currentTimeMillis() - start);
        //        System.out.println(total);
        //        System.out.println((total) / (double)c);
        //        System.out.println();
        //        System.out.println(result);
        System.out.println(new Date(1363110874754l));
    }

}
