import junit.framework.Assert;

public class RomanNumerals {
    //    public static String test(String arg, int c) {
    //        String result = null;
    //
    //        long start = System.currentTimeMillis();
    //        for (int i = 0; i < c; ++i) {
    //            result = run2(arg);
    //            //            result = run2(arg);
    //        }
    //        long total = (System.currentTimeMillis() - start);
    //        System.out.println();
    //        System.out.println("total: " + total + "ms");
    //        double time = (total) / (double)c;
    //        System.out.println("each: " + time + "ms");
    //        System.out.println(result);
    //        //        System.out.println("expected:" + expected);
    //
    //        return result;
    //    }

    public static double test(String arg, String expected, int c) {
        String result = null;

        long start = System.currentTimeMillis();
        for (int i = 0; i < c; ++i) {
            result = run4(arg);
            //            result = run2(arg);
        }
        long total = (System.currentTimeMillis() - start);
        System.out.println();
        System.out.println("total: " + total + "ms");
        double time = (total) / (double)c;
        System.out.println("each: " + time + "ms");
        System.out.println(result);
        //        System.out.println("expected:" + expected);
        if (expected != null && !expected.isEmpty()) {
            Assert.assertEquals(expected, result);
        }
        return time;

    }

    //    public static String test(String arg, int c) {
    //        String result = null;
    //        long start = System.currentTimeMillis();
    //        for (int i = 0; i < c; ++i) {
    //            result = run3(arg);
    //            //            result = run(arg);
    //        }
    //        System.out.println();
    //        long total = (System.currentTimeMillis() - start);
    //        System.out.println("total:" + total);
    //        System.out.println("each:" + (total) / (double)c);
    //        System.out.println(result);
    //        return result;
    //    }

    public static void testRoman1(int c) {
        test("3500", "ILLEGAL", c);
        test("0", "ILLEGAL", c);
        test("280", "I392,V140,X411,L141,C282", 1);
        test("156", "I218,V78,X225,L67,C67", 1);
        test("378", "I531,V190,X555,L189,C567", 1);
        //        test("5", null, 1);//7I 2V
        test("20", null, c);
        test("500", null, c);
        test("3000", null, c);
        test("3499", null, c);
        test("68", null, c);
    }

    public static void testRoman2() {

        //illegal case
        test("MM0CDLXXXIX", "ILLEGAL", 1);
        test("IIILXV", "ILLEGAL", 1);
        test("IVLXD", "ILLEGAL", 1);
        test("VILXD", "ILLEGAL", 1);
        test("LXD", "ILLEGAL", 1);
        test("IL", "ILLEGAL", 1);
        test("IIII", "ILLEGAL", 1);
        test("IIIA", "ILLEGAL", 1);

        test("中国", "ILLEGAL", 1);

        test("lxiV", "ILLEGAL", 1);

        //legal case
        test("LXIV", "64", 1);
        test("MDLXVIII", "1568", 1);
        test("L", "50", 1);
        test("DLV", "555", 1);
        test("LXVIII", "68", 1);

        test("CMCDCLXIV", "1464", 1);

        test("CMCMCLIX", "1959", 1);

        test("CMXCIX", "999", 1);

        test("CDXC", "490", 1);

        test("XCIX", "99", 1);

        test("CMXC", "990", 1);

        test("XC", "90", 1);

    }

    public static String generateMap(int start, int maxn) {
        /**
         * count the result for each number
         * [0] first element stands for flag, 1 for filled
         * 1)I=1
         * 2)V=5
         * 3)X=10
         * 4)L=50
         * 5)C=100
         * 6)D=500
         * 7)M=1000
         * default value is 0
         * use map[0][] as total result
         */
        char [] romanNumerals = {'I', 'V', 'X', 'L', 'C', 'D', 'M'};
        int [][] map = new int[maxn + 1][romanNumerals.length + 1];
        /**
         * M,MM,MMM
         * C,CC,CCC,CD,D,DC,DCC,DCCC,CM
         * X,XX,XXX,XL,L,LX,LXX,LXXX,XC
         * I,II,III,IV,V,VI,VII,VIII,IX
         */
        for (int j = 1, i = 1; j < map.length; i += 2, j *= 10) {
            for (int k = j, c = 1; k < map.length && c < 10; k += j, ++c) {
                map[k][0] = 1;
                switch (c) {
                    case 1:
                    case 2:
                    case 3:
                        map[k][i] = c;
                        break;
                    case 4:
                        map[k][i] = 1;
                    case 5:
                        map[k][i + 1] = 1;
                        break;
                    case 6:
                    case 7:
                    case 8:
                        map[k][i + 1] = 1;
                        map[k][i] = c - 5;
                        break;
                    case 9:
                        map[k][i] = 1;
                        map[k][i + 2] = 1;
                        break;
                }
                //                    System.out.println(Arrays.toString(map[k]));
            }
        }
        ////////////////////////////////////////////////////////////////////////////////
        int [] mapForDigit = {1000, 100, 10, 1};
        for (int i = 1; i <= maxn; ++i) {
            if (map[i][0] == 0) {
                int high = i / mapForDigit[0];
                int j = 0;
                while (high == 0) {
                    ++j;
                    high = i / mapForDigit[j];
                }
                if (mapForDigit[j] > 1) {
                    high *= mapForDigit[j];
                    int low = i % mapForDigit[j];
                    if (low > 0) {
                        for (j = 1; j <= romanNumerals.length; ++j) {
                            map[i][j] += map[low][j];
                        }
                    }
                }
                for (j = 1; j <= romanNumerals.length; ++j) {
                    map[i][j] += map[high][j];
                }
                map[i][0] = 1;
            }
            //
            //            System.out.println(i + ": " + Arrays.toString(map[i]));
        }

        //        System.out.println();
        //////////////////////////////////////////////////////////////
        //construct answer string
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (int i = 0, len = map.length - 1; i < len; ++i) {
            if (i >= start)
                sb.append("{");
            for (int j = 0; j <= romanNumerals.length; ++j) {
                map[i + 1][j] += map[i][j];
                if (i >= start) {
                    sb.append(map[i][j]);
                    sb.append(",");
                }

            }
            if (i >= start)
                sb.append("},");
            //            System.out.println(i + ": " + Arrays.toString(map[i]));
        }
        sb.append("}");
        return sb.toString();
    }

    public static String run(String arg) {
        StringBuilder sb = new StringBuilder();
        char [] romanNumerals = {'I', 'V', 'X', 'L', 'C', 'D', 'M'};
        if (arg.charAt(0) > '9') {
            /**
             * 2) Given a number represented by the Roman numerals, output and
             * print the number it present by using digits. For Example, given
             * input of LXVIII, output 68.
             */
            //            System.out.println("Given a number represented by the Roman numerals");
            int [] romanNumeralsMap = new int[128];
            for (int i = 0, j = 1; i < romanNumerals.length; ++i) {
                romanNumeralsMap[romanNumerals[i]] = j;
                if (i % 2 == 0) {
                    j *= 5;
                } else {
                    j <<= 1;
                }
            }

            int pre = 0;
            int sum = 0;
            boolean isPreProcessed = false;
            int sameCount = 1;
            int cur = 0;
            for (int i = 0, len = arg.length(); i < len; ++i) {
                pre = (i - 1) < 0 ? -1 : arg.charAt(i - 1);
                cur = arg.charAt(i);
                //                System.out.println((char)(cur));
                //avoid Chinese char
                if (cur >= 127 || cur < 0 || romanNumeralsMap[cur] == 0) {
                    System.out.println((char)cur + " is not the legal char----" + arg);
                    return "ILLEGAL";
                }
                switch (cur) {
                    case 'I':
                    case 'X':
                    case 'C':
                    case 'M':
                        if (!isPreProcessed) {
                            if (pre != -1) {
                                if (romanNumeralsMap[pre] == romanNumeralsMap[cur]) {
                                    ++sameCount;
                                    if (sameCount == 4) {
                                        System.out.println("more than 3 " + (char)cur + "----" + arg);
                                        return "ILLEGAL";
                                    }
                                    sum += romanNumeralsMap[pre];
                                    if (i == len - 1) {//this is the last one
                                        sum += romanNumeralsMap[cur];
                                    }
                                } else if (romanNumeralsMap[pre] < romanNumeralsMap[cur]) {
                                    if (romanNumeralsMap[cur] / romanNumeralsMap[pre] == 10) {
                                        isPreProcessed = true;
                                        sum -= romanNumeralsMap[pre];
                                        sum += romanNumeralsMap[cur];
                                        //                                        System.out.println("romanNumeralsMap[cur] / romanNumeralsMap[pre] == 10" + " "
                                        //                                                + sum);
                                    } else {
                                        System.out.println(romanNumeralsMap[cur] + "/" + romanNumeralsMap[pre]
                                                + "!= 10----" + arg);
                                        return "ILLEGAL";
                                    }
                                } else {
                                    sum += romanNumeralsMap[pre];
                                    sameCount = 1;
                                    if (i == len - 1) {//this is the last one
                                        sum += romanNumeralsMap[cur];
                                    }
                                }
                            }
                        } else {
                            isPreProcessed = false;
                            //                            System.out.println("unproccessed " + (char)cur);
                            if (i == len - 1) {//this is the last one
                                sum += romanNumeralsMap[cur];
                            }
                        }

                        break;
                    //always plus
                    case 'V':
                    case 'L':
                    case 'D':
                        if (sameCount == 3) {
                            sameCount = 1;
                        }
                        if (!isPreProcessed) {
                            if (pre != -1) {
                                if (romanNumeralsMap[pre] > romanNumeralsMap[cur]) {
                                    sum += romanNumeralsMap[pre];
                                } else if (romanNumeralsMap[pre] < romanNumeralsMap[cur]) {
                                    if (romanNumeralsMap[cur] / romanNumeralsMap[pre] > 5) {
                                        System.out.println(romanNumeralsMap[cur] + "/" + romanNumeralsMap[pre]
                                                + "> 5----" + arg);
                                        return "ILLEGAL";
                                    }
                                    sum -= romanNumeralsMap[pre];
                                } else {
                                    //can't be equal
                                    System.out.println((char)cur + " can't be equal to " + (char)pre + "----" + arg);
                                    return "ILLEGAL";
                                }
                            }
                            isPreProcessed = true;
                        } else {
                            if (pre != -1) {
                                if (romanNumeralsMap[cur] / romanNumeralsMap[pre] > 5) {
                                    System.out.println(romanNumeralsMap[cur] + "/" + romanNumeralsMap[pre] + "> 5----"
                                            + arg);
                                    return "ILLEGAL";
                                }
                            }
                        }
                        sum += romanNumeralsMap[cur];
                        break;
                }
                //                System.out.println(sum);
            }
            sb.append(sum);
        } else {
            //            System.out.println("Given N (1 <= N < 3500), an integer");
            //            final int digitNum = arg.length();
            final int n = Integer.parseInt(arg);
            /**
             * count the result for each number
             * [0] first element stands for flag, 1 for filled
             * 1)I=1
             * 2)V=5
             * 3)X=10
             * 4)L=50
             * 5)C=100
             * 6)D=500
             * 7)M=1000
             * default value is 0
             * use map[0][] as total result
             */

            int [][] map = new int[n + 1][romanNumerals.length + 1];
            /**
             * M,MM,MMM
             * C,CC,CCC,CD,D,DC,DCC,DCCC,CM
             * X,XX,XXX,XL,L,LX,LXX,LXXX,XC
             * I,II,III,IV,V,VI,VII,VIII,IX
             */
            for (int j = 1, i = 1; j < map.length; i += 2, j *= 10) {
                for (int k = j, c = 1; k < map.length && c < 10; k += j, ++c) {
                    map[k][0] = 1;
                    switch (c) {
                        case 1:
                        case 2:
                        case 3:
                            map[k][i] = c;
                            break;
                        case 4:
                            map[k][i] = 1;
                        case 5:
                            map[k][i + 1] = 1;
                            break;
                        case 6:
                        case 7:
                        case 8:
                            map[k][i + 1] = 1;
                            map[k][i] = c - 5;
                            break;
                        case 9:
                            map[k][i] = 1;
                            map[k][i + 2] = 1;
                            break;
                    }
                    //                    System.out.println(Arrays.toString(map[k]));
                }
            }
            int [] mapForDigit = {1000, 100, 10, 1};
            for (int i = 1; i <= n; ++i) {
                if (map[i][0] > 0) {//for single digit
                    for (int j = 1; j <= romanNumerals.length; ++j) {
                        map[0][j] += map[i][j];
                    }
                } else {
                    //from highest
                    int high = i / mapForDigit[0];
                    int j = 0;
                    while (high == 0) {
                        ++j;
                        high = i / mapForDigit[j];
                    }
                    if (mapForDigit[j] > 1) {
                        high *= mapForDigit[j];
                        int low = i % mapForDigit[j];
                        if (low > 0) {
                            for (j = 1; j <= romanNumerals.length; ++j) {
                                map[i][j] += map[low][j];
                            }
                        }
                    }
                    for (j = 1; j <= romanNumerals.length; ++j) {
                        map[i][j] += map[high][j];
                        map[0][j] += map[i][j];
                    }
                    map[i][0] = 1;
                }
                //for test
                //                if (i == 68) {
                //                    System.out.println(Arrays.toString(map[i]));
                //                }
            }

            //construct answer string
            boolean first = true;
            for (int i = 1; i <= romanNumerals.length; ++i) {
                if (map[0][i] > 0) {
                    if (!first) {
                        sb.append(',');
                    } else {
                        first = false;
                    }
                    sb.append(romanNumerals[i - 1]);
                    sb.append(map[0][i]);
                }
            }
        }
        return sb.toString();
    }

    public static String run2(String arg) {
        char [] romanNumerals = {'I', 'V', 'X', 'L', 'C', 'D', 'M'};
        if (arg.charAt(0) > '9') {
            int [] romanNumeralsMap = new int[128];
            for (int i = 0, j = 1; i < romanNumerals.length; ++i) {
                romanNumeralsMap[romanNumerals[i]] = j;
                if (i % 2 == 0) {
                    j *= 5;
                } else {
                    j <<= 1;
                }
            }

            int pre = 0;
            int sum = 0;
            boolean isPreProcessed = false;
            int sameCount = 1;
            int cur = 0;
            for (int i = 0, len = arg.length(); i < len; ++i) {
                pre = (i - 1) < 0 ? -1 : arg.charAt(i - 1);
                cur = arg.charAt(i);
                //                System.out.println((char)(cur));
                //avoid Chinese char
                if (cur >= 127 || cur < 0 || romanNumeralsMap[cur] == 0) {
                    System.out.println((char)cur + " is not the legal char----" + arg);
                    return "ILLEGAL";
                }
                switch (cur) {
                    case 'I':
                    case 'X':
                    case 'C':
                    case 'M':
                        if (!isPreProcessed) {
                            if (pre != -1) {
                                if (romanNumeralsMap[pre] == romanNumeralsMap[cur]) {
                                    ++sameCount;
                                    if (sameCount == 4) {
                                        //                                        System.out.println("more than 3 " + (char)cur + "----" + arg);
                                        return "ILLEGAL";
                                    }
                                    sum += romanNumeralsMap[pre];
                                    if (i == len - 1) {//this is the last one
                                        sum += romanNumeralsMap[cur];
                                    }
                                } else if (romanNumeralsMap[pre] < romanNumeralsMap[cur]) {
                                    if (romanNumeralsMap[cur] / romanNumeralsMap[pre] == 10) {
                                        isPreProcessed = true;
                                        sum -= romanNumeralsMap[pre];
                                        sum += romanNumeralsMap[cur];
                                        //                                        System.out.println("romanNumeralsMap[cur] / romanNumeralsMap[pre] == 10" + " "
                                        //                                                + sum);
                                    } else {
                                        //                                        System.out.println(romanNumeralsMap[cur] + "/" + romanNumeralsMap[pre]
                                        //                                                + "!= 10----" + arg);
                                        return "ILLEGAL";
                                    }
                                } else {
                                    sum += romanNumeralsMap[pre];
                                    sameCount = 1;
                                    if (i == len - 1) {//this is the last one
                                        sum += romanNumeralsMap[cur];
                                    }
                                }
                            }
                        } else {
                            isPreProcessed = false;
                            //                            System.out.println("unproccessed " + (char)cur);
                            if (i == len - 1) {//this is the last one
                                sum += romanNumeralsMap[cur];
                            }
                        }

                        break;
                    //always plus
                    case 'V':
                    case 'L':
                    case 'D':
                        if (sameCount == 3) {
                            sameCount = 1;
                        }
                        if (!isPreProcessed) {
                            if (pre != -1) {
                                if (romanNumeralsMap[pre] > romanNumeralsMap[cur]) {
                                    sum += romanNumeralsMap[pre];
                                } else if (romanNumeralsMap[pre] < romanNumeralsMap[cur]) {
                                    if (romanNumeralsMap[cur] / romanNumeralsMap[pre] > 5) {
                                        //                                        System.out.println(romanNumeralsMap[cur] + "/" + romanNumeralsMap[pre]
                                        //                                                + "> 5----" + arg);
                                        return "ILLEGAL";
                                    }
                                    sum -= romanNumeralsMap[pre];
                                } else {
                                    //can't be equal
                                    //                                    System.out.println((char)cur + " can't be equal to " + (char)pre + "----" + arg);
                                    return "ILLEGAL";
                                }
                            }
                            isPreProcessed = true;
                        } else {
                            if (pre != -1) {
                                if (romanNumeralsMap[cur] / romanNumeralsMap[pre] > 5) {
                                    //                                    System.out.println(romanNumeralsMap[cur] + "/" + romanNumeralsMap[pre] + "> 5----"
                                    //                                            + arg);
                                    return "ILLEGAL";
                                }
                            }
                        }
                        sum += romanNumeralsMap[cur];
                        break;
                }
                //                System.out.println(sum);
            }
            return String.valueOf(sum);
        } else {
            StringBuilder sb = new StringBuilder();
            //it takes 5 ms?
            int n = -1;
            try {
                n = Integer.parseInt(arg);
            } catch (Exception e) {
                return "ILLEGAL";
            }
            //            final int n=Integer.parseInt(arg);
            boolean first = true;
            int [][] cachedMap = { {2500, 3500, 1250, 3750, 1250, 3750, 1101, 2202,},
                    {2501, 3501, 1250, 3750, 1250, 3750, 1102, 2204,},
                    {2502, 3503, 1250, 3750, 1250, 3750, 1103, 2206,},
                    {2503, 3506, 1250, 3750, 1250, 3750, 1104, 2208,},
                    {2504, 3507, 1251, 3750, 1250, 3750, 1105, 2210,},
                    {2505, 3507, 1252, 3750, 1250, 3750, 1106, 2212,},
                    {2506, 3508, 1253, 3750, 1250, 3750, 1107, 2214,},
                    {2507, 3510, 1254, 3750, 1250, 3750, 1108, 2216,},
                    {2508, 3513, 1255, 3750, 1250, 3750, 1109, 2218,},
                    {2509, 3514, 1255, 3751, 1250, 3750, 1110, 2220,},
                    {2510, 3514, 1255, 3752, 1250, 3750, 1111, 2222,},
                    {2511, 3515, 1255, 3753, 1250, 3750, 1112, 2224,},
                    {2512, 3517, 1255, 3754, 1250, 3750, 1113, 2226,},
                    {2513, 3520, 1255, 3755, 1250, 3750, 1114, 2228,},
                    {2514, 3521, 1256, 3756, 1250, 3750, 1115, 2230,},
                    {2515, 3521, 1257, 3757, 1250, 3750, 1116, 2232,},
                    {2516, 3522, 1258, 3758, 1250, 3750, 1117, 2234,},
                    {2517, 3524, 1259, 3759, 1250, 3750, 1118, 2236,},
                    {2518, 3527, 1260, 3760, 1250, 3750, 1119, 2238,},
                    {2519, 3528, 1260, 3762, 1250, 3750, 1120, 2240,},
                    {2520, 3528, 1260, 3764, 1250, 3750, 1121, 2242,},
                    {2521, 3529, 1260, 3766, 1250, 3750, 1122, 2244,},
                    {2522, 3531, 1260, 3768, 1250, 3750, 1123, 2246,},
                    {2523, 3534, 1260, 3770, 1250, 3750, 1124, 2248,},
                    {2524, 3535, 1261, 3772, 1250, 3750, 1125, 2250,},
                    {2525, 3535, 1262, 3774, 1250, 3750, 1126, 2252,},
                    {2526, 3536, 1263, 3776, 1250, 3750, 1127, 2254,},
                    {2527, 3538, 1264, 3778, 1250, 3750, 1128, 2256,},
                    {2528, 3541, 1265, 3780, 1250, 3750, 1129, 2258,},
                    {2529, 3542, 1265, 3783, 1250, 3750, 1130, 2260,},
                    {2530, 3542, 1265, 3786, 1250, 3750, 1131, 2262,},
                    {2531, 3543, 1265, 3789, 1250, 3750, 1132, 2264,},
                    {2532, 3545, 1265, 3792, 1250, 3750, 1133, 2266,},
                    {2533, 3548, 1265, 3795, 1250, 3750, 1134, 2268,},
                    {2534, 3549, 1266, 3798, 1250, 3750, 1135, 2270,},
                    {2535, 3549, 1267, 3801, 1250, 3750, 1136, 2272,},
                    {2536, 3550, 1268, 3804, 1250, 3750, 1137, 2274,},
                    {2537, 3552, 1269, 3807, 1250, 3750, 1138, 2276,},
                    {2538, 3555, 1270, 3810, 1250, 3750, 1139, 2278,},
                    {2539, 3556, 1270, 3814, 1250, 3750, 1140, 2280,},
                    {2540, 3556, 1270, 3815, 1251, 3750, 1141, 2282,},
                    {2541, 3557, 1270, 3816, 1252, 3750, 1142, 2284,},
                    {2542, 3559, 1270, 3817, 1253, 3750, 1143, 2286,},
                    {2543, 3562, 1270, 3818, 1254, 3750, 1144, 2288,},
                    {2544, 3563, 1271, 3819, 1255, 3750, 1145, 2290,},
                    {2545, 3563, 1272, 3820, 1256, 3750, 1146, 2292,},
                    {2546, 3564, 1273, 3821, 1257, 3750, 1147, 2294,},
                    {2547, 3566, 1274, 3822, 1258, 3750, 1148, 2296,},
                    {2548, 3569, 1275, 3823, 1259, 3750, 1149, 2298,},
                    {2549, 3570, 1275, 3825, 1260, 3750, 1150, 2300,},
                    {2550, 3570, 1275, 3825, 1261, 3750, 1151, 2302,},
                    {2551, 3571, 1275, 3825, 1262, 3750, 1152, 2304,},
                    {2552, 3573, 1275, 3825, 1263, 3750, 1153, 2306,},
                    {2553, 3576, 1275, 3825, 1264, 3750, 1154, 2308,},
                    {2554, 3577, 1276, 3825, 1265, 3750, 1155, 2310,},
                    {2555, 3577, 1277, 3825, 1266, 3750, 1156, 2312,},
                    {2556, 3578, 1278, 3825, 1267, 3750, 1157, 2314,},
                    {2557, 3580, 1279, 3825, 1268, 3750, 1158, 2316,},
                    {2558, 3583, 1280, 3825, 1269, 3750, 1159, 2318,},
                    {2559, 3584, 1280, 3826, 1270, 3750, 1160, 2320,},
                    {2560, 3584, 1280, 3827, 1271, 3750, 1161, 2322,},
                    {2561, 3585, 1280, 3828, 1272, 3750, 1162, 2324,},
                    {2562, 3587, 1280, 3829, 1273, 3750, 1163, 2326,},
                    {2563, 3590, 1280, 3830, 1274, 3750, 1164, 2328,},
                    {2564, 3591, 1281, 3831, 1275, 3750, 1165, 2330,},
                    {2565, 3591, 1282, 3832, 1276, 3750, 1166, 2332,},
                    {2566, 3592, 1283, 3833, 1277, 3750, 1167, 2334,},
                    {2567, 3594, 1284, 3834, 1278, 3750, 1168, 2336,},
                    {2568, 3597, 1285, 3835, 1279, 3750, 1169, 2338,},
                    {2569, 3598, 1285, 3837, 1280, 3750, 1170, 2340,},
                    {2570, 3598, 1285, 3839, 1281, 3750, 1171, 2342,},
                    {2571, 3599, 1285, 3841, 1282, 3750, 1172, 2344,},
                    {2572, 3601, 1285, 3843, 1283, 3750, 1173, 2346,},
                    {2573, 3604, 1285, 3845, 1284, 3750, 1174, 2348,},
                    {2574, 3605, 1286, 3847, 1285, 3750, 1175, 2350,},
                    {2575, 3605, 1287, 3849, 1286, 3750, 1176, 2352,},
                    {2576, 3606, 1288, 3851, 1287, 3750, 1177, 2354,},
                    {2577, 3608, 1289, 3853, 1288, 3750, 1178, 2356,},
                    {2578, 3611, 1290, 3855, 1289, 3750, 1179, 2358,},
                    {2579, 3612, 1290, 3858, 1290, 3750, 1180, 2360,},
                    {2580, 3612, 1290, 3861, 1291, 3750, 1181, 2362,},
                    {2581, 3613, 1290, 3864, 1292, 3750, 1182, 2364,},
                    {2582, 3615, 1290, 3867, 1293, 3750, 1183, 2366,},
                    {2583, 3618, 1290, 3870, 1294, 3750, 1184, 2368,},
                    {2584, 3619, 1291, 3873, 1295, 3750, 1185, 2370,},
                    {2585, 3619, 1292, 3876, 1296, 3750, 1186, 2372,},
                    {2586, 3620, 1293, 3879, 1297, 3750, 1187, 2374,},
                    {2587, 3622, 1294, 3882, 1298, 3750, 1188, 2376,},
                    {2588, 3625, 1295, 3885, 1299, 3750, 1189, 2378,},
                    {2589, 3626, 1295, 3889, 1300, 3750, 1190, 2380,},
                    {2590, 3626, 1295, 3890, 1300, 3751, 1191, 2382,},
                    {2591, 3627, 1295, 3891, 1300, 3752, 1192, 2384,},
                    {2592, 3629, 1295, 3892, 1300, 3753, 1193, 2386,},
                    {2593, 3632, 1295, 3893, 1300, 3754, 1194, 2388,},
                    {2594, 3633, 1296, 3894, 1300, 3755, 1195, 2390,},
                    {2595, 3633, 1297, 3895, 1300, 3756, 1196, 2392,},
                    {2596, 3634, 1298, 3896, 1300, 3757, 1197, 2394,},
                    {2597, 3636, 1299, 3897, 1300, 3758, 1198, 2396,},
                    {2598, 3639, 1300, 3898, 1300, 3759, 1199, 2398,},
                    {2599, 3640, 1300, 3900, 1300, 3760, 1200, 2400,},
                    {2600, 3640, 1300, 3900, 1300, 3761, 1201, 2402,},
                    {2601, 3641, 1300, 3900, 1300, 3762, 1202, 2404,},
                    {2602, 3643, 1300, 3900, 1300, 3763, 1203, 2406,},
                    {2603, 3646, 1300, 3900, 1300, 3764, 1204, 2408,},
                    {2604, 3647, 1301, 3900, 1300, 3765, 1205, 2410,},
                    {2605, 3647, 1302, 3900, 1300, 3766, 1206, 2412,},
                    {2606, 3648, 1303, 3900, 1300, 3767, 1207, 2414,},
                    {2607, 3650, 1304, 3900, 1300, 3768, 1208, 2416,},
                    {2608, 3653, 1305, 3900, 1300, 3769, 1209, 2418,},
                    {2609, 3654, 1305, 3901, 1300, 3770, 1210, 2420,},
                    {2610, 3654, 1305, 3902, 1300, 3771, 1211, 2422,},
                    {2611, 3655, 1305, 3903, 1300, 3772, 1212, 2424,},
                    {2612, 3657, 1305, 3904, 1300, 3773, 1213, 2426,},
                    {2613, 3660, 1305, 3905, 1300, 3774, 1214, 2428,},
                    {2614, 3661, 1306, 3906, 1300, 3775, 1215, 2430,},
                    {2615, 3661, 1307, 3907, 1300, 3776, 1216, 2432,},
                    {2616, 3662, 1308, 3908, 1300, 3777, 1217, 2434,},
                    {2617, 3664, 1309, 3909, 1300, 3778, 1218, 2436,},
                    {2618, 3667, 1310, 3910, 1300, 3779, 1219, 2438,},
                    {2619, 3668, 1310, 3912, 1300, 3780, 1220, 2440,},
                    {2620, 3668, 1310, 3914, 1300, 3781, 1221, 2442,},
                    {2621, 3669, 1310, 3916, 1300, 3782, 1222, 2444,},
                    {2622, 3671, 1310, 3918, 1300, 3783, 1223, 2446,},
                    {2623, 3674, 1310, 3920, 1300, 3784, 1224, 2448,},
                    {2624, 3675, 1311, 3922, 1300, 3785, 1225, 2450,},
                    {2625, 3675, 1312, 3924, 1300, 3786, 1226, 2452,},
                    {2626, 3676, 1313, 3926, 1300, 3787, 1227, 2454,},
                    {2627, 3678, 1314, 3928, 1300, 3788, 1228, 2456,},
                    {2628, 3681, 1315, 3930, 1300, 3789, 1229, 2458,},
                    {2629, 3682, 1315, 3933, 1300, 3790, 1230, 2460,},
                    {2630, 3682, 1315, 3936, 1300, 3791, 1231, 2462,},
                    {2631, 3683, 1315, 3939, 1300, 3792, 1232, 2464,},
                    {2632, 3685, 1315, 3942, 1300, 3793, 1233, 2466,},
                    {2633, 3688, 1315, 3945, 1300, 3794, 1234, 2468,},
                    {2634, 3689, 1316, 3948, 1300, 3795, 1235, 2470,},
                    {2635, 3689, 1317, 3951, 1300, 3796, 1236, 2472,},
                    {2636, 3690, 1318, 3954, 1300, 3797, 1237, 2474,},
                    {2637, 3692, 1319, 3957, 1300, 3798, 1238, 2476,},
                    {2638, 3695, 1320, 3960, 1300, 3799, 1239, 2478,},
                    {2639, 3696, 1320, 3964, 1300, 3800, 1240, 2480,},
                    {2640, 3696, 1320, 3965, 1301, 3801, 1241, 2482,},
                    {2641, 3697, 1320, 3966, 1302, 3802, 1242, 2484,},
                    {2642, 3699, 1320, 3967, 1303, 3803, 1243, 2486,},
                    {2643, 3702, 1320, 3968, 1304, 3804, 1244, 2488,},
                    {2644, 3703, 1321, 3969, 1305, 3805, 1245, 2490,},
                    {2645, 3703, 1322, 3970, 1306, 3806, 1246, 2492,},
                    {2646, 3704, 1323, 3971, 1307, 3807, 1247, 2494,},
                    {2647, 3706, 1324, 3972, 1308, 3808, 1248, 2496,},
                    {2648, 3709, 1325, 3973, 1309, 3809, 1249, 2498,},
                    {2649, 3710, 1325, 3975, 1310, 3810, 1250, 2500,},
                    {2650, 3710, 1325, 3975, 1311, 3811, 1251, 2502,},
                    {2651, 3711, 1325, 3975, 1312, 3812, 1252, 2504,},
                    {2652, 3713, 1325, 3975, 1313, 3813, 1253, 2506,},
                    {2653, 3716, 1325, 3975, 1314, 3814, 1254, 2508,},
                    {2654, 3717, 1326, 3975, 1315, 3815, 1255, 2510,},
                    {2655, 3717, 1327, 3975, 1316, 3816, 1256, 2512,},
                    {2656, 3718, 1328, 3975, 1317, 3817, 1257, 2514,},
                    {2657, 3720, 1329, 3975, 1318, 3818, 1258, 2516,},
                    {2658, 3723, 1330, 3975, 1319, 3819, 1259, 2518,},
                    {2659, 3724, 1330, 3976, 1320, 3820, 1260, 2520,},
                    {2660, 3724, 1330, 3977, 1321, 3821, 1261, 2522,},
                    {2661, 3725, 1330, 3978, 1322, 3822, 1262, 2524,},
                    {2662, 3727, 1330, 3979, 1323, 3823, 1263, 2526,},
                    {2663, 3730, 1330, 3980, 1324, 3824, 1264, 2528,},
                    {2664, 3731, 1331, 3981, 1325, 3825, 1265, 2530,},
                    {2665, 3731, 1332, 3982, 1326, 3826, 1266, 2532,},
                    {2666, 3732, 1333, 3983, 1327, 3827, 1267, 2534,},
                    {2667, 3734, 1334, 3984, 1328, 3828, 1268, 2536,},
                    {2668, 3737, 1335, 3985, 1329, 3829, 1269, 2538,},
                    {2669, 3738, 1335, 3987, 1330, 3830, 1270, 2540,},
                    {2670, 3738, 1335, 3989, 1331, 3831, 1271, 2542,},
                    {2671, 3739, 1335, 3991, 1332, 3832, 1272, 2544,},
                    {2672, 3741, 1335, 3993, 1333, 3833, 1273, 2546,},
                    {2673, 3744, 1335, 3995, 1334, 3834, 1274, 2548,},
                    {2674, 3745, 1336, 3997, 1335, 3835, 1275, 2550,},
                    {2675, 3745, 1337, 3999, 1336, 3836, 1276, 2552,},
                    {2676, 3746, 1338, 4001, 1337, 3837, 1277, 2554,},
                    {2677, 3748, 1339, 4003, 1338, 3838, 1278, 2556,},
                    {2678, 3751, 1340, 4005, 1339, 3839, 1279, 2558,},
                    {2679, 3752, 1340, 4008, 1340, 3840, 1280, 2560,},
                    {2680, 3752, 1340, 4011, 1341, 3841, 1281, 2562,},
                    {2681, 3753, 1340, 4014, 1342, 3842, 1282, 2564,},
                    {2682, 3755, 1340, 4017, 1343, 3843, 1283, 2566,},
                    {2683, 3758, 1340, 4020, 1344, 3844, 1284, 2568,},
                    {2684, 3759, 1341, 4023, 1345, 3845, 1285, 2570,},
                    {2685, 3759, 1342, 4026, 1346, 3846, 1286, 2572,},
                    {2686, 3760, 1343, 4029, 1347, 3847, 1287, 2574,},
                    {2687, 3762, 1344, 4032, 1348, 3848, 1288, 2576,},
                    {2688, 3765, 1345, 4035, 1349, 3849, 1289, 2578,},
                    {2689, 3766, 1345, 4039, 1350, 3850, 1290, 2580,},
                    {2690, 3766, 1345, 4040, 1350, 3852, 1291, 2582,},
                    {2691, 3767, 1345, 4041, 1350, 3854, 1292, 2584,},
                    {2692, 3769, 1345, 4042, 1350, 3856, 1293, 2586,},
                    {2693, 3772, 1345, 4043, 1350, 3858, 1294, 2588,},
                    {2694, 3773, 1346, 4044, 1350, 3860, 1295, 2590,},
                    {2695, 3773, 1347, 4045, 1350, 3862, 1296, 2592,},
                    {2696, 3774, 1348, 4046, 1350, 3864, 1297, 2594,},
                    {2697, 3776, 1349, 4047, 1350, 3866, 1298, 2596,},
                    {2698, 3779, 1350, 4048, 1350, 3868, 1299, 2598,},
                    {2699, 3780, 1350, 4050, 1350, 3870, 1300, 2600,},
                    {2700, 3780, 1350, 4050, 1350, 3872, 1301, 2602,},
                    {2701, 3781, 1350, 4050, 1350, 3874, 1302, 2604,},
                    {2702, 3783, 1350, 4050, 1350, 3876, 1303, 2606,},
                    {2703, 3786, 1350, 4050, 1350, 3878, 1304, 2608,},
                    {2704, 3787, 1351, 4050, 1350, 3880, 1305, 2610,},
                    {2705, 3787, 1352, 4050, 1350, 3882, 1306, 2612,},
                    {2706, 3788, 1353, 4050, 1350, 3884, 1307, 2614,},
                    {2707, 3790, 1354, 4050, 1350, 3886, 1308, 2616,},
                    {2708, 3793, 1355, 4050, 1350, 3888, 1309, 2618,},
                    {2709, 3794, 1355, 4051, 1350, 3890, 1310, 2620,},
                    {2710, 3794, 1355, 4052, 1350, 3892, 1311, 2622,},
                    {2711, 3795, 1355, 4053, 1350, 3894, 1312, 2624,},
                    {2712, 3797, 1355, 4054, 1350, 3896, 1313, 2626,},
                    {2713, 3800, 1355, 4055, 1350, 3898, 1314, 2628,},
                    {2714, 3801, 1356, 4056, 1350, 3900, 1315, 2630,},
                    {2715, 3801, 1357, 4057, 1350, 3902, 1316, 2632,},
                    {2716, 3802, 1358, 4058, 1350, 3904, 1317, 2634,},
                    {2717, 3804, 1359, 4059, 1350, 3906, 1318, 2636,},
                    {2718, 3807, 1360, 4060, 1350, 3908, 1319, 2638,},
                    {2719, 3808, 1360, 4062, 1350, 3910, 1320, 2640,},
                    {2720, 3808, 1360, 4064, 1350, 3912, 1321, 2642,},
                    {2721, 3809, 1360, 4066, 1350, 3914, 1322, 2644,},
                    {2722, 3811, 1360, 4068, 1350, 3916, 1323, 2646,},
                    {2723, 3814, 1360, 4070, 1350, 3918, 1324, 2648,},
                    {2724, 3815, 1361, 4072, 1350, 3920, 1325, 2650,},
                    {2725, 3815, 1362, 4074, 1350, 3922, 1326, 2652,},
                    {2726, 3816, 1363, 4076, 1350, 3924, 1327, 2654,},
                    {2727, 3818, 1364, 4078, 1350, 3926, 1328, 2656,},
                    {2728, 3821, 1365, 4080, 1350, 3928, 1329, 2658,},
                    {2729, 3822, 1365, 4083, 1350, 3930, 1330, 2660,},
                    {2730, 3822, 1365, 4086, 1350, 3932, 1331, 2662,},
                    {2731, 3823, 1365, 4089, 1350, 3934, 1332, 2664,},
                    {2732, 3825, 1365, 4092, 1350, 3936, 1333, 2666,},
                    {2733, 3828, 1365, 4095, 1350, 3938, 1334, 2668,},
                    {2734, 3829, 1366, 4098, 1350, 3940, 1335, 2670,},
                    {2735, 3829, 1367, 4101, 1350, 3942, 1336, 2672,},
                    {2736, 3830, 1368, 4104, 1350, 3944, 1337, 2674,},
                    {2737, 3832, 1369, 4107, 1350, 3946, 1338, 2676,},
                    {2738, 3835, 1370, 4110, 1350, 3948, 1339, 2678,},
                    {2739, 3836, 1370, 4114, 1350, 3950, 1340, 2680,},
                    {2740, 3836, 1370, 4115, 1351, 3952, 1341, 2682,},
                    {2741, 3837, 1370, 4116, 1352, 3954, 1342, 2684,},
                    {2742, 3839, 1370, 4117, 1353, 3956, 1343, 2686,},
                    {2743, 3842, 1370, 4118, 1354, 3958, 1344, 2688,},
                    {2744, 3843, 1371, 4119, 1355, 3960, 1345, 2690,},
                    {2745, 3843, 1372, 4120, 1356, 3962, 1346, 2692,},
                    {2746, 3844, 1373, 4121, 1357, 3964, 1347, 2694,},
                    {2747, 3846, 1374, 4122, 1358, 3966, 1348, 2696,},
                    {2748, 3849, 1375, 4123, 1359, 3968, 1349, 2698,},
                    {2749, 3850, 1375, 4125, 1360, 3970, 1350, 2700,},
                    {2750, 3850, 1375, 4125, 1361, 3972, 1351, 2702,},
                    {2751, 3851, 1375, 4125, 1362, 3974, 1352, 2704,},
                    {2752, 3853, 1375, 4125, 1363, 3976, 1353, 2706,},
                    {2753, 3856, 1375, 4125, 1364, 3978, 1354, 2708,},
                    {2754, 3857, 1376, 4125, 1365, 3980, 1355, 2710,},
                    {2755, 3857, 1377, 4125, 1366, 3982, 1356, 2712,},
                    {2756, 3858, 1378, 4125, 1367, 3984, 1357, 2714,},
                    {2757, 3860, 1379, 4125, 1368, 3986, 1358, 2716,},
                    {2758, 3863, 1380, 4125, 1369, 3988, 1359, 2718,},
                    {2759, 3864, 1380, 4126, 1370, 3990, 1360, 2720,},
                    {2760, 3864, 1380, 4127, 1371, 3992, 1361, 2722,},
                    {2761, 3865, 1380, 4128, 1372, 3994, 1362, 2724,},
                    {2762, 3867, 1380, 4129, 1373, 3996, 1363, 2726,},
                    {2763, 3870, 1380, 4130, 1374, 3998, 1364, 2728,},
                    {2764, 3871, 1381, 4131, 1375, 4000, 1365, 2730,},
                    {2765, 3871, 1382, 4132, 1376, 4002, 1366, 2732,},
                    {2766, 3872, 1383, 4133, 1377, 4004, 1367, 2734,},
                    {2767, 3874, 1384, 4134, 1378, 4006, 1368, 2736,},
                    {2768, 3877, 1385, 4135, 1379, 4008, 1369, 2738,},
                    {2769, 3878, 1385, 4137, 1380, 4010, 1370, 2740,},
                    {2770, 3878, 1385, 4139, 1381, 4012, 1371, 2742,},
                    {2771, 3879, 1385, 4141, 1382, 4014, 1372, 2744,},
                    {2772, 3881, 1385, 4143, 1383, 4016, 1373, 2746,},
                    {2773, 3884, 1385, 4145, 1384, 4018, 1374, 2748,},
                    {2774, 3885, 1386, 4147, 1385, 4020, 1375, 2750,},
                    {2775, 3885, 1387, 4149, 1386, 4022, 1376, 2752,},
                    {2776, 3886, 1388, 4151, 1387, 4024, 1377, 2754,},
                    {2777, 3888, 1389, 4153, 1388, 4026, 1378, 2756,},
                    {2778, 3891, 1390, 4155, 1389, 4028, 1379, 2758,},
                    {2779, 3892, 1390, 4158, 1390, 4030, 1380, 2760,},
                    {2780, 3892, 1390, 4161, 1391, 4032, 1381, 2762,},
                    {2781, 3893, 1390, 4164, 1392, 4034, 1382, 2764,},
                    {2782, 3895, 1390, 4167, 1393, 4036, 1383, 2766,},
                    {2783, 3898, 1390, 4170, 1394, 4038, 1384, 2768,},
                    {2784, 3899, 1391, 4173, 1395, 4040, 1385, 2770,},
                    {2785, 3899, 1392, 4176, 1396, 4042, 1386, 2772,},
                    {2786, 3900, 1393, 4179, 1397, 4044, 1387, 2774,},
                    {2787, 3902, 1394, 4182, 1398, 4046, 1388, 2776,},
                    {2788, 3905, 1395, 4185, 1399, 4048, 1389, 2778,},
                    {2789, 3906, 1395, 4189, 1400, 4050, 1390, 2780,},
                    {2790, 3906, 1395, 4190, 1400, 4053, 1391, 2782,},
                    {2791, 3907, 1395, 4191, 1400, 4056, 1392, 2784,},
                    {2792, 3909, 1395, 4192, 1400, 4059, 1393, 2786,},
                    {2793, 3912, 1395, 4193, 1400, 4062, 1394, 2788,},
                    {2794, 3913, 1396, 4194, 1400, 4065, 1395, 2790,},
                    {2795, 3913, 1397, 4195, 1400, 4068, 1396, 2792,},
                    {2796, 3914, 1398, 4196, 1400, 4071, 1397, 2794,},
                    {2797, 3916, 1399, 4197, 1400, 4074, 1398, 2796,},
                    {2798, 3919, 1400, 4198, 1400, 4077, 1399, 2798,},
                    {2799, 3920, 1400, 4200, 1400, 4080, 1400, 2800,},
                    {2800, 3920, 1400, 4200, 1400, 4083, 1401, 2802,},
                    {2801, 3921, 1400, 4200, 1400, 4086, 1402, 2804,},
                    {2802, 3923, 1400, 4200, 1400, 4089, 1403, 2806,},
                    {2803, 3926, 1400, 4200, 1400, 4092, 1404, 2808,},
                    {2804, 3927, 1401, 4200, 1400, 4095, 1405, 2810,},
                    {2805, 3927, 1402, 4200, 1400, 4098, 1406, 2812,},
                    {2806, 3928, 1403, 4200, 1400, 4101, 1407, 2814,},
                    {2807, 3930, 1404, 4200, 1400, 4104, 1408, 2816,},
                    {2808, 3933, 1405, 4200, 1400, 4107, 1409, 2818,},
                    {2809, 3934, 1405, 4201, 1400, 4110, 1410, 2820,},
                    {2810, 3934, 1405, 4202, 1400, 4113, 1411, 2822,},
                    {2811, 3935, 1405, 4203, 1400, 4116, 1412, 2824,},
                    {2812, 3937, 1405, 4204, 1400, 4119, 1413, 2826,},
                    {2813, 3940, 1405, 4205, 1400, 4122, 1414, 2828,},
                    {2814, 3941, 1406, 4206, 1400, 4125, 1415, 2830,},
                    {2815, 3941, 1407, 4207, 1400, 4128, 1416, 2832,},
                    {2816, 3942, 1408, 4208, 1400, 4131, 1417, 2834,},
                    {2817, 3944, 1409, 4209, 1400, 4134, 1418, 2836,},
                    {2818, 3947, 1410, 4210, 1400, 4137, 1419, 2838,},
                    {2819, 3948, 1410, 4212, 1400, 4140, 1420, 2840,},
                    {2820, 3948, 1410, 4214, 1400, 4143, 1421, 2842,},
                    {2821, 3949, 1410, 4216, 1400, 4146, 1422, 2844,},
                    {2822, 3951, 1410, 4218, 1400, 4149, 1423, 2846,},
                    {2823, 3954, 1410, 4220, 1400, 4152, 1424, 2848,},
                    {2824, 3955, 1411, 4222, 1400, 4155, 1425, 2850,},
                    {2825, 3955, 1412, 4224, 1400, 4158, 1426, 2852,},
                    {2826, 3956, 1413, 4226, 1400, 4161, 1427, 2854,},
                    {2827, 3958, 1414, 4228, 1400, 4164, 1428, 2856,},
                    {2828, 3961, 1415, 4230, 1400, 4167, 1429, 2858,},
                    {2829, 3962, 1415, 4233, 1400, 4170, 1430, 2860,},
                    {2830, 3962, 1415, 4236, 1400, 4173, 1431, 2862,},
                    {2831, 3963, 1415, 4239, 1400, 4176, 1432, 2864,},
                    {2832, 3965, 1415, 4242, 1400, 4179, 1433, 2866,},
                    {2833, 3968, 1415, 4245, 1400, 4182, 1434, 2868,},
                    {2834, 3969, 1416, 4248, 1400, 4185, 1435, 2870,},
                    {2835, 3969, 1417, 4251, 1400, 4188, 1436, 2872,},
                    {2836, 3970, 1418, 4254, 1400, 4191, 1437, 2874,},
                    {2837, 3972, 1419, 4257, 1400, 4194, 1438, 2876,},
                    {2838, 3975, 1420, 4260, 1400, 4197, 1439, 2878,},
                    {2839, 3976, 1420, 4264, 1400, 4200, 1440, 2880,},
                    {2840, 3976, 1420, 4265, 1401, 4203, 1441, 2882,},
                    {2841, 3977, 1420, 4266, 1402, 4206, 1442, 2884,},
                    {2842, 3979, 1420, 4267, 1403, 4209, 1443, 2886,},
                    {2843, 3982, 1420, 4268, 1404, 4212, 1444, 2888,},
                    {2844, 3983, 1421, 4269, 1405, 4215, 1445, 2890,},
                    {2845, 3983, 1422, 4270, 1406, 4218, 1446, 2892,},
                    {2846, 3984, 1423, 4271, 1407, 4221, 1447, 2894,},
                    {2847, 3986, 1424, 4272, 1408, 4224, 1448, 2896,},
                    {2848, 3989, 1425, 4273, 1409, 4227, 1449, 2898,},
                    {2849, 3990, 1425, 4275, 1410, 4230, 1450, 2900,},
                    {2850, 3990, 1425, 4275, 1411, 4233, 1451, 2902,},
                    {2851, 3991, 1425, 4275, 1412, 4236, 1452, 2904,},
                    {2852, 3993, 1425, 4275, 1413, 4239, 1453, 2906,},
                    {2853, 3996, 1425, 4275, 1414, 4242, 1454, 2908,},
                    {2854, 3997, 1426, 4275, 1415, 4245, 1455, 2910,},
                    {2855, 3997, 1427, 4275, 1416, 4248, 1456, 2912,},
                    {2856, 3998, 1428, 4275, 1417, 4251, 1457, 2914,},
                    {2857, 4000, 1429, 4275, 1418, 4254, 1458, 2916,},
                    {2858, 4003, 1430, 4275, 1419, 4257, 1459, 2918,},
                    {2859, 4004, 1430, 4276, 1420, 4260, 1460, 2920,},
                    {2860, 4004, 1430, 4277, 1421, 4263, 1461, 2922,},
                    {2861, 4005, 1430, 4278, 1422, 4266, 1462, 2924,},
                    {2862, 4007, 1430, 4279, 1423, 4269, 1463, 2926,},
                    {2863, 4010, 1430, 4280, 1424, 4272, 1464, 2928,},
                    {2864, 4011, 1431, 4281, 1425, 4275, 1465, 2930,},
                    {2865, 4011, 1432, 4282, 1426, 4278, 1466, 2932,},
                    {2866, 4012, 1433, 4283, 1427, 4281, 1467, 2934,},
                    {2867, 4014, 1434, 4284, 1428, 4284, 1468, 2936,},
                    {2868, 4017, 1435, 4285, 1429, 4287, 1469, 2938,},
                    {2869, 4018, 1435, 4287, 1430, 4290, 1470, 2940,},
                    {2870, 4018, 1435, 4289, 1431, 4293, 1471, 2942,},
                    {2871, 4019, 1435, 4291, 1432, 4296, 1472, 2944,},
                    {2872, 4021, 1435, 4293, 1433, 4299, 1473, 2946,},
                    {2873, 4024, 1435, 4295, 1434, 4302, 1474, 2948,},
                    {2874, 4025, 1436, 4297, 1435, 4305, 1475, 2950,},
                    {2875, 4025, 1437, 4299, 1436, 4308, 1476, 2952,},
                    {2876, 4026, 1438, 4301, 1437, 4311, 1477, 2954,},
                    {2877, 4028, 1439, 4303, 1438, 4314, 1478, 2956,},
                    {2878, 4031, 1440, 4305, 1439, 4317, 1479, 2958,},
                    {2879, 4032, 1440, 4308, 1440, 4320, 1480, 2960,},
                    {2880, 4032, 1440, 4311, 1441, 4323, 1481, 2962,},
                    {2881, 4033, 1440, 4314, 1442, 4326, 1482, 2964,},
                    {2882, 4035, 1440, 4317, 1443, 4329, 1483, 2966,},
                    {2883, 4038, 1440, 4320, 1444, 4332, 1484, 2968,},
                    {2884, 4039, 1441, 4323, 1445, 4335, 1485, 2970,},
                    {2885, 4039, 1442, 4326, 1446, 4338, 1486, 2972,},
                    {2886, 4040, 1443, 4329, 1447, 4341, 1487, 2974,},
                    {2887, 4042, 1444, 4332, 1448, 4344, 1488, 2976,},
                    {2888, 4045, 1445, 4335, 1449, 4347, 1489, 2978,},
                    {2889, 4046, 1445, 4339, 1450, 4350, 1490, 2980,},
                    {2890, 4046, 1445, 4340, 1450, 4354, 1491, 2982,},
                    {2891, 4047, 1445, 4341, 1450, 4358, 1492, 2984,},
                    {2892, 4049, 1445, 4342, 1450, 4362, 1493, 2986,},
                    {2893, 4052, 1445, 4343, 1450, 4366, 1494, 2988,},
                    {2894, 4053, 1446, 4344, 1450, 4370, 1495, 2990,},
                    {2895, 4053, 1447, 4345, 1450, 4374, 1496, 2992,},
                    {2896, 4054, 1448, 4346, 1450, 4378, 1497, 2994,},
                    {2897, 4056, 1449, 4347, 1450, 4382, 1498, 2996,},
                    {2898, 4059, 1450, 4348, 1450, 4386, 1499, 2998,},
                    {2899, 4060, 1450, 4350, 1450, 4390, 1500, 3000,},
                    {2900, 4060, 1450, 4350, 1450, 4391, 1500, 3003,},
                    {2901, 4061, 1450, 4350, 1450, 4392, 1500, 3006,},
                    {2902, 4063, 1450, 4350, 1450, 4393, 1500, 3009,},
                    {2903, 4066, 1450, 4350, 1450, 4394, 1500, 3012,},
                    {2904, 4067, 1451, 4350, 1450, 4395, 1500, 3015,},
                    {2905, 4067, 1452, 4350, 1450, 4396, 1500, 3018,},
                    {2906, 4068, 1453, 4350, 1450, 4397, 1500, 3021,},
                    {2907, 4070, 1454, 4350, 1450, 4398, 1500, 3024,},
                    {2908, 4073, 1455, 4350, 1450, 4399, 1500, 3027,},
                    {2909, 4074, 1455, 4351, 1450, 4400, 1500, 3030,},
                    {2910, 4074, 1455, 4352, 1450, 4401, 1500, 3033,},
                    {2911, 4075, 1455, 4353, 1450, 4402, 1500, 3036,},
                    {2912, 4077, 1455, 4354, 1450, 4403, 1500, 3039,},
                    {2913, 4080, 1455, 4355, 1450, 4404, 1500, 3042,},
                    {2914, 4081, 1456, 4356, 1450, 4405, 1500, 3045,},
                    {2915, 4081, 1457, 4357, 1450, 4406, 1500, 3048,},
                    {2916, 4082, 1458, 4358, 1450, 4407, 1500, 3051,},
                    {2917, 4084, 1459, 4359, 1450, 4408, 1500, 3054,},
                    {2918, 4087, 1460, 4360, 1450, 4409, 1500, 3057,},
                    {2919, 4088, 1460, 4362, 1450, 4410, 1500, 3060,},
                    {2920, 4088, 1460, 4364, 1450, 4411, 1500, 3063,},
                    {2921, 4089, 1460, 4366, 1450, 4412, 1500, 3066,},
                    {2922, 4091, 1460, 4368, 1450, 4413, 1500, 3069,},
                    {2923, 4094, 1460, 4370, 1450, 4414, 1500, 3072,},
                    {2924, 4095, 1461, 4372, 1450, 4415, 1500, 3075,},
                    {2925, 4095, 1462, 4374, 1450, 4416, 1500, 3078,},
                    {2926, 4096, 1463, 4376, 1450, 4417, 1500, 3081,},
                    {2927, 4098, 1464, 4378, 1450, 4418, 1500, 3084,},
                    {2928, 4101, 1465, 4380, 1450, 4419, 1500, 3087,},
                    {2929, 4102, 1465, 4383, 1450, 4420, 1500, 3090,},
                    {2930, 4102, 1465, 4386, 1450, 4421, 1500, 3093,},
                    {2931, 4103, 1465, 4389, 1450, 4422, 1500, 3096,},
                    {2932, 4105, 1465, 4392, 1450, 4423, 1500, 3099,},
                    {2933, 4108, 1465, 4395, 1450, 4424, 1500, 3102,},
                    {2934, 4109, 1466, 4398, 1450, 4425, 1500, 3105,},
                    {2935, 4109, 1467, 4401, 1450, 4426, 1500, 3108,},
                    {2936, 4110, 1468, 4404, 1450, 4427, 1500, 3111,},
                    {2937, 4112, 1469, 4407, 1450, 4428, 1500, 3114,},
                    {2938, 4115, 1470, 4410, 1450, 4429, 1500, 3117,},
                    {2939, 4116, 1470, 4414, 1450, 4430, 1500, 3120,},
                    {2940, 4116, 1470, 4415, 1451, 4431, 1500, 3123,},
                    {2941, 4117, 1470, 4416, 1452, 4432, 1500, 3126,},
                    {2942, 4119, 1470, 4417, 1453, 4433, 1500, 3129,},
                    {2943, 4122, 1470, 4418, 1454, 4434, 1500, 3132,},
                    {2944, 4123, 1471, 4419, 1455, 4435, 1500, 3135,},
                    {2945, 4123, 1472, 4420, 1456, 4436, 1500, 3138,},
                    {2946, 4124, 1473, 4421, 1457, 4437, 1500, 3141,},
                    {2947, 4126, 1474, 4422, 1458, 4438, 1500, 3144,},
                    {2948, 4129, 1475, 4423, 1459, 4439, 1500, 3147,},
                    {2949, 4130, 1475, 4425, 1460, 4440, 1500, 3150,},
                    {2950, 4130, 1475, 4425, 1461, 4441, 1500, 3153,},
                    {2951, 4131, 1475, 4425, 1462, 4442, 1500, 3156,},
                    {2952, 4133, 1475, 4425, 1463, 4443, 1500, 3159,},
                    {2953, 4136, 1475, 4425, 1464, 4444, 1500, 3162,},
                    {2954, 4137, 1476, 4425, 1465, 4445, 1500, 3165,},
                    {2955, 4137, 1477, 4425, 1466, 4446, 1500, 3168,},
                    {2956, 4138, 1478, 4425, 1467, 4447, 1500, 3171,},
                    {2957, 4140, 1479, 4425, 1468, 4448, 1500, 3174,},
                    {2958, 4143, 1480, 4425, 1469, 4449, 1500, 3177,},
                    {2959, 4144, 1480, 4426, 1470, 4450, 1500, 3180,},
                    {2960, 4144, 1480, 4427, 1471, 4451, 1500, 3183,},
                    {2961, 4145, 1480, 4428, 1472, 4452, 1500, 3186,},
                    {2962, 4147, 1480, 4429, 1473, 4453, 1500, 3189,},
                    {2963, 4150, 1480, 4430, 1474, 4454, 1500, 3192,},
                    {2964, 4151, 1481, 4431, 1475, 4455, 1500, 3195,},
                    {2965, 4151, 1482, 4432, 1476, 4456, 1500, 3198,},
                    {2966, 4152, 1483, 4433, 1477, 4457, 1500, 3201,},
                    {2967, 4154, 1484, 4434, 1478, 4458, 1500, 3204,},
                    {2968, 4157, 1485, 4435, 1479, 4459, 1500, 3207,},
                    {2969, 4158, 1485, 4437, 1480, 4460, 1500, 3210,},
                    {2970, 4158, 1485, 4439, 1481, 4461, 1500, 3213,},
                    {2971, 4159, 1485, 4441, 1482, 4462, 1500, 3216,},
                    {2972, 4161, 1485, 4443, 1483, 4463, 1500, 3219,},
                    {2973, 4164, 1485, 4445, 1484, 4464, 1500, 3222,},
                    {2974, 4165, 1486, 4447, 1485, 4465, 1500, 3225,},
                    {2975, 4165, 1487, 4449, 1486, 4466, 1500, 3228,},
                    {2976, 4166, 1488, 4451, 1487, 4467, 1500, 3231,},
                    {2977, 4168, 1489, 4453, 1488, 4468, 1500, 3234,},
                    {2978, 4171, 1490, 4455, 1489, 4469, 1500, 3237,},
                    {2979, 4172, 1490, 4458, 1490, 4470, 1500, 3240,},
                    {2980, 4172, 1490, 4461, 1491, 4471, 1500, 3243,},
                    {2981, 4173, 1490, 4464, 1492, 4472, 1500, 3246,},
                    {2982, 4175, 1490, 4467, 1493, 4473, 1500, 3249,},
                    {2983, 4178, 1490, 4470, 1494, 4474, 1500, 3252,},
                    {2984, 4179, 1491, 4473, 1495, 4475, 1500, 3255,},
                    {2985, 4179, 1492, 4476, 1496, 4476, 1500, 3258,},
                    {2986, 4180, 1493, 4479, 1497, 4477, 1500, 3261,},
                    {2987, 4182, 1494, 4482, 1498, 4478, 1500, 3264,},
                    {2988, 4185, 1495, 4485, 1499, 4479, 1500, 3267,},
                    {2989, 4186, 1495, 4489, 1500, 4480, 1500, 3270,},
                    {2990, 4186, 1495, 4490, 1500, 4482, 1500, 3273,},
                    {2991, 4187, 1495, 4491, 1500, 4484, 1500, 3276,},
                    {2992, 4189, 1495, 4492, 1500, 4486, 1500, 3279,},
                    {2993, 4192, 1495, 4493, 1500, 4488, 1500, 3282,},
                    {2994, 4193, 1496, 4494, 1500, 4490, 1500, 3285,},
                    {2995, 4193, 1497, 4495, 1500, 4492, 1500, 3288,},
                    {2996, 4194, 1498, 4496, 1500, 4494, 1500, 3291,},
                    {2997, 4196, 1499, 4497, 1500, 4496, 1500, 3294,},
                    {2998, 4199, 1500, 4498, 1500, 4498, 1500, 3297,},
                    {2999, 4200, 1500, 4500, 1500, 4500, 1500, 3300,},
                    {3000, 4200, 1500, 4500, 1500, 4500, 1500, 3303,},
                    {3001, 4201, 1500, 4500, 1500, 4500, 1500, 3306,},
                    {3002, 4203, 1500, 4500, 1500, 4500, 1500, 3309,},
                    {3003, 4206, 1500, 4500, 1500, 4500, 1500, 3312,},
                    {3004, 4207, 1501, 4500, 1500, 4500, 1500, 3315,},
                    {3005, 4207, 1502, 4500, 1500, 4500, 1500, 3318,},
                    {3006, 4208, 1503, 4500, 1500, 4500, 1500, 3321,},
                    {3007, 4210, 1504, 4500, 1500, 4500, 1500, 3324,},
                    {3008, 4213, 1505, 4500, 1500, 4500, 1500, 3327,},
                    {3009, 4214, 1505, 4501, 1500, 4500, 1500, 3330,},
                    {3010, 4214, 1505, 4502, 1500, 4500, 1500, 3333,},
                    {3011, 4215, 1505, 4503, 1500, 4500, 1500, 3336,},
                    {3012, 4217, 1505, 4504, 1500, 4500, 1500, 3339,},
                    {3013, 4220, 1505, 4505, 1500, 4500, 1500, 3342,},
                    {3014, 4221, 1506, 4506, 1500, 4500, 1500, 3345,},
                    {3015, 4221, 1507, 4507, 1500, 4500, 1500, 3348,},
                    {3016, 4222, 1508, 4508, 1500, 4500, 1500, 3351,},
                    {3017, 4224, 1509, 4509, 1500, 4500, 1500, 3354,},
                    {3018, 4227, 1510, 4510, 1500, 4500, 1500, 3357,},
                    {3019, 4228, 1510, 4512, 1500, 4500, 1500, 3360,},
                    {3020, 4228, 1510, 4514, 1500, 4500, 1500, 3363,},
                    {3021, 4229, 1510, 4516, 1500, 4500, 1500, 3366,},
                    {3022, 4231, 1510, 4518, 1500, 4500, 1500, 3369,},
                    {3023, 4234, 1510, 4520, 1500, 4500, 1500, 3372,},
                    {3024, 4235, 1511, 4522, 1500, 4500, 1500, 3375,},
                    {3025, 4235, 1512, 4524, 1500, 4500, 1500, 3378,},
                    {3026, 4236, 1513, 4526, 1500, 4500, 1500, 3381,},
                    {3027, 4238, 1514, 4528, 1500, 4500, 1500, 3384,},
                    {3028, 4241, 1515, 4530, 1500, 4500, 1500, 3387,},
                    {3029, 4242, 1515, 4533, 1500, 4500, 1500, 3390,},
                    {3030, 4242, 1515, 4536, 1500, 4500, 1500, 3393,},
                    {3031, 4243, 1515, 4539, 1500, 4500, 1500, 3396,},
                    {3032, 4245, 1515, 4542, 1500, 4500, 1500, 3399,},
                    {3033, 4248, 1515, 4545, 1500, 4500, 1500, 3402,},
                    {3034, 4249, 1516, 4548, 1500, 4500, 1500, 3405,},
                    {3035, 4249, 1517, 4551, 1500, 4500, 1500, 3408,},
                    {3036, 4250, 1518, 4554, 1500, 4500, 1500, 3411,},
                    {3037, 4252, 1519, 4557, 1500, 4500, 1500, 3414,},
                    {3038, 4255, 1520, 4560, 1500, 4500, 1500, 3417,},
                    {3039, 4256, 1520, 4564, 1500, 4500, 1500, 3420,},
                    {3040, 4256, 1520, 4565, 1501, 4500, 1500, 3423,},
                    {3041, 4257, 1520, 4566, 1502, 4500, 1500, 3426,},
                    {3042, 4259, 1520, 4567, 1503, 4500, 1500, 3429,},
                    {3043, 4262, 1520, 4568, 1504, 4500, 1500, 3432,},
                    {3044, 4263, 1521, 4569, 1505, 4500, 1500, 3435,},
                    {3045, 4263, 1522, 4570, 1506, 4500, 1500, 3438,},
                    {3046, 4264, 1523, 4571, 1507, 4500, 1500, 3441,},
                    {3047, 4266, 1524, 4572, 1508, 4500, 1500, 3444,},
                    {3048, 4269, 1525, 4573, 1509, 4500, 1500, 3447,},
                    {3049, 4270, 1525, 4575, 1510, 4500, 1500, 3450,},
                    {3050, 4270, 1525, 4575, 1511, 4500, 1500, 3453,},
                    {3051, 4271, 1525, 4575, 1512, 4500, 1500, 3456,},
                    {3052, 4273, 1525, 4575, 1513, 4500, 1500, 3459,},
                    {3053, 4276, 1525, 4575, 1514, 4500, 1500, 3462,},
                    {3054, 4277, 1526, 4575, 1515, 4500, 1500, 3465,},
                    {3055, 4277, 1527, 4575, 1516, 4500, 1500, 3468,},
                    {3056, 4278, 1528, 4575, 1517, 4500, 1500, 3471,},
                    {3057, 4280, 1529, 4575, 1518, 4500, 1500, 3474,},
                    {3058, 4283, 1530, 4575, 1519, 4500, 1500, 3477,},
                    {3059, 4284, 1530, 4576, 1520, 4500, 1500, 3480,},
                    {3060, 4284, 1530, 4577, 1521, 4500, 1500, 3483,},
                    {3061, 4285, 1530, 4578, 1522, 4500, 1500, 3486,},
                    {3062, 4287, 1530, 4579, 1523, 4500, 1500, 3489,},
                    {3063, 4290, 1530, 4580, 1524, 4500, 1500, 3492,},
                    {3064, 4291, 1531, 4581, 1525, 4500, 1500, 3495,},
                    {3065, 4291, 1532, 4582, 1526, 4500, 1500, 3498,},
                    {3066, 4292, 1533, 4583, 1527, 4500, 1500, 3501,},
                    {3067, 4294, 1534, 4584, 1528, 4500, 1500, 3504,},
                    {3068, 4297, 1535, 4585, 1529, 4500, 1500, 3507,},
                    {3069, 4298, 1535, 4587, 1530, 4500, 1500, 3510,},
                    {3070, 4298, 1535, 4589, 1531, 4500, 1500, 3513,},
                    {3071, 4299, 1535, 4591, 1532, 4500, 1500, 3516,},
                    {3072, 4301, 1535, 4593, 1533, 4500, 1500, 3519,},
                    {3073, 4304, 1535, 4595, 1534, 4500, 1500, 3522,},
                    {3074, 4305, 1536, 4597, 1535, 4500, 1500, 3525,},
                    {3075, 4305, 1537, 4599, 1536, 4500, 1500, 3528,},
                    {3076, 4306, 1538, 4601, 1537, 4500, 1500, 3531,},
                    {3077, 4308, 1539, 4603, 1538, 4500, 1500, 3534,},
                    {3078, 4311, 1540, 4605, 1539, 4500, 1500, 3537,},
                    {3079, 4312, 1540, 4608, 1540, 4500, 1500, 3540,},
                    {3080, 4312, 1540, 4611, 1541, 4500, 1500, 3543,},
                    {3081, 4313, 1540, 4614, 1542, 4500, 1500, 3546,},
                    {3082, 4315, 1540, 4617, 1543, 4500, 1500, 3549,},
                    {3083, 4318, 1540, 4620, 1544, 4500, 1500, 3552,},
                    {3084, 4319, 1541, 4623, 1545, 4500, 1500, 3555,},
                    {3085, 4319, 1542, 4626, 1546, 4500, 1500, 3558,},
                    {3086, 4320, 1543, 4629, 1547, 4500, 1500, 3561,},
                    {3087, 4322, 1544, 4632, 1548, 4500, 1500, 3564,},
                    {3088, 4325, 1545, 4635, 1549, 4500, 1500, 3567,},
                    {3089, 4326, 1545, 4639, 1550, 4500, 1500, 3570,},
                    {3090, 4326, 1545, 4640, 1550, 4501, 1500, 3573,},
                    {3091, 4327, 1545, 4641, 1550, 4502, 1500, 3576,},
                    {3092, 4329, 1545, 4642, 1550, 4503, 1500, 3579,},
                    {3093, 4332, 1545, 4643, 1550, 4504, 1500, 3582,},
                    {3094, 4333, 1546, 4644, 1550, 4505, 1500, 3585,},
                    {3095, 4333, 1547, 4645, 1550, 4506, 1500, 3588,},
                    {3096, 4334, 1548, 4646, 1550, 4507, 1500, 3591,},
                    {3097, 4336, 1549, 4647, 1550, 4508, 1500, 3594,},
                    {3098, 4339, 1550, 4648, 1550, 4509, 1500, 3597,},
                    {3099, 4340, 1550, 4650, 1550, 4510, 1500, 3600,},
                    {3100, 4340, 1550, 4650, 1550, 4511, 1500, 3603,},
                    {3101, 4341, 1550, 4650, 1550, 4512, 1500, 3606,},
                    {3102, 4343, 1550, 4650, 1550, 4513, 1500, 3609,},
                    {3103, 4346, 1550, 4650, 1550, 4514, 1500, 3612,},
                    {3104, 4347, 1551, 4650, 1550, 4515, 1500, 3615,},
                    {3105, 4347, 1552, 4650, 1550, 4516, 1500, 3618,},
                    {3106, 4348, 1553, 4650, 1550, 4517, 1500, 3621,},
                    {3107, 4350, 1554, 4650, 1550, 4518, 1500, 3624,},
                    {3108, 4353, 1555, 4650, 1550, 4519, 1500, 3627,},
                    {3109, 4354, 1555, 4651, 1550, 4520, 1500, 3630,},
                    {3110, 4354, 1555, 4652, 1550, 4521, 1500, 3633,},
                    {3111, 4355, 1555, 4653, 1550, 4522, 1500, 3636,},
                    {3112, 4357, 1555, 4654, 1550, 4523, 1500, 3639,},
                    {3113, 4360, 1555, 4655, 1550, 4524, 1500, 3642,},
                    {3114, 4361, 1556, 4656, 1550, 4525, 1500, 3645,},
                    {3115, 4361, 1557, 4657, 1550, 4526, 1500, 3648,},
                    {3116, 4362, 1558, 4658, 1550, 4527, 1500, 3651,},
                    {3117, 4364, 1559, 4659, 1550, 4528, 1500, 3654,},
                    {3118, 4367, 1560, 4660, 1550, 4529, 1500, 3657,},
                    {3119, 4368, 1560, 4662, 1550, 4530, 1500, 3660,},
                    {3120, 4368, 1560, 4664, 1550, 4531, 1500, 3663,},
                    {3121, 4369, 1560, 4666, 1550, 4532, 1500, 3666,},
                    {3122, 4371, 1560, 4668, 1550, 4533, 1500, 3669,},
                    {3123, 4374, 1560, 4670, 1550, 4534, 1500, 3672,},
                    {3124, 4375, 1561, 4672, 1550, 4535, 1500, 3675,},
                    {3125, 4375, 1562, 4674, 1550, 4536, 1500, 3678,},
                    {3126, 4376, 1563, 4676, 1550, 4537, 1500, 3681,},
                    {3127, 4378, 1564, 4678, 1550, 4538, 1500, 3684,},
                    {3128, 4381, 1565, 4680, 1550, 4539, 1500, 3687,},
                    {3129, 4382, 1565, 4683, 1550, 4540, 1500, 3690,},
                    {3130, 4382, 1565, 4686, 1550, 4541, 1500, 3693,},
                    {3131, 4383, 1565, 4689, 1550, 4542, 1500, 3696,},
                    {3132, 4385, 1565, 4692, 1550, 4543, 1500, 3699,},
                    {3133, 4388, 1565, 4695, 1550, 4544, 1500, 3702,},
                    {3134, 4389, 1566, 4698, 1550, 4545, 1500, 3705,},
                    {3135, 4389, 1567, 4701, 1550, 4546, 1500, 3708,},
                    {3136, 4390, 1568, 4704, 1550, 4547, 1500, 3711,},
                    {3137, 4392, 1569, 4707, 1550, 4548, 1500, 3714,},
                    {3138, 4395, 1570, 4710, 1550, 4549, 1500, 3717,},
                    {3139, 4396, 1570, 4714, 1550, 4550, 1500, 3720,},
                    {3140, 4396, 1570, 4715, 1551, 4551, 1500, 3723,},
                    {3141, 4397, 1570, 4716, 1552, 4552, 1500, 3726,},
                    {3142, 4399, 1570, 4717, 1553, 4553, 1500, 3729,},
                    {3143, 4402, 1570, 4718, 1554, 4554, 1500, 3732,},
                    {3144, 4403, 1571, 4719, 1555, 4555, 1500, 3735,},
                    {3145, 4403, 1572, 4720, 1556, 4556, 1500, 3738,},
                    {3146, 4404, 1573, 4721, 1557, 4557, 1500, 3741,},
                    {3147, 4406, 1574, 4722, 1558, 4558, 1500, 3744,},
                    {3148, 4409, 1575, 4723, 1559, 4559, 1500, 3747,},
                    {3149, 4410, 1575, 4725, 1560, 4560, 1500, 3750,},
                    {3150, 4410, 1575, 4725, 1561, 4561, 1500, 3753,},
                    {3151, 4411, 1575, 4725, 1562, 4562, 1500, 3756,},
                    {3152, 4413, 1575, 4725, 1563, 4563, 1500, 3759,},
                    {3153, 4416, 1575, 4725, 1564, 4564, 1500, 3762,},
                    {3154, 4417, 1576, 4725, 1565, 4565, 1500, 3765,},
                    {3155, 4417, 1577, 4725, 1566, 4566, 1500, 3768,},
                    {3156, 4418, 1578, 4725, 1567, 4567, 1500, 3771,},
                    {3157, 4420, 1579, 4725, 1568, 4568, 1500, 3774,},
                    {3158, 4423, 1580, 4725, 1569, 4569, 1500, 3777,},
                    {3159, 4424, 1580, 4726, 1570, 4570, 1500, 3780,},
                    {3160, 4424, 1580, 4727, 1571, 4571, 1500, 3783,},
                    {3161, 4425, 1580, 4728, 1572, 4572, 1500, 3786,},
                    {3162, 4427, 1580, 4729, 1573, 4573, 1500, 3789,},
                    {3163, 4430, 1580, 4730, 1574, 4574, 1500, 3792,},
                    {3164, 4431, 1581, 4731, 1575, 4575, 1500, 3795,},
                    {3165, 4431, 1582, 4732, 1576, 4576, 1500, 3798,},
                    {3166, 4432, 1583, 4733, 1577, 4577, 1500, 3801,},
                    {3167, 4434, 1584, 4734, 1578, 4578, 1500, 3804,},
                    {3168, 4437, 1585, 4735, 1579, 4579, 1500, 3807,},
                    {3169, 4438, 1585, 4737, 1580, 4580, 1500, 3810,},
                    {3170, 4438, 1585, 4739, 1581, 4581, 1500, 3813,},
                    {3171, 4439, 1585, 4741, 1582, 4582, 1500, 3816,},
                    {3172, 4441, 1585, 4743, 1583, 4583, 1500, 3819,},
                    {3173, 4444, 1585, 4745, 1584, 4584, 1500, 3822,},
                    {3174, 4445, 1586, 4747, 1585, 4585, 1500, 3825,},
                    {3175, 4445, 1587, 4749, 1586, 4586, 1500, 3828,},
                    {3176, 4446, 1588, 4751, 1587, 4587, 1500, 3831,},
                    {3177, 4448, 1589, 4753, 1588, 4588, 1500, 3834,},
                    {3178, 4451, 1590, 4755, 1589, 4589, 1500, 3837,},
                    {3179, 4452, 1590, 4758, 1590, 4590, 1500, 3840,},
                    {3180, 4452, 1590, 4761, 1591, 4591, 1500, 3843,},
                    {3181, 4453, 1590, 4764, 1592, 4592, 1500, 3846,},
                    {3182, 4455, 1590, 4767, 1593, 4593, 1500, 3849,},
                    {3183, 4458, 1590, 4770, 1594, 4594, 1500, 3852,},
                    {3184, 4459, 1591, 4773, 1595, 4595, 1500, 3855,},
                    {3185, 4459, 1592, 4776, 1596, 4596, 1500, 3858,},
                    {3186, 4460, 1593, 4779, 1597, 4597, 1500, 3861,},
                    {3187, 4462, 1594, 4782, 1598, 4598, 1500, 3864,},
                    {3188, 4465, 1595, 4785, 1599, 4599, 1500, 3867,},
                    {3189, 4466, 1595, 4789, 1600, 4600, 1500, 3870,},
                    {3190, 4466, 1595, 4790, 1600, 4602, 1500, 3873,},
                    {3191, 4467, 1595, 4791, 1600, 4604, 1500, 3876,},
                    {3192, 4469, 1595, 4792, 1600, 4606, 1500, 3879,},
                    {3193, 4472, 1595, 4793, 1600, 4608, 1500, 3882,},
                    {3194, 4473, 1596, 4794, 1600, 4610, 1500, 3885,},
                    {3195, 4473, 1597, 4795, 1600, 4612, 1500, 3888,},
                    {3196, 4474, 1598, 4796, 1600, 4614, 1500, 3891,},
                    {3197, 4476, 1599, 4797, 1600, 4616, 1500, 3894,},
                    {3198, 4479, 1600, 4798, 1600, 4618, 1500, 3897,},
                    {3199, 4480, 1600, 4800, 1600, 4620, 1500, 3900,},
                    {3200, 4480, 1600, 4800, 1600, 4622, 1500, 3903,},
                    {3201, 4481, 1600, 4800, 1600, 4624, 1500, 3906,},
                    {3202, 4483, 1600, 4800, 1600, 4626, 1500, 3909,},
                    {3203, 4486, 1600, 4800, 1600, 4628, 1500, 3912,},
                    {3204, 4487, 1601, 4800, 1600, 4630, 1500, 3915,},
                    {3205, 4487, 1602, 4800, 1600, 4632, 1500, 3918,},
                    {3206, 4488, 1603, 4800, 1600, 4634, 1500, 3921,},
                    {3207, 4490, 1604, 4800, 1600, 4636, 1500, 3924,},
                    {3208, 4493, 1605, 4800, 1600, 4638, 1500, 3927,},
                    {3209, 4494, 1605, 4801, 1600, 4640, 1500, 3930,},
                    {3210, 4494, 1605, 4802, 1600, 4642, 1500, 3933,},
                    {3211, 4495, 1605, 4803, 1600, 4644, 1500, 3936,},
                    {3212, 4497, 1605, 4804, 1600, 4646, 1500, 3939,},
                    {3213, 4500, 1605, 4805, 1600, 4648, 1500, 3942,},
                    {3214, 4501, 1606, 4806, 1600, 4650, 1500, 3945,},
                    {3215, 4501, 1607, 4807, 1600, 4652, 1500, 3948,},
                    {3216, 4502, 1608, 4808, 1600, 4654, 1500, 3951,},
                    {3217, 4504, 1609, 4809, 1600, 4656, 1500, 3954,},
                    {3218, 4507, 1610, 4810, 1600, 4658, 1500, 3957,},
                    {3219, 4508, 1610, 4812, 1600, 4660, 1500, 3960,},
                    {3220, 4508, 1610, 4814, 1600, 4662, 1500, 3963,},
                    {3221, 4509, 1610, 4816, 1600, 4664, 1500, 3966,},
                    {3222, 4511, 1610, 4818, 1600, 4666, 1500, 3969,},
                    {3223, 4514, 1610, 4820, 1600, 4668, 1500, 3972,},
                    {3224, 4515, 1611, 4822, 1600, 4670, 1500, 3975,},
                    {3225, 4515, 1612, 4824, 1600, 4672, 1500, 3978,},
                    {3226, 4516, 1613, 4826, 1600, 4674, 1500, 3981,},
                    {3227, 4518, 1614, 4828, 1600, 4676, 1500, 3984,},
                    {3228, 4521, 1615, 4830, 1600, 4678, 1500, 3987,},
                    {3229, 4522, 1615, 4833, 1600, 4680, 1500, 3990,},
                    {3230, 4522, 1615, 4836, 1600, 4682, 1500, 3993,},
                    {3231, 4523, 1615, 4839, 1600, 4684, 1500, 3996,},
                    {3232, 4525, 1615, 4842, 1600, 4686, 1500, 3999,},
                    {3233, 4528, 1615, 4845, 1600, 4688, 1500, 4002,},
                    {3234, 4529, 1616, 4848, 1600, 4690, 1500, 4005,},
                    {3235, 4529, 1617, 4851, 1600, 4692, 1500, 4008,},
                    {3236, 4530, 1618, 4854, 1600, 4694, 1500, 4011,},
                    {3237, 4532, 1619, 4857, 1600, 4696, 1500, 4014,},
                    {3238, 4535, 1620, 4860, 1600, 4698, 1500, 4017,},
                    {3239, 4536, 1620, 4864, 1600, 4700, 1500, 4020,},
                    {3240, 4536, 1620, 4865, 1601, 4702, 1500, 4023,},
                    {3241, 4537, 1620, 4866, 1602, 4704, 1500, 4026,},
                    {3242, 4539, 1620, 4867, 1603, 4706, 1500, 4029,},
                    {3243, 4542, 1620, 4868, 1604, 4708, 1500, 4032,},
                    {3244, 4543, 1621, 4869, 1605, 4710, 1500, 4035,},
                    {3245, 4543, 1622, 4870, 1606, 4712, 1500, 4038,},
                    {3246, 4544, 1623, 4871, 1607, 4714, 1500, 4041,},
                    {3247, 4546, 1624, 4872, 1608, 4716, 1500, 4044,},
                    {3248, 4549, 1625, 4873, 1609, 4718, 1500, 4047,},
                    {3249, 4550, 1625, 4875, 1610, 4720, 1500, 4050,},
                    {3250, 4550, 1625, 4875, 1611, 4722, 1500, 4053,},
                    {3251, 4551, 1625, 4875, 1612, 4724, 1500, 4056,},
                    {3252, 4553, 1625, 4875, 1613, 4726, 1500, 4059,},
                    {3253, 4556, 1625, 4875, 1614, 4728, 1500, 4062,},
                    {3254, 4557, 1626, 4875, 1615, 4730, 1500, 4065,},
                    {3255, 4557, 1627, 4875, 1616, 4732, 1500, 4068,},
                    {3256, 4558, 1628, 4875, 1617, 4734, 1500, 4071,},
                    {3257, 4560, 1629, 4875, 1618, 4736, 1500, 4074,},
                    {3258, 4563, 1630, 4875, 1619, 4738, 1500, 4077,},
                    {3259, 4564, 1630, 4876, 1620, 4740, 1500, 4080,},
                    {3260, 4564, 1630, 4877, 1621, 4742, 1500, 4083,},
                    {3261, 4565, 1630, 4878, 1622, 4744, 1500, 4086,},
                    {3262, 4567, 1630, 4879, 1623, 4746, 1500, 4089,},
                    {3263, 4570, 1630, 4880, 1624, 4748, 1500, 4092,},
                    {3264, 4571, 1631, 4881, 1625, 4750, 1500, 4095,},
                    {3265, 4571, 1632, 4882, 1626, 4752, 1500, 4098,},
                    {3266, 4572, 1633, 4883, 1627, 4754, 1500, 4101,},
                    {3267, 4574, 1634, 4884, 1628, 4756, 1500, 4104,},
                    {3268, 4577, 1635, 4885, 1629, 4758, 1500, 4107,},
                    {3269, 4578, 1635, 4887, 1630, 4760, 1500, 4110,},
                    {3270, 4578, 1635, 4889, 1631, 4762, 1500, 4113,},
                    {3271, 4579, 1635, 4891, 1632, 4764, 1500, 4116,},
                    {3272, 4581, 1635, 4893, 1633, 4766, 1500, 4119,},
                    {3273, 4584, 1635, 4895, 1634, 4768, 1500, 4122,},
                    {3274, 4585, 1636, 4897, 1635, 4770, 1500, 4125,},
                    {3275, 4585, 1637, 4899, 1636, 4772, 1500, 4128,},
                    {3276, 4586, 1638, 4901, 1637, 4774, 1500, 4131,},
                    {3277, 4588, 1639, 4903, 1638, 4776, 1500, 4134,},
                    {3278, 4591, 1640, 4905, 1639, 4778, 1500, 4137,},
                    {3279, 4592, 1640, 4908, 1640, 4780, 1500, 4140,},
                    {3280, 4592, 1640, 4911, 1641, 4782, 1500, 4143,},
                    {3281, 4593, 1640, 4914, 1642, 4784, 1500, 4146,},
                    {3282, 4595, 1640, 4917, 1643, 4786, 1500, 4149,},
                    {3283, 4598, 1640, 4920, 1644, 4788, 1500, 4152,},
                    {3284, 4599, 1641, 4923, 1645, 4790, 1500, 4155,},
                    {3285, 4599, 1642, 4926, 1646, 4792, 1500, 4158,},
                    {3286, 4600, 1643, 4929, 1647, 4794, 1500, 4161,},
                    {3287, 4602, 1644, 4932, 1648, 4796, 1500, 4164,},
                    {3288, 4605, 1645, 4935, 1649, 4798, 1500, 4167,},
                    {3289, 4606, 1645, 4939, 1650, 4800, 1500, 4170,},
                    {3290, 4606, 1645, 4940, 1650, 4803, 1500, 4173,},
                    {3291, 4607, 1645, 4941, 1650, 4806, 1500, 4176,},
                    {3292, 4609, 1645, 4942, 1650, 4809, 1500, 4179,},
                    {3293, 4612, 1645, 4943, 1650, 4812, 1500, 4182,},
                    {3294, 4613, 1646, 4944, 1650, 4815, 1500, 4185,},
                    {3295, 4613, 1647, 4945, 1650, 4818, 1500, 4188,},
                    {3296, 4614, 1648, 4946, 1650, 4821, 1500, 4191,},
                    {3297, 4616, 1649, 4947, 1650, 4824, 1500, 4194,},
                    {3298, 4619, 1650, 4948, 1650, 4827, 1500, 4197,},
                    {3299, 4620, 1650, 4950, 1650, 4830, 1500, 4200,},
                    {3300, 4620, 1650, 4950, 1650, 4833, 1500, 4203,},
                    {3301, 4621, 1650, 4950, 1650, 4836, 1500, 4206,},
                    {3302, 4623, 1650, 4950, 1650, 4839, 1500, 4209,},
                    {3303, 4626, 1650, 4950, 1650, 4842, 1500, 4212,},
                    {3304, 4627, 1651, 4950, 1650, 4845, 1500, 4215,},
                    {3305, 4627, 1652, 4950, 1650, 4848, 1500, 4218,},
                    {3306, 4628, 1653, 4950, 1650, 4851, 1500, 4221,},
                    {3307, 4630, 1654, 4950, 1650, 4854, 1500, 4224,},
                    {3308, 4633, 1655, 4950, 1650, 4857, 1500, 4227,},
                    {3309, 4634, 1655, 4951, 1650, 4860, 1500, 4230,},
                    {3310, 4634, 1655, 4952, 1650, 4863, 1500, 4233,},
                    {3311, 4635, 1655, 4953, 1650, 4866, 1500, 4236,},
                    {3312, 4637, 1655, 4954, 1650, 4869, 1500, 4239,},
                    {3313, 4640, 1655, 4955, 1650, 4872, 1500, 4242,},
                    {3314, 4641, 1656, 4956, 1650, 4875, 1500, 4245,},
                    {3315, 4641, 1657, 4957, 1650, 4878, 1500, 4248,},
                    {3316, 4642, 1658, 4958, 1650, 4881, 1500, 4251,},
                    {3317, 4644, 1659, 4959, 1650, 4884, 1500, 4254,},
                    {3318, 4647, 1660, 4960, 1650, 4887, 1500, 4257,},
                    {3319, 4648, 1660, 4962, 1650, 4890, 1500, 4260,},
                    {3320, 4648, 1660, 4964, 1650, 4893, 1500, 4263,},
                    {3321, 4649, 1660, 4966, 1650, 4896, 1500, 4266,},
                    {3322, 4651, 1660, 4968, 1650, 4899, 1500, 4269,},
                    {3323, 4654, 1660, 4970, 1650, 4902, 1500, 4272,},
                    {3324, 4655, 1661, 4972, 1650, 4905, 1500, 4275,},
                    {3325, 4655, 1662, 4974, 1650, 4908, 1500, 4278,},
                    {3326, 4656, 1663, 4976, 1650, 4911, 1500, 4281,},
                    {3327, 4658, 1664, 4978, 1650, 4914, 1500, 4284,},
                    {3328, 4661, 1665, 4980, 1650, 4917, 1500, 4287,},
                    {3329, 4662, 1665, 4983, 1650, 4920, 1500, 4290,},
                    {3330, 4662, 1665, 4986, 1650, 4923, 1500, 4293,},
                    {3331, 4663, 1665, 4989, 1650, 4926, 1500, 4296,},
                    {3332, 4665, 1665, 4992, 1650, 4929, 1500, 4299,},
                    {3333, 4668, 1665, 4995, 1650, 4932, 1500, 4302,},
                    {3334, 4669, 1666, 4998, 1650, 4935, 1500, 4305,},
                    {3335, 4669, 1667, 5001, 1650, 4938, 1500, 4308,},
                    {3336, 4670, 1668, 5004, 1650, 4941, 1500, 4311,},
                    {3337, 4672, 1669, 5007, 1650, 4944, 1500, 4314,},
                    {3338, 4675, 1670, 5010, 1650, 4947, 1500, 4317,},
                    {3339, 4676, 1670, 5014, 1650, 4950, 1500, 4320,},
                    {3340, 4676, 1670, 5015, 1651, 4953, 1500, 4323,},
                    {3341, 4677, 1670, 5016, 1652, 4956, 1500, 4326,},
                    {3342, 4679, 1670, 5017, 1653, 4959, 1500, 4329,},
                    {3343, 4682, 1670, 5018, 1654, 4962, 1500, 4332,},
                    {3344, 4683, 1671, 5019, 1655, 4965, 1500, 4335,},
                    {3345, 4683, 1672, 5020, 1656, 4968, 1500, 4338,},
                    {3346, 4684, 1673, 5021, 1657, 4971, 1500, 4341,},
                    {3347, 4686, 1674, 5022, 1658, 4974, 1500, 4344,},
                    {3348, 4689, 1675, 5023, 1659, 4977, 1500, 4347,},
                    {3349, 4690, 1675, 5025, 1660, 4980, 1500, 4350,},
                    {3350, 4690, 1675, 5025, 1661, 4983, 1500, 4353,},
                    {3351, 4691, 1675, 5025, 1662, 4986, 1500, 4356,},
                    {3352, 4693, 1675, 5025, 1663, 4989, 1500, 4359,},
                    {3353, 4696, 1675, 5025, 1664, 4992, 1500, 4362,},
                    {3354, 4697, 1676, 5025, 1665, 4995, 1500, 4365,},
                    {3355, 4697, 1677, 5025, 1666, 4998, 1500, 4368,},
                    {3356, 4698, 1678, 5025, 1667, 5001, 1500, 4371,},
                    {3357, 4700, 1679, 5025, 1668, 5004, 1500, 4374,},
                    {3358, 4703, 1680, 5025, 1669, 5007, 1500, 4377,},
                    {3359, 4704, 1680, 5026, 1670, 5010, 1500, 4380,},
                    {3360, 4704, 1680, 5027, 1671, 5013, 1500, 4383,},
                    {3361, 4705, 1680, 5028, 1672, 5016, 1500, 4386,},
                    {3362, 4707, 1680, 5029, 1673, 5019, 1500, 4389,},
                    {3363, 4710, 1680, 5030, 1674, 5022, 1500, 4392,},
                    {3364, 4711, 1681, 5031, 1675, 5025, 1500, 4395,},
                    {3365, 4711, 1682, 5032, 1676, 5028, 1500, 4398,},
                    {3366, 4712, 1683, 5033, 1677, 5031, 1500, 4401,},
                    {3367, 4714, 1684, 5034, 1678, 5034, 1500, 4404,},
                    {3368, 4717, 1685, 5035, 1679, 5037, 1500, 4407,},
                    {3369, 4718, 1685, 5037, 1680, 5040, 1500, 4410,},
                    {3370, 4718, 1685, 5039, 1681, 5043, 1500, 4413,},
                    {3371, 4719, 1685, 5041, 1682, 5046, 1500, 4416,},
                    {3372, 4721, 1685, 5043, 1683, 5049, 1500, 4419,},
                    {3373, 4724, 1685, 5045, 1684, 5052, 1500, 4422,},
                    {3374, 4725, 1686, 5047, 1685, 5055, 1500, 4425,},
                    {3375, 4725, 1687, 5049, 1686, 5058, 1500, 4428,},
                    {3376, 4726, 1688, 5051, 1687, 5061, 1500, 4431,},
                    {3377, 4728, 1689, 5053, 1688, 5064, 1500, 4434,},
                    {3378, 4731, 1690, 5055, 1689, 5067, 1500, 4437,},
                    {3379, 4732, 1690, 5058, 1690, 5070, 1500, 4440,},
                    {3380, 4732, 1690, 5061, 1691, 5073, 1500, 4443,},
                    {3381, 4733, 1690, 5064, 1692, 5076, 1500, 4446,},
                    {3382, 4735, 1690, 5067, 1693, 5079, 1500, 4449,},
                    {3383, 4738, 1690, 5070, 1694, 5082, 1500, 4452,},
                    {3384, 4739, 1691, 5073, 1695, 5085, 1500, 4455,},
                    {3385, 4739, 1692, 5076, 1696, 5088, 1500, 4458,},
                    {3386, 4740, 1693, 5079, 1697, 5091, 1500, 4461,},
                    {3387, 4742, 1694, 5082, 1698, 5094, 1500, 4464,},
                    {3388, 4745, 1695, 5085, 1699, 5097, 1500, 4467,},
                    {3389, 4746, 1695, 5089, 1700, 5100, 1500, 4470,},
                    {3390, 4746, 1695, 5090, 1700, 5104, 1500, 4473,},
                    {3391, 4747, 1695, 5091, 1700, 5108, 1500, 4476,},
                    {3392, 4749, 1695, 5092, 1700, 5112, 1500, 4479,},
                    {3393, 4752, 1695, 5093, 1700, 5116, 1500, 4482,},
                    {3394, 4753, 1696, 5094, 1700, 5120, 1500, 4485,},
                    {3395, 4753, 1697, 5095, 1700, 5124, 1500, 4488,},
                    {3396, 4754, 1698, 5096, 1700, 5128, 1500, 4491,},
                    {3397, 4756, 1699, 5097, 1700, 5132, 1500, 4494,},
                    {3398, 4759, 1700, 5098, 1700, 5136, 1500, 4497,},
                    {3399, 4760, 1700, 5100, 1700, 5140, 1500, 4500,},
                    {3400, 4760, 1700, 5100, 1700, 5141, 1501, 4503,},
                    {3401, 4761, 1700, 5100, 1700, 5142, 1502, 4506,},
                    {3402, 4763, 1700, 5100, 1700, 5143, 1503, 4509,},
                    {3403, 4766, 1700, 5100, 1700, 5144, 1504, 4512,},
                    {3404, 4767, 1701, 5100, 1700, 5145, 1505, 4515,},
                    {3405, 4767, 1702, 5100, 1700, 5146, 1506, 4518,},
                    {3406, 4768, 1703, 5100, 1700, 5147, 1507, 4521,},
                    {3407, 4770, 1704, 5100, 1700, 5148, 1508, 4524,},
                    {3408, 4773, 1705, 5100, 1700, 5149, 1509, 4527,},
                    {3409, 4774, 1705, 5101, 1700, 5150, 1510, 4530,},
                    {3410, 4774, 1705, 5102, 1700, 5151, 1511, 4533,},
                    {3411, 4775, 1705, 5103, 1700, 5152, 1512, 4536,},
                    {3412, 4777, 1705, 5104, 1700, 5153, 1513, 4539,},
                    {3413, 4780, 1705, 5105, 1700, 5154, 1514, 4542,},
                    {3414, 4781, 1706, 5106, 1700, 5155, 1515, 4545,},
                    {3415, 4781, 1707, 5107, 1700, 5156, 1516, 4548,},
                    {3416, 4782, 1708, 5108, 1700, 5157, 1517, 4551,},
                    {3417, 4784, 1709, 5109, 1700, 5158, 1518, 4554,},
                    {3418, 4787, 1710, 5110, 1700, 5159, 1519, 4557,},
                    {3419, 4788, 1710, 5112, 1700, 5160, 1520, 4560,},
                    {3420, 4788, 1710, 5114, 1700, 5161, 1521, 4563,},
                    {3421, 4789, 1710, 5116, 1700, 5162, 1522, 4566,},
                    {3422, 4791, 1710, 5118, 1700, 5163, 1523, 4569,},
                    {3423, 4794, 1710, 5120, 1700, 5164, 1524, 4572,},
                    {3424, 4795, 1711, 5122, 1700, 5165, 1525, 4575,},
                    {3425, 4795, 1712, 5124, 1700, 5166, 1526, 4578,},
                    {3426, 4796, 1713, 5126, 1700, 5167, 1527, 4581,},
                    {3427, 4798, 1714, 5128, 1700, 5168, 1528, 4584,},
                    {3428, 4801, 1715, 5130, 1700, 5169, 1529, 4587,},
                    {3429, 4802, 1715, 5133, 1700, 5170, 1530, 4590,},
                    {3430, 4802, 1715, 5136, 1700, 5171, 1531, 4593,},
                    {3431, 4803, 1715, 5139, 1700, 5172, 1532, 4596,},
                    {3432, 4805, 1715, 5142, 1700, 5173, 1533, 4599,},
                    {3433, 4808, 1715, 5145, 1700, 5174, 1534, 4602,},
                    {3434, 4809, 1716, 5148, 1700, 5175, 1535, 4605,},
                    {3435, 4809, 1717, 5151, 1700, 5176, 1536, 4608,},
                    {3436, 4810, 1718, 5154, 1700, 5177, 1537, 4611,},
                    {3437, 4812, 1719, 5157, 1700, 5178, 1538, 4614,},
                    {3438, 4815, 1720, 5160, 1700, 5179, 1539, 4617,},
                    {3439, 4816, 1720, 5164, 1700, 5180, 1540, 4620,},
                    {3440, 4816, 1720, 5165, 1701, 5181, 1541, 4623,},
                    {3441, 4817, 1720, 5166, 1702, 5182, 1542, 4626,},
                    {3442, 4819, 1720, 5167, 1703, 5183, 1543, 4629,},
                    {3443, 4822, 1720, 5168, 1704, 5184, 1544, 4632,},
                    {3444, 4823, 1721, 5169, 1705, 5185, 1545, 4635,},
                    {3445, 4823, 1722, 5170, 1706, 5186, 1546, 4638,},
                    {3446, 4824, 1723, 5171, 1707, 5187, 1547, 4641,},
                    {3447, 4826, 1724, 5172, 1708, 5188, 1548, 4644,},
                    {3448, 4829, 1725, 5173, 1709, 5189, 1549, 4647,},
                    {3449, 4830, 1725, 5175, 1710, 5190, 1550, 4650,},
                    {3450, 4830, 1725, 5175, 1711, 5191, 1551, 4653,},
                    {3451, 4831, 1725, 5175, 1712, 5192, 1552, 4656,},
                    {3452, 4833, 1725, 5175, 1713, 5193, 1553, 4659,},
                    {3453, 4836, 1725, 5175, 1714, 5194, 1554, 4662,},
                    {3454, 4837, 1726, 5175, 1715, 5195, 1555, 4665,},
                    {3455, 4837, 1727, 5175, 1716, 5196, 1556, 4668,},
                    {3456, 4838, 1728, 5175, 1717, 5197, 1557, 4671,},
                    {3457, 4840, 1729, 5175, 1718, 5198, 1558, 4674,},
                    {3458, 4843, 1730, 5175, 1719, 5199, 1559, 4677,},
                    {3459, 4844, 1730, 5176, 1720, 5200, 1560, 4680,},
                    {3460, 4844, 1730, 5177, 1721, 5201, 1561, 4683,},
                    {3461, 4845, 1730, 5178, 1722, 5202, 1562, 4686,},
                    {3462, 4847, 1730, 5179, 1723, 5203, 1563, 4689,},
                    {3463, 4850, 1730, 5180, 1724, 5204, 1564, 4692,},
                    {3464, 4851, 1731, 5181, 1725, 5205, 1565, 4695,},
                    {3465, 4851, 1732, 5182, 1726, 5206, 1566, 4698,},
                    {3466, 4852, 1733, 5183, 1727, 5207, 1567, 4701,},
                    {3467, 4854, 1734, 5184, 1728, 5208, 1568, 4704,},
                    {3468, 4857, 1735, 5185, 1729, 5209, 1569, 4707,},
                    {3469, 4858, 1735, 5187, 1730, 5210, 1570, 4710,},
                    {3470, 4858, 1735, 5189, 1731, 5211, 1571, 4713,},
                    {3471, 4859, 1735, 5191, 1732, 5212, 1572, 4716,},
                    {3472, 4861, 1735, 5193, 1733, 5213, 1573, 4719,},
                    {3473, 4864, 1735, 5195, 1734, 5214, 1574, 4722,},
                    {3474, 4865, 1736, 5197, 1735, 5215, 1575, 4725,},
                    {3475, 4865, 1737, 5199, 1736, 5216, 1576, 4728,},
                    {3476, 4866, 1738, 5201, 1737, 5217, 1577, 4731,},
                    {3477, 4868, 1739, 5203, 1738, 5218, 1578, 4734,},
                    {3478, 4871, 1740, 5205, 1739, 5219, 1579, 4737,},
                    {3479, 4872, 1740, 5208, 1740, 5220, 1580, 4740,},
                    {3480, 4872, 1740, 5211, 1741, 5221, 1581, 4743,},
                    {3481, 4873, 1740, 5214, 1742, 5222, 1582, 4746,},
                    {3482, 4875, 1740, 5217, 1743, 5223, 1583, 4749,},
                    {3483, 4878, 1740, 5220, 1744, 5224, 1584, 4752,},
                    {3484, 4879, 1741, 5223, 1745, 5225, 1585, 4755,},
                    {3485, 4879, 1742, 5226, 1746, 5226, 1586, 4758,},
                    {3486, 4880, 1743, 5229, 1747, 5227, 1587, 4761,},
                    {3487, 4882, 1744, 5232, 1748, 5228, 1588, 4764,},
                    {3488, 4885, 1745, 5235, 1749, 5229, 1589, 4767,},
                    {3489, 4886, 1745, 5239, 1750, 5230, 1590, 4770,},
                    {3490, 4886, 1745, 5240, 1750, 5232, 1591, 4773,},
                    {3491, 4887, 1745, 5241, 1750, 5234, 1592, 4776,},
                    {3492, 4889, 1745, 5242, 1750, 5236, 1593, 4779,},
                    {3493, 4892, 1745, 5243, 1750, 5238, 1594, 4782,},
                    {3494, 4893, 1746, 5244, 1750, 5240, 1595, 4785,},
                    {3495, 4893, 1747, 5245, 1750, 5242, 1596, 4788,},
                    {3496, 4894, 1748, 5246, 1750, 5244, 1597, 4791,},
                    {3497, 4896, 1749, 5247, 1750, 5246, 1598, 4794,},
                    {3498, 4899, 1750, 5248, 1750, 5248, 1599, 4797,},
                    {3499, 4900, 1750, 5250, 1750, 5250, 1600, 4800,},};
            if (n >= 2500 && n < 3500) {
                int num = n - 2500;
                for (int i = 1; i <= romanNumerals.length; ++i) {
                    if (cachedMap[num][i] > 0) {
                        if (!first) {
                            sb.append(',');
                        } else {
                            first = false;
                        }
                        sb.append(romanNumerals[i - 1]);
                        sb.append(cachedMap[num][i]);
                    }
                }
            } else {
                int [][] map = new int[n + 1][romanNumerals.length + 1];
                /**
                 * M,MM,MMM
                 * C,CC,CCC,CD,D,DC,DCC,DCCC,CM
                 * X,XX,XXX,XL,L,LX,LXX,LXXX,XC
                 * I,II,III,IV,V,VI,VII,VIII,IX
                 */
                for (int j = 1, i = 1; j < map.length; i += 2, j *= 10) {
                    for (int k = j, c = 1; k < map.length && c < 10; k += j, ++c) {
                        map[k][0] = 1;
                        switch (c) {
                            case 1:
                            case 2:
                            case 3:
                                map[k][i] = c;
                                break;
                            case 4:
                                map[k][i] = 1;
                            case 5:
                                map[k][i + 1] = 1;
                                break;
                            case 6:
                            case 7:
                            case 8:
                                map[k][i + 1] = 1;
                                map[k][i] = c - 5;
                                break;
                            case 9:
                                map[k][i] = 1;
                                map[k][i + 2] = 1;
                                break;
                        }
                        //                    System.out.println(Arrays.toString(map[k]));
                    }
                }
                int [] mapForDigit = {1000, 100, 10, 1};
                for (int i = 1; i <= n; ++i) {
                    if (map[i][0] > 0) {//for single digit
                        for (int j = 1; j <= romanNumerals.length; ++j) {
                            map[0][j] += map[i][j];
                        }
                    } else {
                        //from highest
                        int high = i / mapForDigit[0];
                        int j = 0;
                        while (high == 0) {
                            ++j;
                            high = i / mapForDigit[j];
                        }
                        if (mapForDigit[j] > 1) {
                            high *= mapForDigit[j];
                            int low = i % mapForDigit[j];
                            if (low > 0) {
                                for (j = 1; j <= romanNumerals.length; ++j) {
                                    map[i][j] += map[low][j];
                                }
                            }
                        }
                        for (j = 1; j <= romanNumerals.length; ++j) {
                            map[i][j] += map[high][j];
                            map[0][j] += map[i][j];
                        }
                        map[i][0] = 1;
                    }
                    //for test
                    //                if (i == 68) {
                    //                    System.out.println(Arrays.toString(map[i]));
                    //                }
                }

                //construct answer string

                for (int i = 1; i <= romanNumerals.length; ++i) {
                    if (map[0][i] > 0) {
                        if (!first) {
                            sb.append(',');
                        } else {
                            first = false;
                        }
                        sb.append(romanNumerals[i - 1]);
                        sb.append(map[0][i]);
                    }
                }
            }
            return sb.toString();
        }

    }

    /**
     * mapForSingle
     * first element is not used
     * I:the idx is 1
     * ____I V X L C
     * I
     * II
     * III
     * ...
     * -----------------------------------
     * * three parts
     * High*Weight*[1,9]+Weight*[1,(Cur-1)]+(Num%(Weight*10)-Cur*100+1)*
     * [Cur
     * ]
     * 378
     * 0*100*[1,9]+100*[1,(3-1)]+(378-3*100+1)*[3]
     * 3*10*[1,9]+10*[1,(7-1)]+(78-70+1)*[7]
     * 37*[1,9]+1*[1,(8-1)]+(8-8+1)*[8]
     */
    //TODO
    public static String run3(String arg) {
        char [] romanNumerals = {'I', 'V', 'X', 'L', 'C', 'D', 'M'};
        if (arg.charAt(0) > '9') {
            int [] romanNumeralsMap = new int[128];
            for (int i = 0, j = 1; i < romanNumerals.length; ++i) {
                romanNumeralsMap[romanNumerals[i]] = j;
                if (i % 2 == 0) {
                    j *= 5;
                } else {
                    j <<= 1;
                }
            }

            int pre = 0;
            int sum = 0;
            boolean isPreProcessed = false;
            int sameCount = 1;
            int cur = 0;
            for (int i = 0, len = arg.length(); i < len; ++i) {
                pre = (i - 1) < 0 ? -1 : arg.charAt(i - 1);
                cur = arg.charAt(i);
                //                System.out.println((char)(cur));
                //avoid Chinese char
                if (cur >= 127 || cur < 0 || romanNumeralsMap[cur] == 0) {
                    //                    System.out.println((char)cur + " is not the legal char----" + arg);
                    return "ILLEGAL";
                }
                switch (cur) {
                    case 'I':
                    case 'X':
                    case 'C':
                    case 'M':
                        if (!isPreProcessed) {
                            if (pre != -1) {
                                if (romanNumeralsMap[pre] == romanNumeralsMap[cur]) {
                                    ++sameCount;
                                    if (sameCount == 4) {
                                        //                                        System.out.println("more than 3 " + (char)cur + "----" + arg);
                                        return "ILLEGAL";
                                    }
                                    sum += romanNumeralsMap[pre];
                                    if (i == len - 1) {//this is the last one
                                        sum += romanNumeralsMap[cur];
                                    }
                                } else if (romanNumeralsMap[pre] < romanNumeralsMap[cur]) {
                                    if (romanNumeralsMap[cur] / romanNumeralsMap[pre] == 10) {
                                        isPreProcessed = true;
                                        sum -= romanNumeralsMap[pre];
                                        sum += romanNumeralsMap[cur];
                                        //                                        System.out.println("romanNumeralsMap[cur] / romanNumeralsMap[pre] == 10" + " "
                                        //                                                + sum);
                                    } else {
                                        //                                        System.out.println(romanNumeralsMap[cur] + "/" + romanNumeralsMap[pre]
                                        //                                                + "!= 10----" + arg);
                                        return "ILLEGAL";
                                    }
                                } else {
                                    sum += romanNumeralsMap[pre];
                                    sameCount = 1;
                                    if (i == len - 1) {//this is the last one
                                        sum += romanNumeralsMap[cur];
                                    }
                                }
                            }
                        } else {
                            isPreProcessed = false;
                            //                            System.out.println("unproccessed " + (char)cur);
                            if (i == len - 1) {//this is the last one
                                sum += romanNumeralsMap[cur];
                            }
                        }

                        break;
                    //always plus
                    case 'V':
                    case 'L':
                    case 'D':
                        if (sameCount == 3) {
                            sameCount = 1;
                        }
                        if (!isPreProcessed) {
                            if (pre != -1) {
                                if (romanNumeralsMap[pre] > romanNumeralsMap[cur]) {
                                    sum += romanNumeralsMap[pre];
                                } else if (romanNumeralsMap[pre] < romanNumeralsMap[cur]) {
                                    if (romanNumeralsMap[cur] / romanNumeralsMap[pre] > 5) {
                                        //                                        System.out.println(romanNumeralsMap[cur] + "/" + romanNumeralsMap[pre]
                                        //                                                + "> 5----" + arg);
                                        return "ILLEGAL";
                                    }
                                    sum -= romanNumeralsMap[pre];
                                } else {
                                    //can't be equal
                                    //                                    System.out.println((char)cur + " can't be equal to " + (char)pre + "----" + arg);
                                    return "ILLEGAL";
                                }
                            }
                            isPreProcessed = true;
                        } else {
                            if (pre != -1) {
                                if (romanNumeralsMap[cur] / romanNumeralsMap[pre] > 5) {
                                    //                                    System.out.println(romanNumeralsMap[cur] + "/" + romanNumeralsMap[pre] + "> 5----"
                                    //                                            + arg);
                                    return "ILLEGAL";
                                }
                            }
                        }
                        sum += romanNumeralsMap[cur];
                        break;
                }
                //                System.out.println(sum);
            }
            return String.valueOf(sum);
        } else {

            int n = -1;
            try {
                n = Integer.parseInt(arg);
            } catch (Exception e) {
                return "ILLEGAL";
            }

            final int [][] mapForSingle = {
                    {0, 0, 0},
                    {1, 0, 0},
                    {2, 0, 0},
                    {3, 0, 0},
                    {1, 1, 0},
                    {0, 1, 0},
                    {1, 1, 0},
                    {2, 1, 0},
                    {3, 1, 0},
                    {1, 0, 1},
            };
            int [][] mapForAll = new int[mapForSingle.length][3];
            //calc sum
            for (int i = 0; i < mapForSingle.length - 1; ++i) {
                for (int j = 0; j < 3; ++j) {
                    mapForAll[i + 1][j] = mapForSingle[i + 1][j] + mapForAll[i][j];
                }
            }
            ////////////////////////////////////////////////////////
            //test
            //        for (int i = 1; i < mapForAll.length; ++i) {
            //            for (int j = 0; j < 3; ++j) {
            //                System.out.print(mapForAll[i][j] + " ");
            //            }
            //            System.out.println();
            //        }
            //////////////////////////////////////////////////////
            int num = n;
            int bitOffset = 0;
            int high = 0;
            int curDigit = 0;
            int [] result = new int[romanNumerals.length + 2];
            int weight = 1;
            int nextWeight = 1;
            int remain = 0;
            //from low to high
            while (num > 0) {
                nextWeight *= 10;
                remain = n % nextWeight;
                high = num / 10;//might be zero
                curDigit = num % 10;//not be zero
                //            if (high > 0) {
                //            System.out.println("high:" + high + ", curDigit:" + curDigit + ",remain:" + remain);
                for (int i = 0; i < 3; ++i) {
                    if (high > 0) {
                        result[i + bitOffset] += high * weight * mapForAll[9][i];
                        //                    System.out.println("---" + Arrays.toString(result));
                    }
                    if (curDigit > 1) {
                        result[i + bitOffset] += weight * mapForAll[curDigit - 1][i];
                        //                    System.out.println("---" + Arrays.toString(result));
                    }
                    if (curDigit > 0) {
                        //                    System.out.println("remain:"+remain);
                        //                    System.out.println((remain - (curDigit * weight) + 1));
                        result[i + bitOffset] += ((remain - (curDigit * weight) + 1) * mapForSingle[curDigit][i]);
                        //                    System.out.println("---" + Arrays.toString(result));
                    }
                }
                //            } else {
                //                break;
                //            }
                weight = nextWeight;
                num = high;
                bitOffset += 2;
            }
            //construct answer string
            StringBuilder sb = new StringBuilder(64);
            boolean first = true;
            for (int i = 0; i < result.length; ++i) {
                if (result[i] > 0) {
                    if (!first) {
                        sb.append(',');
                    } else {
                        first = false;
                    }
                    sb.append(romanNumerals[i]);
                    sb.append(result[i]);
                }
            }
            return sb.toString();
        }

    }

    /**
     * mapForSingle
     * first element is not used
     * I:the idx is 1
     * ____I V X L C
     * I
     * II
     * III
     * ...
     * -----------------------------------
     * * three parts
     * High*Weight*[1,9]+Weight*[1,(Cur-1)]+(Num%(Weight*10)-Cur*100+1)*
     * [Cur
     * ]
     * 378
     * 0*100*[1,9]+100*[1,(3-1)]+(378-3*100+1)*[3]
     * 3*10*[1,9]+10*[1,(7-1)]+(78-70+1)*[7]
     * 37*[1,9]+1*[1,(8-1)]+(8-8+1)*[8]
     */
    //TODO
    public static String run4(String arg) {
        char [] romanNumerals = {'I', 'V', 'X', 'L', 'C', 'D', 'M'};
        if (arg.charAt(0) > '9') {
            int [] romanNumeralsMap = new int[128];
            for (int i = 0, j = 1; i < romanNumerals.length; ++i) {
                romanNumeralsMap[romanNumerals[i]] = j;
                if (i % 2 == 0) {
                    j *= 5;
                } else {
                    j <<= 1;
                }
            }

            int pre = 0;
            int sum = 0;
            boolean isPreProcessed = false;
            int sameCount = 1;
            int cur = 0;
            for (int i = 0, len = arg.length(); i < len; ++i) {
                pre = (i - 1) < 0 ? -1 : arg.charAt(i - 1);
                cur = arg.charAt(i);
                //avoid Chinese char
                if (cur >= 127 || cur < 0 || romanNumeralsMap[cur] == 0) {//is not the legal char
                    return "ILLEGAL";
                }
                switch (cur) {
                    case 'I':
                    case 'X':
                    case 'C':
                    case 'M':
                        if (!isPreProcessed) {
                            if (pre != -1) {
                                if (romanNumeralsMap[pre] == romanNumeralsMap[cur]) {
                                    ++sameCount;
                                    if (sameCount == 4) {//more than 3 same char
                                        return "ILLEGAL";
                                    }
                                    sum += romanNumeralsMap[pre];
                                    if (i == len - 1) {//this is the last one
                                        sum += romanNumeralsMap[cur];
                                    }
                                } else if (romanNumeralsMap[pre] < romanNumeralsMap[cur]) {
                                    if (romanNumeralsMap[cur] / romanNumeralsMap[pre] == 10) {
                                        isPreProcessed = true;
                                        sum -= romanNumeralsMap[pre];
                                        sum += romanNumeralsMap[cur];
                                    } else {
                                        return "ILLEGAL";
                                    }
                                } else {
                                    sum += romanNumeralsMap[pre];
                                    sameCount = 1;
                                    if (i == len - 1) {//this is the last one
                                        sum += romanNumeralsMap[cur];
                                    }
                                }
                            }
                        } else {
                            isPreProcessed = false;
                            if (i == len - 1) {//this is the last one
                                sum += romanNumeralsMap[cur];
                            }
                        }

                        break;
                    //always plus
                    case 'V':
                    case 'L':
                    case 'D':
                        if (sameCount == 3) {
                            sameCount = 1;
                        }
                        if (!isPreProcessed) {
                            if (pre != -1) {
                                if (romanNumeralsMap[pre] > romanNumeralsMap[cur]) {
                                    sum += romanNumeralsMap[pre];
                                } else if (romanNumeralsMap[pre] < romanNumeralsMap[cur]) {
                                    if (romanNumeralsMap[cur] / romanNumeralsMap[pre] > 5) {
                                        //                                        System.out.println(romanNumeralsMap[cur] + "/" + romanNumeralsMap[pre]
                                        //                                                + "> 5----" + arg);
                                        return "ILLEGAL";
                                    }
                                    sum -= romanNumeralsMap[pre];
                                } else {
                                    //can't be equal
                                    //                                    System.out.println((char)cur + " can't be equal to " + (char)pre + "----" + arg);
                                    return "ILLEGAL";
                                }
                            }
                            isPreProcessed = true;
                        } else {
                            if (pre != -1) {
                                if (romanNumeralsMap[cur] / romanNumeralsMap[pre] > 5) {
                                    //                                    System.out.println(romanNumeralsMap[cur] + "/" + romanNumeralsMap[pre] + "> 5----"
                                    //                                            + arg);
                                    return "ILLEGAL";
                                }
                            }
                        }
                        sum += romanNumeralsMap[cur];
                        break;
                    default:
                        return "ILLEGAL";
                }
            }
            return String.valueOf(sum);
        } else {

            int n = -1;
            try {
                n = Integer.parseInt(arg);
            } catch (Exception e) {
                return "ILLEGAL";
            }
            if (n < 1 || n >= 3500) {
                return "ILLEGAL";
            }

            final int [][] mapForSingle = {
                    {0, 0, 0},
                    {1, 0, 0},
                    {2, 0, 0},
                    {3, 0, 0},
                    {1, 1, 0},
                    {0, 1, 0},
                    {1, 1, 0},
                    {2, 1, 0},
                    {3, 1, 0},
                    {1, 0, 1},
            };
            int [][] mapForAll = new int[mapForSingle.length][3];
            //calc sum
            for (int i = 0; i < mapForSingle.length - 1; ++i) {
                for (int j = 0; j < 3; ++j) {
                    mapForAll[i + 1][j] = mapForSingle[i + 1][j] + mapForAll[i][j];
                }
            }
            int num = n;
            int bitOffset = 0;
            int high = 0;
            int curDigit = 0;
            int [] result = new int[romanNumerals.length + 2];
            int weight = 1;
            int nextWeight = 1;
            int remain = 0;
            //from low to high
            while (num > 0) {
                nextWeight *= 10;
                remain = n % nextWeight;
                high = num / 10;//might be zero
                curDigit = num % 10;//not be zero
                for (int i = 0; i < 3; ++i) {
                    if (high > 0) {
                        result[i + bitOffset] += high * weight * mapForAll[9][i];
                    }
                    if (curDigit > 1) {
                        result[i + bitOffset] += weight * mapForAll[curDigit - 1][i];
                    }
                    if (curDigit > 0) {
                        result[i + bitOffset] += ((remain - (curDigit * weight) + 1) * mapForSingle[curDigit][i]);
                    }
                }
                weight = nextWeight;
                num = high;
                bitOffset += 2;
            }
            //construct answer string
            StringBuilder sb = new StringBuilder(64);
            boolean first = true;
            for (int i = 0; i < result.length; ++i) {
                if (result[i] > 0) {
                    if (!first) {
                        sb.append(',');
                    } else {
                        first = false;
                    }
                    sb.append(romanNumerals[i]);
                    sb.append(result[i]);
                }
            }
            return sb.toString();
        }

    }

    public static void testStandard() {

        //case 1
        test("5", "I7,V2", 1);
        //case 2
        test("CDXCVIII", "498", 1);
        //case 3
        test("100", "I140,V50,X150,L50,C11", 1);
        //case 4
        test("MCCCXVIII", "1318", 1);
        //case 5
        test("2974", null, 1);
        //case 6
        test("DCCLXXVII", "777", 1);
        //case 7
        test("3213", null, 1);
        //case 8
        test("MMCDXIX", "2419", 1);
        //case 9
        test("3499", null, 1);
        //case 10
        test("MMMCDLXXXIX", "3489", 1);
    }

    public static void testAll() {
        testRoman1(1);
        testRoman2();
        testStandard();
    }

    /**
     * @param args
     */
    public static void main(String [] args) {
//       
        testAll();
        //

        //        run3("156");
        //        run3("56");
        //        run3("378");
        //        System.out.println(run("378"));
        //I392,V140,X411,L141,C282
        //        System.out.println(run2("280"));
        //        System.out.println(run("280"));
        //I140,V50,X150,L50,C11
        //        System.out.println(run2("100"));
        //I218,V78,X225,L67,C67
        //                System.out.println(run2("156"));
        //I78,V28,X75,L17
        //                System.out.println(run2("56"));
        //        System.out.println(run2("111"))
        //        testRoman2();
        //        testRoman1();
        //        System.out.println(generateMap(2500, 3500));
        //        int [][] a = {
        //                {1, 2,},
        //                {2, 3,},
        //
        //        };

    }

}
