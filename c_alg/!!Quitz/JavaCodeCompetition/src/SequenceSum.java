import java.util.Arrays;
import java.util.LinkedList;

import junit.framework.Assert;

/**
 * the operator should be ordered by [space] -> "+" -> "-"
 * 1+2-3
 * +:0
 * -:1
 * ' ':2
 * cache[]={0,1}
 */
public class SequenceSum {
    //for test
    //            for (int i = 1; i <= n; ++i) {
    //                for (int j = i + 1; j <= n; ++j) {
    //                    if (combinedNumMap[i][j] != 0)
    //                        System.out.print(combinedNumMap[i][j] + " ");
    //                    else {
    //                        System.out.print("  ");
    //                    }
    //                }
    //                System.out.println();
    //            }
    //            System.out.println();

    //            cimbniedNum = combinedNumMap[i + 1][j + 1];
    //        
    //            StringBuilder combined = new StringBuilder();
    //            for (int k = i + 1; k <= j + 1; ++k) {
    //                combined.append(k);
    //            }
    //            cimbniedNum = Long.parseLong(combined.toString());
    //            combinedNumMap[i + 1][j + 1] = cimbniedNum;
    static int caseCount = 1;

    public static double test(String arg, String expected, int c) {
        String result = null;
        System.out.println("case " + caseCount + ":");
        ++caseCount;
        System.out.println("input:" + arg);
        long start = System.currentTimeMillis();
        for (int i = 0; i < c; ++i) {
            //                        result = run7BF(arg);
            result = run7_6(arg);
            //            result = run7_5(arg);
            //            result = run7(arg);
            //                        result = run9(arg);
        }

        long total = (System.currentTimeMillis() - start);
        System.out.println();
        System.out.println("total: " + total + "ms");
        double time = (total) / (double)c;
        System.out.println("each: " + time + "ms");
        System.out.println(result);
        System.out.println("------------------------------------");
        //        System.out.println("expected:" + expected);
        if (expected != null && !expected.isEmpty()) {
            Assert.assertEquals(expected, result);
        }
        return time;

    }

    public static int calcOneOp(int [] cache, int calc, int i, int n) {
        switch (cache[i]) {
            case 1:
                calc += (i + 2);
                break;
            case 2:
                calc -= (i + 2);
                break;
            case 0: {
                //if found space op, check if continuous
                int j = i + 1;
                while (j < n - 1) {
                    //count space op
                    if (0 == cache[j]) {
                        ++j;
                    } else {
                        break;
                    }
                }
                StringBuilder combined = new StringBuilder();
                for (int k = i + 1; k <= j + 1; ++k) {
                    combined.append(k);
                }
                if (i == 0) {
                    calc = Integer.parseInt(combined.toString());
                } else {
                    switch (cache[i - 1]) {
                        case 1:
                            //                                System.out.println(calc);
                            calc -= (i + 1);
                            //                                System.out.println(calc);
                            calc += Integer.parseInt(combined.toString());
                            //                                System.out.println(calc);
                            break;
                        case 2:
                            //                                System.out.println(calc);
                            calc += (i + 1);
                            //                                System.out.println(calc);
                            calc -= Integer.parseInt(combined.toString());
                            //                                System.out.println(calc);
                            break;
                    }
                }

            }
                break;
        }
        return calc;
    }

    /**
     * @param cache
     * @param n
     */
    public static int printAndCalc(int cache[], int n) {
        StringBuilder sb = new StringBuilder();
        sb.append(1);
        int calc = 1;
        for (int i = 0; i < n - 1; ++i) {
            //construct string
            switch (cache[i]) {
                case 1:
                    sb.append('+');
                    break;
                case 2:
                    sb.append('-');
                    break;
                case 0:
                    sb.append(' ');
                    break;
            }
            sb.append((i + 2));
            //calc sum
            calc = calcOneOp(cache, calc, i, n);
        }
        System.out.println(sb.toString() + "=" + calc);
        return calc;
    }

    public static long calcOneOp(char [] cache, long calc, int i) {
        switch (cache[i]) {
            case 1:
                calc += (i + 2);
                break;
            case 2:
                calc -= (i + 2);
                break;
            case 0: {
                //if found space op, check if continuous
                int j = i + 1;
                while (j < cache.length) {
                    //count space op
                    if (0 == cache[j]) {
                        ++j;
                    } else {
                        break;
                    }
                }
                StringBuilder combined = new StringBuilder();
                for (int k = i + 1; k <= j + 1; ++k) {
                    combined.append(k);
                }
                if (i == 0) {
                    calc = Long.parseLong(combined.toString());
                } else {
                    switch (cache[i - 1]) {
                        case 1:
                            //                                System.out.println(calc);
                            calc -= (i + 1);
                            //                                System.out.println(calc);
                            calc += Long.parseLong(combined.toString());
                            //                                System.out.println(calc);
                            break;
                        case 2:
                            //                                System.out.println(calc);
                            calc += (i + 1);
                            //                                System.out.println(calc);
                            calc -= Long.parseLong(combined.toString());
                            //                                System.out.println(calc);
                            break;
                    }
                }

            }
                break;
        }
        return calc;
    }

    public static long printAndCalc(char cache[]) {
        StringBuilder sb = new StringBuilder();
        sb.append(1);
        long calc = 1;
        for (int i = 0; i < cache.length; ++i) {
            //construct string
            switch (cache[i]) {
                case 1:
                    sb.append('+');
                    break;
                case 2:
                    sb.append('-');
                    break;
                case 0:
                    sb.append(' ');
                    break;
            }
            sb.append((i + 2));
            //calc sum
            calc = calcOneOp(cache, calc, i);
        }
        System.out.println(sb.toString() + "=" + calc);
        return calc;
    }

    public static String run(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        char ch = arg.charAt(0);
        StringBuilder result = new StringBuilder();
        if (ch == 'S') {
            //3<=N<=15
            int n = 0;
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            final int MAX_N = 15;
            //3<=N<=15
            if (n < 3 || n > MAX_N) {
                return "ILLEGAL";
            }
            //' '：0
            //+：1
            //-： 2
            //brute force
            //for current op idx
            int [] opFlag = new int[n - 1];
            //op sequence cache
            int [] cache = new int[n - 1];
            //first op idx
            int opSeqOrder = 0;
            //set to first number
            while (true) {
                ////////////////////////////////////////////////
                boolean hasNewOp = false;
                if (opFlag[opSeqOrder] < 3) {
                    cache[opSeqOrder] = opFlag[opSeqOrder];
                    ++opFlag[opSeqOrder];
                    hasNewOp = true;
                }
                /////////////////////////////////////////////////
                if (hasNewOp) {
                    //up to end
                    if (opSeqOrder == n - 2) {
                        //                        System.out.println(Arrays.toString(cache));
                        long calc = 1;
                        for (int i = 0; i < n - 1; ++i) {
                            switch (cache[i]) {
                                case 1:
                                    calc += (i + 2);
                                    break;
                                case 2:
                                    calc -= (i + 2);
                                    break;
                                case 0: {
                                    //if found space op, check if continuous
                                    int j = i + 1;
                                    while (j < n - 1) {
                                        //count space op
                                        if (0 == cache[j]) {
                                            ++j;
                                        } else {
                                            break;
                                        }
                                    }
                                    //special
                                    if (i + 1 >= 3 || j + 1 != MAX_N) {
                                        StringBuilder combined = new StringBuilder();
                                        for (int k = i + 1; k <= j + 1; ++k) {
                                            combined.append(k);
                                        }
                                        if (i == 0) {
                                            calc = Long.parseLong(combined.toString());
                                        } else {
                                            switch (cache[i - 1]) {
                                                case 1:
                                                    //                                System.out.println(calc);
                                                    calc -= (i + 1);
                                                    //                                System.out.println(calc);
                                                    calc += Long.parseLong(combined.toString());
                                                    //                                System.out.println(calc);
                                                    break;
                                                case 2:
                                                    //                                System.out.println(calc);
                                                    calc += (i + 1);
                                                    //                                System.out.println(calc);
                                                    calc -= Long.parseLong(combined.toString());
                                                    //                                System.out.println(calc);
                                                    break;
                                            }
                                        }
                                    }
                                }
                                    break;
                            }
                        }
                        if (calc == 0) {
                            result.append(1);
                            for (int i = 0; i < n - 1; ++i) {
                                //construct string
                                switch (cache[i]) {
                                    case 1:
                                        result.append('+');
                                        break;
                                    case 2:
                                        result.append('-');
                                        break;
                                    case 0:
                                        result.append(' ');
                                        break;
                                }
                                result.append((i + 2));
                            }
                            result.append(';');
                        }
                    } else {
                        ++opSeqOrder;
                    }
                } else {
                    //                    System.out.println("opSeqOrder:" + opSeqOrder);
                    if (opSeqOrder - 1 < 0) {
                        //                        System.out.println("all checked!");
                        break;
                    } else {
                        //clear
                        opFlag[opSeqOrder] = 0;
                        --opSeqOrder;
                    }
                }
            }
        } else if (ch == 'R') {} else {
            return "ILLEGAL";
        }
        if (result.length() == 0) {
            return "NOTFOUND";
        }
        return result.substring(0, result.length() - 1);
        //        return result.toString();
    }

    ////////////////////////////////////////////////////////////////////////////
    //|| (arg.charAt(0) != 'S') || (arg.charAt(0) != 'R')
    //without using big number calc
    public static String run2(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        char ch = arg.charAt(0);
        if (ch == 'S') {
            //3<=N<=15
            //TODO write a table contains 3~15???
            int n = 0;
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            final int MAX_N = 15;
            final int MIN_N = 3;

            //3<=N<=15
            if (n < MIN_N || n > MAX_N) {
                return "ILLEGAL";
            }
            //various buffer size
            StringBuilder result = new StringBuilder(1 << n);
            //' '：0
            //+：1
            //-： 2
            //brute force
            //for current op idx
            char [] opFlag = new char[n - 1];
            //op sequence cache
            char [] cache = new char[n - 1];
            //first op idx
            int opSeqOrder = 0; //
            //row: startNum
            //column:endNum
            long [][] combinedNumMap = new long[n + 1][n + 1];
            for (int i = 1; i <= n; ++i) {
                combinedNumMap[i][i] = i;
                for (int j = i + 1; j <= n; ++j) {
                    if (i < MIN_N && j == MAX_N) {

                    } else {
                        //for 2 digit, *100
                        if (j < 10) {
                            combinedNumMap[i][j] = combinedNumMap[i][j - 1] * 10 + j;
                        } else {
                            combinedNumMap[i][j] = combinedNumMap[i][j - 1] * 100 + j;
                        }
                    }
                }
            }

            //set to first number
            while (true) {
                ////////////////////////////////////////////////
                boolean hasNewOp = false;
                if (opFlag[opSeqOrder] < 3) {
                    cache[opSeqOrder] = opFlag[opSeqOrder];
                    ++opFlag[opSeqOrder];
                    hasNewOp = true;
                }
                /////////////////////////////////////////////////
                if (hasNewOp) {
                    //up to end
                    if (opSeqOrder == n - 2) {
                        //                        System.out.println(Arrays.toString(cache));
                        long calc = 1;
                        for (int i = 0; i < n - 1; ++i) {
                            switch (cache[i]) {
                                case 1:
                                    calc += (i + 2);
                                    break;
                                case 2:
                                    calc -= (i + 2);
                                    break;
                                case 0: {
                                    //if found space op, check if continuous
                                    int j = i + 1;
                                    while (j < n - 1) {
                                        //count space op
                                        if (0 == cache[j]) {
                                            ++j;
                                        } else {
                                            break;
                                        }
                                    }
                                    //special
                                    if (i + 1 < MIN_N && j + 1 == MAX_N) {

                                    } else {
                                        //use table
                                        long cimbniedNum = combinedNumMap[i + 1][j + 1];
                                        if (i == 0) {
                                            calc = cimbniedNum;
                                        } else {
                                            switch (cache[i - 1]) {
                                                case 1:
                                                    //                                System.out.println(calc);
                                                    calc -= (i + 1);
                                                    //                                System.out.println(calc);
                                                    calc += cimbniedNum;
                                                    //                                System.out.println(calc);
                                                    break;
                                                case 2:
                                                    //                                System.out.println(calc);
                                                    calc += (i + 1);
                                                    //                                System.out.println(calc);
                                                    calc -= cimbniedNum;
                                                    //                                System.out.println(calc);
                                                    break;
                                            }
                                        }
                                    }

                                }
                                    break;
                            }
                        }
                        if (calc == 0) {
                            result.append(1);
                            for (int i = 0; i < n - 1; ++i) {
                                //construct string
                                switch (cache[i]) {
                                    case 1:
                                        result.append('+');
                                        break;
                                    case 2:
                                        result.append('-');
                                        break;
                                    case 0:
                                        result.append(' ');
                                        break;
                                }
                                result.append((i + 2));
                            }
                            result.append(';');
                        }

                    } else {
                        ++opSeqOrder;
                    }
                } else {
                    //                    System.out.println("opSeqOrder:" + opSeqOrder);
                    if (opSeqOrder - 1 < 0) {
                        //                        System.out.println("all checked!");
                        break;
                    } else {
                        //clear
                        opFlag[opSeqOrder] = 0;
                        --opSeqOrder;
                    }

                }

            }
            if (result.length() == 0) {
                return "NOTFOUND";
            }
            return result.substring(0, result.length() - 1);
        } else if (ch == 'R') {
            return "NOTFOUND";
        } else {
            return "ILLEGAL";
        }

        //        return result.toString();
    }

    public static String run3(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        char ch = arg.charAt(0);
        if (ch == 'S') {
            //3<=N<=15
            //TODO write a table contains 3~15???
            int n = 0;
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            final int MAX_N = 15;
            final int MIN_N = 3;

            //3<=N<=15
            if (n < MIN_N || n > MAX_N) {
                return "ILLEGAL";
            }
            ///////////////////////////////////////////////////////////////
            //various buffer size
            StringBuilder result = new StringBuilder(1 << n);
            //' '：0
            //+：1
            //-： 2
            //brute force
            //for current op idx
            char [] opFlag = new char[n - 1];
            //op sequence cache
            char [] cache = new char[n - 1];
            //first op idx
            int opSeqOrder = 0; //
            //row: startNum
            //column:endNum
            long [][] combinedNumMap = new long[n + 1][n + 1];
            for (int i = 1; i <= n; ++i) {
                combinedNumMap[i][i] = i;
                for (int j = i + 1; j <= n; ++j) {
                    if (i < MIN_N && j == MAX_N) {

                    } else {
                        //for 2 digit, *100
                        if (j < 10) {
                            combinedNumMap[i][j] = combinedNumMap[i][j - 1] * 10 + j;
                        } else {
                            combinedNumMap[i][j] = combinedNumMap[i][j - 1] * 100 + j;
                        }
                    }
                }
            }

            //set to first number
            while (true) {
                ////////////////////////////////////////////////
                boolean hasNewOp = false;
                if (opFlag[opSeqOrder] < 3) {
                    cache[opSeqOrder] = opFlag[opSeqOrder];
                    ++opFlag[opSeqOrder];
                    hasNewOp = true;
                }
                /////////////////////////////////////////////////
                if (hasNewOp) {
                    //up to end
                    if (opSeqOrder == n - 2) {
                        //                        System.out.println(Arrays.toString(cache));
                        long calc = 1;
                        for (int i = 0; i < n - 1; ++i) {
                            switch (cache[i]) {
                                case 1:
                                    calc += (i + 2);
                                    break;
                                case 2:
                                    calc -= (i + 2);
                                    break;
                                case 0: {
                                    //if found space op, check if continuous
                                    int j = i + 1;
                                    while (j < n - 1) {
                                        //count space op
                                        if (0 == cache[j]) {
                                            ++j;
                                        } else {
                                            break;
                                        }
                                    }
                                    //special
                                    if (i + 1 < MIN_N && j + 1 == MAX_N) {

                                    } else {
                                        //use table
                                        long cimbniedNum = combinedNumMap[i + 1][j + 1];
                                        if (i == 0) {
                                            calc = cimbniedNum;
                                        } else {
                                            switch (cache[i - 1]) {
                                                case 1:
                                                    //                                System.out.println(calc);
                                                    calc -= (i + 1);
                                                    //                                System.out.println(calc);
                                                    calc += cimbniedNum;
                                                    //                                System.out.println(calc);
                                                    break;
                                                case 2:
                                                    //                                System.out.println(calc);
                                                    calc += (i + 1);
                                                    //                                System.out.println(calc);
                                                    calc -= cimbniedNum;
                                                    //                                System.out.println(calc);
                                                    break;
                                            }
                                        }
                                    }

                                }
                                    break;
                            }
                        }
                        if (calc == 0) {
                            result.append(1);
                            for (int i = 0; i < n - 1; ++i) {
                                //construct string
                                switch (cache[i]) {
                                    case 1:
                                        result.append('+');
                                        break;
                                    case 2:
                                        result.append('-');
                                        break;
                                    case 0:
                                        result.append(' ');
                                        break;
                                }
                                result.append((i + 2));
                            }
                            result.append(';');
                        }

                    } else {
                        ++opSeqOrder;
                    }
                } else {
                    //                    System.out.println("opSeqOrder:" + opSeqOrder);
                    if (opSeqOrder - 1 < 0) {
                        //                        System.out.println("all checked!");
                        break;
                    } else {
                        //clear
                        opFlag[opSeqOrder] = 0;
                        --opSeqOrder;
                    }

                }

            }
            if (result.length() == 0) {
                return "NOTFOUND";
            }
            return result.substring(0, result.length() - 1);
        } else if (ch == 'R') {
            String asdStr = arg.substring(1);
            String [] numberStr = asdStr.split(",");
            int [] numbers = new int[numberStr.length];
            int pre = Integer.MIN_VALUE;
            try {
                for (int i = 0; i < numbers.length; ++i) {
                    if (numberStr[i].isEmpty() || numberStr[i].charAt(0) > '9') {
                        //not a number
                        return "ILLEGAL";
                    }
                    numbers[i] = Integer.parseInt(numberStr[i]);
                    if (numbers[i] < 0 || numbers[i] < pre) {
                        //not larger than previous
                        return "ILLEGAL";
                    }
                    pre = numbers[i];
                }
            } catch (Exception e) {
                return "ILLEGAL";
            }
            //test
            //            System.out.println(Arrays.toString(numbers));
            //            int maxdigit = (asdStr.length() - numberStr.length + 1) / 2;
            //            System.out.println("max digit num:" + maxdigit);

            //consider the max digit num of single num is the length of highest number
            //            final int MAX_SINGLE_NUM_WIDTH = numberStr[numberStr.length - 1].length();
            //            final int MAX_COMBINED_NUM = (numberStr.length * MAX_SINGLE_NUM_WIDTH) / 2;
            final int MAX = numberStr.length / 2 + 1;
            //            System.out.println("MAX_COMBINED_NUM:" + MAX_COMBINED_NUM);
            //            System.out.println("MAX:" + MAX);

            //row: startNum
            //column:endNum
            //save the index of number
            long [][] combinedNumMap = new long[numbers.length + 1][numbers.length + 1];
            for (int i = 0; i < numbers.length; ++i) {
                combinedNumMap[i][i] = numbers[i];
                for (int j = i + 1; j < numbers.length; ++j) {
                    if ((j - i + 1) <= MAX) {
                        combinedNumMap[i][j] = numbers[j];
                        long cur = combinedNumMap[i][j];
                        int c = 1;
                        do {
                            cur /= 10;
                            c *= 10;
                        } while (cur > 0);
                        combinedNumMap[i][j] = combinedNumMap[i][j - 1] * c + combinedNumMap[i][j];
                    } else {
                        combinedNumMap[i][j] = 0;
                    }
                }
            }
            //for test
            //            for (int i = 0; i < numbers.length; ++i) {
            //                System.out.print(numbers[i] + " ");
            //                for (int j = i + 1; j < numbers.length; ++j) {
            //                    if (combinedNumMap[i][j] != 0)
            //                        System.out.print(combinedNumMap[i][j] + " ");
            //                    else {
            //                        System.out.print("  ");
            //                    }
            //                }
            //                System.out.println();
            //            }
            //            System.out.println();
            //
            //
            //various buffer size
            StringBuilder result = new StringBuilder(1 << numberStr.length);
            //' '：0
            //+：1
            //-： 2
            //brute force
            //for current op idx
            char [] opFlag = new char[numbers.length - 1];
            //op sequence cache
            //TODO
            int [] cache = new int[numbers.length - 1];
            //            char [] cache = new char[numbers.length - 1];
            //first op idx
            int opSeqOrder = 0; //

            //set to first number
            while (true) {
                ////////////////////////////////////////////////
                boolean hasNewOp = false;
                if (opFlag[opSeqOrder] < 3) {
                    cache[opSeqOrder] = opFlag[opSeqOrder];
                    ++opFlag[opSeqOrder];
                    hasNewOp = true;
                }
                /////////////////////////////////////////////////
                if (hasNewOp) {
                    //up to end
                    if (opSeqOrder == cache.length - 1) {
                        //                        System.out.println(Arrays.toString(cache));
                        long calc = numbers[0];
                        boolean isSuccess = true;
                        //next number: opIdx + 1
                        for (int opIdx = 0; opIdx < cache.length; ++opIdx) {
                            switch (cache[opIdx]) {
                                case 1:
                                    calc += numbers[opIdx + 1];
                                    break;
                                case 2:
                                    calc -= numbers[opIdx + 1];
                                    break;
                                case 0: {
                                    //if found space op, check if continuous                                    
                                    int nextOpIdx = opIdx + 1;
                                    int c = 1;
                                    //count space op
                                    while (nextOpIdx < cache.length) {
                                        if (0 == cache[nextOpIdx]) {
                                            ++c;
                                            ++nextOpIdx;
                                        } else {
                                            break;
                                        }
                                    }
                                    if (c + 1 > MAX) {
                                        isSuccess = false;
                                        //the space is too long
                                        break;
                                    }
                                    //special,use table
                                    long cimbinedNum = combinedNumMap[opIdx][opIdx + c];
                                    if (cimbinedNum > 0) {
                                        //                                        System.out.println("space count:" + c + ", op:" + opIdx + ",nextop:"
                                        //                                                + (opIdx + c - 1)
                                        //                                                + ", combined:"
                                        //                                                + cimbinedNum);
                                        if (opIdx == 0) {
                                            calc = cimbinedNum;
                                        } else {
                                            switch (cache[opIdx - 1]) {
                                                case 1:
                                                    //                                System.out.println(calc);
                                                    calc -= numbers[opIdx];
                                                    //                                System.out.println(calc);
                                                    calc += cimbinedNum;
                                                    //                                System.out.println(calc);
                                                    break;
                                                case 2:
                                                    //                                System.out.println(calc);
                                                    calc += numbers[opIdx];
                                                    //                                System.out.println(calc);
                                                    calc -= cimbinedNum;
                                                    //                                System.out.println(calc);
                                                    break;
                                            }
                                        }
                                    }
                                    opIdx = nextOpIdx - 1;
                                    break;
                                }
                            }
                            if (!isSuccess) {
                                calc = -1;
                                break;
                            }
                        }
                        //                        System.out.println("sum:" + calc);
                        //                        System.out.println(Arrays.toString(cache));
                        //                        System.out.println();
                        if (calc == 0) {
                            result.append(numbers[0]);
                            for (int i = 0; i < cache.length; ++i) {
                                //construct string
                                switch (cache[i]) {
                                    case 1:
                                        result.append('+');
                                        break;
                                    case 2:
                                        result.append('-');
                                        break;
                                    case 0:
                                        result.append(' ');
                                        break;
                                }
                                result.append(numbers[i + 1]);
                            }
                            result.append(';');
                            //                            System.out.println(result);
                            //                            System.out.println();
                        }

                    } else {
                        ++opSeqOrder;
                    }
                } else {
                    //                    System.out.println("opSeqOrder:" + opSeqOrder);
                    if (opSeqOrder - 1 < 0) {
                        //                        System.out.println("all checked!");
                        break;
                    } else {
                        //clear
                        opFlag[opSeqOrder] = 0;
                        --opSeqOrder;
                    }

                }

            }
            if (result.length() == 0) {
                return "NOTFOUND";
            }
            return result.substring(0, result.length() - 1);
        } else {
            return "ILLEGAL";
        }

        //        return result.toString();
    }

    //BF
    public static String run4BF(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        StringBuilder result = null;
        int [] numbers = null;
        int MAX = 0;
        char ch = arg.charAt(0);
        if (ch == 'S') {
            //TODO write a table contains 3~15???
            int n = 0;
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            //3<=N<=15
            if (n < 3 || n > 15) {
                return "ILLEGAL";
            }
            numbers = new int[n];
            for (int i = 0; i < numbers.length; ++i) {
                numbers[i] = i + 1;
            }
            //various buffer size
            result = new StringBuilder(2 << n);
            //            int bufferSize=(1<<(n+1));
            //            if(n>12){
            //                bufferSize=50000;
            //            }else if(n>7){
            //                bufferSize=10000;
            //            }
            //            result = new StringBuilder();
            MAX = n - 1;
        } else if (ch == 'R') {
            String asdStr = arg.substring(1);
            String [] numberStr = asdStr.split(",");
            numbers = new int[numberStr.length];
            int pre = Integer.MIN_VALUE;
            try {
                for (int i = 0; i < numbers.length; ++i) {
                    if (numberStr[i].isEmpty() || numberStr[i].charAt(0) > '9') {
                        //not a number
                        return "ILLEGAL";
                    }
                    numbers[i] = Integer.parseInt(numberStr[i]);
                    if (numbers[i] < 0 || numbers[i] < pre) {
                        //not larger than previous
                        return "ILLEGAL";
                    }
                    pre = numbers[i];
                }
            } catch (Exception e) {
                return "ILLEGAL";
            }
            result = new StringBuilder(128);
            MAX = (numberStr.length * numberStr[numberStr.length - 1].length()) / 2;
            if (MAX > 16) {
                //!!!have to use array for number
                return "ILLEGAL";
            }
        } else {
            return "ILLEGAL";
        }
        if (numbers == null || result == null) {
            return "ILLEGAL";
        }

        //test
        //            System.out.println(Arrays.toString(numbers));
        //            int maxdigit = (asdStr.length() - numberStr.length + 1) / 2;
        //            System.out.println("max digit num:" + maxdigit);

        //consider the max digit num of single num is the length of highest number
        //            final int MAX_SINGLE_NUM_WIDTH = numberStr[numberStr.length - 1].length();
        //            final int MAX_COMBINED_NUM = (numberStr.length * MAX_SINGLE_NUM_WIDTH) / 2;

        //            System.out.println("MAX_COMBINED_NUM:" + MAX_COMBINED_NUM);
        //            System.out.println("MAX:" + MAX);

        //row: startNum
        //column:endNum
        //save the index of number
        long [][] combinedNumMap = new long[numbers.length][numbers.length];
        for (int i = 0; i < numbers.length; ++i) {
            combinedNumMap[i][i] = numbers[i];
            for (int j = i + 1; j < numbers.length; ++j) {
                if ((j - i + 1) <= MAX) {
                    combinedNumMap[i][j] = numbers[j];
                    long cur = combinedNumMap[i][j];
                    int c = 1;
                    do {
                        cur /= 10;
                        c *= 10;
                    } while (cur > 0);
                    combinedNumMap[i][j] = combinedNumMap[i][j - 1] * c + combinedNumMap[i][j];
                } else {
                    combinedNumMap[i][j] = 0;
                }
            }
        }
        //' '：0
        //+：1
        //-： 2
        //brute force
        //for current op idx
        char [] opFlag = new char[numbers.length - 1];
        //op sequence cache
        char [] cache = new char[numbers.length - 1];
        //first op idx
        int opSeqOrder = 0; //
        //set to first number
        while (true) {
            ////////////////////////////////////////////////
            boolean hasNewOp = false;
            if (opFlag[opSeqOrder] < 3) {
                cache[opSeqOrder] = opFlag[opSeqOrder];
                ++opFlag[opSeqOrder];
                hasNewOp = true;
            }
            /////////////////////////////////////////////////
            if (hasNewOp) {
                //up to end
                if (opSeqOrder == cache.length - 1) {
                    //                        System.out.println(Arrays.toString(cache));
                    long calc = numbers[0];
                    boolean isSuccess = true;
                    //next number: opIdx + 1
                    for (int opIdx = 0; opIdx < cache.length; ++opIdx) {
                        switch (cache[opIdx]) {
                            case 1:
                                calc += numbers[opIdx + 1];
                                break;
                            case 2:
                                calc -= numbers[opIdx + 1];
                                break;
                            case 0: {
                                //if found space op, check if continuous                                    
                                int nextOpIdx = opIdx + 1;
                                int c = 1;
                                //count space op
                                while (nextOpIdx < cache.length) {
                                    if (0 == cache[nextOpIdx]) {
                                        ++c;
                                        ++nextOpIdx;
                                    } else {
                                        break;
                                    }
                                }
                                if (c + 1 > MAX) {
                                    isSuccess = false;
                                    //the space is too long
                                    break;
                                }
                                //special,use table
                                long cimbinedNum = combinedNumMap[opIdx][opIdx + c];
                                if (cimbinedNum > 0) {
                                    //                                        System.out.println("space count:" + c + ", op:" + opIdx + ",nextop:"
                                    //                                                + (opIdx + c - 1)
                                    //                                                + ", combined:"
                                    //                                                + cimbinedNum);
                                    if (opIdx == 0) {
                                        calc = cimbinedNum;
                                    } else {
                                        switch (cache[opIdx - 1]) {
                                            case 1:
                                                //                                System.out.println(calc);
                                                calc -= numbers[opIdx];
                                                //                                System.out.println(calc);
                                                calc += cimbinedNum;
                                                //                                System.out.println(calc);
                                                break;
                                            case 2:
                                                //                                System.out.println(calc);
                                                calc += numbers[opIdx];
                                                //                                System.out.println(calc);
                                                calc -= cimbinedNum;
                                                //                                System.out.println(calc);
                                                break;
                                        }
                                    }
                                }
                                opIdx = nextOpIdx - 1;
                                break;
                            }
                        }
                        if (!isSuccess) {
                            calc = -1;
                            break;
                        }
                    }
                    //                        System.out.println("sum:" + calc);
                    //                        System.out.println(Arrays.toString(cache));
                    //                        System.out.println();
                    if (calc == 0) {
                        result.append(numbers[0]);
                        for (int i = 0; i < cache.length; ++i) {
                            //construct string
                            switch (cache[i]) {
                                case 1:
                                    result.append('+');
                                    break;
                                case 2:
                                    result.append('-');
                                    break;
                                case 0:
                                    result.append(' ');
                                    break;
                            }
                            result.append(numbers[i + 1]);
                        }
                        result.append(';');
                        //                            System.out.println(result);
                        //                            System.out.println();
                    }

                } else {
                    ++opSeqOrder;
                }
            } else {
                //                    System.out.println("opSeqOrder:" + opSeqOrder);
                if (opSeqOrder - 1 < 0) {
                    //                        System.out.println("all checked!");
                    break;
                } else {
                    //clear
                    opFlag[opSeqOrder] = 0;
                    --opSeqOrder;
                }

            }

        }
        if (result.length() == 0) {
            return "NOTFOUND";
        }
        return result.substring(0, result.length() - 1);
    }

    public static void testAllS() {
        double totalTime = 0;
        totalTime += test("S3", "1+2-3", 1);
        totalTime += test("S4", "1-2-3+4", 1);
        totalTime += test("S5", "1 2-3-4-5", 1);
        totalTime += test("S6", "1 2+3-4-5-6", 1);
        totalTime += test("S7", "1+2-3+4-5-6+7;1+2-3-4+5+6-7;1-2 3+4+5+6+7;1-2 3-4 5+6 7;1-2+3+4-5+6-7;1-2-3-4-5+6+7",
                1);
        totalTime += test(
                "S8",
                "1 2-3 4-5 6+7 8;1+2 3-4 5+6+7+8;1+2+3+4-5-6-7+8;1+2+3-4+5-6+7-8;1+2-3+4+5+6-7-8;1+2-3-4-5-6+7+8;1-2 3-4+5+6+7+8;1-2+3-4-5+6-7+8;1-2-3+4+5-6-7+8;1-2-3+4-5+6+7-8",
                1);
        totalTime += test(
                "S9",
                "1 2+3 4-5 6-7+8+9;1 2+3+4-5-6-7+8-9;1 2+3-4 5+6+7+8+9;1 2+3-4+5-6+7-8-9;1 2-3+4+5 6-7 8+9;1 2-3+4+5+6-7-8-9;1 2-3-4-5+6-7-8+9;1 2-3-4-5-6+7+8-9;1+2-3 4-5 6+7 8+9;1-2 3-4-5 6-7+8 9;1-2-3 4+5+6+7+8+9",
                1);

        totalTime += test(
                "S10",
                "1 2 3-4 5-6 7+8-9-10;1 2 3-4 5-6+7-8 9+10;1 2+3+4 5-6 7+8+9-10;1 2+3+4+5-6-7+8-9-10;1 2+3+4-5+6+7-8-9-10;1 2+3-4-5-6+7-8-9+10;1 2+3-4-5-6-7+8+9-10;1 2-3 4+5 6-7-8-9-10;1 2-3+4-5+6-7-8-9+10;1 2-3+4-5-6+7-8+9-10;1 2-3-4+5+6-7-8+9-10;1 2-3-4+5-6+7+8-9-10;1+2+3 4-5+6 7-8 9-10;1+2-3 4+5+6-7+8+9+10;1-2+3 4-5+6-7-8-9-10;1-2-3 4-5+6 7-8-9-10;1-2-3 4-5+6+7+8+9+10",
                1);
        totalTime += test(
                "S11",
                "1 2 3-4-5-6-7 8-9-10-11;1 2+3 4+5-6 7+8+9+10-11;1 2+3 4+5-6-7-8-9-10-11;1 2+3+4 5+6-7 8-9+10+11;1 2+3+4+5 6-7-8 9+10+11;1 2+3-4-5 6+7+8+9+10+11;1 2-3 4+5+6+7-8-9+10+11;1 2-3 4+5+6-7+8+9-10+11;1 2-3 4+5-6+7+8+9+10-11;1 2-3+4 5-6-7 8+9+10+11;1 2-3-4+5 6+7-8 9+10+11;1+2 3+4 5+6-7-8 9+10+11;1+2 3+4 5-6 7+8-9+10-11;1+2 3+4+5-6-7-8+9-10-11;1+2 3+4-5+6 7-8 9+10-11;1+2 3+4-5+6-7+8-9-10-11;1+2 3-4 5+6-7-8+9+10+11;1+2 3-4 5-6 7+8 9+10-11;1+2 3-4 5-6+7+8-9+10+11;1+2 3-4+5+6+7-8-9-10-11;1+2 3-4-5-6+7-8-9-10+11;1+2 3-4-5-6-7+8-9+10-11;1+2+3 4+5+6-7 8+9+10+11;1+2+3+4 5-6 7+8+9+10-11;1+2+3+4 5-6-7-8-9-10-11;1+2+3+4+5 6-7 8-9+10+11;1+2+3+4+5-6+7-8-9-10+11;1+2+3+4+5-6-7+8-9+10-11;1+2+3+4-5+6+7-8-9+10-11;1+2+3+4-5+6-7+8+9-10-11;1+2+3-4 5-6+7+8+9+10+11;1+2+3-4+5+6+7-8+9-10-11;1+2+3-4-5+6-7-8-9+10+11;1+2+3-4-5-6+7-8+9-10+11;1+2+3-4-5-6-7+8+9+10-11;1+2-3 4 5-6 7 8+9+10 11;1+2-3 4+5 6-7-8-9+10-11;1+2-3+4 5-6 7-8+9+10+11;1+2-3+4+5+6+7+8-9-10-11;1+2-3+4+5-6-7-8-9+10+11;1+2-3+4-5+6-7-8+9-10+11;1+2-3+4-5-6 7+8 9-10-11;1+2-3+4-5-6+7+8-9-10+11;1+2-3+4-5-6+7-8+9+10-11;1+2-3-4 5+6 7+8-9-10-11;1+2-3-4+5+6 7-8 9+10+11;1+2-3-4+5+6-7+8-9-10+11;1+2-3-4+5+6-7-8+9+10-11;1+2-3-4+5-6+7+8-9+10-11;1+2-3-4-5+6+7+8+9-10-11;1+2-3-4-5-6-7-8+9+10+11;1-2 3 4+5+6-7 8 9+10 11;1-2 3+4 5+6 7-8 9+10-11;1-2 3+4 5+6-7+8-9-10-11;1-2 3+4+5+6+7+8-9-10+11;1-2 3+4+5+6+7-8+9+10-11;1-2 3+4-5-6 7+8 9-10+11;1-2 3+4-5-6+7-8+9+10+11;1-2 3-4 5+6 7+8-9-10+11;1-2 3-4 5+6 7-8+9+10-11;1-2 3-4 5+6-7+8 9-10-11;1-2 3-4+5+6-7-8+9+10+11;1-2 3-4+5-6 7+8 9+10-11;1-2 3-4+5-6+7+8-9+10+11;1-2 3-4-5+6+7+8+9-10+11;1-2+3 4-5 6+7+8+9+10-11;1-2+3+4 5-6 7+8-9+10+11;1-2+3+4+5-6-7-8+9-10+11;1-2+3+4-5+6 7-8 9+10+11;1-2+3+4-5+6-7+8-9-10+11;1-2+3+4-5+6-7-8+9+10-11;1-2+3+4-5-6+7+8-9+10-11;1-2+3-4 5-6 7+8 9+10+11;1-2+3-4+5+6+7-8-9-10+11;1-2+3-4+5+6-7+8-9+10-11;1-2+3-4+5-6+7+8+9-10-11;1-2+3-4-5-6-7+8-9+10+11;1-2-3+4+5+6+7-8-9+10-11;1-2-3+4+5+6-7+8+9-10-11;1-2-3+4-5-6+7-8-9+10+11;1-2-3+4-5-6-7+8+9-10+11;1-2-3-4 5+6 7-8-9+10-11;1-2-3-4+5 6-7 8+9+10+11;1-2-3-4+5+6-7-8-9+10+11;1-2-3-4+5-6+7-8+9-10+11;1-2-3-4+5-6-7+8+9+10-11;1-2-3-4-5+6+7+8-9-10+11;1-2-3-4-5+6+7-8+9+10-11",
                1);
        totalTime += test(
                "S12",
                "1 2 3+4-5 6+7-8 9+10-11+12;1 2 3-4 5 6-7 8 9+10+11 12;1 2 3-4-5 6-7-8 9+10+11+12;1 2 3-4-5+6-7 8-9-10-11-12;1 2+3 4+5 6-7 8+9-10-11-12;1 2+3 4+5+6-7-8-9-10-11-12;1 2+3 4+5-6 7-8-9+10+11+12;1 2+3 4-5-6 7+8+9+10+11-12;1 2+3 4-5-6-7-8-9-10+11-12;1 2+3+4 5-6-7 8-9+10+11+12;1 2+3+4-5 6+7+8+9-10+11+12;1 2+3-4 5-6+7 8-9-10-11-12;1 2-3 4+5+6 7-8-9-10-11-12;1 2-3 4+5+6+7+8+9+10-11-12;1 2-3 4+5-6+7-8-9+10+11+12;1 2-3 4+5-6-7+8+9-10+11+12;1 2-3 4-5 6+7 8+9-10-11+12;1 2-3 4-5 6+7 8-9+10+11-12;1 2-3 4-5+6+7-8+9-10+11+12;1 2-3 4-5+6-7+8+9+10-11+12;1 2-3 4-5-6+7+8+9+10+11-12;1 2-3+4 5+6-7 8+9+10+11-12;1 2-3+4+5 6+7-8 9-10+11+12;1 2-3+4-5 6-7+8+9+10+11+12;1+2 3 4+5-6+7 8 9-10 11-12;1+2 3+4 5-6-7-8 9+10+11+12;1+2 3+4+5+6 7-8 9-10+11-12;1+2 3+4+5+6-7-8+9-10-11-12;1+2 3+4+5-6 7-8+9+10+11+12;1+2 3+4+5-6+7+8-9-10-11-12;1+2 3+4-5-6-7+8-9-10-11+12;1+2 3+4-5-6-7-8+9-10+11-12;1+2 3-4 5+6+7+8+9-10-11+12;1+2 3-4 5+6+7+8-9+10+11-12;1+2 3-4 5-6-7-8+9+10+11+12;1+2 3-4+5 6-7 8-9+10-11+12;1+2 3-4+5-6+7-8-9-10-11+12;1+2 3-4+5-6-7+8-9-10+11-12;1+2 3-4+5-6-7-8+9+10-11-12;1+2 3-4-5 6+7 8-9-10-11-12;1+2 3-4-5+6+7-8-9-10+11-12;1+2 3-4-5+6-7+8-9+10-11-12;1+2 3-4-5-6+7+8+9-10-11-12;1+2+3 4+5 6+7-8 9-10+11-12;1+2+3 4+5-6-7 8+9+10+11+12;1+2+3 4-5 6+7+8-9-10+11+12;1+2+3 4-5 6+7-8+9+10-11+12;1+2+3 4-5 6-7+8+9+10+11-12;1+2+3+4 5+6-7-8-9-10-11-12;1+2+3+4 5-6 7-8-9+10+11+12;1+2+3+4+5+6+7-8-9-10+11-12;1+2+3+4+5+6-7+8-9+10-11-12;1+2+3+4+5-6+7+8+9-10-11-12;1+2+3+4-5+6-7-8-9-10+11+12;1+2+3+4-5-6+7-8-9+10-11+12;1+2+3+4-5-6-7+8+9-10-11+12;1+2+3+4-5-6-7+8-9+10+11-12;1+2+3-4 5+6 7-8-9-10+11-12;1+2+3-4 5+6+7+8+9+10+11-12;1+2+3-4+5 6-7 8+9+10-11+12;1+2+3-4+5+6-7-8-9+10-11+12;1+2+3-4+5-6+7-8+9-10-11+12;1+2+3-4+5-6+7-8-9+10+11-12;1+2+3-4+5-6-7+8+9-10+11-12;1+2+3-4-5 6+7 8+9-10-11-12;1+2+3-4-5+6+7+8-9-10-11+12;1+2+3-4-5+6+7-8+9-10+11-12;1+2+3-4-5+6-7+8+9+10-11-12;1+2+3-4-5-6-7-8-9+10+11+12;1+2-3 4+5 6+7-8+9-10-11-12;1+2-3 4-5 6+7+8 9-10-11+12;1+2-3+4 5-6-7-8+9-10-11-12;1+2-3+4+5 6-7 8+9+10+11-12;1+2-3+4+5+6 7-8 9-10+11+12;1+2-3+4+5+6-7-8+9-10-11+12;1+2-3+4+5+6-7-8-9+10+11-12;1+2-3+4+5-6+7+8-9-10-11+12;1+2-3+4+5-6+7-8+9-10+11-12;1+2-3+4+5-6-7+8+9+10-11-12;1+2-3+4-5+6+7+8-9-10+11-12;1+2-3+4-5+6+7-8+9+10-11-12;1+2-3+4-5-6-7-8+9-10+11+12;1+2-3-4 5+6+7+8-9+10+11+12;1+2-3-4+5+6+7+8-9+10-11-12;1+2-3-4+5-6-7+8-9-10+11+12;1+2-3-4+5-6-7-8+9+10-11+12;1+2-3-4-5 6+7 8-9-10-11+12;1+2-3-4-5+6+7-8-9-10+11+12;1+2-3-4-5+6-7+8-9+10-11+12;1+2-3-4-5+6-7-8+9+10+11-12;1+2-3-4-5-6 7+8 9+10-11-12;1+2-3-4-5-6+7+8+9-10-11+12;1+2-3-4-5-6+7+8-9+10+11-12;1-2 3 4+5-6-7 8 9+10 11+12;1-2 3+4 5-6-7+8-9-10-11+12;1-2 3+4 5-6-7-8+9-10+11-12;1-2 3+4+5+6-7-8+9-10+11+12;1-2 3+4+5-6 7+8 9-10-11+12;1-2 3+4+5-6+7+8-9-10+11+12;1-2 3+4+5-6+7-8+9+10-11+12;1-2 3+4+5-6-7+8+9+10+11-12;1-2 3+4-5 6+7 8+9+10-11-12;1-2 3+4-5+6+7+8-9+10-11+12;1-2 3+4-5+6+7-8+9+10+11-12;1-2 3-4 5-6-7+8 9-10-11+12;1-2 3-4+5+6+7+8+9-10-11+12;1-2 3-4+5+6+7+8-9+10+11-12;1-2 3-4+5-6-7-8+9+10+11+12;1-2 3-4-5 6+7 8-9-10+11+12;1-2 3-4-5+6-7+8-9+10+11+12;1-2 3-4-5-6 7+8 9+10+11-12;1-2 3-4-5-6+7+8+9-10+11+12;1-2+3 4-5 6+7-8-9+10+11+12;1-2+3 4-5 6-7+8+9-10+11+12;1-2+3+4 5-6-7+8-9-10-11-12;1-2+3+4+5+6 7-8 9+10-11+12;1-2+3+4+5+6-7+8-9-10-11+12;1-2+3+4+5+6-7-8+9-10+11-12;1-2+3+4+5-6 7+8 9-10-11-12;1-2+3+4+5-6+7+8-9-10+11-12;1-2+3+4+5-6+7-8+9+10-11-12;1-2+3+4-5+6+7+8-9+10-11-12;1-2+3+4-5-6-7+8-9-10+11+12;1-2+3+4-5-6-7-8+9+10-11+12;1-2+3-4 5+6+7+8+9-10+11+12;1-2+3-4 5-6-7+8 9-10-11-12;1-2+3-4+5 6-7 8-9+10+11+12;1-2+3-4+5+6+7+8+9-10-11-12;1-2+3-4+5-6+7-8-9-10+11+12;1-2+3-4+5-6-7+8-9+10-11+12;1-2+3-4+5-6-7-8+9+10+11-12;1-2+3-4-5 6+7 8-9-10+11-12;1-2+3-4-5+6+7-8-9+10-11+12;1-2+3-4-5+6-7+8+9-10-11+12;1-2+3-4-5+6-7+8-9+10+11-12;1-2+3-4-5-6+7+8+9-10+11-12;1-2-3 4+5 6+7-8-9-10+11-12;1-2-3 4+5 6-7+8-9+10-11-12;1-2-3 4+5-6+7 8-9-10-11-12;1-2-3 4-5 6-7+8 9+10+11-12;1-2-3+4 5-6 7+8+9+10+11-12;1-2-3+4 5-6-7-8-9-10+11-12;1-2-3+4+5 6-7 8+9-10+11+12;1-2-3+4+5+6-7-8-9-10+11+12;1-2-3+4+5-6+7-8-9+10-11+12;1-2-3+4+5-6-7+8+9-10-11+12;1-2-3+4+5-6-7+8-9+10+11-12;1-2-3+4-5 6+7 8-9+10-11-12;1-2-3+4-5+6+7-8+9-10-11+12;1-2-3+4-5+6+7-8-9+10+11-12;1-2-3+4-5+6-7+8+9-10+11-12;1-2-3+4-5-6+7+8+9+10-11-12;1-2-3-4 5+6-7+8+9+10+11+12;1-2-3-4+5+6+7+8-9-10-11+12;1-2-3-4+5+6+7-8+9-10+11-12;1-2-3-4+5+6-7+8+9+10-11-12;1-2-3-4+5-6-7-8-9+10+11+12;1-2-3-4-5+6-7-8+9-10+11+12;1-2-3-4-5-6 7+8 9-10-11+12;1-2-3-4-5-6+7+8-9-10+11+12;1-2-3-4-5-6+7-8+9+10-11+12;1-2-3-4-5-6-7+8+9+10+11-12",
                1);
        totalTime += test(
                "S13",
                "1 2 3 4-5 6-7 8+9-10-11 12+13;1 2 3+4 5+6+7+8+9 10-11 12+13;1 2 3+4+5-6 7-8 9+10-11+12+13;1 2 3+4-5 6-7 8+9+10-11+12-13;1 2 3+4-5+6+7-8 9-10-11-12-13;1 2 3+4-5-6-7-8 9-10-11-12+13;1 2 3-4 5+6+7-8 9+10-11+12-13;1 2 3-4 5-6-7-8 9+10-11+12+13;1 2 3-4+5-6-7-8 9-10-11+12-13;1 2 3-4-5 6-7 8-9+10-11+12+13;1 2 3-4-5+6 7+8+9 10-11 12+13;1 2 3-4-5+6-7+8 9+10 11-12 13;1 2 3-4-5+6-7-8 9-10+11-12-13;1 2 3-4-5-6 7+8-9-10-11-12-13;1 2 3-4-5-6+7-8 9+10-11-12-13;1 2+3 4+5 6+7-8 9-10-11-12+13;1 2+3 4+5-6-7 8+9+10-11+12+13;1 2+3 4-5 6+7+8-9-10-11+12+13;1 2+3 4-5 6+7-8+9-10+11-12+13;1 2+3 4-5 6+7-8-9+10+11+12-13;1 2+3 4-5 6-7+8+9+10-11-12+13;1 2+3 4-5 6-7+8+9-10+11+12-13;1 2+3 4-5+6-7 8+9+10+11-12+13;1 2+3+4 5-6 7-8-9+10-11+12+13;1 2+3+4 5-6-7+8-9-10-11-12-13;1 2+3+4+5+6 7-8 9+10-11+12-13;1 2+3+4+5+6+7-8-9-10-11-12+13;1 2+3+4+5+6-7+8-9-10-11+12-13;1 2+3+4+5+6-7-8+9-10+11-12-13;1 2+3+4+5-6 7+8 9-10-11-12-13;1 2+3+4+5-6 7+8+9-10+11+12+13;1 2+3+4+5-6+7+8-9-10+11-12-13;1 2+3+4+5-6+7-8+9+10-11-12-13;1 2+3+4-5+6+7+8-9+10-11-12-13;1 2+3+4-5+6-7-8-9-10-11+12+13;1 2+3+4-5-6+7-8-9-10+11-12+13;1 2+3+4-5-6-7+8-9+10-11-12+13;1 2+3+4-5-6-7+8-9-10+11+12-13;1 2+3+4-5-6-7-8+9+10-11+12-13;1 2+3-4 5+6 7-8-9-10-11-12+13;1 2+3-4 5+6+7+8+9+10-11-12+13;1 2+3-4 5+6+7+8+9-10+11+12-13;1 2+3-4 5-6+7-8-9+10+11+12+13;1 2+3-4 5-6-7+8 9-10-11-12-13;1 2+3-4 5-6-7+8+9-10+11+12+13;1 2+3-4+5 6-7 8+9-10+11-12+13;1 2+3-4+5 6-7 8-9+10+11+12-13;1 2+3-4+5+6+7+8+9-10-11-12-13;1 2+3-4+5+6-7-8-9-10+11-12+13;1 2+3-4+5-6+7-8-9+10-11-12+13;1 2+3-4+5-6+7-8-9-10+11+12-13;1 2+3-4+5-6-7+8+9-10-11-12+13;1 2+3-4+5-6-7+8-9+10-11+12-13;1 2+3-4+5-6-7-8+9+10+11-12-13;1 2+3-4-5 6+7 8-9-10+11-12-13;1 2+3-4-5+6+7-8+9-10-11-12+13;1 2+3-4-5+6+7-8-9+10-11+12-13;1 2+3-4-5+6-7+8+9-10-11+12-13;1 2+3-4-5+6-7+8-9+10+11-12-13;1 2+3-4-5-6+7+8+9-10+11-12-13;1 2+3-4-5-6-7-8-9+10-11+12+13;1 2-3 4+5 6+7-8-9-10+11-12-13;1 2-3 4+5 6-7+8-9+10-11-12-13;1 2-3 4+5-6+7 8-9-10-11-12-13;1 2-3 4-5 6-7+8 9+10+11-12-13;1 2-3+4 5+6+7-8 9+10+11-12+13;1 2-3+4 5-6 7+8+9+10+11-12-13;1 2-3+4 5-6-7-8-9-10+11-12-13;1 2-3+4+5 6-7 8+9+10-11-12+13;1 2-3+4+5 6-7 8+9-10+11+12-13;1 2-3+4+5+6 7-8 9-10-11+12+13;1 2-3+4+5+6-7-8-9+10-11-12+13;1 2-3+4+5+6-7-8-9-10+11+12-13;1 2-3+4+5-6+7-8+9-10-11-12+13;1 2-3+4+5-6+7-8-9+10-11+12-13;1 2-3+4+5-6-7+8+9-10-11+12-13;1 2-3+4+5-6-7+8-9+10+11-12-13;1 2-3+4-5 6+7 8-9+10-11-12-13;1 2-3+4-5+6+7+8-9-10-11-12+13;1 2-3+4-5+6+7-8+9-10-11+12-13;1 2-3+4-5+6+7-8-9+10+11-12-13;1 2-3+4-5+6-7+8+9-10+11-12-13;1 2-3+4-5-6+7+8+9+10-11-12-13;1 2-3+4-5-6-7-8+9-10-11+12+13;1 2-3+4-5-6-7-8-9+10+11-12+13;1 2-3-4 5+6+7+8-9+10-11+12+13;1 2-3-4 5+6+7-8+9+10+11-12+13;1 2-3-4 5+6-7+8+9+10+11+12-13;1 2-3-4+5 6-7 8-9-10+11+12+13;1 2-3-4+5+6+7+8-9-10-11+12-13;1 2-3-4+5+6+7-8+9-10+11-12-13;1 2-3-4+5+6-7+8+9+10-11-12-13;1 2-3-4+5-6-7+8-9-10-11+12+13;1 2-3-4+5-6-7-8+9-10+11-12+13;1 2-3-4+5-6-7-8-9+10+11+12-13;1 2-3-4-5+6 7-8 9+10+11-12+13;1 2-3-4-5+6+7-8-9-10-11+12+13;1 2-3-4-5+6-7+8-9-10+11-12+13;1 2-3-4-5+6-7-8+9+10-11-12+13;1 2-3-4-5+6-7-8+9-10+11+12-13;1 2-3-4-5-6 7+8 9-10-11+12-13;1 2-3-4-5-6+7+8-9+10-11-12+13;1 2-3-4-5-6+7+8-9-10+11+12-13;1 2-3-4-5-6+7-8+9+10-11+12-13;1 2-3-4-5-6-7+8+9+10+11-12-13;1+2 3 4+5 6-7+8+9 10+11-12 13;1+2 3 4-5+6+7 8+9 10-11-12 13;1+2 3+4 5-6-7 8-9+10-11+12+13;1+2 3+4+5 6+7-8 9+10-11+12-13;1+2 3+4-5 6+7+8+9-10-11+12+13;1+2 3+4-5 6+7+8-9+10+11-12+13;1+2 3+4-5 6+7-8+9+10+11+12-13;1+2 3-4+5 6-7-8 9+10+11+12-13;1+2 3-4-5 6+7-8-9+10+11+12+13;1+2 3-4-5 6-7+8 9-10-11-12-13;1+2 3-4-5 6-7+8+9-10+11+12+13;1+2+3 4+5 6-7 8+9-10+11-12-13;1+2+3 4+5+6 7-8 9-10-11-12+13;1+2+3 4+5+6-7-8-9-10+11-12-13;1+2+3 4+5-6 7+8-9-10+11+12+13;1+2+3 4+5-6 7-8+9+10-11+12+13;1+2+3 4+5-6+7-8 9+10+11+12+13;1+2+3 4+5-6+7-8-9+10-11-12-13;1+2+3 4+5-6-7+8+9-10-11-12-13;1+2+3 4-5+6+7-8+9-10-11-12-13;1+2+3 4-5-6-7-8+9-10-11-12+13;1+2+3 4-5-6-7-8-9+10-11+12-13;1+2+3+4 5-6-7 8+9+10-11+12+13;1+2+3+4-5 6+7+8+9+10+11-12+13;1+2+3-4 5-6+7 8-9-10+11-12-13;1+2+3-4+5 6+7-8 9+10-11+12+13;1+2+3-4-5 6+7-8+9+10+11+12+13;1+2-3 4+5+6 7-8-9-10+11-12-13;1+2-3 4+5+6+7+8+9+10+11-12-13;1+2-3 4+5+6-7-8+9-10+11+12+13;1+2-3 4+5-6 7+8 9-10-11+12+13;1+2-3 4+5-6+7+8-9-10+11+12+13;1+2-3 4+5-6+7-8+9+10-11+12+13;1+2-3 4+5-6-7+8+9+10+11-12+13;1+2-3 4-5 6+7 8+9+10-11-12+13;1+2-3 4-5 6+7 8+9-10+11+12-13;1+2-3 4-5+6+7+8-9+10-11+12+13;1+2-3 4-5+6+7-8+9+10+11-12+13;1+2-3 4-5+6-7+8+9+10+11+12-13;1+2-3+4+5 6+7-8 9+10+11-12+13;1+2-3+4-5 6+7+8-9+10+11+12+13;1+2-3-4+5 6-7-8+9-10-11-12-13;1+2-3-4-5 6-7+8 9-10-11+12-13;1-2 3 4-5 6+7 8+9-10 11+12 13;1-2 3+4+5 6+7-8+9-10-11-12-13;1-2 3+4-5 6+7+8 9-10-11+12-13;1-2 3-4 5+6+7 8+9+10-11-12-13;1-2 3-4 5-6+7 8-9-10-11+12+13;1-2 3-4+5 6+7-8-9-10-11-12+13;1-2 3-4+5 6-7+8-9-10-11+12-13;1-2 3-4+5 6-7-8+9-10+11-12-13;1-2 3-4-5 6-7+8 9+10-11-12+13;1-2 3-4-5 6-7+8 9-10+11+12-13;1-2+3 4+5 6-7 8+9-10-11-12+13;1-2+3 4+5 6-7 8-9+10-11+12-13;1-2+3 4+5+6-7-8-9-10-11-12+13;1-2+3 4+5-6 7-8-9+10+11+12+13;1-2+3 4+5-6+7-8-9-10-11+12-13;1-2+3 4+5-6-7+8-9-10+11-12-13;1-2+3 4+5-6-7-8+9+10-11-12-13;1-2+3 4-5 6+7 8-9-10-11-12-13;1-2+3 4-5+6+7-8-9-10+11-12-13;1-2+3 4-5+6-7+8-9+10-11-12-13;1-2+3 4-5-6 7+8+9+10+11-12+13;1-2+3 4-5-6+7+8+9-10-11-12-13;1-2+3 4-5-6-7-8-9-10+11-12+13;1-2+3+4 5-6-7 8-9+10+11+12+13;1-2+3+4+5 6+7-8 9+10+11+12-13;1-2+3+4-5 6+7+8 9-10-11-12-13;1-2+3+4-5 6+7+8+9-10+11+12+13;1-2+3-4 5-6+7 8-9-10-11-12+13;1-2+3-4+5 6-7+8-9-10-11-12-13;1-2+3-4-5 6-7+8 9-10+11-12-13;1-2-3 4+5+6 7-8-9-10-11-12+13;1-2-3 4+5+6+7+8+9+10-11-12+13;1-2-3 4+5+6+7+8+9-10+11+12-13;1-2-3 4+5-6+7-8-9+10+11+12+13;1-2-3 4+5-6-7+8 9-10-11-12-13;1-2-3 4+5-6-7+8+9-10+11+12+13;1-2-3 4-5 6+7 8+9-10-11+12+13;1-2-3 4-5 6+7 8-9+10+11-12+13;1-2-3 4-5+6 7+8-9+10-11-12-13;1-2-3 4-5+6+7-8+9-10+11+12+13;1-2-3 4-5+6-7+8+9+10-11+12+13;1-2-3 4-5-6+7+8+9+10+11-12+13;1-2-3+4 5+6-7 8+9+10+11-12+13;1-2-3+4+5 6+7-8 9-10+11+12+13;1-2-3+4+5 6+7-8-9-10-11-12-13;1-2-3+4-5 6-7+8 9+10-11-12-13;1-2-3+4-5 6-7+8+9+10+11+12+13;1-2-3-4 5+6+7 8-9+10-11-12-13;1-2-3-4+5 6-7-8-9-10+11-12-13",
                1);
        totalTime += test(
                "S14",
                "1 2 3 4+5+6 7+8+9-10-11+12-13 14;1 2 3 4+5+6 7+8-9+10+11-12-13 14;1 2 3 4+5+6-7+8 9+10-11-12-13 14;1 2 3 4+5+6-7-8 9-10-11 12-13-14;1 2 3 4-5-6-7+8 9+10+11-12-13 14;1 2 3+4 5+6-7+8+9 10-11 12+13+14;1 2 3+4 5-6 7-8 9+10-11-12-13+14;1 2 3+4 5-6 7-8 9-10+11-12+13-14;1 2 3+4+5 6+7 8 9-10 11+12+13+14;1 2 3+4+5+6 7+8+9+10 11-12 13-14;1 2 3+4-5 6-7 8-9-10+11-12+13+14;1 2 3+4-5+6 7+8-9+10 11-12 13+14;1 2 3+4-5+6-7-8 9-10-11-12-13+14;1 2 3+4-5-6 7-8 9+10+11+12-13+14;1 2 3+4-5-6 7-8-9-10+11-12-13-14;1 2 3+4-5-6+7+8 9-10+11 12-13 14;1 2 3+4-5-6+7-8 9-10-11-12+13-14;1 2 3-4 5+6-7-8 9+10-11+12-13+14;1 2 3-4 5+6-7-8 9-10+11+12+13-14;1 2 3-4 5+6-7-8-9-10-11-12-13-14;1 2 3-4 5-6 7+8-9-10+11-12-13+14;1 2 3-4 5-6 7+8-9-10-11+12+13-14;1 2 3-4 5-6 7-8+9+10-11-12-13+14;1 2 3-4 5-6 7-8+9-10+11-12+13-14;1 2 3-4 5-6 7-8-9+10+11+12-13-14;1 2 3-4 5-6+7-8 9+10+11-12-13+14;1 2 3-4 5-6+7-8 9+10-11+12+13-14;1 2 3-4+5+6 7-8+9+10+11 12-13 14;1 2 3-4+5+6-7+8 9-10+11 12-13 14;1 2 3-4+5+6-7-8 9-10-11-12+13-14;1 2 3-4+5-6 7-8 9+10+11+12+13-14;1 2 3-4+5-6 7-8-9+10-11-12-13-14;1 2 3-4+5-6+7-8 9-10-11+12-13-14;1 2 3-4-5 6-7 8+9+10+11+12-13-14;1 2 3-4-5+6+7+8 9+10 11-12 13-14;1 2 3-4-5+6+7-8 9-10+11-12-13-14;1 2 3-4-5-6-7-8 9+10-11-12-13+14;1 2 3-4-5-6-7-8 9-10+11-12+13-14;1 2+3 4 5+6 7 8-9-10 11+12-13-14;1 2+3 4 5-6 7-8+9+10 11+12-13 14;1 2+3 4+5 6-7-8 9-10-11-12+13+14;1 2+3 4+5+6-7 8+9+10-11+12-13+14;1 2+3 4+5+6-7 8+9-10+11+12+13-14;1 2+3 4-5 6+7+8+9+10-11-12+13-14;1 2+3 4-5 6+7+8+9-10+11+12-13-14;1 2+3 4-5 6-7+8-9-10-11+12+13+14;1 2+3 4-5 6-7-8+9-10+11-12+13+14;1 2+3 4-5 6-7-8-9+10+11+12-13+14;1 2+3 4-5+6-7 8-9-10+11+12+13+14;1 2+3 4-5-6-7 8+9+10+11+12-13+14;1 2+3+4 5+6 7+8 9+10 11-12 13-14;1 2+3+4 5+6 7-8 9-10+11-12-13-14;1 2+3+4 5+6+7-8 9-10+11-12+13+14;1 2+3+4 5+6-7-8+9-10-11-12-13-14;1 2+3+4 5-6 7+8+9-10+11-12-13+14;1 2+3+4 5-6 7+8+9-10-11+12+13-14;1 2+3+4 5-6 7+8-9+10+11-12+13-14;1 2+3+4 5-6 7-8+9+10+11+12-13-14;1 2+3+4 5-6+7+8-9-10-11-12-13-14;1 2+3+4+5 6-7 8+9-10-11-12+13+14;1 2+3+4+5 6-7 8-9+10-11+12-13+14;1 2+3+4+5 6-7 8-9-10+11+12+13-14;1 2+3+4+5+6+7+8-9-10-11+12-13-14;1 2+3+4+5+6+7-8+9-10+11-12-13-14;1 2+3+4+5+6-7+8+9+10-11-12-13-14;1 2+3+4+5+6-7-8-9-10-11-12+13+14;1 2+3+4+5-6 7-8-9+10+11+12+13+14;1 2+3+4+5-6+7-8-9-10-11+12-13+14;1 2+3+4+5-6-7+8-9-10+11-12-13+14;1 2+3+4+5-6-7+8-9-10-11+12+13-14;1 2+3+4+5-6-7-8+9+10-11-12-13+14;1 2+3+4+5-6-7-8+9-10+11-12+13-14;1 2+3+4+5-6-7-8-9+10+11+12-13-14;1 2+3+4-5 6+7 8-9-10-11-12-13+14;1 2+3+4-5+6 7-8 9+10+11-12+13-14;1 2+3+4-5+6+7-8-9-10+11-12-13+14;1 2+3+4-5+6+7-8-9-10-11+12+13-14;1 2+3+4-5+6-7+8-9+10-11-12-13+14;1 2+3+4-5+6-7+8-9-10+11-12+13-14;1 2+3+4-5+6-7-8+9+10-11-12+13-14;1 2+3+4-5+6-7-8+9-10+11+12-13-14;1 2+3+4-5-6 7+8 9-10-11+12-13-14;1 2+3+4-5-6 7+8+9+10+11-12+13+14;1 2+3+4-5-6+7+8+9-10-11-12-13+14;1 2+3+4-5-6+7+8-9+10-11-12+13-14;1 2+3+4-5-6+7+8-9-10+11+12-13-14;1 2+3+4-5-6+7-8+9+10-11+12-13-14;1 2+3+4-5-6-7+8+9+10+11-12-13-14;1 2+3+4-5-6-7-8-9-10+11-12+13+14;1 2+3-4 5+6 7+8-9-10-11+12-13-14;1 2+3-4 5+6 7-8+9-10+11-12-13-14;1 2+3-4 5+6+7+8-9-10-11+12+13+14;1 2+3-4 5+6+7-8+9-10+11-12+13+14;1 2+3-4 5+6+7-8-9+10+11+12-13+14;1 2+3-4 5+6-7+8+9+10-11-12+13+14;1 2+3-4 5+6-7+8+9-10+11+12-13+14;1 2+3-4 5+6-7+8-9+10+11+12+13-14;1 2+3-4 5-6 7 8+9 10+11 12-13 14;1 2+3-4 5-6 7+8 9+10+11-12+13-14;1 2+3-4 5-6+7+8 9-10-11-12-13-14;1 2+3-4 5-6+7+8+9+10-11+12-13+14;1 2+3-4 5-6+7+8+9-10+11+12+13-14;1 2+3-4 5-6-7-8-9+10+11+12+13+14;1 2+3-4+5+6 7-8 9+10+11+12-13-14;1 2+3-4+5+6+7-8 9+10+11+12+13+14;1 2+3-4+5+6+7-8-9+10-11-12-13+14;1 2+3-4+5+6+7-8-9-10+11-12+13-14;1 2+3-4+5+6-7+8+9-10-11-12-13+14;1 2+3-4+5+6-7+8-9+10-11-12+13-14;1 2+3-4+5+6-7+8-9-10+11+12-13-14;1 2+3-4+5+6-7-8+9+10-11+12-13-14;1 2+3-4+5-6 7+8 9-10+11-12-13-14;1 2+3-4+5-6 7+8+9+10+11+12-13+14;1 2+3-4+5-6+7+8+9-10-11-12+13-14;1 2+3-4+5-6+7+8-9+10-11+12-13-14;1 2+3-4+5-6+7-8+9+10+11-12-13-14;1 2+3-4+5-6-7-8-9+10-11-12+13+14;1 2+3-4+5-6-7-8-9-10+11+12-13+14;1 2+3-4-5+6 7-8 9-10+11-12+13+14;1 2+3-4-5+6+7+8+9-10-11+12-13-14;1 2+3-4-5+6+7+8-9+10+11-12-13-14;1 2+3-4-5+6-7-8+9-10-11-12+13+14;1 2+3-4-5+6-7-8-9+10-11+12-13+14;1 2+3-4-5+6-7-8-9-10+11+12+13-14;1 2+3-4-5-6 7-8+9+10+11+12+13+14;1 2+3-4-5-6+7+8-9-10-11-12+13+14;1 2+3-4-5-6+7-8+9-10-11+12-13+14;1 2+3-4-5-6+7-8-9+10+11-12-13+14;1 2+3-4-5-6+7-8-9+10-11+12+13-14;1 2+3-4-5-6-7+8+9-10+11-12-13+14;1 2+3-4-5-6-7+8+9-10-11+12+13-14;1 2+3-4-5-6-7+8-9+10+11-12+13-14;1 2+3-4-5-6-7-8+9+10+11+12-13-14;1 2-3 4 5+6+7-8-9 10+11+12 13+14;1 2-3 4 5+6-7 8-9 10-11+12+13 14;1 2-3 4 5-6+7+8+9-10 11+12+13 14;1 2-3 4+5 6+7+8-9+10-11-12-13-14;1 2-3 4+5 6-7-8-9-10+11-12-13+14;1 2-3 4+5 6-7-8-9-10-11+12+13-14;1 2-3 4-5 6+7+8 9+10+11-12-13-14;1 2-3 4-5-6+7 8-9-10-11+12-13-14;1 2-3+4 5+6 7-8 9-10-11-12-13+14;1 2-3+4 5+6-7-8 9+10+11-12+13+14;1 2-3+4 5+6-7-8-9-10-11+12-13-14;1 2-3+4 5-6 7+8-9+10-11-12+13+14;1 2-3+4 5-6 7+8-9-10+11+12-13+14;1 2-3+4 5-6 7-8+9+10-11+12-13+14;1 2-3+4 5-6 7-8+9-10+11+12+13-14;1 2-3+4 5-6+7-8 9+10+11+12-13+14;1 2-3+4 5-6+7-8-9-10+11-12-13-14;1 2-3+4 5-6-7+8-9+10-11-12-13-14;1 2-3+4+5 6-7 8-9-10-11+12+13+14;1 2-3+4+5+6+7-8+9-10-11-12-13+14;1 2-3+4+5+6+7-8-9+10-11-12+13-14;1 2-3+4+5+6+7-8-9-10+11+12-13-14;1 2-3+4+5+6-7+8+9-10-11-12+13-14;1 2-3+4+5+6-7+8-9+10-11+12-13-14;1 2-3+4+5+6-7-8+9+10+11-12-13-14;1 2-3+4+5-6 7+8 9+10-11-12-13-14;1 2-3+4+5-6 7+8+9+10+11+12+13-14;1 2-3+4+5-6+7+8+9-10-11+12-13-14;1 2-3+4+5-6+7+8-9+10+11-12-13-14;1 2-3+4+5-6-7-8+9-10-11-12+13+14;1 2-3+4+5-6-7-8-9+10-11+12-13+14;1 2-3+4+5-6-7-8-9-10+11+12+13-14;1 2-3+4-5+6 7-8 9+10-11-12+13+14;1 2-3+4-5+6 7-8 9-10+11+12-13+14;1 2-3+4-5+6+7+8+9-10+11-12-13-14;1 2-3+4-5+6-7+8-9-10-11-12+13+14;1 2-3+4-5+6-7-8+9-10-11+12-13+14;1 2-3+4-5+6-7-8-9+10+11-12-13+14;1 2-3+4-5+6-7-8-9+10-11+12+13-14;1 2-3+4-5-6 7+8-9+10+11+12+13+14;1 2-3+4-5-6+7+8-9-10-11+12-13+14;1 2-3+4-5-6+7-8+9-10+11-12-13+14;1 2-3+4-5-6+7-8+9-10-11+12+13-14;1 2-3+4-5-6+7-8-9+10+11-12+13-14;1 2-3+4-5-6-7+8+9+10-11-12-13+14;1 2-3+4-5-6-7+8+9-10+11-12+13-14;1 2-3+4-5-6-7+8-9+10+11+12-13-14;1 2-3-4 5+6 7-8+9-10-11-12-13+14;1 2-3-4 5+6 7-8-9+10-11-12+13-14;1 2-3-4 5+6 7-8-9-10+11+12-13-14;1 2-3-4 5+6+7+8+9+10+11+12-13-14;1 2-3-4 5+6+7-8-9-10+11+12+13+14;1 2-3-4 5+6-7+8-9+10-11+12+13+14;1 2-3-4 5+6-7-8+9+10+11-12+13+14;1 2-3-4 5-6 7+8 9+10-11-12+13+14;1 2-3-4 5-6 7+8 9-10+11+12-13+14;1 2-3-4 5-6 7-8 9-10-11 12+13 14;1 2-3-4 5-6+7+8+9-10-11+12+13+14;1 2-3-4 5-6+7+8-9+10+11-12+13+14;1 2-3-4 5-6+7-8+9+10+11+12-13+14;1 2-3-4 5-6-7+8 9+10-11-12-13-14;1 2-3-4 5-6-7+8+9+10+11+12+13-14;1 2-3-4+5 6-7 8+9+10+11-12+13-14;1 2-3-4+5+6 7-8 9+10-11+12-13+14;1 2-3-4+5+6 7-8 9-10+11+12+13-14;1 2-3-4+5+6 7-8-9-10-11-12-13-14;1 2-3-4+5+6+7+8+9+10-11-12-13-14;1 2-3-4+5+6+7-8-9-10-11-12+13+14;1 2-3-4+5+6-7+8-9-10-11+12-13+14;1 2-3-4+5+6-7-8+9-10+11-12-13+14;1 2-3-4+5+6-7-8+9-10-11+12+13-14;1 2-3-4+5+6-7-8-9+10+11-12+13-14;1 2-3-4+5-6 7+8 9-10-11-12-13+14;1 2-3-4+5-6 7+8+9-10+11+12+13+14;1 2-3-4+5-6+7+8-9-10+11-12-13+14;1 2-3-4+5-6+7+8-9-10-11+12+13-14;1 2-3-4+5-6+7-8+9+10-11-12-13+14;1 2-3-4+5-6+7-8+9-10+11-12+13-14;1 2-3-4+5-6+7-8-9+10+11+12-13-14;1 2-3-4+5-6-7+8+9+10-11-12+13-14;1 2-3-4+5-6-7+8+9-10+11+12-13-14;1 2-3-4-5 6+7 8+9-10-11+12-13-14;1 2-3-4-5 6+7 8-9+10+11-12-13-14;1 2-3-4-5+6+7+8-9+10-11-12-13+14;1 2-3-4-5+6+7+8-9-10+11-12+13-14;1 2-3-4-5+6+7-8+9+10-11-12+13-14;1 2-3-4-5+6+7-8+9-10+11+12-13-14;1 2-3-4-5+6-7+8+9+10-11+12-13-14;1 2-3-4-5+6-7-8-9-10-11+12+13+14;1 2-3-4-5-6+7+8+9+10+11-12-13-14;1 2-3-4-5-6+7-8-9-10+11-12+13+14;1 2-3-4-5-6-7+8-9+10-11-12+13+14;1 2-3-4-5-6-7+8-9-10+11+12-13+14;1 2-3-4-5-6-7-8+9+10-11+12-13+14;1 2-3-4-5-6-7-8+9-10+11+12+13-14;1+2 3 4 5-6+7+8 9-10-11 12-13 14;1+2 3 4+5 6+7+8+9 10+11-12 13-14;1+2 3 4+5 6+7+8+9+10 11-12-13 14;1+2 3 4+5+6+7 8-9+10 11-12-13 14;1+2 3 4-5 6 7+8+9-10 11+12+13 14;1+2 3 4-5+6 7 8-9 10-11+12-13+14;1+2 3 4-5+6-7-8-9-10+11 12-13 14;1+2 3 4-5-6-7+8-9+10 11-12 13-14;1+2 3+4 5+6-7 8+9-10-11-12+13+14;1+2 3+4 5+6-7 8-9+10-11+12-13+14;1+2 3+4 5+6-7 8-9-10+11+12+13-14;1+2 3+4 5-6-7 8+9+10+11+12-13-14;1+2 3+4+5 6-7-8 9+10-11+12-13+14;1+2 3+4+5 6-7-8 9-10+11+12+13-14;1+2 3+4+5 6-7-8-9-10-11-12-13-14;1+2 3+4+5-6-7 8-9+10+11+12+13+14;1+2 3+4-5 6+7-8-9+10-11+12+13+14;1+2 3+4-5 6-7+8+9-10-11+12+13+14;1+2 3+4-5 6-7+8-9+10+11-12+13+14;1+2 3+4-5 6-7-8+9+10+11+12-13+14;1+2 3+4-5+6-7 8+9-10+11+12+13+14;1+2 3-4 5-6+7 8+9-10-11-12-13-14;1+2 3-4+5 6+7-8 9+10+11+12-13-14;1+2 3-4+5+6-7 8+9+10-11+12+13+14;1+2 3-4-5 6+7+8 9-10-11-12-13-14;1+2 3-4-5 6+7+8+9+10-11+12-13+14;1+2 3-4-5 6+7+8+9-10+11+12+13-14;1+2 3-4-5 6-7-8-9+10+11+12+13+14;1+2 3-4-5-6-7 8+9+10+11+12+13+14;1+2+3 4 5-6 7-8 9+10+11 12-13 14;1+2+3 4+5 6-7 8-9-10-11-12+13+14;1+2+3 4+5+6+7-8 9+10+11+12-13+14;1+2+3 4+5+6+7-8-9-10+11-12-13-14;1+2+3 4+5+6-7+8-9+10-11-12-13-14;1+2+3 4+5-6 7+8+9+10+11-12+13-14;1+2+3 4+5-6+7+8+9-10-11-12-13-14;1+2+3 4+5-6-7-8 9+10+11+12+13+14;1+2+3 4+5-6-7-8-9+10-11-12-13+14;1+2+3 4+5-6-7-8-9-10+11-12+13-14;1+2+3 4-5+6 7+8 9+10 11-12 13+14;1+2+3 4-5+6 7-8 9-10+11-12-13+14;1+2+3 4-5+6 7-8 9-10-11+12+13-14;1+2+3 4-5+6-7-8+9-10-11-12-13+14;1+2+3 4-5+6-7-8-9+10-11-12+13-14;1+2+3 4-5+6-7-8-9-10+11+12-13-14;1+2+3 4-5-6 7+8+9-10-11+12+13+14;1+2+3 4-5-6 7+8-9+10+11-12+13+14;1+2+3 4-5-6 7-8+9+10+11+12-13+14;1+2+3 4-5-6+7+8-9-10-11-12-13+14;1+2+3 4-5-6+7-8+9-10-11-12+13-14;1+2+3 4-5-6+7-8-9+10-11+12-13-14;1+2+3 4-5-6-7+8+9-10-11+12-13-14;1+2+3 4-5-6-7+8-9+10+11-12-13-14;1+2+3+4 5+6-7 8+9+10-11+12-13+14;1+2+3+4 5+6-7 8+9-10+11+12+13-14;1+2+3+4+5 6+7-8 9-10+11-12+13+14;1+2+3+4+5 6-7-8+9-10-11-12-13-14;1+2+3+4+5-6-7 8+9+10+11+12+13+14;1+2+3+4-5 6+7+8-9-10+11+12+13+14;1+2+3+4-5 6+7-8+9+10-11+12+13+14;1+2+3+4-5 6-7+8 9-10-11+12-13-14;1+2+3+4-5 6-7+8+9+10+11-12+13+14;1+2+3-4 5+6+7 8-9-10-11+12-13-14;1+2+3-4+5 6-7-8 9+10-11+12+13+14;1+2+3-4+5 6-7-8-9-10-11-12+13-14;1+2+3-4-5 6-7-8+9+10+11+12+13+14;1+2+3-4-5-6+7 8-9-10-11-12-13-14;1+2-3 4+5+6 7+8-9+10-11-12-13-14;1+2-3 4+5+6+7+8-9+10-11-12+13+14;1+2-3 4+5+6+7+8-9-10+11+12-13+14;1+2-3 4+5+6+7-8+9+10-11+12-13+14;1+2-3 4+5+6+7-8+9-10+11+12+13-14;1+2-3 4+5+6-7+8+9+10+11-12-13+14;1+2-3 4+5+6-7+8+9+10-11+12+13-14;1+2-3 4+5-6+7+8+9+10+11-12+13-14;1+2-3 4+5-6-7+8-9-10+11+12+13+14;1+2-3 4+5-6-7-8+9+10-11+12+13+14;1+2-3 4-5 6+7 8-9-10-11+12+13+14;1+2-3 4-5+6 7-8+9-10-11-12-13+14;1+2-3 4-5+6 7-8-9+10-11-12+13-14;1+2-3 4-5+6 7-8-9-10+11+12-13-14;1+2-3 4-5+6+7+8+9+10+11+12-13-14;1+2-3 4-5+6+7-8-9-10+11+12+13+14;1+2-3 4-5+6-7+8-9+10-11+12+13+14;1+2-3 4-5+6-7-8+9+10+11-12+13+14;1+2-3 4-5-6 7+8 9+10-11-12+13+14;1+2-3 4-5-6 7+8 9-10+11+12-13+14;1+2-3 4-5-6 7-8 9-10-11 12+13 14;1+2-3 4-5-6+7+8+9-10-11+12+13+14;1+2-3 4-5-6+7+8-9+10+11-12+13+14;1+2-3 4-5-6+7-8+9+10+11+12-13+14;1+2-3 4-5-6-7+8 9+10-11-12-13-14;1+2-3 4-5-6-7+8+9+10+11+12+13-14;1+2-3+4 5+6-7 8+9-10-11+12+13+14;1+2-3+4 5+6-7 8-9+10+11-12+13+14;1+2-3+4+5 6-7-8 9+10+11-12+13+14;1+2-3+4+5 6-7-8-9-10-11+12-13-14;1+2-3+4-5 6-7+8-9+10+11+12+13+14;1+2-3-4 5-6+7 8+9-10-11+12-13-14;1+2-3-4 5-6+7 8-9+10+11-12-13-14;1+2-3-4+5 6+7-8+9-10-11-12-13-14;1+2-3-4-5 6+7+8 9-10-11+12-13-14;1+2-3-4-5 6+7+8+9+10+11-12+13+14;1-2 3 4+5+6+7+8-9-10 11+12 13+14;1-2 3 4+5+6-7 8+9-10 11-12+13 14;1-2 3 4+5+6-7 8-9 10+11+12 13-14;1-2 3 4+5+6-7+8+9+10-11 12+13 14;1-2 3 4+5-6 7 8+9 10+11+12-13-14;1-2 3 4-5+6 7-8-9-10 11+12 13-14;1-2 3 4-5-6 7+8 9-10 11+12 13+14;1-2 3 4-5-6+7+8-9 10+11 12+13+14;1-2 3+4 5+6-7 8+9-10+11+12+13+14;1-2 3+4+5 6-7-8+9-10-11-12-13+14;1-2 3+4+5 6-7-8-9+10-11-12+13-14;1-2 3+4+5 6-7-8-9-10+11+12-13-14;1-2 3+4-5 6-7+8 9-10-11+12-13+14;1-2 3+4-5-6+7 8-9+10-11-12-13-14;1-2 3-4 5+6+7 8-9-10-11+12-13+14;1-2 3-4 5-6+7 8+9+10-11-12+13-14;1-2 3-4 5-6+7 8+9-10+11+12-13-14;1-2 3-4+5 6+7+8-9-10-11+12-13-14;1-2 3-4+5 6+7-8+9-10+11-12-13-14;1-2 3-4+5 6-7+8+9+10-11-12-13-14;1-2 3-4+5 6-7-8-9-10-11-12+13+14;1-2 3-4+5-6+7 8+9-10-11-12-13-14;1-2 3-4-5 6+7+8 9+10-11-12+13-14;1-2 3-4-5 6+7+8 9-10+11+12-13-14;1-2 3-4-5-6+7 8-9-10-11-12-13+14;1-2+3 4 5+6 7-8+9 10-11+12-13 14;1-2+3 4 5-6 7-8 9+10 11-12 13+14;1-2+3 4+5+6 7-8 9+10-11+12-13-14;1-2+3 4+5+6+7-8 9+10-11+12+13+14;1-2+3 4+5+6+7-8-9-10-11-12+13-14;1-2+3 4+5+6-7+8-9-10-11+12-13-14;1-2+3 4+5+6-7-8+9-10+11-12-13-14;1-2+3 4+5-6 7+8 9-10-11-12-13-14;1-2+3 4+5-6 7+8+9+10-11+12-13+14;1-2+3 4+5-6 7+8+9-10+11+12+13-14;1-2+3 4+5-6+7+8-9-10+11-12-13-14;1-2+3 4+5-6+7-8+9+10-11-12-13-14;1-2+3 4+5-6-7-8-9-10-11+12-13+14;1-2+3 4-5+6 7-8 9-10-11-12+13+14;1-2+3 4-5+6+7+8-9+10-11-12-13-14;1-2+3 4-5+6-7-8-9-10+11-12-13+14;1-2+3 4-5+6-7-8-9-10-11+12+13-14;1-2+3 4-5-6 7+8-9-10+11+12+13+14;1-2+3 4-5-6 7-8+9+10-11+12+13+14;1-2+3 4-5-6+7-8 9+10+11+12+13+14;1-2+3 4-5-6+7-8-9+10-11-12-13+14;1-2+3 4-5-6+7-8-9-10+11-12+13-14;1-2+3 4-5-6-7+8+9-10-11-12-13+14;1-2+3 4-5-6-7+8-9+10-11-12+13-14;1-2+3 4-5-6-7+8-9-10+11+12-13-14;1-2+3 4-5-6-7-8+9+10-11+12-13-14;1-2+3+4 5+6-7 8+9-10+11-12+13+14;1-2+3+4 5+6-7 8-9+10+11+12-13+14;1-2+3+4+5 6-7-8 9+10+11+12-13+14;1-2+3+4+5 6-7-8-9-10+11-12-13-14;1-2+3+4-5 6+7-8-9+10+11+12+13+14;1-2+3+4-5 6-7+8 9-10-11-12-13+14;1-2+3+4-5 6-7+8+9-10+11+12+13+14;1-2+3-4 5+6+7 8-9-10-11-12-13+14;1-2+3-4 5-6+7 8+9-10+11-12-13-14;1-2+3-4+5 6+7+8-9-10-11-12-13-14;1-2+3-4+5+6-7 8+9+10+11+12+13+14;1-2+3-4-5 6+7+8 9-10+11-12-13-14;1-2+3-4-5 6+7+8+9+10+11+12-13+14;1-2-3 4 5+6-7 8 9-10+11 12+13+14;1-2-3 4 5-6 7+8-9 10-11+12+13 14;1-2-3 4+5+6 7+8-9-10-11+12-13-14;1-2-3 4+5+6 7-8+9-10+11-12-13-14;1-2-3 4+5+6+7+8-9-10-11+12+13+14;1-2-3 4+5+6+7-8+9-10+11-12+13+14;1-2-3 4+5+6+7-8-9+10+11+12-13+14;1-2-3 4+5+6-7+8+9+10-11-12+13+14;1-2-3 4+5+6-7+8+9-10+11+12-13+14;1-2-3 4+5+6-7+8-9+10+11+12+13-14;1-2-3 4+5-6 7 8+9 10+11 12-13 14;1-2-3 4+5-6 7+8 9+10+11-12+13-14;1-2-3 4+5-6+7+8 9-10-11-12-13-14;1-2-3 4+5-6+7+8+9+10-11+12-13+14;1-2-3 4+5-6+7+8+9-10+11+12+13-14;1-2-3 4+5-6-7-8-9+10+11+12+13+14;1-2-3 4-5+6 7-8-9-10+11-12-13+14;1-2-3 4-5+6 7-8-9-10-11+12+13-14;1-2-3 4-5+6+7+8+9+10+11-12-13+14;1-2-3 4-5+6+7+8+9+10-11+12+13-14;1-2-3 4-5+6-7-8+9-10+11+12+13+14;1-2-3 4-5-6 7+8 9-10-11+12+13+14;1-2-3 4-5-6+7+8-9-10+11+12+13+14;1-2-3 4-5-6+7-8+9+10-11+12+13+14;1-2-3 4-5-6-7+8 9-10-11+12-13-14;1-2-3 4-5-6-7+8+9+10+11-12+13+14;1-2-3+4 5+6-7 8-9-10+11+12+13+14;1-2-3+4 5-6-7 8+9+10+11+12-13+14;1-2-3+4+5 6-7-8 9-10+11+12+13+14;1-2-3+4+5 6-7-8-9-10-11-12-13+14;1-2-3+4-5 6+7+8 9+10-11-12-13-14;1-2-3+4-5 6+7+8+9+10+11+12+13-14;1-2-3-4 5-6+7 8+9-10-11-12-13+14;1-2-3-4 5-6+7 8-9+10-11-12+13-14;1-2-3-4 5-6+7 8-9-10+11+12-13-14;1-2-3-4+5 6+7-8 9+10+11+12-13+14;1-2-3-4+5 6+7-8-9-10+11-12-13-14;1-2-3-4+5 6-7+8-9+10-11-12-13-14;1-2-3-4+5-6+7 8-9-10-11-12-13-14;1-2-3-4-5 6+7+8 9-10-11-12-13+14;1-2-3-4-5 6+7+8+9-10+11+12+13+14;1-2-3-4-5 6-7+8 9+10+11-12-13-14",
                1);
        totalTime += test(
                "S15",
                "1 2 3 4+5 6+7+8-9+10+11+12-13 14-15;1 2 3 4+5 6-7 8+9+10+11-12 13-14-15;1 2 3 4+5 6-7-8-9+10+11+12-13 14+15;1 2 3 4+5+6 7+8 9+10+11+12-13-14 15;1 2 3 4+5+6+7 8+9-10-11-12-13 14+15;1 2 3 4+5+6+7-8 9+10+11-12 13+14+15;1 2 3 4+5+6+7-8-9-10-11-12 13+14-15;1 2 3 4+5+6-7-8-9+10+11-12 13-14-15;1 2 3 4+5-6+7 8+9+10+11-12-13 14-15;1 2 3 4+5-6+7-8+9-10+11-12 13-14-15;1 2 3 4+5-6-7 8+9-10-11 12-13-14-15;1 2 3 4+5-6-7+8+9+10-11-12 13-14-15;1 2 3 4-5+6+7+8-9-10+11-12 13-14-15;1 2 3 4-5+6+7-8+9+10-11-12 13-14-15;1 2 3 4-5+6-7-8-9-10+11-12 13-14+15;1 2 3 4-5-6 7-8+9+10+11-12 13+14+15;1 2 3 4-5-6+7 8+9-10+11-12-13 14+15;1 2 3 4-5-6+7-8-9+10-11-12 13-14+15;1 2 3 4-5-6+7-8-9-10 11+12 13-14 15;1 2 3 4-5-6+7-8-9-10+11-12 13+14-15;1 2 3 4-5-6-7 8-9-10-11 12-13+14-15;1 2 3 4-5-6-7+8+9-10-11-12 13-14+15;1 2 3 4-5-6-7+8-9+10-11-12 13+14-15;1 2 3+4 5 6+7+8-9 10-11 12+13+14 15;1 2 3+4 5-6+7 8-9+10 11-12 13-14-15;1 2 3+4 5-6-7 8-9-10-11-12-13-14-15;1 2 3+4+5 6+7+8+9+10+11 12-13 14-15;1 2 3+4+5 6+7-8-9+10 11-12 13+14+15;1 2 3+4+5 6-7-8+9+10+11 12-13 14+15;1 2 3+4+5+6+7 8+9 10-11 12-13+14-15;1 2 3+4+5+6+7 8-9+10+11 12-13 14-15;1 2 3+4+5+6-7 8-9-10-11+12-13-14-15;1 2 3+4-5 6-7-8 9+10+11-12-13+14+15;1 2 3+4-5 6-7-8 9+10-11+12+13-14+15;1 2 3+4-5 6-7-8 9-10+11+12+13+14-15;1 2 3+4-5 6-7-8-9-10-11-12-13+14-15;1 2 3+4-5+6 7 8+9-10 11-12 13+14 15;1 2 3+4-5+6+7 8-9-10+11 12-13 14+15;1 2 3+4-5-6+7 8+9+10 11-12 13+14-15;1 2 3+4-5-6+7 8+9+10-11+12 13-14 15;1 2 3+4-5-6-7 8+9-10-11-12-13+14-15;1 2 3+4-5-6-7 8-9+10-11-12+13-14-15;1 2 3+4-5-6-7 8-9-10+11+12-13-14-15;1 2 3-4 5 6-7-8 9-10 11+12+13+14 15;1 2 3-4 5+6-7 8-9+10-11-12-13+14+15;1 2 3-4 5+6-7 8-9-10+11-12+13-14+15;1 2 3-4 5+6-7 8-9-10-11+12+13+14-15;1 2 3-4 5-6-7 8+9+10+11-12-13-14+15;1 2 3-4 5-6-7 8+9+10-11+12-13+14-15;1 2 3-4 5-6-7 8+9-10+11+12+13-14-15;1 2 3-4+5 6+7+8+9 10-11 12+13+14-15;1 2 3-4+5 6+7+8-9+10+11+12 13-14 15;1 2 3-4+5 6-7-8+9 10-11 12+13+14+15;1 2 3-4+5+6 7+8 9+10+11 12+13-14 15;1 2 3-4+5-6-7 8+9-10-11-12+13-14-15;1 2 3-4+5-6-7 8-9+10-11+12-13-14-15;1 2 3-4-5 6+7-8 9+10+11+12-13+14-15;1 2 3-4-5 6+7-8-9-10+11-12-13-14-15;1 2 3-4-5 6-7+8-9+10-11-12-13-14-15;1 2 3-4-5 6-7-8 9-10-11+12+13+14+15;1 2 3-4-5+6+7 8+9+10+11 12-13 14-15;1 2 3-4-5+6-7 8+9-10-11+12-13-14-15;1 2 3-4-5+6-7 8-9+10+11-12-13-14-15;1 2 3-4-5-6+7 8+9 10-11 12-13+14+15;1 2 3-4-5-6+7 8-9+10+11 12-13 14+15;1 2 3-4-5-6-7 8-9-10-11+12-13-14+15;1 2 3-4-5-6-7 8-9-10-11-12+13+14-15;1 2+3 4 5+6-7 8+9 10-11-12 13+14+15;1 2+3 4 5+6-7 8-9+10 11+12-13 14+15;1 2+3 4 5-6 7 8-9 10-11+12 13+14+15;1 2+3 4 5-6 7 8-9-10 11+12+13 14+15;1 2+3 4 5-6 7+8 9+10 11+12+13-14 15;1 2+3 4 5-6 7-8 9+10 11-12 13-14+15;1 2+3 4 5-6 7-8 9-10+11+12 13-14 15;1 2+3 4 5-6+7+8+9 10+11+12-13 14+15;1 2+3 4+5 6-7 8+9+10+11-12-13-14-15;1 2+3 4+5+6 7+8 9+10+11 12-13 14-15;1 2+3 4+5+6 7-8 9+10-11-12+13-14-15;1 2+3 4+5+6 7-8 9-10+11+12-13-14-15;1 2+3 4+5+6+7-8 9+10+11-12-13+14+15;1 2+3 4+5+6+7-8 9+10-11+12+13-14+15;1 2+3 4+5+6+7-8 9-10+11+12+13+14-15;1 2+3 4+5+6+7-8-9-10-11-12-13+14-15;1 2+3 4+5+6-7+8-9-10-11-12+13-14-15;1 2+3 4+5+6-7-8+9-10-11+12-13-14-15;1 2+3 4+5+6-7-8-9+10+11-12-13-14-15;1 2+3 4+5-6 7+8+9+10-11+12-13-14+15;1 2+3 4+5-6 7+8+9+10-11-12+13+14-15;1 2+3 4+5-6 7+8+9-10+11+12-13+14-15;1 2+3 4+5-6 7+8-9+10+11+12+13-14-15;1 2+3 4+5-6 7-8-9-10-11+12+13+14+15;1 2+3 4+5-6+7+8-9-10-11+12-13-14-15;1 2+3 4+5-6+7-8+9-10+11-12-13-14-15;1 2+3 4+5-6-7+8+9+10-11-12-13-14-15;1 2+3 4+5-6-7-8 9+10+11-12+13+14+15;1 2+3 4+5-6-7-8-9-10-11+12-13-14+15;1 2+3 4+5-6-7-8-9-10-11-12+13+14-15;1 2+3 4-5+6 7 8-9 10-11-12 13+14 15;1 2+3 4-5+6 7+8 9-10+11 12-13 14+15;1 2+3 4-5+6 7-8 9-10-11-12+13-14+15;1 2+3 4-5+6+7+8-9-10+11-12-13-14-15;1 2+3 4-5+6+7-8+9+10-11-12-13-14-15;1 2+3 4-5+6-7-8 9+10+11+12-13+14+15;1 2+3 4-5+6-7-8-9-10+11-12-13-14+15;1 2+3 4-5+6-7-8-9-10-11+12-13+14-15;1 2+3 4-5-6 7+8+9-10-11-12+13+14+15;1 2+3 4-5-6 7+8-9+10-11+12-13+14+15;1 2+3 4-5-6 7+8-9-10+11+12+13-14+15;1 2+3 4-5-6 7-8+9+10+11-12-13+14+15;1 2+3 4-5-6 7-8+9+10-11+12+13-14+15;1 2+3 4-5-6 7-8+9-10+11+12+13+14-15;1 2+3 4-5-6+7-8 9+10+11+12+13-14+15;1 2+3 4-5-6+7-8-9+10-11-12-13-14+15;1 2+3 4-5-6+7-8-9-10+11-12-13+14-15;1 2+3 4-5-6+7-8-9-10-11+12+13-14-15;1 2+3 4-5-6-7+8+9-10-11-12-13-14+15;1 2+3 4-5-6-7+8-9+10-11-12-13+14-15;1 2+3 4-5-6-7+8-9-10+11-12+13-14-15;1 2+3 4-5-6-7-8+9+10-11-12+13-14-15;1 2+3 4-5-6-7-8+9-10+11+12-13-14-15;1 2+3+4 5+6-7 8+9+10-11-12-13+14+15;1 2+3+4 5+6-7 8+9-10+11-12+13-14+15;1 2+3+4 5+6-7 8+9-10-11+12+13+14-15;1 2+3+4 5+6-7 8-9+10+11+12-13-14+15;1 2+3+4 5+6-7 8-9+10+11-12+13+14-15;1 2+3+4 5-6-7 8-9-10-11+12+13+14+15;1 2+3+4+5 6 7+8-9 10-11 12+13+14 15;1 2+3+4+5 6+7-8 9-10-11+12-13+14+15;1 2+3+4+5 6-7-8 9+10+11+12-13-14+15;1 2+3+4+5 6-7-8 9+10+11-12+13+14-15;1 2+3+4+5 6-7-8-9-10-11+12-13-14-15;1 2+3+4+5-6-7 8+9+10+11-12+13+14+15;1 2+3+4-5 6+7+8-9-10+11-12+13+14+15;1 2+3+4-5 6+7-8+9+10-11-12+13+14+15;1 2+3+4-5 6+7-8+9-10+11+12-13+14+15;1 2+3+4-5 6+7-8-9+10+11+12+13-14+15;1 2+3+4-5 6-7+8 9-10-11-12-13-14+15;1 2+3+4-5 6-7+8+9+10-11+12-13+14+15;1 2+3+4-5 6-7+8+9-10+11+12+13-14+15;1 2+3+4-5 6-7+8-9+10+11+12+13+14-15;1 2+3+4-5+6+7 8 9-10 11-12 13+14 15;1 2+3+4-5+6-7 8+9+10+11+12-13+14+15;1 2+3-4 5 6+7-8-9 10+11+12+13 14+15;1 2+3-4 5 6-7 8-9 10-11+12+13+14 15;1 2+3-4 5+6+7 8-9-10-11-12-13-14+15;1 2+3-4 5-6+7 8+9-10-11+12-13-14-15;1 2+3-4 5-6+7 8-9+10+11-12-13-14-15;1 2+3-4+5 6+7-8+9-10-11-12-13-14-15;1 2+3-4+5 6-7-8 9+10-11-12+13+14+15;1 2+3-4+5 6-7-8 9-10+11+12-13+14+15;1 2+3-4+5+6-7 8+9+10+11+12+13-14+15;1 2+3-4-5 6+7+8 9-10-11+12-13-14-15;1 2+3-4-5 6+7+8+9+10+11+12-13-14+15;1 2+3-4-5 6+7+8+9+10+11-12+13+14-15;1 2+3-4-5 6+7-8-9-10+11+12+13+14+15;1 2+3-4-5 6-7+8-9+10-11+12+13+14+15;1 2+3-4-5 6-7-8+9+10+11-12+13+14+15;1 2+3-4-5+6-7 8-9+10+11+12+13+14+15;1 2-3 4 5+6 7+8 9-10-11 12+13 14-15;1 2-3 4 5+6+7+8 9-10 11+12 13+14+15;1 2-3 4 5+6-7 8 9-10+11 12+13-14+15;1 2-3 4+5 6-7 8-9+10-11+12+13+14+15;1 2-3 4+5+6 7+8-9-10-11-12+13-14-15;1 2-3 4+5+6 7-8+9-10-11+12-13-14-15;1 2-3 4+5+6 7-8-9+10+11-12-13-14-15;1 2-3 4+5+6+7+8-9-10+11-12-13+14+15;1 2-3 4+5+6+7+8-9-10-11+12+13-14+15;1 2-3 4+5+6+7-8+9+10-11-12-13+14+15;1 2-3 4+5+6+7-8+9-10+11-12+13-14+15;1 2-3 4+5+6+7-8+9-10-11+12+13+14-15;1 2-3 4+5+6+7-8-9+10+11+12-13-14+15;1 2-3 4+5+6+7-8-9+10+11-12+13+14-15;1 2-3 4+5+6-7+8+9+10-11-12+13-14+15;1 2-3 4+5+6-7+8+9-10+11+12-13-14+15;1 2-3 4+5+6-7+8+9-10+11-12+13+14-15;1 2-3 4+5+6-7+8-9+10+11+12-13+14-15;1 2-3 4+5+6-7-8+9+10+11+12+13-14-15;1 2-3 4+5-6 7+8 9+10+11-12-13+14-15;1 2-3 4+5-6 7+8 9+10-11+12+13-14-15;1 2-3 4+5-6 7-8 9-10 11+12 13-14-15;1 2-3 4+5-6+7+8+9+10-11+12-13-14+15;1 2-3 4+5-6+7+8+9+10-11-12+13+14-15;1 2-3 4+5-6+7+8+9-10+11+12-13+14-15;1 2-3 4+5-6+7+8-9+10+11+12+13-14-15;1 2-3 4+5-6+7-8-9-10-11+12+13+14+15;1 2-3 4+5-6-7+8-9-10+11-12+13+14+15;1 2-3 4+5-6-7-8+9+10-11-12+13+14+15;1 2-3 4+5-6-7-8+9-10+11+12-13+14+15;1 2-3 4+5-6-7-8-9+10+11+12+13-14+15;1 2-3 4-5 6+7 8+9+10+11+12-13-14-15;1 2-3 4-5 6+7 8-9-10-11-12+13+14+15;1 2-3 4-5+6 7-8 9+10+11+12-13+14+15;1 2-3 4-5+6 7-8-9-10+11-12-13-14+15;1 2-3 4-5+6 7-8-9-10-11+12-13+14-15;1 2-3 4-5+6+7+8 9-10-11-12-13-14-15;1 2-3 4-5+6+7+8+9+10+11-12-13-14+15;1 2-3 4-5+6+7+8+9+10-11+12-13+14-15;1 2-3 4-5+6+7+8+9-10+11+12+13-14-15;1 2-3 4-5+6+7-8-9-10+11-12+13+14+15;1 2-3 4-5+6-7+8-9+10-11-12+13+14+15;1 2-3 4-5+6-7+8-9-10+11+12-13+14+15;1 2-3 4-5+6-7-8+9+10-11+12-13+14+15;1 2-3 4-5+6-7-8+9-10+11+12+13-14+15;1 2-3 4-5+6-7-8-9+10+11+12+13+14-15;1 2-3 4-5-6 7+8 9-10+11-12-13+14+15;1 2-3 4-5-6 7+8 9-10-11+12+13-14+15;1 2-3 4-5-6+7+8+9-10-11-12+13+14+15;1 2-3 4-5-6+7+8-9+10-11+12-13+14+15;1 2-3 4-5-6+7+8-9-10+11+12+13-14+15;1 2-3 4-5-6+7-8+9+10+11-12-13+14+15;1 2-3 4-5-6+7-8+9+10-11+12+13-14+15;1 2-3 4-5-6+7-8+9-10+11+12+13+14-15;1 2-3 4-5-6-7+8 9-10-11-12+13-14-15;1 2-3 4-5-6-7+8+9+10+11-12+13-14+15;1 2-3 4-5-6-7+8+9+10-11+12+13+14-15;1 2-3+4 5+6-7 8+9-10-11-12+13+14+15;1 2-3+4 5+6-7 8-9+10-11+12-13+14+15;1 2-3+4 5+6-7 8-9-10+11+12+13-14+15;1 2-3+4 5-6-7 8+9+10+11+12-13-14+15;1 2-3+4 5-6-7 8+9+10+11-12+13+14-15;1 2-3+4+5 6+7+8-9-10-11-12-13-14-15;1 2-3+4+5 6-7-8 9+10-11+12-13+14+15;1 2-3+4+5 6-7-8 9-10+11+12+13-14+15;1 2-3+4+5 6-7-8-9-10-11-12-13-14+15;1 2-3+4+5+6-7 8+9+10+11+12+13+14-15;1 2-3+4+5-6-7 8-9+10+11+12+13+14+15;1 2-3+4-5 6+7+8 9-10+11-12-13-14-15;1 2-3+4-5 6+7+8+9+10+11+12-13+14-15;1 2-3+4-5 6+7-8-9+10-11+12+13+14+15;1 2-3+4-5 6-7+8+9-10-11+12+13+14+15;1 2-3+4-5 6-7+8-9+10+11-12+13+14+15;1 2-3+4-5 6-7-8+9+10+11+12-13+14+15;1 2-3+4-5+6-7 8+9-10+11+12+13+14+15;1 2-3-4 5-6+7 8+9-10-11-12-13-14+15;1 2-3-4 5-6+7 8-9+10-11-12-13+14-15;1 2-3-4 5-6+7 8-9-10+11-12+13-14-15;1 2-3-4+5 6+7-8 9+10+11+12-13-14+15;1 2-3-4+5 6+7-8 9+10+11-12+13+14-15;1 2-3-4+5 6+7-8-9-10-11+12-13-14-15;1 2-3-4+5 6-7+8-9-10+11-12-13-14-15;1 2-3-4+5 6-7-8+9+10-11-12-13-14-15;1 2-3-4+5+6-7 8+9+10-11+12+13+14+15;1 2-3-4-5 6+7+8 9-10-11-12-13-14+15;1 2-3-4-5 6+7+8+9+10-11+12-13+14+15;1 2-3-4-5 6+7+8+9-10+11+12+13-14+15;1 2-3-4-5 6+7+8-9+10+11+12+13+14-15;1 2-3-4-5 6-7+8 9+10-11+12-13-14-15;1 2-3-4-5 6-7-8-9+10+11+12+13+14+15;1 2-3-4-5+6+7 8-9-10-11-12-13-14-15;1 2-3-4-5-6-7 8+9+10+11+12+13+14+15;1+2 3 4 5+6+7 8-9-10-11 12-13 14+15;1+2 3 4 5+6-7-8-9 10-11+12-13-14 15;1+2 3 4 5+6-7-8-9-10 11+12-13 14-15;1+2 3 4 5-6+7 8+9-10 11+12-13-14 15;1+2 3 4 5-6+7-8-9 10+11-12-13-14 15;1+2 3 4 5-6-7 8-9 10-11-12-13 14-15;1+2 3 4 5-6-7 8-9-10 11-12 13-14-15;1+2 3 4+5 6+7 8 9-10-11 12+13+14+15;1+2 3 4+5+6 7+8+9 10-11-12 13+14-15;1+2 3 4+5+6 7+8-9+10 11+12-13 14-15;1+2 3 4+5+6 7+8-9+10+11 12-13-14 15;1+2 3 4+5+6+7 8 9-10 11-12-13-14+15;1+2 3 4+5+6-7 8+9+10+11 12-13 14+15;1+2 3 4+5-6 7 8+9-10 11+12+13+14 15;1+2 3 4+5-6+7+8 9+10 11-12-13 14-15;1+2 3 4-5 6+7+8+9+10 11-12 13+14-15;1+2 3 4-5 6+7+8+9+10-11+12 13-14 15;1+2 3 4-5 6-7-8+9+10 11-12 13+14+15;1+2 3 4-5+6 7-8-9+10+11 12+13-14 15;1+2 3 4-5-6+7 8 9-10 11+12-13+14-15;1+2 3 4-5-6-7+8 9+10 11+12-13 14-15;1+2 3 4-5-6-7+8 9+10+11 12-13-14 15;1+2 3+4 5 6+7+8+9 10+11+12-13-14 15;1+2 3+4 5+6 7-8 9-10-11-12-13+14-15;1+2 3+4 5+6+7-8 9-10-11+12-13+14+15;1+2 3+4 5+6-7-8 9+10+11+12-13-14+15;1+2 3+4 5+6-7-8 9+10+11-12+13+14-15;1+2 3+4 5+6-7-8-9-10-11+12-13-14-15;1+2 3+4 5-6 7+8+9-10-11-12+13-14+15;1+2 3+4 5-6 7+8-9+10-11+12-13-14+15;1+2 3+4 5-6 7+8-9+10-11-12+13+14-15;1+2 3+4 5-6 7+8-9-10+11+12-13+14-15;1+2 3+4 5-6 7-8+9+10+11-12-13-14+15;1+2 3+4 5-6 7-8+9+10-11+12-13+14-15;1+2 3+4 5-6 7-8+9-10+11+12+13-14-15;1+2 3+4 5-6+7-8 9+10+11+12-13+14-15;1+2 3+4 5-6+7-8-9-10+11-12-13-14-15;1+2 3+4 5-6-7+8-9+10-11-12-13-14-15;1+2 3+4 5-6-7-8 9-10-11+12+13+14+15;1+2 3+4+5 6-7 8-9+10-11-12-13+14+15;1+2 3+4+5 6-7 8-9-10+11-12+13-14+15;1+2 3+4+5 6-7 8-9-10-11+12+13+14-15;1+2 3+4+5+6+7+8-9-10-11-12-13-14+15;1+2 3+4+5+6+7-8+9-10-11-12-13+14-15;1+2 3+4+5+6+7-8-9+10-11-12+13-14-15;1+2 3+4+5+6+7-8-9-10+11+12-13-14-15;1+2 3+4+5+6-7+8+9-10-11-12+13-14-15;1+2 3+4+5+6-7+8-9+10-11+12-13-14-15;1+2 3+4+5+6-7-8+9+10+11-12-13-14-15;1+2 3+4+5-6 7+8 9+10-11-12-13-14-15;1+2 3+4+5-6 7+8+9+10+11+12+13-14-15;1+2 3+4+5-6 7-8+9-10-11+12+13+14+15;1+2 3+4+5-6 7-8-9+10+11-12+13+14+15;1+2 3+4+5-6+7+8+9-10-11+12-13-14-15;1+2 3+4+5-6+7+8-9+10+11-12-13-14-15;1+2 3+4+5-6+7-8 9-10+11+12+13+14+15;1+2 3+4+5-6+7-8-9-10-11-12-13+14+15;1+2 3+4+5-6-7+8-9-10-11-12+13-14+15;1+2 3+4+5-6-7-8+9-10-11+12-13-14+15;1+2 3+4+5-6-7-8+9-10-11-12+13+14-15;1+2 3+4+5-6-7-8-9+10+11-12-13-14+15;1+2 3+4+5-6-7-8-9+10-11+12-13+14-15;1+2 3+4+5-6-7-8-9-10+11+12+13-14-15;1+2 3+4-5+6 7-8 9+10-11+12-13-14+15;1+2 3+4-5+6 7-8 9+10-11-12+13+14-15;1+2 3+4-5+6 7-8 9-10+11+12-13+14-15;1+2 3+4-5+6+7+8+9-10+11-12-13-14-15;1+2 3+4-5+6+7-8 9+10-11+12+13+14+15;1+2 3+4-5+6+7-8-9-10-11-12+13-14+15;1+2 3+4-5+6-7+8-9-10-11+12-13-14+15;1+2 3+4-5+6-7+8-9-10-11-12+13+14-15;1+2 3+4-5+6-7-8+9-10+11-12-13-14+15;1+2 3+4-5+6-7-8+9-10-11+12-13+14-15;1+2 3+4-5+6-7-8-9+10+11-12-13+14-15;1+2 3+4-5+6-7-8-9+10-11+12+13-14-15;1+2 3+4-5-6 7+8 9-10-11-12-13-14+15;1+2 3+4-5-6 7+8+9+10-11+12-13+14+15;1+2 3+4-5-6 7+8+9-10+11+12+13-14+15;1+2 3+4-5-6 7+8-9+10+11+12+13+14-15;1+2 3+4-5-6+7+8-9-10+11-12-13-14+15;1+2 3+4-5-6+7+8-9-10-11+12-13+14-15;1+2 3+4-5-6+7-8+9+10-11-12-13-14+15;1+2 3+4-5-6+7-8+9-10+11-12-13+14-15;1+2 3+4-5-6+7-8+9-10-11+12+13-14-15;1+2 3+4-5-6+7-8-9+10+11-12+13-14-15;1+2 3+4-5-6-7+8+9+10-11-12-13+14-15;1+2 3+4-5-6-7+8+9-10+11-12+13-14-15;1+2 3+4-5-6-7+8-9+10+11+12-13-14-15;1+2 3+4-5-6-7-8-9-10-11+12-13+14+15;1+2 3-4 5 6-7 8 9-10-11+12 13+14+15;1+2 3-4 5+6 7+8-9-10-11-12-13-14+15;1+2 3-4 5+6 7-8+9-10-11-12-13+14-15;1+2 3-4 5+6 7-8-9+10-11-12+13-14-15;1+2 3-4 5+6 7-8-9-10+11+12-13-14-15;1+2 3-4 5+6+7+8+9+10+11+12-13-14-15;1+2 3-4 5+6+7+8-9-10-11-12+13+14+15;1+2 3-4 5+6+7-8+9-10-11+12-13+14+15;1+2 3-4 5+6+7-8-9+10+11-12-13+14+15;1+2 3-4 5+6+7-8-9+10-11+12+13-14+15;1+2 3-4 5+6+7-8-9-10+11+12+13+14-15;1+2 3-4 5+6-7+8+9-10+11-12-13+14+15;1+2 3-4 5+6-7+8+9-10-11+12+13-14+15;1+2 3-4 5+6-7+8-9+10+11-12+13-14+15;1+2 3-4 5+6-7+8-9+10-11+12+13+14-15;1+2 3-4 5+6-7-8+9+10+11+12-13-14+15;1+2 3-4 5+6-7-8+9+10+11-12+13+14-15;1+2 3-4 5-6 7+8 9+10-11+12-13-14+15;1+2 3-4 5-6 7+8 9+10-11-12+13+14-15;1+2 3-4 5-6 7+8 9-10+11+12-13+14-15;1+2 3-4 5-6 7-8 9-10-11 12+13 14-15;1+2 3-4 5-6+7+8+9+10-11-12-13+14+15;1+2 3-4 5-6+7+8+9-10+11-12+13-14+15;1+2 3-4 5-6+7+8+9-10-11+12+13+14-15;1+2 3-4 5-6+7+8-9+10+11+12-13-14+15;1+2 3-4 5-6+7+8-9+10+11-12+13+14-15;1+2 3-4 5-6+7-8+9+10+11+12-13+14-15;1+2 3-4 5-6-7+8 9+10-11-12-13-14-15;1+2 3-4 5-6-7+8+9+10+11+12+13-14-15;1+2 3-4 5-6-7-8+9-10-11+12+13+14+15;1+2 3-4 5-6-7-8-9+10+11-12+13+14+15;1+2 3-4+5 6-7 8+9+10+11-12+13-14-15;1+2 3-4+5+6 7+8 9+10+11+12 13-14 15;1+2 3-4+5+6 7-8 9+10+11-12-13-14+15;1+2 3-4+5+6 7-8 9+10-11+12-13+14-15;1+2 3-4+5+6 7-8 9-10+11+12+13-14-15;1+2 3-4+5+6 7-8-9-10-11-12-13-14-15;1+2 3-4+5+6+7+8+9+10-11-12-13-14-15;1+2 3-4+5+6+7-8 9+10+11-12+13+14+15;1+2 3-4+5+6+7-8-9-10-11+12-13-14+15;1+2 3-4+5+6+7-8-9-10-11-12+13+14-15;1+2 3-4+5+6-7+8-9-10+11-12-13-14+15;1+2 3-4+5+6-7+8-9-10-11+12-13+14-15;1+2 3-4+5+6-7-8+9+10-11-12-13-14+15;1+2 3-4+5+6-7-8+9-10+11-12-13+14-15;1+2 3-4+5+6-7-8+9-10-11+12+13-14-15;1+2 3-4+5+6-7-8-9+10+11-12+13-14-15;1+2 3-4+5-6 7+8 9-10-11-12-13+14-15;1+2 3-4+5-6 7+8+9+10+11-12-13+14+15;1+2 3-4+5-6 7+8+9+10-11+12+13-14+15;1+2 3-4+5-6 7+8+9-10+11+12+13+14-15;1+2 3-4+5-6+7+8-9+10-11-12-13-14+15;1+2 3-4+5-6+7+8-9-10+11-12-13+14-15;1+2 3-4+5-6+7+8-9-10-11+12+13-14-15;1+2 3-4+5-6+7-8+9+10-11-12-13+14-15;1+2 3-4+5-6+7-8+9-10+11-12+13-14-15;1+2 3-4+5-6+7-8-9+10+11+12-13-14-15;1+2 3-4+5-6-7+8+9+10-11-12+13-14-15;1+2 3-4+5-6-7+8+9-10+11+12-13-14-15;1+2 3-4+5-6-7-8-9-10+11-12-13+14+15;1+2 3-4+5-6-7-8-9-10-11+12+13-14+15;1+2 3-4-5 6+7 8+9-10-11+12-13-14-15;1+2 3-4-5 6+7 8-9+10+11-12-13-14-15;1+2 3-4-5+6 7 8-9 10-11 12+13 14+15;1+2 3-4-5+6 7-8 9-10-11+12-13+14+15;1+2 3-4-5+6+7+8+9-10-11-12-13-14+15;1+2 3-4-5+6+7+8-9+10-11-12-13+14-15;1+2 3-4-5+6+7+8-9-10+11-12+13-14-15;1+2 3-4-5+6+7-8+9+10-11-12+13-14-15;1+2 3-4-5+6+7-8+9-10+11+12-13-14-15;1+2 3-4-5+6-7+8+9+10-11+12-13-14-15;1+2 3-4-5+6-7-8 9+10+11+12+13+14+15;1+2 3-4-5+6-7-8-9+10-11-12-13+14+15;1+2 3-4-5+6-7-8-9-10+11-12+13-14+15;1+2 3-4-5+6-7-8-9-10-11+12+13+14-15;1+2 3-4-5-6 7+8-9+10-11+12+13+14+15;1+2 3-4-5-6 7-8+9+10+11-12+13+14+15;1+2 3-4-5-6+7+8+9+10+11-12-13-14-15;1+2 3-4-5-6+7-8+9-10-11-12-13+14+15;1+2 3-4-5-6+7-8-9+10-11-12+13-14+15;1+2 3-4-5-6+7-8-9-10+11+12-13-14+15;1+2 3-4-5-6+7-8-9-10+11-12+13+14-15;1+2 3-4-5-6-7+8+9-10-11-12+13-14+15;1+2 3-4-5-6-7+8-9+10-11+12-13-14+15;1+2 3-4-5-6-7+8-9+10-11-12+13+14-15;1+2 3-4-5-6-7+8-9-10+11+12-13+14-15;1+2 3-4-5-6-7-8+9+10+11-12-13-14+15;1+2 3-4-5-6-7-8+9+10-11+12-13+14-15;1+2 3-4-5-6-7-8+9-10+11+12+13-14-15;1+2+3 4 5+6 7 8+9-10 11-12-13-14+15;1+2+3 4 5+6 7-8 9-10+11 12-13-14 15;1+2+3 4 5+6-7-8-9+10 11-12-13 14-15;1+2+3 4 5-6 7+8-9+10+11 12+13-14 15;1+2+3 4 5-6+7 8+9 10+11-12-13 14-15;1+2+3 4 5-6+7 8+9+10 11-12-13-14 15;1+2+3 4 5-6-7+8+9 10-11-12 13-14-15;1+2+3 4+5 6-7-8 9+10-11-12-13+14+15;1+2+3 4+5 6-7-8 9-10+11-12+13-14+15;1+2+3 4+5 6-7-8 9-10-11+12+13+14-15;1+2+3 4+5+6-7 8+9+10+11+12-13-14+15;1+2+3 4+5+6-7 8+9+10+11-12+13+14-15;1+2+3 4+5-6-7 8+9-10-11+12+13+14+15;1+2+3 4+5-6-7 8-9+10+11-12+13+14+15;1+2+3 4-5 6+7+8+9+10+11-12-13+14-15;1+2+3 4-5 6+7+8+9+10-11+12+13-14-15;1+2+3 4-5 6+7-8-9+10-11-12+13+14+15;1+2+3 4-5 6+7-8-9-10+11+12-13+14+15;1+2+3 4-5 6-7+8+9-10-11-12+13+14+15;1+2+3 4-5 6-7+8-9+10-11+12-13+14+15;1+2+3 4-5 6-7+8-9-10+11+12+13-14+15;1+2+3 4-5 6-7-8+9+10+11-12-13+14+15;1+2+3 4-5 6-7-8+9+10-11+12+13-14+15;1+2+3 4-5 6-7-8+9-10+11+12+13+14-15;1+2+3 4-5+6-7 8+9-10+11-12+13+14+15;1+2+3 4-5+6-7 8-9+10+11+12-13+14+15;1+2+3+4 5+6 7+8 9+10+11 12-13 14-15;1+2+3+4 5+6 7-8 9+10-11-12+13-14-15;1+2+3+4 5+6 7-8 9-10+11+12-13-14-15;1+2+3+4 5+6+7-8 9+10+11-12-13+14+15;1+2+3+4 5+6+7-8 9+10-11+12+13-14+15;1+2+3+4 5+6+7-8 9-10+11+12+13+14-15;1+2+3+4 5+6+7-8-9-10-11-12-13+14-15;1+2+3+4 5+6-7+8-9-10-11-12+13-14-15;1+2+3+4 5+6-7-8+9-10-11+12-13-14-15;1+2+3+4 5+6-7-8-9+10+11-12-13-14-15;1+2+3+4 5-6 7+8+9+10-11+12-13-14+15;1+2+3+4 5-6 7+8+9+10-11-12+13+14-15;1+2+3+4 5-6 7+8+9-10+11+12-13+14-15;1+2+3+4 5-6 7+8-9+10+11+12+13-14-15;1+2+3+4 5-6 7-8-9-10-11+12+13+14+15;1+2+3+4 5-6+7+8-9-10-11+12-13-14-15;1+2+3+4 5-6+7-8+9-10+11-12-13-14-15;1+2+3+4 5-6-7+8+9+10-11-12-13-14-15;1+2+3+4 5-6-7-8 9+10+11-12+13+14+15;1+2+3+4 5-6-7-8-9-10-11+12-13-14+15;1+2+3+4 5-6-7-8-9-10-11-12+13+14-15;1+2+3+4+5 6-7 8+9+10-11-12-13+14+15;1+2+3+4+5 6-7 8+9-10+11-12+13-14+15;1+2+3+4+5 6-7 8+9-10-11+12+13+14-15;1+2+3+4+5 6-7 8-9+10+11+12-13-14+15;1+2+3+4+5 6-7 8-9+10+11-12+13+14-15;1+2+3+4+5+6 7 8-9 10-11 12+13 14+15;1+2+3+4+5+6 7-8 9-10-11+12-13+14+15;1+2+3+4+5+6+7+8+9-10-11-12-13-14+15;1+2+3+4+5+6+7+8-9+10-11-12-13+14-15;1+2+3+4+5+6+7+8-9-10+11-12+13-14-15;1+2+3+4+5+6+7-8+9+10-11-12+13-14-15;1+2+3+4+5+6+7-8+9-10+11+12-13-14-15;1+2+3+4+5+6-7+8+9+10-11+12-13-14-15;1+2+3+4+5+6-7-8 9+10+11+12+13+14+15;1+2+3+4+5+6-7-8-9+10-11-12-13+14+15;1+2+3+4+5+6-7-8-9-10+11-12+13-14+15;1+2+3+4+5+6-7-8-9-10-11+12+13+14-15;1+2+3+4+5-6 7+8-9+10-11+12+13+14+15;1+2+3+4+5-6 7-8+9+10+11-12+13+14+15;1+2+3+4+5-6+7+8+9+10+11-12-13-14-15;1+2+3+4+5-6+7-8+9-10-11-12-13+14+15;1+2+3+4+5-6+7-8-9+10-11-12+13-14+15;1+2+3+4+5-6+7-8-9-10+11+12-13-14+15;1+2+3+4+5-6+7-8-9-10+11-12+13+14-15;1+2+3+4+5-6-7+8+9-10-11-12+13-14+15;1+2+3+4+5-6-7+8-9+10-11+12-13-14+15;1+2+3+4+5-6-7+8-9+10-11-12+13+14-15;1+2+3+4+5-6-7+8-9-10+11+12-13+14-15;1+2+3+4+5-6-7-8+9+10+11-12-13-14+15;1+2+3+4+5-6-7-8+9+10-11+12-13+14-15;1+2+3+4+5-6-7-8+9-10+11+12+13-14-15;1+2+3+4-5 6+7 8-9-10+11-12-13-14+15;1+2+3+4-5 6+7 8-9-10-11+12-13+14-15;1+2+3+4-5+6 7-8 9+10+11+12+13-14-15;1+2+3+4-5+6 7-8-9+10-11-12-13-14-15;1+2+3+4-5+6+7+8-9-10-11-12-13+14+15;1+2+3+4-5+6+7-8+9-10-11-12+13-14+15;1+2+3+4-5+6+7-8-9+10-11+12-13-14+15;1+2+3+4-5+6+7-8-9+10-11-12+13+14-15;1+2+3+4-5+6+7-8-9-10+11+12-13+14-15;1+2+3+4-5+6-7+8+9-10-11+12-13-14+15;1+2+3+4-5+6-7+8+9-10-11-12+13+14-15;1+2+3+4-5+6-7+8-9+10+11-12-13-14+15;1+2+3+4-5+6-7+8-9+10-11+12-13+14-15;1+2+3+4-5+6-7+8-9-10+11+12+13-14-15;1+2+3+4-5+6-7-8+9+10+11-12-13+14-15;1+2+3+4-5+6-7-8+9+10-11+12+13-14-15;1+2+3+4-5-6 7+8 9+10-11-12-13+14-15;1+2+3+4-5-6 7+8 9-10+11-12+13-14-15;1+2+3+4-5-6 7+8+9+10+11+12+13+14-15;1+2+3+4-5-6+7+8+9-10+11-12-13-14+15;1+2+3+4-5-6+7+8+9-10-11+12-13+14-15;1+2+3+4-5-6+7+8-9+10+11-12-13+14-15;1+2+3+4-5-6+7+8-9+10-11+12+13-14-15;1+2+3+4-5-6+7-8+9+10+11-12+13-14-15;1+2+3+4-5-6-7+8+9+10+11+12-13-14-15;1+2+3+4-5-6-7+8-9-10-11-12+13+14+15;1+2+3+4-5-6-7-8+9-10-11+12-13+14+15;1+2+3+4-5-6-7-8-9+10+11-12-13+14+15;1+2+3+4-5-6-7-8-9+10-11+12+13-14+15;1+2+3+4-5-6-7-8-9-10+11+12+13+14-15;1+2+3-4 5+6 7+8+9-10-11-12-13-14+15;1+2+3-4 5+6 7+8-9+10-11-12-13+14-15;1+2+3-4 5+6 7+8-9-10+11-12+13-14-15;1+2+3-4 5+6 7-8+9+10-11-12+13-14-15;1+2+3-4 5+6 7-8+9-10+11+12-13-14-15;1+2+3-4 5+6+7+8+9-10-11-12+13+14+15;1+2+3-4 5+6+7+8-9+10-11+12-13+14+15;1+2+3-4 5+6+7+8-9-10+11+12+13-14+15;1+2+3-4 5+6+7-8+9+10+11-12-13+14+15;1+2+3-4 5+6+7-8+9+10-11+12+13-14+15;1+2+3-4 5+6+7-8+9-10+11+12+13+14-15;1+2+3-4 5+6-7+8 9-10-11-12+13-14-15;1+2+3-4 5+6-7+8+9+10+11-12+13-14+15;1+2+3-4 5+6-7+8+9+10-11+12+13+14-15;1+2+3-4 5-6 7+8 9+10+11+12+13-14-15;1+2+3-4 5-6+7+8 9-10-11+12-13-14-15;1+2+3-4 5-6+7+8+9+10+11+12-13-14+15;1+2+3-4 5-6+7+8+9+10+11-12+13+14-15;1+2+3-4 5-6+7-8-9-10+11+12+13+14+15;1+2+3-4 5-6-7+8-9+10-11+12+13+14+15;1+2+3-4 5-6-7-8+9+10+11-12+13+14+15;1+2+3-4+5 6-7 8-9+10-11-12+13+14+15;1+2+3-4+5 6-7 8-9-10+11+12-13+14+15;1+2+3-4+5+6 7-8+9-10-11-12-13-14-15;1+2+3-4+5+6+7+8-9-10-11-12+13-14+15;1+2+3-4+5+6+7-8+9-10-11+12-13-14+15;1+2+3-4+5+6+7-8+9-10-11-12+13+14-15;1+2+3-4+5+6+7-8-9+10+11-12-13-14+15;1+2+3-4+5+6+7-8-9+10-11+12-13+14-15;1+2+3-4+5+6+7-8-9-10+11+12+13-14-15;1+2+3-4+5+6-7+8+9-10+11-12-13-14+15;1+2+3-4+5+6-7+8+9-10-11+12-13+14-15;1+2+3-4+5+6-7+8-9+10+11-12-13+14-15;1+2+3-4+5+6-7+8-9+10-11+12+13-14-15;1+2+3-4+5+6-7-8+9+10+11-12+13-14-15;1+2+3-4+5-6 7+8 9+10-11-12+13-14-15;1+2+3-4+5-6 7+8 9-10+11+12-13-14-15;1+2+3-4+5-6+7+8+9+10-11-12-13-14+15;1+2+3-4+5-6+7+8+9-10+11-12-13+14-15;1+2+3-4+5-6+7+8+9-10-11+12+13-14-15;1+2+3-4+5-6+7+8-9+10+11-12+13-14-15;1+2+3-4+5-6+7-8+9+10+11+12-13-14-15;1+2+3-4+5-6+7-8-9-10-11-12+13+14+15;1+2+3-4+5-6-7+8-9-10-11+12-13+14+15;1+2+3-4+5-6-7-8+9-10+11-12-13+14+15;1+2+3-4+5-6-7-8+9-10-11+12+13-14+15;1+2+3-4+5-6-7-8-9+10+11-12+13-14+15;1+2+3-4+5-6-7-8-9+10-11+12+13+14-15;1+2+3-4-5 6+7 8+9+10+11-12-13-14-15;1+2+3-4-5+6 7-8 9+10+11-12-13+14+15;1+2+3-4-5+6 7-8 9+10-11+12+13-14+15;1+2+3-4-5+6 7-8 9-10+11+12+13+14-15;1+2+3-4-5+6 7-8-9-10-11-12-13+14-15;1+2+3-4-5+6+7+8+9+10-11-12-13+14-15;1+2+3-4-5+6+7+8+9-10+11-12+13-14-15;1+2+3-4-5+6+7+8-9+10+11+12-13-14-15;1+2+3-4-5+6+7-8-9-10-11+12-13+14+15;1+2+3-4-5+6-7+8-9-10+11-12-13+14+15;1+2+3-4-5+6-7+8-9-10-11+12+13-14+15;1+2+3-4-5+6-7-8+9+10-11-12-13+14+15;1+2+3-4-5+6-7-8+9-10+11-12+13-14+15;1+2+3-4-5+6-7-8+9-10-11+12+13+14-15;1+2+3-4-5+6-7-8-9+10+11+12-13-14+15;1+2+3-4-5+6-7-8-9+10+11-12+13+14-15;1+2+3-4-5-6 7+8 9-10-11-12+13-14+15;1+2+3-4-5-6 7+8+9+10-11+12+13+14+15;1+2+3-4-5-6+7+8-9+10-11-12-13+14+15;1+2+3-4-5-6+7+8-9-10+11-12+13-14+15;1+2+3-4-5-6+7+8-9-10-11+12+13+14-15;1+2+3-4-5-6+7-8+9+10-11-12+13-14+15;1+2+3-4-5-6+7-8+9-10+11+12-13-14+15;1+2+3-4-5-6+7-8+9-10+11-12+13+14-15;1+2+3-4-5-6+7-8-9+10+11+12-13+14-15;1+2+3-4-5-6-7+8+9+10-11+12-13-14+15;1+2+3-4-5-6-7+8+9+10-11-12+13+14-15;1+2+3-4-5-6-7+8+9-10+11+12-13+14-15;1+2+3-4-5-6-7+8-9+10+11+12+13-14-15;1+2+3-4-5-6-7-8-9-10-11+12+13+14+15;1+2-3 4 5+6 7+8-9-10 11-12+13 14-15;1+2-3 4 5+6+7+8-9 10-11+12 13+14+15;1+2-3 4 5+6+7+8-9-10 11+12+13 14+15;1+2-3 4 5+6-7 8+9-10 11-12+13+14 15;1+2-3 4 5+6-7+8+9+10-11 12+13+14 15;1+2-3 4 5-6 7 8+9+10 11+12-13-14+15;1+2-3 4 5-6 7 8+9+10 11-12+13+14-15;1+2-3 4+5 6+7+8+9-10-11-12+13-14-15;1+2-3 4+5 6+7+8-9+10-11+12-13-14-15;1+2-3 4+5 6+7-8+9+10+11-12-13-14-15;1+2-3 4+5 6-7+8-9-10-11-12-13+14+15;1+2-3 4+5 6-7-8+9-10-11-12+13-14+15;1+2-3 4+5 6-7-8-9+10-11+12-13-14+15;1+2-3 4+5 6-7-8-9+10-11-12+13+14-15;1+2-3 4+5 6-7-8-9-10+11+12-13+14-15;1+2-3 4+5+6+7 8-9-10-11-12+13-14-15;1+2-3 4+5+6-7 8 9+10 11+12 13-14 15;1+2-3 4+5-6+7 8+9+10-11-12-13-14-15;1+2-3 4-5 6+7+8 9+10+11+12-13-14-15;1+2-3 4-5 6-7+8 9-10+11-12-13+14+15;1+2-3 4-5 6-7+8 9-10-11+12+13-14+15;1+2-3 4-5-6+7 8+9-10-11-12-13-14+15;1+2-3 4-5-6+7 8-9+10-11-12-13+14-15;1+2-3 4-5-6+7 8-9-10+11-12+13-14-15;1+2-3+4 5+6 7 8-9 10-11 12+13 14-15;1+2-3+4 5+6 7+8 9+10 11-12 13-14+15;1+2-3+4 5+6 7+8 9-10+11+12 13-14 15;1+2-3+4 5+6 7-8 9-10+11-12-13-14+15;1+2-3+4 5+6 7-8 9-10-11+12-13+14-15;1+2-3+4 5+6+7+8+9-10-11-12-13-14-15;1+2-3+4 5+6+7-8 9-10+11-12+13+14+15;1+2-3+4 5+6-7-8 9+10+11+12+13+14-15;1+2-3+4 5+6-7-8+9-10-11-12-13-14+15;1+2-3+4 5+6-7-8-9+10-11-12-13+14-15;1+2-3+4 5+6-7-8-9-10+11-12+13-14-15;1+2-3+4 5-6 7+8+9-10+11-12-13+14+15;1+2-3+4 5-6 7+8+9-10-11+12+13-14+15;1+2-3+4 5-6 7+8-9+10+11-12+13-14+15;1+2-3+4 5-6 7+8-9+10-11+12+13+14-15;1+2-3+4 5-6 7-8+9+10+11+12-13-14+15;1+2-3+4 5-6 7-8+9+10+11-12+13+14-15;1+2-3+4 5-6+7+8-9-10-11-12-13-14+15;1+2-3+4 5-6+7-8+9-10-11-12-13+14-15;1+2-3+4 5-6+7-8-9+10-11-12+13-14-15;1+2-3+4 5-6+7-8-9-10+11+12-13-14-15;1+2-3+4 5-6-7+8+9-10-11-12+13-14-15;1+2-3+4 5-6-7+8-9+10-11+12-13-14-15;1+2-3+4 5-6-7-8+9+10+11-12-13-14-15;1+2-3+4+5 6-7 8+9-10-11-12+13+14+15;1+2-3+4+5 6-7 8-9+10-11+12-13+14+15;1+2-3+4+5 6-7 8-9-10+11+12+13-14+15;1+2-3+4+5+6 7+8-9-10-11-12-13-14-15;1+2-3+4+5+6+7+8-9-10-11+12-13-14+15;1+2-3+4+5+6+7+8-9-10-11-12+13+14-15;1+2-3+4+5+6+7-8+9-10+11-12-13-14+15;1+2-3+4+5+6+7-8+9-10-11+12-13+14-15;1+2-3+4+5+6+7-8-9+10+11-12-13+14-15;1+2-3+4+5+6+7-8-9+10-11+12+13-14-15;1+2-3+4+5+6-7+8+9+10-11-12-13-14+15;1+2-3+4+5+6-7+8+9-10+11-12-13+14-15;1+2-3+4+5+6-7+8+9-10-11+12+13-14-15;1+2-3+4+5+6-7+8-9+10+11-12+13-14-15;1+2-3+4+5+6-7-8+9+10+11+12-13-14-15;1+2-3+4+5+6-7-8-9-10-11-12+13+14+15;1+2-3+4+5-6 7+8 9+10-11+12-13-14-15;1+2-3+4+5-6 7-8-9+10+11+12+13+14+15;1+2-3+4+5-6+7+8+9+10-11-12-13+14-15;1+2-3+4+5-6+7+8+9-10+11-12+13-14-15;1+2-3+4+5-6+7+8-9+10+11+12-13-14-15;1+2-3+4+5-6+7-8-9-10-11+12-13+14+15;1+2-3+4+5-6-7+8-9-10+11-12-13+14+15;1+2-3+4+5-6-7+8-9-10-11+12+13-14+15;1+2-3+4+5-6-7-8+9+10-11-12-13+14+15;1+2-3+4+5-6-7-8+9-10+11-12+13-14+15;1+2-3+4+5-6-7-8+9-10-11+12+13+14-15;1+2-3+4+5-6-7-8-9+10+11+12-13-14+15;1+2-3+4+5-6-7-8-9+10+11-12+13+14-15;1+2-3+4-5 6+7 8-9-10-11-12-13+14+15;1+2-3+4-5+6 7-8 9+10+11-12+13-14+15;1+2-3+4-5+6 7-8 9+10-11+12+13+14-15;1+2-3+4-5+6 7-8-9-10-11-12+13-14-15;1+2-3+4-5+6+7+8+9+10-11-12+13-14-15;1+2-3+4-5+6+7+8+9-10+11+12-13-14-15;1+2-3+4-5+6+7-8-9-10+11-12-13+14+15;1+2-3+4-5+6+7-8-9-10-11+12+13-14+15;1+2-3+4-5+6-7+8-9+10-11-12-13+14+15;1+2-3+4-5+6-7+8-9-10+11-12+13-14+15;1+2-3+4-5+6-7+8-9-10-11+12+13+14-15;1+2-3+4-5+6-7-8+9+10-11-12+13-14+15;1+2-3+4-5+6-7-8+9-10+11+12-13-14+15;1+2-3+4-5+6-7-8+9-10+11-12+13+14-15;1+2-3+4-5+6-7-8-9+10+11+12-13+14-15;1+2-3+4-5-6 7+8 9-10-11+12-13-14+15;1+2-3+4-5-6 7+8 9-10-11-12+13+14-15;1+2-3+4-5-6 7+8+9+10+11-12+13+14+15;1+2-3+4-5-6+7+8+9-10-11-12-13+14+15;1+2-3+4-5-6+7+8-9+10-11-12+13-14+15;1+2-3+4-5-6+7+8-9-10+11+12-13-14+15;1+2-3+4-5-6+7+8-9-10+11-12+13+14-15;1+2-3+4-5-6+7-8+9+10-11+12-13-14+15;1+2-3+4-5-6+7-8+9+10-11-12+13+14-15;1+2-3+4-5-6+7-8+9-10+11+12-13+14-15;1+2-3+4-5-6+7-8-9+10+11+12+13-14-15;1+2-3+4-5-6-7+8 9-10-11-12-13-14-15;1+2-3+4-5-6-7+8+9+10+11-12-13-14+15;1+2-3+4-5-6-7+8+9+10-11+12-13+14-15;1+2-3+4-5-6-7+8+9-10+11+12+13-14-15;1+2-3+4-5-6-7-8-9-10+11-12+13+14+15;1+2-3-4 5+6 7+8-9-10-11+12-13-14+15;1+2-3-4 5+6 7+8-9-10-11-12+13+14-15;1+2-3-4 5+6 7-8+9-10+11-12-13-14+15;1+2-3-4 5+6 7-8+9-10-11+12-13+14-15;1+2-3-4 5+6 7-8-9+10+11-12-13+14-15;1+2-3-4 5+6 7-8-9+10-11+12+13-14-15;1+2-3-4 5+6+7+8-9-10-11+12+13+14+15;1+2-3-4 5+6+7-8+9-10+11-12+13+14+15;1+2-3-4 5+6+7-8-9+10+11+12-13+14+15;1+2-3-4 5+6-7+8+9+10-11-12+13+14+15;1+2-3-4 5+6-7+8+9-10+11+12-13+14+15;1+2-3-4 5+6-7+8-9+10+11+12+13-14+15;1+2-3-4 5+6-7-8+9+10+11+12+13+14-15;1+2-3-4 5-6 7 8+9 10+11 12-13 14+15;1+2-3-4 5-6 7+8 9+10+11-12+13-14+15;1+2-3-4 5-6 7+8 9+10-11+12+13+14-15;1+2-3-4 5-6 7-8 9+10-11-12 13+14 15;1+2-3-4 5-6 7-8 9-10 11+12 13+14-15;1+2-3-4 5-6+7+8 9-10-11-12-13-14+15;1+2-3-4 5-6+7+8+9+10-11+12-13+14+15;1+2-3-4 5-6+7+8+9-10+11+12+13-14+15;1+2-3-4 5-6+7+8-9+10+11+12+13+14-15;1+2-3-4 5-6-7+8 9+10-11+12-13-14-15;1+2-3-4 5-6-7-8-9+10+11+12+13+14+15;1+2-3-4+5 6-7 8+9+10+11+12+13-14-15;1+2-3-4+5+6 7-8 9+10+11+12-13-14+15;1+2-3-4+5+6 7-8 9+10+11-12+13+14-15;1+2-3-4+5+6 7-8-9-10-11+12-13-14-15;1+2-3-4+5+6+7+8+9+10-11+12-13-14-15;1+2-3-4+5+6+7-8 9+10+11+12+13+14+15;1+2-3-4+5+6+7-8-9+10-11-12-13+14+15;1+2-3-4+5+6+7-8-9-10+11-12+13-14+15;1+2-3-4+5+6+7-8-9-10-11+12+13+14-15;1+2-3-4+5+6-7+8+9-10-11-12-13+14+15;1+2-3-4+5+6-7+8-9+10-11-12+13-14+15;1+2-3-4+5+6-7+8-9-10+11+12-13-14+15;1+2-3-4+5+6-7+8-9-10+11-12+13+14-15;1+2-3-4+5+6-7-8+9+10-11+12-13-14+15;1+2-3-4+5+6-7-8+9+10-11-12+13+14-15;1+2-3-4+5+6-7-8+9-10+11+12-13+14-15;1+2-3-4+5+6-7-8-9+10+11+12+13-14-15;1+2-3-4+5-6 7+8 9-10+11-12-13-14+15;1+2-3-4+5-6 7+8 9-10-11+12-13+14-15;1+2-3-4+5-6 7+8+9+10+11+12-13+14+15;1+2-3-4+5-6+7+8+9-10-11-12+13-14+15;1+2-3-4+5-6+7+8-9+10-11+12-13-14+15;1+2-3-4+5-6+7+8-9+10-11-12+13+14-15;1+2-3-4+5-6+7+8-9-10+11+12-13+14-15;1+2-3-4+5-6+7-8+9+10+11-12-13-14+15;1+2-3-4+5-6+7-8+9+10-11+12-13+14-15;1+2-3-4+5-6+7-8+9-10+11+12+13-14-15;1+2-3-4+5-6-7+8+9+10+11-12-13+14-15;1+2-3-4+5-6-7+8+9+10-11+12+13-14-15;1+2-3-4+5-6-7-8-9+10-11-12+13+14+15;1+2-3-4+5-6-7-8-9-10+11+12-13+14+15;1+2-3-4-5 6+7 8+9+10-11-12-13+14-15;1+2-3-4-5 6+7 8+9-10+11-12+13-14-15;1+2-3-4-5 6+7 8-9+10+11+12-13-14-15;1+2-3-4-5+6 7+8+9-10-11-12-13-14-15;1+2-3-4-5+6 7-8 9-10+11-12+13+14+15;1+2-3-4-5+6+7+8+9-10-11+12-13-14+15;1+2-3-4-5+6+7+8+9-10-11-12+13+14-15;1+2-3-4-5+6+7+8-9+10+11-12-13-14+15;1+2-3-4-5+6+7+8-9+10-11+12-13+14-15;1+2-3-4-5+6+7+8-9-10+11+12+13-14-15;1+2-3-4-5+6+7-8+9+10+11-12-13+14-15;1+2-3-4-5+6+7-8+9+10-11+12+13-14-15;1+2-3-4-5+6-7+8+9+10+11-12+13-14-15;1+2-3-4-5+6-7-8+9-10-11-12+13+14+15;1+2-3-4-5+6-7-8-9+10-11+12-13+14+15;1+2-3-4-5+6-7-8-9-10+11+12+13-14+15;1+2-3-4-5-6 7-8+9+10+11+12+13+14+15;1+2-3-4-5-6+7+8+9+10+11+12-13-14-15;1+2-3-4-5-6+7+8-9-10-11-12+13+14+15;1+2-3-4-5-6+7-8+9-10-11+12-13+14+15;1+2-3-4-5-6+7-8-9+10+11-12-13+14+15;1+2-3-4-5-6+7-8-9+10-11+12+13-14+15;1+2-3-4-5-6+7-8-9-10+11+12+13+14-15;1+2-3-4-5-6-7+8+9-10+11-12-13+14+15;1+2-3-4-5-6-7+8+9-10-11+12+13-14+15;1+2-3-4-5-6-7+8-9+10+11-12+13-14+15;1+2-3-4-5-6-7+8-9+10-11+12+13+14-15;1+2-3-4-5-6-7-8+9+10+11+12-13-14+15;1+2-3-4-5-6-7-8+9+10+11-12+13+14-15;1-2 3 4 5+6+7-8+9 10-11+12+13+14 15;1-2 3 4 5+6-7 8-9+10 11+12-13+14 15;1-2 3 4 5+6-7+8+9 10+11-12+13+14 15;1-2 3 4 5+6-7+8+9+10 11-12+13 14+15;1-2 3 4 5-6 7+8 9+10 11+12+13 14-15;1-2 3 4 5-6+7+8+9 10+11+12-13+14 15;1-2 3 4+5 6-7-8-9+10-11-12 13+14 15;1-2 3 4+5 6-7-8-9-10 11+12 13+14-15;1-2 3 4+5+6-7 8 9+10 11+12-13-14+15;1-2 3 4+5+6-7 8 9+10 11-12+13+14-15;1-2 3 4+5-6-7-8 9-10 11+12+13 14+15;1-2 3 4-5 6-7+8 9-10-11 12+13 14+15;1-2 3 4-5-6 7+8-9-10 11-12+13 14+15;1-2 3 4-5-6 7+8-9-10-11 12+13+14 15;1-2 3 4-5-6 7-8-9 10+11+12 13+14-15;1-2 3 4-5-6+7-8 9+10-11 12+13+14 15;1-2 3+4 5+6 7-8 9+10-11+12-13-14+15;1-2 3+4 5+6 7-8 9+10-11-12+13+14-15;1-2 3+4 5+6 7-8 9-10+11+12-13+14-15;1-2 3+4 5+6+7+8+9-10+11-12-13-14-15;1-2 3+4 5+6+7-8 9+10-11+12+13+14+15;1-2 3+4 5+6+7-8-9-10-11-12+13-14+15;1-2 3+4 5+6-7+8-9-10-11+12-13-14+15;1-2 3+4 5+6-7+8-9-10-11-12+13+14-15;1-2 3+4 5+6-7-8+9-10+11-12-13-14+15;1-2 3+4 5+6-7-8+9-10-11+12-13+14-15;1-2 3+4 5+6-7-8-9+10+11-12-13+14-15;1-2 3+4 5+6-7-8-9+10-11+12+13-14-15;1-2 3+4 5-6 7+8 9-10-11-12-13-14+15;1-2 3+4 5-6 7+8+9+10-11+12-13+14+15;1-2 3+4 5-6 7+8+9-10+11+12+13-14+15;1-2 3+4 5-6 7+8-9+10+11+12+13+14-15;1-2 3+4 5-6+7+8-9-10+11-12-13-14+15;1-2 3+4 5-6+7+8-9-10-11+12-13+14-15;1-2 3+4 5-6+7-8+9+10-11-12-13-14+15;1-2 3+4 5-6+7-8+9-10+11-12-13+14-15;1-2 3+4 5-6+7-8+9-10-11+12+13-14-15;1-2 3+4 5-6+7-8-9+10+11-12+13-14-15;1-2 3+4 5-6-7+8+9+10-11-12-13+14-15;1-2 3+4 5-6-7+8+9-10+11-12+13-14-15;1-2 3+4 5-6-7+8-9+10+11+12-13-14-15;1-2 3+4 5-6-7-8-9-10-11+12-13+14+15;1-2 3+4+5 6 7-8+9 10-11-12-13-14 15;1-2 3+4+5 6-7 8+9-10+11-12+13+14+15;1-2 3+4+5 6-7 8-9+10+11+12-13+14+15;1-2 3+4+5+6 7+8-9-10+11-12-13-14-15;1-2 3+4+5+6 7-8+9+10-11-12-13-14-15;1-2 3+4+5+6+7+8+9-10-11-12-13+14+15;1-2 3+4+5+6+7+8-9+10-11-12+13-14+15;1-2 3+4+5+6+7+8-9-10+11+12-13-14+15;1-2 3+4+5+6+7+8-9-10+11-12+13+14-15;1-2 3+4+5+6+7-8+9+10-11+12-13-14+15;1-2 3+4+5+6+7-8+9+10-11-12+13+14-15;1-2 3+4+5+6+7-8+9-10+11+12-13+14-15;1-2 3+4+5+6+7-8-9+10+11+12+13-14-15;1-2 3+4+5+6-7+8 9-10-11-12-13-14-15;1-2 3+4+5+6-7+8+9+10+11-12-13-14+15;1-2 3+4+5+6-7+8+9+10-11+12-13+14-15;1-2 3+4+5+6-7+8+9-10+11+12+13-14-15;1-2 3+4+5+6-7-8-9-10+11-12+13+14+15;1-2 3+4+5-6 7+8 9+10+11+12-13-14-15;1-2 3+4+5-6+7+8+9+10+11-12-13+14-15;1-2 3+4+5-6+7+8+9+10-11+12+13-14-15;1-2 3+4+5-6+7-8-9+10-11-12+13+14+15;1-2 3+4+5-6+7-8-9-10+11+12-13+14+15;1-2 3+4+5-6-7+8+9-10-11-12+13+14+15;1-2 3+4+5-6-7+8-9+10-11+12-13+14+15;1-2 3+4+5-6-7+8-9-10+11+12+13-14+15;1-2 3+4+5-6-7-8+9+10+11-12-13+14+15;1-2 3+4+5-6-7-8+9+10-11+12+13-14+15;1-2 3+4+5-6-7-8+9-10+11+12+13+14-15;1-2 3+4-5 6+7 8-9-10+11-12-13+14+15;1-2 3+4-5 6+7 8-9-10-11+12+13-14+15;1-2 3+4-5+6 7-8 9+10+11+12+13+14-15;1-2 3+4-5+6 7-8+9-10-11-12-13-14+15;1-2 3+4-5+6 7-8-9+10-11-12-13+14-15;1-2 3+4-5+6 7-8-9-10+11-12+13-14-15;1-2 3+4-5+6+7+8+9+10+11-12+13-14-15;1-2 3+4-5+6+7-8+9-10-11-12+13+14+15;1-2 3+4-5+6+7-8-9+10-11+12-13+14+15;1-2 3+4-5+6+7-8-9-10+11+12+13-14+15;1-2 3+4-5+6-7+8+9-10-11+12-13+14+15;1-2 3+4-5+6-7+8-9+10+11-12-13+14+15;1-2 3+4-5+6-7+8-9+10-11+12+13-14+15;1-2 3+4-5+6-7+8-9-10+11+12+13+14-15;1-2 3+4-5+6-7-8+9+10+11-12+13-14+15;1-2 3+4-5+6-7-8+9+10-11+12+13+14-15;1-2 3+4-5-6 7+8 9+10-11-12+13-14+15;1-2 3+4-5-6 7+8 9-10+11+12-13-14+15;1-2 3+4-5-6 7+8 9-10+11-12+13+14-15;1-2 3+4-5-6+7+8+9-10+11-12-13+14+15;1-2 3+4-5-6+7+8+9-10-11+12+13-14+15;1-2 3+4-5-6+7+8-9+10+11-12+13-14+15;1-2 3+4-5-6+7+8-9+10-11+12+13+14-15;1-2 3+4-5-6+7-8+9+10+11+12-13-14+15;1-2 3+4-5-6+7-8+9+10+11-12+13+14-15;1-2 3+4-5-6-7+8 9-10+11-12-13-14-15;1-2 3+4-5-6-7+8+9+10+11+12-13+14-15;1-2 3+4-5-6-7-8-9+10-11+12+13+14+15;1-2 3-4 5 6-7-8-9 10-11+12-13+14 15;1-2 3-4 5+6 7+8+9-10-11-12-13+14+15;1-2 3-4 5+6 7+8-9+10-11-12+13-14+15;1-2 3-4 5+6 7+8-9-10+11+12-13-14+15;1-2 3-4 5+6 7+8-9-10+11-12+13+14-15;1-2 3-4 5+6 7-8+9+10-11+12-13-14+15;1-2 3-4 5+6 7-8+9+10-11-12+13+14-15;1-2 3-4 5+6 7-8+9-10+11+12-13+14-15;1-2 3-4 5+6 7-8-9+10+11+12+13-14-15;1-2 3-4 5+6+7+8-9-10+11+12+13+14+15;1-2 3-4 5+6+7-8+9+10-11+12+13+14+15;1-2 3-4 5+6-7+8 9-10-11+12-13-14+15;1-2 3-4 5+6-7+8 9-10-11-12+13+14-15;1-2 3-4 5+6-7+8+9+10+11-12+13+14+15;1-2 3-4 5-6 7+8 9+10+11+12+13+14-15;1-2 3-4 5-6 7-8 9+10+11-12 13+14 15;1-2 3-4 5-6+7+8 9-10+11-12-13-14+15;1-2 3-4 5-6+7+8 9-10-11+12-13+14-15;1-2 3-4 5-6+7+8+9+10+11+12-13+14+15;1-2 3-4 5-6-7+8 9+10+11+12-13-14-15;1-2 3-4+5+6 7+8-9-10-11-12-13-14+15;1-2 3-4+5+6 7-8+9-10-11-12-13+14-15;1-2 3-4+5+6 7-8-9+10-11-12+13-14-15;1-2 3-4+5+6 7-8-9-10+11+12-13-14-15;1-2 3-4+5+6+7+8+9+10+11+12-13-14-15;1-2 3-4+5+6+7+8-9-10-11-12+13+14+15;1-2 3-4+5+6+7-8+9-10-11+12-13+14+15;1-2 3-4+5+6+7-8-9+10+11-12-13+14+15;1-2 3-4+5+6+7-8-9+10-11+12+13-14+15;1-2 3-4+5+6+7-8-9-10+11+12+13+14-15;1-2 3-4+5+6-7+8+9-10+11-12-13+14+15;1-2 3-4+5+6-7+8+9-10-11+12+13-14+15;1-2 3-4+5+6-7+8-9+10+11-12+13-14+15;1-2 3-4+5+6-7+8-9+10-11+12+13+14-15;1-2 3-4+5+6-7-8+9+10+11+12-13-14+15;1-2 3-4+5+6-7-8+9+10+11-12+13+14-15;1-2 3-4+5-6 7+8 9+10-11+12-13-14+15;1-2 3-4+5-6 7+8 9+10-11-12+13+14-15;1-2 3-4+5-6 7+8 9-10+11+12-13+14-15;1-2 3-4+5-6 7-8 9-10-11 12+13 14-15;1-2 3-4+5-6+7+8+9+10-11-12-13+14+15;1-2 3-4+5-6+7+8+9-10+11-12+13-14+15;1-2 3-4+5-6+7+8+9-10-11+12+13+14-15;1-2 3-4+5-6+7+8-9+10+11+12-13-14+15;1-2 3-4+5-6+7+8-9+10+11-12+13+14-15;1-2 3-4+5-6+7-8+9+10+11+12-13+14-15;1-2 3-4+5-6-7+8 9+10-11-12-13-14-15;1-2 3-4+5-6-7+8+9+10+11+12+13-14-15;1-2 3-4+5-6-7-8+9-10-11+12+13+14+15;1-2 3-4+5-6-7-8-9+10+11-12+13+14+15;1-2 3-4-5 6+7 8+9+10+11-12-13+14-15;1-2 3-4-5 6+7 8+9+10-11+12+13-14-15;1-2 3-4-5 6-7 8-9 10+11 12-13-14-15;1-2 3-4-5+6 7+8+9-10+11-12-13-14-15;1-2 3-4-5+6 7-8 9+10-11+12+13+14+15;1-2 3-4-5+6 7-8-9-10-11-12+13-14+15;1-2 3-4-5+6+7+8+9+10-11-12+13-14+15;1-2 3-4-5+6+7+8+9-10+11+12-13-14+15;1-2 3-4-5+6+7+8+9-10+11-12+13+14-15;1-2 3-4-5+6+7+8-9+10+11+12-13+14-15;1-2 3-4-5+6+7-8+9+10+11+12+13-14-15;1-2 3-4-5+6-7+8-9-10-11+12+13+14+15;1-2 3-4-5+6-7-8+9-10+11-12+13+14+15;1-2 3-4-5+6-7-8-9+10+11+12-13+14+15;1-2 3-4-5-6 7+8 9-10-11-12+13+14+15;1-2 3-4-5-6+7+8-9-10+11-12+13+14+15;1-2 3-4-5-6+7-8+9+10-11-12+13+14+15;1-2 3-4-5-6+7-8+9-10+11+12-13+14+15;1-2 3-4-5-6+7-8-9+10+11+12+13-14+15;1-2 3-4-5-6-7+8 9-10-11-12-13-14+15;1-2 3-4-5-6-7+8+9+10-11+12-13+14+15;1-2 3-4-5-6-7+8+9-10+11+12+13-14+15;1-2 3-4-5-6-7+8-9+10+11+12+13+14-15;1-2+3 4 5+6 7 8-9-10 11+12-13+14-15;1-2+3 4 5+6-7-8-9-10+11 12-13-14 15;1-2+3 4 5-6 7+8+9 10-11-12 13+14+15;1-2+3 4 5-6 7+8-9+10 11+12-13 14+15;1-2+3 4 5-6-7+8-9+10 11-12-13 14-15;1-2+3 4+5 6+7-8 9+10+11-12+13-14-15;1-2+3 4+5 6-7-8 9-10-11+12-13+14+15;1-2+3 4+5+6-7 8+9+10+11-12-13+14+15;1-2+3 4+5+6-7 8+9+10-11+12+13-14+15;1-2+3 4+5+6-7 8+9-10+11+12+13+14-15;1-2+3 4+5-6-7 8-9-10+11+12+13+14+15;1-2+3 4-5 6+7+8+9+10-11+12-13-14+15;1-2+3 4-5 6+7+8+9+10-11-12+13+14-15;1-2+3 4-5 6+7+8+9-10+11+12-13+14-15;1-2+3 4-5 6+7+8-9+10+11+12+13-14-15;1-2+3 4-5 6+7-8-9-10-11+12+13+14+15;1-2+3 4-5 6-7+8-9-10+11-12+13+14+15;1-2+3 4-5 6-7-8+9+10-11-12+13+14+15;1-2+3 4-5 6-7-8+9-10+11+12-13+14+15;1-2+3 4-5 6-7-8-9+10+11+12+13-14+15;1-2+3 4-5+6-7 8-9+10-11+12+13+14+15;1-2+3 4-5-6-7 8+9+10+11+12+13-14+15;1-2+3+4 5+6 7+8 9+10 11-12 13+14-15;1-2+3+4 5+6 7+8 9+10-11+12 13-14 15;1-2+3+4 5+6 7-8 9+10-11-12-13-14+15;1-2+3+4 5+6 7-8 9-10+11-12-13+14-15;1-2+3+4 5+6 7-8 9-10-11+12+13-14-15;1-2+3+4 5+6+7-8 9+10-11-12+13+14+15;1-2+3+4 5+6+7-8 9-10+11+12-13+14+15;1-2+3+4 5+6-7+8-9-10-11-12-13-14+15;1-2+3+4 5+6-7-8+9-10-11-12-13+14-15;1-2+3+4 5+6-7-8-9+10-11-12+13-14-15;1-2+3+4 5+6-7-8-9-10+11+12-13-14-15;1-2+3+4 5-6 7+8+9+10-11-12-13+14+15;1-2+3+4 5-6 7+8+9-10+11-12+13-14+15;1-2+3+4 5-6 7+8+9-10-11+12+13+14-15;1-2+3+4 5-6 7+8-9+10+11+12-13-14+15;1-2+3+4 5-6 7+8-9+10+11-12+13+14-15;1-2+3+4 5-6 7-8+9+10+11+12-13+14-15;1-2+3+4 5-6+7+8-9-10-11-12-13+14-15;1-2+3+4 5-6+7-8+9-10-11-12+13-14-15;1-2+3+4 5-6+7-8-9+10-11+12-13-14-15;1-2+3+4 5-6-7+8+9-10-11+12-13-14-15;1-2+3+4 5-6-7+8-9+10+11-12-13-14-15;1-2+3+4 5-6-7-8 9-10+11+12+13+14+15;1-2+3+4 5-6-7-8-9-10-11-12-13+14+15;1-2+3+4+5 6-7 8+9-10-11+12-13+14+15;1-2+3+4+5 6-7 8-9+10+11-12-13+14+15;1-2+3+4+5 6-7 8-9+10-11+12+13-14+15;1-2+3+4+5 6-7 8-9-10+11+12+13+14-15;1-2+3+4+5+6+7+8-9-10+11-12-13-14+15;1-2+3+4+5+6+7+8-9-10-11+12-13+14-15;1-2+3+4+5+6+7-8+9+10-11-12-13-14+15;1-2+3+4+5+6+7-8+9-10+11-12-13+14-15;1-2+3+4+5+6+7-8+9-10-11+12+13-14-15;1-2+3+4+5+6+7-8-9+10+11-12+13-14-15;1-2+3+4+5+6-7+8+9+10-11-12-13+14-15;1-2+3+4+5+6-7+8+9-10+11-12+13-14-15;1-2+3+4+5+6-7+8-9+10+11+12-13-14-15;1-2+3+4+5+6-7-8-9-10-11+12-13+14+15;1-2+3+4+5-6 7+8 9+10+11-12-13-14-15;1-2+3+4+5-6 7-8+9-10+11+12+13+14+15;1-2+3+4+5-6+7+8+9+10-11-12+13-14-15;1-2+3+4+5-6+7+8+9-10+11+12-13-14-15;1-2+3+4+5-6+7-8-9-10+11-12-13+14+15;1-2+3+4+5-6+7-8-9-10-11+12+13-14+15;1-2+3+4+5-6-7+8-9+10-11-12-13+14+15;1-2+3+4+5-6-7+8-9-10+11-12+13-14+15;1-2+3+4+5-6-7+8-9-10-11+12+13+14-15;1-2+3+4+5-6-7-8+9+10-11-12+13-14+15;1-2+3+4+5-6-7-8+9-10+11+12-13-14+15;1-2+3+4+5-6-7-8+9-10+11-12+13+14-15;1-2+3+4+5-6-7-8-9+10+11+12-13+14-15;1-2+3+4-5 6+7 8-9-10-11-12+13-14+15;1-2+3+4-5+6 7-8 9+10+11+12-13-14+15;1-2+3+4-5+6 7-8 9+10+11-12+13+14-15;1-2+3+4-5+6 7-8-9-10-11+12-13-14-15;1-2+3+4-5+6+7+8+9+10-11+12-13-14-15;1-2+3+4-5+6+7-8 9+10+11+12+13+14+15;1-2+3+4-5+6+7-8-9+10-11-12-13+14+15;1-2+3+4-5+6+7-8-9-10+11-12+13-14+15;1-2+3+4-5+6+7-8-9-10-11+12+13+14-15;1-2+3+4-5+6-7+8+9-10-11-12-13+14+15;1-2+3+4-5+6-7+8-9+10-11-12+13-14+15;1-2+3+4-5+6-7+8-9-10+11+12-13-14+15;1-2+3+4-5+6-7+8-9-10+11-12+13+14-15;1-2+3+4-5+6-7-8+9+10-11+12-13-14+15;1-2+3+4-5+6-7-8+9+10-11-12+13+14-15;1-2+3+4-5+6-7-8+9-10+11+12-13+14-15;1-2+3+4-5+6-7-8-9+10+11+12+13-14-15;1-2+3+4-5-6 7+8 9-10+11-12-13-14+15;1-2+3+4-5-6 7+8 9-10-11+12-13+14-15;1-2+3+4-5-6 7+8+9+10+11+12-13+14+15;1-2+3+4-5-6+7+8+9-10-11-12+13-14+15;1-2+3+4-5-6+7+8-9+10-11+12-13-14+15;1-2+3+4-5-6+7+8-9+10-11-12+13+14-15;1-2+3+4-5-6+7+8-9-10+11+12-13+14-15;1-2+3+4-5-6+7-8+9+10+11-12-13-14+15;1-2+3+4-5-6+7-8+9+10-11+12-13+14-15;1-2+3+4-5-6+7-8+9-10+11+12+13-14-15;1-2+3+4-5-6-7+8+9+10+11-12-13+14-15;1-2+3+4-5-6-7+8+9+10-11+12+13-14-15;1-2+3+4-5-6-7-8-9+10-11-12+13+14+15;1-2+3+4-5-6-7-8-9-10+11+12-13+14+15;1-2+3-4 5 6-7 8 9-10+11+12 13+14+15;1-2+3-4 5 6-7-8-9 10-11-12-13+14 15;1-2+3-4 5+6 7+8-9-10+11-12-13-14+15;1-2+3-4 5+6 7+8-9-10-11+12-13+14-15;1-2+3-4 5+6 7-8+9+10-11-12-13-14+15;1-2+3-4 5+6 7-8+9-10+11-12-13+14-15;1-2+3-4 5+6 7-8+9-10-11+12+13-14-15;1-2+3-4 5+6 7-8-9+10+11-12+13-14-15;1-2+3-4 5+6+7+8-9-10+11-12+13+14+15;1-2+3-4 5+6+7-8+9+10-11-12+13+14+15;1-2+3-4 5+6+7-8+9-10+11+12-13+14+15;1-2+3-4 5+6+7-8-9+10+11+12+13-14+15;1-2+3-4 5+6-7+8 9-10-11-12-13-14+15;1-2+3-4 5+6-7+8+9+10-11+12-13+14+15;1-2+3-4 5+6-7+8+9-10+11+12+13-14+15;1-2+3-4 5+6-7+8-9+10+11+12+13+14-15;1-2+3-4 5-6 7+8 9+10+11+12-13-14+15;1-2+3-4 5-6 7+8 9+10+11-12+13+14-15;1-2+3-4 5-6+7+8 9-10-11-12-13+14-15;1-2+3-4 5-6+7+8+9+10+11-12-13+14+15;1-2+3-4 5-6+7+8+9+10-11+12+13-14+15;1-2+3-4 5-6+7+8+9-10+11+12+13+14-15;1-2+3-4 5-6-7+8 9+10+11-12-13-14-15;1-2+3-4 5-6-7-8+9-10+11+12+13+14+15;1-2+3-4+5 6-7 8-9-10-11+12+13+14+15;1-2+3-4+5+6 7-8 9+10+11+12-13+14-15;1-2+3-4+5+6 7-8-9-10+11-12-13-14-15;1-2+3-4+5+6+7+8+9+10+11-12-13-14-15;1-2+3-4+5+6+7-8+9-10-11-12-13+14+15;1-2+3-4+5+6+7-8-9+10-11-12+13-14+15;1-2+3-4+5+6+7-8-9-10+11+12-13-14+15;1-2+3-4+5+6+7-8-9-10+11-12+13+14-15;1-2+3-4+5+6-7+8+9-10-11-12+13-14+15;1-2+3-4+5+6-7+8-9+10-11+12-13-14+15;1-2+3-4+5+6-7+8-9+10-11-12+13+14-15;1-2+3-4+5+6-7+8-9-10+11+12-13+14-15;1-2+3-4+5+6-7-8+9+10+11-12-13-14+15;1-2+3-4+5+6-7-8+9+10-11+12-13+14-15;1-2+3-4+5+6-7-8+9-10+11+12+13-14-15;1-2+3-4+5-6 7+8 9+10-11-12-13-14+15;1-2+3-4+5-6 7+8 9-10+11-12-13+14-15;1-2+3-4+5-6 7+8 9-10-11+12+13-14-15;1-2+3-4+5-6 7+8+9+10+11+12+13-14+15;1-2+3-4+5-6+7+8+9-10-11+12-13-14+15;1-2+3-4+5-6+7+8+9-10-11-12+13+14-15;1-2+3-4+5-6+7+8-9+10+11-12-13-14+15;1-2+3-4+5-6+7+8-9+10-11+12-13+14-15;1-2+3-4+5-6+7+8-9-10+11+12+13-14-15;1-2+3-4+5-6+7-8+9+10+11-12-13+14-15;1-2+3-4+5-6+7-8+9+10-11+12+13-14-15;1-2+3-4+5-6-7+8+9+10+11-12+13-14-15;1-2+3-4+5-6-7-8+9-10-11-12+13+14+15;1-2+3-4+5-6-7-8-9+10-11+12-13+14+15;1-2+3-4+5-6-7-8-9-10+11+12+13-14+15;1-2+3-4-5 6+7 8+9+10-11-12+13-14-15;1-2+3-4-5 6+7 8+9-10+11+12-13-14-15;1-2+3-4-5+6 7-8 9+10-11-12+13+14+15;1-2+3-4-5+6 7-8 9-10+11+12-13+14+15;1-2+3-4-5+6+7+8+9-10+11-12-13-14+15;1-2+3-4-5+6+7+8+9-10-11+12-13+14-15;1-2+3-4-5+6+7+8-9+10+11-12-13+14-15;1-2+3-4-5+6+7+8-9+10-11+12+13-14-15;1-2+3-4-5+6+7-8+9+10+11-12+13-14-15;1-2+3-4-5+6-7+8+9+10+11+12-13-14-15;1-2+3-4-5+6-7+8-9-10-11-12+13+14+15;1-2+3-4-5+6-7-8+9-10-11+12-13+14+15;1-2+3-4-5+6-7-8-9+10+11-12-13+14+15;1-2+3-4-5+6-7-8-9+10-11+12+13-14+15;1-2+3-4-5+6-7-8-9-10+11+12+13+14-15;1-2+3-4-5-6 7+8-9+10+11+12+13+14+15;1-2+3-4-5-6+7+8-9-10-11+12-13+14+15;1-2+3-4-5-6+7-8+9-10+11-12-13+14+15;1-2+3-4-5-6+7-8+9-10-11+12+13-14+15;1-2+3-4-5-6+7-8-9+10+11-12+13-14+15;1-2+3-4-5-6+7-8-9+10-11+12+13+14-15;1-2+3-4-5-6-7+8+9+10-11-12-13+14+15;1-2+3-4-5-6-7+8+9-10+11-12+13-14+15;1-2+3-4-5-6-7+8+9-10-11+12+13+14-15;1-2+3-4-5-6-7+8-9+10+11+12-13-14+15;1-2+3-4-5-6-7+8-9+10+11-12+13+14-15;1-2+3-4-5-6-7-8+9+10+11+12-13+14-15;1-2-3 4 5+6 7+8-9-10-11 12-13+14 15;1-2-3 4 5+6-7 8-9 10+11-12+13 14+15;1-2-3 4 5+6-7+8+9-10 11+12+13 14+15;1-2-3 4 5-6 7 8+9+10 11-12-13+14+15;1-2-3 4+5 6+7+8+9-10-11-12-13-14+15;1-2-3 4+5 6+7+8-9+10-11-12-13+14-15;1-2-3 4+5 6+7+8-9-10+11-12+13-14-15;1-2-3 4+5 6+7-8+9+10-11-12+13-14-15;1-2-3 4+5 6+7-8+9-10+11+12-13-14-15;1-2-3 4+5 6-7+8+9+10-11+12-13-14-15;1-2-3 4+5 6-7-8 9+10+11+12+13+14+15;1-2-3 4+5 6-7-8-9+10-11-12-13+14+15;1-2-3 4+5 6-7-8-9-10+11-12+13-14+15;1-2-3 4+5 6-7-8-9-10-11+12+13+14-15;1-2-3 4+5+6+7 8-9-10-11-12-13-14+15;1-2-3 4+5-6+7 8+9-10-11+12-13-14-15;1-2-3 4+5-6+7 8-9+10+11-12-13-14-15;1-2-3 4-5 6 7+8+9 10+11 12-13-14 15;1-2-3 4-5 6+7+8 9+10+11-12-13+14-15;1-2-3 4-5 6+7+8 9+10-11+12+13-14-15;1-2-3 4-5 6+7-8 9-10 11+12 13-14-15;1-2-3 4-5 6-7+8 9-10-11-12+13+14+15;1-2-3 4-5+6+7 8+9-10+11-12-13-14-15;1-2-3 4-5-6+7 8-9-10+11-12-13-14+15;1-2-3 4-5-6+7 8-9-10-11+12-13+14-15;1-2-3+4 5 6+7 8 9-10+11-12 13-14-15;1-2-3+4 5+6 7 8-9 10-11-12 13+14 15;1-2-3+4 5+6 7+8 9-10+11 12-13 14+15;1-2-3+4 5+6 7-8 9-10-11-12+13-14+15;1-2-3+4 5+6+7+8-9-10+11-12-13-14-15;1-2-3+4 5+6+7-8+9+10-11-12-13-14-15;1-2-3+4 5+6-7-8 9+10+11+12-13+14+15;1-2-3+4 5+6-7-8-9-10+11-12-13-14+15;1-2-3+4 5+6-7-8-9-10-11+12-13+14-15;1-2-3+4 5-6 7+8+9-10-11-12+13+14+15;1-2-3+4 5-6 7+8-9+10-11+12-13+14+15;1-2-3+4 5-6 7+8-9-10+11+12+13-14+15;1-2-3+4 5-6 7-8+9+10+11-12-13+14+15;1-2-3+4 5-6 7-8+9+10-11+12+13-14+15;1-2-3+4 5-6 7-8+9-10+11+12+13+14-15;1-2-3+4 5-6+7-8 9+10+11+12+13-14+15;1-2-3+4 5-6+7-8-9+10-11-12-13-14+15;1-2-3+4 5-6+7-8-9-10+11-12-13+14-15;1-2-3+4 5-6+7-8-9-10-11+12+13-14-15;1-2-3+4 5-6-7+8+9-10-11-12-13-14+15;1-2-3+4 5-6-7+8-9+10-11-12-13+14-15;1-2-3+4 5-6-7+8-9-10+11-12+13-14-15;1-2-3+4 5-6-7-8+9+10-11-12+13-14-15;1-2-3+4 5-6-7-8+9-10+11+12-13-14-15;1-2-3+4+5 6-7 8-9-10+11-12+13+14+15;1-2-3+4+5+6 7-8 9+10+11+12+13-14-15;1-2-3+4+5+6 7-8-9+10-11-12-13-14-15;1-2-3+4+5+6+7+8-9-10-11-12-13+14+15;1-2-3+4+5+6+7-8+9-10-11-12+13-14+15;1-2-3+4+5+6+7-8-9+10-11+12-13-14+15;1-2-3+4+5+6+7-8-9+10-11-12+13+14-15;1-2-3+4+5+6+7-8-9-10+11+12-13+14-15;1-2-3+4+5+6-7+8+9-10-11+12-13-14+15;1-2-3+4+5+6-7+8+9-10-11-12+13+14-15;1-2-3+4+5+6-7+8-9+10+11-12-13-14+15;1-2-3+4+5+6-7+8-9+10-11+12-13+14-15;1-2-3+4+5+6-7+8-9-10+11+12+13-14-15;1-2-3+4+5+6-7-8+9+10+11-12-13+14-15;1-2-3+4+5+6-7-8+9+10-11+12+13-14-15;1-2-3+4+5-6 7+8 9+10-11-12-13+14-15;1-2-3+4+5-6 7+8 9-10+11-12+13-14-15;1-2-3+4+5-6 7+8+9+10+11+12+13+14-15;1-2-3+4+5-6+7+8+9-10+11-12-13-14+15;1-2-3+4+5-6+7+8+9-10-11+12-13+14-15;1-2-3+4+5-6+7+8-9+10+11-12-13+14-15;1-2-3+4+5-6+7+8-9+10-11+12+13-14-15;1-2-3+4+5-6+7-8+9+10+11-12+13-14-15;1-2-3+4+5-6-7+8+9+10+11+12-13-14-15;1-2-3+4+5-6-7+8-9-10-11-12+13+14+15;1-2-3+4+5-6-7-8+9-10-11+12-13+14+15;1-2-3+4+5-6-7-8-9+10+11-12-13+14+15;1-2-3+4+5-6-7-8-9+10-11+12+13-14+15;1-2-3+4+5-6-7-8-9-10+11+12+13+14-15;1-2-3+4-5 6+7 8+9+10-11+12-13-14-15;1-2-3+4-5+6 7-8 9+10-11+12-13+14+15;1-2-3+4-5+6 7-8 9-10+11+12+13-14+15;1-2-3+4-5+6 7-8-9-10-11-12-13-14+15;1-2-3+4-5+6+7+8+9+10-11-12-13-14+15;1-2-3+4-5+6+7+8+9-10+11-12-13+14-15;1-2-3+4-5+6+7+8+9-10-11+12+13-14-15;1-2-3+4-5+6+7+8-9+10+11-12+13-14-15;1-2-3+4-5+6+7-8+9+10+11+12-13-14-15;1-2-3+4-5+6+7-8-9-10-11-12+13+14+15;1-2-3+4-5+6-7+8-9-10-11+12-13+14+15;1-2-3+4-5+6-7-8+9-10+11-12-13+14+15;1-2-3+4-5+6-7-8+9-10-11+12+13-14+15;1-2-3+4-5+6-7-8-9+10+11-12+13-14+15;1-2-3+4-5+6-7-8-9+10-11+12+13+14-15;1-2-3+4-5-6 7+8 9-10-11-12-13+14+15;1-2-3+4-5-6 7+8+9-10+11+12+13+14+15;1-2-3+4-5-6+7+8-9-10+11-12-13+14+15;1-2-3+4-5-6+7+8-9-10-11+12+13-14+15;1-2-3+4-5-6+7-8+9+10-11-12-13+14+15;1-2-3+4-5-6+7-8+9-10+11-12+13-14+15;1-2-3+4-5-6+7-8+9-10-11+12+13+14-15;1-2-3+4-5-6+7-8-9+10+11+12-13-14+15;1-2-3+4-5-6+7-8-9+10+11-12+13+14-15;1-2-3+4-5-6-7+8+9+10-11-12+13-14+15;1-2-3+4-5-6-7+8+9-10+11+12-13-14+15;1-2-3+4-5-6-7+8+9-10+11-12+13+14-15;1-2-3+4-5-6-7+8-9+10+11+12-13+14-15;1-2-3+4-5-6-7-8+9+10+11+12+13-14-15;1-2-3-4 5+6 7+8-9-10-11-12-13+14+15;1-2-3-4 5+6 7-8+9-10-11-12+13-14+15;1-2-3-4 5+6 7-8-9+10-11+12-13-14+15;1-2-3-4 5+6 7-8-9+10-11-12+13+14-15;1-2-3-4 5+6 7-8-9-10+11+12-13+14-15;1-2-3-4 5+6+7+8 9-10+11-12-13-14-15;1-2-3-4 5+6+7+8+9+10+11+12-13+14-15;1-2-3-4 5+6+7-8-9+10-11+12+13+14+15;1-2-3-4 5+6-7+8+9-10-11+12+13+14+15;1-2-3-4 5+6-7+8-9+10+11-12+13+14+15;1-2-3-4 5+6-7-8+9+10+11+12-13+14+15;1-2-3-4 5-6 7+8 9+10-11+12-13+14+15;1-2-3-4 5-6 7+8 9-10+11+12+13-14+15;1-2-3-4 5-6+7+8+9-10+11-12+13+14+15;1-2-3-4 5-6+7+8-9+10+11+12-13+14+15;1-2-3-4 5-6+7-8+9+10+11+12+13-14+15;1-2-3-4 5-6-7+8 9+10-11-12-13+14-15;1-2-3-4 5-6-7+8 9-10+11-12+13-14-15;1-2-3-4 5-6-7+8+9+10+11+12+13+14-15;1-2-3-4+5 6-7 8+9+10+11+12-13-14+15;1-2-3-4+5 6-7 8+9+10+11-12+13+14-15;1-2-3-4+5+6 7-8 9+10+11-12-13+14+15;1-2-3-4+5+6 7-8 9+10-11+12+13-14+15;1-2-3-4+5+6 7-8 9-10+11+12+13+14-15;1-2-3-4+5+6 7-8-9-10-11-12-13+14-15;1-2-3-4+5+6+7+8+9+10-11-12-13+14-15;1-2-3-4+5+6+7+8+9-10+11-12+13-14-15;1-2-3-4+5+6+7+8-9+10+11+12-13-14-15;1-2-3-4+5+6+7-8-9-10-11+12-13+14+15;1-2-3-4+5+6-7+8-9-10+11-12-13+14+15;1-2-3-4+5+6-7+8-9-10-11+12+13-14+15;1-2-3-4+5+6-7-8+9+10-11-12-13+14+15;1-2-3-4+5+6-7-8+9-10+11-12+13-14+15;1-2-3-4+5+6-7-8+9-10-11+12+13+14-15;1-2-3-4+5+6-7-8-9+10+11+12-13-14+15;1-2-3-4+5+6-7-8-9+10+11-12+13+14-15;1-2-3-4+5-6 7+8 9-10-11-12+13-14+15;1-2-3-4+5-6 7+8+9+10-11+12+13+14+15;1-2-3-4+5-6+7+8-9+10-11-12-13+14+15;1-2-3-4+5-6+7+8-9-10+11-12+13-14+15;1-2-3-4+5-6+7+8-9-10-11+12+13+14-15;1-2-3-4+5-6+7-8+9+10-11-12+13-14+15;1-2-3-4+5-6+7-8+9-10+11+12-13-14+15;1-2-3-4+5-6+7-8+9-10+11-12+13+14-15;1-2-3-4+5-6+7-8-9+10+11+12-13+14-15;1-2-3-4+5-6-7+8+9+10-11+12-13-14+15;1-2-3-4+5-6-7+8+9+10-11-12+13+14-15;1-2-3-4+5-6-7+8+9-10+11+12-13+14-15;1-2-3-4+5-6-7+8-9+10+11+12+13-14-15;1-2-3-4+5-6-7-8-9-10-11+12+13+14+15;1-2-3-4-5 6+7 8+9-10+11-12-13-14+15;1-2-3-4-5 6+7 8+9-10-11+12-13+14-15;1-2-3-4-5 6+7 8-9+10+11-12-13+14-15;1-2-3-4-5 6+7 8-9+10-11+12+13-14-15;1-2-3-4-5+6 7+8-9-10+11-12-13-14-15;1-2-3-4-5+6 7-8+9+10-11-12-13-14-15;1-2-3-4-5+6+7+8+9-10-11-12-13+14+15;1-2-3-4-5+6+7+8-9+10-11-12+13-14+15;1-2-3-4-5+6+7+8-9-10+11+12-13-14+15;1-2-3-4-5+6+7+8-9-10+11-12+13+14-15;1-2-3-4-5+6+7-8+9+10-11+12-13-14+15;1-2-3-4-5+6+7-8+9+10-11-12+13+14-15;1-2-3-4-5+6+7-8+9-10+11+12-13+14-15;1-2-3-4-5+6+7-8-9+10+11+12+13-14-15;1-2-3-4-5+6-7+8 9-10-11-12-13-14-15;1-2-3-4-5+6-7+8+9+10+11-12-13-14+15;1-2-3-4-5+6-7+8+9+10-11+12-13+14-15;1-2-3-4-5+6-7+8+9-10+11+12+13-14-15;1-2-3-4-5+6-7-8-9-10+11-12+13+14+15;1-2-3-4-5-6 7+8 9+10+11+12-13-14-15;1-2-3-4-5-6+7+8+9+10+11-12-13+14-15;1-2-3-4-5-6+7+8+9+10-11+12+13-14-15;1-2-3-4-5-6+7-8-9+10-11-12+13+14+15;1-2-3-4-5-6+7-8-9-10+11+12-13+14+15;1-2-3-4-5-6-7+8+9-10-11-12+13+14+15;1-2-3-4-5-6-7+8-9+10-11+12-13+14+15;1-2-3-4-5-6-7+8-9-10+11+12+13-14+15;1-2-3-4-5-6-7-8+9+10+11-12-13+14+15;1-2-3-4-5-6-7-8+9+10-11+12+13-14+15;1-2-3-4-5-6-7-8+9-10+11+12+13+14-15",
                1);
        System.out.println("All time:" + totalTime);
    }

    public static String run4_1(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        StringBuilder result = null;
        int [] numbers = null;
        int MAX = 0;
        char ch = arg.charAt(0);
        if (ch == 'S') {
            //write a table contains 3~15???
            int n = 0;
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            //3<=N<=15
            if (n < 3 || n > 15) {
                return "ILLEGAL";
            }
            numbers = new int[n];
            for (int i = 0; i < numbers.length; ++i) {
                numbers[i] = i + 1;
            }
            //various buffer size
            result = new StringBuilder(2 << n);
            MAX = n - 1;
        } else if (ch == 'R') {
            String asdStr = arg.substring(1);
            String [] numberStr = asdStr.split(",");
            numbers = new int[numberStr.length];
            int pre = Integer.MIN_VALUE;
            try {
                for (int i = 0; i < numbers.length; ++i) {
                    if (numberStr[i].isEmpty() || numberStr[i].charAt(0) > '9') {
                        //not a number
                        return "ILLEGAL";
                    }
                    numbers[i] = Integer.parseInt(numberStr[i]);
                    if (numbers[i] < 0 || numbers[i] < pre) {
                        //not larger than previous
                        return "ILLEGAL";
                    }
                    pre = numbers[i];
                }
            } catch (Exception e) {
                return "ILLEGAL";
            }
            result = new StringBuilder(128);
            MAX = (numberStr.length * numberStr[numberStr.length - 1].length()) / 2 + 1;
            if (MAX > 15) {
                return "ILLEGAL";
            }
        } else {
            return "ILLEGAL";
        }

        //cache large number
        //row: startNum
        //column:endNum
        //save the index of number
        long [][] combinedNumMap = new long[numbers.length][numbers.length];
        for (int i = 0; i < numbers.length; ++i) {
            combinedNumMap[i][i] = numbers[i];
            for (int j = i + 1; j < numbers.length; ++j) {
                if ((j - i + 1) <= MAX) {
                    combinedNumMap[i][j] = numbers[j];
                    long cur = combinedNumMap[i][j];
                    int c = 1;
                    do {
                        cur /= 10;
                        c *= 10;
                    } while (cur > 0);
                    combinedNumMap[i][j] = combinedNumMap[i][j - 1] * c + combinedNumMap[i][j];
                } else {
                    combinedNumMap[i][j] = 0;
                }
            }
        }
        final int OP_NUM = 3;
        //            final char OP_SPAECE=1;
        //            final char OP_PLUS=2;
        //            final char OP_MINUS=4;
        //a tree to find duplicate....
        class Node {
            public Node [] nodes;
            //000
            //' ':001,1
            //+:010,2
            //-:100,4
            public char    op = 0;

        }
        Node parentNode = new Node();

        //////////////////////////////////
        //row: the idx is the size of set, the size of f(3,5,6,9) is 4, map[4][][]
        //column:start idx
        //element:value, order by ' ',+,-
        //XXX if the value is not illegal, make sure it won't be used
        final char [] opStr = {' ', '+', '-'};
        long [][][] map = new long[numbers.length + 1][numbers.length][];

        boolean isFirst = true;
        int [] answerNumMap = new int[map.length];
        for (int size = 1, answerNum = 1; size < map.length; ++size, answerNum *= OP_NUM) {
            answerNumMap[size] = answerNum;
        }
        /////////////////////////////////////////////////////////////////
        for (int startIdx = 0; startIdx < numbers.length; ++startIdx) {
            map[1][startIdx] = new long[1];
            map[1][startIdx][0] = numbers[startIdx];
        }
        //store op seq for each answer map
        //row:answer index
        //column: 
        //element:op 
        int [][][] opMap = new int[map.length][][];
        opMap[1] = new int[1][0];

        for (int size = 2; size < map.length; ++size) {
            opMap[size] = new int[answerNumMap[size]][size - 1];
            for (int aIdx = 0; aIdx < opMap[size].length; ++aIdx) {
                int t = aIdx;
                int j = opMap[size][aIdx].length - 1;
                while (j >= 0) {
                    opMap[size][aIdx][j] = (t % OP_NUM);
                    t /= OP_NUM;
                    --j;
                    if (t == 0) {
                        for (; j >= 0; --j) {
                            opMap[size][aIdx][j] = (0);
                        }
                        break;
                    }

                }
            }
            //
            //
            //
            //                System.out.println("size: " + size);
            //endIdx=secondStartIdx-1
            //secondEndIdx=startIdx+size-1
            int k = 0;
            boolean isBeyondMax = false;
            for (int startIdx = 0; startIdx + size - 1 < numbers.length; ++startIdx) {
                if (map[size][startIdx] == null) {
                    map[size][startIdx] = new long[answerNumMap[size]];

                    for (int secondStartIdx = startIdx + 1; secondStartIdx < startIdx + size; ++secondStartIdx) {
                        System.out.print("( ");
                        for (int i = startIdx; i < secondStartIdx; ++i) {
                            System.out.print(numbers[i] + " ");
                        }
                        System.out.print(") , ( ");
                        for (int i = secondStartIdx; i < startIdx + size; ++i) {
                            System.out.print(numbers[i] + " ");
                        }
                        System.out.println(")");
                        ////////////////////////////////////////////////
                        //???? if subset size is too large, don't calc it
                        int size1 = secondStartIdx - startIdx;
                        int size2 = size - size1;
                        if (size1 > MAX || size2 > MAX) {
                            isBeyondMax = true;
                            break;
                        }
                        long [] v1 = map[size1][startIdx];
                        long [] v2 = map[size2][secondStartIdx];
                        long [] curResult = map[size][startIdx];
                        //                            System.out.println(Arrays.toString(v1));
                        //                            System.out.println(Arrays.toString(v2));
                        int idx = 0;
                        for (int i = 0; i < v1.length; ++i) {
                            int space1 = 0;
                            int [] op1 = opMap[size1][i];

                            int k1 = op1.length - 1;
                            while (k1 >= 0) {
                                if (op1[k1] == 0) {
                                    ++space1;
                                    --k1;
                                } else {
                                    break;
                                }
                            }
                            for (int opMiddle = 0; opMiddle < OP_NUM; ++opMiddle) {
                                for (int j = 0; j < v2.length; ++j) {
                                    int space2 = 0;
                                    //
                                    int [] op2 = opMap[size2][j];
                                    k1 = 0;
                                    while (k1 < op2.length) {
                                        if (op2[k1] == 0) {
                                            ++space2;
                                            ++k1;
                                        } else {
                                            break;
                                        }
                                    }
                                    isBeyondMax = false;
                                    switch (opMiddle) {
                                        case 0:
                                            if (size > MAX) {
                                                //                                                    System.out.println("size>MAX");

                                                isBeyondMax = true;
                                                break;
                                            }
                                            //                                                System.out.println("space");
                                            curResult[idx] = v1[i] + v2[j];
                                            //second prefix
                                            curResult[idx] -= combinedNumMap[secondStartIdx][secondStartIdx
                                                    + space2];
                                            //                                                System.out.println("MAX:"+ MAX+", width:" + (secondStartIdx
                                            //                                                        + space2 - (secondStartIdx - 1 - space1) + 1));
                                            //!!!! substract
                                            if (op1.length - 1 - space1 >= 0) {//there is other op before space in first part
                                                if (op1[op1.length - space1 - 1] == 1) {//+
                                                    //first postfix
                                                    curResult[idx] -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                    curResult[idx] += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                            + space2];
                                                } else if (op1[op1.length - space1 - 1] == 2) {//-
                                                    curResult[idx] += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                    curResult[idx] -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                            + space2];
                                                } else {
                                                    System.out.println("error");
                                                }

                                            } else {

                                                curResult[idx] -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                //may be larger than MAX width
                                                curResult[idx] += combinedNumMap[startIdx][secondStartIdx + space2];
                                            }

                                            //                                                System.out.println(combinedNumMap[startIdx][secondStartIdx + space2]);
                                            break;
                                        case 1:
                                            curResult[idx] = v1[i] + v2[j];
                                            //                                              if (size == 7&&v1[i]==-67) {
                                            //                                              System.out.println("space1:" + space1 + ", space2:" + space2
                                            //                                                      + ", startIdx:"
                                            //                                                      + startIdx + ", secondIdx:" + secondStartIdx);
                                            //                                              System.out.println(v1[i] + " " + v2[j] + "=" + curResult[idx]);
                                            //                                          }
                                            break;
                                        case 2: {
                                            //!!!!!substract, braceket...
                                            //only first number need to change to substract
                                            int preSpace = 0;
                                            for (k = 0; k < op2.length; ++k) {
                                                if (op2[k] != 0) {
                                                    break;
                                                } else {
                                                    ++preSpace;
                                                }
                                            }
                                            curResult[idx] = v1[i]
                                                    - 2 * combinedNumMap[secondStartIdx][secondStartIdx + preSpace]
                                                    + v2[j];
                                        }
                                            break;
                                    }

                                    if (!isBeyondMax) {
                                        System.out.println("Found: " + v1[i]
                                                + (Character.toString(opStr[opMiddle]))
                                                + v2[j]
                                                + "="
                                                + curResult[idx]);
                                        if (curResult[idx] == 0 && size == numbers.length) {
                                            //                                                boolean isNeedSwap = false;

                                            int numIdx = 0;
                                            /////////////////////////////////////////////////////////
                                            //                                                System.out.println("Found: " + v1[i]
                                            //                                                        + (Character.toString(opStr[opMiddle]))
                                            //                                                        + v2[j]
                                            //                                                        + "="
                                            //                                                        + curResult[idx]);
                                            /////////////////////////////////////////////////////////
                                            /////////////////////////////////////////////////////////
                                            boolean isDuplicate = true;

                                            //!! duplicate answer
                                            Node node = parentNode;
                                            char opForTree = 0;
                                            for (; numIdx < op1.length; ++numIdx) {
                                                switch (op1[numIdx]) {
                                                    case 0:
                                                        opForTree = 1;
                                                        break;
                                                    case 1:
                                                        opForTree = 0x2;
                                                        break;
                                                    case 2:
                                                        opForTree = 0x4;
                                                        break;
                                                }
                                                if ((node.op & opForTree) == 0) {
                                                    //add this node
                                                    node.op |= opForTree;
                                                    if (node.nodes == null) {
                                                        node.nodes = new Node[OP_NUM];
                                                    }
                                                    node.nodes[op1[numIdx]] = new Node();
                                                    isDuplicate = false;
                                                    //                                                        System.out.println("1) create a node " + opStr[op1[numIdx]]);
                                                }
                                                node = node.nodes[op1[numIdx]];
                                                //                                                    System.out.println("1) go to next node");
                                            }
                                            switch (opMiddle) {
                                                case 0:
                                                    opForTree = 1;
                                                    break;
                                                case 1:
                                                    opForTree = 0x2;
                                                    break;
                                                case 2:
                                                    opForTree = 0x4;
                                                    break;
                                            }
                                            if ((node.op & opForTree) == 0) {
                                                //add this node
                                                node.op |= opForTree;
                                                if (node.nodes == null) {
                                                    node.nodes = new Node[OP_NUM];
                                                }
                                                node.nodes[opMiddle] = new Node();
                                                isDuplicate = false;
                                                //                                                    System.out.println("2) create a node " + opStr[opMiddle]);
                                            }
                                            node = node.nodes[opMiddle];
                                            //                                                System.out.println("2) go to next node");

                                            k = 0;
                                            for (; k < op2.length; ++numIdx, ++k) {
                                                switch (op2[k]) {
                                                    case 0:
                                                        opForTree = 1;
                                                        break;
                                                    case 1:
                                                        opForTree = 0x2;
                                                        break;
                                                    case 2:
                                                        opForTree = 0x4;
                                                        break;
                                                }
                                                if ((node.op & opForTree) == 0) {
                                                    //add this node
                                                    node.op |= opForTree;
                                                    if (node.nodes == null) {
                                                        node.nodes = new Node[OP_NUM];
                                                    }
                                                    node.nodes[op2[k]] = new Node();
                                                    isDuplicate = false;
                                                    //                                                        System.out.println("3) create a node " + opStr[op2[k]]);
                                                }
                                                node = node.nodes[op2[k]];
                                                //                                                    System.out.println("3) go to next node");
                                            }

                                            ////////////////////////////////////////////////////////

                                            if (!isDuplicate) {
                                                //                                                System.out.println("Found: " + v1[i]
                                                //                                                        + (Character.toString(opStr[opMiddle]))
                                                //                                                        + v2[j]
                                                //                                                        + "="
                                                //                                                        + curResult[idx]);
                                                if (isFirst) {
                                                    isFirst = false;
                                                } else {
                                                    result.append(';');
                                                }
                                                numIdx = 0;
                                                //                                                System.out.println("op1:" + op1.length + ",op2:" + op2.length);
                                                for (; numIdx < op1.length; ++numIdx) {
                                                    result.append(numbers[numIdx]);
                                                    //construct string
                                                    switch (op1[numIdx]) {
                                                        case 1:
                                                            result.append('+');
                                                            break;
                                                        case 2:
                                                            result.append('-');
                                                            break;
                                                        case 0:
                                                            result.append(' ');
                                                            break;
                                                    }
                                                }
                                                result.append(numbers[numIdx]);
                                                result.append(opStr[opMiddle]);
                                                ++numIdx;
                                                for (k = 0; k < op2.length; ++k, ++numIdx) {
                                                    result.append(numbers[numIdx]);
                                                    //construct string
                                                    switch (op2[k]) {
                                                        case 1:
                                                            result.append('+');
                                                            break;
                                                        case 2:
                                                            result.append('-');
                                                            break;
                                                        case 0:
                                                            result.append(' ');
                                                            break;
                                                    }
                                                }
                                                result.append(numbers[numIdx]);

                                            } else {
                                                //                                                    System.out.println("find duplicate");
                                            }
                                        }
                                    }

                                    ++idx;
                                }

                            }
                        }

                        System.out.println(Arrays.toString(curResult));
                    }
                }
            }
        }
        if (result.length() == 0) {
            return "NOTFOUND";
        }
        return result.toString();
    }

    //' '：0
    //+：1
    //-： 2
    public static String run5(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        StringBuilder result = null;
        int [] numbers = null;
        int MAX = 0;
        char ch = arg.charAt(0);
        if (ch == 'S') {
            //write a table contains 3~15???
            int n = 0;
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            //3<=N<=15
            if (n < 3 || n > 15) {
                return "ILLEGAL";
            }
            numbers = new int[n];
            for (int i = 0; i < numbers.length; ++i) {
                numbers[i] = i + 1;
            }
            //various buffer size
            result = new StringBuilder(2 << n);
            //            MAX = n / 2 + 1;
        } else if (ch == 'R') {
            String asdStr = arg.substring(1);
            String [] numberStr = asdStr.split(",");
            numbers = new int[numberStr.length];
            int pre = Integer.MIN_VALUE;
            try {
                for (int i = 0; i < numbers.length; ++i) {
                    if (numberStr[i].isEmpty() || numberStr[i].charAt(0) > '9') {
                        //not a number
                        return "ILLEGAL";
                    }
                    numbers[i] = Integer.parseInt(numberStr[i]);
                    if (numbers[i] < 0 || numbers[i] < pre) {
                        //not larger than previous
                        return "ILLEGAL";
                    }
                    pre = numbers[i];
                }
            } catch (Exception e) {
                return "ILLEGAL";
            }
            result = new StringBuilder(128);
            //            MAX = numberStr.length / 2 + 1;
            //            MAX = (numberStr.length * numberStr[numberStr.length - 1].length()) / 2 + 1;
            //            if (MAX < numbers.length - 1) {
            //                MAX = numbers.length - 1;
            //            }

        } else {
            return "ILLEGAL";
        }
        //!!!
        MAX = numbers.length - 2;
        //        MAX = numbers.length / 2 + 1;
        if (MAX > 15) {
            return "ILLEGAL";
        }
        //cache large number
        //row: startNum
        //column:endNum
        //save the index of number
        long [][] combinedNumMap = new long[numbers.length][numbers.length];
        for (int i = 0; i < numbers.length; ++i) {
            combinedNumMap[i][i] = numbers[i];
            for (int j = i + 1; j < numbers.length; ++j) {
                if ((j - i + 1) <= MAX) {
                    combinedNumMap[i][j] = numbers[j];
                    long cur = combinedNumMap[i][j];
                    int c = 1;
                    do {
                        cur /= 10;
                        c *= 10;
                    } while (cur > 0);
                    combinedNumMap[i][j] = combinedNumMap[i][j - 1] * c + combinedNumMap[i][j];
                } else {
                    combinedNumMap[i][j] = 0;
                }
            }
        }
        //TODO use brute force
        //        if(numbers.length<5){
        if (numbers.length < 1) {
            boolean isFirst = true;
            //brute force
            //for current op idx
            char [] opFlag = new char[numbers.length - 1];
            //op sequence cache
            char [] cache = new char[numbers.length - 1];
            //first op idx
            int opSeqOrder = 0;
            //set to first number
            while (true) {
                boolean hasNewOp = false;
                if (opFlag[opSeqOrder] < 3) {
                    cache[opSeqOrder] = opFlag[opSeqOrder];
                    ++opFlag[opSeqOrder];
                    hasNewOp = true;
                }
                if (hasNewOp) {
                    //up to end
                    if (opSeqOrder == cache.length - 1) {
                        long calc = numbers[0];
                        boolean isSuccess = true;
                        //next number: opIdx + 1
                        for (int opIdx = 0; opIdx < cache.length; ++opIdx) {
                            switch (cache[opIdx]) {
                                case 1:
                                    calc += numbers[opIdx + 1];
                                    break;
                                case 2:
                                    calc -= numbers[opIdx + 1];
                                    break;
                                case 0: {
                                    //if found space op, check if continuous                                    
                                    int nextOpIdx = opIdx + 1;
                                    int c = 1;
                                    //count space op
                                    while (nextOpIdx < cache.length) {
                                        if (0 == cache[nextOpIdx]) {
                                            ++c;
                                            ++nextOpIdx;
                                        } else {
                                            break;
                                        }
                                    }
                                    if (c + 1 > MAX) {
                                        isSuccess = false;
                                        //the space is too long
                                        break;
                                    }
                                    //special,use table
                                    long cimbinedNum = combinedNumMap[opIdx][opIdx + c];
                                    if (cimbinedNum > 0) {
                                        if (opIdx == 0) {
                                            calc = cimbinedNum;
                                        } else {
                                            switch (cache[opIdx - 1]) {
                                                case 1:
                                                    calc -= numbers[opIdx];
                                                    calc += cimbinedNum;
                                                    break;
                                                case 2:
                                                    calc += numbers[opIdx];
                                                    calc -= cimbinedNum;
                                                    break;
                                            }
                                        }
                                    }
                                    opIdx = nextOpIdx - 1;
                                    break;
                                }
                            }
                            if (!isSuccess) {
                                calc = -1;
                                break;
                            }
                        }
                        if (calc == 0) {
                            if (isFirst) {
                                isFirst = false;
                            } else {
                                result.append(';');
                            }
                            result.append(numbers[0]);
                            for (int i = 0; i < cache.length; ++i) {
                                //construct string
                                switch (cache[i]) {
                                    case 1:
                                        result.append('+');
                                        break;
                                    case 2:
                                        result.append('-');
                                        break;
                                    case 0:
                                        result.append(' ');
                                        break;
                                }
                                result.append(numbers[i + 1]);
                            }

                        }
                    } else {
                        ++opSeqOrder;
                    }
                } else {
                    if (opSeqOrder - 1 < 0) {
                        break;
                    } else {
                        //clear
                        opFlag[opSeqOrder] = 0;
                        --opSeqOrder;
                    }

                }
            }
        } else {
            final int OP_NUM = 3;
            //////////////////////////////////
            //row: the idx is the size of set, the size of f(3,5,6,9) is 4, map[4][][]
            //column:start idx
            //element:value, order by ' ',+,-
            //if the value is not illegal, make sure it won't be used
            final char [] opStr = {' ', '+', '-'};
            //XXX
            //TODO only save the latest 
            long [][][] map = new long[numbers.length + 1][numbers.length][];

            boolean isFirst = true;
            int [] answerNumMap = new int[map.length];
            for (int size = 1, answerNum = 1; size < map.length; ++size, answerNum *= OP_NUM) {
                answerNumMap[size] = answerNum;
            }
            /////////////////////////////////////////////////////////////////
            for (int startIdx = 0; startIdx < numbers.length; ++startIdx) {
                map[1][startIdx] = new long[1];
                map[1][startIdx][0] = numbers[startIdx];
            }
            //store op seq for each answer map
            //row:answer index
            //column: 
            //element:op 
            //XXX out of memory...
            int [][][] opMap = new int[map.length][][];
            opMap[1] = new int[1][0];

            for (int size = 2; size < map.length; ++size) {
                opMap[size] = new int[answerNumMap[size]][size - 1];
                for (int aIdx = 0; aIdx < opMap[size].length; ++aIdx) {
                    int t = aIdx;
                    int j = opMap[size][aIdx].length - 1;
                    while (j >= 0) {
                        opMap[size][aIdx][j] = (t % OP_NUM);
                        t /= OP_NUM;
                        --j;
                        if (t == 0) {
                            for (; j >= 0; --j) {
                                opMap[size][aIdx][j] = (0);
                            }
                            break;
                        }

                    }
                }
                /////////////////////////////////////////////////
                //
                //
                //                System.out.println("size: " + size);
                //endIdx=secondStartIdx-1
                //secondEndIdx=startIdx+size-1
                int k = 0;
                boolean isBeyondMax = false;
                for (int startIdx = 0; startIdx + size - 1 < numbers.length; ++startIdx) {
                    if (map[size][startIdx] == null) {
                        map[size][startIdx] = new long[answerNumMap[size]];
                        int secondStartIdx = startIdx + 1;
                        if (secondStartIdx < startIdx + size) {
                            //                            System.out.print("( ");
                            //                            for (int i = startIdx; i < secondStartIdx; ++i) {
                            //                                System.out.print(numbers[i] + " ");
                            //                            }
                            //                            System.out.print(") , ( ");
                            //                            for (int i = secondStartIdx; i < startIdx + size; ++i) {
                            //                                System.out.print(numbers[i] + " ");
                            //                            }
                            //                            System.out.println(")");
                            ////////////////////////////////////////////////
                            //???? if subset size is too large, don't calc it
                            int size2 = size - 1;
                            long [] v1 = map[1][startIdx];
                            long [] v2 = map[size2][secondStartIdx];
                            long [] curResult = map[size][startIdx];
                            //                            System.out.println(Arrays.toString(v1));
                            //                            System.out.println(Arrays.toString(v2));
                            int idx = 0;
                            //first part size is 1
                            //                            int [] op1 = opMap[1][0];
                            for (int opMiddle = 0; opMiddle < OP_NUM; ++opMiddle) {
                                for (int j = 0; j < v2.length; ++j) {
                                    int [] op2 = opMap[size2][j];
                                    isBeyondMax = false;
                                    switch (opMiddle) {
                                        case 0: {
                                            int space2 = 0;
                                            int k1 = 0;
                                            while (k1 < op2.length) {
                                                if (op2[k1] == 0) {
                                                    ++space2;
                                                    ++k1;
                                                } else {
                                                    break;
                                                }
                                            }
                                            if (space2 >= MAX) {
                                                continue;
                                            }

                                            //                                                System.out.println("space");
                                            curResult[idx] = v1[0] + v2[j];
                                            //second prefix
                                            curResult[idx] -= combinedNumMap[secondStartIdx][secondStartIdx
                                                    + space2];
                                            //                                                System.out.println("MAX:"+ MAX+", width:" + (secondStartIdx
                                            //                                                        + space2 - (secondStartIdx - 1 - space1) + 1));
                                            //!!!! substract

                                            curResult[idx] -= combinedNumMap[secondStartIdx - 1][secondStartIdx - 1];
                                            //may be larger than MAX width
                                            curResult[idx] += combinedNumMap[startIdx][secondStartIdx + space2];

                                            //                                                System.out.println(combinedNumMap[startIdx][secondStartIdx + space2]);
                                        }
                                            break;
                                        case 1:
                                            curResult[idx] = v1[0] + v2[j];
                                            break;
                                        case 2: {
                                            //!!!!!substract, braceket...
                                            //only first number need to change to substract
                                            int preSpace = 0;
                                            for (k = 0; k < op2.length; ++k) {
                                                if (op2[k] != 0) {
                                                    break;
                                                } else {
                                                    ++preSpace;
                                                }
                                            }
                                            curResult[idx] = v1[0]
                                                    - 2 * combinedNumMap[secondStartIdx][secondStartIdx + preSpace]
                                                    + v2[j];
                                        }
                                            break;
                                    }

                                    if (!isBeyondMax) {
                                        //                                        System.out.println("Found: " + v1[0]
                                        //                                                + (Character.toString(opStr[opMiddle]))
                                        //                                                + v2[j]
                                        //                                                + "="
                                        //                                                + curResult[idx]);
                                        //                                        System.out.print(Arrays.toString(op1) + " ");
                                        //                                        System.out.print((opMiddle) + " ");
                                        //                                        System.out.println(Arrays.toString(op2));
                                        if (curResult[idx] == 0 && size == numbers.length) {
                                            //                                            System.out.println("Found: " + v1[0]
                                            //                                                    + (Character.toString(opStr[opMiddle]))
                                            //                                                    + v2[j]
                                            //                                                    + "="
                                            //                                                    + curResult[idx]);
                                            //                                            System.out.print(Arrays.toString(op1) + " ");
                                            //                                            System.out.print((opMiddle) + " ");
                                            //                                            System.out.println(Arrays.toString(op2));
                                            //                                                boolean isNeedSwap = false;

                                            int numIdx = 0;

                                            //                                                System.out.println("Found: " + v1[i]
                                            //                                                        + (Character.toString(opStr[opMiddle]))
                                            //                                                        + v2[j]
                                            //                                                        + "="
                                            //     + curResult[idx]);
                                            if (isFirst) {
                                                isFirst = false;
                                            } else {
                                                result.append(';');
                                            }
                                            numIdx = 0;
                                            //                                                System.out.println("op1:" + op1.length + ",op2:" + op2.length);                                           
                                            result.append(numbers[numIdx]);
                                            result.append(opStr[opMiddle]);
                                            ++numIdx;
                                            for (k = 0; k < op2.length; ++k, ++numIdx) {
                                                result.append(numbers[numIdx]);
                                                //construct string
                                                switch (op2[k]) {
                                                    case 1:
                                                        result.append('+');
                                                        break;
                                                    case 2:
                                                        result.append('-');
                                                        break;
                                                    case 0:
                                                        result.append(' ');
                                                        break;
                                                }
                                            }
                                            result.append(numbers[numIdx]);

                                        }
                                    }

                                    ++idx;
                                }

                            }

                            //                            System.out.println(Arrays.toString(curResult));
                            //                            System.out.println();

                        }
                    }
                }
                //                System.out.println();

            }

            /////////////////////////////////////////////////
            //test
            //            System.out.println();
            //            for (int size = 2; size < map.length; ++size) {
            //                System.out.println("size: " + size);
            //                //startIdx+size
            //                for (int startIdx = 0; startIdx + size - 1 < numbers.length; ++startIdx) {
            //                    for (int secondStartIdx = startIdx + 1; secondStartIdx < startIdx + size; ++secondStartIdx) {
            //                        System.out.print("( ");
            //                        for (int i = startIdx; i < secondStartIdx; ++i) {
            //                            System.out.print(numbers[i] + " ");
            //                        }
            //                        System.out.print(") , ( ");
            //                        for (int i = secondStartIdx; i < startIdx + size; ++i) {
            //                            System.out.print(numbers[i] + " ");
            //                        }
            //                        System.out.println(")");
            //                    }
            //                }
            //                System.out.println();
            //
            //            }

            //        System.out.println("size 1:"+Arrays.toString(map[1][startIdx]));
            //        for (int i = 1; i < map.length; ++i) {

            //test
            //                        for (int size = 2; size < map.length; ++size) {
            //                            System.out.println(opMap[size].length + ":");
            //                            for (int i = 0; i < opMap[size].length; ++i) {
            //                                System.out.println(Arrays.toString(opMap[size][i]));
            //                            }
            //                            System.out.println();
            //            
            //                        }
            //            System.out.print("size 1: ");
            //            for (int startIdx = 0; startIdx < numbers.length; ++startIdx) {
            //                System.out.print(map[1][startIdx][0] + " ");
            //            }
            //            System.out.println();
            //            System.out.println("MAX:" + MAX);
            //            System.out.println("  " + Arrays.toString(numbers));
            //            for (int size = 1; size < map.length; ++size) {
            //                System.out.print(size + ": ");
            //                for (int startIdx = 0; startIdx < numbers.length; ++startIdx) {
            //                    if (map[size][startIdx] != null) {
            //                        System.out.print(map[size][startIdx].length + "  ");
            //                    } else {
            //                        System.out.print("    ");
            //                    }
            //                }
            //                System.out.println();
            //            }
            //        for (int i = 1; i < map.length; ++i) {
            //            System.out.println(i + ": " + Arrays.toString(map[i]));
            //        }
        }
        if (result.length() == 0) {
            return "NOTFOUND";
        }
        return result.toString();
    }

    //' '：0
    //+：1
    //-： 2
    public static String run6(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        StringBuilder result = null;
        int [] numbers = null;
        //        int MAX = 0;
        char ch = arg.charAt(0);
        if (ch == 'S') {
            //write a table contains 3~15???
            int n = 0;
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            //3<=N<=15
            if (n < 3 || n > 15) {
                return "ILLEGAL";
            }
            numbers = new int[n];
            for (int i = 0; i < numbers.length; ++i) {
                numbers[i] = i + 1;
            }
            //various buffer size
            result = new StringBuilder(2 << n);
            //            MAX = n / 2 + 1;
        } else if (ch == 'R') {
            String asdStr = arg.substring(1);
            String [] numberStr = asdStr.split(",");
            numbers = new int[numberStr.length];
            int pre = Integer.MIN_VALUE;
            try {
                for (int i = 0; i < numbers.length; ++i) {
                    if (numberStr[i].isEmpty() || numberStr[i].charAt(0) > '9') {
                        //not a number
                        return "ILLEGAL";
                    }
                    numbers[i] = Integer.parseInt(numberStr[i]);
                    if (numbers[i] < 0 || numbers[i] < pre) {
                        //not larger than previous
                        return "ILLEGAL";
                    }
                    pre = numbers[i];
                }
            } catch (Exception e) {
                return "ILLEGAL";
            }
            result = new StringBuilder(64);
            //            MAX = numberStr.length / 2 + 1;
            //            MAX = (numberStr.length * numberStr[numberStr.length - 1].length()) / 2 + 1;
            //            if (MAX < numbers.length - 1) {
            //                MAX = numbers.length - 1;
            //            }

        } else {
            return "ILLEGAL";
        }
        final int MAX_TREE_DEPTH = ((numbers.length % 2) == 0) ? numbers.length >> 1 : (numbers.length >> 1) + 1;
        //        System.out.println("MAX_TREE_DEPTH:" + MAX_TREE_DEPTH);
        //!!!
        //        MAX = numbers.length - 2;
        //        MAX = numbers.length / 2 + 1;
        //n<=15, MAX_TREE_DEPTH<=8, use int type
        if (MAX_TREE_DEPTH > 15) {
            return "ILLEGAL";
        }
        //cache large number
        //row: startNum
        //column:endNum
        //save the index of number
        int [][] combinedNumMap = new int[numbers.length][numbers.length];
        for (int i = 0; i < numbers.length; ++i) {
            combinedNumMap[i][i] = numbers[i];
            for (int j = i + 1; j < numbers.length; ++j) {
                //!!!limit the combined number
                if ((j - i + 1) <= MAX_TREE_DEPTH + 1) {
                    combinedNumMap[i][j] = numbers[j];
                    long cur = combinedNumMap[i][j];
                    int c = 1;
                    do {
                        cur /= 10;
                        c *= 10;
                    } while (cur > 0);
                    combinedNumMap[i][j] = combinedNumMap[i][j - 1] * c + combinedNumMap[i][j];
                } else {
                    combinedNumMap[i][j] = 0;
                }
            }
        }

        final int OP_NUM = 3;
        int [] answerNumMap = new int[MAX_TREE_DEPTH + 1];
        for (int size = 1, answerNum = 1; size < answerNumMap.length; ++size, answerNum *= OP_NUM) {
            answerNumMap[size] = answerNum;
        }
        //////////////////////////////////
        //row: the idx is the size of set, the size of f(3,5,6,9) is 4, map[4][][]
        //column:start idx
        //element:value, order by ' ',+,-
        //if the value is not illegal, make sure it won't be used
        final char [] opStr = {' ', '+', '-'};
        //TODO decrease the number
        int [][][] results = new int[MAX_TREE_DEPTH + 1][numbers.length][];
        //initial the single value
        for (int startIdx = 0; startIdx < numbers.length; ++startIdx) {
            results[1][startIdx] = new int[1];
            results[1][startIdx][0] = numbers[startIdx];
        }
        /////////////////////////////////////////////////////////////////

        boolean isFirst = true;
        //store op seq for each answer map
        //row:answer index
        //column: 
        //element:op 
        //!!!!pay attention out of memory...
        //TODO
        char [][][] opMap = new char[MAX_TREE_DEPTH + 1][][];
        for (int size = 1; size < opMap.length; ++size) {
            //last one stores if the op is valid!!!!
            opMap[size] = new char[answerNumMap[size]][size];
            for (int aIdx = 0; aIdx < opMap[size].length; ++aIdx) {
                int t = aIdx;
                int j = opMap[size][aIdx].length - 2;
                while (j >= 0) {
                    opMap[size][aIdx][j] = (char)(t % OP_NUM);
                    t /= OP_NUM;
                    --j;
                    if (t == 0) {
                        for (; j >= 0; --j) {
                            opMap[size][aIdx][j] = (0);
                        }
                        break;
                    }

                }
                //mark it valid
                opMap[size][aIdx][opMap[size][aIdx].length - 1] = 1;
            }

        }

        for (int size = 2; size <= numbers.length; ++size) {
            /////////////////////////////////////////////////
            //                System.out.println("size: " + size);
            //endIdx=secondStartIdx-1
            //secondEndIdx=startIdx+size-1
            int k = 0;
            for (int startIdx = 0; startIdx + size - 1 < numbers.length; ++startIdx) {
                if (size < numbers.length && (size <= MAX_TREE_DEPTH && results[size][startIdx] == null)) {
                    results[size][startIdx] = new int[answerNumMap[size]];
                }
                int secondStartIdx = startIdx + (size / 2);
                int size1 = secondStartIdx - startIdx;
                int size2 = size - size1;
                if (size1 <= MAX_TREE_DEPTH && size2 <= MAX_TREE_DEPTH) {
                    //                        System.out.print("( ");
                    //                        for (int i = startIdx; i < secondStartIdx; ++i) {
                    //                            System.out.print(numbers[i] + " ");
                    //                        }
                    //                        System.out.print(") , ( ");
                    //                        for (int i = secondStartIdx; i < startIdx + size; ++i) {
                    //                            System.out.print(numbers[i] + " ");
                    //                        }
                    //                        System.out.println(")");
                    ////////////////////////////////////////////////
                    int [] v1 = results[size1][startIdx];
                    int [] v2 = results[size2][secondStartIdx];
                    //                        System.out.println(Arrays.toString(v1));
                    //                        System.out.println(Arrays.toString(v2));
                    int idx = 0;
                    int value = -1;
                    //1) first part size is 1                            
                    for (int i = 0; i < v1.length; ++i) {
                        char [] op1 = opMap[size1][i];
                        if (op1[op1.length - 1] == 0) {
                            idx += (OP_NUM * v2.length);
                        } else {
                            //                                System.out.println("1) op1[op1.length - 1] == 0");
                            int space1 = 0;
                            int k1 = op1.length - 2;
                            while (k1 >= 0) {
                                if (op1[k1] == 0) {
                                    ++space1;
                                    --k1;
                                } else {
                                    break;
                                }
                            }
                            if (space1 >= MAX_TREE_DEPTH) {
                                //                                System.out.println("2) space1 >= MAX_TREE_DEPTH");
                                //mark it invalid
                                op1[op1.length - 1] = 0;
                                idx += (OP_NUM * v2.length);
                            } else {
                                //2) middle op
                                for (int opMiddle = 0; opMiddle < OP_NUM; ++opMiddle) {
                                    for (int j = 0; j < v2.length; ++j) {
                                        char [] op2 = opMap[size2][j];
                                        if (op2[op2.length - 1] > 0) {
                                            //                                        System.out.println(op2.length);
                                            //                                        for (k = 0; k < op2.length - 1; ++k) {
                                            //                                            System.out.print(((char)opStr[op2[k]]) + " ");
                                            //                                        }
                                            //                                        System.out.println("\n3) op2[op2.length - 1] == 0");

                                            boolean isMergedWidthBeyond = false;
                                            switch (opMiddle) {
                                                case 0: {
                                                    int space2 = 0;
                                                    k1 = 0;
                                                    while (k1 < op2.length - 1) {
                                                        if (op2[k1] == 0) {
                                                            ++space2;
                                                            ++k1;
                                                        } else {
                                                            break;
                                                        }
                                                    }
                                                    if (space2 >= MAX_TREE_DEPTH) {
                                                        //                                                value = Long.MIN_VALUE;
                                                        //mark it invalid
                                                        op2[op2.length - 1] = 0;
                                                        //                                                System.out.println("4) op2[op2.length - 1] == 0");

                                                        break;//for switch
                                                    }
                                                    if (space1 + space2 + 1 >= MAX_TREE_DEPTH) {
                                                        isMergedWidthBeyond = true;
                                                        break;
                                                    }

                                                    value = v1[i] + v2[j];
                                                    //second prefix
                                                    value -= combinedNumMap[secondStartIdx][secondStartIdx
                                                            + space2];
                                                    //                                                System.out.println("MAX:"+ MAX+", width:" + (secondStartIdx
                                                    //                                                        + space2 - (secondStartIdx - 1 - space1) + 1));
                                                    if (op1.length - 2 - space1 >= 0) {//there is other op before space in first part
                                                        if (op1[op1.length - space1 - 2] == 1) {//+
                                                            //first postfix
                                                            value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                            value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                    + space2];
                                                        } else if (op1[op1.length - space1 - 2] == 2) {//-
                                                            value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                            value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                    + space2];
                                                        } else {
                                                            //                                                    System.out.println("error");
                                                        }

                                                    } else {

                                                        value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                        //may be larger than MAX width
                                                        value += combinedNumMap[startIdx][secondStartIdx + space2];
                                                    }
                                                    //                                                System.out.println(combinedNumMap[startIdx][secondStartIdx + space2]);
                                                }
                                                    break;
                                                case 1:
                                                    value = v1[i] + v2[j];
                                                    break;
                                                case 2: {
                                                    //!!!!!substract, braceket...
                                                    //only first number need to change to substract
                                                    int preSpace = 0;
                                                    for (k = 0; k < op2.length - 1; ++k) {
                                                        if (op2[k] != 0) {
                                                            break;
                                                        } else {
                                                            ++preSpace;
                                                        }
                                                    }
                                                    if (preSpace >= MAX_TREE_DEPTH) {
                                                        //mark it invalid
                                                        op2[op2.length - 1] = 0;
                                                        break;
                                                    }
                                                    value = v1[i]
                                                            - 2
                                                            * combinedNumMap[secondStartIdx][secondStartIdx
                                                                    + preSpace]
                                                            + v2[j];
                                                }
                                                    break;
                                            }
                                            if (size <= MAX_TREE_DEPTH) {
                                                results[size][startIdx][idx] = value;
                                            }

                                            //                                        System.out.println("Found: " + v1[i]
                                            //                                                + (Character.toString(opStr[opMiddle]))
                                            //                                                + v2[j]
                                            //                                                + "="
                                            //                                                + value);
                                            //                                        System.out.print(Arrays.toString(op1) + " ");
                                            //                                        System.out.print((opMiddle) + " ");
                                            //                                        System.out.println(Arrays.toString(op2));
                                            //                                        System.out.println();
                                            if (value == 0 && size == numbers.length && op1[op1.length - 1] > 0
                                                    && op2[op2.length - 1] > 0 && !isMergedWidthBeyond) {
                                                //                                            System.out.println("Found: " + v1[0]
                                                //                                                    + (Character.toString(opStr[opMiddle]))
                                                //                                                    + v2[j]
                                                //                                                    + "="
                                                //                                                    + curResult[idx]);
                                                //                                            System.out.print(Arrays.toString(op1) + " ");
                                                //                                            System.out.print((opMiddle) + " ");
                                                //                                            System.out.println(Arrays.toString(op2));
                                                //                                                boolean isNeedSwap = false;
                                                if (isFirst) {
                                                    isFirst = false;
                                                } else {
                                                    result.append(';');
                                                }
                                                int numIdx = 0;
                                                //                                                System.out.println("op1:" + op1.length + ",op2:" + op2.length);
                                                for (numIdx = 0; numIdx < op1.length - 1; ++numIdx) {
                                                    result.append(numbers[numIdx]);
                                                    //construct string
                                                    result.append(opStr[op1[numIdx]]);
                                                }
                                                result.append(numbers[numIdx]);
                                                result.append(opStr[opMiddle]);
                                                ++numIdx;
                                                for (k = 0; k < op2.length - 1; ++k, ++numIdx) {
                                                    result.append(numbers[numIdx]);
                                                    //construct string
                                                    result.append(opStr[op2[k]]);
                                                }
                                                result.append(numbers[numIdx]);
                                            }
                                        }
                                        ++idx;
                                    }
                                }
                            }
                        }
                    }
                    //                            System.out.println(Arrays.toString(curResult));
                    //                            System.out.println();
                }
            }
        }

        //test
        //            for (int size = 2; size < opMap.length; ++size) {
        //                System.out.println(opMap[size].length + ":");
        //                for (int i = 0; i < opMap[size].length; ++i) {
        //                    for (int j = 0; j < opMap[size][i].length - 1; ++j) {
        //                        //                    System.out.println(Arrays.toString(opStr[opMap[size][i]]));
        //                        System.out.println(opStr[opMap[size][i][j]] + " ");
        //                    }
        //                }
        //                System.out.println();
        //
        //            }
        //            System.out.print("size 1: ");
        //            for (int startIdx = 0; startIdx < numbers.length; ++startIdx) {
        //                System.out.print(map[1][startIdx][0] + " ");
        //            }
        //            System.out.println();
        //            System.out.println("MAX:" + MAX);
        //            System.out.println("  " + Arrays.toString(numbers));
        //            for (int size = 1; size < map.length; ++size) {
        //                System.out.print(size + ": ");
        //                for (int startIdx = 0; startIdx < numbers.length; ++startIdx) {
        //                    if (map[size][startIdx] != null) {
        //                        System.out.print(map[size][startIdx].length + "  ");
        //                    } else {
        //                        System.out.print("    ");
        //                    }
        //                }
        //                System.out.println();
        //            }
        //        for (int i = 1; i < map.length; ++i) {
        //            System.out.println(i + ": " + Arrays.toString(map[i]));
        //        }

        if (result.length() == 0) {
            return "NOTFOUND";
        }
        return result.toString();
    }

    //correct: 130321
    public static String run7(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        //        long start = System.currentTimeMillis();
        StringBuilder result = null;
        int [] numbers = null;

        //        int MAX = 0;
        char ch = arg.charAt(0);
        if (ch == 'S') {
            //write a table contains 3~15???
            int n = 0;
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            //3<=N<=15
            if (n < 3 || n > 15) {
                return "ILLEGAL";
            }
            numbers = new int[n];
            for (int i = 0; i < numbers.length; ++i) {
                numbers[i] = i + 1;
            }
            //various buffer size
            result = new StringBuilder(2 << n);
            //            MAX = n / 2 + 1;
        } else if (ch == 'R') {
            String asdStr = arg.substring(1);
            String [] numberStr = asdStr.split(",");
            numbers = new int[numberStr.length];
            int pre = Integer.MIN_VALUE;
            try {
                for (int i = 0; i < numbers.length; ++i) {
                    if (numberStr[i].isEmpty() || numberStr[i].charAt(0) > '9') {
                        //not a number
                        return "ILLEGAL";
                    }
                    numbers[i] = Integer.parseInt(numberStr[i]);
                    if (numbers[i] < 0 || numbers[i] < pre) {
                        //not larger than previous
                        return "ILLEGAL";
                    }
                    pre = numbers[i];
                }
            } catch (Exception e) {
                return "ILLEGAL";
            }
            result = new StringBuilder(64);
            //            MAX = numberStr.length / 2 + 1;
            //            MAX = (numberStr.length * numberStr[numberStr.length - 1].length()) / 2 + 1;
            //            if (MAX < numbers.length - 1) {
            //                MAX = numbers.length - 1;
            //            }

        } else {
            return "ILLEGAL";
        }
        //        System.out.println("1 input " + (System.currentTimeMillis() - start));
        //        start = System.currentTimeMillis();
        final int MAX_TREE_DEPTH = ((numbers.length % 2) == 0) ? numbers.length >> 1 : (numbers.length >> 1) + 1;
        //        System.out.println("MAX_TREE_DEPTH:" + MAX_TREE_DEPTH);
        //!!!
        //        MAX = numbers.length - 2;
        //        MAX = numbers.length / 2 + 1;
        //n<=15, MAX_TREE_DEPTH<=8, use int type
        if (MAX_TREE_DEPTH > 15) {
            return "ILLEGAL";
        }

        final char [] opStr = {' ', '+', '-'};
        final int OP_NUM = 3;
        boolean isFirst = true;

        //cache large number
        //row: startNum
        //column:endNum
        //save the index of number
        int [][] combinedNumMap = new int[numbers.length][numbers.length];

        if (ch == 'S') {
            for (int i = 0; i < numbers.length; ++i) {
                combinedNumMap[i][i] = numbers[i];
                for (int j = i + 1; j < numbers.length; ++j) {
                    //!!!limit the combined number
                    if ((j - i + 1) <= MAX_TREE_DEPTH + 1) {
                        if (numbers[j] < 10) {
                            combinedNumMap[i][j] = combinedNumMap[i][j - 1] * 10 + numbers[j];
                        } else {
                            combinedNumMap[i][j] = combinedNumMap[i][j - 1] * 100 + numbers[j];
                        }
                    } else {
                        combinedNumMap[i][j] = 0;
                    }
                }
            }
        } else {
            for (int i = 0; i < numbers.length; ++i) {
                combinedNumMap[i][i] = numbers[i];
                for (int j = i + 1; j < numbers.length; ++j) {
                    //!!!limit the combined number
                    if ((j - i + 1) <= MAX_TREE_DEPTH + 1) {
                        combinedNumMap[i][j] = numbers[j];
                        long cur = combinedNumMap[i][j];
                        int c = 1;
                        do {
                            cur /= 10;
                            c *= 10;
                        } while (cur > 0);
                        combinedNumMap[i][j] = combinedNumMap[i][j - 1] * c + combinedNumMap[i][j];
                    } else {
                        combinedNumMap[i][j] = 0;
                    }
                }
            }
        }
        int [] answerNumMap = new int[MAX_TREE_DEPTH + 1];
        for (int size = 1, answerNum = 1; size < answerNumMap.length; ++size, answerNum *= OP_NUM) {
            answerNumMap[size] = answerNum;
        }
        //////////////////////////////////
        //row: the idx is the size of set, the size of f(3,5,6,9) is 4, map[4][][]
        //column:start idx
        //element:value, order by ' ',+,-
        //if the value is not illegal, make sure it won't be used

        //TODO decrease the number
        int [][][] results = new int[MAX_TREE_DEPTH + 1][numbers.length][];
        //initial the single value
        for (int startIdx = 0; startIdx < numbers.length; ++startIdx) {
            results[1][startIdx] = new int[1];
            results[1][startIdx][0] = numbers[startIdx];
        }
        /////////////////////////////////////////////////////////////////

        //store op seq for each answer map
        //row:size
        //column: answer index
        //element:op 
        //!!!!pay attention out of memory...
        //the space count won't larger than MAX_TREE_DEPTH for single set
        //except merged set
        char [][][] opMap = new char[MAX_TREE_DEPTH + 1][][];
        for (int size = 1; size < opMap.length; ++size) {
            //last one stores if the op is valid!!!!
            opMap[size] = new char[answerNumMap[size]][size];
            if (opMap[size][0].length - 1 <= MAX_TREE_DEPTH) {
                opMap[size][0][opMap[size][0].length - 1] = 1;
            }
            //by default, it invalid
            for (int aIdx = 1; aIdx < opMap[size].length; ++aIdx) {
                int t = aIdx;
                int j = opMap[size][aIdx].length - 2;
                while (j >= 0) {//store op
                    opMap[size][aIdx][j] = (char)(t % OP_NUM);
                    t /= OP_NUM;
                    --j;
                    if (t == 0) {
                        for (; j >= 0; --j) {
                            opMap[size][aIdx][j] = (0);
                        }
                        break;
                    }

                }
                //mark it valid
                opMap[size][aIdx][opMap[size][aIdx].length - 1] = 1;
            }
        }
        /////////////////////////////////////////////////
        final int NODE_LEN = 3;
        //partition
        //0:startidx
        //1:len
        //2:layer
        int [] unprocessedStack = new int[(MAX_TREE_DEPTH + 1) * NODE_LEN];
        //use the last one as idx
        int [][] processedQueue = new int[MAX_TREE_DEPTH][numbers.length * NODE_LEN];
        //        int layerIdx = 0;

        //push the whole seq
        unprocessedStack[0] = 0;
        unprocessedStack[1] = numbers.length;
        //first layer is -1
        unprocessedStack[2] = -1;
        //point to the first unprocessed element
        int unprocessedStackIdx = NODE_LEN;
        //        int processedQueueIdx = 0;
        int maxLayer = 0;
        while (unprocessedStackIdx > 0) {
            //pop
            int startIdx = unprocessedStack[unprocessedStackIdx - 3];
            int len = unprocessedStack[unprocessedStackIdx - 2];
            int layer = unprocessedStack[unprocessedStackIdx - 1];
            unprocessedStackIdx -= NODE_LEN;

            //test
            //            System.out.print("pop number [" + numbers[startIdx]);
            //            if (len > 1) {
            //                System.out.print(", " + numbers[(startIdx + len - 1)]);
            //            }
            //            System.out.println("]");
            //
            //

            if (len <= 2) {
                //                System.out.print("add to process queue, number [" + numbers[startIdx]);
                //                if (len > 1) {
                //                    System.out.print(", " + numbers[(startIdx + len - 1)]);
                //                }
                //                System.out.println("]");
                //
                //
                if (results[1][startIdx] == null) {
                    results[1][startIdx] = new int[1];
                    results[1][startIdx][0] = numbers[startIdx];
                }
                if (len == 2) {
                    if (results[1][startIdx + 1] == null) {
                        results[1][startIdx + 1] = new int[1];
                        results[1][startIdx + 1][0] = numbers[startIdx + 1];
                    }
                    if (results[2][startIdx] == null) {
                        results[2][startIdx] = new int[answerNumMap[2]];
                    }
                    int v1 = results[1][startIdx][0];
                    int v2 = results[1][startIdx + 1][0];
                    for (int opMiddle = 0, idx = 0; opMiddle < OP_NUM; ++opMiddle, ++idx) {
                        switch (opMiddle) {
                            case 0:
                                results[2][startIdx][idx] = combinedNumMap[startIdx][startIdx + 1];
                                break;
                            case 1:
                                results[2][startIdx][idx] = v1 + v2;
                                break;
                            case 2:
                                results[2][startIdx][idx] = v1 - v2;
                                break;
                        }
                    }
                }
            } else {
                int nextLayer = layer + 1;
                if (nextLayer > maxLayer) {
                    maxLayer = nextLayer;
                }
                //push to unproccesed queue
                int secondStartIdx = startIdx + (len / 2);
                int size1 = secondStartIdx - startIdx;
                int size2 = len - size1;
                //Test
                //                                System.out.println("push:");
                //                                System.out.println("idx [" + startIdx + ", " + (startIdx + size1 - 1) + "], [" + secondStartIdx + ","
                //                                        + (secondStartIdx + size2 - 1) + "]");
                //                
                //                                System.out.print("push number [" + numbers[startIdx]);
                //                                if (size1 > 1) {
                //                                    System.out.print(", " + numbers[(startIdx + size1 - 1)]);
                //                                }
                //                                System.out.print("], [" + numbers[secondStartIdx]);
                //                                if (size2 > 1) {
                //                                    System.out.print("," + numbers[(secondStartIdx + size2 - 1)]);
                //                                }
                //                                System.out.println("], layer:"+nextLayer);

                //

                //push
                unprocessedStack[unprocessedStackIdx] = secondStartIdx;
                unprocessedStack[unprocessedStackIdx + 1] = size2;
                unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
                unprocessedStackIdx += NODE_LEN;
                //
                unprocessedStack[unprocessedStackIdx] = startIdx;
                unprocessedStack[unprocessedStackIdx + 1] = size1;
                unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
                unprocessedStackIdx += NODE_LEN;
                //
                //
                //                System.out.println("----------------------------");
                int idx = processedQueue[nextLayer][processedQueue[nextLayer].length - 1];

                processedQueue[nextLayer][idx] = startIdx;
                processedQueue[nextLayer][idx + 1] = size1;
                processedQueue[nextLayer][idx + 2] = nextLayer;

                //                System.out.println(processedQueue[layerIdx][idx]);
                //                System.out.println(processedQueue[layerIdx][idx+1]);
                //                System.out.println(processedQueue[layerIdx][processedQueue[layerIdx].length - 1]);

                processedQueue[nextLayer][idx + 3] = secondStartIdx;
                processedQueue[nextLayer][idx + 4] = size2;
                processedQueue[nextLayer][idx + 5] = nextLayer;

                processedQueue[nextLayer][processedQueue[nextLayer].length - 1] += 6;

            }
        }
        //        System.out.println("2 prepare " + (System.currentTimeMillis() - start));
        //        start = System.currentTimeMillis();
        //////////////////////////////////////////////////////////////
        //test

        //        System.out.println("\nmerge");
        //        for (int i = maxLayer; i >= 0; --i) {
        //            //            for (int i = layerIdx - 1; i >= 0; --i) {
        //            for (int j = 0; j < processedQueue[i][processedQueue[i].length - 1]; j += NODE_LEN) {
        //                int startIdx = processedQueue[i][j];
        //                int len = processedQueue[i][j + 1];
        //                int layer = processedQueue[i][j + 2];
        //                //pop
        //                //test
        //                System.out.print("check number [" + numbers[startIdx]);
        //                if (len > 1) {
        //                    System.out.print(", " + numbers[(startIdx + len - 1)]);
        //                }
        //                System.out.println("] layer:" + layer);
        //                //
        //                //
        //                //
        //
        //            }
        //            System.out.println();
        //        }
        //
        //        System.out.println();
        //        System.out.println("processedQueueIdx:" + processedQueueIdx);
        //        long strTime = 0;
        //use queue like a stack here,
        //point to the head of the queue

        //test
        //        int calcCount = 0;
        boolean isFinish = false;
        for (int layerIdx = maxLayer; layerIdx >= 0 && !isFinish; --layerIdx)
        //                int layerIdx = maxLayer; 
        {
            //            int headIdx = 0;
            int k = 0;
            int d = NODE_LEN;
            while (d <= numbers.length && !isFinish) {
                int nextD = (d << 1);
                //            System.out.println(d);
                for (int qIdx = 0; qIdx < processedQueue[layerIdx].length - 1; qIdx += nextD) {
                    if (qIdx + d >= processedQueue[layerIdx][processedQueue[layerIdx].length - 1]) {
                        break;
                    }
                    //                    System.out.println("d:" + d + ",next d:" + nextD + ",i:" + qIdx);
                    int startIdx = processedQueue[layerIdx][qIdx];
                    int size1 = processedQueue[layerIdx][qIdx + 1];
                    int secondStartIdx = processedQueue[layerIdx][qIdx + d];
                    int size2 = processedQueue[layerIdx][qIdx + d + 1];
                    int size = size1 + size2;

                    //                System.out.println("startIdx:" + startIdx);
                    //                System.out.println("size:" + size);
                    //                System.out.println("i=" + qIdx);
                    char [][] opAll = null;
                    int opAllIdx = 0;
                    if (size <= MAX_TREE_DEPTH) {
                        opAll = opMap[size];
                        if (results[size][startIdx] == null) {
                            results[size][startIdx] = new int[answerNumMap[size]];
                        }
                    }

                    processedQueue[layerIdx][qIdx] = startIdx;
                    processedQueue[layerIdx][qIdx + 1] = size;
                    int [] v1 = results[size1][startIdx];
                    int [] v2 = results[size2][secondStartIdx];

                    ////////////////////////////////////
                    //test
                    //                    System.out.print("process number [" + numbers[startIdx]);
                    //                    if (size1 > 1) {
                    //                        System.out.print(", " + numbers[(startIdx + size1 - 1)]);
                    //                    }
                    //                    System.out.print("] (" + v1.length + ") , [" + numbers[secondStartIdx]);
                    //                    if (size2 > 1) {
                    //                        System.out.print("," + numbers[(secondStartIdx + size2 - 1)]);
                    //                    }
                    //                    System.out.println("] (" + v2.length + ")");
                    //            /////////////////////////////////////////////////////////////
                    //                        System.out.println(Arrays.toString(v1));
                    //                        System.out.println(Arrays.toString(v2));

                    int value = -1;

                    //1) first part size is 1                            
                    for (int i = 0; i < v1.length; ++i) {
                        char [] op1 = opMap[size1][i];
                        if (op1[op1.length - 1] == 0) {//[invalid1]this op seq is invalid
                            if (opAll != null) {
                                opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                System.out.println("op1 is invalid");
                            }
                            opAllIdx += (OP_NUM * v2.length);
                            //                                System.out.println("1) op1[op1.length - 1] == 0");
                        } else {

                            int space1 = 0;
                            int k1 = op1.length - 2;
                            while (k1 >= 0) {
                                if (op1[k1] == 0) {
                                    ++space1;
                                    --k1;
                                } else {
                                    break;
                                }
                            }
                            //2) middle op
                            for (int opMiddle = 0; opMiddle < OP_NUM; ++opMiddle) {
                                for (int j = 0; j < v2.length; ++j) {
                                    char [] op2 = opMap[size2][j];
                                    if (op2[op2.length - 1] > 0) {
                                        boolean isMergedWidthBeyond = false;

                                        switch (opMiddle) {
                                            case 0: {
                                                int space2 = 0;
                                                k1 = 0;
                                                while (k1 < op2.length - 1) {
                                                    if (op2[k1] == 0) {
                                                        ++space2;
                                                        ++k1;
                                                    } else {
                                                        break;
                                                    }
                                                }
                                                //[invalid2] merged width is larger than depth
                                                //this case only happened in last time
                                                if (space1 + space2 + 1 >= MAX_TREE_DEPTH) {
                                                    isMergedWidthBeyond = true;
                                                    if (opAll != null) {
                                                        opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                                        System.out.println("isMergedWidthBeyond = true");
                                                    }
                                                    //                                                   
                                                    break;
                                                }

                                                value = v1[i] + v2[j];
                                                //second prefix
                                                value -= combinedNumMap[secondStartIdx][secondStartIdx
                                                        + space2];
                                                //                                                System.out.println("MAX:"+ MAX+", width:" + (secondStartIdx
                                                //                                                        + space2 - (secondStartIdx - 1 - space1) + 1));
                                                if (op1.length - 2 - space1 >= 0) {//there is other op before space in first part
                                                    if (op1[op1.length - space1 - 2] == 1) {//+
                                                        //first postfix
                                                        value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                        value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                + space2];
                                                    } else if (op1[op1.length - space1 - 2] == 2) {//-
                                                        value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                        value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                + space2];
                                                    } else {
                                                        //                                                    System.out.println("error");
                                                    }

                                                } else {

                                                    value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                    //may be larger than MAX width
                                                    value += combinedNumMap[startIdx][secondStartIdx + space2];
                                                }
                                                //                                                    ++calcCount;
                                                //                                                System.out.println(combinedNumMap[startIdx][secondStartIdx + space2]);
                                            }
                                                break;
                                            case 1:
                                                value = v1[i] + v2[j];
                                                //                                                    ++calcCount;
                                                break;
                                            case 2: {
                                                //!!!!!substract, braceket...
                                                //only first number need to change to substract
                                                int preSpace = 0;
                                                for (k = 0; k < op2.length - 1; ++k) {
                                                    if (op2[k] != 0) {
                                                        break;
                                                    } else {
                                                        ++preSpace;
                                                    }
                                                }
                                                value = v1[i]
                                                        - 2
                                                        * combinedNumMap[secondStartIdx][secondStartIdx
                                                                + preSpace]
                                                        + v2[j];
                                                //                                                    ++calcCount;
                                            }
                                                break;
                                        }

                                        if (size <= MAX_TREE_DEPTH) {
                                            results[size][startIdx][opAllIdx] = value;
                                        }
                                        //construct string cost 0ms
                                        if (value == 0 && size == numbers.length && op1[op1.length - 1] > 0
                                                && op2[op2.length - 1] > 0 && !isMergedWidthBeyond) {

                                            //                                              System.out.println("2 prepare " + (System.currentTimeMillis() - start));  
                                            //                                                start = System.currentTimeMillis();
                                            if (isFirst) {
                                                isFirst = false;
                                            } else {
                                                result.append(';');
                                            }
                                            int numIdx = 0;
                                            //                                                System.out.println("op1:" + op1.length + ",op2:" + op2.length);
                                            for (numIdx = 0; numIdx < op1.length - 1; ++numIdx) {
                                                result.append(numbers[numIdx]);
                                                //construct string
                                                result.append(opStr[op1[numIdx]]);
                                            }
                                            result.append(numbers[numIdx]);
                                            result.append(opStr[opMiddle]);
                                            ++numIdx;
                                            for (k = 0; k < op2.length - 1; ++k, ++numIdx) {
                                                result.append(numbers[numIdx]);
                                                //construct string
                                                result.append(opStr[op2[k]]);
                                            }
                                            result.append(numbers[numIdx]);

                                            //                                                start = System.currentTimeMillis();
                                            //                                                strTime += (System.currentTimeMillis() - start);

                                        }
                                    }
                                    //                                        else{
                                    //                                            System.out.println("op2 is invalid");
                                    //                                        }
                                    ++opAllIdx;
                                }
                            }

                        }
                    }
                    if (size == numbers.length) {
                        isFinish = true;
                        //                        System.out.println("isfinish");
                        break;
                    }
                }

                d = nextD;
            }
        }
        //        System.out.println("construct string time: " + strTime + " ms");
        //        System.out.println("3 calc and output " + (System.currentTimeMillis() - start));
        //        System.out.println("calc count:" + calcCount);
        //        start = System.currentTimeMillis();
        if (result.length() == 0) {
            return "NOTFOUND";
        }
        return result.toString();
    }

    //TODO ??? do not calc when back
    public static String run7_2(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        long start = System.currentTimeMillis();
        StringBuilder result = null;
        int [] numbers = null;

        //        int TOTAL_DIGIT_NUM = 0;
        int maxSpaceCount = 0;

        //        int MAX = 0;
        char ch = arg.charAt(0);
        //        int [][] combinedNumDigitMap = null;
        if (ch == 'S') {
            //write a table contains 3~15???
            int n = 0;
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            //3<=N<=15
            if (n < 3 || n > 15) {
                return "ILLEGAL";
            }
            maxSpaceCount = n / 2 - 1;
            if (maxSpaceCount > n / 10 + 2) {
                maxSpaceCount = n / 10 + 2;
            }
            //calc digit data
            //            if (n < 10) {
            //                TOTAL_DIGIT_NUM = n;
            //            } else {
            //                TOTAL_DIGIT_NUM = (n - 10 + 1) * 2 + (10 - 1);
            //            }
            //            maxDigitNum = TOTAL_DIGIT_NUM / 2;

            //            combinedNumDigitMap = new int[n][n];
            numbers = new int[n];
            for (int i = 0; i < numbers.length; ++i) {
                numbers[i] = i + 1;
            }

            ///////////////////////////////////////////
            //various buffer size
            result = new StringBuilder(2 << n);
            //            MAX = n / 2 + 1;
        } else if (ch == 'R') {
            String asdStr = arg.substring(1);
            String [] numberStr = asdStr.split(",");
            numbers = new int[numberStr.length];
            int pre = Integer.MIN_VALUE;
            try {
                for (int i = 0; i < numbers.length; ++i) {
                    if (numberStr[i].isEmpty() || numberStr[i].charAt(0) > '9') {
                        //not a number
                        return "ILLEGAL";
                    }
                    numbers[i] = Integer.parseInt(numberStr[i]);
                    if (numbers[i] < 0 || numbers[i] < pre) {
                        //not larger than previous
                        return "ILLEGAL";
                    }
                    pre = numbers[i];
                }
            } catch (Exception e) {
                return "ILLEGAL";
            }

            result = new StringBuilder(64);

            ///////////////////////////////////////////
            //TODO calc digit data
            //            if (n < 10) {
            //                TOTAL_DIGIT_NUM = n;
            //            } else {
            //                TOTAL_DIGIT_NUM = (n - 10 + 1) * 2 + (10 - 1);
            //            }
            //            maxDigitNum = TOTAL_DIGIT_NUM / 2;

            //            MAX = numberStr.length / 2 + 1;
            //            MAX = (numberStr.length * numberStr[numberStr.length - 1].length()) / 2 + 1;
            //            if (MAX < numbers.length - 1) {
            //                MAX = numbers.length - 1;
            //            }
            maxSpaceCount = ((numbers.length % 2) == 0) ? numbers.length >> 1 : (numbers.length >> 1) + 1;
        } else {
            return "ILLEGAL";
        }
        ///////////////////////////////////////////
        //row:startIdx
        //column:endIdx
        //value:the total digit of the number constructed from startidx to endidx
        //if value is zero, it means invalid startidx and endidx, or beyond max digit
        int [][] combinedNumDigitMap = new int[numbers.length][numbers.length];
        int w = 10;
        int digit = 1;
        int maxDigitNum = 0;
        for (int i = 0; i < numbers.length; ++i) {
            digit = 1;
            w = 10;
            while (true) {
                if (numbers[i] / w == 0) {
                    combinedNumDigitMap[i][i] = digit;
                    break;
                }
                w *= 10;
                ++digit;
            }
            maxDigitNum += digit;
        }
        maxDigitNum >>= 1;
        //        System.out.println("Total digit num:" + TOTAL_DIGIT_NUM);
        System.out.println("Max digit num:" + maxDigitNum);

        //        final int 
        System.out.println("MAX_SPACE_COUNT:" + maxSpaceCount);
        if (maxDigitNum > 15) {
            return "ILLEGAL";
        }

        int halfSize = ((numbers.length % 2) == 0) ? numbers.length >> 1 : (numbers.length >> 1) + 1;
        System.out.println("halfSize：" + halfSize);
        //        int maxTreePath = 1;
        //        int w = numbers.length;
        //        while (w > 0) {
        //            w >>= 1;
        //            ++maxTreePath;
        //        }
        //        System.out.println("maxTreePath:" + maxTreePath);

        final char [] opStr = {' ', '+', '-'};
        final int OP_NUM = 3;
        boolean isFirst = true;
        for (int i = 0; i < numbers.length; ++i) {
            for (int j = i + 1; j < numbers.length; ++j) {
                combinedNumDigitMap[i][j] = combinedNumDigitMap[j][j] + combinedNumDigitMap[i][j - 1];
                if (combinedNumDigitMap[i][j] > maxDigitNum) {
                    combinedNumDigitMap[i][j] = 0;
                    break;
                }
            }
            System.out.println(Arrays.toString(combinedNumDigitMap[i]));
        }
        //cache large number
        //row: startNum
        //column:endNum
        //save the index of number
        int [][] combinedNumMap = new int[numbers.length][numbers.length];
        for (int i = 0; i < numbers.length; ++i) {
            combinedNumMap[i][i] = numbers[i];
            for (int j = i + 1; j < numbers.length; ++j) {
                //!!!limit the combined number
                //                if (j - i <= halfSize) {
                //                    long cur = combinedNumMap[i][j];
                //                    w = 1;
                //                    do {
                //                        cur /= 10;
                //                        w *= 10;
                //                    } while (cur > 0);
                //                    combinedNumMap[i][j] = combinedNumMap[i][j - 1] * w + numbers[j];
                //
                //                } else {
                //                    break;
                //                }
                if (combinedNumDigitMap[i][j] > 0) {
                    int c = combinedNumDigitMap[j][j];
                    w = 10;
                    while (c > 1) {
                        w *= 10;
                        --c;
                    }
                    combinedNumMap[i][j] = combinedNumMap[i][j - 1] * w + numbers[j];
                } else {
                    break;
                }
            }
            System.out.println(Arrays.toString(combinedNumMap[i]));
        }

        //the set size:1~half total size
        int [] answerNumMap = new int[halfSize + 1];
        for (int size = 1, answerNum = 1; size < answerNumMap.length; ++size, answerNum *= OP_NUM) {
            answerNumMap[size] = answerNum;
        }
        //////////////////////////////////
        //row: the idx is the size of set, the size of f(3,5,6,9) is 4, map[4][][]
        //column:start idx
        //element:value, order by ' ',+,-
        //if the value is not illegal, make sure it won't be used

        //decrease the number
        int [][][] results = new int[halfSize + 1][numbers.length][];
        //initial the single value
        for (int startIdx = 0; startIdx < numbers.length; ++startIdx) {
            results[1][startIdx] = new int[1];
            results[1][startIdx][0] = numbers[startIdx];
        }
        //        System.out.println("1 input " + (System.currentTimeMillis() - start));
        //        start = System.currentTimeMillis();
        /////////////////////////////////////////////////////////////////

        //store op seq for each answer map
        //row:size
        //column: answer index
        //element:op 
        //!!!!pay attention out of memory...
        //the space count won't larger than MAX_TREE_DEPTH for single set
        //except merged set
        char [][][] opMap = new char[halfSize + 1][][];
        for (int size = 1; size < opMap.length; ++size) {
            //last one stores if the op is valid!!!!
            opMap[size] = new char[answerNumMap[size]][size];
            if (opMap[size][0].length - 1 <= halfSize) {
                opMap[size][0][opMap[size][0].length - 1] = 1;
            }
            //by default, it invalid
            for (int aIdx = 1; aIdx < opMap[size].length; ++aIdx) {
                int t = aIdx;
                int j = opMap[size][aIdx].length - 2;
                while (j >= 0) {//store op
                    opMap[size][aIdx][j] = (char)(t % OP_NUM);
                    t /= OP_NUM;
                    --j;
                    if (t == 0) {
                        for (; j >= 0; --j) {
                            opMap[size][aIdx][j] = (0);
                        }
                        break;
                    }

                }
                //mark it valid
                opMap[size][aIdx][opMap[size][aIdx].length - 1] = 1;
            }
        }
        /////////////////////////////////////////////////
        final int NODE_LEN = 3;
        //partition
        //0:startidx
        //1:len
        //2:layer
        int [] unprocessedStack = new int[(halfSize + 1) * NODE_LEN];
        //use the last one as idx
        int [][] processedQueue = new int[halfSize][numbers.length * NODE_LEN];
        //        int layerIdx = 0;

        //push the whole seq
        unprocessedStack[0] = 0;
        unprocessedStack[1] = numbers.length;
        //first layer is -1
        unprocessedStack[2] = -1;
        //point to the first unprocessed element
        int unprocessedStackIdx = NODE_LEN;
        //        int processedQueueIdx = 0;
        int maxLayer = 0;
        while (unprocessedStackIdx > 0) {
            //pop
            int startIdx = unprocessedStack[unprocessedStackIdx - 3];
            int len = unprocessedStack[unprocessedStackIdx - 2];
            int layer = unprocessedStack[unprocessedStackIdx - 1];
            unprocessedStackIdx -= NODE_LEN;
            if (len <= 2) {
                if (results[1][startIdx] == null) {
                    results[1][startIdx] = new int[1];
                    results[1][startIdx][0] = numbers[startIdx];
                }
                if (len == 2) {
                    if (results[1][startIdx + 1] == null) {
                        results[1][startIdx + 1] = new int[1];
                        results[1][startIdx + 1][0] = numbers[startIdx + 1];
                    }
                    if (results[2][startIdx] == null) {
                        results[2][startIdx] = new int[answerNumMap[2]];
                    }
                    int v1 = results[1][startIdx][0];
                    int v2 = results[1][startIdx + 1][0];
                    for (int opMiddle = 0, idx = 0; opMiddle < OP_NUM; ++opMiddle, ++idx) {
                        switch (opMiddle) {
                            case 0:
                                results[2][startIdx][idx] = combinedNumMap[startIdx][startIdx + 1];
                                break;
                            case 1:
                                results[2][startIdx][idx] = v1 + v2;
                                break;
                            case 2:
                                results[2][startIdx][idx] = v1 - v2;
                                break;
                        }
                    }
                }
            } else {
                int nextLayer = layer + 1;
                if (nextLayer > maxLayer) {
                    maxLayer = nextLayer;
                }
                //push to unproccesed queue
                int secondStartIdx = startIdx + (len / 2);
                int size1 = secondStartIdx - startIdx;
                int size2 = len - size1;
                //Test
                //                                System.out.println("push:");
                //                                System.out.println("idx [" + startIdx + ", " + (startIdx + size1 - 1) + "], [" + secondStartIdx + ","
                //                                        + (secondStartIdx + size2 - 1) + "]");
                //                
                //                                System.out.print("push number [" + numbers[startIdx]);
                //                                if (size1 > 1) {
                //                                    System.out.print(", " + numbers[(startIdx + size1 - 1)]);
                //                                }
                //                                System.out.print("], [" + numbers[secondStartIdx]);
                //                                if (size2 > 1) {
                //                                    System.out.print("," + numbers[(secondStartIdx + size2 - 1)]);
                //                                }
                //                                System.out.println("], layer:"+nextLayer);

                //

                //push
                unprocessedStack[unprocessedStackIdx] = secondStartIdx;
                unprocessedStack[unprocessedStackIdx + 1] = size2;
                unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
                unprocessedStackIdx += NODE_LEN;
                //
                unprocessedStack[unprocessedStackIdx] = startIdx;
                unprocessedStack[unprocessedStackIdx + 1] = size1;
                unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
                unprocessedStackIdx += NODE_LEN;
                //
                //
                //                System.out.println("----------------------------");
                int idx = processedQueue[nextLayer][processedQueue[nextLayer].length - 1];

                processedQueue[nextLayer][idx] = startIdx;
                processedQueue[nextLayer][idx + 1] = size1;
                processedQueue[nextLayer][idx + 2] = nextLayer;

                //                System.out.println(processedQueue[layerIdx][idx]);
                //                System.out.println(processedQueue[layerIdx][idx+1]);
                //                System.out.println(processedQueue[layerIdx][processedQueue[layerIdx].length - 1]);

                processedQueue[nextLayer][idx + 3] = secondStartIdx;
                processedQueue[nextLayer][idx + 4] = size2;
                processedQueue[nextLayer][idx + 5] = nextLayer;

                processedQueue[nextLayer][processedQueue[nextLayer].length - 1] += 6;

            }
        }
        System.out.println("1 prepare " + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        //////////////////////////////////////////////////////////////
        //test

        //        System.out.println("\nmerge");
        //        for (int i = maxLayer; i >= 0; --i) {
        //            //            for (int i = layerIdx - 1; i >= 0; --i) {
        //            for (int j = 0; j < processedQueue[i][processedQueue[i].length - 1]; j += NODE_LEN) {
        //                int startIdx = processedQueue[i][j];
        //                int len = processedQueue[i][j + 1];
        //                int layer = processedQueue[i][j + 2];
        //                //pop
        //                //test
        //                System.out.print("check number [" + numbers[startIdx]);
        //                if (len > 1) {
        //                    System.out.print(", " + numbers[(startIdx + len - 1)]);
        //                }
        //                System.out.println("] layer:" + layer);
        //                //
        //                //
        //                //
        //
        //            }
        //            System.out.println();
        //        }
        //
        //        System.out.println();
        //        System.out.println("processedQueueIdx:" + processedQueueIdx);
        //        long strTime = 0;
        //use queue like a stack here,
        //point to the head of the queue

        System.out.println("layer:" + maxLayer);
        //test
        //        int calcCount = 0;
        boolean isFinish = false;
        for (int layerIdx = maxLayer; layerIdx >= 0 && !isFinish; --layerIdx) {
            int k = 0;
            int d = NODE_LEN;
            while (d <= numbers.length && !isFinish) {
                int nextD = (d << 1);
                //            System.out.println(d);
                for (int qIdx = 0; qIdx < processedQueue[layerIdx].length - 1; qIdx += nextD) {
                    if (qIdx + d >= processedQueue[layerIdx][processedQueue[layerIdx].length - 1]) {
                        break;
                    }
                    //                    System.out.println("d:" + d + ",next d:" + nextD + ",i:" + qIdx);
                    int startIdx = processedQueue[layerIdx][qIdx];
                    int size1 = processedQueue[layerIdx][qIdx + 1];
                    int secondStartIdx = processedQueue[layerIdx][qIdx + d];
                    int size2 = processedQueue[layerIdx][qIdx + d + 1];
                    int size = size1 + size2;

                    ////////////////////////////////////
                    //test
                    System.out.print("process number [" + numbers[startIdx]);
                    if (size1 > 1) {
                        System.out.print(", " + numbers[(startIdx + size1 - 1)]);
                    }
                    System.out.print("] , [" + numbers[secondStartIdx]);
                    if (size2 > 1) {
                        System.out.print("," + numbers[(secondStartIdx + size2 - 1)]);
                    }
                    System.out.println("]");
                    System.out.println("size1:" + size1 + "," + "size2:" + size2);
                    /////////////////////////////////////////////////////////////
                    //                System.out.println("startIdx:" + startIdx);
                    //                System.out.println("size:" + size);
                    //                System.out.println("i=" + qIdx);
                    //next layer op
                    char [][] opAll = null;
                    int opAllIdx = 0;
                    if (size <= halfSize) {
                        opAll = opMap[size];
                        if (results[size][startIdx] == null) {
                            results[size][startIdx] = new int[answerNumMap[size]];
                            //                            System.out.println("new results "+size);
                        }
                    }

                    processedQueue[layerIdx][qIdx] = startIdx;
                    processedQueue[layerIdx][qIdx + 1] = size;
                    int [] v1 = results[size1][startIdx];
                    int [] v2 = results[size2][secondStartIdx];

                    System.out.println(Arrays.toString(v1));
                    System.out.println(Arrays.toString(v2));

                    System.out.println("op1 size " + opMap[size1].length);
                    System.out.println("op2 size " + opMap[size2].length);
                    int value = -1;

                    //1) first part size is 1                            
                    for (int i = 0; i < v1.length; ++i) {
                        char [] op1 = opMap[size1][i];
                        if (op1[op1.length - 1] == 0) {//[invalid1]this op seq is invalid
                            if (opAll != null) {
                                opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                System.out.println("1) opAll is found invalid by op1");
                            }
                            opAllIdx += (OP_NUM * v2.length);
                            //                                System.out.println("1) op1[op1.length - 1] == 0");
                        } else {
                            int space1 = 0;
                            int k1 = op1.length - 2;
                            while (k1 >= 0) {
                                if (op1[k1] == 0) {
                                    ++space1;
                                    --k1;
                                } else {
                                    break;
                                }
                            }
                            if (space1 >= maxSpaceCount) {
                                op1[op1.length - 1] = 0;
                                System.out.println(space1 + ">=" + maxSpaceCount);
                                System.out.println(combinedNumMap[op1.length - 1 - space1][op1.length - 1]);
                                System.out.print("op1 " + i + ": ");
                                for (k = 0; k < op1.length; ++k) {
                                    System.out.print((int)op1[k]);
                                }
                                System.out.println();
                                //                            }
                                //                            if (size1 > 2 && combinedNumMap[op1.length - 1 - space1][op1.length - 1] == 0) {
                                //                                op1[op1.length - 1] = 0;
                                //                                //                            }
                                //TODO
                                //                            if (space1 > maxSpaceCount) {

                                if (opAll != null) {
                                    opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;

                                }
                                //                                System.out.println("2) opAll is found invalid by op1");
                                opAllIdx += (OP_NUM * v2.length);
                            } else //
                            {
                                //2) middle op
                                for (int opMiddle = 0; opMiddle < OP_NUM; ++opMiddle) {
                                    for (int j = 0; j < v2.length; ++j) {
                                        char [] op2 = opMap[size2][j];
                                        if (op2[op2.length - 1] > 0) {
                                            boolean isMergedWidthBeyond = false;

                                            //                                            if (space2 >= maxSpaceCount) {
                                            //                                                isMergedWidthBeyond = true;
                                            //                                                if (opAll != null) {
                                            //                                                    opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                            //                                                    System.out.println("3) opAll is found invalid by merge");
                                            //                                                }
                                            //
                                            //                                            } else 
                                            {
                                                switch (opMiddle) {
                                                    case 0: {
                                                        int space2 = 0;
                                                        k1 = 0;
                                                        while (k1 < op2.length - 1) {
                                                            if (op2[k1] == 0) {
                                                                ++space2;
                                                                ++k1;
                                                            } else {
                                                                break;
                                                            }
                                                        }
                                                        if (space2 >= maxSpaceCount) {
                                                            isMergedWidthBeyond = true;
                                                            op2[op2.length - 1] = 0;
                                                            if (opAll != null) {
                                                                opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                                                System.out.println("4) opAll is found invalid by merge");
                                                            }
                                                            //                                                            System.out.println(op2.length);
                                                            System.out.print("op2 " + j + ": ");
                                                            for (k = 0; k < op2.length; ++k) {
                                                                System.out.print((int)op2[k]);
                                                            }
                                                            System.out.println();
                                                            //                                                   
                                                            break;
                                                        }
                                                        //[invalid2] merged width is larger than depth
                                                        //this case only happened in last time
                                                        if (space1 + space2 + 1 >= maxSpaceCount) {
                                                            isMergedWidthBeyond = true;
                                                            break;
                                                        }

                                                        value = v1[i] + v2[j];
                                                        //second prefix
                                                        value -= combinedNumMap[secondStartIdx][secondStartIdx
                                                                + space2];
                                                        //                                                System.out.println("MAX:"+ MAX+", width:" + (secondStartIdx
                                                        //                                                        + space2 - (secondStartIdx - 1 - space1) + 1));
                                                        if (op1.length - 2 - space1 >= 0) {//there is other op before space in first part
                                                            if (op1[op1.length - space1 - 2] == 1) {//+
                                                                //first postfix
                                                                value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                                value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                        + space2];
                                                            } else if (op1[op1.length - space1 - 2] == 2) {//-
                                                                value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                                value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                        + space2];
                                                            } else {
                                                                //                                                    System.out.println("error");
                                                            }

                                                        } else {

                                                            value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                            //may be larger than MAX width
                                                            value += combinedNumMap[startIdx][secondStartIdx + space2];
                                                        }
                                                        //                                                    ++calcCount;
                                                        //                                                System.out.println(combinedNumMap[startIdx][secondStartIdx + space2]);
                                                    }
                                                        break;
                                                    case 1:
                                                        value = v1[i] + v2[j];
                                                        //                                                    ++calcCount;
                                                        break;
                                                    case 2: {
                                                        //!!!!!substract, braceket...
                                                        //only first number need to change to substract
                                                        int preSpace = 0;
                                                        for (k = 0; k < op2.length - 1; ++k) {
                                                            if (op2[k] != 0) {
                                                                break;
                                                            } else {
                                                                ++preSpace;
                                                            }
                                                        }
                                                        value = v1[i]
                                                                - 2
                                                                * combinedNumMap[secondStartIdx][secondStartIdx
                                                                        + preSpace]
                                                                + v2[j];
                                                        //                                                    ++calcCount;
                                                    }
                                                        break;
                                                }

                                                if (size <= halfSize) {
                                                    results[size][startIdx][opAllIdx] = value;
                                                }
                                                //construct string cost 0ms
                                                if (value == 0 && size == numbers.length && op1[op1.length - 1] > 0
                                                        && op2[op2.length - 1] > 0 && !isMergedWidthBeyond) {

                                                    //                                              System.out.println("2 prepare " + (System.currentTimeMillis() - start));  
                                                    //                                                start = System.currentTimeMillis();
                                                    if (isFirst) {
                                                        isFirst = false;
                                                    } else {
                                                        result.append(';');
                                                    }
                                                    int numIdx = 0;
                                                    //                                                System.out.println("op1:" + op1.length + ",op2:" + op2.length);
                                                    for (numIdx = 0; numIdx < op1.length - 1; ++numIdx) {
                                                        result.append(numbers[numIdx]);
                                                        //construct string
                                                        result.append(opStr[op1[numIdx]]);
                                                    }
                                                    result.append(numbers[numIdx]);
                                                    result.append(opStr[opMiddle]);
                                                    ++numIdx;
                                                    for (k = 0; k < op2.length - 1; ++k, ++numIdx) {
                                                        result.append(numbers[numIdx]);
                                                        //construct string
                                                        result.append(opStr[op2[k]]);
                                                    }
                                                    result.append(numbers[numIdx]);

                                                    //                                                start = System.currentTimeMillis();
                                                    //                                                strTime += (System.currentTimeMillis() - start);

                                                }
                                            }
                                            //                                        else{
                                            //                                            System.out.println("op2 is invalid");
                                            //                                        }
                                            ++opAllIdx;
                                        }
                                    }
                                }
                            }

                        }
                    }
                    if (size == numbers.length) {
                        isFinish = true;
                        //                        System.out.println("isfinish");
                        break;
                    }
                }

                d = nextD;
            }
        }
        //        System.out.println("construct string time: " + strTime + " ms");
        System.out.println("2 calc and output " + (System.currentTimeMillis() - start));
        //        System.out.println("calc count:" + calcCount);
        //                start = System.currentTimeMillis();
        if (result.length() == 0) {
            return "NOTFOUND";
        }
        return result.toString();
    }

    public static String run7_3(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        long start = System.currentTimeMillis();
        StringBuilder result = null;
        int [] numbers = null;

        //        int TOTAL_DIGIT_NUM = 0;
        int maxSpaceCount = 0;

        //        int MAX = 0;
        char ch = arg.charAt(0);
        //        int [][] combinedNumDigitMap = null;
        if (ch == 'S') {
            //write a table contains 3~15???
            int n = 0;
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            //3<=N<=15
            if (n < 3 || n > 15) {
                return "ILLEGAL";
            }
            maxSpaceCount = n / 2 - 1;
            if (maxSpaceCount > n / 10 + 2) {
                maxSpaceCount = n / 10 + 2;
            }
            //calc digit data
            //            if (n < 10) {
            //                TOTAL_DIGIT_NUM = n;
            //            } else {
            //                TOTAL_DIGIT_NUM = (n - 10 + 1) * 2 + (10 - 1);
            //            }
            //            maxDigitNum = TOTAL_DIGIT_NUM / 2;

            //            combinedNumDigitMap = new int[n][n];
            numbers = new int[n];
            for (int i = 0; i < numbers.length; ++i) {
                numbers[i] = i + 1;
            }

            ///////////////////////////////////////////
            //various buffer size
            result = new StringBuilder(2 << n);
            //            MAX = n / 2 + 1;
        } else if (ch == 'R') {
            String asdStr = arg.substring(1);
            String [] numberStr = asdStr.split(",");
            numbers = new int[numberStr.length];
            int pre = Integer.MIN_VALUE;
            try {
                for (int i = 0; i < numbers.length; ++i) {
                    if (numberStr[i].isEmpty() || numberStr[i].charAt(0) > '9') {
                        //not a number
                        return "ILLEGAL";
                    }
                    numbers[i] = Integer.parseInt(numberStr[i]);
                    if (numbers[i] < 0 || numbers[i] < pre) {
                        //not larger than previous
                        return "ILLEGAL";
                    }
                    pre = numbers[i];
                }
            } catch (Exception e) {
                return "ILLEGAL";
            }

            result = new StringBuilder(64);

            ///////////////////////////////////////////
            //TODO calc digit data
            //            if (n < 10) {
            //                TOTAL_DIGIT_NUM = n;
            //            } else {
            //                TOTAL_DIGIT_NUM = (n - 10 + 1) * 2 + (10 - 1);
            //            }
            //            maxDigitNum = TOTAL_DIGIT_NUM / 2;

            //            MAX = numberStr.length / 2 + 1;
            //            MAX = (numberStr.length * numberStr[numberStr.length - 1].length()) / 2 + 1;
            //            if (MAX < numbers.length - 1) {
            //                MAX = numbers.length - 1;
            //            }
            maxSpaceCount = ((numbers.length % 2) == 0) ? numbers.length >> 1 : (numbers.length >> 1) + 1;
        } else {
            return "ILLEGAL";
        }
        ///////////////////////////////////////////
        //row:startIdx
        //column:endIdx
        //value:the total digit of the number constructed from startidx to endidx
        //if value is zero, it means invalid startidx and endidx, or beyond max digit
        int [][] combinedNumDigitMap = new int[numbers.length][numbers.length];
        int w = 10;
        int digit = 1;
        int maxDigitNum = 0;
        for (int i = 0; i < numbers.length; ++i) {
            digit = 1;
            w = 10;
            while (true) {
                if (numbers[i] / w == 0) {
                    combinedNumDigitMap[i][i] = digit;
                    break;
                }
                w *= 10;
                ++digit;
            }
            maxDigitNum += digit;
        }
        maxDigitNum >>= 1;
        //        System.out.println("Total digit num:" + TOTAL_DIGIT_NUM);
        System.out.println("Max digit num:" + maxDigitNum);

        //        final int 
        System.out.println("MAX_SPACE_COUNT:" + maxSpaceCount);
        if (maxDigitNum > 15) {
            return "ILLEGAL";
        }

        final int maxIntLen = 9;
        int halfSize = ((numbers.length % 2) == 0) ? numbers.length >> 1 : (numbers.length >> 1) + 1;
        System.out.println("halfSize：" + halfSize);
        //        int maxTreePath = 1;
        //        int w = numbers.length;
        //        while (w > 0) {
        //            w >>= 1;
        //            ++maxTreePath;
        //        }
        //        System.out.println("maxTreePath:" + maxTreePath);

        final char [] opStr = {' ', '+', '-'};
        final int OP_NUM = 3;
        boolean isFirst = true;
        for (int i = 0; i < numbers.length; ++i) {
            for (int j = i + 1; j < numbers.length; ++j) {
                combinedNumDigitMap[i][j] = combinedNumDigitMap[j][j] + combinedNumDigitMap[i][j - 1];
                //TODO
                if (combinedNumDigitMap[i][j] > maxIntLen) {
                    combinedNumDigitMap[i][j] = 0;
                    break;
                }
            }
            System.out.println(Arrays.toString(combinedNumDigitMap[i]));
        }
        //cache large number
        //row: startNum
        //column:endNum
        //save the index of number
        int [][] combinedNumMap = new int[numbers.length][numbers.length];
        for (int i = 0; i < numbers.length; ++i) {
            combinedNumMap[i][i] = numbers[i];
            for (int j = i + 1; j < numbers.length; ++j) {
                //!!!limit the combined number
                //                if (j - i <= halfSize) {
                //                    long cur = combinedNumMap[i][j];
                //                    w = 1;
                //                    do {
                //                        cur /= 10;
                //                        w *= 10;
                //                    } while (cur > 0);
                //                    combinedNumMap[i][j] = combinedNumMap[i][j - 1] * w + numbers[j];
                //
                //                } else {
                //                    break;
                //                }
                //TODO
                if (combinedNumDigitMap[i][j] > 0 && combinedNumDigitMap[i][j] < maxIntLen) {
                    int c = combinedNumDigitMap[j][j];
                    w = 10;
                    while (c > 1) {
                        w *= 10;
                        --c;
                    }
                    combinedNumMap[i][j] = combinedNumMap[i][j - 1] * w + numbers[j];
                } else {
                    break;
                }
            }
            System.out.println(Arrays.toString(combinedNumMap[i]));
        }

        //the set size:1~half total size
        int [] answerNumMap = new int[halfSize + 1];
        for (int size = 1, answerNum = 1; size < answerNumMap.length; ++size, answerNum *= OP_NUM) {
            answerNumMap[size] = answerNum;
        }
        //////////////////////////////////
        //row: the idx is the size of set, the size of f(3,5,6,9) is 4, map[4][][]
        //column:start idx
        //element:value, order by ' ',+,-
        //if the value is not illegal, make sure it won't be used

        //decrease the number
        int [][][] results = new int[halfSize + 1][numbers.length][];
        //initial the single value
        for (int startIdx = 0; startIdx < numbers.length; ++startIdx) {
            results[1][startIdx] = new int[1];
            results[1][startIdx][0] = numbers[startIdx];
        }
        //        System.out.println("1 input " + (System.currentTimeMillis() - start));
        //        start = System.currentTimeMillis();
        /////////////////////////////////////////////////////////////////

        //store op seq for each answer map
        //row:size
        //column: answer index
        //element:op 
        //!!!!pay attention out of memory...
        //the space count won't larger than MAX_TREE_DEPTH for single set
        //except merged set
        char [][][] opMap = new char[halfSize + 1][][];
        for (int size = 1; size < opMap.length; ++size) {
            //last one stores if the op is valid!!!!
            opMap[size] = new char[answerNumMap[size]][size];
            if (opMap[size][0].length - 1 <= halfSize) {
                opMap[size][0][opMap[size][0].length - 1] = 1;
            }
            //by default, it invalid
            for (int aIdx = 1; aIdx < opMap[size].length; ++aIdx) {
                int t = aIdx;
                int j = opMap[size][aIdx].length - 2;
                while (j >= 0) {//store op
                    opMap[size][aIdx][j] = (char)(t % OP_NUM);
                    t /= OP_NUM;
                    --j;
                    if (t == 0) {
                        for (; j >= 0; --j) {
                            opMap[size][aIdx][j] = (0);
                        }
                        break;
                    }

                }
                //mark it valid
                opMap[size][aIdx][opMap[size][aIdx].length - 1] = 1;
            }
        }
        /////////////////////////////////////////////////
        final int NODE_LEN = 3;
        //partition
        //0:startidx
        //1:len
        //2:layer
        int [] unprocessedStack = new int[(halfSize + 1) * NODE_LEN];
        //use the last one as idx
        int [][] processedQueue = new int[halfSize][numbers.length * NODE_LEN];
        //        int layerIdx = 0;

        //push the whole seq
        unprocessedStack[0] = 0;
        unprocessedStack[1] = numbers.length;
        //first layer is -1
        unprocessedStack[2] = -1;
        //point to the first unprocessed element
        int unprocessedStackIdx = NODE_LEN;
        //        int processedQueueIdx = 0;
        int maxLayer = 0;
        while (unprocessedStackIdx > 0) {
            //pop
            int startIdx = unprocessedStack[unprocessedStackIdx - 3];
            int len = unprocessedStack[unprocessedStackIdx - 2];
            int layer = unprocessedStack[unprocessedStackIdx - 1];
            unprocessedStackIdx -= NODE_LEN;
            if (len <= 2) {
                if (results[1][startIdx] == null) {
                    results[1][startIdx] = new int[1];
                    results[1][startIdx][0] = numbers[startIdx];
                }
                if (len == 2) {
                    if (results[1][startIdx + 1] == null) {
                        results[1][startIdx + 1] = new int[1];
                        results[1][startIdx + 1][0] = numbers[startIdx + 1];
                    }
                    if (results[2][startIdx] == null) {
                        results[2][startIdx] = new int[answerNumMap[2]];
                    }
                    int v1 = results[1][startIdx][0];
                    int v2 = results[1][startIdx + 1][0];
                    for (int opMiddle = 0, idx = 0; opMiddle < OP_NUM; ++opMiddle, ++idx) {
                        switch (opMiddle) {
                            case 0:
                                results[2][startIdx][idx] = combinedNumMap[startIdx][startIdx + 1];
                                break;
                            case 1:
                                results[2][startIdx][idx] = v1 + v2;
                                break;
                            case 2:
                                results[2][startIdx][idx] = v1 - v2;
                                break;
                        }
                    }
                }
            } else {
                int nextLayer = layer + 1;
                if (nextLayer > maxLayer) {
                    maxLayer = nextLayer;
                }
                //push to unproccesed queue
                int secondStartIdx = startIdx + (len / 2);
                int size1 = secondStartIdx - startIdx;
                int size2 = len - size1;
                //Test
                //                                System.out.println("push:");
                //                                System.out.println("idx [" + startIdx + ", " + (startIdx + size1 - 1) + "], [" + secondStartIdx + ","
                //                                        + (secondStartIdx + size2 - 1) + "]");
                //                
                //                                System.out.print("push number [" + numbers[startIdx]);
                //                                if (size1 > 1) {
                //                                    System.out.print(", " + numbers[(startIdx + size1 - 1)]);
                //                                }
                //                                System.out.print("], [" + numbers[secondStartIdx]);
                //                                if (size2 > 1) {
                //                                    System.out.print("," + numbers[(secondStartIdx + size2 - 1)]);
                //                                }
                //                                System.out.println("], layer:"+nextLayer);

                //

                //push
                unprocessedStack[unprocessedStackIdx] = secondStartIdx;
                unprocessedStack[unprocessedStackIdx + 1] = size2;
                unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
                unprocessedStackIdx += NODE_LEN;
                //
                unprocessedStack[unprocessedStackIdx] = startIdx;
                unprocessedStack[unprocessedStackIdx + 1] = size1;
                unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
                unprocessedStackIdx += NODE_LEN;
                //
                //
                //                System.out.println("----------------------------");
                int idx = processedQueue[nextLayer][processedQueue[nextLayer].length - 1];

                processedQueue[nextLayer][idx] = startIdx;
                processedQueue[nextLayer][idx + 1] = size1;
                processedQueue[nextLayer][idx + 2] = nextLayer;

                //                System.out.println(processedQueue[layerIdx][idx]);
                //                System.out.println(processedQueue[layerIdx][idx+1]);
                //                System.out.println(processedQueue[layerIdx][processedQueue[layerIdx].length - 1]);

                processedQueue[nextLayer][idx + 3] = secondStartIdx;
                processedQueue[nextLayer][idx + 4] = size2;
                processedQueue[nextLayer][idx + 5] = nextLayer;

                processedQueue[nextLayer][processedQueue[nextLayer].length - 1] += 6;

            }
        }
        System.out.println("1 prepare " + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        //////////////////////////////////////////////////////////////
        //test

        //        System.out.println("\nmerge");
        //        for (int i = maxLayer; i >= 0; --i) {
        //            //            for (int i = layerIdx - 1; i >= 0; --i) {
        //            for (int j = 0; j < processedQueue[i][processedQueue[i].length - 1]; j += NODE_LEN) {
        //                int startIdx = processedQueue[i][j];
        //                int len = processedQueue[i][j + 1];
        //                int layer = processedQueue[i][j + 2];
        //                //pop
        //                //test
        //                System.out.print("check number [" + numbers[startIdx]);
        //                if (len > 1) {
        //                    System.out.print(", " + numbers[(startIdx + len - 1)]);
        //                }
        //                System.out.println("] layer:" + layer);
        //                //
        //                //
        //                //
        //
        //            }
        //            System.out.println();
        //        }
        //
        //        System.out.println();
        //        System.out.println("processedQueueIdx:" + processedQueueIdx);
        //        long strTime = 0;
        //use queue like a stack here,
        //point to the head of the queue

        System.out.println("layer:" + maxLayer);
        //test
        int calcCount = 0;
        boolean isFinish = false;
        for (int layerIdx = maxLayer; layerIdx >= 0 && !isFinish; --layerIdx) {
            int k = 0;
            int d = NODE_LEN;
            while (d <= numbers.length && !isFinish) {
                int nextD = (d << 1);
                //            System.out.println(d);
                for (int qIdx = 0; qIdx < processedQueue[layerIdx].length - 1; qIdx += nextD) {
                    if (qIdx + d >= processedQueue[layerIdx][processedQueue[layerIdx].length - 1]) {
                        break;
                    }
                    //                    System.out.println("d:" + d + ",next d:" + nextD + ",i:" + qIdx);
                    int startIdx = processedQueue[layerIdx][qIdx];
                    int size1 = processedQueue[layerIdx][qIdx + 1];
                    int secondStartIdx = processedQueue[layerIdx][qIdx + d];
                    int size2 = processedQueue[layerIdx][qIdx + d + 1];
                    int size = size1 + size2;

                    ////////////////////////////////////
                    //test
                    System.out.print("process number [" + numbers[startIdx]);
                    if (size1 > 1) {
                        System.out.print(", " + numbers[(startIdx + size1 - 1)]);
                    }
                    System.out.print("] , [" + numbers[secondStartIdx]);
                    if (size2 > 1) {
                        System.out.print("," + numbers[(secondStartIdx + size2 - 1)]);
                    }
                    System.out.println("]");
                    //                    System.out.println("size1:" + size1 + "," + "size2:" + size2);
                    /////////////////////////////////////////////////////////////
                    //                System.out.println("startIdx:" + startIdx);
                    //                System.out.println("size:" + size);
                    //                System.out.println("i=" + qIdx);
                    //next layer op
                    char [][] opAll = null;
                    int opAllIdx = 0;
                    if (size <= halfSize) {
                        opAll = opMap[size];
                        if (results[size][startIdx] == null) {
                            results[size][startIdx] = new int[answerNumMap[size]];
                            //                            System.out.println("new results "+size);
                        }
                    }

                    processedQueue[layerIdx][qIdx] = startIdx;
                    processedQueue[layerIdx][qIdx + 1] = size;
                    int [] v1 = results[size1][startIdx];
                    int [] v2 = results[size2][secondStartIdx];

                    System.out.println(Arrays.toString(v1));
                    System.out.println(Arrays.toString(v2));

                    //                    System.out.println("op1 size "+opMap[size1].length);
                    //                    System.out.println("op2 size "+opMap[size2].length);
                    int value = Integer.MIN_VALUE;

                    //1) first part size is 1                            
                    for (int i = 0; i < v1.length; ++i) {
                        char [] op1 = opMap[size1][i];
                        if (op1[op1.length - 1] == 0) {//[invalid1]this op seq is invalid
                            if (opAll != null) {
                                opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                System.out.println("1) opAll is found invalid by op1");
                            }
                            opAllIdx += (OP_NUM * v2.length);
                            //                                                            System.out.println("1) op1[op1.length - 1] == 0");
                        } else {
                            int space1 = 0;
                            int k1 = op1.length - 2;
                            while (k1 >= 0) {
                                if (op1[k1] == 0) {
                                    ++space1;
                                    --k1;
                                } else {
                                    break;
                                }
                            }
                            //XXX not >=
                            if (space1 > maxSpaceCount) {
                                op1[op1.length - 1] = 0;
                                if (opAll != null) {
                                    opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;

                                }
                                opAllIdx += (OP_NUM * v2.length);
                                ////////////////////////////////////////
                                System.out.print("[ignore]" + combinedNumMap[op1.length - 1 - space1][op1.length - 1]);
                                System.out.print("op1 " + i + ": ");
                                for (k = 0; k < op1.length; ++k) {
                                    System.out.print((int)op1[k]);
                                }
                                System.out.println(",  " + space1 + ">=" + maxSpaceCount);

                            } else {
                                //2) middle op
                                for (int opMiddle = 0; opMiddle < OP_NUM; ++opMiddle) {
                                    for (int j = 0; j < v2.length; ++j) {
                                        char [] op2 = opMap[size2][j];
                                        if (op2[op2.length - 1] > 0) {
                                            boolean isMergedWidthBeyond = false;
                                            int space2 = 0;
                                            k1 = 0;
                                            while (k1 < op2.length - 1) {
                                                if (op2[k1] == 0) {
                                                    ++space2;
                                                    ++k1;
                                                } else {
                                                    break;
                                                }
                                            }
                                            if (space2 > maxSpaceCount) {
                                                isMergedWidthBeyond = true;
                                                op2[op2.length - 1] = 0;
                                                if (opAll != null) {
                                                    opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                                }
                                                ////////////////////////////////////////                                                
                                                System.out.print("[ignore]"
                                                        + combinedNumMap[secondStartIdx][secondStartIdx
                                                                + space2] + " , ");
                                                //                                                            System.out.println(op2.length);
                                                System.out.print("op2 " + j + ": ");
                                                for (k = 0; k < op2.length; ++k) {
                                                    System.out.print((int)op2[k]);
                                                }
                                                System.out.print(", " + space2 + ">=" + maxSpaceCount);

                                            } else
                                            {
                                                switch (opMiddle) {
                                                    case 0: {

                                                        //                                                        if (space2 > maxSpaceCount) {
                                                        //                                                           
                                                        //                                                            //                                                   
                                                        //                                                            break;
                                                        //                                                        }
                                                        //[invalid2] merged width is larger than depth
                                                        //this case only happened in last time
                                                        if (space1 + space2 + 1 > maxSpaceCount) {
                                                            isMergedWidthBeyond = true;
                                                            if (opAll != null) {
                                                                opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                                                System.out.println("4) opAll is found invalid by merge");
                                                            }
                                                            break;
                                                        }

                                                        value = v1[i] + v2[j];
                                                        //second prefix
                                                        value -= combinedNumMap[secondStartIdx][secondStartIdx
                                                                + space2];
                                                        //                                                System.out.println("MAX:"+ MAX+", width:" + (secondStartIdx
                                                        //                                                        + space2 - (secondStartIdx - 1 - space1) + 1));
                                                        if (op1.length - 2 - space1 >= 0) {//there is other op before space in first part
                                                            if (op1[op1.length - space1 - 2] == 1) {//+
                                                                //first postfix
                                                                value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                                value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                        + space2];
                                                            } else if (op1[op1.length - space1 - 2] == 2) {//-
                                                                value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                                value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                        + space2];
                                                            } else {
                                                                //                                                    System.out.println("error");
                                                            }

                                                        } else {

                                                            value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                            //may be larger than MAX width
                                                            value += combinedNumMap[startIdx][secondStartIdx + space2];
                                                        }
                                                        ++calcCount;
                                                        //                                                System.out.println(combinedNumMap[startIdx][secondStartIdx + space2]);
                                                    }
                                                        break;
                                                    case 1:
                                                        value = v1[i] + v2[j];
                                                        ++calcCount;
                                                        break;
                                                    case 2: {
                                                        //!!!!!substract, braceket...
                                                        //only first number need to change to substract
                                                        int preSpace = 0;
                                                        for (k = 0; k < op2.length - 1; ++k) {
                                                            if (op2[k] != 0) {
                                                                break;
                                                            } else {
                                                                ++preSpace;
                                                            }
                                                        }
                                                        value = v1[i]
                                                                - 2
                                                                * combinedNumMap[secondStartIdx][secondStartIdx
                                                                        + preSpace]
                                                                + v2[j];
                                                        ++calcCount;
                                                    }
                                                        break;
                                                }

                                                if (size <= halfSize) {
                                                    results[size][startIdx][opAllIdx] = value;
                                                }
                                                //construct string cost 0ms
                                                if (value == 0 && size == numbers.length && op1[op1.length - 1] > 0
                                                        && op2[op2.length - 1] > 0 && !isMergedWidthBeyond) {

                                                    //                                              System.out.println("2 prepare " + (System.currentTimeMillis() - start));  
                                                    //                                                start = System.currentTimeMillis();
                                                    if (isFirst) {
                                                        isFirst = false;
                                                    } else {
                                                        result.append(';');
                                                    }
                                                    int numIdx = 0;
                                                    //                                                System.out.println("op1:" + op1.length + ",op2:" + op2.length);
                                                    for (numIdx = 0; numIdx < op1.length - 1; ++numIdx) {
                                                        result.append(numbers[numIdx]);
                                                        //construct string
                                                        result.append(opStr[op1[numIdx]]);
                                                    }
                                                    result.append(numbers[numIdx]);
                                                    result.append(opStr[opMiddle]);
                                                    ++numIdx;
                                                    for (k = 0; k < op2.length - 1; ++k, ++numIdx) {
                                                        result.append(numbers[numIdx]);
                                                        //construct string
                                                        result.append(opStr[op2[k]]);
                                                    }
                                                    result.append(numbers[numIdx]);

                                                    //                                                start = System.currentTimeMillis();
                                                    //                                                strTime += (System.currentTimeMillis() - start);

                                                }
                                            }
                                            //                                        else{
                                            //                                            System.out.println("op2 is invalid");
                                            //                                        }
                                            ++opAllIdx;
                                        }
                                    }
                                }
                            }

                        }
                    }
                    if (size == numbers.length) {
                        isFinish = true;
                        //                        System.out.println("isfinish");
                        break;
                    }
                }

                d = nextD;
            }
        }
        //        System.out.println("construct string time: " + strTime + " ms");
        System.out.println("2 calc and output " + (System.currentTimeMillis() - start));
        System.out.println("calc count:" + calcCount);
        //                start = System.currentTimeMillis();
        if (result.length() == 0) {
            return "NOTFOUND";
        }
        return result.toString();
    }

    //correct130323
    public static String run7_4(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        long start = System.currentTimeMillis();
        StringBuilder result = null;
        int [] numbers = null;

        //        int TOTAL_DIGIT_NUM = 0;
        int maxSpaceCount = 0;

        //        int maxValueFilter = 0;
        //        int MAX = 0;
        char ch = arg.charAt(0);
        //        int [][] combinedNumDigitMap = null;
        if (ch == 'S') {
            //write a table contains 3~15???
            int n = 0;
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            //3<=N<=15
            if (n < 3 || n > 15) {
                return "ILLEGAL";
            }
            maxSpaceCount = n / 2 - 1;
            if (maxSpaceCount > n / 10 + 2) {
                maxSpaceCount = n / 10 + 2;
            }
            //calc digit data
            //            if (n < 10) {
            //                TOTAL_DIGIT_NUM = n;
            //            } else {
            //                TOTAL_DIGIT_NUM = (n - 10 + 1) * 2 + (10 - 1);
            //            }
            //            maxDigitNum = TOTAL_DIGIT_NUM / 2;

            //            combinedNumDigitMap = new int[n][n];
            numbers = new int[n];
            for (int i = 0; i < numbers.length; ++i) {
                numbers[i] = i + 1;
            }

            ///////////////////////////////////////////
            //various buffer size
            result = new StringBuilder(2 << n);
            //            MAX = n / 2 + 1;
        } else if (ch == 'R') {
            String asdStr = arg.substring(1);
            String [] numberStr = asdStr.split(",");
            numbers = new int[numberStr.length];
            int pre = Integer.MIN_VALUE;
            try {
                for (int i = 0; i < numbers.length; ++i) {
                    if (numberStr[i].isEmpty() || numberStr[i].charAt(0) > '9') {
                        //not a number
                        return "ILLEGAL";
                    }
                    numbers[i] = Integer.parseInt(numberStr[i]);
                    if (numbers[i] < 0 || numbers[i] < pre) {
                        //not larger than previous
                        return "ILLEGAL";
                    }
                    pre = numbers[i];
                }
            } catch (Exception e) {
                return "ILLEGAL";
            }

            result = new StringBuilder(64);

            ///////////////////////////////////////////
            //calc digit data
            //            if (n < 10) {
            //                TOTAL_DIGIT_NUM = n;
            //            } else {
            //                TOTAL_DIGIT_NUM = (n - 10 + 1) * 2 + (10 - 1);
            //            }
            //            maxDigitNum = TOTAL_DIGIT_NUM / 2;

            //            MAX = numberStr.length / 2 + 1;
            //            MAX = (numberStr.length * numberStr[numberStr.length - 1].length()) / 2 + 1;
            //            if (MAX < numbers.length - 1) {
            //                MAX = numbers.length - 1;
            //            }
            //TODO
            maxSpaceCount = 3;
            //            maxSpaceCount = ((numbers.length % 2) == 0) ? numbers.length >> 1 : (numbers.length >> 1)+1;
        } else {
            return "ILLEGAL";
        }
        ///////////////////////////////////////////
        //row:startIdx
        //column:endIdx
        //value:the total digit of the number constructed from startidx to endidx
        //if value is zero, it means invalid startidx and endidx, or beyond max digit
        int [][] combinedNumDigitMap = new int[numbers.length][numbers.length];
        int w = 10;
        int digit = 1;
        int maxDigitNum = 0;
        for (int i = 0; i < numbers.length; ++i) {
            digit = 1;
            w = 10;
            while (true) {
                if (numbers[i] / w == 0) {
                    combinedNumDigitMap[i][i] = digit;
                    break;
                }
                w *= 10;
                ++digit;
            }
            maxDigitNum += digit;
        }
        maxDigitNum >>= 1;
        //        System.out.println("Total digit num:" + TOTAL_DIGIT_NUM);
        System.out.println("Max digit num:" + maxDigitNum);

        //        final int 
        System.out.println("MAX_SPACE_COUNT:" + maxSpaceCount);
        if (maxDigitNum > 15) {
            return "ILLEGAL";
        }

        final int maxIntLen = 9;
        int halfSize = ((numbers.length % 2) == 0) ? numbers.length >> 1 : (numbers.length >> 1) + 1;
        System.out.println("halfSize：" + halfSize);
        //        int maxTreePath = 1;
        //        int w = numbers.length;
        //        while (w > 0) {
        //            w >>= 1;
        //            ++maxTreePath;
        //        }
        //        System.out.println("maxTreePath:" + maxTreePath);

        final char [] opStr = {' ', '+', '-'};
        final int OP_NUM = 3;
        boolean isFirst = true;
        for (int i = 0; i < numbers.length; ++i) {
            for (int j = i + 1; j < numbers.length; ++j) {
                combinedNumDigitMap[i][j] = combinedNumDigitMap[j][j] + combinedNumDigitMap[i][j - 1];
                //TODO
                if (combinedNumDigitMap[i][j] > maxIntLen) {
                    combinedNumDigitMap[i][j] = 0;
                    break;
                }
            }
            //            System.out.println(Arrays.toString(combinedNumDigitMap[i]));
        }
        //cache large number
        //row: startNum
        //column:endNum
        //save the index of number
        int [][] combinedNumMap = new int[numbers.length][numbers.length];
        for (int i = 0; i < numbers.length; ++i) {
            combinedNumMap[i][i] = numbers[i];
            for (int j = i + 1; j < numbers.length; ++j) {
                //TODO
                if (combinedNumDigitMap[i][j] > 0 && combinedNumDigitMap[i][j] < maxIntLen) {
                    int c = combinedNumDigitMap[j][j];
                    w = 10;
                    while (c > 1) {
                        w *= 10;
                        --c;
                    }
                    combinedNumMap[i][j] = combinedNumMap[i][j - 1] * w + numbers[j];
                    //                    if (combinedNumDigitMap[i][j] == maxSpaceCount + 1) {
                    //                        if (combinedNumMap[i][j] > maxValueFilter) {
                    //                            maxValueFilter = combinedNumMap[i][j];
                    //                        }
                    //                    }
                } else {
                    break;
                }
            }
            //            System.out.println(Arrays.toString(combinedNumMap[i]));
        }
        //        System.out.println("maxValueFilter:" + maxValueFilter);
        //the set size:1~half total size
        int [] answerNumMap = new int[halfSize + 1];
        for (int size = 1, answerNum = 1; size < answerNumMap.length; ++size, answerNum *= OP_NUM) {
            answerNumMap[size] = answerNum;
        }
        //////////////////////////////////
        //row: the idx is the size of set, the size of f(3,5,6,9) is 4, map[4][][]
        //column:start idx
        //element:value, order by ' ',+,-
        //if the value is not illegal, make sure it won't be used

        //decrease the number
        int [][][] results = new int[halfSize + 1][numbers.length][];
        //initial the single value
        for (int startIdx = 0; startIdx < numbers.length; ++startIdx) {
            results[1][startIdx] = new int[1];
            results[1][startIdx][0] = numbers[startIdx];
        }
        //        System.out.println("1 input " + (System.currentTimeMillis() - start));
        //        start = System.currentTimeMillis();
        /////////////////////////////////////////////////////////////////

        //store op seq for each answer map
        //row:size
        //column: answer index
        //element:op 
        //!!!!pay attention out of memory...
        //the space count won't larger than MAX_TREE_DEPTH for single set
        //except merged set
        char [][][] opMap = new char[halfSize + 1][][];
        for (int size = 1; size < opMap.length; ++size) {
            //last one stores if the op is valid!!!!
            opMap[size] = new char[answerNumMap[size]][size];
            if (opMap[size][0].length - 1 <= halfSize) {
                opMap[size][0][opMap[size][0].length - 1] = 1;
            }
            //by default, it invalid
            for (int aIdx = 1; aIdx < opMap[size].length; ++aIdx) {
                int t = aIdx;
                int j = opMap[size][aIdx].length - 2;
                w = 0;
                while (j >= 0) {//store op
                    opMap[size][aIdx][j] = (char)(t % OP_NUM);
                    t /= OP_NUM;
                    --j;
                    if (t == 0) {
                        for (; j >= 0; --j) {
                            opMap[size][aIdx][j] = (0);
                        }
                        break;
                    }

                }
                //mark it valid
                opMap[size][aIdx][opMap[size][aIdx].length - 1] = 1;
            }
        }
        /////////////////////////////////////////////////
        final int NODE_LEN = 3;
        //partition
        //0:startidx
        //1:len
        //2:layer
        int [] unprocessedStack = new int[(halfSize + 1) * NODE_LEN];
        //use the last one as idx
        int [][] processedQueue = new int[halfSize][numbers.length * NODE_LEN];
        //        int layerIdx = 0;

        //push the whole seq
        unprocessedStack[0] = 0;
        unprocessedStack[1] = numbers.length;
        //first layer is -1
        unprocessedStack[2] = -1;
        //point to the first unprocessed element
        int unprocessedStackIdx = NODE_LEN;
        //        int processedQueueIdx = 0;
        int maxLayer = 0;
        while (unprocessedStackIdx > 0) {
            //pop
            int startIdx = unprocessedStack[unprocessedStackIdx - 3];
            int len = unprocessedStack[unprocessedStackIdx - 2];
            int layer = unprocessedStack[unprocessedStackIdx - 1];
            unprocessedStackIdx -= NODE_LEN;
            if (len <= 2) {
                if (results[1][startIdx] == null) {
                    results[1][startIdx] = new int[1];
                    results[1][startIdx][0] = numbers[startIdx];
                }
                if (len == 2) {
                    if (results[1][startIdx + 1] == null) {
                        results[1][startIdx + 1] = new int[1];
                        results[1][startIdx + 1][0] = numbers[startIdx + 1];
                    }
                    if (results[2][startIdx] == null) {
                        results[2][startIdx] = new int[answerNumMap[2]];
                    }
                    int v1 = results[1][startIdx][0];
                    int v2 = results[1][startIdx + 1][0];
                    for (int opMiddle = 0, idx = 0; opMiddle < OP_NUM; ++opMiddle, ++idx) {
                        switch (opMiddle) {
                            case 0:
                                results[2][startIdx][idx] = combinedNumMap[startIdx][startIdx + 1];
                                break;
                            case 1:
                                results[2][startIdx][idx] = v1 + v2;
                                break;
                            case 2:
                                results[2][startIdx][idx] = v1 - v2;
                                break;
                        }
                    }
                }
            } else {
                int nextLayer = layer + 1;
                if (nextLayer > maxLayer) {
                    maxLayer = nextLayer;
                }
                //push to unproccesed queue
                int secondStartIdx = startIdx + (len / 2);
                int size1 = secondStartIdx - startIdx;
                int size2 = len - size1;
                //Test
                //                                System.out.println("push:");
                //                                System.out.println("idx [" + startIdx + ", " + (startIdx + size1 - 1) + "], [" + secondStartIdx + ","
                //                                        + (secondStartIdx + size2 - 1) + "]");
                //                
                //                                System.out.print("push number [" + numbers[startIdx]);
                //                                if (size1 > 1) {
                //                                    System.out.print(", " + numbers[(startIdx + size1 - 1)]);
                //                                }
                //                                System.out.print("], [" + numbers[secondStartIdx]);
                //                                if (size2 > 1) {
                //                                    System.out.print("," + numbers[(secondStartIdx + size2 - 1)]);
                //                                }
                //                                System.out.println("], layer:"+nextLayer);

                //

                //push
                unprocessedStack[unprocessedStackIdx] = secondStartIdx;
                unprocessedStack[unprocessedStackIdx + 1] = size2;
                unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
                unprocessedStackIdx += NODE_LEN;
                //
                unprocessedStack[unprocessedStackIdx] = startIdx;
                unprocessedStack[unprocessedStackIdx + 1] = size1;
                unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
                unprocessedStackIdx += NODE_LEN;
                //
                //
                //                System.out.println("----------------------------");
                int idx = processedQueue[nextLayer][processedQueue[nextLayer].length - 1];

                processedQueue[nextLayer][idx] = startIdx;
                processedQueue[nextLayer][idx + 1] = size1;
                processedQueue[nextLayer][idx + 2] = nextLayer;

                //                System.out.println(processedQueue[layerIdx][idx]);
                //                System.out.println(processedQueue[layerIdx][idx+1]);
                //                System.out.println(processedQueue[layerIdx][processedQueue[layerIdx].length - 1]);

                processedQueue[nextLayer][idx + 3] = secondStartIdx;
                processedQueue[nextLayer][idx + 4] = size2;
                processedQueue[nextLayer][idx + 5] = nextLayer;

                processedQueue[nextLayer][processedQueue[nextLayer].length - 1] += 6;

            }
        }
        System.out.println("1 prepare " + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        //////////////////////////////////////////////////////////////
        //test

        //        System.out.println("\nmerge");
        //        for (int i = maxLayer; i >= 0; --i) {
        //            //            for (int i = layerIdx - 1; i >= 0; --i) {
        //            for (int j = 0; j < processedQueue[i][processedQueue[i].length - 1]; j += NODE_LEN) {
        //                int startIdx = processedQueue[i][j];
        //                int len = processedQueue[i][j + 1];
        //                int layer = processedQueue[i][j + 2];
        //                //pop
        //                //test
        //                System.out.print("check number [" + numbers[startIdx]);
        //                if (len > 1) {
        //                    System.out.print(", " + numbers[(startIdx + len - 1)]);
        //                }
        //                System.out.println("] layer:" + layer);
        //                //
        //                //
        //                //
        //
        //            }
        //            System.out.println();
        //        }
        //
        //        System.out.println();
        //        System.out.println("processedQueueIdx:" + processedQueueIdx);
        //        long strTime = 0;
        //use queue like a stack here,
        //point to the head of the queue

        //        System.out.println("layer:" + maxLayer);
        //test
        int calcCount = 0;
        boolean isFinish = false;
        for (int layerIdx = maxLayer; layerIdx >= 0 && !isFinish; --layerIdx) {
            int k = 0;
            int d = NODE_LEN;
            while (d <= numbers.length && !isFinish) {
                int nextD = (d << 1);
                //            System.out.println(d);
                for (int qIdx = 0; qIdx < processedQueue[layerIdx].length - 1; qIdx += nextD) {
                    if (qIdx + d >= processedQueue[layerIdx][processedQueue[layerIdx].length - 1]) {
                        break;
                    }
                    //                    System.out.println("d:" + d + ",next d:" + nextD + ",i:" + qIdx);
                    int startIdx = processedQueue[layerIdx][qIdx];
                    int size1 = processedQueue[layerIdx][qIdx + 1];
                    int secondStartIdx = processedQueue[layerIdx][qIdx + d];
                    int size2 = processedQueue[layerIdx][qIdx + d + 1];
                    int size = size1 + size2;

                    ////////////////////////////////////
                    //test
                    //                    System.out.print("process number [" + numbers[startIdx]);
                    //                    if (size1 > 1) {
                    //                        System.out.print(", " + numbers[(startIdx + size1 - 1)]);
                    //                    }
                    //                    System.out.print("] , [" + numbers[secondStartIdx]);
                    //                    if (size2 > 1) {
                    //                        System.out.print("," + numbers[(secondStartIdx + size2 - 1)]);
                    //                    }
                    //                    System.out.println("]");
                    //                    System.out.println("size1:" + size1 + "," + "size2:" + size2);
                    /////////////////////////////////////////////////////////////
                    //                System.out.println("startIdx:" + startIdx);
                    //                System.out.println("size:" + size);
                    //                System.out.println("i=" + qIdx);
                    //next layer op
                    char [][] opAll = null;
                    int opAllIdx = 0;
                    if (size <= halfSize) {
                        opAll = opMap[size];
                        if (results[size][startIdx] == null) {
                            results[size][startIdx] = new int[answerNumMap[size]];
                            //                            System.out.println("new results "+size);
                        }
                    }

                    processedQueue[layerIdx][qIdx] = startIdx;
                    processedQueue[layerIdx][qIdx + 1] = size;
                    int [] v1 = results[size1][startIdx];
                    int [] v2 = results[size2][secondStartIdx];

                    //                    System.out.println(Arrays.toString(v1));
                    //                    System.out.println(Arrays.toString(v2));

                    //                    System.out.println("op1 size "+opMap[size1].length);
                    //                    System.out.println("op2 size "+opMap[size2].length);
                    int value = -1;

                    //1) first part size is 1                            
                    for (int i = 0; i < v1.length; ++i) {
                        char [] op1 = opMap[size1][i];
                        if (op1[op1.length - 1] == 0) {//[invalid1]this op seq is invalid
                            if (opAll != null) {
                                opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                //                                System.out.println("1) opAll is found invalid by op1");
                            }
                            opAllIdx += (OP_NUM * v2.length);
                            //                                                            System.out.println("1) op1[op1.length - 1] == 0");
                        } else {
                            int space1 = 0;
                            int k1 = op1.length - 2;
                            while (k1 >= 0) {
                                if (op1[k1] == 0) {
                                    ++space1;
                                    --k1;
                                } else {
                                    break;
                                }
                            }

                            //2) middle op
                            for (int opMiddle = 0; opMiddle < OP_NUM; ++opMiddle) {
                                for (int j = 0; j < v2.length; ++j) {
                                    char [] op2 = opMap[size2][j];
                                    if (op2[op2.length - 1] > 0) {
                                        boolean isMergedWidthBeyond = false;
                                        switch (opMiddle) {
                                            case 0: {
                                                int space2 = 0;
                                                k1 = 0;
                                                while (k1 < op2.length - 1) {
                                                    if (op2[k1] == 0) {
                                                        ++space2;
                                                        ++k1;
                                                    } else {
                                                        break;
                                                    }
                                                }
                                                //                                                        if (space2 > maxSpaceCount) {
                                                //                                                           
                                                //                                                            //                                                   
                                                //                                                            break;
                                                //                                                        }
                                                //[invalid2] merged width is larger than depth
                                                //this case only happened in last time
                                                if (space1 + space2 + 1 > maxSpaceCount) {
                                                    isMergedWidthBeyond = true;
                                                    if (opAll != null) {//it's null for last layer
                                                        opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                                        //                                                        System.out.println("opAll[" + size + "][" + opAllIdx
                                                        //                                                                + "]is found invalid by merge");
                                                    }
                                                    break;
                                                }

                                                value = v1[i] + v2[j];
                                                //second prefix
                                                value -= combinedNumMap[secondStartIdx][secondStartIdx
                                                        + space2];
                                                //                                                System.out.println("MAX:"+ MAX+", width:" + (secondStartIdx
                                                //                                                        + space2 - (secondStartIdx - 1 - space1) + 1));
                                                if (op1.length - 2 - space1 >= 0) {//there is other op before space in first part
                                                    if (op1[op1.length - space1 - 2] == 1) {//+
                                                        //first postfix
                                                        value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                        value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                + space2];
                                                    } else if (op1[op1.length - space1 - 2] == 2) {//-
                                                        value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                        value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                + space2];
                                                    } else {
                                                        //                                                    System.out.println("error");
                                                    }

                                                } else {

                                                    value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                    //may be larger than MAX width
                                                    value += combinedNumMap[startIdx][secondStartIdx + space2];
                                                }
                                                //XXX
                                                //                                                if(value>maxValueFilter){
                                                //                                                    isMergedWidthBeyond = true;
                                                //                                                    
                                                //                                                    if (opAll != null) {//it's null for last layer
                                                //                                                        opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                                //                                                        System.out.println(value+" is larger than "+maxValueFilter);
                                                //                                                    }
                                                //                                                    break;
                                                //                                                }
                                                ++calcCount;
                                                //                                                System.out.println(combinedNumMap[startIdx][secondStartIdx + space2]);
                                            }
                                                break;
                                            case 1:
                                                value = v1[i] + v2[j];
                                                ++calcCount;
                                                break;
                                            case 2: {
                                                //!!!!!substract, braceket...
                                                //only first number need to change to substract
                                                int preSpace = 0;
                                                for (k = 0; k < op2.length - 1; ++k) {
                                                    if (op2[k] != 0) {
                                                        break;
                                                    } else {
                                                        ++preSpace;
                                                    }
                                                }
                                                value = v1[i]
                                                        - 2
                                                        * combinedNumMap[secondStartIdx][secondStartIdx
                                                                + preSpace]
                                                        + v2[j];
                                                ++calcCount;
                                            }
                                                break;
                                        }

                                        if (size <= halfSize) {
                                            results[size][startIdx][opAllIdx] = value;
                                        }
                                        //construct string cost 0ms
                                        if (value == 0 && size == numbers.length && op1[op1.length - 1] > 0
                                                && op2[op2.length - 1] > 0 && !isMergedWidthBeyond) {

                                            //                                              System.out.println("2 prepare " + (System.currentTimeMillis() - start));  
                                            //                                                start = System.currentTimeMillis();
                                            if (isFirst) {
                                                isFirst = false;
                                            } else {
                                                result.append(';');
                                            }
                                            int numIdx = 0;
                                            //                                                System.out.println("op1:" + op1.length + ",op2:" + op2.length);
                                            for (numIdx = 0; numIdx < op1.length - 1; ++numIdx) {
                                                result.append(numbers[numIdx]);
                                                //construct string
                                                result.append(opStr[op1[numIdx]]);
                                            }
                                            result.append(numbers[numIdx]);
                                            result.append(opStr[opMiddle]);
                                            ++numIdx;
                                            for (k = 0; k < op2.length - 1; ++k, ++numIdx) {
                                                result.append(numbers[numIdx]);
                                                //construct string
                                                result.append(opStr[op2[k]]);
                                            }
                                            result.append(numbers[numIdx]);

                                            //                                                start = System.currentTimeMillis();
                                            //                                                strTime += (System.currentTimeMillis() - start);

                                        }
                                        //                                            }
                                    }
                                    //                                        else{
                                    //                                            System.out.println("op2 is invalid");
                                    //                                        }
                                    ++opAllIdx;
                                }
                            }
                        }
                    }
                    if (size == numbers.length) {
                        isFinish = true;
                        //                        System.out.println("isfinish");
                        break;
                    }
                }

                d = nextD;
            }
        }
        //        System.out.println("construct string time: " + strTime + " ms");
        System.out.println("2 calc and output " + (System.currentTimeMillis() - start));
        System.out.println("calc count:" + calcCount);
        //                start = System.currentTimeMillis();
        if (result.length() == 0) {
            return "NOTFOUND";
        }
        return result.toString();
    }

    public static String run7_5(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        //        long start = System.currentTimeMillis();
        StringBuilder result = null;
        int [] numbers = null;
        //        int maxValueFilter = 0;
        char ch = arg.charAt(0);
        if (ch == 'S') {
            int n = 0;
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            if (n < 3 || n > 15) {
                return "ILLEGAL";
            }
            numbers = new int[n];
            for (int i = 0; i < numbers.length; ++i) {
                numbers[i] = i + 1;
            }

            ///////////////////////////////////////////
            //various buffer size
            result = new StringBuilder(2 << n);
        } else if (ch == 'R') {
            String asdStr = arg.substring(1);
            String [] numberStr = asdStr.split(",");
            numbers = new int[numberStr.length];
            int pre = Integer.MIN_VALUE;
            try {
                for (int i = 0; i < numbers.length; ++i) {
                    if (numberStr[i].isEmpty() || numberStr[i].charAt(0) > '9') {
                        //not a number
                        return "ILLEGAL";
                    }
                    numbers[i] = Integer.parseInt(numberStr[i]);
                    if (numbers[i] < 0 || numbers[i] < pre) {
                        //not larger than previous
                        return "ILLEGAL";
                    }
                    pre = numbers[i];
                }
            } catch (Exception e) {
                return "ILLEGAL";
            }

            result = new StringBuilder(64);

        } else {
            return "ILLEGAL";
        }
        ///////////////////////////////////////////
        //row:startIdx
        //column:endIdx
        //value:the total digit of the number constructed from startidx to endidx
        //if value is zero, it means invalid startidx and endidx, or beyond max digit
        int [][] combinedNumDigitMap = new int[numbers.length][numbers.length];
        int w = 10;
        int digit = 1;
        int maxDigitNum = 0;
        for (int i = 0; i < numbers.length; ++i) {
            digit = 1;
            w = 10;
            while (true) {
                if (numbers[i] / w == 0) {
                    combinedNumDigitMap[i][i] = digit;
                    break;
                }
                w *= 10;
                ++digit;
            }
            maxDigitNum += digit;
        }
        maxDigitNum >>= 1;
        //        System.out.println("Total digit num:" + TOTAL_DIGIT_NUM);
        //        System.out.println("Max digit num:" + maxDigitNum);

        if (maxDigitNum > 15) {//too large
            return "ILLEGAL";
        }

        int halfSize = ((numbers.length % 2) == 0) ? numbers.length >> 1 : (numbers.length >> 1) + 1;
        //        System.out.println("halfSize：" + halfSize);
        //        int maxTreePath = 1;
        //        int w = numbers.length;
        //        while (w > 0) {
        //            w >>= 1;
        //            ++maxTreePath;
        //        }
        //        System.out.println("maxTreePath:" + maxTreePath);

        final char [] opStr = {' ', '+', '-'};
        final int OP_NUM = 3;
        boolean isFirst = true;
        //      row:startidx
        //      column:digit
        //value:spacecount
        int [][] combinedNumSpaceMap = new int[numbers.length][maxDigitNum + 1];
        //cache large number
        //
        //row: startNum
        //column:endNum
        //save the index of number
        int [][] combinedNumMap = new int[numbers.length][numbers.length];
        for (int i = 0; i < numbers.length; ++i) {
            combinedNumMap[i][i] = numbers[i];
            for (int j = i + 1; j < numbers.length; ++j) {
                //1) update combined num digit map
                combinedNumDigitMap[i][j] = combinedNumDigitMap[j][j] + combinedNumDigitMap[i][j - 1];
                if (combinedNumDigitMap[i][j] > maxDigitNum) {
                    combinedNumDigitMap[i][j] = 0;
                    break;
                }
                ////////////////////////////////////////////////////////
                int c = combinedNumDigitMap[j][j];
                w = 10;
                while (c > 1) {
                    w *= 10;
                    --c;
                }
                //2)update combined num map 
                combinedNumMap[i][j] = combinedNumMap[i][j - 1] * w + numbers[j];

                //3)count space number
                combinedNumSpaceMap[i][combinedNumDigitMap[i][j]] = j - i;
            }
            //                        System.out.println(Arrays.toString(combinedNumDigitMap[i]));
        }
        //        System.out.println();

        //        System.out.println("maxValueFilter:" + maxValueFilter);
        //the set size:1~half total size
        int [] answerNumMap = new int[halfSize + 1];
        for (int size = 1, answerNum = 1; size < answerNumMap.length; ++size, answerNum *= OP_NUM) {
            answerNumMap[size] = answerNum;
        }
        //////////////////////////////////
        //row: the idx is the size of set, the size of f(3,5,6,9) is 4, map[4][][]
        //column:start idx
        //element:value, order by ' ',+,-
        //if the value is not illegal, make sure it won't be used
        int [][][] results = new int[halfSize + 1][numbers.length][];
        //initial the single value
        for (int startIdx = 0; startIdx < numbers.length; ++startIdx) {
            results[1][startIdx] = new int[1];
            results[1][startIdx][0] = numbers[startIdx];
        }
        //        System.out.println("1 input " + (System.currentTimeMillis() - start));
        //        start = System.currentTimeMillis();
        /////////////////////////////////////////////////////////////////

        //store op seq for each answer map
        //row:size
        //column: answer index
        //element:op 
        //!!!!pay attention out of memory...
        //the space count won't larger than MAX_TREE_DEPTH for single set
        //except merged set
        char [][][] opMap = new char[halfSize + 1][][];
        for (int size = 1; size < opMap.length; ++size) {
            //last one stores if the op is valid!!!!
            opMap[size] = new char[answerNumMap[size]][size];
            if (opMap[size][0].length - 1 <= halfSize) {
                opMap[size][0][opMap[size][0].length - 1] = 1;
            }
            //by default, it invalid
            for (int aIdx = 1; aIdx < opMap[size].length; ++aIdx) {
                int t = aIdx;
                int j = opMap[size][aIdx].length - 2;
                w = 0;
                while (j >= 0) {//store op
                    opMap[size][aIdx][j] = (char)(t % OP_NUM);
                    t /= OP_NUM;
                    --j;
                    if (t == 0) {
                        for (; j >= 0; --j) {
                            opMap[size][aIdx][j] = (0);
                        }
                        break;
                    }
                }
                //mark it valid
                opMap[size][aIdx][opMap[size][aIdx].length - 1] = 1;
            }
        }
        /////////////////////////////////////////////////
        final int NODE_LEN = 3;
        //partition
        //0:startidx
        //1:len
        //2:layer
        int [] unprocessedStack = new int[(halfSize + 1) * NODE_LEN];
        //use the last one as idx
        int [][] processedQueue = new int[halfSize][numbers.length * NODE_LEN];
        //push the whole seq
        unprocessedStack[0] = 0;
        unprocessedStack[1] = numbers.length;
        //first layer is -1
        unprocessedStack[2] = -1;
        //point to the first unprocessed element
        int unprocessedStackIdx = NODE_LEN;
        //        int processedQueueIdx = 0;
        int maxLayer = 0;
        while (unprocessedStackIdx > 0) {
            //pop
            int startIdx = unprocessedStack[unprocessedStackIdx - 3];
            int len = unprocessedStack[unprocessedStackIdx - 2];
            int layer = unprocessedStack[unprocessedStackIdx - 1];
            unprocessedStackIdx -= NODE_LEN;
            if (len <= 2) {
                if (results[1][startIdx] == null) {
                    results[1][startIdx] = new int[1];
                    results[1][startIdx][0] = numbers[startIdx];
                }
                if (len == 2) {
                    if (results[1][startIdx + 1] == null) {
                        results[1][startIdx + 1] = new int[1];
                        results[1][startIdx + 1][0] = numbers[startIdx + 1];
                    }
                    if (results[2][startIdx] == null) {
                        results[2][startIdx] = new int[answerNumMap[2]];
                    }
                    int v1 = results[1][startIdx][0];
                    int v2 = results[1][startIdx + 1][0];
                    for (int opMiddle = 0, idx = 0; opMiddle < OP_NUM; ++opMiddle, ++idx) {
                        switch (opMiddle) {
                            case 0:
                                results[2][startIdx][idx] = combinedNumMap[startIdx][startIdx + 1];
                                break;
                            case 1:
                                results[2][startIdx][idx] = v1 + v2;
                                break;
                            case 2:
                                results[2][startIdx][idx] = v1 - v2;
                                break;
                        }
                    }
                }
            } else {
                int nextLayer = layer + 1;
                if (nextLayer > maxLayer) {
                    maxLayer = nextLayer;
                }
                //push to unproccesed queue
                int secondStartIdx = startIdx + (len / 2);
                int size1 = secondStartIdx - startIdx;
                int size2 = len - size1;
                //push
                unprocessedStack[unprocessedStackIdx] = secondStartIdx;
                unprocessedStack[unprocessedStackIdx + 1] = size2;
                unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
                unprocessedStackIdx += NODE_LEN;
                //
                unprocessedStack[unprocessedStackIdx] = startIdx;
                unprocessedStack[unprocessedStackIdx + 1] = size1;
                unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
                unprocessedStackIdx += NODE_LEN;
                //
                int idx = processedQueue[nextLayer][processedQueue[nextLayer].length - 1];

                processedQueue[nextLayer][idx] = startIdx;
                processedQueue[nextLayer][idx + 1] = size1;
                processedQueue[nextLayer][idx + 2] = nextLayer;
                //
                processedQueue[nextLayer][idx + 3] = secondStartIdx;
                processedQueue[nextLayer][idx + 4] = size2;
                processedQueue[nextLayer][idx + 5] = nextLayer;

                processedQueue[nextLayer][processedQueue[nextLayer].length - 1] += 6;

            }
        }
        //        System.out.println("1 prepare " + (System.currentTimeMillis() - start));
        //        start = System.currentTimeMillis();

        //use queue like a stack here,
        //point to the head of the queue
        //        System.out.println("layer:" + maxLayer);
        //test
        //        int calcCount = 0;
        boolean isFinish = false;
        int value = -1;

        char [] op1 = null;
        char [] op2 = null;
        int [] v1 = null;
        int [] v2 = null;
        int d = 0;
        int k = 0;
        int space1 = 0;
        int space2 = 0;
        for (int layerIdx = maxLayer; layerIdx >= 0 && !isFinish; --layerIdx) {
            d = NODE_LEN;
            while (d <= numbers.length && !isFinish) {
                int nextD = (d << 1);
                //            System.out.println(d);
                for (int qIdx = 0; qIdx < processedQueue[layerIdx].length - 1; qIdx += nextD) {
                    if (qIdx + d >= processedQueue[layerIdx][processedQueue[layerIdx].length - 1]) {
                        break;
                    }
                    //                    System.out.println("d:" + d + ",next d:" + nextD + ",i:" + qIdx);
                    int startIdx = processedQueue[layerIdx][qIdx];
                    int size1 = processedQueue[layerIdx][qIdx + 1];
                    int secondStartIdx = processedQueue[layerIdx][qIdx + d];
                    int size2 = processedQueue[layerIdx][qIdx + d + 1];
                    int size = size1 + size2;

                    //next layer op
                    char [][] opAll = null;
                    int opAllIdx = 0;
                    if (size <= halfSize) {
                        opAll = opMap[size];
                        if (results[size][startIdx] == null) {
                            results[size][startIdx] = new int[answerNumMap[size]];
                        }
                    }

                    processedQueue[layerIdx][qIdx] = startIdx;
                    processedQueue[layerIdx][qIdx + 1] = size;
                    v1 = results[size1][startIdx];
                    v2 = results[size2][secondStartIdx];
                    System.out.print("merge set [" + numbers[startIdx]);
                    if (size1 > 1) {
                        System.out.print(", " + numbers[(startIdx + size1 - 1)]);
                    }
                    System.out.print("], [" + numbers[secondStartIdx]);
                    if (size2 > 1) {
                        System.out.print("," + numbers[(secondStartIdx + size2 - 1)]);
                    }
                    System.out.println("]");
                    System.out.println(Arrays.toString(v1));
                    System.out.println(Arrays.toString(v2));

                    //                    System.out.println("op1 size "+opMap[size1].length);
                    //                    System.out.println("op2 size "+opMap[size2].length);
                    value = -1;

                    //1) first part size is 1                            
                    for (int i = 0; i < v1.length; ++i) {
                        op1 = opMap[size1][i];
                        if (op1[op1.length - 1] == 0) {//[invalid1]this op seq is invalid
                            if (opAll != null) {
                                opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                //                                System.out.println("1) opAll is found invalid by op1");
                            }
                            opAllIdx += (OP_NUM * v2.length);
                            //                                                            System.out.println("1) op1[op1.length - 1] == 0");
                        } else {
                            space1 = 0;
                            k = op1.length - 2;
                            while (k >= 0) {
                                if (op1[k] == 0) {
                                    ++space1;
                                    --k;
                                } else {
                                    break;
                                }
                            }

                            //2) middle op
                            for (int opMiddle = 0; opMiddle < OP_NUM; ++opMiddle) {
                                for (int j = 0; j < v2.length; ++j) {
                                    op2 = opMap[size2][j];
                                    if (op2[op2.length - 1] > 0) {
                                        boolean isMergedWidthBeyond = false;
                                        switch (opMiddle) {
                                            case 0: {
                                                space2 = 0;
                                                k = 0;
                                                while (k < op2.length - 1) {
                                                    if (op2[k] == 0) {
                                                        ++space2;
                                                        ++k;
                                                    } else {
                                                        break;
                                                    }
                                                }
                                                //                                                
                                                //[invalid] merged width is larger than depth
                                                //this case only happened in last time
                                                if (combinedNumSpaceMap[secondStartIdx - 1 - space1][combinedNumDigitMap[secondStartIdx
                                                        - 1 - space1][secondStartIdx
                                                        + space2]] == 0) {
                                                    //                                                if (space1 + space2 + 1 > maxSpaceCount) {
                                                    isMergedWidthBeyond = true;
                                                    if (opAll != null) {//it's null for last layer
                                                        opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                                        //                                                        System.out.println("opAll[" + size + "][" + opAllIdx
                                                        //                                                                + "]is found invalid by merge");
                                                    }
                                                    break;
                                                }

                                                value = v1[i] + v2[j];
                                                //second prefix
                                                value -= combinedNumMap[secondStartIdx][secondStartIdx
                                                        + space2];
                                                //                                                System.out.println("MAX:"+ MAX+", width:" + (secondStartIdx
                                                //                                                        + space2 - (secondStartIdx - 1 - space1) + 1));
                                                if (op1.length - 2 - space1 >= 0) {//there is other op before space in first part
                                                    if (op1[op1.length - space1 - 2] == 1) {//+
                                                        //first postfix
                                                        value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                        value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                + space2];
                                                    } else if (op1[op1.length - space1 - 2] == 2) {//-
                                                        value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                        value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                + space2];
                                                    } else {
                                                        //                                                    System.out.println("error");
                                                    }

                                                } else {

                                                    value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                    //may be larger than MAX width
                                                    value += combinedNumMap[startIdx][secondStartIdx + space2];
                                                }
                                                //TODO
                                                //                                                if(value>maxValueFilter){
                                                //                                                    isMergedWidthBeyond = true;
                                                //                                                    
                                                //                                                    if (opAll != null) {//it's null for last layer
                                                //                                                        opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                                //                                                        System.out.println(value+" is larger than "+maxValueFilter);
                                                //                                                    }
                                                //                                                    break;
                                                //                                                }
                                                //                                                ++calcCount;
                                                //                                                System.out.println(combinedNumMap[startIdx][secondStartIdx + space2]);
                                            }
                                                break;
                                            case 1:
                                                value = v1[i] + v2[j];
                                                //                                                ++calcCount;
                                                break;
                                            case 2: {
                                                //!!!!!substract, braceket...
                                                //only first number need to change to substract
                                                int preSpace = 0;
                                                for (k = 0; k < op2.length - 1; ++k) {
                                                    if (op2[k] != 0) {
                                                        break;
                                                    } else {
                                                        ++preSpace;
                                                    }
                                                }
                                                value = v1[i]
                                                        - 2
                                                        * combinedNumMap[secondStartIdx][secondStartIdx
                                                                + preSpace]
                                                        + v2[j];
                                                //                                                ++calcCount;
                                            }
                                                break;
                                        }

                                        if (size <= halfSize) {
                                            results[size][startIdx][opAllIdx] = value;
                                        }
                                        if (value == 0 && size == numbers.length && op1[op1.length - 1] > 0
                                                && op2[op2.length - 1] > 0 && !isMergedWidthBeyond) {
                                            if (isFirst) {
                                                isFirst = false;
                                            } else {
                                                result.append(';');
                                            }
                                            int numIdx = 0;
                                            //                                                System.out.println("op1:" + op1.length + ",op2:" + op2.length);
                                            for (numIdx = 0; numIdx < op1.length - 1; ++numIdx) {
                                                result.append(numbers[numIdx]);
                                                //construct string
                                                result.append(opStr[op1[numIdx]]);
                                            }
                                            result.append(numbers[numIdx]);
                                            result.append(opStr[opMiddle]);
                                            ++numIdx;
                                            for (k = 0; k < op2.length - 1; ++k, ++numIdx) {
                                                result.append(numbers[numIdx]);
                                                //construct string
                                                result.append(opStr[op2[k]]);
                                            }
                                            result.append(numbers[numIdx]);

                                        }
                                        //                                            }
                                    }
                                    ++opAllIdx;
                                }
                            }
                        }
                    }
                    if (size == numbers.length) {
                        isFinish = true;
                        //                        System.out.println("isfinish");
                        break;
                    }
                }

                d = nextD;
            }
        }
        //        System.out.println("construct string time: " + strTime + " ms");
        //        System.out.println("2 calc and output " + (System.currentTimeMillis() - start));
        //        System.out.println("calc count:" + calcCount);
        //                start = System.currentTimeMillis();
        if (result.length() == 0) {
            return "NOTFOUND";
        }
        return result.toString();
    }

    /**
     * 3~15
     * 16~
     * 
     * @param arg
     * @return
     */
    public static String run7_6(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        //        long start = System.currentTimeMillis();

        int maxDigitNum = 0;
        //the number of numbers
        int n = 0;
        //        int maxValueFilter = 0;
        char ch = arg.charAt(0);
        /**
         * find the max combined number digit num
         * route to fitable alg according to max digit
         * check if valid
         */
        if (ch == 'S') {
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            if (n < 3 || n > 15) {
                return "ILLEGAL";
            }
            maxDigitNum = n / 10 + 3;
        } else if (ch == 'R') {
            //check valid            
            char cc = 0;
            int digitLen = 0;
            for (int i = 1, len = arg.length(); i < len; ++i) {
                cc = arg.charAt(i);
                if (cc <= '9' && cc >= '0') {
                    if (digitLen == 0 && ch == '0') {//the prefix should not be 0
                        return "ILLEGAL";
                    }
                    ++digitLen;
                } else if (cc == ',') {
                    if (digitLen == 0) {//blank number
                        return "ILLEGAL";
                    }
                    maxDigitNum += digitLen;
                    digitLen = 0;
                    ++n;
                } else {//invalid
                    return "ILLEGAL";
                }
            }
            maxDigitNum += digitLen;//the last one
            ++n;
            maxDigitNum = maxDigitNum % 2 == 0 ? maxDigitNum / 2 : (maxDigitNum - 1) / 2;
        } else {
            return "ILLEGAL";
        }
        //        System.out.println("Max digit num:" + maxDigitNum);
        ///////////////////////////////////////////////////////
        StringBuilder result = null;
        //1) Integer
        if (maxDigitNum < 16) {
            long [] numbers = new long[n];
            if (ch == 'S') {
                for (int i = 0; i < numbers.length; ++i) {
                    numbers[i] = i + 1;
                }
                //various buffer size
                result = new StringBuilder(2 << n);
            } else {
                int pre = Integer.MIN_VALUE;
                int intNum = 0;
                int idx = 0;
                for (int i = 1, len = arg.length(); i < len; ++i) {
                    char cc = arg.charAt(i);
                    if (cc <= '9' && cc >= '0') {
                        intNum = intNum * 10 + (cc - '0');
                    } else if (cc == ',') {
                        if (intNum < 0 || intNum < pre) {
                            //not larger than previous
                            return "ILLEGAL";
                        }
                        pre = intNum;
                        numbers[idx] = intNum;
                        ++idx;
                        intNum = 0;
                    }
                }
                if (intNum < 0 || intNum < pre) {
                    //not larger than previous
                    return "ILLEGAL";
                }
                numbers[idx] = intNum;
                result = new StringBuilder(64);
            }
            ///////////////////////////////////////////
            //row:startIdx
            //column:endIdx
            //value:the total digit of the number constructed from startidx to endidx
            //if value is zero, it means invalid startidx and endidx, or beyond max digit
            int [][] combinedNumDigitMap = new int[n][n];
            int w = 10;
            int digit = 1;

            for (int i = 0; i < n; ++i) {
                digit = 1;
                w = 10;
                while (true) {
                    if (numbers[i] / w == 0) {
                        combinedNumDigitMap[i][i] = digit;
                        break;
                    }
                    w *= 10;
                    ++digit;
                }
            }
            int halfSize = ((numbers.length % 2) == 0) ? numbers.length >> 1 : (numbers.length >> 1) + 1;
            final char [] opStr = {' ', '+', '-'};
            final int OP_NUM = 3;
            boolean isFirst = true;
            //row:startidx
            //column:endIdx
            //value:spacecount
            char [][] combinedNumSpaceMap = new char[numbers.length][numbers.length];
            //cache large number
            //row: startNum
            //column:endNum
            //save the index of number
            long [][] combinedNumMap = new long[numbers.length][numbers.length];
            for (int i = 0; i < numbers.length; ++i) {
                combinedNumMap[i][i] = numbers[i];
                for (int j = i + 1; j < numbers.length; ++j) {
                    //1) update combined num digit map
                    combinedNumDigitMap[i][j] = combinedNumDigitMap[j][j] + combinedNumDigitMap[i][j - 1];
                    if (combinedNumDigitMap[i][j] > maxDigitNum) {
                        combinedNumDigitMap[i][j] = 0;
                        break;
                    }
                    ////////////////////////////////////////////////////////
                    int c = combinedNumDigitMap[j][j];
                    w = 10;
                    while (c > 1) {
                        w *= 10;
                        --c;
                    }
                    //2)update combined num map 
                    combinedNumMap[i][j] = combinedNumMap[i][j - 1] * w + numbers[j];
                    //3)count space number
                    combinedNumSpaceMap[i][j] = (char)(j - i);
                }
                //                        System.out.println(Arrays.toString(combinedNumDigitMap[i]));
                //                System.out.println(Arrays.toString(combinedNumMap[i]));
                //                System.out.println(Arrays.toString(combinedNumSpaceMap[i]));
            }
            //        System.out.println();

            //the set size:1~half total size
            int [] answerNumMap = new int[halfSize + 1];
            for (int size = 1, answerNum = 1; size < answerNumMap.length; ++size, answerNum *= OP_NUM) {
                answerNumMap[size] = answerNum;
            }
            //////////////////////////////////
            //row: the idx is the size of set, the size of f(3,5,6,9) is 4, map[4][][]
            //column:start idx
            //element:value, order by ' ',+,-
            //if the value is not illegal, make sure it won't be used
            long [][][] results = new long[halfSize + 1][numbers.length][];
            //initial the single value
            for (int startIdx = 0; startIdx < numbers.length; ++startIdx) {
                results[1][startIdx] = new long[1];
                results[1][startIdx][0] = numbers[startIdx];
            }
            //        System.out.println("1 input " + (System.currentTimeMillis() - start));
            //        start = System.currentTimeMillis();
            /////////////////////////////////////////////////////////////////

            //!!!!do not use it to store if op is valid......

            //store op seq for each type answer map
            //row:the size of set
            //column: answer index from 0
            //element:op size
            //!!!!pay attention out of memory...
            //the space count won't larger than MAX_TREE_DEPTH for single set
            //except merged set
            //
            //e.g. [1,2,3,4], set size is 4, answer size is 81, each answer has 3 ops
            //XXX
            char [][][] opMap = new char[halfSize + 1][][];
            for (int numberSize = 1; numberSize < opMap.length; ++numberSize) {
                //last one stores if the op is valid!!!!
                //numberSize-1: op numbers
                opMap[numberSize] = new char[answerNumMap[numberSize]][numberSize - 1];
                //by default, it invalid
                for (int aIdx = 0; aIdx < opMap[numberSize].length; ++aIdx) {
                    int t = aIdx;
                    int j = opMap[numberSize][aIdx].length - 1;
                    w = 0;
                    while (j >= 0) {//store op
                        opMap[numberSize][aIdx][j] = (char)(t % OP_NUM);
                        t /= OP_NUM;
                        --j;
                        if (t == 0) {
                            for (; j >= 0; --j) {
                                opMap[numberSize][aIdx][j] = (0);
                            }
                            break;
                        }
                    }
                    //                    System.out.println(numberSize + ":" + Arrays.toString(opMap[numberSize][aIdx]));
                }

            }
            //row:set size
            //column:startidx
            //z:answer idx
            //value:if valid, default is not invalid, false
            boolean [][][] opInvalidMap = new boolean[halfSize + 1][n][];
            //            for(int i=1;i<opInvalidMap.length;++i){
            //                opInvalidMap[i]=new boolean[i][answerNumMap[i]];
            //            }
            /////////////////////////////////////////////////
            final int NODE_LEN = 3;
            //partition
            //0:startidx
            //1:len
            //2:layer
            int [] unprocessedStack = new int[(halfSize + 1) * NODE_LEN];
            //use the last one as idx
            int [][] processedQueue = new int[halfSize][numbers.length * NODE_LEN];
            //push the whole seq
            unprocessedStack[0] = 0;
            unprocessedStack[1] = numbers.length;
            //first layer is -1
            unprocessedStack[2] = -1;
            //point to the first unprocessed element
            int unprocessedStackIdx = NODE_LEN;
            //        int processedQueueIdx = 0;
            int maxLayer = 0;
            while (unprocessedStackIdx > 0) {
                //pop
                int startIdx = unprocessedStack[unprocessedStackIdx - 3];
                int len = unprocessedStack[unprocessedStackIdx - 2];
                int layer = unprocessedStack[unprocessedStackIdx - 1];
                unprocessedStackIdx -= NODE_LEN;
                if (len <= 2) {
                    if (results[1][startIdx] == null) {
                        results[1][startIdx] = new long[1];
                        results[1][startIdx][0] = numbers[startIdx];
                    }
                    if (len == 2) {
                        if (results[1][startIdx + 1] == null) {
                            results[1][startIdx + 1] = new long[1];
                            results[1][startIdx + 1][0] = numbers[startIdx + 1];
                        }
                        if (results[2][startIdx] == null) {
                            results[2][startIdx] = new long[answerNumMap[2]];
                        }
                        long v1 = results[1][startIdx][0];
                        long v2 = results[1][startIdx + 1][0];
                        for (int opMiddle = 0, idx = 0; opMiddle < OP_NUM; ++opMiddle, ++idx) {
                            switch (opMiddle) {
                                case 0:
                                    results[2][startIdx][idx] = combinedNumMap[startIdx][startIdx + 1];
                                    break;
                                case 1:
                                    results[2][startIdx][idx] = v1 + v2;
                                    break;
                                case 2:
                                    results[2][startIdx][idx] = v1 - v2;
                                    break;
                            }
                        }
                    }
                } else {
                    int nextLayer = layer + 1;
                    if (nextLayer > maxLayer) {
                        maxLayer = nextLayer;
                    }
                    //push to unproccesed queue
                    int secondStartIdx = startIdx + (len / 2);
                    int size1 = secondStartIdx - startIdx;
                    int size2 = len - size1;
                    //push
                    unprocessedStack[unprocessedStackIdx] = secondStartIdx;
                    unprocessedStack[unprocessedStackIdx + 1] = size2;
                    unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
                    unprocessedStackIdx += NODE_LEN;
                    //
                    unprocessedStack[unprocessedStackIdx] = startIdx;
                    unprocessedStack[unprocessedStackIdx + 1] = size1;
                    unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
                    unprocessedStackIdx += NODE_LEN;
                    //
                    int idx = processedQueue[nextLayer][processedQueue[nextLayer].length - 1];

                    processedQueue[nextLayer][idx] = startIdx;
                    processedQueue[nextLayer][idx + 1] = size1;
                    processedQueue[nextLayer][idx + 2] = nextLayer;
                    //
                    processedQueue[nextLayer][idx + 3] = secondStartIdx;
                    processedQueue[nextLayer][idx + 4] = size2;
                    processedQueue[nextLayer][idx + 5] = nextLayer;

                    processedQueue[nextLayer][processedQueue[nextLayer].length - 1] += 6;

                }
            }
            //        System.out.println("1 prepare " + (System.currentTimeMillis() - start));
            //        start = System.currentTimeMillis();

            /////////////////////////////////////////////////////////////////
            //use queue like a stack here,
            //point to the head of the queue
            //        System.out.println("layer:" + maxLayer);
            //test
            //            int calcCount = 0;
            boolean isFinish = false;
            long value = Integer.MIN_VALUE;
            //op type
            char [] op1 = null;
            char [] op2 = null;
            //next layer op type
            //            int [][] opAll = null;
            //op valid flag
            boolean [] op1InvalidFlag = null;
            boolean [] op2InvalidFlag = null;
            boolean [] opInvalidFlag = null;
            long [] v1 = null;
            long [] v2 = null;
            int d = 0;
            int k = 0;
            int space1 = 0;
            int space2 = 0;
            for (int layerIdx = maxLayer; layerIdx >= 0 && !isFinish; --layerIdx) {
                d = NODE_LEN;
                while (d <= numbers.length && !isFinish) {
                    int nextD = (d << 1);
                    //            System.out.println(d);
                    for (int qIdx = 0; qIdx < processedQueue[layerIdx].length - 1; qIdx += nextD) {
                        if (qIdx + d >= processedQueue[layerIdx][processedQueue[layerIdx].length - 1]) {
                            break;
                        }
                        //                    System.out.println("d:" + d + ",next d:" + nextD + ",i:" + qIdx);
                        int startIdx = processedQueue[layerIdx][qIdx];
                        int size1 = processedQueue[layerIdx][qIdx + 1];
                        int secondStartIdx = processedQueue[layerIdx][qIdx + d];
                        int size2 = processedQueue[layerIdx][qIdx + d + 1];
                        int size = size1 + size2;

                        int opAllIdx = 0;
                        opInvalidFlag = null;
                        if (size <= halfSize) {
                            if (results[size][startIdx] == null) {
                                results[size][startIdx] = new long[answerNumMap[size]];
                            }
                            if (opInvalidMap[size][startIdx] == null) {
                                opInvalidMap[size][startIdx] = new boolean[answerNumMap[size]];
                                //                                System.out.println("new opInvalidMap[" + size + "][" + startIdx + "]");
                            }
                            opInvalidFlag = opInvalidMap[size][startIdx];
                        }
                        if (opInvalidMap[size1][startIdx] == null) {
                            opInvalidMap[size1][startIdx] = new boolean[answerNumMap[size1]];
                        }
                        if (opInvalidMap[size2][secondStartIdx] == null) {
                            opInvalidMap[size2][secondStartIdx] = new boolean[answerNumMap[size2]];
                        }

                        processedQueue[layerIdx][qIdx] = startIdx;
                        processedQueue[layerIdx][qIdx + 1] = size;
                        v1 = results[size1][startIdx];
                        v2 = results[size2][secondStartIdx];
                        op1InvalidFlag = opInvalidMap[size1][startIdx];
                        op2InvalidFlag = opInvalidMap[size2][secondStartIdx];

                        /////////////////////////////////////////////////////
                        //Test
                        //                                                        System.out.println("push:");
                        //                                                        System.out.println("idx [" + startIdx + ", " + (startIdx + size1 - 1) + "], [" + secondStartIdx + ","
                        //                                                                + (secondStartIdx + size2 - 1) + "]");

                        //                        System.out.print("merge set [" + numbers[startIdx]);
                        //                        if (size1 > 1) {
                        //                            System.out.print(", " + numbers[(startIdx + size1 - 1)]);
                        //                        }
                        //                        System.out.print("], [" + numbers[secondStartIdx]);
                        //                        if (size2 > 1) {
                        //                            System.out.print("," + numbers[(secondStartIdx + size2 - 1)]);
                        //                        }
                        //                        System.out.println("]");
                        //                        for (k = 0; k < v1.length; ++k) {
                        //                            System.out.print(v1[k]);
                        //                            System.out.print("(" + opInvalidMap[size1][startIdx][k] + "),");
                        //                        }
                        //                        System.out.println();
                        //                        for (k = 0; k < v2.length; ++k) {
                        //                            System.out.print(v2[k]);
                        //                            System.out.print("(" + opInvalidMap[size2][secondStartIdx][k] + "),");
                        //                        }
                        //                        System.out.println();
                        /////////////////////////////////////////////////////

                        //                        System.out.println(Arrays.toString(v1));
                        //                        System.out.println(Arrays.toString(v2));

                        //                    System.out.println("op1 size "+opMap[size1].length);
                        //                    System.out.println("op2 size "+opMap[size2].length);
                        value = -1;
                        //1) first part size is 1                            
                        for (int i = 0; i < v1.length; ++i) {
                            op1 = opMap[size1][i];
                            if (op1InvalidFlag[i]) {//[invalid1]this op seq is invalid,true for invalid
                                k = (OP_NUM * v2.length) + opAllIdx;
                                if (opInvalidFlag != null) {
                                    for (w = opAllIdx; w < k; ++w) {
                                        opInvalidFlag[w] = true;
                                    }
                                    //                                System.out.println("1) opAll is found invalid by op1");
                                }

                                opAllIdx = k;
                                //                                                                                            System.out.println("1) op1[op1.length - 1] == 0");
                            } else {
                                space1 = 0;
                                k = op1.length - 1;
                                while (k >= 0) {
                                    if (op1[k] == 0) {
                                        ++space1;
                                        --k;
                                    } else {
                                        break;
                                    }
                                }

                                //2) middle op
                                for (int opMiddle = 0; opMiddle < OP_NUM; ++opMiddle) {
                                    for (int j = 0; j < v2.length; ++j) {
                                        op2 = opMap[size2][j];
                                        if (op2InvalidFlag[j]) {
                                            if (opInvalidFlag != null) {
                                                opInvalidFlag[opAllIdx] = true;
                                                //                                System.out.println("1) opAll is found invalid by op1");
                                            }
                                        } else {
                                            boolean isMergedWidthBeyond = false;
                                            switch (opMiddle) {
                                                case 0:
                                                    space2 = 0;
                                                    k = 0;
                                                    while (k < op2.length) {
                                                        if (op2[k] == 0) {
                                                            ++space2;
                                                            ++k;
                                                        } else {
                                                            break;
                                                        }
                                                    }
                                                    //                                                    if(startIdx==0&&secondStartIdx==1)

                                                    //                                                    if(secondStartIdx - 1 - space1==2&&secondStartIdx
                                                    //                                                            + space2==4){
                                                    //                                                        System.out.print("space1:"+space1+",space2:"+space2);
                                                    //                                                        System.out.println(" "+combinedNumSpaceMap[secondStartIdx - 1 - space1][secondStartIdx
                                                    //                                                            + space2]);
                                                    //                                                    }
                                                    //                                                    
                                                    //[invalid] merged width is larger than depth
                                                    //this case only happened in last time
                                                    if (combinedNumSpaceMap[secondStartIdx - 1 - space1][secondStartIdx
                                                            + space2] == 0) {
                                                        //test
                                                        //                                                        System.out.print(numbers[secondStartIdx - 1 - space1] + "~"
                                                        //                                                                + numbers[secondStartIdx
                                                        //                                                                        + space2]);
                                                        //                                                        System.out.print(" " + numbers[startIdx]);
                                                        //                                                        for (k = 0; k < op1.length; ++k) {
                                                        //                                                            System.out.print(opStr[op1[k]]);
                                                        //                                                            System.out.print(numbers[startIdx + k + 1]);
                                                        //                                                        }
                                                        //                                                        System.out.print("(" + opStr[opMiddle] + ")");
                                                        //                                                        System.out.print(numbers[secondStartIdx]);
                                                        //                                                        for (k = 0; k < op2.length; ++k) {
                                                        //                                                            System.out.print(opStr[op2[k]]);
                                                        //                                                            System.out.print(numbers[secondStartIdx + k + 1]);
                                                        //                                                        }
                                                        //                                                        System.out.print(" "
                                                        //                                                                + combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                        //                                                                        + space2]);
                                                        //                                                        System.out.println(" "
                                                        //                                                                + combinedNumSpaceMap[secondStartIdx - 1 - space1][secondStartIdx
                                                        //                                                                        + space2]);

                                                        //////////////////////////////////////
                                                        //                                                if (space1 + space2 + 1 > maxSpaceCount) {
                                                        isMergedWidthBeyond = true;
                                                        if (opInvalidFlag != null) {//it's null for last layer
                                                            opInvalidFlag[opAllIdx] = true;
                                                            //                                                          System.out.println("opAll[" + size + "][" + opAllIdx
                                                            //                                                                + "]is found invalid by merge");
                                                        }
                                                        break;
                                                    }

                                                    value = v1[i] + v2[j];
                                                    //second prefix
                                                    value -= combinedNumMap[secondStartIdx][secondStartIdx
                                                            + space2];
                                                    //                                                System.out.println("MAX:"+ MAX+", width:" + (secondStartIdx
                                                    //                                                        + space2 - (secondStartIdx - 1 - space1) + 1));
                                                    if (op1.length - 1 - space1 >= 0) {//there is other op before space in first part
                                                        if (op1[op1.length - space1 - 1] == 1) {//+
                                                            //first postfix
                                                            value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                            value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                    + space2];
                                                        } else if (op1[op1.length - space1 - 1] == 2) {//-
                                                            value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                            value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                    + space2];
                                                        }
                                                    } else {
                                                        value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                        //may be larger than MAX width
                                                        value += combinedNumMap[startIdx][secondStartIdx + space2];
                                                    }
                                                    //                                                    ++calcCount;
                                                    break;
                                                case 1:
                                                    value = v1[i] + v2[j];
                                                    //                                                    ++calcCount;
                                                    break;
                                                case 2: {
                                                    //!!!!!substract, braceket...
                                                    //only first number need to change to substract
                                                    int preSpace = 0;
                                                    for (k = 0; k < op2.length; ++k) {
                                                        if (op2[k] != 0) {
                                                            break;
                                                        } else {
                                                            ++preSpace;
                                                        }
                                                    }
                                                    value = v1[i]
                                                            - 2
                                                            * combinedNumMap[secondStartIdx][secondStartIdx
                                                                    + preSpace]
                                                            + v2[j];
                                                    //                                                    ++calcCount;
                                                }
                                                    break;
                                            }

                                            if (size <= halfSize) {
                                                results[size][startIdx][opAllIdx] = value;
                                            } else {
                                                //test
                                                //                                                int numIdx = 0;
                                                //                                                //                                                System.out.println("op1:" + op1.length + ",op2:" + op2.length);
                                                //                                                for (numIdx = 0; numIdx < op1.length; ++numIdx) {
                                                //                                                    System.out.print(numbers[numIdx]);
                                                //                                                    //construct string
                                                //                                                    System.out.print(opStr[op1[numIdx]]);
                                                //                                                }
                                                //                                                System.out.print(numbers[numIdx]);
                                                //                                                System.out.print(opStr[opMiddle]);
                                                //                                                ++numIdx;
                                                //                                                for (k = 0; k < op2.length; ++k, ++numIdx) {
                                                //                                                    System.out.print(numbers[numIdx]);
                                                //                                                    //construct string
                                                //                                                    System.out.print(opStr[op2[k]]);
                                                //                                                }
                                                //                                                System.out.println(numbers[numIdx]+"="+value+"="+v1[i]+opStr[opMiddle]+v2[j]);
                                            }
                                            if (value == 0 && size == numbers.length && !op1InvalidFlag[i]
                                                    && !op2InvalidFlag[j] && !isMergedWidthBeyond) {
                                                if (isFirst) {
                                                    isFirst = false;
                                                } else {
                                                    result.append(';');
                                                }
                                                int numIdx = 0;
                                                //                                                System.out.println("op1:" + op1.length + ",op2:" + op2.length);
                                                for (numIdx = 0; numIdx < op1.length; ++numIdx) {
                                                    result.append(numbers[numIdx]);
                                                    //construct string
                                                    result.append(opStr[op1[numIdx]]);
                                                }
                                                result.append(numbers[numIdx]);
                                                result.append(opStr[opMiddle]);
                                                ++numIdx;
                                                for (k = 0; k < op2.length; ++k, ++numIdx) {
                                                    result.append(numbers[numIdx]);
                                                    //construct string
                                                    result.append(opStr[op2[k]]);
                                                }
                                                result.append(numbers[numIdx]);
                                            }
                                        }
                                        ++opAllIdx;
                                    }
                                }
                            }
                        }
                        if (size == numbers.length) {
                            isFinish = true;
                            break;
                        }
                    }

                    d = nextD;
                }
            }
            //            System.out.println("calc count:" + calcCount);
            //        }
            //        else if (maxDigitNum < 16) {//2) Long
            //            long [] numbers = null;
        } else {//3) big integer

        }
        //        System.out.println("construct string time: " + strTime + " ms");
        //        System.out.println("2 calc and output " + (System.currentTimeMillis() - start));

        //                start = System.currentTimeMillis();
        if (result.length() == 0) {
            return "NOTFOUND";
        }
        return result.toString();
    }

    //TODO ??? do not calc when back
    public static String run7ForSmall(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        long start = System.currentTimeMillis();
        StringBuilder result = null;
        int [] numbers = null;
        int maxDigitNum = 0;
        int TOTAL_DIGIT_NUM = 0;
        //        int MAX = 0;
        char ch = arg.charAt(0);
        int [][] combinedNumDigitMap = null;
        if (ch == 'S') {
            int n = 0;
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            //3<=N<=15
            if (n < 3 || n > 15) {
                return "ILLEGAL";
            }
            ///////////////////////////////////////////
            //TODO calc digit data
            if (n < 10) {
                TOTAL_DIGIT_NUM = n;
            } else {
                TOTAL_DIGIT_NUM = (n - 10 + 1) * 2 + (10 - 1);
            }
            maxDigitNum = TOTAL_DIGIT_NUM / 2;
            ///////////////////////////////////////////
            //row:startIdx
            //column:endIdx
            //value:the total digit of the number constructed from startidx to endidx
            //if value is zero, it means invalid startidx and endidx, or beyond max digit
            combinedNumDigitMap = new int[n][n];
            numbers = new int[n];
            int w = 10;
            int digit = 1;
            for (int i = 0; i < n; ++i) {
                while (true) {
                    if ((i + 1) / w == 0) {
                        combinedNumDigitMap[i][i] = digit;
                        break;
                    }
                    w *= 10;
                    ++digit;
                }
                numbers[i] = i + 1;
            }

            for (int i = 0; i < n; ++i) {
                for (int j = i + 1; j < n; ++j) {
                    combinedNumDigitMap[i][j] = combinedNumDigitMap[j][j] + combinedNumDigitMap[i][j - 1];
                    if (combinedNumDigitMap[i][j] > maxDigitNum) {
                        combinedNumDigitMap[i][j] = 0;
                        break;
                    }
                }
                System.out.println(Arrays.toString(combinedNumDigitMap[i]));
            }

            ///////////////////////////////////////////
            //various buffer size
            result = new StringBuilder(2 << n);
            //            MAX = n / 2 + 1;
        } else if (ch == 'R') {
            String asdStr = arg.substring(1);
            String [] numberStr = asdStr.split(",");
            numbers = new int[numberStr.length];
            int pre = Integer.MIN_VALUE;
            try {
                for (int i = 0; i < numbers.length; ++i) {
                    if (numberStr[i].isEmpty() || numberStr[i].charAt(0) > '9') {
                        //not a number
                        return "ILLEGAL";
                    }
                    numbers[i] = Integer.parseInt(numberStr[i]);
                    if (numbers[i] < 0 || numbers[i] < pre) {
                        //not larger than previous
                        return "ILLEGAL";
                    }
                    pre = numbers[i];
                }
            } catch (Exception e) {
                return "ILLEGAL";
            }
            result = new StringBuilder(64);

            //            MAX = numberStr.length / 2 + 1;
            //            MAX = (numberStr.length * numberStr[numberStr.length - 1].length()) / 2 + 1;
            //            if (MAX < numbers.length - 1) {
            //                MAX = numbers.length - 1;
            //            }

        } else {
            return "ILLEGAL";
        }

        System.out.println("Total digit num:" + TOTAL_DIGIT_NUM);
        System.out.println("Max digit num:" + maxDigitNum);

        final int maxSpaceCount = ((numbers.length % 2) == 0) ? numbers.length >> 1 : (numbers.length >> 1) + 1;
        System.out.println("MAX_SPACE_COUNT:" + maxSpaceCount);
        //        int maxTreeDepth = 0;
        //        int temp = numbers.length;
        //        while (temp > 0) {
        //            temp >>= 1;
        //            ++maxTreeDepth;
        //        }
        //
        //        System.out.println("MAX_TREE_DEPTH:" + maxTreeDepth);
        //!!!
        //        MAX = numbers.length - 2;
        //        MAX = numbers.length / 2 + 1;
        //n<=15, MAX_TREE_DEPTH<=8, use int type
        //TODO
        if (maxSpaceCount > 15) {
            return "ILLEGAL";
        }

        final char [] opStr = {' ', '+', '-'};
        final int OP_NUM = 3;
        boolean isFirst = true;

        //cache large number
        //row: startNum
        //column:endNum
        //save the index of number
        long [][] combinedNumMap = new long[numbers.length][numbers.length];
        long maxValue = 0;
        if (ch == 'S') {

            for (int i = 0; i < numbers.length; ++i) {
                combinedNumMap[i][i] = numbers[i];
                for (int j = i + 1; j < numbers.length; ++j) {
                    //!!!limit the combined number
                    if (combinedNumDigitMap[i][j] > 0) {
                        if (numbers[j] < 10) {
                            combinedNumMap[i][j] = combinedNumMap[i][j - 1] * 10 + numbers[j];
                        } else {
                            combinedNumMap[i][j] = combinedNumMap[i][j - 1] * 100 + numbers[j];
                        }
                        if (combinedNumMap[i][j] > maxValue) {
                            maxValue = combinedNumMap[i][j];
                        }
                    } else {
                        break;
                    }
                }
                //                System.out.println(Arrays.toString(combinedNumMap[i]));
            }

            //TODO
            System.out.println(numbers.length - maxDigitNum + "," + (numbers.length - 1));
            System.out.println("maxValue:" + maxValue);
        } else {
            for (int i = 0; i < numbers.length; ++i) {
                combinedNumMap[i][i] = numbers[i];
                for (int j = i + 1; j < numbers.length; ++j) {
                    //!!!limit the combined number
                    if ((j - i + 1) <= maxSpaceCount + 1) {
                        combinedNumMap[i][j] = numbers[j];
                        long cur = combinedNumMap[i][j];
                        int c = 1;
                        do {
                            cur /= 10;
                            c *= 10;
                        } while (cur > 0);
                        combinedNumMap[i][j] = combinedNumMap[i][j - 1] * c + combinedNumMap[i][j];
                    } else {
                        combinedNumMap[i][j] = 0;
                    }
                }
            }
        }
        int [] answerNumMap = new int[maxSpaceCount + 1];
        for (int size = 1, answerNum = 1; size < answerNumMap.length; ++size, answerNum *= OP_NUM) {
            answerNumMap[size] = answerNum;
        }
        //////////////////////////////////
        //row: the idx is the size of set, the size of f(3,5,6,9) is 4, map[4][][]
        //column:start idx
        //element:value, order by ' ',+,-
        //if the value is not illegal, make sure it won't be used

        //TODO decrease the number
        long [][][] results = new long[maxSpaceCount + 1][numbers.length][];
        //initial the single value
        for (int startIdx = 0; startIdx < numbers.length; ++startIdx) {
            results[1][startIdx] = new long[1];
            results[1][startIdx][0] = numbers[startIdx];
        }
        //        System.out.println("1 input " + (System.currentTimeMillis() - start));
        //        start = System.currentTimeMillis();
        /////////////////////////////////////////////////////////////////

        //store op seq for each answer map
        //row:size
        //column: answer index
        //element:op 
        //!!!!pay attention out of memory...
        //the space count won't larger than MAX_TREE_DEPTH for single set
        //except merged set
        char [][][] opMap = new char[maxSpaceCount + 1][][];
        for (int size = 1; size < opMap.length; ++size) {
            //last one stores if the op is valid!!!!
            opMap[size] = new char[answerNumMap[size]][size];
            if (opMap[size][0].length - 1 <= maxSpaceCount) {
                opMap[size][0][opMap[size][0].length - 1] = 1;
            }
            //by default, it invalid
            for (int aIdx = 1; aIdx < opMap[size].length; ++aIdx) {
                int t = aIdx;
                int j = opMap[size][aIdx].length - 2;
                while (j >= 0) {//store op
                    opMap[size][aIdx][j] = (char)(t % OP_NUM);
                    t /= OP_NUM;
                    --j;
                    if (t == 0) {
                        for (; j >= 0; --j) {
                            opMap[size][aIdx][j] = (0);
                        }
                        break;
                    }

                }
                //mark it valid
                opMap[size][aIdx][opMap[size][aIdx].length - 1] = 1;
            }
        }
        /////////////////////////////////////////////////
        final int NODE_LEN = 3;
        //partition
        //0:startidx
        //1:len
        //2:layer
        int [] unprocessedStack = new int[(maxSpaceCount + 1) * NODE_LEN];
        //use the last one as idx
        int [][] processedQueue = new int[maxSpaceCount][numbers.length * NODE_LEN];
        //        int layerIdx = 0;

        //push the whole seq
        unprocessedStack[0] = 0;
        unprocessedStack[1] = numbers.length;
        //first layer is -1
        unprocessedStack[2] = -1;
        //point to the first unprocessed element
        int unprocessedStackIdx = NODE_LEN;
        //        int processedQueueIdx = 0;
        int maxLayer = 0;
        while (unprocessedStackIdx > 0) {
            //pop
            int startIdx = unprocessedStack[unprocessedStackIdx - 3];
            int len = unprocessedStack[unprocessedStackIdx - 2];
            int layer = unprocessedStack[unprocessedStackIdx - 1];
            unprocessedStackIdx -= NODE_LEN;

            //test
            //            System.out.print("pop number [" + numbers[startIdx]);
            //            if (len > 1) {
            //                System.out.print(", " + numbers[(startIdx + len - 1)]);
            //            }
            //            System.out.println("]");
            //
            //

            if (len <= 2) {
                //                System.out.print("add to process queue, number [" + numbers[startIdx]);
                //                if (len > 1) {
                //                    System.out.print(", " + numbers[(startIdx + len - 1)]);
                //                }
                //                System.out.println("]");
                //
                //
                if (results[1][startIdx] == null) {
                    results[1][startIdx] = new long[1];
                    results[1][startIdx][0] = numbers[startIdx];
                }
                if (len == 2) {
                    if (results[1][startIdx + 1] == null) {
                        results[1][startIdx + 1] = new long[1];
                        results[1][startIdx + 1][0] = numbers[startIdx + 1];
                    }
                    if (results[2][startIdx] == null) {
                        results[2][startIdx] = new long[answerNumMap[2]];
                    }
                    long v1 = results[1][startIdx][0];
                    long v2 = results[1][startIdx + 1][0];
                    for (int opMiddle = 0, idx = 0; opMiddle < OP_NUM; ++opMiddle, ++idx) {
                        switch (opMiddle) {
                            case 0:
                                results[2][startIdx][idx] = combinedNumMap[startIdx][startIdx + 1];
                                break;
                            case 1:
                                results[2][startIdx][idx] = v1 + v2;
                                break;
                            case 2:
                                results[2][startIdx][idx] = v1 - v2;
                                break;
                        }
                    }
                }
            } else {
                int nextLayer = layer + 1;
                if (nextLayer > maxLayer) {
                    maxLayer = nextLayer;
                }
                //push to unproccesed queue
                int secondStartIdx = startIdx + (len / 2);
                int size1 = secondStartIdx - startIdx;
                int size2 = len - size1;
                //Test
                //                                System.out.println("push:");
                //                                System.out.println("idx [" + startIdx + ", " + (startIdx + size1 - 1) + "], [" + secondStartIdx + ","
                //                                        + (secondStartIdx + size2 - 1) + "]");
                //                
                //                                System.out.print("push number [" + numbers[startIdx]);
                //                                if (size1 > 1) {
                //                                    System.out.print(", " + numbers[(startIdx + size1 - 1)]);
                //                                }
                //                                System.out.print("], [" + numbers[secondStartIdx]);
                //                                if (size2 > 1) {
                //                                    System.out.print("," + numbers[(secondStartIdx + size2 - 1)]);
                //                                }
                //                                System.out.println("], layer:"+nextLayer);

                //

                //push
                unprocessedStack[unprocessedStackIdx] = secondStartIdx;
                unprocessedStack[unprocessedStackIdx + 1] = size2;
                unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
                unprocessedStackIdx += NODE_LEN;
                //
                unprocessedStack[unprocessedStackIdx] = startIdx;
                unprocessedStack[unprocessedStackIdx + 1] = size1;
                unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
                unprocessedStackIdx += NODE_LEN;
                //
                //
                //                System.out.println("----------------------------");
                int idx = processedQueue[nextLayer][processedQueue[nextLayer].length - 1];

                processedQueue[nextLayer][idx] = startIdx;
                processedQueue[nextLayer][idx + 1] = size1;
                processedQueue[nextLayer][idx + 2] = nextLayer;

                //                System.out.println(processedQueue[layerIdx][idx]);
                //                System.out.println(processedQueue[layerIdx][idx+1]);
                //                System.out.println(processedQueue[layerIdx][processedQueue[layerIdx].length - 1]);

                processedQueue[nextLayer][idx + 3] = secondStartIdx;
                processedQueue[nextLayer][idx + 4] = size2;
                processedQueue[nextLayer][idx + 5] = nextLayer;

                processedQueue[nextLayer][processedQueue[nextLayer].length - 1] += 6;

            }
        }
        System.out.println("1 prepare " + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        //////////////////////////////////////////////////////////////
        //test

        //        System.out.println("\nmerge");
        //        for (int i = maxLayer; i >= 0; --i) {
        //            //            for (int i = layerIdx - 1; i >= 0; --i) {
        //            for (int j = 0; j < processedQueue[i][processedQueue[i].length - 1]; j += NODE_LEN) {
        //                int startIdx = processedQueue[i][j];
        //                int len = processedQueue[i][j + 1];
        //                int layer = processedQueue[i][j + 2];
        //                //pop
        //                //test
        //                System.out.print("check number [" + numbers[startIdx]);
        //                if (len > 1) {
        //                    System.out.print(", " + numbers[(startIdx + len - 1)]);
        //                }
        //                System.out.println("] layer:" + layer);
        //                //
        //                //
        //                //
        //
        //            }
        //            System.out.println();
        //        }
        //
        //        System.out.println();
        //        System.out.println("processedQueueIdx:" + processedQueueIdx);
        //        long strTime = 0;
        //use queue like a stack here,
        //point to the head of the queue

        //        System.out.println("layer:" + maxLayer);
        //test
        //        int calcCount = 0;
        boolean isFinish = false;
        for (int layerIdx = maxLayer; layerIdx >= 0 && !isFinish; --layerIdx)
        //                int layerIdx = maxLayer; 
        {
            //            int headIdx = 0;
            int k = 0;
            int d = NODE_LEN;
            while (d <= numbers.length && !isFinish) {
                int nextD = (d << 1);
                //            System.out.println(d);
                for (int qIdx = 0; qIdx < processedQueue[layerIdx].length - 1; qIdx += nextD) {
                    if (qIdx + d >= processedQueue[layerIdx][processedQueue[layerIdx].length - 1]) {
                        break;
                    }
                    //                    System.out.println("d:" + d + ",next d:" + nextD + ",i:" + qIdx);
                    int startIdx = processedQueue[layerIdx][qIdx];
                    int size1 = processedQueue[layerIdx][qIdx + 1];
                    int secondStartIdx = processedQueue[layerIdx][qIdx + d];
                    int size2 = processedQueue[layerIdx][qIdx + d + 1];
                    int size = size1 + size2;

                    //                System.out.println("startIdx:" + startIdx);
                    //                System.out.println("size:" + size);
                    //                System.out.println("i=" + qIdx);
                    char [][] opAll = null;
                    int opAllIdx = 0;
                    if (size <= maxSpaceCount) {
                        opAll = opMap[size];
                        if (results[size][startIdx] == null) {
                            results[size][startIdx] = new long[answerNumMap[size]];
                        }
                    }

                    processedQueue[layerIdx][qIdx] = startIdx;
                    processedQueue[layerIdx][qIdx + 1] = size;
                    long [] v1 = results[size1][startIdx];
                    long [] v2 = results[size2][secondStartIdx];

                    ////////////////////////////////////
                    //test
                    //                    System.out.print("process number [" + numbers[startIdx]);
                    //                    if (size1 > 1) {
                    //                        System.out.print(", " + numbers[(startIdx + size1 - 1)]);
                    //                    }
                    //                    System.out.print("] (" + v1.length + ") , [" + numbers[secondStartIdx]);
                    //                    if (size2 > 1) {
                    //                        System.out.print("," + numbers[(secondStartIdx + size2 - 1)]);
                    //                    }
                    //                    System.out.println("] (" + v2.length + ")");
                    //            /////////////////////////////////////////////////////////////
                    //                        System.out.println(Arrays.toString(v1));
                    //                        System.out.println(Arrays.toString(v2));

                    long value = -1;

                    //1) first part size is 1                            
                    for (int i = 0; i < v1.length; ++i) {
                        char [] op1 = opMap[size1][i];
                        if (op1[op1.length - 1] == 0) {//[invalid1]this op seq is invalid
                            if (opAll != null) {
                                opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                System.out.println("op1 is invalid");
                            }
                            opAllIdx += (OP_NUM * v2.length);
                            //                                System.out.println("1) op1[op1.length - 1] == 0");
                        } else {

                            int space1 = 0;
                            int k1 = op1.length - 2;
                            while (k1 >= 0) {
                                if (op1[k1] == 0) {
                                    ++space1;
                                    --k1;
                                } else {
                                    break;
                                }
                            }
                            //2) middle op
                            for (int opMiddle = 0; opMiddle < OP_NUM; ++opMiddle) {
                                for (int j = 0; j < v2.length; ++j) {
                                    char [] op2 = opMap[size2][j];
                                    if (op2[op2.length - 1] > 0) {
                                        boolean isMergedWidthBeyond = false;

                                        switch (opMiddle) {
                                            case 0: {
                                                int space2 = 0;
                                                k1 = 0;
                                                while (k1 < op2.length - 1) {
                                                    if (op2[k1] == 0) {
                                                        ++space2;
                                                        ++k1;
                                                    } else {
                                                        break;
                                                    }
                                                }
                                                //[invalid2] merged width is larger than depth
                                                //this case only happened in last time
                                                if (space1 + space2 + 1 >= maxSpaceCount) {
                                                    isMergedWidthBeyond = true;
                                                    if (opAll != null) {
                                                        opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                                        //                                                        System.out.println("isMergedWidthBeyond = true");
                                                    }
                                                    //                                                   
                                                    break;
                                                }

                                                value = v1[i] + v2[j];
                                                //second prefix
                                                value -= combinedNumMap[secondStartIdx][secondStartIdx
                                                        + space2];
                                                //                                                System.out.println("MAX:"+ MAX+", width:" + (secondStartIdx
                                                //                                                        + space2 - (secondStartIdx - 1 - space1) + 1));
                                                if (op1.length - 2 - space1 >= 0) {//there is other op before space in first part
                                                    if (op1[op1.length - space1 - 2] == 1) {//+
                                                        //first postfix
                                                        value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                        value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                + space2];
                                                    } else if (op1[op1.length - space1 - 2] == 2) {//-
                                                        value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                        value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                + space2];
                                                    } else {
                                                        //                                                    System.out.println("error");
                                                    }

                                                } else {

                                                    value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                    //may be larger than MAX width
                                                    value += combinedNumMap[startIdx][secondStartIdx + space2];
                                                }
                                                //                                                    ++calcCount;
                                                //                                                System.out.println(combinedNumMap[startIdx][secondStartIdx + space2]);
                                            }
                                                break;
                                            case 1:
                                                value = v1[i] + v2[j];
                                                //                                                    ++calcCount;
                                                break;
                                            case 2: {
                                                //!!!!!substract, braceket...
                                                //only first number need to change to substract
                                                int preSpace = 0;
                                                for (k = 0; k < op2.length - 1; ++k) {
                                                    if (op2[k] != 0) {
                                                        break;
                                                    } else {
                                                        ++preSpace;
                                                    }
                                                }
                                                value = v1[i]
                                                        - 2
                                                        * combinedNumMap[secondStartIdx][secondStartIdx
                                                                + preSpace]
                                                        + v2[j];
                                                //                                                    ++calcCount;
                                            }
                                                break;
                                        }

                                        if (size <= maxSpaceCount) {
                                            results[size][startIdx][opAllIdx] = value;
                                        }
                                        //construct string cost 0ms
                                        if (value == 0 && size == numbers.length && op1[op1.length - 1] > 0
                                                && op2[op2.length - 1] > 0 && !isMergedWidthBeyond) {

                                            //                                              System.out.println("2 prepare " + (System.currentTimeMillis() - start));  
                                            //                                                start = System.currentTimeMillis();
                                            if (isFirst) {
                                                isFirst = false;
                                            } else {
                                                result.append(';');
                                            }
                                            int numIdx = 0;
                                            //                                                System.out.println("op1:" + op1.length + ",op2:" + op2.length);
                                            for (numIdx = 0; numIdx < op1.length - 1; ++numIdx) {
                                                result.append(numbers[numIdx]);
                                                //construct string
                                                result.append(opStr[op1[numIdx]]);
                                            }
                                            result.append(numbers[numIdx]);
                                            result.append(opStr[opMiddle]);
                                            ++numIdx;
                                            for (k = 0; k < op2.length - 1; ++k, ++numIdx) {
                                                result.append(numbers[numIdx]);
                                                //construct string
                                                result.append(opStr[op2[k]]);
                                            }
                                            result.append(numbers[numIdx]);

                                            //                                                start = System.currentTimeMillis();
                                            //                                                strTime += (System.currentTimeMillis() - start);

                                        }
                                    }
                                    //                                        else{
                                    //                                            System.out.println("op2 is invalid");
                                    //                                        }
                                    ++opAllIdx;
                                }
                            }

                        }
                    }
                    if (size == numbers.length) {
                        isFinish = true;
                        //                        System.out.println("isfinish");
                        break;
                    }
                }

                d = nextD;
            }
        }
        //        System.out.println("construct string time: " + strTime + " ms");
        System.out.println("2 calc and output " + (System.currentTimeMillis() - start));
        //        System.out.println("calc count:" + calcCount);
        //                start = System.currentTimeMillis();
        if (result.length() == 0) {
            return "NOTFOUND";
        }
        return result.toString();
    }

    //special for S and R
    //    public static String run8(String arg) {
    //        if (arg == null || arg.length() < 2) {
    //            return "ILLEGAL";
    //        }
    //        long start = System.currentTimeMillis();
    //
    //        StringBuilder result = null;
    //        //        int MAX = 0;
    //        char ch = arg.charAt(0);
    //        if (ch == 'S') {
    //            //write a table contains 3~15???
    //            int n = 0;
    //            try {
    //                n = Integer.valueOf(arg.substring(1));
    //            } catch (Exception e) {
    //                return "ILLEGAL";
    //            }
    //            //3<=N<=15
    //            if (n < 3 || n > 15) {
    //                return "ILLEGAL";
    //            }
    //            final int MAX_TREE_DEPTH = ((n % 2) == 0) ? n >> 1 : (n >> 1) + 1;
    //            if (MAX_TREE_DEPTH > 15) {
    //                return "ILLEGAL";
    //            }
    //            //various buffer size
    //            result = new StringBuilder(2 << n);
    //            //            final char [] opStr = {' ', '+', '-'};
    //            //            final int OP_NUM = 3;
    //            //            boolean isFirst = true;
    //            //cache large number
    //            //row: startNum
    //            //column:endNum
    //            //save the index of number
    //            int [][] combinedNumMap = new int[n + 1][n + 1];
    //            for (int i = 1; i <= n; ++i) {
    //                combinedNumMap[i][i] = i;
    //                for (int j = i + 1; j < n; ++j) {
    //                    //!!!limit the combined number
    //                    if ((j - i + 1) <= MAX_TREE_DEPTH + 1) {
    //                        if (j < 10) {
    //                            combinedNumMap[i][j] = combinedNumMap[i][j - 1] * 10 + j;
    //                        } else {
    //                            combinedNumMap[i][j] = combinedNumMap[i][j - 1] * 100 + j;
    //                        }
    //                    } else {
    //                        combinedNumMap[i][j] = 0;
    //                    }
    //                }
    //            }
    //        } else if (ch == 'R') {
    //
    //            int [] numbers = null;
    //            String asdStr = arg.substring(1);
    //            String [] numberStr = asdStr.split(",");
    //            numbers = new int[numberStr.length];
    //            int pre = Integer.MIN_VALUE;
    //            try {
    //                for (int i = 0; i < numbers.length; ++i) {
    //                    if (numberStr[i].isEmpty() || numberStr[i].charAt(0) > '9') {
    //                        //not a number
    //                        return "ILLEGAL";
    //                    }
    //                    numbers[i] = Integer.parseInt(numberStr[i]);
    //                    if (numbers[i] < 0 || numbers[i] < pre) {
    //                        //not larger than previous
    //                        return "ILLEGAL";
    //                    }
    //                    pre = numbers[i];
    //                }
    //            } catch (Exception e) {
    //                return "ILLEGAL";
    //            }
    //            result = new StringBuilder(64);
    //            //            MAX = numberStr.length / 2 + 1;
    //            //            MAX = (numberStr.length * numberStr[numberStr.length - 1].length()) / 2 + 1;
    //            //            if (MAX < numbers.length - 1) {
    //            //                MAX = numbers.length - 1;
    //            //            }
    //            //        System.out.println("1 input " + (System.currentTimeMillis() - start));
    //            //        start = System.currentTimeMillis();
    //            final int MAX_TREE_DEPTH = ((numbers.length % 2) == 0) ? numbers.length >> 1 : (numbers.length >> 1) + 1;
    //            //        System.out.println("MAX_TREE_DEPTH:" + MAX_TREE_DEPTH);
    //            //!!!
    //            //        MAX = numbers.length - 2;
    //            //        MAX = numbers.length / 2 + 1;
    //            //n<=15, MAX_TREE_DEPTH<=8, use int type
    //            if (MAX_TREE_DEPTH > 15) {
    //                return "ILLEGAL";
    //            }
    //
    //            final char [] opStr = {' ', '+', '-'};
    //            final int OP_NUM = 3;
    //            boolean isFirst = true;
    //
    //            //cache large number
    //            //row: startNum
    //            //column:endNum
    //            //save the index of number
    //            int [][] combinedNumMap = new int[numbers.length][numbers.length];
    //            for (int i = 0; i < numbers.length; ++i) {
    //                combinedNumMap[i][i] = numbers[i];
    //                for (int j = i + 1; j < numbers.length; ++j) {
    //                    //!!!limit the combined number
    //                    if ((j - i + 1) <= MAX_TREE_DEPTH + 1) {
    //                        combinedNumMap[i][j] = numbers[j];
    //                        long cur = combinedNumMap[i][j];
    //                        int c = 1;
    //                        do {
    //                            cur /= 10;
    //                            c *= 10;
    //                        } while (cur > 0);
    //                        combinedNumMap[i][j] = combinedNumMap[i][j - 1] * c + combinedNumMap[i][j];
    //                    } else {
    //                        combinedNumMap[i][j] = 0;
    //                    }
    //                }
    //            }
    //            int [] answerNumMap = new int[MAX_TREE_DEPTH + 1];
    //            for (int size = 1, answerNum = 1; size < answerNumMap.length; ++size, answerNum *= OP_NUM) {
    //                answerNumMap[size] = answerNum;
    //            }
    //            //////////////////////////////////
    //            //row: the idx is the size of set, the size of f(3,5,6,9) is 4, map[4][][]
    //            //column:start idx
    //            //element:value, order by ' ',+,-
    //            //if the value is not illegal, make sure it won't be used
    //
    //            //TODO decrease the number
    //            int [][][] results = new int[MAX_TREE_DEPTH + 1][numbers.length][];
    //            //initial the single value
    //            for (int startIdx = 0; startIdx < numbers.length; ++startIdx) {
    //                results[1][startIdx] = new int[1];
    //                results[1][startIdx][0] = numbers[startIdx];
    //            }
    //            /////////////////////////////////////////////////////////////////
    //
    //            //store op seq for each answer map
    //            //row:answer index
    //            //column: 
    //            //element:op 
    //            //!!!!pay attention out of memory...
    //            //TODO
    //            char [][][] opMap = new char[MAX_TREE_DEPTH + 1][][];
    //            for (int size = 1; size < opMap.length; ++size) {
    //                //last one stores if the op is valid!!!!
    //                opMap[size] = new char[answerNumMap[size]][size];
    //                for (int aIdx = 0; aIdx < opMap[size].length; ++aIdx) {
    //                    int t = aIdx;
    //                    int j = opMap[size][aIdx].length - 2;
    //                    while (j >= 0) {
    //                        opMap[size][aIdx][j] = (char)(t % OP_NUM);
    //                        t /= OP_NUM;
    //                        --j;
    //                        if (t == 0) {
    //                            for (; j >= 0; --j) {
    //                                opMap[size][aIdx][j] = (0);
    //                            }
    //                            break;
    //                        }
    //
    //                    }
    //                    //mark it valid
    //                    opMap[size][aIdx][opMap[size][aIdx].length - 1] = 1;
    //                }
    //
    //            }
    //            /////////////////////////////////////////////////
    //            final int NODE_LEN = 3;
    //            //partition
    //            //0:startidx
    //            //1:len
    //            //2:layer
    //            int [] unprocessedStack = new int[(MAX_TREE_DEPTH + 1) * NODE_LEN];
    //            //use the last one as idx
    //            int [][] processedQueue = new int[MAX_TREE_DEPTH][numbers.length * NODE_LEN];
    //            //        int layerIdx = 0;
    //
    //            //push the whole seq
    //            unprocessedStack[0] = 0;
    //            unprocessedStack[1] = numbers.length;
    //            //first layer is -1
    //            unprocessedStack[2] = -1;
    //            //point to the first unprocessed element
    //            int unprocessedStackIdx = NODE_LEN;
    //            //        int processedQueueIdx = 0;
    //            int maxLayer = 0;
    //            while (unprocessedStackIdx > 0) {
    //                //pop
    //                int startIdx = unprocessedStack[unprocessedStackIdx - 3];
    //                int len = unprocessedStack[unprocessedStackIdx - 2];
    //                int layer = unprocessedStack[unprocessedStackIdx - 1];
    //                unprocessedStackIdx -= NODE_LEN;
    //
    //                //test
    //                //            System.out.print("pop number [" + numbers[startIdx]);
    //                //            if (len > 1) {
    //                //                System.out.print(", " + numbers[(startIdx + len - 1)]);
    //                //            }
    //                //            System.out.println("]");
    //                //
    //                //
    //
    //                if (len <= 2) {
    //                    //                System.out.print("add to process queue, number [" + numbers[startIdx]);
    //                    //                if (len > 1) {
    //                    //                    System.out.print(", " + numbers[(startIdx + len - 1)]);
    //                    //                }
    //                    //                System.out.println("]");
    //                    //
    //                    //
    //                    if (results[1][startIdx] == null) {
    //                        results[1][startIdx] = new int[1];
    //                        results[1][startIdx][0] = numbers[startIdx];
    //                    }
    //                    if (len == 2) {
    //                        if (results[1][startIdx + 1] == null) {
    //                            results[1][startIdx + 1] = new int[1];
    //                            results[1][startIdx + 1][0] = numbers[startIdx + 1];
    //                        }
    //                        if (results[2][startIdx] == null) {
    //                            results[2][startIdx] = new int[answerNumMap[2]];
    //                        }
    //                        int v1 = results[1][startIdx][0];
    //                        int v2 = results[1][startIdx + 1][0];
    //                        for (int opMiddle = 0, idx = 0; opMiddle < OP_NUM; ++opMiddle, ++idx) {
    //                            switch (opMiddle) {
    //                                case 0:
    //                                    results[2][startIdx][idx] = combinedNumMap[startIdx][startIdx + 1];
    //                                    break;
    //                                case 1:
    //                                    results[2][startIdx][idx] = v1 + v2;
    //                                    break;
    //                                case 2:
    //                                    results[2][startIdx][idx] = v1 - v2;
    //                                    break;
    //                            }
    //                        }
    //                    }
    //                } else {
    //                    int nextLayer = layer + 1;
    //                    if (nextLayer > maxLayer) {
    //                        maxLayer = nextLayer;
    //                    }
    //                    //push to unproccesed queue
    //                    int secondStartIdx = startIdx + (len / 2);
    //                    int size1 = secondStartIdx - startIdx;
    //                    int size2 = len - size1;
    //                    //Test
    //                    //                                System.out.println("push:");
    //                    //                                System.out.println("idx [" + startIdx + ", " + (startIdx + size1 - 1) + "], [" + secondStartIdx + ","
    //                    //                                        + (secondStartIdx + size2 - 1) + "]");
    //                    //                
    //                    //                                System.out.print("push number [" + numbers[startIdx]);
    //                    //                                if (size1 > 1) {
    //                    //                                    System.out.print(", " + numbers[(startIdx + size1 - 1)]);
    //                    //                                }
    //                    //                                System.out.print("], [" + numbers[secondStartIdx]);
    //                    //                                if (size2 > 1) {
    //                    //                                    System.out.print("," + numbers[(secondStartIdx + size2 - 1)]);
    //                    //                                }
    //                    //                                System.out.println("], layer:"+nextLayer);
    //
    //                    //
    //
    //                    //push
    //                    unprocessedStack[unprocessedStackIdx] = secondStartIdx;
    //                    unprocessedStack[unprocessedStackIdx + 1] = size2;
    //                    unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
    //                    unprocessedStackIdx += NODE_LEN;
    //                    //
    //                    unprocessedStack[unprocessedStackIdx] = startIdx;
    //                    unprocessedStack[unprocessedStackIdx + 1] = size1;
    //                    unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
    //                    unprocessedStackIdx += NODE_LEN;
    //                    //
    //                    //
    //                    //                System.out.println("----------------------------");
    //                    int idx = processedQueue[nextLayer][processedQueue[nextLayer].length - 1];
    //
    //                    processedQueue[nextLayer][idx] = startIdx;
    //                    processedQueue[nextLayer][idx + 1] = size1;
    //                    processedQueue[nextLayer][idx + 2] = nextLayer;
    //
    //                    //                System.out.println(processedQueue[layerIdx][idx]);
    //                    //                System.out.println(processedQueue[layerIdx][idx+1]);
    //                    //                System.out.println(processedQueue[layerIdx][processedQueue[layerIdx].length - 1]);
    //
    //                    processedQueue[nextLayer][idx + 3] = secondStartIdx;
    //                    processedQueue[nextLayer][idx + 4] = size2;
    //                    processedQueue[nextLayer][idx + 5] = nextLayer;
    //
    //                    processedQueue[nextLayer][processedQueue[nextLayer].length - 1] += 6;
    //
    //                }
    //            }
    //            //        System.out.println("2 prepare " + (System.currentTimeMillis() - start));
    //            //        start = System.currentTimeMillis();
    //            //////////////////////////////////////////////////////////////
    //            //test
    //
    //            //        System.out.println("\nmerge");
    //            //        for (int i = maxLayer; i >= 0; --i) {
    //            //            //            for (int i = layerIdx - 1; i >= 0; --i) {
    //            //            for (int j = 0; j < processedQueue[i][processedQueue[i].length - 1]; j += NODE_LEN) {
    //            //                int startIdx = processedQueue[i][j];
    //            //                int len = processedQueue[i][j + 1];
    //            //                int layer = processedQueue[i][j + 2];
    //            //                //pop
    //            //                //test
    //            //                System.out.print("check number [" + numbers[startIdx]);
    //            //                if (len > 1) {
    //            //                    System.out.print(", " + numbers[(startIdx + len - 1)]);
    //            //                }
    //            //                System.out.println("] layer:" + layer);
    //            //                //
    //            //                //
    //            //                //
    //            //
    //            //            }
    //            //            System.out.println();
    //            //        }
    //            //
    //            //        System.out.println();
    //            //        System.out.println("processedQueueIdx:" + processedQueueIdx);
    //            //use queue like a stack here,
    //            //point to the head of the queue
    //            boolean isFinish = false;
    //            for (int layerIdx = maxLayer; layerIdx >= 0 && !isFinish; --layerIdx)
    //            //                int layerIdx = maxLayer; 
    //            {
    //                //            int headIdx = 0;
    //                int k = 0;
    //                int d = NODE_LEN;
    //                while (d <= numbers.length && !isFinish) {
    //                    int nextD = (d << 1);
    //                    //            System.out.println(d);
    //                    for (int qIdx = 0; qIdx < processedQueue[layerIdx].length - 1; qIdx += nextD) {
    //                        //                    System.out.println("d:" + d + ",next d:" + nextD + ",i:" + qIdx);
    //                        int startIdx = processedQueue[layerIdx][qIdx];
    //                        int size1 = processedQueue[layerIdx][qIdx + 1];
    //                        //
    //                        if (qIdx + d >= processedQueue[layerIdx][processedQueue[layerIdx].length - 1]) {
    //                            //                        System.out.println("finish");
    //                            break;
    //                        }
    //                        //
    //
    //                        int secondStartIdx = processedQueue[layerIdx][qIdx + d];
    //                        int size2 = processedQueue[layerIdx][qIdx + d + 1];
    //                        int size = size1 + size2;
    //
    //                        //                System.out.println("startIdx:" + startIdx);
    //                        //                System.out.println("size:" + size);
    //                        //                System.out.println("i=" + qIdx);
    //                        if (size <= MAX_TREE_DEPTH && results[size][startIdx] == null) {
    //                            results[size][startIdx] = new int[answerNumMap[size]];
    //                        }
    //
    //                        processedQueue[layerIdx][qIdx] = startIdx;
    //                        processedQueue[layerIdx][qIdx + 1] = size;
    //
    //                        //                    System.out.print("process number [" + numbers[startIdx]);
    //                        //                    if (size1 > 1) {
    //                        //                        System.out.print(", " + numbers[(startIdx + size1 - 1)]);
    //                        //                    }
    //                        //                    System.out.print("], [" + numbers[secondStartIdx]);
    //                        //                    if (size2 > 1) {
    //                        //                        System.out.print("," + numbers[(secondStartIdx + size2 - 1)]);
    //                        //                    }
    //                        //                    System.out.println("]");
    //
    //                        ////////////////////////////////////
    //                        //            /////////////////////////////////////////////////////////////
    //                        //
    //
    //                        int [] v1 = results[size1][startIdx];
    //                        int [] v2 = results[size2][secondStartIdx];
    //                        //                        System.out.println(Arrays.toString(v1));
    //                        //                        System.out.println(Arrays.toString(v2));
    //                        int idx = 0;
    //                        int value = -1;
    //                        //1) first part size is 1                            
    //                        for (int i = 0; i < v1.length; ++i) {
    //                            char [] op1 = opMap[size1][i];
    //                            if (op1[op1.length - 1] == 0) {
    //                                idx += (OP_NUM * v2.length);
    //                            } else {
    //                                //                                System.out.println("1) op1[op1.length - 1] == 0");
    //                                int space1 = 0;
    //                                int k1 = op1.length - 2;
    //                                while (k1 >= 0) {
    //                                    if (op1[k1] == 0) {
    //                                        ++space1;
    //                                        --k1;
    //                                    } else {
    //                                        break;
    //                                    }
    //                                }
    //                                if (space1 >= MAX_TREE_DEPTH) {
    //                                    //                                System.out.println("2) space1 >= MAX_TREE_DEPTH");
    //                                    //mark it invalid
    //                                    op1[op1.length - 1] = 0;
    //                                    idx += (OP_NUM * v2.length);
    //                                } else {
    //                                    //2) middle op
    //                                    for (int opMiddle = 0; opMiddle < OP_NUM; ++opMiddle) {
    //                                        for (int j = 0; j < v2.length; ++j) {
    //                                            char [] op2 = opMap[size2][j];
    //                                            if (op2[op2.length - 1] > 0) {
    //                                                boolean isMergedWidthBeyond = false;
    //                                                switch (opMiddle) {
    //                                                    case 0: {
    //                                                        int space2 = 0;
    //                                                        k1 = 0;
    //                                                        while (k1 < op2.length - 1) {
    //                                                            if (op2[k1] == 0) {
    //                                                                ++space2;
    //                                                                ++k1;
    //                                                            } else {
    //                                                                break;
    //                                                            }
    //                                                        }
    //                                                        if (space2 >= MAX_TREE_DEPTH) {
    //                                                            //                                                value = Long.MIN_VALUE;
    //                                                            //mark it invalid
    //                                                            op2[op2.length - 1] = 0;
    //                                                            //                                                System.out.println("4) op2[op2.length - 1] == 0");
    //
    //                                                            break;//for switch
    //                                                        }
    //                                                        if (space1 + space2 + 1 >= MAX_TREE_DEPTH) {
    //                                                            isMergedWidthBeyond = true;
    //                                                            break;
    //                                                        }
    //
    //                                                        value = v1[i] + v2[j];
    //                                                        //second prefix
    //                                                        value -= combinedNumMap[secondStartIdx][secondStartIdx
    //                                                                + space2];
    //                                                        //                                                System.out.println("MAX:"+ MAX+", width:" + (secondStartIdx
    //                                                        //                                                        + space2 - (secondStartIdx - 1 - space1) + 1));
    //                                                        if (op1.length - 2 - space1 >= 0) {//there is other op before space in first part
    //                                                            if (op1[op1.length - space1 - 2] == 1) {//+
    //                                                                //first postfix
    //                                                                value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
    //                                                                value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
    //                                                                        + space2];
    //                                                            } else if (op1[op1.length - space1 - 2] == 2) {//-
    //                                                                value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
    //                                                                value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
    //                                                                        + space2];
    //                                                            } else {
    //                                                                //                                                    System.out.println("error");
    //                                                            }
    //
    //                                                        } else {
    //
    //                                                            value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
    //                                                            //may be larger than MAX width
    //                                                            value += combinedNumMap[startIdx][secondStartIdx + space2];
    //                                                        }
    //                                                        //                                                System.out.println(combinedNumMap[startIdx][secondStartIdx + space2]);
    //                                                    }
    //                                                        break;
    //                                                    case 1:
    //                                                        value = v1[i] + v2[j];
    //                                                        break;
    //                                                    case 2: {
    //                                                        //!!!!!substract, braceket...
    //                                                        //only first number need to change to substract
    //                                                        int preSpace = 0;
    //                                                        for (k = 0; k < op2.length - 1; ++k) {
    //                                                            if (op2[k] != 0) {
    //                                                                break;
    //                                                            } else {
    //                                                                ++preSpace;
    //                                                            }
    //                                                        }
    //                                                        if (preSpace >= MAX_TREE_DEPTH) {
    //                                                            //mark it invalid
    //                                                            op2[op2.length - 1] = 0;
    //                                                            break;
    //                                                        }
    //                                                        value = v1[i]
    //                                                                - 2
    //                                                                * combinedNumMap[secondStartIdx][secondStartIdx
    //                                                                        + preSpace]
    //                                                                + v2[j];
    //                                                    }
    //                                                        break;
    //                                                }
    //                                                if (size <= MAX_TREE_DEPTH) {
    //                                                    results[size][startIdx][idx] = value;
    //                                                }
    //                                                if (value == 0 && size == numbers.length && op1[op1.length - 1] > 0
    //                                                        && op2[op2.length - 1] > 0 && !isMergedWidthBeyond) {
    //                                                    if (isFirst) {
    //                                                        isFirst = false;
    //                                                    } else {
    //                                                        result.append(';');
    //                                                    }
    //                                                    int numIdx = 0;
    //                                                    //                                                System.out.println("op1:" + op1.length + ",op2:" + op2.length);
    //                                                    for (numIdx = 0; numIdx < op1.length - 1; ++numIdx) {
    //                                                        result.append(numbers[numIdx]);
    //                                                        //construct string
    //                                                        result.append(opStr[op1[numIdx]]);
    //                                                    }
    //                                                    result.append(numbers[numIdx]);
    //                                                    result.append(opStr[opMiddle]);
    //                                                    ++numIdx;
    //                                                    for (k = 0; k < op2.length - 1; ++k, ++numIdx) {
    //                                                        result.append(numbers[numIdx]);
    //                                                        //construct string
    //                                                        result.append(opStr[op2[k]]);
    //                                                    }
    //                                                    result.append(numbers[numIdx]);
    //                                                }
    //                                            }
    //                                            ++idx;
    //                                        }
    //                                    }
    //                                }
    //                            }
    //                        }
    //                        if (size == numbers.length) {
    //                            isFinish = true;
    //                            //                        System.out.println("isfinish");
    //                            break;
    //                        }
    //                    }
    //
    //                    d = nextD;
    //                }
    //            }
    //
    //        } else {
    //            return "ILLEGAL";
    //        }
    //
    //        //        System.out.println("3 calc and output " + (System.currentTimeMillis() - start));
    //        //        start = System.currentTimeMillis();
    //        if (result.length() == 0) {
    //            return "NOTFOUND";
    //        }
    //        return result.toString();
    //    }

    //XXX not correct
    public static String run9(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        long start = System.currentTimeMillis();
        StringBuilder result = null;
        //XXX is not for >15 digit number count
        int [] numbers = null;
        //TODO
        int MAX_DIGIT_NUM = 0;
        int TOTAL_DIGIT_NUM = 0;
        //        int [] numberDigit = null;
        int [][] combinedNumDigitMap = null;
        //        int MAX = 0;
        char ch = arg.charAt(0);
        if (ch == 'S') {
            //write a table contains 3~15???
            int n = 0;
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            //3<=N<=15
            if (n < 3 || n > 15) {
                return "ILLEGAL";
            }
            numbers = new int[n];

            ///////////////////////////////////////////
            //TODO calc digit data
            if (n < 10) {
                TOTAL_DIGIT_NUM = n;
            } else {
                TOTAL_DIGIT_NUM = (n - 10 + 1) * 2 + (10 - 1);
            }
            MAX_DIGIT_NUM = TOTAL_DIGIT_NUM / 2;
            //            if (TOTAL_DIGIT_NUM % 2 != 0) {
            //                ++MAX_DIGIT_NUM;
            //            }
            ///////////////////////////////////////////
            //row:startIdx
            //column:endIdx
            //value:the total digit of the number constructed from startidx to endidx
            //if value is zero, it means invalid startidx and endidx, or beyond max digit
            combinedNumDigitMap = new int[n][n];
            int w = 10;
            int digit = 1;
            for (int i = 0; i < n; ++i) {
                while (true) {
                    if ((i + 1) / w == 0) {
                        combinedNumDigitMap[i][0] = digit;
                        break;
                    }
                    w *= 10;
                    ++digit;
                }
                numbers[i] = i + 1;
            }

            for (int i = 0; i < n; ++i) {
                for (int j = i + 1; j < n; ++j) {
                    combinedNumDigitMap[i][j] = combinedNumDigitMap[j][0] + combinedNumDigitMap[i][j - 1];
                    if (combinedNumDigitMap[i][j] > MAX_DIGIT_NUM) {
                        combinedNumDigitMap[i][j] = 0;
                        break;
                    }
                }
                System.out.println(Arrays.toString(combinedNumDigitMap[i]));
            }
            ///////////////////////////////////////////

            //various buffer size
            result = new StringBuilder(2 << n);
            //            MAX = n / 2 + 1;
        } else if (ch == 'R') {
            String asdStr = arg.substring(1);
            String [] numberStr = asdStr.split(",");
            numbers = new int[numberStr.length];
            int pre = Integer.MIN_VALUE;
            try {
                for (int i = 0; i < numbers.length; ++i) {
                    if (numberStr[i].isEmpty() || numberStr[i].charAt(0) > '9') {
                        //not a number
                        return "ILLEGAL";
                    }
                    numbers[i] = Integer.parseInt(numberStr[i]);
                    if (numbers[i] < 0 || numbers[i] < pre) {
                        //not larger than previous
                        return "ILLEGAL";
                    }
                    pre = numbers[i];
                }
            } catch (Exception e) {
                return "ILLEGAL";
            }
            //TODO
            combinedNumDigitMap = new int[numbers.length][numbers.length];

            result = new StringBuilder(64);
            //            MAX = numberStr.length / 2 + 1;
            //            MAX = (numberStr.length * numberStr[numberStr.length - 1].length()) / 2 + 1;
            //            if (MAX < numbers.length - 1) {
            //                MAX = numbers.length - 1;
            //            }

        } else {
            return "ILLEGAL";
        }

        //        int weight=10;
        //        int i=0;
        //        while(i<numbers.length){
        //            numbers[i]/weight
        //        }

        System.out.println("Total digit num:" + TOTAL_DIGIT_NUM);
        System.out.println("Max digit num:" + MAX_DIGIT_NUM);
        //        System.out.println("Digit array:" + Arrays.toString(numberDigit));
        //TODO use big integer number
        if (MAX_DIGIT_NUM > 15) {
            return "ILLEGAL";
        }
        //XXX e.g. 1,2,3,4,5,12345
        final int MAX_TREE_DEPTH = ((numbers.length % 2) == 0) ? numbers.length >> 1 : (numbers.length >> 1) + 1;
        System.out.println("MAX_TREE_DEPTH:" + MAX_TREE_DEPTH);
        //!!!
        //        MAX = numbers.length - 2;
        //        MAX = numbers.length / 2 + 1;
        //n<=15, MAX_TREE_DEPTH<=8, use int type

        final char [] opStr = {' ', '+', '-'};
        final int OP_NUM = 3;
        boolean isFirst = true;

        //cache large number
        //row: startNum
        //column:endNum
        //save the index of number
        long [][] combinedNumMap = new long[numbers.length][numbers.length];

        if (ch == 'S') {
            for (int i = 0; i < numbers.length; ++i) {
                combinedNumMap[i][i] = numbers[i];
                for (int j = i + 1; j < numbers.length; ++j) {
                    //!!!limit the combined number
                    if (combinedNumDigitMap[i][j] > 0) {
                        if (numbers[j] < 10) {
                            combinedNumMap[i][j] = combinedNumMap[i][j - 1] * 10 + numbers[j];
                        } else {
                            combinedNumMap[i][j] = combinedNumMap[i][j - 1] * 100 + numbers[j];
                        }
                    } else {
                        break;
                    }
                }
            }
        } else {
            for (int i = 0; i < numbers.length; ++i) {
                combinedNumMap[i][i] = numbers[i];
                for (int j = i + 1; j < numbers.length; ++j) {
                    //!!!limit the combined number
                    if (combinedNumDigitMap[i][j] > 0) {
                        //TODO use combinedNumDigitMap to calc the combined number
                        combinedNumMap[i][j] = numbers[j];
                        long cur = combinedNumMap[i][j];
                        int c = 1;
                        do {
                            cur /= 10;
                            c *= 10;
                        } while (cur > 0);
                        combinedNumMap[i][j] = combinedNumMap[i][j - 1] * c + combinedNumMap[i][j];
                    } else {
                        combinedNumMap[i][j] = 0;
                    }
                }
            }
        }
        //correct to use MAX_TREE_DEPTH
        int [] answerNumMap = new int[MAX_TREE_DEPTH + 1];
        for (int size = 1, answerNum = 1; size < answerNumMap.length; ++size, answerNum *= OP_NUM) {
            answerNumMap[size] = answerNum;
        }
        //////////////////////////////////
        //row: the idx is the size of set, the size of f(3,5,6,9) is 4, map[4][][]
        //column:start idx
        //element:value, order by ' ',+,-
        //if the value is not illegal, make sure it won't be used

        //decrease the number
        //correct to use MAX_TREE_DEPTH
        long [][][] results = new long[MAX_TREE_DEPTH + 1][numbers.length][];
        //initial the single value
        for (int startIdx = 0; startIdx < numbers.length; ++startIdx) {
            results[1][startIdx] = new long[1];
            results[1][startIdx][0] = numbers[startIdx];
        }
        /////////////////////////////////////////////////////////////////

        //store op seq for each answer map
        //row:size
        //column: answer index
        //element:op 
        //!!!!pay attention out of memory...
        //the space count won't larger than MAX_TREE_DEPTH for single set
        //except merged set
        //correct to use MAX_TREE_DEPTH
        char [][][] opMap = new char[MAX_TREE_DEPTH + 1][][];
        for (int size = 1; size < opMap.length; ++size) {
            //last one stores if the op is valid!!!!
            opMap[size] = new char[answerNumMap[size]][size];
            //correct to use MAX_TREE_DEPTH,it's about op len
            if (opMap[size][0].length - 1 <= MAX_TREE_DEPTH) {
                opMap[size][0][opMap[size][0].length - 1] = 1;
            }
            //by default, it invalid
            for (int aIdx = 1; aIdx < opMap[size].length; ++aIdx) {
                int t = aIdx;
                int j = opMap[size][aIdx].length - 2;
                while (j >= 0) {//store op
                    opMap[size][aIdx][j] = (char)(t % OP_NUM);
                    t /= OP_NUM;
                    --j;
                    if (t == 0) {
                        for (; j >= 0; --j) {
                            opMap[size][aIdx][j] = (0);
                        }
                        break;
                    }

                }
                //mark it valid
                opMap[size][aIdx][opMap[size][aIdx].length - 1] = 1;
            }
        }
        //test
        //        for (int size = 2; size < opMap.length; ++size) {
        //            System.out.println(opMap[size].length + ":");
        //            for (int i = 0; i < opMap[size].length; ++i) {
        //                for (int j = 0; j < opMap[size][i].length - 1; ++j) {
        //                    //                    System.out.println(Arrays.toString(opStr[opMap[size][i]]));
        //                    System.out.print((int)opMap[size][i][j]);
        ////                    System.out.print(opStr[opMap[size][i][j]] + " ");
        //                }
        //                System.out.println(",");
        //            }
        //            System.out.println();
        //
        //        }
        //for test
        for (int i = 0; i < numbers.length; ++i) {
            System.out.print(numbers[i] + " ");
            for (int j = i + 1; j < numbers.length; ++j) {
                if (combinedNumMap[i][j] != 0)
                    System.out.print(combinedNumMap[i][j] + " ");
                else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        System.out.println();
        //
        //
        /////////////////////////////////////////////////
        final int NODE_LEN = 3;
        //partition
        //0:startidx
        //1:len
        //2:layer
        int [] unprocessedStack = new int[(MAX_TREE_DEPTH + 1) * NODE_LEN];
        //use the last one as idx
        int [][] processedQueue = new int[MAX_TREE_DEPTH][numbers.length * NODE_LEN];
        //        int layerIdx = 0;

        //push the whole seq
        unprocessedStack[0] = 0;
        unprocessedStack[1] = numbers.length;
        //first layer is -1
        unprocessedStack[2] = -1;
        //point to the first unprocessed element
        int unprocessedStackIdx = NODE_LEN;
        //        int processedQueueIdx = 0;
        int maxLayer = 0;
        while (unprocessedStackIdx > 0) {
            //pop
            int startIdx = unprocessedStack[unprocessedStackIdx - 3];
            int len = unprocessedStack[unprocessedStackIdx - 2];
            int layer = unprocessedStack[unprocessedStackIdx - 1];
            unprocessedStackIdx -= NODE_LEN;

            //test
            //            System.out.print("pop number [" + numbers[startIdx]);
            //            if (len > 1) {
            //                System.out.print(", " + numbers[(startIdx + len - 1)]);
            //            }
            //            System.out.println("]");
            //
            //

            if (len <= 2) {
                //                System.out.print("add to process queue, number [" + numbers[startIdx]);
                //                if (len > 1) {
                //                    System.out.print(", " + numbers[(startIdx + len - 1)]);
                //                }
                //                System.out.println("]");
                //
                //
                if (results[1][startIdx] == null) {
                    results[1][startIdx] = new long[1];
                    results[1][startIdx][0] = numbers[startIdx];
                }
                if (len == 2) {
                    if (results[1][startIdx + 1] == null) {
                        results[1][startIdx + 1] = new long[1];
                        results[1][startIdx + 1][0] = numbers[startIdx + 1];
                    }
                    if (results[2][startIdx] == null) {
                        results[2][startIdx] = new long[answerNumMap[2]];
                    }
                    long v1 = results[1][startIdx][0];
                    long v2 = results[1][startIdx + 1][0];
                    for (int opMiddle = 0, idx = 0; opMiddle < OP_NUM; ++opMiddle, ++idx) {
                        switch (opMiddle) {
                            case 0:
                                results[2][startIdx][idx] = combinedNumMap[startIdx][startIdx + 1];
                                break;
                            case 1:
                                results[2][startIdx][idx] = v1 + v2;
                                break;
                            case 2:
                                results[2][startIdx][idx] = v1 - v2;
                                break;
                        }
                    }
                }
            } else {
                int nextLayer = layer + 1;
                if (nextLayer > maxLayer) {
                    maxLayer = nextLayer;
                }
                //push to unproccesed queue
                int secondStartIdx = startIdx + (len / 2);
                int size1 = secondStartIdx - startIdx;
                int size2 = len - size1;

                //push
                unprocessedStack[unprocessedStackIdx] = secondStartIdx;
                unprocessedStack[unprocessedStackIdx + 1] = size2;
                unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
                unprocessedStackIdx += NODE_LEN;
                //
                unprocessedStack[unprocessedStackIdx] = startIdx;
                unprocessedStack[unprocessedStackIdx + 1] = size1;
                unprocessedStack[unprocessedStackIdx + 2] = nextLayer;
                unprocessedStackIdx += NODE_LEN;
                //
                //
                //                System.out.println("----------------------------");
                int idx = processedQueue[nextLayer][processedQueue[nextLayer].length - 1];

                processedQueue[nextLayer][idx] = startIdx;
                processedQueue[nextLayer][idx + 1] = size1;
                processedQueue[nextLayer][idx + 2] = nextLayer;

                //                System.out.println(processedQueue[layerIdx][idx]);
                //                System.out.println(processedQueue[layerIdx][idx+1]);
                //                System.out.println(processedQueue[layerIdx][processedQueue[layerIdx].length - 1]);

                processedQueue[nextLayer][idx + 3] = secondStartIdx;
                processedQueue[nextLayer][idx + 4] = size2;
                processedQueue[nextLayer][idx + 5] = nextLayer;

                processedQueue[nextLayer][processedQueue[nextLayer].length - 1] += 6;

            }
        }
        //        System.out.println("2 prepare " + (System.currentTimeMillis() - start));
        //        start = System.currentTimeMillis();
        //////////////////////////////////////////////////////////////

        //use queue like a stack here,
        //point to the head of the queue

        System.out.println("1 prepare " + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        //test
        //        int calcCount = 0;
        /////////////////////////////////////////////////////////////////////////////
        //TODO
        boolean isFinish = false;
        for (int layerIdx = maxLayer; layerIdx >= 0 && !isFinish; --layerIdx) {
            int k = 0;
            int d = NODE_LEN;
            while (d <= numbers.length && !isFinish) {
                int nextD = (d << 1);
                //            System.out.println(d);
                for (int qIdx = 0; qIdx < processedQueue[layerIdx].length - 1; qIdx += nextD) {
                    if (qIdx + d >= processedQueue[layerIdx][processedQueue[layerIdx].length - 1]) {
                        break;
                    }
                    //                    System.out.println("d:" + d + ",next d:" + nextD + ",i:" + qIdx);
                    int startIdx = processedQueue[layerIdx][qIdx];
                    int size1 = processedQueue[layerIdx][qIdx + 1];
                    int secondStartIdx = processedQueue[layerIdx][qIdx + d];
                    int size2 = processedQueue[layerIdx][qIdx + d + 1];
                    int size = size1 + size2;

                    //                System.out.println("startIdx:" + startIdx);
                    //                System.out.println("size:" + size);
                    //                System.out.println("i=" + qIdx);
                    char [][] opAll = null;
                    int opAllIdx = 0;
                    if (size <= MAX_TREE_DEPTH) {
                        opAll = opMap[size];
                        if (results[size][startIdx] == null) {
                            results[size][startIdx] = new long[answerNumMap[size]];
                        }
                    }

                    processedQueue[layerIdx][qIdx] = startIdx;
                    processedQueue[layerIdx][qIdx + 1] = size;
                    long [] v1 = results[size1][startIdx];
                    long [] v2 = results[size2][secondStartIdx];

                    ////////////////////////////////////
                    //test
                    //                    System.out.print("process number [" + numbers[startIdx]);
                    //                    if (size1 > 1) {
                    //                        System.out.print(", " + numbers[(startIdx + size1 - 1)]);
                    //                    }
                    //                    System.out.print("] (" + v1.length + ") , [" + numbers[secondStartIdx]);
                    //                    if (size2 > 1) {
                    //                        System.out.print("," + numbers[(secondStartIdx + size2 - 1)]);
                    //                    }
                    //                    System.out.println("] (" + v2.length + ")");
                    //            /////////////////////////////////////////////////////////////
                    //                        System.out.println(Arrays.toString(v1));
                    //                        System.out.println(Arrays.toString(v2));

                    long value = -1;

                    //1) first part size is 1                            
                    for (int i = 0; i < v1.length; ++i) {
                        char [] op1 = opMap[size1][i];
                        if (op1[op1.length - 1] == 0) {//[invalid1]this op seq is invalid
                            if (opAll != null) {
                                opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;

                            }
                            //                            System.out.println("op1 is invalid");
                            opAllIdx += (OP_NUM * v2.length);
                            //                                System.out.println("1) op1[op1.length - 1] == 0");
                        } else {

                            int space1 = 0;
                            int k1 = op1.length - 2;
                            while (k1 >= 0) {
                                if (op1[k1] == 0) {
                                    ++space1;
                                    --k1;
                                } else {
                                    break;
                                }
                            }
                            //2) middle op
                            for (int opMiddle = 0; opMiddle < OP_NUM; ++opMiddle) {
                                for (int j = 0; j < v2.length; ++j) {
                                    char [] op2 = opMap[size2][j];
                                    if (op2[op2.length - 1] > 0) {
                                        boolean isMergedWidthBeyond = false;

                                        switch (opMiddle) {
                                            case 0: {
                                                int space2 = 0;
                                                int k2 = 0;
                                                while (k2 < op2.length - 1) {
                                                    if (op2[k2] == 0) {
                                                        ++space2;
                                                        ++k2;
                                                    } else {
                                                        break;
                                                    }
                                                }

                                                //[invalid2] merged width is larger than depth
                                                //this case only happened in last time
                                                //                                                if (space1 + space2 + 1 >= MAX_TREE_DEPTH) {
                                                //                                                    isMergedWidthBeyond = true;
                                                //                                                    if (opAll != null) {
                                                //                                                        opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                                //                                                        System.out.println("1) isMergedWidthBeyond = true");
                                                //                                                    }
                                                //                                                    //                                                   
                                                //                                                    break;
                                                //                                                }

                                                //                                                System.out.println("MAX:"+ MAX+", width:" + (secondStartIdx
                                                //                                                        + space2 - (secondStartIdx - 1 - space1) + 1));
                                                if (op1.length - 2 - space1 >= 0) {//there is other op before space in first part
                                                    //TODO                                                    
                                                    if (combinedNumDigitMap[startIdx + op1.length - 1 - space1][secondStartIdx
                                                            + space2] == 0) {
                                                        isMergedWidthBeyond = true;
                                                        if (opAll != null) {
                                                            opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;

                                                            System.out.println("2) isMergedWidthBeyond = true, all("
                                                                    + numbers[startIdx] + ","
                                                                    + numbers[startIdx + size1 - 1] + ") ("
                                                                    + numbers[secondStartIdx] + ","
                                                                    + numbers[secondStartIdx + size2 - 1] + ") ("
                                                                    + numbers[startIdx + op1.length - 2 - space1] + ","
                                                                    + numbers[secondStartIdx
                                                                            + space2] + ")"
                                                                    + space1 + "," + space2
                                                                    );
                                                        }
                                                        //                                                   
                                                        break;
                                                    }
                                                    if (op1[op1.length - space1 - 2] == 1) {//+
                                                        //first postfix
                                                        value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                        value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                + space2];
                                                    } else if (op1[op1.length - space1 - 2] == 2) {//-
                                                        value += combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                        value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx
                                                                + space2];
                                                    } else {
                                                        //                                                    System.out.println("error");
                                                    }

                                                } else {
                                                    if (combinedNumDigitMap[startIdx][secondStartIdx
                                                            + space2] == 0) {
                                                        isMergedWidthBeyond = true;
                                                        if (opAll != null) {
                                                            opAll[opAllIdx][opAll[opAllIdx].length - 1] = 0;
                                                            System.out.println("3) isMergedWidthBeyond = true");
                                                        }
                                                        //                                                   
                                                        break;
                                                    }
                                                    value -= combinedNumMap[secondStartIdx - 1 - space1][secondStartIdx - 1];
                                                    value += combinedNumMap[startIdx][secondStartIdx + space2];
                                                }

                                                value = v1[i] + v2[j];
                                                //second prefix
                                                value -= combinedNumMap[secondStartIdx][secondStartIdx
                                                        + space2];
                                                //                                                    ++calcCount;
                                                //                                                System.out.println(combinedNumMap[startIdx][secondStartIdx + space2]);
                                            }
                                                break;
                                            case 1:
                                                value = v1[i] + v2[j];
                                                //                                                    ++calcCount;
                                                break;
                                            case 2: {
                                                //!!!!!substract, braceket...
                                                //only first number need to change to substract
                                                int preSpace = 0;
                                                for (k = 0; k < op2.length - 1; ++k) {
                                                    if (op2[k] != 0) {
                                                        break;
                                                    } else {
                                                        ++preSpace;
                                                    }
                                                }
                                                value = v1[i]
                                                        - 2
                                                        * combinedNumMap[secondStartIdx][secondStartIdx
                                                                + preSpace]
                                                        + v2[j];
                                                //                                                    ++calcCount;
                                            }
                                                break;
                                        }

                                        if (size <= MAX_TREE_DEPTH) {
                                            results[size][startIdx][opAllIdx] = value;
                                        }
                                        //construct string cost 0ms
                                        if (value == 0 && size == numbers.length && op1[op1.length - 1] > 0
                                                && op2[op2.length - 1] > 0 && !isMergedWidthBeyond) {

                                            //                                              System.out.println("2 prepare " + (System.currentTimeMillis() - start));  
                                            //                                                start = System.currentTimeMillis();
                                            if (isFirst) {
                                                isFirst = false;
                                            } else {
                                                result.append(';');
                                            }
                                            int numIdx = 0;
                                            //                                                System.out.println("op1:" + op1.length + ",op2:" + op2.length);
                                            for (numIdx = 0; numIdx < op1.length - 1; ++numIdx) {
                                                result.append(numbers[numIdx]);
                                                //construct string
                                                result.append(opStr[op1[numIdx]]);
                                            }
                                            result.append(numbers[numIdx]);
                                            result.append(opStr[opMiddle]);
                                            ++numIdx;
                                            for (k = 0; k < op2.length - 1; ++k, ++numIdx) {
                                                result.append(numbers[numIdx]);
                                                //construct string
                                                result.append(opStr[op2[k]]);
                                            }
                                            result.append(numbers[numIdx]);

                                            //                                                start = System.currentTimeMillis();
                                            //                                                strTime += (System.currentTimeMillis() - start);

                                        }
                                    }
                                    //                                    else {
                                    //                                        System.out.println("op2 is invalid");
                                    //                                    }
                                    ++opAllIdx;
                                }
                            }

                        }
                    }
                    if (size == numbers.length) {
                        isFinish = true;
                        //                        System.out.println("isfinish");
                        break;
                    }
                }

                d = nextD;
            }
        }
        //        System.out.println("construct string time: " + strTime + " ms");
        System.out.println("2 calc and output " + (System.currentTimeMillis() - start));
        //        System.out.println("calc count:" + calcCount);
        //        start = System.currentTimeMillis();
        if (result.length() == 0) {
            return "NOTFOUND";
        }
        return result.toString();
    }

    //BF
    public static String run5BF(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        StringBuilder result = null;
        int [] numbers = null;
        //        int MAX = 0;

        int MAX_DIGIT_NUM = 0;
        int TOTAL_DIGIT_NUM = 0;

        int [][] combinedNumDigitMap = null;
        char ch = arg.charAt(0);
        if (ch == 'S') {
            int n = 0;
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            //3<=N<=15
            if (n < 3 || n > 15) {
                return "ILLEGAL";
            }
            numbers = new int[n];
            for (int i = 0; i < numbers.length; ++i) {
                numbers[i] = i + 1;
            }
            //various buffer size
            result = new StringBuilder(2 << n);
            //            int bufferSize=(1<<(n+1));
            //            if(n>12){
            //                bufferSize=50000;
            //            }else if(n>7){
            //                bufferSize=10000;
            //            }
            //            result = new StringBuilder();
            ///////////////////////////////////////////
            //calc digit data
            if (n < 10) {
                TOTAL_DIGIT_NUM = n;
            } else {
                TOTAL_DIGIT_NUM = (n - 10 + 1) * 2 + (10 - 1);
            }
            //???
            MAX_DIGIT_NUM = TOTAL_DIGIT_NUM / 2;
            //            MAX_DIGIT_NUM=6;

            ///////////////////////////////////////////
            //row:startIdx
            //column:endIdx
            //value:the total digit of the number constructed from startidx to endidx
            //if value is zero, it means invalid startidx and endidx, or beyond max digit
            combinedNumDigitMap = new int[n][n];
            int w = 10;
            int digit = 1;
            for (int i = 0; i < n; ++i) {
                while (true) {
                    if ((i + 1) / w == 0) {
                        combinedNumDigitMap[i][i] = digit;
                        break;
                    }
                    w *= 10;
                    ++digit;
                }
                numbers[i] = i + 1;
                //                System.out.println(Arrays.toString(combinedNumDigitMap[i]));
            }
            //            System.out.println();

            for (int i = 0; i < n; ++i) {
                for (int j = i + 1; j < n; ++j) {
                    combinedNumDigitMap[i][j] = combinedNumDigitMap[i][i] + combinedNumDigitMap[i][j - 1];
                    if (combinedNumDigitMap[i][j] > MAX_DIGIT_NUM) {
                        combinedNumDigitMap[i][j] = 0;
                        break;
                    }
                }
                //                System.out.println(Arrays.toString(combinedNumDigitMap[i]));
            }
            ///////////////////////////////////////////            
        } else if (ch == 'R') {
            String asdStr = arg.substring(1);
            String [] numberStr = asdStr.split(",");
            numbers = new int[numberStr.length];
            int pre = Integer.MIN_VALUE;
            try {
                for (int i = 0; i < numbers.length; ++i) {
                    if (numberStr[i].isEmpty() || numberStr[i].charAt(0) > '9') {
                        //not a number
                        return "ILLEGAL";
                    }
                    numbers[i] = Integer.parseInt(numberStr[i]);
                    if (numbers[i] < 0 || numbers[i] < pre) {
                        //not larger than previous
                        return "ILLEGAL";
                    }
                    pre = numbers[i];
                }
            } catch (Exception e) {
                return "ILLEGAL";
            }
            result = new StringBuilder(128);
            //TODO
            combinedNumDigitMap = new int[numbers.length][numbers.length];

        } else {
            return "ILLEGAL";
        }
        if (numbers == null || result == null) {
            return "ILLEGAL";
        }
        if (MAX_DIGIT_NUM > 15) {
            //!!!have to use array for number
            return "ILLEGAL";
        }
        //        System.out.println("Total digit num:" + TOTAL_DIGIT_NUM);
        System.out.println("Max digit num:" + MAX_DIGIT_NUM);
        //test
        //            System.out.println(Arrays.toString(numbers));
        //            int maxdigit = (asdStr.length() - numberStr.length + 1) / 2;
        //            System.out.println("max digit num:" + maxdigit);

        //XXX e.g. 1,2,3,4,5,12345
        //        final int MAX_SPACE_NUM = ((numbers.length % 2) == 0) ? numbers.length >> 1 : (numbers.length >> 1) + 1;
        //        System.out.println("MAX_SPACE_NUM:" + MAX_SPACE_NUM);
        long [][] combinedNumMap = new long[numbers.length][numbers.length];
        for (int i = 0; i < numbers.length; ++i) {
            combinedNumMap[i][i] = numbers[i];
            for (int j = i + 1; j < numbers.length; ++j) {
                //!!!limit the combined number
                if (combinedNumDigitMap[i][j] > 0) {
                    //TODO use combinedNumDigitMap to calc the combined number
                    combinedNumMap[i][j] = numbers[j];
                    long cur = combinedNumMap[i][j];
                    int c = 1;
                    do {
                        cur /= 10;
                        c *= 10;
                    } while (cur > 0);
                    combinedNumMap[i][j] = combinedNumMap[i][j - 1] * c + combinedNumMap[i][j];
                } else {
                    break;
                }
            }
        }

        /////////////////////////////////////////////////
        //for test
        //        for (int i = 0; i < numbers.length; ++i) {
        //            System.out.print(numbers[i] + " ");
        //            for (int j = i + 1; j < numbers.length; ++j) {
        //                if (combinedNumMap[i][j] != 0)
        //                    System.out.print(combinedNumMap[i][j] + " ");
        //                else {
        //                    System.out.print("  ");
        //                }
        //            }
        //            System.out.println();
        //        }
        //        System.out.println();
        /////////////////////////////////////////////////

        //' '：0
        //+：1
        //-： 2
        //brute force
        //for current op idx
        char [] opFlag = new char[numbers.length - 1];
        //op sequence cache
        char [] opCache = new char[numbers.length - 1];
        //        final int OP_SPACE = 0;
        //        final int OP_PLUS = 1;
        //        final int OP_MINUS = 2;
        //first op idx
        int opSeqOrder = 0; //
        boolean isFirst = true;

        //        int [] opTypeCount = new int[3];
        //        int [] preTypeCount = new int[3];
        //row:op order
        //column: op type
        int [][] opTypeCount = new int[opCache.length][3];

        long [] sumArray = new long[opCache.length];
        sumArray[0] = numbers[0];
        long cimbinedNum = 0;
        //        boolean isBack = false;
        long calc = numbers[0];
        //        long preSum = calc;
        //        long preCalc = calc;
        //        long maxPartSum = combinedNumMap[opCache.length - 5][opCache.length - 1];
        //        System.out.println(maxPartSum);
        int count = 0;
        //set to first number
        while (opSeqOrder >= 0) {
            if (opFlag[opSeqOrder] < 3) {
                //                preSum = calc;
                ////////////////////////////////////////////////
                //update counter
                opTypeCount[opSeqOrder][opFlag[opSeqOrder]] = 1;
                if (opSeqOrder > 0) {
                    if (opTypeCount[opSeqOrder - 1][opFlag[opSeqOrder]] > 0) {//same as last one
                        opTypeCount[opSeqOrder][opFlag[opSeqOrder]] += opTypeCount[opSeqOrder - 1][opFlag[opSeqOrder]];
                    }
                }
                for (int i = 0; i < 3; ++i) {
                    if (i != opFlag[opSeqOrder]) {
                        opTypeCount[opSeqOrder][i] = 0;
                    }
                }
                ////////////////////////////////////
                opCache[opSeqOrder] = opFlag[opSeqOrder];
                ++opFlag[opSeqOrder];
                //
                //
                //                System.out.println(Arrays.toString(opTypeCount[opSeqOrder]));
                //////////////////////////////////////
                //test
                //                System.out.print(numbers[0]);
                //                for (int i = 0; i <= opSeqOrder; ++i) {
                //                    //construct string
                //                    switch (opCache[i]) {
                //                        case 1:
                //                            System.out.print('+');
                //                            break;
                //                        case 2:
                //                            System.out.print('-');
                //                            break;
                //                        case 0:
                //                            System.out.print(' ');
                //                            break;
                //                    }
                //                    System.out.print(numbers[i + 1]);
                //                }
                //                System.out.println();

                ////////////////////////////////
                //filter
                int preIdx = opSeqOrder - opTypeCount[opSeqOrder][0] + 1;
                cimbinedNum = combinedNumMap[preIdx][opSeqOrder + 1];
                if (cimbinedNum == 0) {
                    //                    System.out.println(">MAX_DIGIT_NUM " + MAX_DIGIT_NUM);
                    calc = sumArray[opSeqOrder - 1];

                    //                    System.out.println("go back from " + opSeqOrder + " to sum " + calc);
                    continue;
                }
                //                System.out.println("cimbinedNum:" + cimbinedNum);
                //                if (opTypeCount[opSeqOrder][0] > MAX_SPACE_NUM) {
                //                    System.out.println(">MAX_SPACE_NUM " + MAX_SPACE_NUM);
                //                    continue;
                //                }

                ////////////////////////////////
                //                long preCalc = calc;
                //                System.out.println("pre calc=" + preCalc);
                ++count;
                switch (opCache[opSeqOrder]) {
                    case 1:
                        calc += numbers[opSeqOrder + 1];
                        break;
                    case 2:
                        calc -= numbers[opSeqOrder + 1];
                        break;
                    case 0: {
                        ///////////////////////////////
                        //if found space op, check if continuous
                        //                                        System.out.println("space count:" + c + ", op:" + opIdx + ",nextop:"
                        //                                                + (opIdx + c - 1)
                        //                                                + ", combined:"
                        //                                                + cimbinedNum);
                        if (opSeqOrder == 0) {
                            calc = cimbinedNum;
                        } else {
                            //                            System.out.println("preIdx:" + preIdx);
                            if (preIdx == 0) {
                                calc = cimbinedNum;
                            } else {
                                //XXX
                                switch (opCache[preIdx - 1]) {
                                    case 1://+
                                        calc -= combinedNumMap[preIdx][opSeqOrder];
                                        calc += cimbinedNum;
                                        break;
                                    case 2:
                                        calc += combinedNumMap[preIdx][opSeqOrder];
                                        calc -= cimbinedNum;
                                        break;
                                }
                            }
                        }
                        break;
                    }
                }
                //                if (calc > maxPartSum) {
                //
                //                }
                sumArray[opSeqOrder] = calc;
                if (opSeqOrder < opCache.length - 1) {
                    //                    preSum = calc;
                    ++opSeqOrder;
                } else {
                    /////////////////////////////////////////////
                    //                        System.out.println("sum:" + calc);
                    //                        System.out.println(Arrays.toString(cache));
                    //                        System.out.println();
                    if (calc == 0) {
                        if (isFirst) {
                            isFirst = false;
                        } else {
                            result.append(';');
                        }
                        result.append(numbers[0]);
                        for (int i = 0; i < opCache.length; ++i) {
                            //construct string
                            switch (opCache[i]) {
                                case 1:
                                    result.append('+');
                                    break;
                                case 2:
                                    result.append('-');
                                    break;
                                case 0:
                                    result.append(' ');
                                    break;
                            }
                            result.append(numbers[i + 1]);
                        }
                    } else {
                        calc = sumArray[opSeqOrder - 1];
                    }
                }
            } else {
                //go back at once
                //TODO
                int c = opTypeCount[opSeqOrder][2];
                //                System.out.println("minus num:" + c);

                //clear
                for (int i = opSeqOrder - c + 1; i <= opSeqOrder; ++i) {
                    opFlag[i] = 0;
                }

                opSeqOrder -= c;

                if (opSeqOrder == 0) {
                    calc = numbers[0];
                } else if (opSeqOrder > 0) {
                    calc = sumArray[opSeqOrder - 1];
                }
                //                System.out.println("go back from " + opSeqOrder + " to sum " + calc);
                //                isBack = true;
            }
        }
        System.out.println(count);
        if (result.length() == 0) {
            return "NOTFOUND";
        }
        return result.toString();
    }

    //TODO
    //BF
    //only for S
    public static String run6BF(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        StringBuilder result = null;
        int MAX_DIGIT_NUM = 0;
        int TOTAL_DIGIT_NUM = 0;
        int maxSpaceCount = 0;

        char ch = arg.charAt(0);
        boolean isFirst = true;
        //cache the whole line with op and number
        //for cp
        char [] expStrLine = null;
        long [] numbers = null;

        int n = 0;
        if (ch == 'S') {
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            //3<=N<=15
            if (n < 3 || n > 15) {
                return "ILLEGAL";
            }
            ///////////////////////////////////////////
            //calc digit data
            if (n < 10) {
                TOTAL_DIGIT_NUM = n;
            } else {
                TOTAL_DIGIT_NUM = (n - 10 + 1) * 2 + (10 - 1);
            }
            //            System.out.println("TOTAL_DIGIT_NUM:" + TOTAL_DIGIT_NUM);

            //???
            MAX_DIGIT_NUM = TOTAL_DIGIT_NUM / 2 + 1;
            //            System.out.println("MAX_DIGIT_NUM:" + MAX_DIGIT_NUM);
            maxSpaceCount = n / 2 - 1;
            /////////////////////////////////////////////
            numbers = new long[n];
            for (int i = 0; i < numbers.length; ++i) {
                numbers[i] = i + 1;
            }
            ///////////////////////////
            //TODO n number and n-1 op
            expStrLine = new char[TOTAL_DIGIT_NUM + n - 1];
            int num = 1;
            for (int i = 0; i < expStrLine.length; i += 2) {
                if (num < 10) {
                    expStrLine[i] = (char)(num + '0');
                } else {
                    expStrLine[i] = '1';
                    expStrLine[i + 1] = (char)(num % 10 + '0');
                    ++i;
                }
                ++num;
            }
            //            System.out.println("line len:" + line.length);
            //            System.out.println(Arrays.toString(line));
            //////////////////////////////////////////////

        } else {
            //TODO R
            return "ILLEGAL";
        }
        //TODO for big number
        if (MAX_DIGIT_NUM > 15) {
            return "ILLEGAL";
        }
        //

        //various buffer size
        result = new StringBuilder(2 << n);
        ///////////////////////////////////////////
        //row:startIdx
        //column:endIdx
        //value:the total digit of the number constructed from startidx to endidx
        //if value is zero, it means invalid startidx and endidx, or beyond max digit
        int [][] combinedNumDigitMap = new int[n][n];
        int w = 10;
        int digit = 1;
        for (int i = 0; i < n; ++i) {
            while (true) {
                if (numbers[i] / w == 0) {
                    combinedNumDigitMap[i][i] = digit;
                    break;
                }
                w *= 10;
                ++digit;
            }
        }
        for (int i = 0; i < n; ++i) {
            for (int j = i + 1; j < n; ++j) {
                combinedNumDigitMap[i][j] = combinedNumDigitMap[j][j] + combinedNumDigitMap[i][j - 1];
                if (combinedNumDigitMap[i][j] > MAX_DIGIT_NUM) {
                    combinedNumDigitMap[i][j] = 0;
                    break;
                }
            }
            //            System.out.println(Arrays.toString(combinedNumDigitMap[i]));
        }
        //        System.out.println();

        //
        long [][] combinedNumMap = new long[n][n];
        for (int i = 0; i < n; ++i) {
            combinedNumMap[i][i] = numbers[i];
            for (int j = i + 1; j < numbers.length; ++j) {
                //!!!limit the combined number
                if (combinedNumDigitMap[i][j] > 0) {
                    int c = combinedNumDigitMap[j][j];
                    w = 10;
                    while (c > 1) {
                        w *= 10;
                        --c;
                    }
                    combinedNumMap[i][j] = combinedNumMap[i][j - 1] * w + numbers[j];
                } else {
                    break;
                }
            }
            //            System.out.println(Arrays.toString(combinedNumMap[i]));
        }
        ///////////////////////////////////////////       
        //
        //
        //        long [] stack = new long[n];
        //        int stackPtr = 0;
        long maxValue = 0;
        for (int i = 0; i < MAX_DIGIT_NUM; ++i) {
            if (combinedNumMap[0][i] > maxValue) {
                maxValue = combinedNumMap[0][i];
            }
        }
        //        System.out.println("maxValue:" + maxValue);
        LinkedList<Long> stackForOther = new LinkedList<Long>();
        //       
        //TODO
        final char [] opStr = {' ', '+', '-'};

        //test
        long max = 0;
        //        System.out.println("Total digit num:" + TOTAL_DIGIT_NUM);
        //        System.out.println("Max digit num:" + MAX_DIGIT_NUM);
        //        //row:op order
        //        //column: op type
        //save the continues op
        int [][] opTypeCount = new int[n - 1][3];
        char [] opCache = new char[n - 1];
        char [] opFlag = new char[n - 1];
        //        char [] lineForCalc = new char[n - 1];
        int opSeqOrder = 0; //
        while (opSeqOrder >= 0) {
            if (opFlag[opSeqOrder] < 3) {
                ////////////////////////////////////////////////
                //update counter
                opTypeCount[opSeqOrder][opFlag[opSeqOrder]] = 1;
                if (opSeqOrder > 0) {
                    if (opTypeCount[opSeqOrder - 1][opFlag[opSeqOrder]] > 0) {//same as last one
                        opTypeCount[opSeqOrder][opFlag[opSeqOrder]] += opTypeCount[opSeqOrder - 1][opFlag[opSeqOrder]];
                    }
                }
                for (int i = 0; i < 3; ++i) {
                    if (i != opFlag[opSeqOrder]) {
                        opTypeCount[opSeqOrder][i] = 0;
                    }
                }
                ////////////////////////////////////
                opCache[opSeqOrder] = opFlag[opSeqOrder];
                //move to next op
                ++opFlag[opSeqOrder];

                //                System.out.println(Arrays.toString(opTypeCount[opSeqOrder]));

                ////////////////////////////////
                //TODO filter

                if (opTypeCount[opSeqOrder][0] > maxSpaceCount) {
                    //                    System.out.println(opTypeCount[opSeqOrder][0] + ">" + maxSpaceCount);
                    continue;

                }

                if (opSeqOrder < opFlag.length - 1) {
                    ++opSeqOrder;
                } else {
                    int preCombinedLastDigitIdx = -1;
                    stackForOther.clear();
                    for (int i = 0; i < opCache.length;) {
                        int spaceCount = 0;
                        while (i < opCache.length && opCache[i] == 0) {
                            ++spaceCount;
                            ++i;
                        }
                        if (spaceCount > 0) {
                            //                            if (preCombinedLastDigitIdx > 0) {
                            for (int j = preCombinedLastDigitIdx + 1; j < i - spaceCount; ++j) {
                                stackForOther.add(numbers[j]);
                            }
                            stackForOther.add(combinedNumMap[i - spaceCount][i]);
                            preCombinedLastDigitIdx = i;
                            //                            System.out.println("[" + (stackPtr - spaceCount) + "," + stackPtr + "], spaceCount:"
                            //                                    + spaceCount);
                        } else {
                            ++i;
                        }

                    }
                    for (int i = preCombinedLastDigitIdx + 1; i < n; ++i) {
                        stackForOther.add(numbers[i]);
                    }
                    ///////////////////////////////////////

                    long sum = stackForOther.removeFirst();//
                    //                    if (sum > maxValue) {
                    //                        System.out.println(sum +">"+ maxValue);
                    //                        continue;
                    //                    }
                    long t = sum;
                    if (t < sum) {
                        t = sum;
                    }

                    //                    System.out.println(sum);
                    for (int i = 0; i < opCache.length; i++) {

                        switch (opCache[i]) {
                            case 1:
                                //test
                                if (t < stackForOther.getFirst()) {
                                    t = stackForOther.getFirst();
                                }
                                sum += stackForOther.removeFirst();
                                break;
                            case 2:
                                //test
                                if (t < stackForOther.getFirst()) {
                                    t = stackForOther.getFirst();
                                }
                                sum -= stackForOther.removeFirst();
                                break;
                        }
                        //                        if (sum > maxValue) {
                        //                            System.out.println(sum +">"+ maxValue);
                        //                            break;
                        //                        }

                    }

                    ///////////////////////////////////
                    //test
                    //                    System.out.println(Arrays.toString(stack));

                    //                    System.out.println("=" + sum);

                    //                    long calc=printAndCalc(opCache);
                    //                    if(calc!=sum){
                    //                        System.err.println("calc!=sum");
                    ////                        throw new Exception();
                    //                    }
                    //////////////////////////////////////
                    if (sum == 0) {
                        if (t > max) {
                            max = t;
                        }
                        if (isFirst) {
                            isFirst = false;
                        } else {
                            result.append(';');
                        }
                        result.append(numbers[0]);
                        for (int i = 0; i < opCache.length; ++i) {
                            //                        System.out.println((int)opCache[i]);
                            result.append(opStr[opCache[i]]);
                            result.append(numbers[i + 1]);
                        }
                        //                        System.out.println("=" + sum);
                    }
                }
            } else {
                //go back at once
                int c = opTypeCount[opSeqOrder][2];
                //clear
                for (int i = opSeqOrder - c + 1; i <= opSeqOrder; ++i) {
                    opFlag[i] = 0;
                }
                opSeqOrder -= c;
            }
        }
        System.out.println(max);
        //////////////////////////////////////////////////
        if (result.length() == 0) {
            return "NOTFOUND";
        }
        return result.toString();
    }

    /**
     * for small long S&R
     * 
     * @param arg
     * @return
     */
    public static String run7BF(String arg) {
        if (arg == null || arg.length() < 2) {
            return "ILLEGAL";
        }
        StringBuilder result = null;
        //        int MAX_DIGIT_NUM = 0;
        //        int TOTAL_DIGIT_NUM = 0;

        char ch = arg.charAt(0);
        boolean isFirst = true;
        //cache the whole line with op and number
        //for cp
        //        char [] expStrLine = null;
        long [] numbers = null;

        int n = 0;
        int maxSpaceCount = 0;
        int MAX_DIGIT_NUM = 0;
        if (ch == 'S') {
            try {
                n = Integer.valueOf(arg.substring(1));
            } catch (Exception e) {
                return "ILLEGAL";
            }
            //3<=N<=15
            if (n < 3 || n > 15) {
                return "ILLEGAL";
            }
            //            System.out.println("MAX_DIGIT_NUM:" + MAX_DIGIT_NUM);

            /////////////////////////////////////////////
            numbers = new long[n];
            for (int i = 0; i < numbers.length; ++i) {
                numbers[i] = i + 1;
            }

            //            System.out.println("line len:" + line.length);
            //            System.out.println(Arrays.toString(line));
            //////////////////////////////////////////////

            ///////////////////////////////////////////
            //calc digit data

            if (n < 10) {
                MAX_DIGIT_NUM = n;
            } else {
                MAX_DIGIT_NUM = (n - 10 + 1) * 2 + (10 - 1);
            }
            //            System.out.println("TOTAL_DIGIT_NUM:" + TOTAL_DIGIT_NUM);

            //???
            MAX_DIGIT_NUM = MAX_DIGIT_NUM / 2;

            maxSpaceCount = n / 2 - 1;
            if (maxSpaceCount > n / 10 + 2) {
                maxSpaceCount = n / 10 + 2;
            }

        } else if (ch == 'R') {
            String asdStr = arg.substring(1);
            String [] numberStr = asdStr.split(",");
            numbers = new long[numberStr.length];
            n = numbers.length;
            long pre = Integer.MIN_VALUE;
            try {
                for (int i = 0; i < numbers.length; ++i) {
                    if (numberStr[i].isEmpty() || numberStr[i].charAt(0) > '9') {
                        //not a number
                        return "ILLEGAL";
                    }
                    numbers[i] = Integer.parseInt(numberStr[i]);
                    if (numbers[i] < 0 || numbers[i] < pre) {
                        //not larger than previous
                        return "ILLEGAL";
                    }
                    //                    MAX_DIGIT_NUM += numberStr[i].length();
                    pre = numbers[i];
                }
            } catch (Exception e) {
                return "ILLEGAL";
            }
            maxSpaceCount = n / 2 - 1;
            result = new StringBuilder(64);
            //            numberStr.length
            MAX_DIGIT_NUM = (numberStr.length / 2) * numberStr[numberStr.length - 1].length();

        } else {
            return "ILLEGAL";
        }
        //TODO for big number
        if (MAX_DIGIT_NUM > 15) {
            return "ILLEGAL";
        }
        //

        //various buffer size
        result = new StringBuilder(2 << n);
        ///////////////////////////////////////////
        //row:startIdx
        //column:endIdx
        //value:the total digit of the number constructed from startidx to endidx
        //if value is zero, it means invalid startidx and endidx, or beyond max digit
        int [][] combinedNumDigitMap = new int[n][n];
        int w = 10;
        int digit = 1;
        for (int i = 0; i < n; ++i) {
            while (true) {
                if (numbers[i] / w == 0) {
                    combinedNumDigitMap[i][i] = digit;
                    break;
                }
                w *= 10;
                ++digit;
            }
        }
        for (int i = 0; i < n; ++i) {
            for (int j = i + 1; j < n; ++j) {
                combinedNumDigitMap[i][j] = combinedNumDigitMap[j][j] + combinedNumDigitMap[i][j - 1];
                if (combinedNumDigitMap[i][j] > MAX_DIGIT_NUM) {
                    combinedNumDigitMap[i][j] = 0;
                    break;
                }
            }
            //            System.out.println(Arrays.toString(combinedNumDigitMap[i]));
        }
        //        System.out.println();

        //
        long [][] combinedNumMap = new long[n][n];
        for (int i = 0; i < n; ++i) {
            combinedNumMap[i][i] = numbers[i];
            for (int j = i + 1; j < numbers.length; ++j) {
                //!!!limit the combined number
                if (combinedNumDigitMap[i][j] > 0) {
                    int c = combinedNumDigitMap[j][j];
                    w = 10;
                    while (c > 1) {
                        w *= 10;
                        --c;
                    }
                    combinedNumMap[i][j] = combinedNumMap[i][j - 1] * w + numbers[j];
                } else {
                    break;
                }
            }
            //            System.out.println(Arrays.toString(combinedNumMap[i]));
        }
        ///////////////////////////////////////////       
        //
        //
        //        long [] stack = new long[n];
        //        int stackPtr = 0;
        long maxValue = 0;
        for (int i = 0; i <= maxSpaceCount; ++i) {
            if (combinedNumMap[0][i] > maxValue) {
                maxValue = combinedNumMap[0][i];
            }
        }
        //        System.out.println("maxValue:" + maxValue);
        LinkedList<Long> stackForOther = new LinkedList<Long>();
        //       
        final char [] opStr = {' ', '+', '-'};

        //test
        //        long max = 0;
        //        System.out.println("Total digit num:" + TOTAL_DIGIT_NUM);
        //        System.out.println("Max digit num:" + MAX_DIGIT_NUM);
        //        //row:op order
        //        //column: op type
        //save the continues op
        int [][] opTypeCount = new int[n - 1][3];
        char [] opCache = new char[n - 1];
        char [] opFlag = new char[n - 1];
        //        char [] lineForCalc = new char[n - 1];
        int opSeqOrder = 0; //
        while (opSeqOrder >= 0) {
            if (opFlag[opSeqOrder] < 3) {
                ////////////////////////////////////////////////
                //update counter
                opTypeCount[opSeqOrder][opFlag[opSeqOrder]] = 1;
                if (opSeqOrder > 0) {
                    if (opTypeCount[opSeqOrder - 1][opFlag[opSeqOrder]] > 0) {//same as last one
                        opTypeCount[opSeqOrder][opFlag[opSeqOrder]] += opTypeCount[opSeqOrder - 1][opFlag[opSeqOrder]];
                    }
                }
                for (int i = 0; i < 3; ++i) {
                    if (i != opFlag[opSeqOrder]) {
                        opTypeCount[opSeqOrder][i] = 0;
                    }
                }
                ////////////////////////////////////
                opCache[opSeqOrder] = opFlag[opSeqOrder];
                //move to next op
                ++opFlag[opSeqOrder];

                //                System.out.println(Arrays.toString(opTypeCount[opSeqOrder]));

                ////////////////////////////////
                //TODO filter

                if (opTypeCount[opSeqOrder][0] > maxSpaceCount) {
                    //                    System.out.println(opTypeCount[opSeqOrder][0] + ">" + maxSpaceCount);
                    continue;

                }

                if (opSeqOrder < opFlag.length - 1) {
                    ++opSeqOrder;
                } else {
                    int preCombinedLastDigitIdx = -1;
                    stackForOther.clear();
                    for (int i = 0; i < opCache.length;) {
                        int spaceCount = 0;
                        while (i < opCache.length && opCache[i] == 0) {
                            ++spaceCount;
                            ++i;
                        }
                        if (spaceCount > 0) {
                            //                            if (preCombinedLastDigitIdx > 0) {
                            for (int j = preCombinedLastDigitIdx + 1; j < i - spaceCount; ++j) {
                                stackForOther.add(numbers[j]);
                            }
                            //TODO
                            stackForOther.add(combinedNumMap[i - spaceCount][i]);
                            preCombinedLastDigitIdx = i;
                            //                            System.out.println("[" + (stackPtr - spaceCount) + "," + stackPtr + "], spaceCount:"
                            //                                    + spaceCount);
                        } else {
                            ++i;
                        }

                    }
                    for (int i = preCombinedLastDigitIdx + 1; i < n; ++i) {
                        stackForOther.add(numbers[i]);
                    }
                    ///////////////////////////////////////

                    long sum = stackForOther.removeFirst();//
                    //                    if (sum > maxValue) {
                    //                        System.out.println(sum +">"+ maxValue);
                    //                        continue;
                    //                    }
                    //                    long t = sum;
                    //                    if (t < sum) {
                    //                        t = sum;
                    //                    }

                    //                    System.out.println(sum);
                    for (int i = 0; i < opCache.length; i++) {

                        switch (opCache[i]) {
                            case 1:
                                //test
                                //                                if (t < stackForOther.getFirst()) {
                                //                                    t = stackForOther.getFirst();
                                //                                }
                                sum += stackForOther.removeFirst();
                                break;
                            case 2:
                                //test
                                //                                if (t < stackForOther.getFirst()) {
                                //                                    t = stackForOther.getFirst();
                                //                                }
                                sum -= stackForOther.removeFirst();
                                break;
                        }
                        //                        if (sum > maxValue) {
                        //                            System.out.println(sum +">"+ maxValue);
                        //                            break;
                        //                        }

                    }

                    ///////////////////////////////////
                    //test
                    //                    System.out.println(Arrays.toString(stack));

                    //                    System.out.println("=" + sum);

                    //                    long calc=printAndCalc(opCache);
                    //                    if(calc!=sum){
                    //                        System.err.println("calc!=sum");
                    ////                        throw new Exception();
                    //                    }
                    //////////////////////////////////////
                    if (sum == 0) {
                        //                        if (t > max) {
                        //                            max = t;
                        //                        }
                        if (isFirst) {
                            isFirst = false;
                        } else {
                            result.append(';');
                        }
                        result.append(numbers[0]);
                        for (int i = 0; i < opCache.length; ++i) {
                            //                        System.out.println((int)opCache[i]);
                            result.append(opStr[opCache[i]]);
                            result.append(numbers[i + 1]);
                        }
                        //                        System.out.println("=" + sum);
                    }
                }
            } else {
                //go back at once
                int c = opTypeCount[opSeqOrder][2];
                //clear
                for (int i = opSeqOrder - c + 1; i <= opSeqOrder; ++i) {
                    opFlag[i] = 0;
                }
                opSeqOrder -= c;
            }
        }
        //        System.out.println(max);
        //////////////////////////////////////////////////
        if (result.length() == 0) {
            return "NOTFOUND";
        }
        return result.toString();
    }

    public static void testStandard(int c) {
        double totalTime = 0;
        //case 1
        totalTime += test("S7",
                "1+2-3+4-5-6+7;1+2-3-4+5+6-7;1-2 3+4+5+6+7;1-2 3-4 5+6 7;1-2+3+4-5+6-7;1-2-3-4-5+6+7",
                c);
        //case 2
        totalTime += test("R3,5,7,9,11,13", "3+5+7+9-11-13", c);
        //case 3:
        totalTime += test(
                "S9",
                "1 2+3 4-5 6-7+8+9;1 2+3+4-5-6-7+8-9;1 2+3-4 5+6+7+8+9;1 2+3-4+5-6+7-8-9;1 2-3+4+5 6-7 8+9;1 2-3+4+5+6-7-8-9;1 2-3-4-5+6-7-8+9;1 2-3-4-5-6+7+8-9;1+2-3 4-5 6+7 8+9;1-2 3-4-5 6-7+8 9;1-2-3 4+5+6+7+8+9",
                c);
        //case 4
        //
        totalTime += test("R2,4,6,7,8,11,12,15", "NOTFOUND", c);

        //case 5
        totalTime += test("R3,A,B,5,7", "ILLEGAL", c);
        //case 6
        totalTime += test(
                "S11",
                "1 2 3-4-5-6-7 8-9-10-11;1 2+3 4+5-6 7+8+9+10-11;1 2+3 4+5-6-7-8-9-10-11;1 2+3+4 5+6-7 8-9+10+11;1 2+3+4+5 6-7-8 9+10+11;1 2+3-4-5 6+7+8+9+10+11;1 2-3 4+5+6+7-8-9+10+11;1 2-3 4+5+6-7+8+9-10+11;1 2-3 4+5-6+7+8+9+10-11;1 2-3+4 5-6-7 8+9+10+11;1 2-3-4+5 6+7-8 9+10+11;1+2 3+4 5+6-7-8 9+10+11;1+2 3+4 5-6 7+8-9+10-11;1+2 3+4+5-6-7-8+9-10-11;1+2 3+4-5+6 7-8 9+10-11;1+2 3+4-5+6-7+8-9-10-11;1+2 3-4 5+6-7-8+9+10+11;1+2 3-4 5-6 7+8 9+10-11;1+2 3-4 5-6+7+8-9+10+11;1+2 3-4+5+6+7-8-9-10-11;1+2 3-4-5-6+7-8-9-10+11;1+2 3-4-5-6-7+8-9+10-11;1+2+3 4+5+6-7 8+9+10+11;1+2+3+4 5-6 7+8+9+10-11;1+2+3+4 5-6-7-8-9-10-11;1+2+3+4+5 6-7 8-9+10+11;1+2+3+4+5-6+7-8-9-10+11;1+2+3+4+5-6-7+8-9+10-11;1+2+3+4-5+6+7-8-9+10-11;1+2+3+4-5+6-7+8+9-10-11;1+2+3-4 5-6+7+8+9+10+11;1+2+3-4+5+6+7-8+9-10-11;1+2+3-4-5+6-7-8-9+10+11;1+2+3-4-5-6+7-8+9-10+11;1+2+3-4-5-6-7+8+9+10-11;1+2-3 4 5-6 7 8+9+10 11;1+2-3 4+5 6-7-8-9+10-11;1+2-3+4 5-6 7-8+9+10+11;1+2-3+4+5+6+7+8-9-10-11;1+2-3+4+5-6-7-8-9+10+11;1+2-3+4-5+6-7-8+9-10+11;1+2-3+4-5-6 7+8 9-10-11;1+2-3+4-5-6+7+8-9-10+11;1+2-3+4-5-6+7-8+9+10-11;1+2-3-4 5+6 7+8-9-10-11;1+2-3-4+5+6 7-8 9+10+11;1+2-3-4+5+6-7+8-9-10+11;1+2-3-4+5+6-7-8+9+10-11;1+2-3-4+5-6+7+8-9+10-11;1+2-3-4-5+6+7+8+9-10-11;1+2-3-4-5-6-7-8+9+10+11;1-2 3 4+5+6-7 8 9+10 11;1-2 3+4 5+6 7-8 9+10-11;1-2 3+4 5+6-7+8-9-10-11;1-2 3+4+5+6+7+8-9-10+11;1-2 3+4+5+6+7-8+9+10-11;1-2 3+4-5-6 7+8 9-10+11;1-2 3+4-5-6+7-8+9+10+11;1-2 3-4 5+6 7+8-9-10+11;1-2 3-4 5+6 7-8+9+10-11;1-2 3-4 5+6-7+8 9-10-11;1-2 3-4+5+6-7-8+9+10+11;1-2 3-4+5-6 7+8 9+10-11;1-2 3-4+5-6+7+8-9+10+11;1-2 3-4-5+6+7+8+9-10+11;1-2+3 4-5 6+7+8+9+10-11;1-2+3+4 5-6 7+8-9+10+11;1-2+3+4+5-6-7-8+9-10+11;1-2+3+4-5+6 7-8 9+10+11;1-2+3+4-5+6-7+8-9-10+11;1-2+3+4-5+6-7-8+9+10-11;1-2+3+4-5-6+7+8-9+10-11;1-2+3-4 5-6 7+8 9+10+11;1-2+3-4+5+6+7-8-9-10+11;1-2+3-4+5+6-7+8-9+10-11;1-2+3-4+5-6+7+8+9-10-11;1-2+3-4-5-6-7+8-9+10+11;1-2-3+4+5+6+7-8-9+10-11;1-2-3+4+5+6-7+8+9-10-11;1-2-3+4-5-6+7-8-9+10+11;1-2-3+4-5-6-7+8+9-10+11;1-2-3-4 5+6 7-8-9+10-11;1-2-3-4+5 6-7 8+9+10+11;1-2-3-4+5+6-7-8-9+10+11;1-2-3-4+5-6+7-8+9-10+11;1-2-3-4+5-6-7+8+9+10-11;1-2-3-4-5+6+7+8-9-10+11;1-2-3-4-5+6+7-8+9+10-11",
                c);
        //case 7
        totalTime += test("R9,12,14,38,41,44,51,55,70,77", "NOTFOUND", c);

        //case 8
        totalTime += test(
                "S13",
                "1 2 3 4-5 6-7 8+9-10-11 12+13;1 2 3+4 5+6+7+8+9 10-11 12+13;1 2 3+4+5-6 7-8 9+10-11+12+13;1 2 3+4-5 6-7 8+9+10-11+12-13;1 2 3+4-5+6+7-8 9-10-11-12-13;1 2 3+4-5-6-7-8 9-10-11-12+13;1 2 3-4 5+6+7-8 9+10-11+12-13;1 2 3-4 5-6-7-8 9+10-11+12+13;1 2 3-4+5-6-7-8 9-10-11+12-13;1 2 3-4-5 6-7 8-9+10-11+12+13;1 2 3-4-5+6 7+8+9 10-11 12+13;1 2 3-4-5+6-7+8 9+10 11-12 13;1 2 3-4-5+6-7-8 9-10+11-12-13;1 2 3-4-5-6 7+8-9-10-11-12-13;1 2 3-4-5-6+7-8 9+10-11-12-13;1 2+3 4+5 6+7-8 9-10-11-12+13;1 2+3 4+5-6-7 8+9+10-11+12+13;1 2+3 4-5 6+7+8-9-10-11+12+13;1 2+3 4-5 6+7-8+9-10+11-12+13;1 2+3 4-5 6+7-8-9+10+11+12-13;1 2+3 4-5 6-7+8+9+10-11-12+13;1 2+3 4-5 6-7+8+9-10+11+12-13;1 2+3 4-5+6-7 8+9+10+11-12+13;1 2+3+4 5-6 7-8-9+10-11+12+13;1 2+3+4 5-6-7+8-9-10-11-12-13;1 2+3+4+5+6 7-8 9+10-11+12-13;1 2+3+4+5+6+7-8-9-10-11-12+13;1 2+3+4+5+6-7+8-9-10-11+12-13;1 2+3+4+5+6-7-8+9-10+11-12-13;1 2+3+4+5-6 7+8 9-10-11-12-13;1 2+3+4+5-6 7+8+9-10+11+12+13;1 2+3+4+5-6+7+8-9-10+11-12-13;1 2+3+4+5-6+7-8+9+10-11-12-13;1 2+3+4-5+6+7+8-9+10-11-12-13;1 2+3+4-5+6-7-8-9-10-11+12+13;1 2+3+4-5-6+7-8-9-10+11-12+13;1 2+3+4-5-6-7+8-9+10-11-12+13;1 2+3+4-5-6-7+8-9-10+11+12-13;1 2+3+4-5-6-7-8+9+10-11+12-13;1 2+3-4 5+6 7-8-9-10-11-12+13;1 2+3-4 5+6+7+8+9+10-11-12+13;1 2+3-4 5+6+7+8+9-10+11+12-13;1 2+3-4 5-6+7-8-9+10+11+12+13;1 2+3-4 5-6-7+8 9-10-11-12-13;1 2+3-4 5-6-7+8+9-10+11+12+13;1 2+3-4+5 6-7 8+9-10+11-12+13;1 2+3-4+5 6-7 8-9+10+11+12-13;1 2+3-4+5+6+7+8+9-10-11-12-13;1 2+3-4+5+6-7-8-9-10+11-12+13;1 2+3-4+5-6+7-8-9+10-11-12+13;1 2+3-4+5-6+7-8-9-10+11+12-13;1 2+3-4+5-6-7+8+9-10-11-12+13;1 2+3-4+5-6-7+8-9+10-11+12-13;1 2+3-4+5-6-7-8+9+10+11-12-13;1 2+3-4-5 6+7 8-9-10+11-12-13;1 2+3-4-5+6+7-8+9-10-11-12+13;1 2+3-4-5+6+7-8-9+10-11+12-13;1 2+3-4-5+6-7+8+9-10-11+12-13;1 2+3-4-5+6-7+8-9+10+11-12-13;1 2+3-4-5-6+7+8+9-10+11-12-13;1 2+3-4-5-6-7-8-9+10-11+12+13;1 2-3 4+5 6+7-8-9-10+11-12-13;1 2-3 4+5 6-7+8-9+10-11-12-13;1 2-3 4+5-6+7 8-9-10-11-12-13;1 2-3 4-5 6-7+8 9+10+11-12-13;1 2-3+4 5+6+7-8 9+10+11-12+13;1 2-3+4 5-6 7+8+9+10+11-12-13;1 2-3+4 5-6-7-8-9-10+11-12-13;1 2-3+4+5 6-7 8+9+10-11-12+13;1 2-3+4+5 6-7 8+9-10+11+12-13;1 2-3+4+5+6 7-8 9-10-11+12+13;1 2-3+4+5+6-7-8-9+10-11-12+13;1 2-3+4+5+6-7-8-9-10+11+12-13;1 2-3+4+5-6+7-8+9-10-11-12+13;1 2-3+4+5-6+7-8-9+10-11+12-13;1 2-3+4+5-6-7+8+9-10-11+12-13;1 2-3+4+5-6-7+8-9+10+11-12-13;1 2-3+4-5 6+7 8-9+10-11-12-13;1 2-3+4-5+6+7+8-9-10-11-12+13;1 2-3+4-5+6+7-8+9-10-11+12-13;1 2-3+4-5+6+7-8-9+10+11-12-13;1 2-3+4-5+6-7+8+9-10+11-12-13;1 2-3+4-5-6+7+8+9+10-11-12-13;1 2-3+4-5-6-7-8+9-10-11+12+13;1 2-3+4-5-6-7-8-9+10+11-12+13;1 2-3-4 5+6+7+8-9+10-11+12+13;1 2-3-4 5+6+7-8+9+10+11-12+13;1 2-3-4 5+6-7+8+9+10+11+12-13;1 2-3-4+5 6-7 8-9-10+11+12+13;1 2-3-4+5+6+7+8-9-10-11+12-13;1 2-3-4+5+6+7-8+9-10+11-12-13;1 2-3-4+5+6-7+8+9+10-11-12-13;1 2-3-4+5-6-7+8-9-10-11+12+13;1 2-3-4+5-6-7-8+9-10+11-12+13;1 2-3-4+5-6-7-8-9+10+11+12-13;1 2-3-4-5+6 7-8 9+10+11-12+13;1 2-3-4-5+6+7-8-9-10-11+12+13;1 2-3-4-5+6-7+8-9-10+11-12+13;1 2-3-4-5+6-7-8+9+10-11-12+13;1 2-3-4-5+6-7-8+9-10+11+12-13;1 2-3-4-5-6 7+8 9-10-11+12-13;1 2-3-4-5-6+7+8-9+10-11-12+13;1 2-3-4-5-6+7+8-9-10+11+12-13;1 2-3-4-5-6+7-8+9+10-11+12-13;1 2-3-4-5-6-7+8+9+10+11-12-13;1+2 3 4+5 6-7+8+9 10+11-12 13;1+2 3 4-5+6+7 8+9 10-11-12 13;1+2 3+4 5-6-7 8-9+10-11+12+13;1+2 3+4+5 6+7-8 9+10-11+12-13;1+2 3+4-5 6+7+8+9-10-11+12+13;1+2 3+4-5 6+7+8-9+10+11-12+13;1+2 3+4-5 6+7-8+9+10+11+12-13;1+2 3-4+5 6-7-8 9+10+11+12-13;1+2 3-4-5 6+7-8-9+10+11+12+13;1+2 3-4-5 6-7+8 9-10-11-12-13;1+2 3-4-5 6-7+8+9-10+11+12+13;1+2+3 4+5 6-7 8+9-10+11-12-13;1+2+3 4+5+6 7-8 9-10-11-12+13;1+2+3 4+5+6-7-8-9-10+11-12-13;1+2+3 4+5-6 7+8-9-10+11+12+13;1+2+3 4+5-6 7-8+9+10-11+12+13;1+2+3 4+5-6+7-8 9+10+11+12+13;1+2+3 4+5-6+7-8-9+10-11-12-13;1+2+3 4+5-6-7+8+9-10-11-12-13;1+2+3 4-5+6+7-8+9-10-11-12-13;1+2+3 4-5-6-7-8+9-10-11-12+13;1+2+3 4-5-6-7-8-9+10-11+12-13;1+2+3+4 5-6-7 8+9+10-11+12+13;1+2+3+4-5 6+7+8+9+10+11-12+13;1+2+3-4 5-6+7 8-9-10+11-12-13;1+2+3-4+5 6+7-8 9+10-11+12+13;1+2+3-4-5 6+7-8+9+10+11+12+13;1+2-3 4+5+6 7-8-9-10+11-12-13;1+2-3 4+5+6+7+8+9+10+11-12-13;1+2-3 4+5+6-7-8+9-10+11+12+13;1+2-3 4+5-6 7+8 9-10-11+12+13;1+2-3 4+5-6+7+8-9-10+11+12+13;1+2-3 4+5-6+7-8+9+10-11+12+13;1+2-3 4+5-6-7+8+9+10+11-12+13;1+2-3 4-5 6+7 8+9+10-11-12+13;1+2-3 4-5 6+7 8+9-10+11+12-13;1+2-3 4-5+6+7+8-9+10-11+12+13;1+2-3 4-5+6+7-8+9+10+11-12+13;1+2-3 4-5+6-7+8+9+10+11+12-13;1+2-3+4+5 6+7-8 9+10+11-12+13;1+2-3+4-5 6+7+8-9+10+11+12+13;1+2-3-4+5 6-7-8+9-10-11-12-13;1+2-3-4-5 6-7+8 9-10-11+12-13;1-2 3 4-5 6+7 8+9-10 11+12 13;1-2 3+4+5 6+7-8+9-10-11-12-13;1-2 3+4-5 6+7+8 9-10-11+12-13;1-2 3-4 5+6+7 8+9+10-11-12-13;1-2 3-4 5-6+7 8-9-10-11+12+13;1-2 3-4+5 6+7-8-9-10-11-12+13;1-2 3-4+5 6-7+8-9-10-11+12-13;1-2 3-4+5 6-7-8+9-10+11-12-13;1-2 3-4-5 6-7+8 9+10-11-12+13;1-2 3-4-5 6-7+8 9-10+11+12-13;1-2+3 4+5 6-7 8+9-10-11-12+13;1-2+3 4+5 6-7 8-9+10-11+12-13;1-2+3 4+5+6-7-8-9-10-11-12+13;1-2+3 4+5-6 7-8-9+10+11+12+13;1-2+3 4+5-6+7-8-9-10-11+12-13;1-2+3 4+5-6-7+8-9-10+11-12-13;1-2+3 4+5-6-7-8+9+10-11-12-13;1-2+3 4-5 6+7 8-9-10-11-12-13;1-2+3 4-5+6+7-8-9-10+11-12-13;1-2+3 4-5+6-7+8-9+10-11-12-13;1-2+3 4-5-6 7+8+9+10+11-12+13;1-2+3 4-5-6+7+8+9-10-11-12-13;1-2+3 4-5-6-7-8-9-10+11-12+13;1-2+3+4 5-6-7 8-9+10+11+12+13;1-2+3+4+5 6+7-8 9+10+11+12-13;1-2+3+4-5 6+7+8 9-10-11-12-13;1-2+3+4-5 6+7+8+9-10+11+12+13;1-2+3-4 5-6+7 8-9-10-11-12+13;1-2+3-4+5 6-7+8-9-10-11-12-13;1-2+3-4-5 6-7+8 9-10+11-12-13;1-2-3 4+5+6 7-8-9-10-11-12+13;1-2-3 4+5+6+7+8+9+10-11-12+13;1-2-3 4+5+6+7+8+9-10+11+12-13;1-2-3 4+5-6+7-8-9+10+11+12+13;1-2-3 4+5-6-7+8 9-10-11-12-13;1-2-3 4+5-6-7+8+9-10+11+12+13;1-2-3 4-5 6+7 8+9-10-11+12+13;1-2-3 4-5 6+7 8-9+10+11-12+13;1-2-3 4-5+6 7+8-9+10-11-12-13;1-2-3 4-5+6+7-8+9-10+11+12+13;1-2-3 4-5+6-7+8+9+10-11+12+13;1-2-3 4-5-6+7+8+9+10+11-12+13;1-2-3+4 5+6-7 8+9+10+11-12+13;1-2-3+4+5 6+7-8 9-10+11+12+13;1-2-3+4+5 6+7-8-9-10-11-12-13;1-2-3+4-5 6-7+8 9+10-11-12-13;1-2-3+4-5 6-7+8+9+10+11+12+13;1-2-3-4 5+6+7 8-9+10-11-12-13;1-2-3-4+5 6-7-8-9-10+11-12-13",
                c);
        //case 9

        totalTime += test("R21,24,40,45,50,58,62,71,78,80,82,84", "21 24+40-45-50-58+62 71-78+80-82 84", c);
        //3+5+7+9-11-13
        //case 10
        totalTime += test(
                "S15",
                "1 2 3 4+5 6+7+8-9+10+11+12-13 14-15;1 2 3 4+5 6-7 8+9+10+11-12 13-14-15;1 2 3 4+5 6-7-8-9+10+11+12-13 14+15;1 2 3 4+5+6 7+8 9+10+11+12-13-14 15;1 2 3 4+5+6+7 8+9-10-11-12-13 14+15;1 2 3 4+5+6+7-8 9+10+11-12 13+14+15;1 2 3 4+5+6+7-8-9-10-11-12 13+14-15;1 2 3 4+5+6-7-8-9+10+11-12 13-14-15;1 2 3 4+5-6+7 8+9+10+11-12-13 14-15;1 2 3 4+5-6+7-8+9-10+11-12 13-14-15;1 2 3 4+5-6-7 8+9-10-11 12-13-14-15;1 2 3 4+5-6-7+8+9+10-11-12 13-14-15;1 2 3 4-5+6+7+8-9-10+11-12 13-14-15;1 2 3 4-5+6+7-8+9+10-11-12 13-14-15;1 2 3 4-5+6-7-8-9-10+11-12 13-14+15;1 2 3 4-5-6 7-8+9+10+11-12 13+14+15;1 2 3 4-5-6+7 8+9-10+11-12-13 14+15;1 2 3 4-5-6+7-8-9+10-11-12 13-14+15;1 2 3 4-5-6+7-8-9-10 11+12 13-14 15;1 2 3 4-5-6+7-8-9-10+11-12 13+14-15;1 2 3 4-5-6-7 8-9-10-11 12-13+14-15;1 2 3 4-5-6-7+8+9-10-11-12 13-14+15;1 2 3 4-5-6-7+8-9+10-11-12 13+14-15;1 2 3+4 5 6+7+8-9 10-11 12+13+14 15;1 2 3+4 5-6+7 8-9+10 11-12 13-14-15;1 2 3+4 5-6-7 8-9-10-11-12-13-14-15;1 2 3+4+5 6+7+8+9+10+11 12-13 14-15;1 2 3+4+5 6+7-8-9+10 11-12 13+14+15;1 2 3+4+5 6-7-8+9+10+11 12-13 14+15;1 2 3+4+5+6+7 8+9 10-11 12-13+14-15;1 2 3+4+5+6+7 8-9+10+11 12-13 14-15;1 2 3+4+5+6-7 8-9-10-11+12-13-14-15;1 2 3+4-5 6-7-8 9+10+11-12-13+14+15;1 2 3+4-5 6-7-8 9+10-11+12+13-14+15;1 2 3+4-5 6-7-8 9-10+11+12+13+14-15;1 2 3+4-5 6-7-8-9-10-11-12-13+14-15;1 2 3+4-5+6 7 8+9-10 11-12 13+14 15;1 2 3+4-5+6+7 8-9-10+11 12-13 14+15;1 2 3+4-5-6+7 8+9+10 11-12 13+14-15;1 2 3+4-5-6+7 8+9+10-11+12 13-14 15;1 2 3+4-5-6-7 8+9-10-11-12-13+14-15;1 2 3+4-5-6-7 8-9+10-11-12+13-14-15;1 2 3+4-5-6-7 8-9-10+11+12-13-14-15;1 2 3-4 5 6-7-8 9-10 11+12+13+14 15;1 2 3-4 5+6-7 8-9+10-11-12-13+14+15;1 2 3-4 5+6-7 8-9-10+11-12+13-14+15;1 2 3-4 5+6-7 8-9-10-11+12+13+14-15;1 2 3-4 5-6-7 8+9+10+11-12-13-14+15;1 2 3-4 5-6-7 8+9+10-11+12-13+14-15;1 2 3-4 5-6-7 8+9-10+11+12+13-14-15;1 2 3-4+5 6+7+8+9 10-11 12+13+14-15;1 2 3-4+5 6+7+8-9+10+11+12 13-14 15;1 2 3-4+5 6-7-8+9 10-11 12+13+14+15;1 2 3-4+5+6 7+8 9+10+11 12+13-14 15;1 2 3-4+5-6-7 8+9-10-11-12+13-14-15;1 2 3-4+5-6-7 8-9+10-11+12-13-14-15;1 2 3-4-5 6+7-8 9+10+11+12-13+14-15;1 2 3-4-5 6+7-8-9-10+11-12-13-14-15;1 2 3-4-5 6-7+8-9+10-11-12-13-14-15;1 2 3-4-5 6-7-8 9-10-11+12+13+14+15;1 2 3-4-5+6+7 8+9+10+11 12-13 14-15;1 2 3-4-5+6-7 8+9-10-11+12-13-14-15;1 2 3-4-5+6-7 8-9+10+11-12-13-14-15;1 2 3-4-5-6+7 8+9 10-11 12-13+14+15;1 2 3-4-5-6+7 8-9+10+11 12-13 14+15;1 2 3-4-5-6-7 8-9-10-11+12-13-14+15;1 2 3-4-5-6-7 8-9-10-11-12+13+14-15;1 2+3 4 5+6-7 8+9 10-11-12 13+14+15;1 2+3 4 5+6-7 8-9+10 11+12-13 14+15;1 2+3 4 5-6 7 8-9 10-11+12 13+14+15;1 2+3 4 5-6 7 8-9-10 11+12+13 14+15;1 2+3 4 5-6 7+8 9+10 11+12+13-14 15;1 2+3 4 5-6 7-8 9+10 11-12 13-14+15;1 2+3 4 5-6 7-8 9-10+11+12 13-14 15;1 2+3 4 5-6+7+8+9 10+11+12-13 14+15;1 2+3 4+5 6-7 8+9+10+11-12-13-14-15;1 2+3 4+5+6 7+8 9+10+11 12-13 14-15;1 2+3 4+5+6 7-8 9+10-11-12+13-14-15;1 2+3 4+5+6 7-8 9-10+11+12-13-14-15;1 2+3 4+5+6+7-8 9+10+11-12-13+14+15;1 2+3 4+5+6+7-8 9+10-11+12+13-14+15;1 2+3 4+5+6+7-8 9-10+11+12+13+14-15;1 2+3 4+5+6+7-8-9-10-11-12-13+14-15;1 2+3 4+5+6-7+8-9-10-11-12+13-14-15;1 2+3 4+5+6-7-8+9-10-11+12-13-14-15;1 2+3 4+5+6-7-8-9+10+11-12-13-14-15;1 2+3 4+5-6 7+8+9+10-11+12-13-14+15;1 2+3 4+5-6 7+8+9+10-11-12+13+14-15;1 2+3 4+5-6 7+8+9-10+11+12-13+14-15;1 2+3 4+5-6 7+8-9+10+11+12+13-14-15;1 2+3 4+5-6 7-8-9-10-11+12+13+14+15;1 2+3 4+5-6+7+8-9-10-11+12-13-14-15;1 2+3 4+5-6+7-8+9-10+11-12-13-14-15;1 2+3 4+5-6-7+8+9+10-11-12-13-14-15;1 2+3 4+5-6-7-8 9+10+11-12+13+14+15;1 2+3 4+5-6-7-8-9-10-11+12-13-14+15;1 2+3 4+5-6-7-8-9-10-11-12+13+14-15;1 2+3 4-5+6 7 8-9 10-11-12 13+14 15;1 2+3 4-5+6 7+8 9-10+11 12-13 14+15;1 2+3 4-5+6 7-8 9-10-11-12+13-14+15;1 2+3 4-5+6+7+8-9-10+11-12-13-14-15;1 2+3 4-5+6+7-8+9+10-11-12-13-14-15;1 2+3 4-5+6-7-8 9+10+11+12-13+14+15;1 2+3 4-5+6-7-8-9-10+11-12-13-14+15;1 2+3 4-5+6-7-8-9-10-11+12-13+14-15;1 2+3 4-5-6 7+8+9-10-11-12+13+14+15;1 2+3 4-5-6 7+8-9+10-11+12-13+14+15;1 2+3 4-5-6 7+8-9-10+11+12+13-14+15;1 2+3 4-5-6 7-8+9+10+11-12-13+14+15;1 2+3 4-5-6 7-8+9+10-11+12+13-14+15;1 2+3 4-5-6 7-8+9-10+11+12+13+14-15;1 2+3 4-5-6+7-8 9+10+11+12+13-14+15;1 2+3 4-5-6+7-8-9+10-11-12-13-14+15;1 2+3 4-5-6+7-8-9-10+11-12-13+14-15;1 2+3 4-5-6+7-8-9-10-11+12+13-14-15;1 2+3 4-5-6-7+8+9-10-11-12-13-14+15;1 2+3 4-5-6-7+8-9+10-11-12-13+14-15;1 2+3 4-5-6-7+8-9-10+11-12+13-14-15;1 2+3 4-5-6-7-8+9+10-11-12+13-14-15;1 2+3 4-5-6-7-8+9-10+11+12-13-14-15;1 2+3+4 5+6-7 8+9+10-11-12-13+14+15;1 2+3+4 5+6-7 8+9-10+11-12+13-14+15;1 2+3+4 5+6-7 8+9-10-11+12+13+14-15;1 2+3+4 5+6-7 8-9+10+11+12-13-14+15;1 2+3+4 5+6-7 8-9+10+11-12+13+14-15;1 2+3+4 5-6-7 8-9-10-11+12+13+14+15;1 2+3+4+5 6 7+8-9 10-11 12+13+14 15;1 2+3+4+5 6+7-8 9-10-11+12-13+14+15;1 2+3+4+5 6-7-8 9+10+11+12-13-14+15;1 2+3+4+5 6-7-8 9+10+11-12+13+14-15;1 2+3+4+5 6-7-8-9-10-11+12-13-14-15;1 2+3+4+5-6-7 8+9+10+11-12+13+14+15;1 2+3+4-5 6+7+8-9-10+11-12+13+14+15;1 2+3+4-5 6+7-8+9+10-11-12+13+14+15;1 2+3+4-5 6+7-8+9-10+11+12-13+14+15;1 2+3+4-5 6+7-8-9+10+11+12+13-14+15;1 2+3+4-5 6-7+8 9-10-11-12-13-14+15;1 2+3+4-5 6-7+8+9+10-11+12-13+14+15;1 2+3+4-5 6-7+8+9-10+11+12+13-14+15;1 2+3+4-5 6-7+8-9+10+11+12+13+14-15;1 2+3+4-5+6+7 8 9-10 11-12 13+14 15;1 2+3+4-5+6-7 8+9+10+11+12-13+14+15;1 2+3-4 5 6+7-8-9 10+11+12+13 14+15;1 2+3-4 5 6-7 8-9 10-11+12+13+14 15;1 2+3-4 5+6+7 8-9-10-11-12-13-14+15;1 2+3-4 5-6+7 8+9-10-11+12-13-14-15;1 2+3-4 5-6+7 8-9+10+11-12-13-14-15;1 2+3-4+5 6+7-8+9-10-11-12-13-14-15;1 2+3-4+5 6-7-8 9+10-11-12+13+14+15;1 2+3-4+5 6-7-8 9-10+11+12-13+14+15;1 2+3-4+5+6-7 8+9+10+11+12+13-14+15;1 2+3-4-5 6+7+8 9-10-11+12-13-14-15;1 2+3-4-5 6+7+8+9+10+11+12-13-14+15;1 2+3-4-5 6+7+8+9+10+11-12+13+14-15;1 2+3-4-5 6+7-8-9-10+11+12+13+14+15;1 2+3-4-5 6-7+8-9+10-11+12+13+14+15;1 2+3-4-5 6-7-8+9+10+11-12+13+14+15;1 2+3-4-5+6-7 8-9+10+11+12+13+14+15;1 2-3 4 5+6 7+8 9-10-11 12+13 14-15;1 2-3 4 5+6+7+8 9-10 11+12 13+14+15;1 2-3 4 5+6-7 8 9-10+11 12+13-14+15;1 2-3 4+5 6-7 8-9+10-11+12+13+14+15;1 2-3 4+5+6 7+8-9-10-11-12+13-14-15;1 2-3 4+5+6 7-8+9-10-11+12-13-14-15;1 2-3 4+5+6 7-8-9+10+11-12-13-14-15;1 2-3 4+5+6+7+8-9-10+11-12-13+14+15;1 2-3 4+5+6+7+8-9-10-11+12+13-14+15;1 2-3 4+5+6+7-8+9+10-11-12-13+14+15;1 2-3 4+5+6+7-8+9-10+11-12+13-14+15;1 2-3 4+5+6+7-8+9-10-11+12+13+14-15;1 2-3 4+5+6+7-8-9+10+11+12-13-14+15;1 2-3 4+5+6+7-8-9+10+11-12+13+14-15;1 2-3 4+5+6-7+8+9+10-11-12+13-14+15;1 2-3 4+5+6-7+8+9-10+11+12-13-14+15;1 2-3 4+5+6-7+8+9-10+11-12+13+14-15;1 2-3 4+5+6-7+8-9+10+11+12-13+14-15;1 2-3 4+5+6-7-8+9+10+11+12+13-14-15;1 2-3 4+5-6 7+8 9+10+11-12-13+14-15;1 2-3 4+5-6 7+8 9+10-11+12+13-14-15;1 2-3 4+5-6 7-8 9-10 11+12 13-14-15;1 2-3 4+5-6+7+8+9+10-11+12-13-14+15;1 2-3 4+5-6+7+8+9+10-11-12+13+14-15;1 2-3 4+5-6+7+8+9-10+11+12-13+14-15;1 2-3 4+5-6+7+8-9+10+11+12+13-14-15;1 2-3 4+5-6+7-8-9-10-11+12+13+14+15;1 2-3 4+5-6-7+8-9-10+11-12+13+14+15;1 2-3 4+5-6-7-8+9+10-11-12+13+14+15;1 2-3 4+5-6-7-8+9-10+11+12-13+14+15;1 2-3 4+5-6-7-8-9+10+11+12+13-14+15;1 2-3 4-5 6+7 8+9+10+11+12-13-14-15;1 2-3 4-5 6+7 8-9-10-11-12+13+14+15;1 2-3 4-5+6 7-8 9+10+11+12-13+14+15;1 2-3 4-5+6 7-8-9-10+11-12-13-14+15;1 2-3 4-5+6 7-8-9-10-11+12-13+14-15;1 2-3 4-5+6+7+8 9-10-11-12-13-14-15;1 2-3 4-5+6+7+8+9+10+11-12-13-14+15;1 2-3 4-5+6+7+8+9+10-11+12-13+14-15;1 2-3 4-5+6+7+8+9-10+11+12+13-14-15;1 2-3 4-5+6+7-8-9-10+11-12+13+14+15;1 2-3 4-5+6-7+8-9+10-11-12+13+14+15;1 2-3 4-5+6-7+8-9-10+11+12-13+14+15;1 2-3 4-5+6-7-8+9+10-11+12-13+14+15;1 2-3 4-5+6-7-8+9-10+11+12+13-14+15;1 2-3 4-5+6-7-8-9+10+11+12+13+14-15;1 2-3 4-5-6 7+8 9-10+11-12-13+14+15;1 2-3 4-5-6 7+8 9-10-11+12+13-14+15;1 2-3 4-5-6+7+8+9-10-11-12+13+14+15;1 2-3 4-5-6+7+8-9+10-11+12-13+14+15;1 2-3 4-5-6+7+8-9-10+11+12+13-14+15;1 2-3 4-5-6+7-8+9+10+11-12-13+14+15;1 2-3 4-5-6+7-8+9+10-11+12+13-14+15;1 2-3 4-5-6+7-8+9-10+11+12+13+14-15;1 2-3 4-5-6-7+8 9-10-11-12+13-14-15;1 2-3 4-5-6-7+8+9+10+11-12+13-14+15;1 2-3 4-5-6-7+8+9+10-11+12+13+14-15;1 2-3+4 5+6-7 8+9-10-11-12+13+14+15;1 2-3+4 5+6-7 8-9+10-11+12-13+14+15;1 2-3+4 5+6-7 8-9-10+11+12+13-14+15;1 2-3+4 5-6-7 8+9+10+11+12-13-14+15;1 2-3+4 5-6-7 8+9+10+11-12+13+14-15;1 2-3+4+5 6+7+8-9-10-11-12-13-14-15;1 2-3+4+5 6-7-8 9+10-11+12-13+14+15;1 2-3+4+5 6-7-8 9-10+11+12+13-14+15;1 2-3+4+5 6-7-8-9-10-11-12-13-14+15;1 2-3+4+5+6-7 8+9+10+11+12+13+14-15;1 2-3+4+5-6-7 8-9+10+11+12+13+14+15;1 2-3+4-5 6+7+8 9-10+11-12-13-14-15;1 2-3+4-5 6+7+8+9+10+11+12-13+14-15;1 2-3+4-5 6+7-8-9+10-11+12+13+14+15;1 2-3+4-5 6-7+8+9-10-11+12+13+14+15;1 2-3+4-5 6-7+8-9+10+11-12+13+14+15;1 2-3+4-5 6-7-8+9+10+11+12-13+14+15;1 2-3+4-5+6-7 8+9-10+11+12+13+14+15;1 2-3-4 5-6+7 8+9-10-11-12-13-14+15;1 2-3-4 5-6+7 8-9+10-11-12-13+14-15;1 2-3-4 5-6+7 8-9-10+11-12+13-14-15;1 2-3-4+5 6+7-8 9+10+11+12-13-14+15;1 2-3-4+5 6+7-8 9+10+11-12+13+14-15;1 2-3-4+5 6+7-8-9-10-11+12-13-14-15;1 2-3-4+5 6-7+8-9-10+11-12-13-14-15;1 2-3-4+5 6-7-8+9+10-11-12-13-14-15;1 2-3-4+5+6-7 8+9+10-11+12+13+14+15;1 2-3-4-5 6+7+8 9-10-11-12-13-14+15;1 2-3-4-5 6+7+8+9+10-11+12-13+14+15;1 2-3-4-5 6+7+8+9-10+11+12+13-14+15;1 2-3-4-5 6+7+8-9+10+11+12+13+14-15;1 2-3-4-5 6-7+8 9+10-11+12-13-14-15;1 2-3-4-5 6-7-8-9+10+11+12+13+14+15;1 2-3-4-5+6+7 8-9-10-11-12-13-14-15;1 2-3-4-5-6-7 8+9+10+11+12+13+14+15;1+2 3 4 5+6+7 8-9-10-11 12-13 14+15;1+2 3 4 5+6-7-8-9 10-11+12-13-14 15;1+2 3 4 5+6-7-8-9-10 11+12-13 14-15;1+2 3 4 5-6+7 8+9-10 11+12-13-14 15;1+2 3 4 5-6+7-8-9 10+11-12-13-14 15;1+2 3 4 5-6-7 8-9 10-11-12-13 14-15;1+2 3 4 5-6-7 8-9-10 11-12 13-14-15;1+2 3 4+5 6+7 8 9-10-11 12+13+14+15;1+2 3 4+5+6 7+8+9 10-11-12 13+14-15;1+2 3 4+5+6 7+8-9+10 11+12-13 14-15;1+2 3 4+5+6 7+8-9+10+11 12-13-14 15;1+2 3 4+5+6+7 8 9-10 11-12-13-14+15;1+2 3 4+5+6-7 8+9+10+11 12-13 14+15;1+2 3 4+5-6 7 8+9-10 11+12+13+14 15;1+2 3 4+5-6+7+8 9+10 11-12-13 14-15;1+2 3 4-5 6+7+8+9+10 11-12 13+14-15;1+2 3 4-5 6+7+8+9+10-11+12 13-14 15;1+2 3 4-5 6-7-8+9+10 11-12 13+14+15;1+2 3 4-5+6 7-8-9+10+11 12+13-14 15;1+2 3 4-5-6+7 8 9-10 11+12-13+14-15;1+2 3 4-5-6-7+8 9+10 11+12-13 14-15;1+2 3 4-5-6-7+8 9+10+11 12-13-14 15;1+2 3+4 5 6+7+8+9 10+11+12-13-14 15;1+2 3+4 5+6 7-8 9-10-11-12-13+14-15;1+2 3+4 5+6+7-8 9-10-11+12-13+14+15;1+2 3+4 5+6-7-8 9+10+11+12-13-14+15;1+2 3+4 5+6-7-8 9+10+11-12+13+14-15;1+2 3+4 5+6-7-8-9-10-11+12-13-14-15;1+2 3+4 5-6 7+8+9-10-11-12+13-14+15;1+2 3+4 5-6 7+8-9+10-11+12-13-14+15;1+2 3+4 5-6 7+8-9+10-11-12+13+14-15;1+2 3+4 5-6 7+8-9-10+11+12-13+14-15;1+2 3+4 5-6 7-8+9+10+11-12-13-14+15;1+2 3+4 5-6 7-8+9+10-11+12-13+14-15;1+2 3+4 5-6 7-8+9-10+11+12+13-14-15;1+2 3+4 5-6+7-8 9+10+11+12-13+14-15;1+2 3+4 5-6+7-8-9-10+11-12-13-14-15;1+2 3+4 5-6-7+8-9+10-11-12-13-14-15;1+2 3+4 5-6-7-8 9-10-11+12+13+14+15;1+2 3+4+5 6-7 8-9+10-11-12-13+14+15;1+2 3+4+5 6-7 8-9-10+11-12+13-14+15;1+2 3+4+5 6-7 8-9-10-11+12+13+14-15;1+2 3+4+5+6+7+8-9-10-11-12-13-14+15;1+2 3+4+5+6+7-8+9-10-11-12-13+14-15;1+2 3+4+5+6+7-8-9+10-11-12+13-14-15;1+2 3+4+5+6+7-8-9-10+11+12-13-14-15;1+2 3+4+5+6-7+8+9-10-11-12+13-14-15;1+2 3+4+5+6-7+8-9+10-11+12-13-14-15;1+2 3+4+5+6-7-8+9+10+11-12-13-14-15;1+2 3+4+5-6 7+8 9+10-11-12-13-14-15;1+2 3+4+5-6 7+8+9+10+11+12+13-14-15;1+2 3+4+5-6 7-8+9-10-11+12+13+14+15;1+2 3+4+5-6 7-8-9+10+11-12+13+14+15;1+2 3+4+5-6+7+8+9-10-11+12-13-14-15;1+2 3+4+5-6+7+8-9+10+11-12-13-14-15;1+2 3+4+5-6+7-8 9-10+11+12+13+14+15;1+2 3+4+5-6+7-8-9-10-11-12-13+14+15;1+2 3+4+5-6-7+8-9-10-11-12+13-14+15;1+2 3+4+5-6-7-8+9-10-11+12-13-14+15;1+2 3+4+5-6-7-8+9-10-11-12+13+14-15;1+2 3+4+5-6-7-8-9+10+11-12-13-14+15;1+2 3+4+5-6-7-8-9+10-11+12-13+14-15;1+2 3+4+5-6-7-8-9-10+11+12+13-14-15;1+2 3+4-5+6 7-8 9+10-11+12-13-14+15;1+2 3+4-5+6 7-8 9+10-11-12+13+14-15;1+2 3+4-5+6 7-8 9-10+11+12-13+14-15;1+2 3+4-5+6+7+8+9-10+11-12-13-14-15;1+2 3+4-5+6+7-8 9+10-11+12+13+14+15;1+2 3+4-5+6+7-8-9-10-11-12+13-14+15;1+2 3+4-5+6-7+8-9-10-11+12-13-14+15;1+2 3+4-5+6-7+8-9-10-11-12+13+14-15;1+2 3+4-5+6-7-8+9-10+11-12-13-14+15;1+2 3+4-5+6-7-8+9-10-11+12-13+14-15;1+2 3+4-5+6-7-8-9+10+11-12-13+14-15;1+2 3+4-5+6-7-8-9+10-11+12+13-14-15;1+2 3+4-5-6 7+8 9-10-11-12-13-14+15;1+2 3+4-5-6 7+8+9+10-11+12-13+14+15;1+2 3+4-5-6 7+8+9-10+11+12+13-14+15;1+2 3+4-5-6 7+8-9+10+11+12+13+14-15;1+2 3+4-5-6+7+8-9-10+11-12-13-14+15;1+2 3+4-5-6+7+8-9-10-11+12-13+14-15;1+2 3+4-5-6+7-8+9+10-11-12-13-14+15;1+2 3+4-5-6+7-8+9-10+11-12-13+14-15;1+2 3+4-5-6+7-8+9-10-11+12+13-14-15;1+2 3+4-5-6+7-8-9+10+11-12+13-14-15;1+2 3+4-5-6-7+8+9+10-11-12-13+14-15;1+2 3+4-5-6-7+8+9-10+11-12+13-14-15;1+2 3+4-5-6-7+8-9+10+11+12-13-14-15;1+2 3+4-5-6-7-8-9-10-11+12-13+14+15;1+2 3-4 5 6-7 8 9-10-11+12 13+14+15;1+2 3-4 5+6 7+8-9-10-11-12-13-14+15;1+2 3-4 5+6 7-8+9-10-11-12-13+14-15;1+2 3-4 5+6 7-8-9+10-11-12+13-14-15;1+2 3-4 5+6 7-8-9-10+11+12-13-14-15;1+2 3-4 5+6+7+8+9+10+11+12-13-14-15;1+2 3-4 5+6+7+8-9-10-11-12+13+14+15;1+2 3-4 5+6+7-8+9-10-11+12-13+14+15;1+2 3-4 5+6+7-8-9+10+11-12-13+14+15;1+2 3-4 5+6+7-8-9+10-11+12+13-14+15;1+2 3-4 5+6+7-8-9-10+11+12+13+14-15;1+2 3-4 5+6-7+8+9-10+11-12-13+14+15;1+2 3-4 5+6-7+8+9-10-11+12+13-14+15;1+2 3-4 5+6-7+8-9+10+11-12+13-14+15;1+2 3-4 5+6-7+8-9+10-11+12+13+14-15;1+2 3-4 5+6-7-8+9+10+11+12-13-14+15;1+2 3-4 5+6-7-8+9+10+11-12+13+14-15;1+2 3-4 5-6 7+8 9+10-11+12-13-14+15;1+2 3-4 5-6 7+8 9+10-11-12+13+14-15;1+2 3-4 5-6 7+8 9-10+11+12-13+14-15;1+2 3-4 5-6 7-8 9-10-11 12+13 14-15;1+2 3-4 5-6+7+8+9+10-11-12-13+14+15;1+2 3-4 5-6+7+8+9-10+11-12+13-14+15;1+2 3-4 5-6+7+8+9-10-11+12+13+14-15;1+2 3-4 5-6+7+8-9+10+11+12-13-14+15;1+2 3-4 5-6+7+8-9+10+11-12+13+14-15;1+2 3-4 5-6+7-8+9+10+11+12-13+14-15;1+2 3-4 5-6-7+8 9+10-11-12-13-14-15;1+2 3-4 5-6-7+8+9+10+11+12+13-14-15;1+2 3-4 5-6-7-8+9-10-11+12+13+14+15;1+2 3-4 5-6-7-8-9+10+11-12+13+14+15;1+2 3-4+5 6-7 8+9+10+11-12+13-14-15;1+2 3-4+5+6 7+8 9+10+11+12 13-14 15;1+2 3-4+5+6 7-8 9+10+11-12-13-14+15;1+2 3-4+5+6 7-8 9+10-11+12-13+14-15;1+2 3-4+5+6 7-8 9-10+11+12+13-14-15;1+2 3-4+5+6 7-8-9-10-11-12-13-14-15;1+2 3-4+5+6+7+8+9+10-11-12-13-14-15;1+2 3-4+5+6+7-8 9+10+11-12+13+14+15;1+2 3-4+5+6+7-8-9-10-11+12-13-14+15;1+2 3-4+5+6+7-8-9-10-11-12+13+14-15;1+2 3-4+5+6-7+8-9-10+11-12-13-14+15;1+2 3-4+5+6-7+8-9-10-11+12-13+14-15;1+2 3-4+5+6-7-8+9+10-11-12-13-14+15;1+2 3-4+5+6-7-8+9-10+11-12-13+14-15;1+2 3-4+5+6-7-8+9-10-11+12+13-14-15;1+2 3-4+5+6-7-8-9+10+11-12+13-14-15;1+2 3-4+5-6 7+8 9-10-11-12-13+14-15;1+2 3-4+5-6 7+8+9+10+11-12-13+14+15;1+2 3-4+5-6 7+8+9+10-11+12+13-14+15;1+2 3-4+5-6 7+8+9-10+11+12+13+14-15;1+2 3-4+5-6+7+8-9+10-11-12-13-14+15;1+2 3-4+5-6+7+8-9-10+11-12-13+14-15;1+2 3-4+5-6+7+8-9-10-11+12+13-14-15;1+2 3-4+5-6+7-8+9+10-11-12-13+14-15;1+2 3-4+5-6+7-8+9-10+11-12+13-14-15;1+2 3-4+5-6+7-8-9+10+11+12-13-14-15;1+2 3-4+5-6-7+8+9+10-11-12+13-14-15;1+2 3-4+5-6-7+8+9-10+11+12-13-14-15;1+2 3-4+5-6-7-8-9-10+11-12-13+14+15;1+2 3-4+5-6-7-8-9-10-11+12+13-14+15;1+2 3-4-5 6+7 8+9-10-11+12-13-14-15;1+2 3-4-5 6+7 8-9+10+11-12-13-14-15;1+2 3-4-5+6 7 8-9 10-11 12+13 14+15;1+2 3-4-5+6 7-8 9-10-11+12-13+14+15;1+2 3-4-5+6+7+8+9-10-11-12-13-14+15;1+2 3-4-5+6+7+8-9+10-11-12-13+14-15;1+2 3-4-5+6+7+8-9-10+11-12+13-14-15;1+2 3-4-5+6+7-8+9+10-11-12+13-14-15;1+2 3-4-5+6+7-8+9-10+11+12-13-14-15;1+2 3-4-5+6-7+8+9+10-11+12-13-14-15;1+2 3-4-5+6-7-8 9+10+11+12+13+14+15;1+2 3-4-5+6-7-8-9+10-11-12-13+14+15;1+2 3-4-5+6-7-8-9-10+11-12+13-14+15;1+2 3-4-5+6-7-8-9-10-11+12+13+14-15;1+2 3-4-5-6 7+8-9+10-11+12+13+14+15;1+2 3-4-5-6 7-8+9+10+11-12+13+14+15;1+2 3-4-5-6+7+8+9+10+11-12-13-14-15;1+2 3-4-5-6+7-8+9-10-11-12-13+14+15;1+2 3-4-5-6+7-8-9+10-11-12+13-14+15;1+2 3-4-5-6+7-8-9-10+11+12-13-14+15;1+2 3-4-5-6+7-8-9-10+11-12+13+14-15;1+2 3-4-5-6-7+8+9-10-11-12+13-14+15;1+2 3-4-5-6-7+8-9+10-11+12-13-14+15;1+2 3-4-5-6-7+8-9+10-11-12+13+14-15;1+2 3-4-5-6-7+8-9-10+11+12-13+14-15;1+2 3-4-5-6-7-8+9+10+11-12-13-14+15;1+2 3-4-5-6-7-8+9+10-11+12-13+14-15;1+2 3-4-5-6-7-8+9-10+11+12+13-14-15;1+2+3 4 5+6 7 8+9-10 11-12-13-14+15;1+2+3 4 5+6 7-8 9-10+11 12-13-14 15;1+2+3 4 5+6-7-8-9+10 11-12-13 14-15;1+2+3 4 5-6 7+8-9+10+11 12+13-14 15;1+2+3 4 5-6+7 8+9 10+11-12-13 14-15;1+2+3 4 5-6+7 8+9+10 11-12-13-14 15;1+2+3 4 5-6-7+8+9 10-11-12 13-14-15;1+2+3 4+5 6-7-8 9+10-11-12-13+14+15;1+2+3 4+5 6-7-8 9-10+11-12+13-14+15;1+2+3 4+5 6-7-8 9-10-11+12+13+14-15;1+2+3 4+5+6-7 8+9+10+11+12-13-14+15;1+2+3 4+5+6-7 8+9+10+11-12+13+14-15;1+2+3 4+5-6-7 8+9-10-11+12+13+14+15;1+2+3 4+5-6-7 8-9+10+11-12+13+14+15;1+2+3 4-5 6+7+8+9+10+11-12-13+14-15;1+2+3 4-5 6+7+8+9+10-11+12+13-14-15;1+2+3 4-5 6+7-8-9+10-11-12+13+14+15;1+2+3 4-5 6+7-8-9-10+11+12-13+14+15;1+2+3 4-5 6-7+8+9-10-11-12+13+14+15;1+2+3 4-5 6-7+8-9+10-11+12-13+14+15;1+2+3 4-5 6-7+8-9-10+11+12+13-14+15;1+2+3 4-5 6-7-8+9+10+11-12-13+14+15;1+2+3 4-5 6-7-8+9+10-11+12+13-14+15;1+2+3 4-5 6-7-8+9-10+11+12+13+14-15;1+2+3 4-5+6-7 8+9-10+11-12+13+14+15;1+2+3 4-5+6-7 8-9+10+11+12-13+14+15;1+2+3+4 5+6 7+8 9+10+11 12-13 14-15;1+2+3+4 5+6 7-8 9+10-11-12+13-14-15;1+2+3+4 5+6 7-8 9-10+11+12-13-14-15;1+2+3+4 5+6+7-8 9+10+11-12-13+14+15;1+2+3+4 5+6+7-8 9+10-11+12+13-14+15;1+2+3+4 5+6+7-8 9-10+11+12+13+14-15;1+2+3+4 5+6+7-8-9-10-11-12-13+14-15;1+2+3+4 5+6-7+8-9-10-11-12+13-14-15;1+2+3+4 5+6-7-8+9-10-11+12-13-14-15;1+2+3+4 5+6-7-8-9+10+11-12-13-14-15;1+2+3+4 5-6 7+8+9+10-11+12-13-14+15;1+2+3+4 5-6 7+8+9+10-11-12+13+14-15;1+2+3+4 5-6 7+8+9-10+11+12-13+14-15;1+2+3+4 5-6 7+8-9+10+11+12+13-14-15;1+2+3+4 5-6 7-8-9-10-11+12+13+14+15;1+2+3+4 5-6+7+8-9-10-11+12-13-14-15;1+2+3+4 5-6+7-8+9-10+11-12-13-14-15;1+2+3+4 5-6-7+8+9+10-11-12-13-14-15;1+2+3+4 5-6-7-8 9+10+11-12+13+14+15;1+2+3+4 5-6-7-8-9-10-11+12-13-14+15;1+2+3+4 5-6-7-8-9-10-11-12+13+14-15;1+2+3+4+5 6-7 8+9+10-11-12-13+14+15;1+2+3+4+5 6-7 8+9-10+11-12+13-14+15;1+2+3+4+5 6-7 8+9-10-11+12+13+14-15;1+2+3+4+5 6-7 8-9+10+11+12-13-14+15;1+2+3+4+5 6-7 8-9+10+11-12+13+14-15;1+2+3+4+5+6 7 8-9 10-11 12+13 14+15;1+2+3+4+5+6 7-8 9-10-11+12-13+14+15;1+2+3+4+5+6+7+8+9-10-11-12-13-14+15;1+2+3+4+5+6+7+8-9+10-11-12-13+14-15;1+2+3+4+5+6+7+8-9-10+11-12+13-14-15;1+2+3+4+5+6+7-8+9+10-11-12+13-14-15;1+2+3+4+5+6+7-8+9-10+11+12-13-14-15;1+2+3+4+5+6-7+8+9+10-11+12-13-14-15;1+2+3+4+5+6-7-8 9+10+11+12+13+14+15;1+2+3+4+5+6-7-8-9+10-11-12-13+14+15;1+2+3+4+5+6-7-8-9-10+11-12+13-14+15;1+2+3+4+5+6-7-8-9-10-11+12+13+14-15;1+2+3+4+5-6 7+8-9+10-11+12+13+14+15;1+2+3+4+5-6 7-8+9+10+11-12+13+14+15;1+2+3+4+5-6+7+8+9+10+11-12-13-14-15;1+2+3+4+5-6+7-8+9-10-11-12-13+14+15;1+2+3+4+5-6+7-8-9+10-11-12+13-14+15;1+2+3+4+5-6+7-8-9-10+11+12-13-14+15;1+2+3+4+5-6+7-8-9-10+11-12+13+14-15;1+2+3+4+5-6-7+8+9-10-11-12+13-14+15;1+2+3+4+5-6-7+8-9+10-11+12-13-14+15;1+2+3+4+5-6-7+8-9+10-11-12+13+14-15;1+2+3+4+5-6-7+8-9-10+11+12-13+14-15;1+2+3+4+5-6-7-8+9+10+11-12-13-14+15;1+2+3+4+5-6-7-8+9+10-11+12-13+14-15;1+2+3+4+5-6-7-8+9-10+11+12+13-14-15;1+2+3+4-5 6+7 8-9-10+11-12-13-14+15;1+2+3+4-5 6+7 8-9-10-11+12-13+14-15;1+2+3+4-5+6 7-8 9+10+11+12+13-14-15;1+2+3+4-5+6 7-8-9+10-11-12-13-14-15;1+2+3+4-5+6+7+8-9-10-11-12-13+14+15;1+2+3+4-5+6+7-8+9-10-11-12+13-14+15;1+2+3+4-5+6+7-8-9+10-11+12-13-14+15;1+2+3+4-5+6+7-8-9+10-11-12+13+14-15;1+2+3+4-5+6+7-8-9-10+11+12-13+14-15;1+2+3+4-5+6-7+8+9-10-11+12-13-14+15;1+2+3+4-5+6-7+8+9-10-11-12+13+14-15;1+2+3+4-5+6-7+8-9+10+11-12-13-14+15;1+2+3+4-5+6-7+8-9+10-11+12-13+14-15;1+2+3+4-5+6-7+8-9-10+11+12+13-14-15;1+2+3+4-5+6-7-8+9+10+11-12-13+14-15;1+2+3+4-5+6-7-8+9+10-11+12+13-14-15;1+2+3+4-5-6 7+8 9+10-11-12-13+14-15;1+2+3+4-5-6 7+8 9-10+11-12+13-14-15;1+2+3+4-5-6 7+8+9+10+11+12+13+14-15;1+2+3+4-5-6+7+8+9-10+11-12-13-14+15;1+2+3+4-5-6+7+8+9-10-11+12-13+14-15;1+2+3+4-5-6+7+8-9+10+11-12-13+14-15;1+2+3+4-5-6+7+8-9+10-11+12+13-14-15;1+2+3+4-5-6+7-8+9+10+11-12+13-14-15;1+2+3+4-5-6-7+8+9+10+11+12-13-14-15;1+2+3+4-5-6-7+8-9-10-11-12+13+14+15;1+2+3+4-5-6-7-8+9-10-11+12-13+14+15;1+2+3+4-5-6-7-8-9+10+11-12-13+14+15;1+2+3+4-5-6-7-8-9+10-11+12+13-14+15;1+2+3+4-5-6-7-8-9-10+11+12+13+14-15;1+2+3-4 5+6 7+8+9-10-11-12-13-14+15;1+2+3-4 5+6 7+8-9+10-11-12-13+14-15;1+2+3-4 5+6 7+8-9-10+11-12+13-14-15;1+2+3-4 5+6 7-8+9+10-11-12+13-14-15;1+2+3-4 5+6 7-8+9-10+11+12-13-14-15;1+2+3-4 5+6+7+8+9-10-11-12+13+14+15;1+2+3-4 5+6+7+8-9+10-11+12-13+14+15;1+2+3-4 5+6+7+8-9-10+11+12+13-14+15;1+2+3-4 5+6+7-8+9+10+11-12-13+14+15;1+2+3-4 5+6+7-8+9+10-11+12+13-14+15;1+2+3-4 5+6+7-8+9-10+11+12+13+14-15;1+2+3-4 5+6-7+8 9-10-11-12+13-14-15;1+2+3-4 5+6-7+8+9+10+11-12+13-14+15;1+2+3-4 5+6-7+8+9+10-11+12+13+14-15;1+2+3-4 5-6 7+8 9+10+11+12+13-14-15;1+2+3-4 5-6+7+8 9-10-11+12-13-14-15;1+2+3-4 5-6+7+8+9+10+11+12-13-14+15;1+2+3-4 5-6+7+8+9+10+11-12+13+14-15;1+2+3-4 5-6+7-8-9-10+11+12+13+14+15;1+2+3-4 5-6-7+8-9+10-11+12+13+14+15;1+2+3-4 5-6-7-8+9+10+11-12+13+14+15;1+2+3-4+5 6-7 8-9+10-11-12+13+14+15;1+2+3-4+5 6-7 8-9-10+11+12-13+14+15;1+2+3-4+5+6 7-8+9-10-11-12-13-14-15;1+2+3-4+5+6+7+8-9-10-11-12+13-14+15;1+2+3-4+5+6+7-8+9-10-11+12-13-14+15;1+2+3-4+5+6+7-8+9-10-11-12+13+14-15;1+2+3-4+5+6+7-8-9+10+11-12-13-14+15;1+2+3-4+5+6+7-8-9+10-11+12-13+14-15;1+2+3-4+5+6+7-8-9-10+11+12+13-14-15;1+2+3-4+5+6-7+8+9-10+11-12-13-14+15;1+2+3-4+5+6-7+8+9-10-11+12-13+14-15;1+2+3-4+5+6-7+8-9+10+11-12-13+14-15;1+2+3-4+5+6-7+8-9+10-11+12+13-14-15;1+2+3-4+5+6-7-8+9+10+11-12+13-14-15;1+2+3-4+5-6 7+8 9+10-11-12+13-14-15;1+2+3-4+5-6 7+8 9-10+11+12-13-14-15;1+2+3-4+5-6+7+8+9+10-11-12-13-14+15;1+2+3-4+5-6+7+8+9-10+11-12-13+14-15;1+2+3-4+5-6+7+8+9-10-11+12+13-14-15;1+2+3-4+5-6+7+8-9+10+11-12+13-14-15;1+2+3-4+5-6+7-8+9+10+11+12-13-14-15;1+2+3-4+5-6+7-8-9-10-11-12+13+14+15;1+2+3-4+5-6-7+8-9-10-11+12-13+14+15;1+2+3-4+5-6-7-8+9-10+11-12-13+14+15;1+2+3-4+5-6-7-8+9-10-11+12+13-14+15;1+2+3-4+5-6-7-8-9+10+11-12+13-14+15;1+2+3-4+5-6-7-8-9+10-11+12+13+14-15;1+2+3-4-5 6+7 8+9+10+11-12-13-14-15;1+2+3-4-5+6 7-8 9+10+11-12-13+14+15;1+2+3-4-5+6 7-8 9+10-11+12+13-14+15;1+2+3-4-5+6 7-8 9-10+11+12+13+14-15;1+2+3-4-5+6 7-8-9-10-11-12-13+14-15;1+2+3-4-5+6+7+8+9+10-11-12-13+14-15;1+2+3-4-5+6+7+8+9-10+11-12+13-14-15;1+2+3-4-5+6+7+8-9+10+11+12-13-14-15;1+2+3-4-5+6+7-8-9-10-11+12-13+14+15;1+2+3-4-5+6-7+8-9-10+11-12-13+14+15;1+2+3-4-5+6-7+8-9-10-11+12+13-14+15;1+2+3-4-5+6-7-8+9+10-11-12-13+14+15;1+2+3-4-5+6-7-8+9-10+11-12+13-14+15;1+2+3-4-5+6-7-8+9-10-11+12+13+14-15;1+2+3-4-5+6-7-8-9+10+11+12-13-14+15;1+2+3-4-5+6-7-8-9+10+11-12+13+14-15;1+2+3-4-5-6 7+8 9-10-11-12+13-14+15;1+2+3-4-5-6 7+8+9+10-11+12+13+14+15;1+2+3-4-5-6+7+8-9+10-11-12-13+14+15;1+2+3-4-5-6+7+8-9-10+11-12+13-14+15;1+2+3-4-5-6+7+8-9-10-11+12+13+14-15;1+2+3-4-5-6+7-8+9+10-11-12+13-14+15;1+2+3-4-5-6+7-8+9-10+11+12-13-14+15;1+2+3-4-5-6+7-8+9-10+11-12+13+14-15;1+2+3-4-5-6+7-8-9+10+11+12-13+14-15;1+2+3-4-5-6-7+8+9+10-11+12-13-14+15;1+2+3-4-5-6-7+8+9+10-11-12+13+14-15;1+2+3-4-5-6-7+8+9-10+11+12-13+14-15;1+2+3-4-5-6-7+8-9+10+11+12+13-14-15;1+2+3-4-5-6-7-8-9-10-11+12+13+14+15;1+2-3 4 5+6 7+8-9-10 11-12+13 14-15;1+2-3 4 5+6+7+8-9 10-11+12 13+14+15;1+2-3 4 5+6+7+8-9-10 11+12+13 14+15;1+2-3 4 5+6-7 8+9-10 11-12+13+14 15;1+2-3 4 5+6-7+8+9+10-11 12+13+14 15;1+2-3 4 5-6 7 8+9+10 11+12-13-14+15;1+2-3 4 5-6 7 8+9+10 11-12+13+14-15;1+2-3 4+5 6+7+8+9-10-11-12+13-14-15;1+2-3 4+5 6+7+8-9+10-11+12-13-14-15;1+2-3 4+5 6+7-8+9+10+11-12-13-14-15;1+2-3 4+5 6-7+8-9-10-11-12-13+14+15;1+2-3 4+5 6-7-8+9-10-11-12+13-14+15;1+2-3 4+5 6-7-8-9+10-11+12-13-14+15;1+2-3 4+5 6-7-8-9+10-11-12+13+14-15;1+2-3 4+5 6-7-8-9-10+11+12-13+14-15;1+2-3 4+5+6+7 8-9-10-11-12+13-14-15;1+2-3 4+5+6-7 8 9+10 11+12 13-14 15;1+2-3 4+5-6+7 8+9+10-11-12-13-14-15;1+2-3 4-5 6+7+8 9+10+11+12-13-14-15;1+2-3 4-5 6-7+8 9-10+11-12-13+14+15;1+2-3 4-5 6-7+8 9-10-11+12+13-14+15;1+2-3 4-5-6+7 8+9-10-11-12-13-14+15;1+2-3 4-5-6+7 8-9+10-11-12-13+14-15;1+2-3 4-5-6+7 8-9-10+11-12+13-14-15;1+2-3+4 5+6 7 8-9 10-11 12+13 14-15;1+2-3+4 5+6 7+8 9+10 11-12 13-14+15;1+2-3+4 5+6 7+8 9-10+11+12 13-14 15;1+2-3+4 5+6 7-8 9-10+11-12-13-14+15;1+2-3+4 5+6 7-8 9-10-11+12-13+14-15;1+2-3+4 5+6+7+8+9-10-11-12-13-14-15;1+2-3+4 5+6+7-8 9-10+11-12+13+14+15;1+2-3+4 5+6-7-8 9+10+11+12+13+14-15;1+2-3+4 5+6-7-8+9-10-11-12-13-14+15;1+2-3+4 5+6-7-8-9+10-11-12-13+14-15;1+2-3+4 5+6-7-8-9-10+11-12+13-14-15;1+2-3+4 5-6 7+8+9-10+11-12-13+14+15;1+2-3+4 5-6 7+8+9-10-11+12+13-14+15;1+2-3+4 5-6 7+8-9+10+11-12+13-14+15;1+2-3+4 5-6 7+8-9+10-11+12+13+14-15;1+2-3+4 5-6 7-8+9+10+11+12-13-14+15;1+2-3+4 5-6 7-8+9+10+11-12+13+14-15;1+2-3+4 5-6+7+8-9-10-11-12-13-14+15;1+2-3+4 5-6+7-8+9-10-11-12-13+14-15;1+2-3+4 5-6+7-8-9+10-11-12+13-14-15;1+2-3+4 5-6+7-8-9-10+11+12-13-14-15;1+2-3+4 5-6-7+8+9-10-11-12+13-14-15;1+2-3+4 5-6-7+8-9+10-11+12-13-14-15;1+2-3+4 5-6-7-8+9+10+11-12-13-14-15;1+2-3+4+5 6-7 8+9-10-11-12+13+14+15;1+2-3+4+5 6-7 8-9+10-11+12-13+14+15;1+2-3+4+5 6-7 8-9-10+11+12+13-14+15;1+2-3+4+5+6 7+8-9-10-11-12-13-14-15;1+2-3+4+5+6+7+8-9-10-11+12-13-14+15;1+2-3+4+5+6+7+8-9-10-11-12+13+14-15;1+2-3+4+5+6+7-8+9-10+11-12-13-14+15;1+2-3+4+5+6+7-8+9-10-11+12-13+14-15;1+2-3+4+5+6+7-8-9+10+11-12-13+14-15;1+2-3+4+5+6+7-8-9+10-11+12+13-14-15;1+2-3+4+5+6-7+8+9+10-11-12-13-14+15;1+2-3+4+5+6-7+8+9-10+11-12-13+14-15;1+2-3+4+5+6-7+8+9-10-11+12+13-14-15;1+2-3+4+5+6-7+8-9+10+11-12+13-14-15;1+2-3+4+5+6-7-8+9+10+11+12-13-14-15;1+2-3+4+5+6-7-8-9-10-11-12+13+14+15;1+2-3+4+5-6 7+8 9+10-11+12-13-14-15;1+2-3+4+5-6 7-8-9+10+11+12+13+14+15;1+2-3+4+5-6+7+8+9+10-11-12-13+14-15;1+2-3+4+5-6+7+8+9-10+11-12+13-14-15;1+2-3+4+5-6+7+8-9+10+11+12-13-14-15;1+2-3+4+5-6+7-8-9-10-11+12-13+14+15;1+2-3+4+5-6-7+8-9-10+11-12-13+14+15;1+2-3+4+5-6-7+8-9-10-11+12+13-14+15;1+2-3+4+5-6-7-8+9+10-11-12-13+14+15;1+2-3+4+5-6-7-8+9-10+11-12+13-14+15;1+2-3+4+5-6-7-8+9-10-11+12+13+14-15;1+2-3+4+5-6-7-8-9+10+11+12-13-14+15;1+2-3+4+5-6-7-8-9+10+11-12+13+14-15;1+2-3+4-5 6+7 8-9-10-11-12-13+14+15;1+2-3+4-5+6 7-8 9+10+11-12+13-14+15;1+2-3+4-5+6 7-8 9+10-11+12+13+14-15;1+2-3+4-5+6 7-8-9-10-11-12+13-14-15;1+2-3+4-5+6+7+8+9+10-11-12+13-14-15;1+2-3+4-5+6+7+8+9-10+11+12-13-14-15;1+2-3+4-5+6+7-8-9-10+11-12-13+14+15;1+2-3+4-5+6+7-8-9-10-11+12+13-14+15;1+2-3+4-5+6-7+8-9+10-11-12-13+14+15;1+2-3+4-5+6-7+8-9-10+11-12+13-14+15;1+2-3+4-5+6-7+8-9-10-11+12+13+14-15;1+2-3+4-5+6-7-8+9+10-11-12+13-14+15;1+2-3+4-5+6-7-8+9-10+11+12-13-14+15;1+2-3+4-5+6-7-8+9-10+11-12+13+14-15;1+2-3+4-5+6-7-8-9+10+11+12-13+14-15;1+2-3+4-5-6 7+8 9-10-11+12-13-14+15;1+2-3+4-5-6 7+8 9-10-11-12+13+14-15;1+2-3+4-5-6 7+8+9+10+11-12+13+14+15;1+2-3+4-5-6+7+8+9-10-11-12-13+14+15;1+2-3+4-5-6+7+8-9+10-11-12+13-14+15;1+2-3+4-5-6+7+8-9-10+11+12-13-14+15;1+2-3+4-5-6+7+8-9-10+11-12+13+14-15;1+2-3+4-5-6+7-8+9+10-11+12-13-14+15;1+2-3+4-5-6+7-8+9+10-11-12+13+14-15;1+2-3+4-5-6+7-8+9-10+11+12-13+14-15;1+2-3+4-5-6+7-8-9+10+11+12+13-14-15;1+2-3+4-5-6-7+8 9-10-11-12-13-14-15;1+2-3+4-5-6-7+8+9+10+11-12-13-14+15;1+2-3+4-5-6-7+8+9+10-11+12-13+14-15;1+2-3+4-5-6-7+8+9-10+11+12+13-14-15;1+2-3+4-5-6-7-8-9-10+11-12+13+14+15;1+2-3-4 5+6 7+8-9-10-11+12-13-14+15;1+2-3-4 5+6 7+8-9-10-11-12+13+14-15;1+2-3-4 5+6 7-8+9-10+11-12-13-14+15;1+2-3-4 5+6 7-8+9-10-11+12-13+14-15;1+2-3-4 5+6 7-8-9+10+11-12-13+14-15;1+2-3-4 5+6 7-8-9+10-11+12+13-14-15;1+2-3-4 5+6+7+8-9-10-11+12+13+14+15;1+2-3-4 5+6+7-8+9-10+11-12+13+14+15;1+2-3-4 5+6+7-8-9+10+11+12-13+14+15;1+2-3-4 5+6-7+8+9+10-11-12+13+14+15;1+2-3-4 5+6-7+8+9-10+11+12-13+14+15;1+2-3-4 5+6-7+8-9+10+11+12+13-14+15;1+2-3-4 5+6-7-8+9+10+11+12+13+14-15;1+2-3-4 5-6 7 8+9 10+11 12-13 14+15;1+2-3-4 5-6 7+8 9+10+11-12+13-14+15;1+2-3-4 5-6 7+8 9+10-11+12+13+14-15;1+2-3-4 5-6 7-8 9+10-11-12 13+14 15;1+2-3-4 5-6 7-8 9-10 11+12 13+14-15;1+2-3-4 5-6+7+8 9-10-11-12-13-14+15;1+2-3-4 5-6+7+8+9+10-11+12-13+14+15;1+2-3-4 5-6+7+8+9-10+11+12+13-14+15;1+2-3-4 5-6+7+8-9+10+11+12+13+14-15;1+2-3-4 5-6-7+8 9+10-11+12-13-14-15;1+2-3-4 5-6-7-8-9+10+11+12+13+14+15;1+2-3-4+5 6-7 8+9+10+11+12+13-14-15;1+2-3-4+5+6 7-8 9+10+11+12-13-14+15;1+2-3-4+5+6 7-8 9+10+11-12+13+14-15;1+2-3-4+5+6 7-8-9-10-11+12-13-14-15;1+2-3-4+5+6+7+8+9+10-11+12-13-14-15;1+2-3-4+5+6+7-8 9+10+11+12+13+14+15;1+2-3-4+5+6+7-8-9+10-11-12-13+14+15;1+2-3-4+5+6+7-8-9-10+11-12+13-14+15;1+2-3-4+5+6+7-8-9-10-11+12+13+14-15;1+2-3-4+5+6-7+8+9-10-11-12-13+14+15;1+2-3-4+5+6-7+8-9+10-11-12+13-14+15;1+2-3-4+5+6-7+8-9-10+11+12-13-14+15;1+2-3-4+5+6-7+8-9-10+11-12+13+14-15;1+2-3-4+5+6-7-8+9+10-11+12-13-14+15;1+2-3-4+5+6-7-8+9+10-11-12+13+14-15;1+2-3-4+5+6-7-8+9-10+11+12-13+14-15;1+2-3-4+5+6-7-8-9+10+11+12+13-14-15;1+2-3-4+5-6 7+8 9-10+11-12-13-14+15;1+2-3-4+5-6 7+8 9-10-11+12-13+14-15;1+2-3-4+5-6 7+8+9+10+11+12-13+14+15;1+2-3-4+5-6+7+8+9-10-11-12+13-14+15;1+2-3-4+5-6+7+8-9+10-11+12-13-14+15;1+2-3-4+5-6+7+8-9+10-11-12+13+14-15;1+2-3-4+5-6+7+8-9-10+11+12-13+14-15;1+2-3-4+5-6+7-8+9+10+11-12-13-14+15;1+2-3-4+5-6+7-8+9+10-11+12-13+14-15;1+2-3-4+5-6+7-8+9-10+11+12+13-14-15;1+2-3-4+5-6-7+8+9+10+11-12-13+14-15;1+2-3-4+5-6-7+8+9+10-11+12+13-14-15;1+2-3-4+5-6-7-8-9+10-11-12+13+14+15;1+2-3-4+5-6-7-8-9-10+11+12-13+14+15;1+2-3-4-5 6+7 8+9+10-11-12-13+14-15;1+2-3-4-5 6+7 8+9-10+11-12+13-14-15;1+2-3-4-5 6+7 8-9+10+11+12-13-14-15;1+2-3-4-5+6 7+8+9-10-11-12-13-14-15;1+2-3-4-5+6 7-8 9-10+11-12+13+14+15;1+2-3-4-5+6+7+8+9-10-11+12-13-14+15;1+2-3-4-5+6+7+8+9-10-11-12+13+14-15;1+2-3-4-5+6+7+8-9+10+11-12-13-14+15;1+2-3-4-5+6+7+8-9+10-11+12-13+14-15;1+2-3-4-5+6+7+8-9-10+11+12+13-14-15;1+2-3-4-5+6+7-8+9+10+11-12-13+14-15;1+2-3-4-5+6+7-8+9+10-11+12+13-14-15;1+2-3-4-5+6-7+8+9+10+11-12+13-14-15;1+2-3-4-5+6-7-8+9-10-11-12+13+14+15;1+2-3-4-5+6-7-8-9+10-11+12-13+14+15;1+2-3-4-5+6-7-8-9-10+11+12+13-14+15;1+2-3-4-5-6 7-8+9+10+11+12+13+14+15;1+2-3-4-5-6+7+8+9+10+11+12-13-14-15;1+2-3-4-5-6+7+8-9-10-11-12+13+14+15;1+2-3-4-5-6+7-8+9-10-11+12-13+14+15;1+2-3-4-5-6+7-8-9+10+11-12-13+14+15;1+2-3-4-5-6+7-8-9+10-11+12+13-14+15;1+2-3-4-5-6+7-8-9-10+11+12+13+14-15;1+2-3-4-5-6-7+8+9-10+11-12-13+14+15;1+2-3-4-5-6-7+8+9-10-11+12+13-14+15;1+2-3-4-5-6-7+8-9+10+11-12+13-14+15;1+2-3-4-5-6-7+8-9+10-11+12+13+14-15;1+2-3-4-5-6-7-8+9+10+11+12-13-14+15;1+2-3-4-5-6-7-8+9+10+11-12+13+14-15;1-2 3 4 5+6+7-8+9 10-11+12+13+14 15;1-2 3 4 5+6-7 8-9+10 11+12-13+14 15;1-2 3 4 5+6-7+8+9 10+11-12+13+14 15;1-2 3 4 5+6-7+8+9+10 11-12+13 14+15;1-2 3 4 5-6 7+8 9+10 11+12+13 14-15;1-2 3 4 5-6+7+8+9 10+11+12-13+14 15;1-2 3 4+5 6-7-8-9+10-11-12 13+14 15;1-2 3 4+5 6-7-8-9-10 11+12 13+14-15;1-2 3 4+5+6-7 8 9+10 11+12-13-14+15;1-2 3 4+5+6-7 8 9+10 11-12+13+14-15;1-2 3 4+5-6-7-8 9-10 11+12+13 14+15;1-2 3 4-5 6-7+8 9-10-11 12+13 14+15;1-2 3 4-5-6 7+8-9-10 11-12+13 14+15;1-2 3 4-5-6 7+8-9-10-11 12+13+14 15;1-2 3 4-5-6 7-8-9 10+11+12 13+14-15;1-2 3 4-5-6+7-8 9+10-11 12+13+14 15;1-2 3+4 5+6 7-8 9+10-11+12-13-14+15;1-2 3+4 5+6 7-8 9+10-11-12+13+14-15;1-2 3+4 5+6 7-8 9-10+11+12-13+14-15;1-2 3+4 5+6+7+8+9-10+11-12-13-14-15;1-2 3+4 5+6+7-8 9+10-11+12+13+14+15;1-2 3+4 5+6+7-8-9-10-11-12+13-14+15;1-2 3+4 5+6-7+8-9-10-11+12-13-14+15;1-2 3+4 5+6-7+8-9-10-11-12+13+14-15;1-2 3+4 5+6-7-8+9-10+11-12-13-14+15;1-2 3+4 5+6-7-8+9-10-11+12-13+14-15;1-2 3+4 5+6-7-8-9+10+11-12-13+14-15;1-2 3+4 5+6-7-8-9+10-11+12+13-14-15;1-2 3+4 5-6 7+8 9-10-11-12-13-14+15;1-2 3+4 5-6 7+8+9+10-11+12-13+14+15;1-2 3+4 5-6 7+8+9-10+11+12+13-14+15;1-2 3+4 5-6 7+8-9+10+11+12+13+14-15;1-2 3+4 5-6+7+8-9-10+11-12-13-14+15;1-2 3+4 5-6+7+8-9-10-11+12-13+14-15;1-2 3+4 5-6+7-8+9+10-11-12-13-14+15;1-2 3+4 5-6+7-8+9-10+11-12-13+14-15;1-2 3+4 5-6+7-8+9-10-11+12+13-14-15;1-2 3+4 5-6+7-8-9+10+11-12+13-14-15;1-2 3+4 5-6-7+8+9+10-11-12-13+14-15;1-2 3+4 5-6-7+8+9-10+11-12+13-14-15;1-2 3+4 5-6-7+8-9+10+11+12-13-14-15;1-2 3+4 5-6-7-8-9-10-11+12-13+14+15;1-2 3+4+5 6 7-8+9 10-11-12-13-14 15;1-2 3+4+5 6-7 8+9-10+11-12+13+14+15;1-2 3+4+5 6-7 8-9+10+11+12-13+14+15;1-2 3+4+5+6 7+8-9-10+11-12-13-14-15;1-2 3+4+5+6 7-8+9+10-11-12-13-14-15;1-2 3+4+5+6+7+8+9-10-11-12-13+14+15;1-2 3+4+5+6+7+8-9+10-11-12+13-14+15;1-2 3+4+5+6+7+8-9-10+11+12-13-14+15;1-2 3+4+5+6+7+8-9-10+11-12+13+14-15;1-2 3+4+5+6+7-8+9+10-11+12-13-14+15;1-2 3+4+5+6+7-8+9+10-11-12+13+14-15;1-2 3+4+5+6+7-8+9-10+11+12-13+14-15;1-2 3+4+5+6+7-8-9+10+11+12+13-14-15;1-2 3+4+5+6-7+8 9-10-11-12-13-14-15;1-2 3+4+5+6-7+8+9+10+11-12-13-14+15;1-2 3+4+5+6-7+8+9+10-11+12-13+14-15;1-2 3+4+5+6-7+8+9-10+11+12+13-14-15;1-2 3+4+5+6-7-8-9-10+11-12+13+14+15;1-2 3+4+5-6 7+8 9+10+11+12-13-14-15;1-2 3+4+5-6+7+8+9+10+11-12-13+14-15;1-2 3+4+5-6+7+8+9+10-11+12+13-14-15;1-2 3+4+5-6+7-8-9+10-11-12+13+14+15;1-2 3+4+5-6+7-8-9-10+11+12-13+14+15;1-2 3+4+5-6-7+8+9-10-11-12+13+14+15;1-2 3+4+5-6-7+8-9+10-11+12-13+14+15;1-2 3+4+5-6-7+8-9-10+11+12+13-14+15;1-2 3+4+5-6-7-8+9+10+11-12-13+14+15;1-2 3+4+5-6-7-8+9+10-11+12+13-14+15;1-2 3+4+5-6-7-8+9-10+11+12+13+14-15;1-2 3+4-5 6+7 8-9-10+11-12-13+14+15;1-2 3+4-5 6+7 8-9-10-11+12+13-14+15;1-2 3+4-5+6 7-8 9+10+11+12+13+14-15;1-2 3+4-5+6 7-8+9-10-11-12-13-14+15;1-2 3+4-5+6 7-8-9+10-11-12-13+14-15;1-2 3+4-5+6 7-8-9-10+11-12+13-14-15;1-2 3+4-5+6+7+8+9+10+11-12+13-14-15;1-2 3+4-5+6+7-8+9-10-11-12+13+14+15;1-2 3+4-5+6+7-8-9+10-11+12-13+14+15;1-2 3+4-5+6+7-8-9-10+11+12+13-14+15;1-2 3+4-5+6-7+8+9-10-11+12-13+14+15;1-2 3+4-5+6-7+8-9+10+11-12-13+14+15;1-2 3+4-5+6-7+8-9+10-11+12+13-14+15;1-2 3+4-5+6-7+8-9-10+11+12+13+14-15;1-2 3+4-5+6-7-8+9+10+11-12+13-14+15;1-2 3+4-5+6-7-8+9+10-11+12+13+14-15;1-2 3+4-5-6 7+8 9+10-11-12+13-14+15;1-2 3+4-5-6 7+8 9-10+11+12-13-14+15;1-2 3+4-5-6 7+8 9-10+11-12+13+14-15;1-2 3+4-5-6+7+8+9-10+11-12-13+14+15;1-2 3+4-5-6+7+8+9-10-11+12+13-14+15;1-2 3+4-5-6+7+8-9+10+11-12+13-14+15;1-2 3+4-5-6+7+8-9+10-11+12+13+14-15;1-2 3+4-5-6+7-8+9+10+11+12-13-14+15;1-2 3+4-5-6+7-8+9+10+11-12+13+14-15;1-2 3+4-5-6-7+8 9-10+11-12-13-14-15;1-2 3+4-5-6-7+8+9+10+11+12-13+14-15;1-2 3+4-5-6-7-8-9+10-11+12+13+14+15;1-2 3-4 5 6-7-8-9 10-11+12-13+14 15;1-2 3-4 5+6 7+8+9-10-11-12-13+14+15;1-2 3-4 5+6 7+8-9+10-11-12+13-14+15;1-2 3-4 5+6 7+8-9-10+11+12-13-14+15;1-2 3-4 5+6 7+8-9-10+11-12+13+14-15;1-2 3-4 5+6 7-8+9+10-11+12-13-14+15;1-2 3-4 5+6 7-8+9+10-11-12+13+14-15;1-2 3-4 5+6 7-8+9-10+11+12-13+14-15;1-2 3-4 5+6 7-8-9+10+11+12+13-14-15;1-2 3-4 5+6+7+8-9-10+11+12+13+14+15;1-2 3-4 5+6+7-8+9+10-11+12+13+14+15;1-2 3-4 5+6-7+8 9-10-11+12-13-14+15;1-2 3-4 5+6-7+8 9-10-11-12+13+14-15;1-2 3-4 5+6-7+8+9+10+11-12+13+14+15;1-2 3-4 5-6 7+8 9+10+11+12+13+14-15;1-2 3-4 5-6 7-8 9+10+11-12 13+14 15;1-2 3-4 5-6+7+8 9-10+11-12-13-14+15;1-2 3-4 5-6+7+8 9-10-11+12-13+14-15;1-2 3-4 5-6+7+8+9+10+11+12-13+14+15;1-2 3-4 5-6-7+8 9+10+11+12-13-14-15;1-2 3-4+5+6 7+8-9-10-11-12-13-14+15;1-2 3-4+5+6 7-8+9-10-11-12-13+14-15;1-2 3-4+5+6 7-8-9+10-11-12+13-14-15;1-2 3-4+5+6 7-8-9-10+11+12-13-14-15;1-2 3-4+5+6+7+8+9+10+11+12-13-14-15;1-2 3-4+5+6+7+8-9-10-11-12+13+14+15;1-2 3-4+5+6+7-8+9-10-11+12-13+14+15;1-2 3-4+5+6+7-8-9+10+11-12-13+14+15;1-2 3-4+5+6+7-8-9+10-11+12+13-14+15;1-2 3-4+5+6+7-8-9-10+11+12+13+14-15;1-2 3-4+5+6-7+8+9-10+11-12-13+14+15;1-2 3-4+5+6-7+8+9-10-11+12+13-14+15;1-2 3-4+5+6-7+8-9+10+11-12+13-14+15;1-2 3-4+5+6-7+8-9+10-11+12+13+14-15;1-2 3-4+5+6-7-8+9+10+11+12-13-14+15;1-2 3-4+5+6-7-8+9+10+11-12+13+14-15;1-2 3-4+5-6 7+8 9+10-11+12-13-14+15;1-2 3-4+5-6 7+8 9+10-11-12+13+14-15;1-2 3-4+5-6 7+8 9-10+11+12-13+14-15;1-2 3-4+5-6 7-8 9-10-11 12+13 14-15;1-2 3-4+5-6+7+8+9+10-11-12-13+14+15;1-2 3-4+5-6+7+8+9-10+11-12+13-14+15;1-2 3-4+5-6+7+8+9-10-11+12+13+14-15;1-2 3-4+5-6+7+8-9+10+11+12-13-14+15;1-2 3-4+5-6+7+8-9+10+11-12+13+14-15;1-2 3-4+5-6+7-8+9+10+11+12-13+14-15;1-2 3-4+5-6-7+8 9+10-11-12-13-14-15;1-2 3-4+5-6-7+8+9+10+11+12+13-14-15;1-2 3-4+5-6-7-8+9-10-11+12+13+14+15;1-2 3-4+5-6-7-8-9+10+11-12+13+14+15;1-2 3-4-5 6+7 8+9+10+11-12-13+14-15;1-2 3-4-5 6+7 8+9+10-11+12+13-14-15;1-2 3-4-5 6-7 8-9 10+11 12-13-14-15;1-2 3-4-5+6 7+8+9-10+11-12-13-14-15;1-2 3-4-5+6 7-8 9+10-11+12+13+14+15;1-2 3-4-5+6 7-8-9-10-11-12+13-14+15;1-2 3-4-5+6+7+8+9+10-11-12+13-14+15;1-2 3-4-5+6+7+8+9-10+11+12-13-14+15;1-2 3-4-5+6+7+8+9-10+11-12+13+14-15;1-2 3-4-5+6+7+8-9+10+11+12-13+14-15;1-2 3-4-5+6+7-8+9+10+11+12+13-14-15;1-2 3-4-5+6-7+8-9-10-11+12+13+14+15;1-2 3-4-5+6-7-8+9-10+11-12+13+14+15;1-2 3-4-5+6-7-8-9+10+11+12-13+14+15;1-2 3-4-5-6 7+8 9-10-11-12+13+14+15;1-2 3-4-5-6+7+8-9-10+11-12+13+14+15;1-2 3-4-5-6+7-8+9+10-11-12+13+14+15;1-2 3-4-5-6+7-8+9-10+11+12-13+14+15;1-2 3-4-5-6+7-8-9+10+11+12+13-14+15;1-2 3-4-5-6-7+8 9-10-11-12-13-14+15;1-2 3-4-5-6-7+8+9+10-11+12-13+14+15;1-2 3-4-5-6-7+8+9-10+11+12+13-14+15;1-2 3-4-5-6-7+8-9+10+11+12+13+14-15;1-2+3 4 5+6 7 8-9-10 11+12-13+14-15;1-2+3 4 5+6-7-8-9-10+11 12-13-14 15;1-2+3 4 5-6 7+8+9 10-11-12 13+14+15;1-2+3 4 5-6 7+8-9+10 11+12-13 14+15;1-2+3 4 5-6-7+8-9+10 11-12-13 14-15;1-2+3 4+5 6+7-8 9+10+11-12+13-14-15;1-2+3 4+5 6-7-8 9-10-11+12-13+14+15;1-2+3 4+5+6-7 8+9+10+11-12-13+14+15;1-2+3 4+5+6-7 8+9+10-11+12+13-14+15;1-2+3 4+5+6-7 8+9-10+11+12+13+14-15;1-2+3 4+5-6-7 8-9-10+11+12+13+14+15;1-2+3 4-5 6+7+8+9+10-11+12-13-14+15;1-2+3 4-5 6+7+8+9+10-11-12+13+14-15;1-2+3 4-5 6+7+8+9-10+11+12-13+14-15;1-2+3 4-5 6+7+8-9+10+11+12+13-14-15;1-2+3 4-5 6+7-8-9-10-11+12+13+14+15;1-2+3 4-5 6-7+8-9-10+11-12+13+14+15;1-2+3 4-5 6-7-8+9+10-11-12+13+14+15;1-2+3 4-5 6-7-8+9-10+11+12-13+14+15;1-2+3 4-5 6-7-8-9+10+11+12+13-14+15;1-2+3 4-5+6-7 8-9+10-11+12+13+14+15;1-2+3 4-5-6-7 8+9+10+11+12+13-14+15;1-2+3+4 5+6 7+8 9+10 11-12 13+14-15;1-2+3+4 5+6 7+8 9+10-11+12 13-14 15;1-2+3+4 5+6 7-8 9+10-11-12-13-14+15;1-2+3+4 5+6 7-8 9-10+11-12-13+14-15;1-2+3+4 5+6 7-8 9-10-11+12+13-14-15;1-2+3+4 5+6+7-8 9+10-11-12+13+14+15;1-2+3+4 5+6+7-8 9-10+11+12-13+14+15;1-2+3+4 5+6-7+8-9-10-11-12-13-14+15;1-2+3+4 5+6-7-8+9-10-11-12-13+14-15;1-2+3+4 5+6-7-8-9+10-11-12+13-14-15;1-2+3+4 5+6-7-8-9-10+11+12-13-14-15;1-2+3+4 5-6 7+8+9+10-11-12-13+14+15;1-2+3+4 5-6 7+8+9-10+11-12+13-14+15;1-2+3+4 5-6 7+8+9-10-11+12+13+14-15;1-2+3+4 5-6 7+8-9+10+11+12-13-14+15;1-2+3+4 5-6 7+8-9+10+11-12+13+14-15;1-2+3+4 5-6 7-8+9+10+11+12-13+14-15;1-2+3+4 5-6+7+8-9-10-11-12-13+14-15;1-2+3+4 5-6+7-8+9-10-11-12+13-14-15;1-2+3+4 5-6+7-8-9+10-11+12-13-14-15;1-2+3+4 5-6-7+8+9-10-11+12-13-14-15;1-2+3+4 5-6-7+8-9+10+11-12-13-14-15;1-2+3+4 5-6-7-8 9-10+11+12+13+14+15;1-2+3+4 5-6-7-8-9-10-11-12-13+14+15;1-2+3+4+5 6-7 8+9-10-11+12-13+14+15;1-2+3+4+5 6-7 8-9+10+11-12-13+14+15;1-2+3+4+5 6-7 8-9+10-11+12+13-14+15;1-2+3+4+5 6-7 8-9-10+11+12+13+14-15;1-2+3+4+5+6+7+8-9-10+11-12-13-14+15;1-2+3+4+5+6+7+8-9-10-11+12-13+14-15;1-2+3+4+5+6+7-8+9+10-11-12-13-14+15;1-2+3+4+5+6+7-8+9-10+11-12-13+14-15;1-2+3+4+5+6+7-8+9-10-11+12+13-14-15;1-2+3+4+5+6+7-8-9+10+11-12+13-14-15;1-2+3+4+5+6-7+8+9+10-11-12-13+14-15;1-2+3+4+5+6-7+8+9-10+11-12+13-14-15;1-2+3+4+5+6-7+8-9+10+11+12-13-14-15;1-2+3+4+5+6-7-8-9-10-11+12-13+14+15;1-2+3+4+5-6 7+8 9+10+11-12-13-14-15;1-2+3+4+5-6 7-8+9-10+11+12+13+14+15;1-2+3+4+5-6+7+8+9+10-11-12+13-14-15;1-2+3+4+5-6+7+8+9-10+11+12-13-14-15;1-2+3+4+5-6+7-8-9-10+11-12-13+14+15;1-2+3+4+5-6+7-8-9-10-11+12+13-14+15;1-2+3+4+5-6-7+8-9+10-11-12-13+14+15;1-2+3+4+5-6-7+8-9-10+11-12+13-14+15;1-2+3+4+5-6-7+8-9-10-11+12+13+14-15;1-2+3+4+5-6-7-8+9+10-11-12+13-14+15;1-2+3+4+5-6-7-8+9-10+11+12-13-14+15;1-2+3+4+5-6-7-8+9-10+11-12+13+14-15;1-2+3+4+5-6-7-8-9+10+11+12-13+14-15;1-2+3+4-5 6+7 8-9-10-11-12+13-14+15;1-2+3+4-5+6 7-8 9+10+11+12-13-14+15;1-2+3+4-5+6 7-8 9+10+11-12+13+14-15;1-2+3+4-5+6 7-8-9-10-11+12-13-14-15;1-2+3+4-5+6+7+8+9+10-11+12-13-14-15;1-2+3+4-5+6+7-8 9+10+11+12+13+14+15;1-2+3+4-5+6+7-8-9+10-11-12-13+14+15;1-2+3+4-5+6+7-8-9-10+11-12+13-14+15;1-2+3+4-5+6+7-8-9-10-11+12+13+14-15;1-2+3+4-5+6-7+8+9-10-11-12-13+14+15;1-2+3+4-5+6-7+8-9+10-11-12+13-14+15;1-2+3+4-5+6-7+8-9-10+11+12-13-14+15;1-2+3+4-5+6-7+8-9-10+11-12+13+14-15;1-2+3+4-5+6-7-8+9+10-11+12-13-14+15;1-2+3+4-5+6-7-8+9+10-11-12+13+14-15;1-2+3+4-5+6-7-8+9-10+11+12-13+14-15;1-2+3+4-5+6-7-8-9+10+11+12+13-14-15;1-2+3+4-5-6 7+8 9-10+11-12-13-14+15;1-2+3+4-5-6 7+8 9-10-11+12-13+14-15;1-2+3+4-5-6 7+8+9+10+11+12-13+14+15;1-2+3+4-5-6+7+8+9-10-11-12+13-14+15;1-2+3+4-5-6+7+8-9+10-11+12-13-14+15;1-2+3+4-5-6+7+8-9+10-11-12+13+14-15;1-2+3+4-5-6+7+8-9-10+11+12-13+14-15;1-2+3+4-5-6+7-8+9+10+11-12-13-14+15;1-2+3+4-5-6+7-8+9+10-11+12-13+14-15;1-2+3+4-5-6+7-8+9-10+11+12+13-14-15;1-2+3+4-5-6-7+8+9+10+11-12-13+14-15;1-2+3+4-5-6-7+8+9+10-11+12+13-14-15;1-2+3+4-5-6-7-8-9+10-11-12+13+14+15;1-2+3+4-5-6-7-8-9-10+11+12-13+14+15;1-2+3-4 5 6-7 8 9-10+11+12 13+14+15;1-2+3-4 5 6-7-8-9 10-11-12-13+14 15;1-2+3-4 5+6 7+8-9-10+11-12-13-14+15;1-2+3-4 5+6 7+8-9-10-11+12-13+14-15;1-2+3-4 5+6 7-8+9+10-11-12-13-14+15;1-2+3-4 5+6 7-8+9-10+11-12-13+14-15;1-2+3-4 5+6 7-8+9-10-11+12+13-14-15;1-2+3-4 5+6 7-8-9+10+11-12+13-14-15;1-2+3-4 5+6+7+8-9-10+11-12+13+14+15;1-2+3-4 5+6+7-8+9+10-11-12+13+14+15;1-2+3-4 5+6+7-8+9-10+11+12-13+14+15;1-2+3-4 5+6+7-8-9+10+11+12+13-14+15;1-2+3-4 5+6-7+8 9-10-11-12-13-14+15;1-2+3-4 5+6-7+8+9+10-11+12-13+14+15;1-2+3-4 5+6-7+8+9-10+11+12+13-14+15;1-2+3-4 5+6-7+8-9+10+11+12+13+14-15;1-2+3-4 5-6 7+8 9+10+11+12-13-14+15;1-2+3-4 5-6 7+8 9+10+11-12+13+14-15;1-2+3-4 5-6+7+8 9-10-11-12-13+14-15;1-2+3-4 5-6+7+8+9+10+11-12-13+14+15;1-2+3-4 5-6+7+8+9+10-11+12+13-14+15;1-2+3-4 5-6+7+8+9-10+11+12+13+14-15;1-2+3-4 5-6-7+8 9+10+11-12-13-14-15;1-2+3-4 5-6-7-8+9-10+11+12+13+14+15;1-2+3-4+5 6-7 8-9-10-11+12+13+14+15;1-2+3-4+5+6 7-8 9+10+11+12-13+14-15;1-2+3-4+5+6 7-8-9-10+11-12-13-14-15;1-2+3-4+5+6+7+8+9+10+11-12-13-14-15;1-2+3-4+5+6+7-8+9-10-11-12-13+14+15;1-2+3-4+5+6+7-8-9+10-11-12+13-14+15;1-2+3-4+5+6+7-8-9-10+11+12-13-14+15;1-2+3-4+5+6+7-8-9-10+11-12+13+14-15;1-2+3-4+5+6-7+8+9-10-11-12+13-14+15;1-2+3-4+5+6-7+8-9+10-11+12-13-14+15;1-2+3-4+5+6-7+8-9+10-11-12+13+14-15;1-2+3-4+5+6-7+8-9-10+11+12-13+14-15;1-2+3-4+5+6-7-8+9+10+11-12-13-14+15;1-2+3-4+5+6-7-8+9+10-11+12-13+14-15;1-2+3-4+5+6-7-8+9-10+11+12+13-14-15;1-2+3-4+5-6 7+8 9+10-11-12-13-14+15;1-2+3-4+5-6 7+8 9-10+11-12-13+14-15;1-2+3-4+5-6 7+8 9-10-11+12+13-14-15;1-2+3-4+5-6 7+8+9+10+11+12+13-14+15;1-2+3-4+5-6+7+8+9-10-11+12-13-14+15;1-2+3-4+5-6+7+8+9-10-11-12+13+14-15;1-2+3-4+5-6+7+8-9+10+11-12-13-14+15;1-2+3-4+5-6+7+8-9+10-11+12-13+14-15;1-2+3-4+5-6+7+8-9-10+11+12+13-14-15;1-2+3-4+5-6+7-8+9+10+11-12-13+14-15;1-2+3-4+5-6+7-8+9+10-11+12+13-14-15;1-2+3-4+5-6-7+8+9+10+11-12+13-14-15;1-2+3-4+5-6-7-8+9-10-11-12+13+14+15;1-2+3-4+5-6-7-8-9+10-11+12-13+14+15;1-2+3-4+5-6-7-8-9-10+11+12+13-14+15;1-2+3-4-5 6+7 8+9+10-11-12+13-14-15;1-2+3-4-5 6+7 8+9-10+11+12-13-14-15;1-2+3-4-5+6 7-8 9+10-11-12+13+14+15;1-2+3-4-5+6 7-8 9-10+11+12-13+14+15;1-2+3-4-5+6+7+8+9-10+11-12-13-14+15;1-2+3-4-5+6+7+8+9-10-11+12-13+14-15;1-2+3-4-5+6+7+8-9+10+11-12-13+14-15;1-2+3-4-5+6+7+8-9+10-11+12+13-14-15;1-2+3-4-5+6+7-8+9+10+11-12+13-14-15;1-2+3-4-5+6-7+8+9+10+11+12-13-14-15;1-2+3-4-5+6-7+8-9-10-11-12+13+14+15;1-2+3-4-5+6-7-8+9-10-11+12-13+14+15;1-2+3-4-5+6-7-8-9+10+11-12-13+14+15;1-2+3-4-5+6-7-8-9+10-11+12+13-14+15;1-2+3-4-5+6-7-8-9-10+11+12+13+14-15;1-2+3-4-5-6 7+8-9+10+11+12+13+14+15;1-2+3-4-5-6+7+8-9-10-11+12-13+14+15;1-2+3-4-5-6+7-8+9-10+11-12-13+14+15;1-2+3-4-5-6+7-8+9-10-11+12+13-14+15;1-2+3-4-5-6+7-8-9+10+11-12+13-14+15;1-2+3-4-5-6+7-8-9+10-11+12+13+14-15;1-2+3-4-5-6-7+8+9+10-11-12-13+14+15;1-2+3-4-5-6-7+8+9-10+11-12+13-14+15;1-2+3-4-5-6-7+8+9-10-11+12+13+14-15;1-2+3-4-5-6-7+8-9+10+11+12-13-14+15;1-2+3-4-5-6-7+8-9+10+11-12+13+14-15;1-2+3-4-5-6-7-8+9+10+11+12-13+14-15;1-2-3 4 5+6 7+8-9-10-11 12-13+14 15;1-2-3 4 5+6-7 8-9 10+11-12+13 14+15;1-2-3 4 5+6-7+8+9-10 11+12+13 14+15;1-2-3 4 5-6 7 8+9+10 11-12-13+14+15;1-2-3 4+5 6+7+8+9-10-11-12-13-14+15;1-2-3 4+5 6+7+8-9+10-11-12-13+14-15;1-2-3 4+5 6+7+8-9-10+11-12+13-14-15;1-2-3 4+5 6+7-8+9+10-11-12+13-14-15;1-2-3 4+5 6+7-8+9-10+11+12-13-14-15;1-2-3 4+5 6-7+8+9+10-11+12-13-14-15;1-2-3 4+5 6-7-8 9+10+11+12+13+14+15;1-2-3 4+5 6-7-8-9+10-11-12-13+14+15;1-2-3 4+5 6-7-8-9-10+11-12+13-14+15;1-2-3 4+5 6-7-8-9-10-11+12+13+14-15;1-2-3 4+5+6+7 8-9-10-11-12-13-14+15;1-2-3 4+5-6+7 8+9-10-11+12-13-14-15;1-2-3 4+5-6+7 8-9+10+11-12-13-14-15;1-2-3 4-5 6 7+8+9 10+11 12-13-14 15;1-2-3 4-5 6+7+8 9+10+11-12-13+14-15;1-2-3 4-5 6+7+8 9+10-11+12+13-14-15;1-2-3 4-5 6+7-8 9-10 11+12 13-14-15;1-2-3 4-5 6-7+8 9-10-11-12+13+14+15;1-2-3 4-5+6+7 8+9-10+11-12-13-14-15;1-2-3 4-5-6+7 8-9-10+11-12-13-14+15;1-2-3 4-5-6+7 8-9-10-11+12-13+14-15;1-2-3+4 5 6+7 8 9-10+11-12 13-14-15;1-2-3+4 5+6 7 8-9 10-11-12 13+14 15;1-2-3+4 5+6 7+8 9-10+11 12-13 14+15;1-2-3+4 5+6 7-8 9-10-11-12+13-14+15;1-2-3+4 5+6+7+8-9-10+11-12-13-14-15;1-2-3+4 5+6+7-8+9+10-11-12-13-14-15;1-2-3+4 5+6-7-8 9+10+11+12-13+14+15;1-2-3+4 5+6-7-8-9-10+11-12-13-14+15;1-2-3+4 5+6-7-8-9-10-11+12-13+14-15;1-2-3+4 5-6 7+8+9-10-11-12+13+14+15;1-2-3+4 5-6 7+8-9+10-11+12-13+14+15;1-2-3+4 5-6 7+8-9-10+11+12+13-14+15;1-2-3+4 5-6 7-8+9+10+11-12-13+14+15;1-2-3+4 5-6 7-8+9+10-11+12+13-14+15;1-2-3+4 5-6 7-8+9-10+11+12+13+14-15;1-2-3+4 5-6+7-8 9+10+11+12+13-14+15;1-2-3+4 5-6+7-8-9+10-11-12-13-14+15;1-2-3+4 5-6+7-8-9-10+11-12-13+14-15;1-2-3+4 5-6+7-8-9-10-11+12+13-14-15;1-2-3+4 5-6-7+8+9-10-11-12-13-14+15;1-2-3+4 5-6-7+8-9+10-11-12-13+14-15;1-2-3+4 5-6-7+8-9-10+11-12+13-14-15;1-2-3+4 5-6-7-8+9+10-11-12+13-14-15;1-2-3+4 5-6-7-8+9-10+11+12-13-14-15;1-2-3+4+5 6-7 8-9-10+11-12+13+14+15;1-2-3+4+5+6 7-8 9+10+11+12+13-14-15;1-2-3+4+5+6 7-8-9+10-11-12-13-14-15;1-2-3+4+5+6+7+8-9-10-11-12-13+14+15;1-2-3+4+5+6+7-8+9-10-11-12+13-14+15;1-2-3+4+5+6+7-8-9+10-11+12-13-14+15;1-2-3+4+5+6+7-8-9+10-11-12+13+14-15;1-2-3+4+5+6+7-8-9-10+11+12-13+14-15;1-2-3+4+5+6-7+8+9-10-11+12-13-14+15;1-2-3+4+5+6-7+8+9-10-11-12+13+14-15;1-2-3+4+5+6-7+8-9+10+11-12-13-14+15;1-2-3+4+5+6-7+8-9+10-11+12-13+14-15;1-2-3+4+5+6-7+8-9-10+11+12+13-14-15;1-2-3+4+5+6-7-8+9+10+11-12-13+14-15;1-2-3+4+5+6-7-8+9+10-11+12+13-14-15;1-2-3+4+5-6 7+8 9+10-11-12-13+14-15;1-2-3+4+5-6 7+8 9-10+11-12+13-14-15;1-2-3+4+5-6 7+8+9+10+11+12+13+14-15;1-2-3+4+5-6+7+8+9-10+11-12-13-14+15;1-2-3+4+5-6+7+8+9-10-11+12-13+14-15;1-2-3+4+5-6+7+8-9+10+11-12-13+14-15;1-2-3+4+5-6+7+8-9+10-11+12+13-14-15;1-2-3+4+5-6+7-8+9+10+11-12+13-14-15;1-2-3+4+5-6-7+8+9+10+11+12-13-14-15;1-2-3+4+5-6-7+8-9-10-11-12+13+14+15;1-2-3+4+5-6-7-8+9-10-11+12-13+14+15;1-2-3+4+5-6-7-8-9+10+11-12-13+14+15;1-2-3+4+5-6-7-8-9+10-11+12+13-14+15;1-2-3+4+5-6-7-8-9-10+11+12+13+14-15;1-2-3+4-5 6+7 8+9+10-11+12-13-14-15;1-2-3+4-5+6 7-8 9+10-11+12-13+14+15;1-2-3+4-5+6 7-8 9-10+11+12+13-14+15;1-2-3+4-5+6 7-8-9-10-11-12-13-14+15;1-2-3+4-5+6+7+8+9+10-11-12-13-14+15;1-2-3+4-5+6+7+8+9-10+11-12-13+14-15;1-2-3+4-5+6+7+8+9-10-11+12+13-14-15;1-2-3+4-5+6+7+8-9+10+11-12+13-14-15;1-2-3+4-5+6+7-8+9+10+11+12-13-14-15;1-2-3+4-5+6+7-8-9-10-11-12+13+14+15;1-2-3+4-5+6-7+8-9-10-11+12-13+14+15;1-2-3+4-5+6-7-8+9-10+11-12-13+14+15;1-2-3+4-5+6-7-8+9-10-11+12+13-14+15;1-2-3+4-5+6-7-8-9+10+11-12+13-14+15;1-2-3+4-5+6-7-8-9+10-11+12+13+14-15;1-2-3+4-5-6 7+8 9-10-11-12-13+14+15;1-2-3+4-5-6 7+8+9-10+11+12+13+14+15;1-2-3+4-5-6+7+8-9-10+11-12-13+14+15;1-2-3+4-5-6+7+8-9-10-11+12+13-14+15;1-2-3+4-5-6+7-8+9+10-11-12-13+14+15;1-2-3+4-5-6+7-8+9-10+11-12+13-14+15;1-2-3+4-5-6+7-8+9-10-11+12+13+14-15;1-2-3+4-5-6+7-8-9+10+11+12-13-14+15;1-2-3+4-5-6+7-8-9+10+11-12+13+14-15;1-2-3+4-5-6-7+8+9+10-11-12+13-14+15;1-2-3+4-5-6-7+8+9-10+11+12-13-14+15;1-2-3+4-5-6-7+8+9-10+11-12+13+14-15;1-2-3+4-5-6-7+8-9+10+11+12-13+14-15;1-2-3+4-5-6-7-8+9+10+11+12+13-14-15;1-2-3-4 5+6 7+8-9-10-11-12-13+14+15;1-2-3-4 5+6 7-8+9-10-11-12+13-14+15;1-2-3-4 5+6 7-8-9+10-11+12-13-14+15;1-2-3-4 5+6 7-8-9+10-11-12+13+14-15;1-2-3-4 5+6 7-8-9-10+11+12-13+14-15;1-2-3-4 5+6+7+8 9-10+11-12-13-14-15;1-2-3-4 5+6+7+8+9+10+11+12-13+14-15;1-2-3-4 5+6+7-8-9+10-11+12+13+14+15;1-2-3-4 5+6-7+8+9-10-11+12+13+14+15;1-2-3-4 5+6-7+8-9+10+11-12+13+14+15;1-2-3-4 5+6-7-8+9+10+11+12-13+14+15;1-2-3-4 5-6 7+8 9+10-11+12-13+14+15;1-2-3-4 5-6 7+8 9-10+11+12+13-14+15;1-2-3-4 5-6+7+8+9-10+11-12+13+14+15;1-2-3-4 5-6+7+8-9+10+11+12-13+14+15;1-2-3-4 5-6+7-8+9+10+11+12+13-14+15;1-2-3-4 5-6-7+8 9+10-11-12-13+14-15;1-2-3-4 5-6-7+8 9-10+11-12+13-14-15;1-2-3-4 5-6-7+8+9+10+11+12+13+14-15;1-2-3-4+5 6-7 8+9+10+11+12-13-14+15;1-2-3-4+5 6-7 8+9+10+11-12+13+14-15;1-2-3-4+5+6 7-8 9+10+11-12-13+14+15;1-2-3-4+5+6 7-8 9+10-11+12+13-14+15;1-2-3-4+5+6 7-8 9-10+11+12+13+14-15;1-2-3-4+5+6 7-8-9-10-11-12-13+14-15;1-2-3-4+5+6+7+8+9+10-11-12-13+14-15;1-2-3-4+5+6+7+8+9-10+11-12+13-14-15;1-2-3-4+5+6+7+8-9+10+11+12-13-14-15;1-2-3-4+5+6+7-8-9-10-11+12-13+14+15;1-2-3-4+5+6-7+8-9-10+11-12-13+14+15;1-2-3-4+5+6-7+8-9-10-11+12+13-14+15;1-2-3-4+5+6-7-8+9+10-11-12-13+14+15;1-2-3-4+5+6-7-8+9-10+11-12+13-14+15;1-2-3-4+5+6-7-8+9-10-11+12+13+14-15;1-2-3-4+5+6-7-8-9+10+11+12-13-14+15;1-2-3-4+5+6-7-8-9+10+11-12+13+14-15;1-2-3-4+5-6 7+8 9-10-11-12+13-14+15;1-2-3-4+5-6 7+8+9+10-11+12+13+14+15;1-2-3-4+5-6+7+8-9+10-11-12-13+14+15;1-2-3-4+5-6+7+8-9-10+11-12+13-14+15;1-2-3-4+5-6+7+8-9-10-11+12+13+14-15;1-2-3-4+5-6+7-8+9+10-11-12+13-14+15;1-2-3-4+5-6+7-8+9-10+11+12-13-14+15;1-2-3-4+5-6+7-8+9-10+11-12+13+14-15;1-2-3-4+5-6+7-8-9+10+11+12-13+14-15;1-2-3-4+5-6-7+8+9+10-11+12-13-14+15;1-2-3-4+5-6-7+8+9+10-11-12+13+14-15;1-2-3-4+5-6-7+8+9-10+11+12-13+14-15;1-2-3-4+5-6-7+8-9+10+11+12+13-14-15;1-2-3-4+5-6-7-8-9-10-11+12+13+14+15;1-2-3-4-5 6+7 8+9-10+11-12-13-14+15;1-2-3-4-5 6+7 8+9-10-11+12-13+14-15;1-2-3-4-5 6+7 8-9+10+11-12-13+14-15;1-2-3-4-5 6+7 8-9+10-11+12+13-14-15;1-2-3-4-5+6 7+8-9-10+11-12-13-14-15;1-2-3-4-5+6 7-8+9+10-11-12-13-14-15;1-2-3-4-5+6+7+8+9-10-11-12-13+14+15;1-2-3-4-5+6+7+8-9+10-11-12+13-14+15;1-2-3-4-5+6+7+8-9-10+11+12-13-14+15;1-2-3-4-5+6+7+8-9-10+11-12+13+14-15;1-2-3-4-5+6+7-8+9+10-11+12-13-14+15;1-2-3-4-5+6+7-8+9+10-11-12+13+14-15;1-2-3-4-5+6+7-8+9-10+11+12-13+14-15;1-2-3-4-5+6+7-8-9+10+11+12+13-14-15;1-2-3-4-5+6-7+8 9-10-11-12-13-14-15;1-2-3-4-5+6-7+8+9+10+11-12-13-14+15;1-2-3-4-5+6-7+8+9+10-11+12-13+14-15;1-2-3-4-5+6-7+8+9-10+11+12+13-14-15;1-2-3-4-5+6-7-8-9-10+11-12+13+14+15;1-2-3-4-5-6 7+8 9+10+11+12-13-14-15;1-2-3-4-5-6+7+8+9+10+11-12-13+14-15;1-2-3-4-5-6+7+8+9+10-11+12+13-14-15;1-2-3-4-5-6+7-8-9+10-11-12+13+14+15;1-2-3-4-5-6+7-8-9-10+11+12-13+14+15;1-2-3-4-5-6-7+8+9-10-11-12+13+14+15;1-2-3-4-5-6-7+8-9+10-11+12-13+14+15;1-2-3-4-5-6-7+8-9-10+11+12+13-14+15;1-2-3-4-5-6-7-8+9+10+11-12-13+14+15;1-2-3-4-5-6-7-8+9+10-11+12+13-14+15;1-2-3-4-5-6-7-8+9-10+11+12+13+14-15",
                c);
        System.out.println("All time:" + totalTime);
    }

    public static void testAll(int c) {
        test("R23,57,89,123,456,654,674,783,894,1023", "23-57+89+123+456-654+674-783-894+1023", c);

//        test("R213,572,893,1234,4565,6546,6747,7838,8941,10231", null, c);

        testIllegalCases(c);
        testAllS();
        testStandard(c);

    }

    public static void testIllegalCases(int c) {
        //illegal
        test("R12,45,13", "ILLEGAL", c);
        test("12,45,13", "ILLEGAL", c);
        test("S12,45,13", "ILLEGAL", c);
        test("S1 2 ", "ILLEGAL", c);
        test("R", "ILLEGAL", c);
        test("S", "ILLEGAL", c);
        test("   ", "ILLEGAL", c);
        test("R,", "ILLEGAL", c);
        test("S,", "ILLEGAL", c);
    }

    /**
     * @param args
     */
    public static void main(String [] args) {
        int c = 1;
        double totalTime = 0;
//        testAll(c);
//        System.out.println(Long.MAX_VALUE);
        System.out.println(Integer.MAX_VALUE);
        //        totalTime += test(
        //                "S11",
        //                "1 2 3-4-5-6-7 8-9-10-11;1 2+3 4+5-6 7+8+9+10-11;1 2+3 4+5-6-7-8-9-10-11;1 2+3+4 5+6-7 8-9+10+11;1 2+3+4+5 6-7-8 9+10+11;1 2+3-4-5 6+7+8+9+10+11;1 2-3 4+5+6+7-8-9+10+11;1 2-3 4+5+6-7+8+9-10+11;1 2-3 4+5-6+7+8+9+10-11;1 2-3+4 5-6-7 8+9+10+11;1 2-3-4+5 6+7-8 9+10+11;1+2 3+4 5+6-7-8 9+10+11;1+2 3+4 5-6 7+8-9+10-11;1+2 3+4+5-6-7-8+9-10-11;1+2 3+4-5+6 7-8 9+10-11;1+2 3+4-5+6-7+8-9-10-11;1+2 3-4 5+6-7-8+9+10+11;1+2 3-4 5-6 7+8 9+10-11;1+2 3-4 5-6+7+8-9+10+11;1+2 3-4+5+6+7-8-9-10-11;1+2 3-4-5-6+7-8-9-10+11;1+2 3-4-5-6-7+8-9+10-11;1+2+3 4+5+6-7 8+9+10+11;1+2+3+4 5-6 7+8+9+10-11;1+2+3+4 5-6-7-8-9-10-11;1+2+3+4+5 6-7 8-9+10+11;1+2+3+4+5-6+7-8-9-10+11;1+2+3+4+5-6-7+8-9+10-11;1+2+3+4-5+6+7-8-9+10-11;1+2+3+4-5+6-7+8+9-10-11;1+2+3-4 5-6+7+8+9+10+11;1+2+3-4+5+6+7-8+9-10-11;1+2+3-4-5+6-7-8-9+10+11;1+2+3-4-5-6+7-8+9-10+11;1+2+3-4-5-6-7+8+9+10-11;1+2-3 4 5-6 7 8+9+10 11;1+2-3 4+5 6-7-8-9+10-11;1+2-3+4 5-6 7-8+9+10+11;1+2-3+4+5+6+7+8-9-10-11;1+2-3+4+5-6-7-8-9+10+11;1+2-3+4-5+6-7-8+9-10+11;1+2-3+4-5-6 7+8 9-10-11;1+2-3+4-5-6+7+8-9-10+11;1+2-3+4-5-6+7-8+9+10-11;1+2-3-4 5+6 7+8-9-10-11;1+2-3-4+5+6 7-8 9+10+11;1+2-3-4+5+6-7+8-9-10+11;1+2-3-4+5+6-7-8+9+10-11;1+2-3-4+5-6+7+8-9+10-11;1+2-3-4-5+6+7+8+9-10-11;1+2-3-4-5-6-7-8+9+10+11;1-2 3 4+5+6-7 8 9+10 11;1-2 3+4 5+6 7-8 9+10-11;1-2 3+4 5+6-7+8-9-10-11;1-2 3+4+5+6+7+8-9-10+11;1-2 3+4+5+6+7-8+9+10-11;1-2 3+4-5-6 7+8 9-10+11;1-2 3+4-5-6+7-8+9+10+11;1-2 3-4 5+6 7+8-9-10+11;1-2 3-4 5+6 7-8+9+10-11;1-2 3-4 5+6-7+8 9-10-11;1-2 3-4+5+6-7-8+9+10+11;1-2 3-4+5-6 7+8 9+10-11;1-2 3-4+5-6+7+8-9+10+11;1-2 3-4-5+6+7+8+9-10+11;1-2+3 4-5 6+7+8+9+10-11;1-2+3+4 5-6 7+8-9+10+11;1-2+3+4+5-6-7-8+9-10+11;1-2+3+4-5+6 7-8 9+10+11;1-2+3+4-5+6-7+8-9-10+11;1-2+3+4-5+6-7-8+9+10-11;1-2+3+4-5-6+7+8-9+10-11;1-2+3-4 5-6 7+8 9+10+11;1-2+3-4+5+6+7-8-9-10+11;1-2+3-4+5+6-7+8-9+10-11;1-2+3-4+5-6+7+8+9-10-11;1-2+3-4-5-6-7+8-9+10+11;1-2-3+4+5+6+7-8-9+10-11;1-2-3+4+5+6-7+8+9-10-11;1-2-3+4-5-6+7-8-9+10+11;1-2-3+4-5-6-7+8+9-10+11;1-2-3-4 5+6 7-8-9+10-11;1-2-3-4+5 6-7 8+9+10+11;1-2-3-4+5+6-7-8-9+10+11;1-2-3-4+5-6+7-8+9-10+11;1-2-3-4+5-6-7+8+9+10-11;1-2-3-4-5+6+7+8-9-10+11;1-2-3-4-5+6+7-8+9+10-11",
        //                c);
        //        test("R23,57,89,123,456,654,674,783,894,1023", "23-57+89+123+456-654+674-783-894+1023", c);

        //        test("R213,572,893,1234,4565,6546,6747,7838,8941,10231", null, c);

        //
        //        8721_HC1_23KHF96_CMM_\d{8}_\d{6}\.tgz
        //        String CMM_SUFFIX_PATTERN = "_CMM_\\d{8}_\\d{6}\\.tgz";
        //        String filenamePattern = "8721_HC1_23KHF96" + CMM_SUFFIX_PATTERN;
        //        System.out.println(filenamePattern);
        //        //XXXX_XXX_XXXXXXX_CMM_20130319_022852
        //        //8721_HC1_23KHF96_CMM_CMM_20130319_022852
        //        System.out.println("8721_HC1_23KHF96_CMM_20130319_022852.tgz".matches(filenamePattern));

        //        System.out.println("--------------test S#----------------");
        //                        testAllS();
        //        totalTime += test("S6", "1 2+3-4-5-6", 1);
        //        System.out.println();
        //
        //        System.out.println("--------------test all----------------");
        //        testStandard(1);
        //run it alone, 287ms,320ms
        //                        totalTime += test("S3", "1+2-3", 1);
        //        System.out.println(Integer.parseInt(" 80"));
        //        System.out.println(Integer.parseInt("80 "));
        //                totalTime += test("S3", null, 1);
        //        totalTime += test("S4", null, 1);
        //                totalTime += test("S5", null, 1);
        //        totalTime += test("S15", null, 1);
        //        totalTime += test("S5", "1 2-3-4-5", 1);
        //                        totalTime += test("S6", "1 2+3-4-5-6", 1);
        //        totalTime += test("S7", "1+2-3+4-5-6+7;1+2-3-4+5+6-7;1-2 3+4+5+6+7;1-2 3-4 5+6 7;1-2+3+4-5+6-7;1-2-3-4-5+6+7",
        //                1);
        //        test(
        //                "S15",
        //                "1 2 3 4+5 6+7+8-9+10+11+12-13 14-15;1 2 3 4+5 6-7 8+9+10+11-12 13-14-15;1 2 3 4+5 6-7-8-9+10+11+12-13 14+15;1 2 3 4+5+6 7+8 9+10+11+12-13-14 15;1 2 3 4+5+6+7 8+9-10-11-12-13 14+15;1 2 3 4+5+6+7-8 9+10+11-12 13+14+15;1 2 3 4+5+6+7-8-9-10-11-12 13+14-15;1 2 3 4+5+6-7-8-9+10+11-12 13-14-15;1 2 3 4+5-6+7 8+9+10+11-12-13 14-15;1 2 3 4+5-6+7-8+9-10+11-12 13-14-15;1 2 3 4+5-6-7 8+9-10-11 12-13-14-15;1 2 3 4+5-6-7+8+9+10-11-12 13-14-15;1 2 3 4-5+6+7+8-9-10+11-12 13-14-15;1 2 3 4-5+6+7-8+9+10-11-12 13-14-15;1 2 3 4-5+6-7-8-9-10+11-12 13-14+15;1 2 3 4-5-6 7-8+9+10+11-12 13+14+15;1 2 3 4-5-6+7 8+9-10+11-12-13 14+15;1 2 3 4-5-6+7-8-9+10-11-12 13-14+15;1 2 3 4-5-6+7-8-9-10 11+12 13-14 15;1 2 3 4-5-6+7-8-9-10+11-12 13+14-15;1 2 3 4-5-6-7 8-9-10-11 12-13+14-15;1 2 3 4-5-6-7+8+9-10-11-12 13-14+15;1 2 3 4-5-6-7+8-9+10-11-12 13+14-15;1 2 3+4 5 6+7+8-9 10-11 12+13+14 15;1 2 3+4 5-6+7 8-9+10 11-12 13-14-15;1 2 3+4 5-6-7 8-9-10-11-12-13-14-15;1 2 3+4+5 6+7+8+9+10+11 12-13 14-15;1 2 3+4+5 6+7-8-9+10 11-12 13+14+15;1 2 3+4+5 6-7-8+9+10+11 12-13 14+15;1 2 3+4+5+6+7 8+9 10-11 12-13+14-15;1 2 3+4+5+6+7 8-9+10+11 12-13 14-15;1 2 3+4+5+6-7 8-9-10-11+12-13-14-15;1 2 3+4-5 6-7-8 9+10+11-12-13+14+15;1 2 3+4-5 6-7-8 9+10-11+12+13-14+15;1 2 3+4-5 6-7-8 9-10+11+12+13+14-15;1 2 3+4-5 6-7-8-9-10-11-12-13+14-15;1 2 3+4-5+6 7 8+9-10 11-12 13+14 15;1 2 3+4-5+6+7 8-9-10+11 12-13 14+15;1 2 3+4-5-6+7 8+9+10 11-12 13+14-15;1 2 3+4-5-6+7 8+9+10-11+12 13-14 15;1 2 3+4-5-6-7 8+9-10-11-12-13+14-15;1 2 3+4-5-6-7 8-9+10-11-12+13-14-15;1 2 3+4-5-6-7 8-9-10+11+12-13-14-15;1 2 3-4 5 6-7-8 9-10 11+12+13+14 15;1 2 3-4 5+6-7 8-9+10-11-12-13+14+15;1 2 3-4 5+6-7 8-9-10+11-12+13-14+15;1 2 3-4 5+6-7 8-9-10-11+12+13+14-15;1 2 3-4 5-6-7 8+9+10+11-12-13-14+15;1 2 3-4 5-6-7 8+9+10-11+12-13+14-15;1 2 3-4 5-6-7 8+9-10+11+12+13-14-15;1 2 3-4+5 6+7+8+9 10-11 12+13+14-15;1 2 3-4+5 6+7+8-9+10+11+12 13-14 15;1 2 3-4+5 6-7-8+9 10-11 12+13+14+15;1 2 3-4+5+6 7+8 9+10+11 12+13-14 15;1 2 3-4+5-6-7 8+9-10-11-12+13-14-15;1 2 3-4+5-6-7 8-9+10-11+12-13-14-15;1 2 3-4-5 6+7-8 9+10+11+12-13+14-15;1 2 3-4-5 6+7-8-9-10+11-12-13-14-15;1 2 3-4-5 6-7+8-9+10-11-12-13-14-15;1 2 3-4-5 6-7-8 9-10-11+12+13+14+15;1 2 3-4-5+6+7 8+9+10+11 12-13 14-15;1 2 3-4-5+6-7 8+9-10-11+12-13-14-15;1 2 3-4-5+6-7 8-9+10+11-12-13-14-15;1 2 3-4-5-6+7 8+9 10-11 12-13+14+15;1 2 3-4-5-6+7 8-9+10+11 12-13 14+15;1 2 3-4-5-6-7 8-9-10-11+12-13-14+15;1 2 3-4-5-6-7 8-9-10-11-12+13+14-15;1 2+3 4 5+6-7 8+9 10-11-12 13+14+15;1 2+3 4 5+6-7 8-9+10 11+12-13 14+15;1 2+3 4 5-6 7 8-9 10-11+12 13+14+15;1 2+3 4 5-6 7 8-9-10 11+12+13 14+15;1 2+3 4 5-6 7+8 9+10 11+12+13-14 15;1 2+3 4 5-6 7-8 9+10 11-12 13-14+15;1 2+3 4 5-6 7-8 9-10+11+12 13-14 15;1 2+3 4 5-6+7+8+9 10+11+12-13 14+15;1 2+3 4+5 6-7 8+9+10+11-12-13-14-15;1 2+3 4+5+6 7+8 9+10+11 12-13 14-15;1 2+3 4+5+6 7-8 9+10-11-12+13-14-15;1 2+3 4+5+6 7-8 9-10+11+12-13-14-15;1 2+3 4+5+6+7-8 9+10+11-12-13+14+15;1 2+3 4+5+6+7-8 9+10-11+12+13-14+15;1 2+3 4+5+6+7-8 9-10+11+12+13+14-15;1 2+3 4+5+6+7-8-9-10-11-12-13+14-15;1 2+3 4+5+6-7+8-9-10-11-12+13-14-15;1 2+3 4+5+6-7-8+9-10-11+12-13-14-15;1 2+3 4+5+6-7-8-9+10+11-12-13-14-15;1 2+3 4+5-6 7+8+9+10-11+12-13-14+15;1 2+3 4+5-6 7+8+9+10-11-12+13+14-15;1 2+3 4+5-6 7+8+9-10+11+12-13+14-15;1 2+3 4+5-6 7+8-9+10+11+12+13-14-15;1 2+3 4+5-6 7-8-9-10-11+12+13+14+15;1 2+3 4+5-6+7+8-9-10-11+12-13-14-15;1 2+3 4+5-6+7-8+9-10+11-12-13-14-15;1 2+3 4+5-6-7+8+9+10-11-12-13-14-15;1 2+3 4+5-6-7-8 9+10+11-12+13+14+15;1 2+3 4+5-6-7-8-9-10-11+12-13-14+15;1 2+3 4+5-6-7-8-9-10-11-12+13+14-15;1 2+3 4-5+6 7 8-9 10-11-12 13+14 15;1 2+3 4-5+6 7+8 9-10+11 12-13 14+15;1 2+3 4-5+6 7-8 9-10-11-12+13-14+15;1 2+3 4-5+6+7+8-9-10+11-12-13-14-15;1 2+3 4-5+6+7-8+9+10-11-12-13-14-15;1 2+3 4-5+6-7-8 9+10+11+12-13+14+15;1 2+3 4-5+6-7-8-9-10+11-12-13-14+15;1 2+3 4-5+6-7-8-9-10-11+12-13+14-15;1 2+3 4-5-6 7+8+9-10-11-12+13+14+15;1 2+3 4-5-6 7+8-9+10-11+12-13+14+15;1 2+3 4-5-6 7+8-9-10+11+12+13-14+15;1 2+3 4-5-6 7-8+9+10+11-12-13+14+15;1 2+3 4-5-6 7-8+9+10-11+12+13-14+15;1 2+3 4-5-6 7-8+9-10+11+12+13+14-15;1 2+3 4-5-6+7-8 9+10+11+12+13-14+15;1 2+3 4-5-6+7-8-9+10-11-12-13-14+15;1 2+3 4-5-6+7-8-9-10+11-12-13+14-15;1 2+3 4-5-6+7-8-9-10-11+12+13-14-15;1 2+3 4-5-6-7+8+9-10-11-12-13-14+15;1 2+3 4-5-6-7+8-9+10-11-12-13+14-15;1 2+3 4-5-6-7+8-9-10+11-12+13-14-15;1 2+3 4-5-6-7-8+9+10-11-12+13-14-15;1 2+3 4-5-6-7-8+9-10+11+12-13-14-15;1 2+3+4 5+6-7 8+9+10-11-12-13+14+15;1 2+3+4 5+6-7 8+9-10+11-12+13-14+15;1 2+3+4 5+6-7 8+9-10-11+12+13+14-15;1 2+3+4 5+6-7 8-9+10+11+12-13-14+15;1 2+3+4 5+6-7 8-9+10+11-12+13+14-15;1 2+3+4 5-6-7 8-9-10-11+12+13+14+15;1 2+3+4+5 6 7+8-9 10-11 12+13+14 15;1 2+3+4+5 6+7-8 9-10-11+12-13+14+15;1 2+3+4+5 6-7-8 9+10+11+12-13-14+15;1 2+3+4+5 6-7-8 9+10+11-12+13+14-15;1 2+3+4+5 6-7-8-9-10-11+12-13-14-15;1 2+3+4+5-6-7 8+9+10+11-12+13+14+15;1 2+3+4-5 6+7+8-9-10+11-12+13+14+15;1 2+3+4-5 6+7-8+9+10-11-12+13+14+15;1 2+3+4-5 6+7-8+9-10+11+12-13+14+15;1 2+3+4-5 6+7-8-9+10+11+12+13-14+15;1 2+3+4-5 6-7+8 9-10-11-12-13-14+15;1 2+3+4-5 6-7+8+9+10-11+12-13+14+15;1 2+3+4-5 6-7+8+9-10+11+12+13-14+15;1 2+3+4-5 6-7+8-9+10+11+12+13+14-15;1 2+3+4-5+6+7 8 9-10 11-12 13+14 15;1 2+3+4-5+6-7 8+9+10+11+12-13+14+15;1 2+3-4 5 6+7-8-9 10+11+12+13 14+15;1 2+3-4 5 6-7 8-9 10-11+12+13+14 15;1 2+3-4 5+6+7 8-9-10-11-12-13-14+15;1 2+3-4 5-6+7 8+9-10-11+12-13-14-15;1 2+3-4 5-6+7 8-9+10+11-12-13-14-15;1 2+3-4+5 6+7-8+9-10-11-12-13-14-15;1 2+3-4+5 6-7-8 9+10-11-12+13+14+15;1 2+3-4+5 6-7-8 9-10+11+12-13+14+15;1 2+3-4+5+6-7 8+9+10+11+12+13-14+15;1 2+3-4-5 6+7+8 9-10-11+12-13-14-15;1 2+3-4-5 6+7+8+9+10+11+12-13-14+15;1 2+3-4-5 6+7+8+9+10+11-12+13+14-15;1 2+3-4-5 6+7-8-9-10+11+12+13+14+15;1 2+3-4-5 6-7+8-9+10-11+12+13+14+15;1 2+3-4-5 6-7-8+9+10+11-12+13+14+15;1 2+3-4-5+6-7 8-9+10+11+12+13+14+15;1 2-3 4 5+6 7+8 9-10-11 12+13 14-15;1 2-3 4 5+6+7+8 9-10 11+12 13+14+15;1 2-3 4 5+6-7 8 9-10+11 12+13-14+15;1 2-3 4+5 6-7 8-9+10-11+12+13+14+15;1 2-3 4+5+6 7+8-9-10-11-12+13-14-15;1 2-3 4+5+6 7-8+9-10-11+12-13-14-15;1 2-3 4+5+6 7-8-9+10+11-12-13-14-15;1 2-3 4+5+6+7+8-9-10+11-12-13+14+15;1 2-3 4+5+6+7+8-9-10-11+12+13-14+15;1 2-3 4+5+6+7-8+9+10-11-12-13+14+15;1 2-3 4+5+6+7-8+9-10+11-12+13-14+15;1 2-3 4+5+6+7-8+9-10-11+12+13+14-15;1 2-3 4+5+6+7-8-9+10+11+12-13-14+15;1 2-3 4+5+6+7-8-9+10+11-12+13+14-15;1 2-3 4+5+6-7+8+9+10-11-12+13-14+15;1 2-3 4+5+6-7+8+9-10+11+12-13-14+15;1 2-3 4+5+6-7+8+9-10+11-12+13+14-15;1 2-3 4+5+6-7+8-9+10+11+12-13+14-15;1 2-3 4+5+6-7-8+9+10+11+12+13-14-15;1 2-3 4+5-6 7+8 9+10+11-12-13+14-15;1 2-3 4+5-6 7+8 9+10-11+12+13-14-15;1 2-3 4+5-6 7-8 9-10 11+12 13-14-15;1 2-3 4+5-6+7+8+9+10-11+12-13-14+15;1 2-3 4+5-6+7+8+9+10-11-12+13+14-15;1 2-3 4+5-6+7+8+9-10+11+12-13+14-15;1 2-3 4+5-6+7+8-9+10+11+12+13-14-15;1 2-3 4+5-6+7-8-9-10-11+12+13+14+15;1 2-3 4+5-6-7+8-9-10+11-12+13+14+15;1 2-3 4+5-6-7-8+9+10-11-12+13+14+15;1 2-3 4+5-6-7-8+9-10+11+12-13+14+15;1 2-3 4+5-6-7-8-9+10+11+12+13-14+15;1 2-3 4-5 6+7 8+9+10+11+12-13-14-15;1 2-3 4-5 6+7 8-9-10-11-12+13+14+15;1 2-3 4-5+6 7-8 9+10+11+12-13+14+15;1 2-3 4-5+6 7-8-9-10+11-12-13-14+15;1 2-3 4-5+6 7-8-9-10-11+12-13+14-15;1 2-3 4-5+6+7+8 9-10-11-12-13-14-15;1 2-3 4-5+6+7+8+9+10+11-12-13-14+15;1 2-3 4-5+6+7+8+9+10-11+12-13+14-15;1 2-3 4-5+6+7+8+9-10+11+12+13-14-15;1 2-3 4-5+6+7-8-9-10+11-12+13+14+15;1 2-3 4-5+6-7+8-9+10-11-12+13+14+15;1 2-3 4-5+6-7+8-9-10+11+12-13+14+15;1 2-3 4-5+6-7-8+9+10-11+12-13+14+15;1 2-3 4-5+6-7-8+9-10+11+12+13-14+15;1 2-3 4-5+6-7-8-9+10+11+12+13+14-15;1 2-3 4-5-6 7+8 9-10+11-12-13+14+15;1 2-3 4-5-6 7+8 9-10-11+12+13-14+15;1 2-3 4-5-6+7+8+9-10-11-12+13+14+15;1 2-3 4-5-6+7+8-9+10-11+12-13+14+15;1 2-3 4-5-6+7+8-9-10+11+12+13-14+15;1 2-3 4-5-6+7-8+9+10+11-12-13+14+15;1 2-3 4-5-6+7-8+9+10-11+12+13-14+15;1 2-3 4-5-6+7-8+9-10+11+12+13+14-15;1 2-3 4-5-6-7+8 9-10-11-12+13-14-15;1 2-3 4-5-6-7+8+9+10+11-12+13-14+15;1 2-3 4-5-6-7+8+9+10-11+12+13+14-15;1 2-3+4 5+6-7 8+9-10-11-12+13+14+15;1 2-3+4 5+6-7 8-9+10-11+12-13+14+15;1 2-3+4 5+6-7 8-9-10+11+12+13-14+15;1 2-3+4 5-6-7 8+9+10+11+12-13-14+15;1 2-3+4 5-6-7 8+9+10+11-12+13+14-15;1 2-3+4+5 6+7+8-9-10-11-12-13-14-15;1 2-3+4+5 6-7-8 9+10-11+12-13+14+15;1 2-3+4+5 6-7-8 9-10+11+12+13-14+15;1 2-3+4+5 6-7-8-9-10-11-12-13-14+15;1 2-3+4+5+6-7 8+9+10+11+12+13+14-15;1 2-3+4+5-6-7 8-9+10+11+12+13+14+15;1 2-3+4-5 6+7+8 9-10+11-12-13-14-15;1 2-3+4-5 6+7+8+9+10+11+12-13+14-15;1 2-3+4-5 6+7-8-9+10-11+12+13+14+15;1 2-3+4-5 6-7+8+9-10-11+12+13+14+15;1 2-3+4-5 6-7+8-9+10+11-12+13+14+15;1 2-3+4-5 6-7-8+9+10+11+12-13+14+15;1 2-3+4-5+6-7 8+9-10+11+12+13+14+15;1 2-3-4 5-6+7 8+9-10-11-12-13-14+15;1 2-3-4 5-6+7 8-9+10-11-12-13+14-15;1 2-3-4 5-6+7 8-9-10+11-12+13-14-15;1 2-3-4+5 6+7-8 9+10+11+12-13-14+15;1 2-3-4+5 6+7-8 9+10+11-12+13+14-15;1 2-3-4+5 6+7-8-9-10-11+12-13-14-15;1 2-3-4+5 6-7+8-9-10+11-12-13-14-15;1 2-3-4+5 6-7-8+9+10-11-12-13-14-15;1 2-3-4+5+6-7 8+9+10-11+12+13+14+15;1 2-3-4-5 6+7+8 9-10-11-12-13-14+15;1 2-3-4-5 6+7+8+9+10-11+12-13+14+15;1 2-3-4-5 6+7+8+9-10+11+12+13-14+15;1 2-3-4-5 6+7+8-9+10+11+12+13+14-15;1 2-3-4-5 6-7+8 9+10-11+12-13-14-15;1 2-3-4-5 6-7-8-9+10+11+12+13+14+15;1 2-3-4-5+6+7 8-9-10-11-12-13-14-15;1 2-3-4-5-6-7 8+9+10+11+12+13+14+15;1+2 3 4 5+6+7 8-9-10-11 12-13 14+15;1+2 3 4 5+6-7-8-9 10-11+12-13-14 15;1+2 3 4 5+6-7-8-9-10 11+12-13 14-15;1+2 3 4 5-6+7 8+9-10 11+12-13-14 15;1+2 3 4 5-6+7-8-9 10+11-12-13-14 15;1+2 3 4 5-6-7 8-9 10-11-12-13 14-15;1+2 3 4 5-6-7 8-9-10 11-12 13-14-15;1+2 3 4+5 6+7 8 9-10-11 12+13+14+15;1+2 3 4+5+6 7+8+9 10-11-12 13+14-15;1+2 3 4+5+6 7+8-9+10 11+12-13 14-15;1+2 3 4+5+6 7+8-9+10+11 12-13-14 15;1+2 3 4+5+6+7 8 9-10 11-12-13-14+15;1+2 3 4+5+6-7 8+9+10+11 12-13 14+15;1+2 3 4+5-6 7 8+9-10 11+12+13+14 15;1+2 3 4+5-6+7+8 9+10 11-12-13 14-15;1+2 3 4-5 6+7+8+9+10 11-12 13+14-15;1+2 3 4-5 6+7+8+9+10-11+12 13-14 15;1+2 3 4-5 6-7-8+9+10 11-12 13+14+15;1+2 3 4-5+6 7-8-9+10+11 12+13-14 15;1+2 3 4-5-6+7 8 9-10 11+12-13+14-15;1+2 3 4-5-6-7+8 9+10 11+12-13 14-15;1+2 3 4-5-6-7+8 9+10+11 12-13-14 15;1+2 3+4 5 6+7+8+9 10+11+12-13-14 15;1+2 3+4 5+6 7-8 9-10-11-12-13+14-15;1+2 3+4 5+6+7-8 9-10-11+12-13+14+15;1+2 3+4 5+6-7-8 9+10+11+12-13-14+15;1+2 3+4 5+6-7-8 9+10+11-12+13+14-15;1+2 3+4 5+6-7-8-9-10-11+12-13-14-15;1+2 3+4 5-6 7+8+9-10-11-12+13-14+15;1+2 3+4 5-6 7+8-9+10-11+12-13-14+15;1+2 3+4 5-6 7+8-9+10-11-12+13+14-15;1+2 3+4 5-6 7+8-9-10+11+12-13+14-15;1+2 3+4 5-6 7-8+9+10+11-12-13-14+15;1+2 3+4 5-6 7-8+9+10-11+12-13+14-15;1+2 3+4 5-6 7-8+9-10+11+12+13-14-15;1+2 3+4 5-6+7-8 9+10+11+12-13+14-15;1+2 3+4 5-6+7-8-9-10+11-12-13-14-15;1+2 3+4 5-6-7+8-9+10-11-12-13-14-15;1+2 3+4 5-6-7-8 9-10-11+12+13+14+15;1+2 3+4+5 6-7 8-9+10-11-12-13+14+15;1+2 3+4+5 6-7 8-9-10+11-12+13-14+15;1+2 3+4+5 6-7 8-9-10-11+12+13+14-15;1+2 3+4+5+6+7+8-9-10-11-12-13-14+15;1+2 3+4+5+6+7-8+9-10-11-12-13+14-15;1+2 3+4+5+6+7-8-9+10-11-12+13-14-15;1+2 3+4+5+6+7-8-9-10+11+12-13-14-15;1+2 3+4+5+6-7+8+9-10-11-12+13-14-15;1+2 3+4+5+6-7+8-9+10-11+12-13-14-15;1+2 3+4+5+6-7-8+9+10+11-12-13-14-15;1+2 3+4+5-6 7+8 9+10-11-12-13-14-15;1+2 3+4+5-6 7+8+9+10+11+12+13-14-15;1+2 3+4+5-6 7-8+9-10-11+12+13+14+15;1+2 3+4+5-6 7-8-9+10+11-12+13+14+15;1+2 3+4+5-6+7+8+9-10-11+12-13-14-15;1+2 3+4+5-6+7+8-9+10+11-12-13-14-15;1+2 3+4+5-6+7-8 9-10+11+12+13+14+15;1+2 3+4+5-6+7-8-9-10-11-12-13+14+15;1+2 3+4+5-6-7+8-9-10-11-12+13-14+15;1+2 3+4+5-6-7-8+9-10-11+12-13-14+15;1+2 3+4+5-6-7-8+9-10-11-12+13+14-15;1+2 3+4+5-6-7-8-9+10+11-12-13-14+15;1+2 3+4+5-6-7-8-9+10-11+12-13+14-15;1+2 3+4+5-6-7-8-9-10+11+12+13-14-15;1+2 3+4-5+6 7-8 9+10-11+12-13-14+15;1+2 3+4-5+6 7-8 9+10-11-12+13+14-15;1+2 3+4-5+6 7-8 9-10+11+12-13+14-15;1+2 3+4-5+6+7+8+9-10+11-12-13-14-15;1+2 3+4-5+6+7-8 9+10-11+12+13+14+15;1+2 3+4-5+6+7-8-9-10-11-12+13-14+15;1+2 3+4-5+6-7+8-9-10-11+12-13-14+15;1+2 3+4-5+6-7+8-9-10-11-12+13+14-15;1+2 3+4-5+6-7-8+9-10+11-12-13-14+15;1+2 3+4-5+6-7-8+9-10-11+12-13+14-15;1+2 3+4-5+6-7-8-9+10+11-12-13+14-15;1+2 3+4-5+6-7-8-9+10-11+12+13-14-15;1+2 3+4-5-6 7+8 9-10-11-12-13-14+15;1+2 3+4-5-6 7+8+9+10-11+12-13+14+15;1+2 3+4-5-6 7+8+9-10+11+12+13-14+15;1+2 3+4-5-6 7+8-9+10+11+12+13+14-15;1+2 3+4-5-6+7+8-9-10+11-12-13-14+15;1+2 3+4-5-6+7+8-9-10-11+12-13+14-15;1+2 3+4-5-6+7-8+9+10-11-12-13-14+15;1+2 3+4-5-6+7-8+9-10+11-12-13+14-15;1+2 3+4-5-6+7-8+9-10-11+12+13-14-15;1+2 3+4-5-6+7-8-9+10+11-12+13-14-15;1+2 3+4-5-6-7+8+9+10-11-12-13+14-15;1+2 3+4-5-6-7+8+9-10+11-12+13-14-15;1+2 3+4-5-6-7+8-9+10+11+12-13-14-15;1+2 3+4-5-6-7-8-9-10-11+12-13+14+15;1+2 3-4 5 6-7 8 9-10-11+12 13+14+15;1+2 3-4 5+6 7+8-9-10-11-12-13-14+15;1+2 3-4 5+6 7-8+9-10-11-12-13+14-15;1+2 3-4 5+6 7-8-9+10-11-12+13-14-15;1+2 3-4 5+6 7-8-9-10+11+12-13-14-15;1+2 3-4 5+6+7+8+9+10+11+12-13-14-15;1+2 3-4 5+6+7+8-9-10-11-12+13+14+15;1+2 3-4 5+6+7-8+9-10-11+12-13+14+15;1+2 3-4 5+6+7-8-9+10+11-12-13+14+15;1+2 3-4 5+6+7-8-9+10-11+12+13-14+15;1+2 3-4 5+6+7-8-9-10+11+12+13+14-15;1+2 3-4 5+6-7+8+9-10+11-12-13+14+15;1+2 3-4 5+6-7+8+9-10-11+12+13-14+15;1+2 3-4 5+6-7+8-9+10+11-12+13-14+15;1+2 3-4 5+6-7+8-9+10-11+12+13+14-15;1+2 3-4 5+6-7-8+9+10+11+12-13-14+15;1+2 3-4 5+6-7-8+9+10+11-12+13+14-15;1+2 3-4 5-6 7+8 9+10-11+12-13-14+15;1+2 3-4 5-6 7+8 9+10-11-12+13+14-15;1+2 3-4 5-6 7+8 9-10+11+12-13+14-15;1+2 3-4 5-6 7-8 9-10-11 12+13 14-15;1+2 3-4 5-6+7+8+9+10-11-12-13+14+15;1+2 3-4 5-6+7+8+9-10+11-12+13-14+15;1+2 3-4 5-6+7+8+9-10-11+12+13+14-15;1+2 3-4 5-6+7+8-9+10+11+12-13-14+15;1+2 3-4 5-6+7+8-9+10+11-12+13+14-15;1+2 3-4 5-6+7-8+9+10+11+12-13+14-15;1+2 3-4 5-6-7+8 9+10-11-12-13-14-15;1+2 3-4 5-6-7+8+9+10+11+12+13-14-15;1+2 3-4 5-6-7-8+9-10-11+12+13+14+15;1+2 3-4 5-6-7-8-9+10+11-12+13+14+15;1+2 3-4+5 6-7 8+9+10+11-12+13-14-15;1+2 3-4+5+6 7+8 9+10+11+12 13-14 15;1+2 3-4+5+6 7-8 9+10+11-12-13-14+15;1+2 3-4+5+6 7-8 9+10-11+12-13+14-15;1+2 3-4+5+6 7-8 9-10+11+12+13-14-15;1+2 3-4+5+6 7-8-9-10-11-12-13-14-15;1+2 3-4+5+6+7+8+9+10-11-12-13-14-15;1+2 3-4+5+6+7-8 9+10+11-12+13+14+15;1+2 3-4+5+6+7-8-9-10-11+12-13-14+15;1+2 3-4+5+6+7-8-9-10-11-12+13+14-15;1+2 3-4+5+6-7+8-9-10+11-12-13-14+15;1+2 3-4+5+6-7+8-9-10-11+12-13+14-15;1+2 3-4+5+6-7-8+9+10-11-12-13-14+15;1+2 3-4+5+6-7-8+9-10+11-12-13+14-15;1+2 3-4+5+6-7-8+9-10-11+12+13-14-15;1+2 3-4+5+6-7-8-9+10+11-12+13-14-15;1+2 3-4+5-6 7+8 9-10-11-12-13+14-15;1+2 3-4+5-6 7+8+9+10+11-12-13+14+15;1+2 3-4+5-6 7+8+9+10-11+12+13-14+15;1+2 3-4+5-6 7+8+9-10+11+12+13+14-15;1+2 3-4+5-6+7+8-9+10-11-12-13-14+15;1+2 3-4+5-6+7+8-9-10+11-12-13+14-15;1+2 3-4+5-6+7+8-9-10-11+12+13-14-15;1+2 3-4+5-6+7-8+9+10-11-12-13+14-15;1+2 3-4+5-6+7-8+9-10+11-12+13-14-15;1+2 3-4+5-6+7-8-9+10+11+12-13-14-15;1+2 3-4+5-6-7+8+9+10-11-12+13-14-15;1+2 3-4+5-6-7+8+9-10+11+12-13-14-15;1+2 3-4+5-6-7-8-9-10+11-12-13+14+15;1+2 3-4+5-6-7-8-9-10-11+12+13-14+15;1+2 3-4-5 6+7 8+9-10-11+12-13-14-15;1+2 3-4-5 6+7 8-9+10+11-12-13-14-15;1+2 3-4-5+6 7 8-9 10-11 12+13 14+15;1+2 3-4-5+6 7-8 9-10-11+12-13+14+15;1+2 3-4-5+6+7+8+9-10-11-12-13-14+15;1+2 3-4-5+6+7+8-9+10-11-12-13+14-15;1+2 3-4-5+6+7+8-9-10+11-12+13-14-15;1+2 3-4-5+6+7-8+9+10-11-12+13-14-15;1+2 3-4-5+6+7-8+9-10+11+12-13-14-15;1+2 3-4-5+6-7+8+9+10-11+12-13-14-15;1+2 3-4-5+6-7-8 9+10+11+12+13+14+15;1+2 3-4-5+6-7-8-9+10-11-12-13+14+15;1+2 3-4-5+6-7-8-9-10+11-12+13-14+15;1+2 3-4-5+6-7-8-9-10-11+12+13+14-15;1+2 3-4-5-6 7+8-9+10-11+12+13+14+15;1+2 3-4-5-6 7-8+9+10+11-12+13+14+15;1+2 3-4-5-6+7+8+9+10+11-12-13-14-15;1+2 3-4-5-6+7-8+9-10-11-12-13+14+15;1+2 3-4-5-6+7-8-9+10-11-12+13-14+15;1+2 3-4-5-6+7-8-9-10+11+12-13-14+15;1+2 3-4-5-6+7-8-9-10+11-12+13+14-15;1+2 3-4-5-6-7+8+9-10-11-12+13-14+15;1+2 3-4-5-6-7+8-9+10-11+12-13-14+15;1+2 3-4-5-6-7+8-9+10-11-12+13+14-15;1+2 3-4-5-6-7+8-9-10+11+12-13+14-15;1+2 3-4-5-6-7-8+9+10+11-12-13-14+15;1+2 3-4-5-6-7-8+9+10-11+12-13+14-15;1+2 3-4-5-6-7-8+9-10+11+12+13-14-15;1+2+3 4 5+6 7 8+9-10 11-12-13-14+15;1+2+3 4 5+6 7-8 9-10+11 12-13-14 15;1+2+3 4 5+6-7-8-9+10 11-12-13 14-15;1+2+3 4 5-6 7+8-9+10+11 12+13-14 15;1+2+3 4 5-6+7 8+9 10+11-12-13 14-15;1+2+3 4 5-6+7 8+9+10 11-12-13-14 15;1+2+3 4 5-6-7+8+9 10-11-12 13-14-15;1+2+3 4+5 6-7-8 9+10-11-12-13+14+15;1+2+3 4+5 6-7-8 9-10+11-12+13-14+15;1+2+3 4+5 6-7-8 9-10-11+12+13+14-15;1+2+3 4+5+6-7 8+9+10+11+12-13-14+15;1+2+3 4+5+6-7 8+9+10+11-12+13+14-15;1+2+3 4+5-6-7 8+9-10-11+12+13+14+15;1+2+3 4+5-6-7 8-9+10+11-12+13+14+15;1+2+3 4-5 6+7+8+9+10+11-12-13+14-15;1+2+3 4-5 6+7+8+9+10-11+12+13-14-15;1+2+3 4-5 6+7-8-9+10-11-12+13+14+15;1+2+3 4-5 6+7-8-9-10+11+12-13+14+15;1+2+3 4-5 6-7+8+9-10-11-12+13+14+15;1+2+3 4-5 6-7+8-9+10-11+12-13+14+15;1+2+3 4-5 6-7+8-9-10+11+12+13-14+15;1+2+3 4-5 6-7-8+9+10+11-12-13+14+15;1+2+3 4-5 6-7-8+9+10-11+12+13-14+15;1+2+3 4-5 6-7-8+9-10+11+12+13+14-15;1+2+3 4-5+6-7 8+9-10+11-12+13+14+15;1+2+3 4-5+6-7 8-9+10+11+12-13+14+15;1+2+3+4 5+6 7+8 9+10+11 12-13 14-15;1+2+3+4 5+6 7-8 9+10-11-12+13-14-15;1+2+3+4 5+6 7-8 9-10+11+12-13-14-15;1+2+3+4 5+6+7-8 9+10+11-12-13+14+15;1+2+3+4 5+6+7-8 9+10-11+12+13-14+15;1+2+3+4 5+6+7-8 9-10+11+12+13+14-15;1+2+3+4 5+6+7-8-9-10-11-12-13+14-15;1+2+3+4 5+6-7+8-9-10-11-12+13-14-15;1+2+3+4 5+6-7-8+9-10-11+12-13-14-15;1+2+3+4 5+6-7-8-9+10+11-12-13-14-15;1+2+3+4 5-6 7+8+9+10-11+12-13-14+15;1+2+3+4 5-6 7+8+9+10-11-12+13+14-15;1+2+3+4 5-6 7+8+9-10+11+12-13+14-15;1+2+3+4 5-6 7+8-9+10+11+12+13-14-15;1+2+3+4 5-6 7-8-9-10-11+12+13+14+15;1+2+3+4 5-6+7+8-9-10-11+12-13-14-15;1+2+3+4 5-6+7-8+9-10+11-12-13-14-15;1+2+3+4 5-6-7+8+9+10-11-12-13-14-15;1+2+3+4 5-6-7-8 9+10+11-12+13+14+15;1+2+3+4 5-6-7-8-9-10-11+12-13-14+15;1+2+3+4 5-6-7-8-9-10-11-12+13+14-15;1+2+3+4+5 6-7 8+9+10-11-12-13+14+15;1+2+3+4+5 6-7 8+9-10+11-12+13-14+15;1+2+3+4+5 6-7 8+9-10-11+12+13+14-15;1+2+3+4+5 6-7 8-9+10+11+12-13-14+15;1+2+3+4+5 6-7 8-9+10+11-12+13+14-15;1+2+3+4+5+6 7 8-9 10-11 12+13 14+15;1+2+3+4+5+6 7-8 9-10-11+12-13+14+15;1+2+3+4+5+6+7+8+9-10-11-12-13-14+15;1+2+3+4+5+6+7+8-9+10-11-12-13+14-15;1+2+3+4+5+6+7+8-9-10+11-12+13-14-15;1+2+3+4+5+6+7-8+9+10-11-12+13-14-15;1+2+3+4+5+6+7-8+9-10+11+12-13-14-15;1+2+3+4+5+6-7+8+9+10-11+12-13-14-15;1+2+3+4+5+6-7-8 9+10+11+12+13+14+15;1+2+3+4+5+6-7-8-9+10-11-12-13+14+15;1+2+3+4+5+6-7-8-9-10+11-12+13-14+15;1+2+3+4+5+6-7-8-9-10-11+12+13+14-15;1+2+3+4+5-6 7+8-9+10-11+12+13+14+15;1+2+3+4+5-6 7-8+9+10+11-12+13+14+15;1+2+3+4+5-6+7+8+9+10+11-12-13-14-15;1+2+3+4+5-6+7-8+9-10-11-12-13+14+15;1+2+3+4+5-6+7-8-9+10-11-12+13-14+15;1+2+3+4+5-6+7-8-9-10+11+12-13-14+15;1+2+3+4+5-6+7-8-9-10+11-12+13+14-15;1+2+3+4+5-6-7+8+9-10-11-12+13-14+15;1+2+3+4+5-6-7+8-9+10-11+12-13-14+15;1+2+3+4+5-6-7+8-9+10-11-12+13+14-15;1+2+3+4+5-6-7+8-9-10+11+12-13+14-15;1+2+3+4+5-6-7-8+9+10+11-12-13-14+15;1+2+3+4+5-6-7-8+9+10-11+12-13+14-15;1+2+3+4+5-6-7-8+9-10+11+12+13-14-15;1+2+3+4-5 6+7 8-9-10+11-12-13-14+15;1+2+3+4-5 6+7 8-9-10-11+12-13+14-15;1+2+3+4-5+6 7-8 9+10+11+12+13-14-15;1+2+3+4-5+6 7-8-9+10-11-12-13-14-15;1+2+3+4-5+6+7+8-9-10-11-12-13+14+15;1+2+3+4-5+6+7-8+9-10-11-12+13-14+15;1+2+3+4-5+6+7-8-9+10-11+12-13-14+15;1+2+3+4-5+6+7-8-9+10-11-12+13+14-15;1+2+3+4-5+6+7-8-9-10+11+12-13+14-15;1+2+3+4-5+6-7+8+9-10-11+12-13-14+15;1+2+3+4-5+6-7+8+9-10-11-12+13+14-15;1+2+3+4-5+6-7+8-9+10+11-12-13-14+15;1+2+3+4-5+6-7+8-9+10-11+12-13+14-15;1+2+3+4-5+6-7+8-9-10+11+12+13-14-15;1+2+3+4-5+6-7-8+9+10+11-12-13+14-15;1+2+3+4-5+6-7-8+9+10-11+12+13-14-15;1+2+3+4-5-6 7+8 9+10-11-12-13+14-15;1+2+3+4-5-6 7+8 9-10+11-12+13-14-15;1+2+3+4-5-6 7+8+9+10+11+12+13+14-15;1+2+3+4-5-6+7+8+9-10+11-12-13-14+15;1+2+3+4-5-6+7+8+9-10-11+12-13+14-15;1+2+3+4-5-6+7+8-9+10+11-12-13+14-15;1+2+3+4-5-6+7+8-9+10-11+12+13-14-15;1+2+3+4-5-6+7-8+9+10+11-12+13-14-15;1+2+3+4-5-6-7+8+9+10+11+12-13-14-15;1+2+3+4-5-6-7+8-9-10-11-12+13+14+15;1+2+3+4-5-6-7-8+9-10-11+12-13+14+15;1+2+3+4-5-6-7-8-9+10+11-12-13+14+15;1+2+3+4-5-6-7-8-9+10-11+12+13-14+15;1+2+3+4-5-6-7-8-9-10+11+12+13+14-15;1+2+3-4 5+6 7+8+9-10-11-12-13-14+15;1+2+3-4 5+6 7+8-9+10-11-12-13+14-15;1+2+3-4 5+6 7+8-9-10+11-12+13-14-15;1+2+3-4 5+6 7-8+9+10-11-12+13-14-15;1+2+3-4 5+6 7-8+9-10+11+12-13-14-15;1+2+3-4 5+6+7+8+9-10-11-12+13+14+15;1+2+3-4 5+6+7+8-9+10-11+12-13+14+15;1+2+3-4 5+6+7+8-9-10+11+12+13-14+15;1+2+3-4 5+6+7-8+9+10+11-12-13+14+15;1+2+3-4 5+6+7-8+9+10-11+12+13-14+15;1+2+3-4 5+6+7-8+9-10+11+12+13+14-15;1+2+3-4 5+6-7+8 9-10-11-12+13-14-15;1+2+3-4 5+6-7+8+9+10+11-12+13-14+15;1+2+3-4 5+6-7+8+9+10-11+12+13+14-15;1+2+3-4 5-6 7+8 9+10+11+12+13-14-15;1+2+3-4 5-6+7+8 9-10-11+12-13-14-15;1+2+3-4 5-6+7+8+9+10+11+12-13-14+15;1+2+3-4 5-6+7+8+9+10+11-12+13+14-15;1+2+3-4 5-6+7-8-9-10+11+12+13+14+15;1+2+3-4 5-6-7+8-9+10-11+12+13+14+15;1+2+3-4 5-6-7-8+9+10+11-12+13+14+15;1+2+3-4+5 6-7 8-9+10-11-12+13+14+15;1+2+3-4+5 6-7 8-9-10+11+12-13+14+15;1+2+3-4+5+6 7-8+9-10-11-12-13-14-15;1+2+3-4+5+6+7+8-9-10-11-12+13-14+15;1+2+3-4+5+6+7-8+9-10-11+12-13-14+15;1+2+3-4+5+6+7-8+9-10-11-12+13+14-15;1+2+3-4+5+6+7-8-9+10+11-12-13-14+15;1+2+3-4+5+6+7-8-9+10-11+12-13+14-15;1+2+3-4+5+6+7-8-9-10+11+12+13-14-15;1+2+3-4+5+6-7+8+9-10+11-12-13-14+15;1+2+3-4+5+6-7+8+9-10-11+12-13+14-15;1+2+3-4+5+6-7+8-9+10+11-12-13+14-15;1+2+3-4+5+6-7+8-9+10-11+12+13-14-15;1+2+3-4+5+6-7-8+9+10+11-12+13-14-15;1+2+3-4+5-6 7+8 9+10-11-12+13-14-15;1+2+3-4+5-6 7+8 9-10+11+12-13-14-15;1+2+3-4+5-6+7+8+9+10-11-12-13-14+15;1+2+3-4+5-6+7+8+9-10+11-12-13+14-15;1+2+3-4+5-6+7+8+9-10-11+12+13-14-15;1+2+3-4+5-6+7+8-9+10+11-12+13-14-15;1+2+3-4+5-6+7-8+9+10+11+12-13-14-15;1+2+3-4+5-6+7-8-9-10-11-12+13+14+15;1+2+3-4+5-6-7+8-9-10-11+12-13+14+15;1+2+3-4+5-6-7-8+9-10+11-12-13+14+15;1+2+3-4+5-6-7-8+9-10-11+12+13-14+15;1+2+3-4+5-6-7-8-9+10+11-12+13-14+15;1+2+3-4+5-6-7-8-9+10-11+12+13+14-15;1+2+3-4-5 6+7 8+9+10+11-12-13-14-15;1+2+3-4-5+6 7-8 9+10+11-12-13+14+15;1+2+3-4-5+6 7-8 9+10-11+12+13-14+15;1+2+3-4-5+6 7-8 9-10+11+12+13+14-15;1+2+3-4-5+6 7-8-9-10-11-12-13+14-15;1+2+3-4-5+6+7+8+9+10-11-12-13+14-15;1+2+3-4-5+6+7+8+9-10+11-12+13-14-15;1+2+3-4-5+6+7+8-9+10+11+12-13-14-15;1+2+3-4-5+6+7-8-9-10-11+12-13+14+15;1+2+3-4-5+6-7+8-9-10+11-12-13+14+15;1+2+3-4-5+6-7+8-9-10-11+12+13-14+15;1+2+3-4-5+6-7-8+9+10-11-12-13+14+15;1+2+3-4-5+6-7-8+9-10+11-12+13-14+15;1+2+3-4-5+6-7-8+9-10-11+12+13+14-15;1+2+3-4-5+6-7-8-9+10+11+12-13-14+15;1+2+3-4-5+6-7-8-9+10+11-12+13+14-15;1+2+3-4-5-6 7+8 9-10-11-12+13-14+15;1+2+3-4-5-6 7+8+9+10-11+12+13+14+15;1+2+3-4-5-6+7+8-9+10-11-12-13+14+15;1+2+3-4-5-6+7+8-9-10+11-12+13-14+15;1+2+3-4-5-6+7+8-9-10-11+12+13+14-15;1+2+3-4-5-6+7-8+9+10-11-12+13-14+15;1+2+3-4-5-6+7-8+9-10+11+12-13-14+15;1+2+3-4-5-6+7-8+9-10+11-12+13+14-15;1+2+3-4-5-6+7-8-9+10+11+12-13+14-15;1+2+3-4-5-6-7+8+9+10-11+12-13-14+15;1+2+3-4-5-6-7+8+9+10-11-12+13+14-15;1+2+3-4-5-6-7+8+9-10+11+12-13+14-15;1+2+3-4-5-6-7+8-9+10+11+12+13-14-15;1+2+3-4-5-6-7-8-9-10-11+12+13+14+15;1+2-3 4 5+6 7+8-9-10 11-12+13 14-15;1+2-3 4 5+6+7+8-9 10-11+12 13+14+15;1+2-3 4 5+6+7+8-9-10 11+12+13 14+15;1+2-3 4 5+6-7 8+9-10 11-12+13+14 15;1+2-3 4 5+6-7+8+9+10-11 12+13+14 15;1+2-3 4 5-6 7 8+9+10 11+12-13-14+15;1+2-3 4 5-6 7 8+9+10 11-12+13+14-15;1+2-3 4+5 6+7+8+9-10-11-12+13-14-15;1+2-3 4+5 6+7+8-9+10-11+12-13-14-15;1+2-3 4+5 6+7-8+9+10+11-12-13-14-15;1+2-3 4+5 6-7+8-9-10-11-12-13+14+15;1+2-3 4+5 6-7-8+9-10-11-12+13-14+15;1+2-3 4+5 6-7-8-9+10-11+12-13-14+15;1+2-3 4+5 6-7-8-9+10-11-12+13+14-15;1+2-3 4+5 6-7-8-9-10+11+12-13+14-15;1+2-3 4+5+6+7 8-9-10-11-12+13-14-15;1+2-3 4+5+6-7 8 9+10 11+12 13-14 15;1+2-3 4+5-6+7 8+9+10-11-12-13-14-15;1+2-3 4-5 6+7+8 9+10+11+12-13-14-15;1+2-3 4-5 6-7+8 9-10+11-12-13+14+15;1+2-3 4-5 6-7+8 9-10-11+12+13-14+15;1+2-3 4-5-6+7 8+9-10-11-12-13-14+15;1+2-3 4-5-6+7 8-9+10-11-12-13+14-15;1+2-3 4-5-6+7 8-9-10+11-12+13-14-15;1+2-3+4 5+6 7 8-9 10-11 12+13 14-15;1+2-3+4 5+6 7+8 9+10 11-12 13-14+15;1+2-3+4 5+6 7+8 9-10+11+12 13-14 15;1+2-3+4 5+6 7-8 9-10+11-12-13-14+15;1+2-3+4 5+6 7-8 9-10-11+12-13+14-15;1+2-3+4 5+6+7+8+9-10-11-12-13-14-15;1+2-3+4 5+6+7-8 9-10+11-12+13+14+15;1+2-3+4 5+6-7-8 9+10+11+12+13+14-15;1+2-3+4 5+6-7-8+9-10-11-12-13-14+15;1+2-3+4 5+6-7-8-9+10-11-12-13+14-15;1+2-3+4 5+6-7-8-9-10+11-12+13-14-15;1+2-3+4 5-6 7+8+9-10+11-12-13+14+15;1+2-3+4 5-6 7+8+9-10-11+12+13-14+15;1+2-3+4 5-6 7+8-9+10+11-12+13-14+15;1+2-3+4 5-6 7+8-9+10-11+12+13+14-15;1+2-3+4 5-6 7-8+9+10+11+12-13-14+15;1+2-3+4 5-6 7-8+9+10+11-12+13+14-15;1+2-3+4 5-6+7+8-9-10-11-12-13-14+15;1+2-3+4 5-6+7-8+9-10-11-12-13+14-15;1+2-3+4 5-6+7-8-9+10-11-12+13-14-15;1+2-3+4 5-6+7-8-9-10+11+12-13-14-15;1+2-3+4 5-6-7+8+9-10-11-12+13-14-15;1+2-3+4 5-6-7+8-9+10-11+12-13-14-15;1+2-3+4 5-6-7-8+9+10+11-12-13-14-15;1+2-3+4+5 6-7 8+9-10-11-12+13+14+15;1+2-3+4+5 6-7 8-9+10-11+12-13+14+15;1+2-3+4+5 6-7 8-9-10+11+12+13-14+15;1+2-3+4+5+6 7+8-9-10-11-12-13-14-15;1+2-3+4+5+6+7+8-9-10-11+12-13-14+15;1+2-3+4+5+6+7+8-9-10-11-12+13+14-15;1+2-3+4+5+6+7-8+9-10+11-12-13-14+15;1+2-3+4+5+6+7-8+9-10-11+12-13+14-15;1+2-3+4+5+6+7-8-9+10+11-12-13+14-15;1+2-3+4+5+6+7-8-9+10-11+12+13-14-15;1+2-3+4+5+6-7+8+9+10-11-12-13-14+15;1+2-3+4+5+6-7+8+9-10+11-12-13+14-15;1+2-3+4+5+6-7+8+9-10-11+12+13-14-15;1+2-3+4+5+6-7+8-9+10+11-12+13-14-15;1+2-3+4+5+6-7-8+9+10+11+12-13-14-15;1+2-3+4+5+6-7-8-9-10-11-12+13+14+15;1+2-3+4+5-6 7+8 9+10-11+12-13-14-15;1+2-3+4+5-6 7-8-9+10+11+12+13+14+15;1+2-3+4+5-6+7+8+9+10-11-12-13+14-15;1+2-3+4+5-6+7+8+9-10+11-12+13-14-15;1+2-3+4+5-6+7+8-9+10+11+12-13-14-15;1+2-3+4+5-6+7-8-9-10-11+12-13+14+15;1+2-3+4+5-6-7+8-9-10+11-12-13+14+15;1+2-3+4+5-6-7+8-9-10-11+12+13-14+15;1+2-3+4+5-6-7-8+9+10-11-12-13+14+15;1+2-3+4+5-6-7-8+9-10+11-12+13-14+15;1+2-3+4+5-6-7-8+9-10-11+12+13+14-15;1+2-3+4+5-6-7-8-9+10+11+12-13-14+15;1+2-3+4+5-6-7-8-9+10+11-12+13+14-15;1+2-3+4-5 6+7 8-9-10-11-12-13+14+15;1+2-3+4-5+6 7-8 9+10+11-12+13-14+15;1+2-3+4-5+6 7-8 9+10-11+12+13+14-15;1+2-3+4-5+6 7-8-9-10-11-12+13-14-15;1+2-3+4-5+6+7+8+9+10-11-12+13-14-15;1+2-3+4-5+6+7+8+9-10+11+12-13-14-15;1+2-3+4-5+6+7-8-9-10+11-12-13+14+15;1+2-3+4-5+6+7-8-9-10-11+12+13-14+15;1+2-3+4-5+6-7+8-9+10-11-12-13+14+15;1+2-3+4-5+6-7+8-9-10+11-12+13-14+15;1+2-3+4-5+6-7+8-9-10-11+12+13+14-15;1+2-3+4-5+6-7-8+9+10-11-12+13-14+15;1+2-3+4-5+6-7-8+9-10+11+12-13-14+15;1+2-3+4-5+6-7-8+9-10+11-12+13+14-15;1+2-3+4-5+6-7-8-9+10+11+12-13+14-15;1+2-3+4-5-6 7+8 9-10-11+12-13-14+15;1+2-3+4-5-6 7+8 9-10-11-12+13+14-15;1+2-3+4-5-6 7+8+9+10+11-12+13+14+15;1+2-3+4-5-6+7+8+9-10-11-12-13+14+15;1+2-3+4-5-6+7+8-9+10-11-12+13-14+15;1+2-3+4-5-6+7+8-9-10+11+12-13-14+15;1+2-3+4-5-6+7+8-9-10+11-12+13+14-15;1+2-3+4-5-6+7-8+9+10-11+12-13-14+15;1+2-3+4-5-6+7-8+9+10-11-12+13+14-15;1+2-3+4-5-6+7-8+9-10+11+12-13+14-15;1+2-3+4-5-6+7-8-9+10+11+12+13-14-15;1+2-3+4-5-6-7+8 9-10-11-12-13-14-15;1+2-3+4-5-6-7+8+9+10+11-12-13-14+15;1+2-3+4-5-6-7+8+9+10-11+12-13+14-15;1+2-3+4-5-6-7+8+9-10+11+12+13-14-15;1+2-3+4-5-6-7-8-9-10+11-12+13+14+15;1+2-3-4 5+6 7+8-9-10-11+12-13-14+15;1+2-3-4 5+6 7+8-9-10-11-12+13+14-15;1+2-3-4 5+6 7-8+9-10+11-12-13-14+15;1+2-3-4 5+6 7-8+9-10-11+12-13+14-15;1+2-3-4 5+6 7-8-9+10+11-12-13+14-15;1+2-3-4 5+6 7-8-9+10-11+12+13-14-15;1+2-3-4 5+6+7+8-9-10-11+12+13+14+15;1+2-3-4 5+6+7-8+9-10+11-12+13+14+15;1+2-3-4 5+6+7-8-9+10+11+12-13+14+15;1+2-3-4 5+6-7+8+9+10-11-12+13+14+15;1+2-3-4 5+6-7+8+9-10+11+12-13+14+15;1+2-3-4 5+6-7+8-9+10+11+12+13-14+15;1+2-3-4 5+6-7-8+9+10+11+12+13+14-15;1+2-3-4 5-6 7 8+9 10+11 12-13 14+15;1+2-3-4 5-6 7+8 9+10+11-12+13-14+15;1+2-3-4 5-6 7+8 9+10-11+12+13+14-15;1+2-3-4 5-6 7-8 9+10-11-12 13+14 15;1+2-3-4 5-6 7-8 9-10 11+12 13+14-15;1+2-3-4 5-6+7+8 9-10-11-12-13-14+15;1+2-3-4 5-6+7+8+9+10-11+12-13+14+15;1+2-3-4 5-6+7+8+9-10+11+12+13-14+15;1+2-3-4 5-6+7+8-9+10+11+12+13+14-15;1+2-3-4 5-6-7+8 9+10-11+12-13-14-15;1+2-3-4 5-6-7-8-9+10+11+12+13+14+15;1+2-3-4+5 6-7 8+9+10+11+12+13-14-15;1+2-3-4+5+6 7-8 9+10+11+12-13-14+15;1+2-3-4+5+6 7-8 9+10+11-12+13+14-15;1+2-3-4+5+6 7-8-9-10-11+12-13-14-15;1+2-3-4+5+6+7+8+9+10-11+12-13-14-15;1+2-3-4+5+6+7-8 9+10+11+12+13+14+15;1+2-3-4+5+6+7-8-9+10-11-12-13+14+15;1+2-3-4+5+6+7-8-9-10+11-12+13-14+15;1+2-3-4+5+6+7-8-9-10-11+12+13+14-15;1+2-3-4+5+6-7+8+9-10-11-12-13+14+15;1+2-3-4+5+6-7+8-9+10-11-12+13-14+15;1+2-3-4+5+6-7+8-9-10+11+12-13-14+15;1+2-3-4+5+6-7+8-9-10+11-12+13+14-15;1+2-3-4+5+6-7-8+9+10-11+12-13-14+15;1+2-3-4+5+6-7-8+9+10-11-12+13+14-15;1+2-3-4+5+6-7-8+9-10+11+12-13+14-15;1+2-3-4+5+6-7-8-9+10+11+12+13-14-15;1+2-3-4+5-6 7+8 9-10+11-12-13-14+15;1+2-3-4+5-6 7+8 9-10-11+12-13+14-15;1+2-3-4+5-6 7+8+9+10+11+12-13+14+15;1+2-3-4+5-6+7+8+9-10-11-12+13-14+15;1+2-3-4+5-6+7+8-9+10-11+12-13-14+15;1+2-3-4+5-6+7+8-9+10-11-12+13+14-15;1+2-3-4+5-6+7+8-9-10+11+12-13+14-15;1+2-3-4+5-6+7-8+9+10+11-12-13-14+15;1+2-3-4+5-6+7-8+9+10-11+12-13+14-15;1+2-3-4+5-6+7-8+9-10+11+12+13-14-15;1+2-3-4+5-6-7+8+9+10+11-12-13+14-15;1+2-3-4+5-6-7+8+9+10-11+12+13-14-15;1+2-3-4+5-6-7-8-9+10-11-12+13+14+15;1+2-3-4+5-6-7-8-9-10+11+12-13+14+15;1+2-3-4-5 6+7 8+9+10-11-12-13+14-15;1+2-3-4-5 6+7 8+9-10+11-12+13-14-15;1+2-3-4-5 6+7 8-9+10+11+12-13-14-15;1+2-3-4-5+6 7+8+9-10-11-12-13-14-15;1+2-3-4-5+6 7-8 9-10+11-12+13+14+15;1+2-3-4-5+6+7+8+9-10-11+12-13-14+15;1+2-3-4-5+6+7+8+9-10-11-12+13+14-15;1+2-3-4-5+6+7+8-9+10+11-12-13-14+15;1+2-3-4-5+6+7+8-9+10-11+12-13+14-15;1+2-3-4-5+6+7+8-9-10+11+12+13-14-15;1+2-3-4-5+6+7-8+9+10+11-12-13+14-15;1+2-3-4-5+6+7-8+9+10-11+12+13-14-15;1+2-3-4-5+6-7+8+9+10+11-12+13-14-15;1+2-3-4-5+6-7-8+9-10-11-12+13+14+15;1+2-3-4-5+6-7-8-9+10-11+12-13+14+15;1+2-3-4-5+6-7-8-9-10+11+12+13-14+15;1+2-3-4-5-6 7-8+9+10+11+12+13+14+15;1+2-3-4-5-6+7+8+9+10+11+12-13-14-15;1+2-3-4-5-6+7+8-9-10-11-12+13+14+15;1+2-3-4-5-6+7-8+9-10-11+12-13+14+15;1+2-3-4-5-6+7-8-9+10+11-12-13+14+15;1+2-3-4-5-6+7-8-9+10-11+12+13-14+15;1+2-3-4-5-6+7-8-9-10+11+12+13+14-15;1+2-3-4-5-6-7+8+9-10+11-12-13+14+15;1+2-3-4-5-6-7+8+9-10-11+12+13-14+15;1+2-3-4-5-6-7+8-9+10+11-12+13-14+15;1+2-3-4-5-6-7+8-9+10-11+12+13+14-15;1+2-3-4-5-6-7-8+9+10+11+12-13-14+15;1+2-3-4-5-6-7-8+9+10+11-12+13+14-15;1-2 3 4 5+6+7-8+9 10-11+12+13+14 15;1-2 3 4 5+6-7 8-9+10 11+12-13+14 15;1-2 3 4 5+6-7+8+9 10+11-12+13+14 15;1-2 3 4 5+6-7+8+9+10 11-12+13 14+15;1-2 3 4 5-6 7+8 9+10 11+12+13 14-15;1-2 3 4 5-6+7+8+9 10+11+12-13+14 15;1-2 3 4+5 6-7-8-9+10-11-12 13+14 15;1-2 3 4+5 6-7-8-9-10 11+12 13+14-15;1-2 3 4+5+6-7 8 9+10 11+12-13-14+15;1-2 3 4+5+6-7 8 9+10 11-12+13+14-15;1-2 3 4+5-6-7-8 9-10 11+12+13 14+15;1-2 3 4-5 6-7+8 9-10-11 12+13 14+15;1-2 3 4-5-6 7+8-9-10 11-12+13 14+15;1-2 3 4-5-6 7+8-9-10-11 12+13+14 15;1-2 3 4-5-6 7-8-9 10+11+12 13+14-15;1-2 3 4-5-6+7-8 9+10-11 12+13+14 15;1-2 3+4 5+6 7-8 9+10-11+12-13-14+15;1-2 3+4 5+6 7-8 9+10-11-12+13+14-15;1-2 3+4 5+6 7-8 9-10+11+12-13+14-15;1-2 3+4 5+6+7+8+9-10+11-12-13-14-15;1-2 3+4 5+6+7-8 9+10-11+12+13+14+15;1-2 3+4 5+6+7-8-9-10-11-12+13-14+15;1-2 3+4 5+6-7+8-9-10-11+12-13-14+15;1-2 3+4 5+6-7+8-9-10-11-12+13+14-15;1-2 3+4 5+6-7-8+9-10+11-12-13-14+15;1-2 3+4 5+6-7-8+9-10-11+12-13+14-15;1-2 3+4 5+6-7-8-9+10+11-12-13+14-15;1-2 3+4 5+6-7-8-9+10-11+12+13-14-15;1-2 3+4 5-6 7+8 9-10-11-12-13-14+15;1-2 3+4 5-6 7+8+9+10-11+12-13+14+15;1-2 3+4 5-6 7+8+9-10+11+12+13-14+15;1-2 3+4 5-6 7+8-9+10+11+12+13+14-15;1-2 3+4 5-6+7+8-9-10+11-12-13-14+15;1-2 3+4 5-6+7+8-9-10-11+12-13+14-15;1-2 3+4 5-6+7-8+9+10-11-12-13-14+15;1-2 3+4 5-6+7-8+9-10+11-12-13+14-15;1-2 3+4 5-6+7-8+9-10-11+12+13-14-15;1-2 3+4 5-6+7-8-9+10+11-12+13-14-15;1-2 3+4 5-6-7+8+9+10-11-12-13+14-15;1-2 3+4 5-6-7+8+9-10+11-12+13-14-15;1-2 3+4 5-6-7+8-9+10+11+12-13-14-15;1-2 3+4 5-6-7-8-9-10-11+12-13+14+15;1-2 3+4+5 6 7-8+9 10-11-12-13-14 15;1-2 3+4+5 6-7 8+9-10+11-12+13+14+15;1-2 3+4+5 6-7 8-9+10+11+12-13+14+15;1-2 3+4+5+6 7+8-9-10+11-12-13-14-15;1-2 3+4+5+6 7-8+9+10-11-12-13-14-15;1-2 3+4+5+6+7+8+9-10-11-12-13+14+15;1-2 3+4+5+6+7+8-9+10-11-12+13-14+15;1-2 3+4+5+6+7+8-9-10+11+12-13-14+15;1-2 3+4+5+6+7+8-9-10+11-12+13+14-15;1-2 3+4+5+6+7-8+9+10-11+12-13-14+15;1-2 3+4+5+6+7-8+9+10-11-12+13+14-15;1-2 3+4+5+6+7-8+9-10+11+12-13+14-15;1-2 3+4+5+6+7-8-9+10+11+12+13-14-15;1-2 3+4+5+6-7+8 9-10-11-12-13-14-15;1-2 3+4+5+6-7+8+9+10+11-12-13-14+15;1-2 3+4+5+6-7+8+9+10-11+12-13+14-15;1-2 3+4+5+6-7+8+9-10+11+12+13-14-15;1-2 3+4+5+6-7-8-9-10+11-12+13+14+15;1-2 3+4+5-6 7+8 9+10+11+12-13-14-15;1-2 3+4+5-6+7+8+9+10+11-12-13+14-15;1-2 3+4+5-6+7+8+9+10-11+12+13-14-15;1-2 3+4+5-6+7-8-9+10-11-12+13+14+15;1-2 3+4+5-6+7-8-9-10+11+12-13+14+15;1-2 3+4+5-6-7+8+9-10-11-12+13+14+15;1-2 3+4+5-6-7+8-9+10-11+12-13+14+15;1-2 3+4+5-6-7+8-9-10+11+12+13-14+15;1-2 3+4+5-6-7-8+9+10+11-12-13+14+15;1-2 3+4+5-6-7-8+9+10-11+12+13-14+15;1-2 3+4+5-6-7-8+9-10+11+12+13+14-15;1-2 3+4-5 6+7 8-9-10+11-12-13+14+15;1-2 3+4-5 6+7 8-9-10-11+12+13-14+15;1-2 3+4-5+6 7-8 9+10+11+12+13+14-15;1-2 3+4-5+6 7-8+9-10-11-12-13-14+15;1-2 3+4-5+6 7-8-9+10-11-12-13+14-15;1-2 3+4-5+6 7-8-9-10+11-12+13-14-15;1-2 3+4-5+6+7+8+9+10+11-12+13-14-15;1-2 3+4-5+6+7-8+9-10-11-12+13+14+15;1-2 3+4-5+6+7-8-9+10-11+12-13+14+15;1-2 3+4-5+6+7-8-9-10+11+12+13-14+15;1-2 3+4-5+6-7+8+9-10-11+12-13+14+15;1-2 3+4-5+6-7+8-9+10+11-12-13+14+15;1-2 3+4-5+6-7+8-9+10-11+12+13-14+15;1-2 3+4-5+6-7+8-9-10+11+12+13+14-15;1-2 3+4-5+6-7-8+9+10+11-12+13-14+15;1-2 3+4-5+6-7-8+9+10-11+12+13+14-15;1-2 3+4-5-6 7+8 9+10-11-12+13-14+15;1-2 3+4-5-6 7+8 9-10+11+12-13-14+15;1-2 3+4-5-6 7+8 9-10+11-12+13+14-15;1-2 3+4-5-6+7+8+9-10+11-12-13+14+15;1-2 3+4-5-6+7+8+9-10-11+12+13-14+15;1-2 3+4-5-6+7+8-9+10+11-12+13-14+15;1-2 3+4-5-6+7+8-9+10-11+12+13+14-15;1-2 3+4-5-6+7-8+9+10+11+12-13-14+15;1-2 3+4-5-6+7-8+9+10+11-12+13+14-15;1-2 3+4-5-6-7+8 9-10+11-12-13-14-15;1-2 3+4-5-6-7+8+9+10+11+12-13+14-15;1-2 3+4-5-6-7-8-9+10-11+12+13+14+15;1-2 3-4 5 6-7-8-9 10-11+12-13+14 15;1-2 3-4 5+6 7+8+9-10-11-12-13+14+15;1-2 3-4 5+6 7+8-9+10-11-12+13-14+15;1-2 3-4 5+6 7+8-9-10+11+12-13-14+15;1-2 3-4 5+6 7+8-9-10+11-12+13+14-15;1-2 3-4 5+6 7-8+9+10-11+12-13-14+15;1-2 3-4 5+6 7-8+9+10-11-12+13+14-15;1-2 3-4 5+6 7-8+9-10+11+12-13+14-15;1-2 3-4 5+6 7-8-9+10+11+12+13-14-15;1-2 3-4 5+6+7+8-9-10+11+12+13+14+15;1-2 3-4 5+6+7-8+9+10-11+12+13+14+15;1-2 3-4 5+6-7+8 9-10-11+12-13-14+15;1-2 3-4 5+6-7+8 9-10-11-12+13+14-15;1-2 3-4 5+6-7+8+9+10+11-12+13+14+15;1-2 3-4 5-6 7+8 9+10+11+12+13+14-15;1-2 3-4 5-6 7-8 9+10+11-12 13+14 15;1-2 3-4 5-6+7+8 9-10+11-12-13-14+15;1-2 3-4 5-6+7+8 9-10-11+12-13+14-15;1-2 3-4 5-6+7+8+9+10+11+12-13+14+15;1-2 3-4 5-6-7+8 9+10+11+12-13-14-15;1-2 3-4+5+6 7+8-9-10-11-12-13-14+15;1-2 3-4+5+6 7-8+9-10-11-12-13+14-15;1-2 3-4+5+6 7-8-9+10-11-12+13-14-15;1-2 3-4+5+6 7-8-9-10+11+12-13-14-15;1-2 3-4+5+6+7+8+9+10+11+12-13-14-15;1-2 3-4+5+6+7+8-9-10-11-12+13+14+15;1-2 3-4+5+6+7-8+9-10-11+12-13+14+15;1-2 3-4+5+6+7-8-9+10+11-12-13+14+15;1-2 3-4+5+6+7-8-9+10-11+12+13-14+15;1-2 3-4+5+6+7-8-9-10+11+12+13+14-15;1-2 3-4+5+6-7+8+9-10+11-12-13+14+15;1-2 3-4+5+6-7+8+9-10-11+12+13-14+15;1-2 3-4+5+6-7+8-9+10+11-12+13-14+15;1-2 3-4+5+6-7+8-9+10-11+12+13+14-15;1-2 3-4+5+6-7-8+9+10+11+12-13-14+15;1-2 3-4+5+6-7-8+9+10+11-12+13+14-15;1-2 3-4+5-6 7+8 9+10-11+12-13-14+15;1-2 3-4+5-6 7+8 9+10-11-12+13+14-15;1-2 3-4+5-6 7+8 9-10+11+12-13+14-15;1-2 3-4+5-6 7-8 9-10-11 12+13 14-15;1-2 3-4+5-6+7+8+9+10-11-12-13+14+15;1-2 3-4+5-6+7+8+9-10+11-12+13-14+15;1-2 3-4+5-6+7+8+9-10-11+12+13+14-15;1-2 3-4+5-6+7+8-9+10+11+12-13-14+15;1-2 3-4+5-6+7+8-9+10+11-12+13+14-15;1-2 3-4+5-6+7-8+9+10+11+12-13+14-15;1-2 3-4+5-6-7+8 9+10-11-12-13-14-15;1-2 3-4+5-6-7+8+9+10+11+12+13-14-15;1-2 3-4+5-6-7-8+9-10-11+12+13+14+15;1-2 3-4+5-6-7-8-9+10+11-12+13+14+15;1-2 3-4-5 6+7 8+9+10+11-12-13+14-15;1-2 3-4-5 6+7 8+9+10-11+12+13-14-15;1-2 3-4-5 6-7 8-9 10+11 12-13-14-15;1-2 3-4-5+6 7+8+9-10+11-12-13-14-15;1-2 3-4-5+6 7-8 9+10-11+12+13+14+15;1-2 3-4-5+6 7-8-9-10-11-12+13-14+15;1-2 3-4-5+6+7+8+9+10-11-12+13-14+15;1-2 3-4-5+6+7+8+9-10+11+12-13-14+15;1-2 3-4-5+6+7+8+9-10+11-12+13+14-15;1-2 3-4-5+6+7+8-9+10+11+12-13+14-15;1-2 3-4-5+6+7-8+9+10+11+12+13-14-15;1-2 3-4-5+6-7+8-9-10-11+12+13+14+15;1-2 3-4-5+6-7-8+9-10+11-12+13+14+15;1-2 3-4-5+6-7-8-9+10+11+12-13+14+15;1-2 3-4-5-6 7+8 9-10-11-12+13+14+15;1-2 3-4-5-6+7+8-9-10+11-12+13+14+15;1-2 3-4-5-6+7-8+9+10-11-12+13+14+15;1-2 3-4-5-6+7-8+9-10+11+12-13+14+15;1-2 3-4-5-6+7-8-9+10+11+12+13-14+15;1-2 3-4-5-6-7+8 9-10-11-12-13-14+15;1-2 3-4-5-6-7+8+9+10-11+12-13+14+15;1-2 3-4-5-6-7+8+9-10+11+12+13-14+15;1-2 3-4-5-6-7+8-9+10+11+12+13+14-15;1-2+3 4 5+6 7 8-9-10 11+12-13+14-15;1-2+3 4 5+6-7-8-9-10+11 12-13-14 15;1-2+3 4 5-6 7+8+9 10-11-12 13+14+15;1-2+3 4 5-6 7+8-9+10 11+12-13 14+15;1-2+3 4 5-6-7+8-9+10 11-12-13 14-15;1-2+3 4+5 6+7-8 9+10+11-12+13-14-15;1-2+3 4+5 6-7-8 9-10-11+12-13+14+15;1-2+3 4+5+6-7 8+9+10+11-12-13+14+15;1-2+3 4+5+6-7 8+9+10-11+12+13-14+15;1-2+3 4+5+6-7 8+9-10+11+12+13+14-15;1-2+3 4+5-6-7 8-9-10+11+12+13+14+15;1-2+3 4-5 6+7+8+9+10-11+12-13-14+15;1-2+3 4-5 6+7+8+9+10-11-12+13+14-15;1-2+3 4-5 6+7+8+9-10+11+12-13+14-15;1-2+3 4-5 6+7+8-9+10+11+12+13-14-15;1-2+3 4-5 6+7-8-9-10-11+12+13+14+15;1-2+3 4-5 6-7+8-9-10+11-12+13+14+15;1-2+3 4-5 6-7-8+9+10-11-12+13+14+15;1-2+3 4-5 6-7-8+9-10+11+12-13+14+15;1-2+3 4-5 6-7-8-9+10+11+12+13-14+15;1-2+3 4-5+6-7 8-9+10-11+12+13+14+15;1-2+3 4-5-6-7 8+9+10+11+12+13-14+15;1-2+3+4 5+6 7+8 9+10 11-12 13+14-15;1-2+3+4 5+6 7+8 9+10-11+12 13-14 15;1-2+3+4 5+6 7-8 9+10-11-12-13-14+15;1-2+3+4 5+6 7-8 9-10+11-12-13+14-15;1-2+3+4 5+6 7-8 9-10-11+12+13-14-15;1-2+3+4 5+6+7-8 9+10-11-12+13+14+15;1-2+3+4 5+6+7-8 9-10+11+12-13+14+15;1-2+3+4 5+6-7+8-9-10-11-12-13-14+15;1-2+3+4 5+6-7-8+9-10-11-12-13+14-15;1-2+3+4 5+6-7-8-9+10-11-12+13-14-15;1-2+3+4 5+6-7-8-9-10+11+12-13-14-15;1-2+3+4 5-6 7+8+9+10-11-12-13+14+15;1-2+3+4 5-6 7+8+9-10+11-12+13-14+15;1-2+3+4 5-6 7+8+9-10-11+12+13+14-15;1-2+3+4 5-6 7+8-9+10+11+12-13-14+15;1-2+3+4 5-6 7+8-9+10+11-12+13+14-15;1-2+3+4 5-6 7-8+9+10+11+12-13+14-15;1-2+3+4 5-6+7+8-9-10-11-12-13+14-15;1-2+3+4 5-6+7-8+9-10-11-12+13-14-15;1-2+3+4 5-6+7-8-9+10-11+12-13-14-15;1-2+3+4 5-6-7+8+9-10-11+12-13-14-15;1-2+3+4 5-6-7+8-9+10+11-12-13-14-15;1-2+3+4 5-6-7-8 9-10+11+12+13+14+15;1-2+3+4 5-6-7-8-9-10-11-12-13+14+15;1-2+3+4+5 6-7 8+9-10-11+12-13+14+15;1-2+3+4+5 6-7 8-9+10+11-12-13+14+15;1-2+3+4+5 6-7 8-9+10-11+12+13-14+15;1-2+3+4+5 6-7 8-9-10+11+12+13+14-15;1-2+3+4+5+6+7+8-9-10+11-12-13-14+15;1-2+3+4+5+6+7+8-9-10-11+12-13+14-15;1-2+3+4+5+6+7-8+9+10-11-12-13-14+15;1-2+3+4+5+6+7-8+9-10+11-12-13+14-15;1-2+3+4+5+6+7-8+9-10-11+12+13-14-15;1-2+3+4+5+6+7-8-9+10+11-12+13-14-15;1-2+3+4+5+6-7+8+9+10-11-12-13+14-15;1-2+3+4+5+6-7+8+9-10+11-12+13-14-15;1-2+3+4+5+6-7+8-9+10+11+12-13-14-15;1-2+3+4+5+6-7-8-9-10-11+12-13+14+15;1-2+3+4+5-6 7+8 9+10+11-12-13-14-15;1-2+3+4+5-6 7-8+9-10+11+12+13+14+15;1-2+3+4+5-6+7+8+9+10-11-12+13-14-15;1-2+3+4+5-6+7+8+9-10+11+12-13-14-15;1-2+3+4+5-6+7-8-9-10+11-12-13+14+15;1-2+3+4+5-6+7-8-9-10-11+12+13-14+15;1-2+3+4+5-6-7+8-9+10-11-12-13+14+15;1-2+3+4+5-6-7+8-9-10+11-12+13-14+15;1-2+3+4+5-6-7+8-9-10-11+12+13+14-15;1-2+3+4+5-6-7-8+9+10-11-12+13-14+15;1-2+3+4+5-6-7-8+9-10+11+12-13-14+15;1-2+3+4+5-6-7-8+9-10+11-12+13+14-15;1-2+3+4+5-6-7-8-9+10+11+12-13+14-15;1-2+3+4-5 6+7 8-9-10-11-12+13-14+15;1-2+3+4-5+6 7-8 9+10+11+12-13-14+15;1-2+3+4-5+6 7-8 9+10+11-12+13+14-15;1-2+3+4-5+6 7-8-9-10-11+12-13-14-15;1-2+3+4-5+6+7+8+9+10-11+12-13-14-15;1-2+3+4-5+6+7-8 9+10+11+12+13+14+15;1-2+3+4-5+6+7-8-9+10-11-12-13+14+15;1-2+3+4-5+6+7-8-9-10+11-12+13-14+15;1-2+3+4-5+6+7-8-9-10-11+12+13+14-15;1-2+3+4-5+6-7+8+9-10-11-12-13+14+15;1-2+3+4-5+6-7+8-9+10-11-12+13-14+15;1-2+3+4-5+6-7+8-9-10+11+12-13-14+15;1-2+3+4-5+6-7+8-9-10+11-12+13+14-15;1-2+3+4-5+6-7-8+9+10-11+12-13-14+15;1-2+3+4-5+6-7-8+9+10-11-12+13+14-15;1-2+3+4-5+6-7-8+9-10+11+12-13+14-15;1-2+3+4-5+6-7-8-9+10+11+12+13-14-15;1-2+3+4-5-6 7+8 9-10+11-12-13-14+15;1-2+3+4-5-6 7+8 9-10-11+12-13+14-15;1-2+3+4-5-6 7+8+9+10+11+12-13+14+15;1-2+3+4-5-6+7+8+9-10-11-12+13-14+15;1-2+3+4-5-6+7+8-9+10-11+12-13-14+15;1-2+3+4-5-6+7+8-9+10-11-12+13+14-15;1-2+3+4-5-6+7+8-9-10+11+12-13+14-15;1-2+3+4-5-6+7-8+9+10+11-12-13-14+15;1-2+3+4-5-6+7-8+9+10-11+12-13+14-15;1-2+3+4-5-6+7-8+9-10+11+12+13-14-15;1-2+3+4-5-6-7+8+9+10+11-12-13+14-15;1-2+3+4-5-6-7+8+9+10-11+12+13-14-15;1-2+3+4-5-6-7-8-9+10-11-12+13+14+15;1-2+3+4-5-6-7-8-9-10+11+12-13+14+15;1-2+3-4 5 6-7 8 9-10+11+12 13+14+15;1-2+3-4 5 6-7-8-9 10-11-12-13+14 15;1-2+3-4 5+6 7+8-9-10+11-12-13-14+15;1-2+3-4 5+6 7+8-9-10-11+12-13+14-15;1-2+3-4 5+6 7-8+9+10-11-12-13-14+15;1-2+3-4 5+6 7-8+9-10+11-12-13+14-15;1-2+3-4 5+6 7-8+9-10-11+12+13-14-15;1-2+3-4 5+6 7-8-9+10+11-12+13-14-15;1-2+3-4 5+6+7+8-9-10+11-12+13+14+15;1-2+3-4 5+6+7-8+9+10-11-12+13+14+15;1-2+3-4 5+6+7-8+9-10+11+12-13+14+15;1-2+3-4 5+6+7-8-9+10+11+12+13-14+15;1-2+3-4 5+6-7+8 9-10-11-12-13-14+15;1-2+3-4 5+6-7+8+9+10-11+12-13+14+15;1-2+3-4 5+6-7+8+9-10+11+12+13-14+15;1-2+3-4 5+6-7+8-9+10+11+12+13+14-15;1-2+3-4 5-6 7+8 9+10+11+12-13-14+15;1-2+3-4 5-6 7+8 9+10+11-12+13+14-15;1-2+3-4 5-6+7+8 9-10-11-12-13+14-15;1-2+3-4 5-6+7+8+9+10+11-12-13+14+15;1-2+3-4 5-6+7+8+9+10-11+12+13-14+15;1-2+3-4 5-6+7+8+9-10+11+12+13+14-15;1-2+3-4 5-6-7+8 9+10+11-12-13-14-15;1-2+3-4 5-6-7-8+9-10+11+12+13+14+15;1-2+3-4+5 6-7 8-9-10-11+12+13+14+15;1-2+3-4+5+6 7-8 9+10+11+12-13+14-15;1-2+3-4+5+6 7-8-9-10+11-12-13-14-15;1-2+3-4+5+6+7+8+9+10+11-12-13-14-15;1-2+3-4+5+6+7-8+9-10-11-12-13+14+15;1-2+3-4+5+6+7-8-9+10-11-12+13-14+15;1-2+3-4+5+6+7-8-9-10+11+12-13-14+15;1-2+3-4+5+6+7-8-9-10+11-12+13+14-15;1-2+3-4+5+6-7+8+9-10-11-12+13-14+15;1-2+3-4+5+6-7+8-9+10-11+12-13-14+15;1-2+3-4+5+6-7+8-9+10-11-12+13+14-15;1-2+3-4+5+6-7+8-9-10+11+12-13+14-15;1-2+3-4+5+6-7-8+9+10+11-12-13-14+15;1-2+3-4+5+6-7-8+9+10-11+12-13+14-15;1-2+3-4+5+6-7-8+9-10+11+12+13-14-15;1-2+3-4+5-6 7+8 9+10-11-12-13-14+15;1-2+3-4+5-6 7+8 9-10+11-12-13+14-15;1-2+3-4+5-6 7+8 9-10-11+12+13-14-15;1-2+3-4+5-6 7+8+9+10+11+12+13-14+15;1-2+3-4+5-6+7+8+9-10-11+12-13-14+15;1-2+3-4+5-6+7+8+9-10-11-12+13+14-15;1-2+3-4+5-6+7+8-9+10+11-12-13-14+15;1-2+3-4+5-6+7+8-9+10-11+12-13+14-15;1-2+3-4+5-6+7+8-9-10+11+12+13-14-15;1-2+3-4+5-6+7-8+9+10+11-12-13+14-15;1-2+3-4+5-6+7-8+9+10-11+12+13-14-15;1-2+3-4+5-6-7+8+9+10+11-12+13-14-15;1-2+3-4+5-6-7-8+9-10-11-12+13+14+15;1-2+3-4+5-6-7-8-9+10-11+12-13+14+15;1-2+3-4+5-6-7-8-9-10+11+12+13-14+15;1-2+3-4-5 6+7 8+9+10-11-12+13-14-15;1-2+3-4-5 6+7 8+9-10+11+12-13-14-15;1-2+3-4-5+6 7-8 9+10-11-12+13+14+15;1-2+3-4-5+6 7-8 9-10+11+12-13+14+15;1-2+3-4-5+6+7+8+9-10+11-12-13-14+15;1-2+3-4-5+6+7+8+9-10-11+12-13+14-15;1-2+3-4-5+6+7+8-9+10+11-12-13+14-15;1-2+3-4-5+6+7+8-9+10-11+12+13-14-15;1-2+3-4-5+6+7-8+9+10+11-12+13-14-15;1-2+3-4-5+6-7+8+9+10+11+12-13-14-15;1-2+3-4-5+6-7+8-9-10-11-12+13+14+15;1-2+3-4-5+6-7-8+9-10-11+12-13+14+15;1-2+3-4-5+6-7-8-9+10+11-12-13+14+15;1-2+3-4-5+6-7-8-9+10-11+12+13-14+15;1-2+3-4-5+6-7-8-9-10+11+12+13+14-15;1-2+3-4-5-6 7+8-9+10+11+12+13+14+15;1-2+3-4-5-6+7+8-9-10-11+12-13+14+15;1-2+3-4-5-6+7-8+9-10+11-12-13+14+15;1-2+3-4-5-6+7-8+9-10-11+12+13-14+15;1-2+3-4-5-6+7-8-9+10+11-12+13-14+15;1-2+3-4-5-6+7-8-9+10-11+12+13+14-15;1-2+3-4-5-6-7+8+9+10-11-12-13+14+15;1-2+3-4-5-6-7+8+9-10+11-12+13-14+15;1-2+3-4-5-6-7+8+9-10-11+12+13+14-15;1-2+3-4-5-6-7+8-9+10+11+12-13-14+15;1-2+3-4-5-6-7+8-9+10+11-12+13+14-15;1-2+3-4-5-6-7-8+9+10+11+12-13+14-15;1-2-3 4 5+6 7+8-9-10-11 12-13+14 15;1-2-3 4 5+6-7 8-9 10+11-12+13 14+15;1-2-3 4 5+6-7+8+9-10 11+12+13 14+15;1-2-3 4 5-6 7 8+9+10 11-12-13+14+15;1-2-3 4+5 6+7+8+9-10-11-12-13-14+15;1-2-3 4+5 6+7+8-9+10-11-12-13+14-15;1-2-3 4+5 6+7+8-9-10+11-12+13-14-15;1-2-3 4+5 6+7-8+9+10-11-12+13-14-15;1-2-3 4+5 6+7-8+9-10+11+12-13-14-15;1-2-3 4+5 6-7+8+9+10-11+12-13-14-15;1-2-3 4+5 6-7-8 9+10+11+12+13+14+15;1-2-3 4+5 6-7-8-9+10-11-12-13+14+15;1-2-3 4+5 6-7-8-9-10+11-12+13-14+15;1-2-3 4+5 6-7-8-9-10-11+12+13+14-15;1-2-3 4+5+6+7 8-9-10-11-12-13-14+15;1-2-3 4+5-6+7 8+9-10-11+12-13-14-15;1-2-3 4+5-6+7 8-9+10+11-12-13-14-15;1-2-3 4-5 6 7+8+9 10+11 12-13-14 15;1-2-3 4-5 6+7+8 9+10+11-12-13+14-15;1-2-3 4-5 6+7+8 9+10-11+12+13-14-15;1-2-3 4-5 6+7-8 9-10 11+12 13-14-15;1-2-3 4-5 6-7+8 9-10-11-12+13+14+15;1-2-3 4-5+6+7 8+9-10+11-12-13-14-15;1-2-3 4-5-6+7 8-9-10+11-12-13-14+15;1-2-3 4-5-6+7 8-9-10-11+12-13+14-15;1-2-3+4 5 6+7 8 9-10+11-12 13-14-15;1-2-3+4 5+6 7 8-9 10-11-12 13+14 15;1-2-3+4 5+6 7+8 9-10+11 12-13 14+15;1-2-3+4 5+6 7-8 9-10-11-12+13-14+15;1-2-3+4 5+6+7+8-9-10+11-12-13-14-15;1-2-3+4 5+6+7-8+9+10-11-12-13-14-15;1-2-3+4 5+6-7-8 9+10+11+12-13+14+15;1-2-3+4 5+6-7-8-9-10+11-12-13-14+15;1-2-3+4 5+6-7-8-9-10-11+12-13+14-15;1-2-3+4 5-6 7+8+9-10-11-12+13+14+15;1-2-3+4 5-6 7+8-9+10-11+12-13+14+15;1-2-3+4 5-6 7+8-9-10+11+12+13-14+15;1-2-3+4 5-6 7-8+9+10+11-12-13+14+15;1-2-3+4 5-6 7-8+9+10-11+12+13-14+15;1-2-3+4 5-6 7-8+9-10+11+12+13+14-15;1-2-3+4 5-6+7-8 9+10+11+12+13-14+15;1-2-3+4 5-6+7-8-9+10-11-12-13-14+15;1-2-3+4 5-6+7-8-9-10+11-12-13+14-15;1-2-3+4 5-6+7-8-9-10-11+12+13-14-15;1-2-3+4 5-6-7+8+9-10-11-12-13-14+15;1-2-3+4 5-6-7+8-9+10-11-12-13+14-15;1-2-3+4 5-6-7+8-9-10+11-12+13-14-15;1-2-3+4 5-6-7-8+9+10-11-12+13-14-15;1-2-3+4 5-6-7-8+9-10+11+12-13-14-15;1-2-3+4+5 6-7 8-9-10+11-12+13+14+15;1-2-3+4+5+6 7-8 9+10+11+12+13-14-15;1-2-3+4+5+6 7-8-9+10-11-12-13-14-15;1-2-3+4+5+6+7+8-9-10-11-12-13+14+15;1-2-3+4+5+6+7-8+9-10-11-12+13-14+15;1-2-3+4+5+6+7-8-9+10-11+12-13-14+15;1-2-3+4+5+6+7-8-9+10-11-12+13+14-15;1-2-3+4+5+6+7-8-9-10+11+12-13+14-15;1-2-3+4+5+6-7+8+9-10-11+12-13-14+15;1-2-3+4+5+6-7+8+9-10-11-12+13+14-15;1-2-3+4+5+6-7+8-9+10+11-12-13-14+15;1-2-3+4+5+6-7+8-9+10-11+12-13+14-15;1-2-3+4+5+6-7+8-9-10+11+12+13-14-15;1-2-3+4+5+6-7-8+9+10+11-12-13+14-15;1-2-3+4+5+6-7-8+9+10-11+12+13-14-15;1-2-3+4+5-6 7+8 9+10-11-12-13+14-15;1-2-3+4+5-6 7+8 9-10+11-12+13-14-15;1-2-3+4+5-6 7+8+9+10+11+12+13+14-15;1-2-3+4+5-6+7+8+9-10+11-12-13-14+15;1-2-3+4+5-6+7+8+9-10-11+12-13+14-15;1-2-3+4+5-6+7+8-9+10+11-12-13+14-15;1-2-3+4+5-6+7+8-9+10-11+12+13-14-15;1-2-3+4+5-6+7-8+9+10+11-12+13-14-15;1-2-3+4+5-6-7+8+9+10+11+12-13-14-15;1-2-3+4+5-6-7+8-9-10-11-12+13+14+15;1-2-3+4+5-6-7-8+9-10-11+12-13+14+15;1-2-3+4+5-6-7-8-9+10+11-12-13+14+15;1-2-3+4+5-6-7-8-9+10-11+12+13-14+15;1-2-3+4+5-6-7-8-9-10+11+12+13+14-15;1-2-3+4-5 6+7 8+9+10-11+12-13-14-15;1-2-3+4-5+6 7-8 9+10-11+12-13+14+15;1-2-3+4-5+6 7-8 9-10+11+12+13-14+15;1-2-3+4-5+6 7-8-9-10-11-12-13-14+15;1-2-3+4-5+6+7+8+9+10-11-12-13-14+15;1-2-3+4-5+6+7+8+9-10+11-12-13+14-15;1-2-3+4-5+6+7+8+9-10-11+12+13-14-15;1-2-3+4-5+6+7+8-9+10+11-12+13-14-15;1-2-3+4-5+6+7-8+9+10+11+12-13-14-15;1-2-3+4-5+6+7-8-9-10-11-12+13+14+15;1-2-3+4-5+6-7+8-9-10-11+12-13+14+15;1-2-3+4-5+6-7-8+9-10+11-12-13+14+15;1-2-3+4-5+6-7-8+9-10-11+12+13-14+15;1-2-3+4-5+6-7-8-9+10+11-12+13-14+15;1-2-3+4-5+6-7-8-9+10-11+12+13+14-15;1-2-3+4-5-6 7+8 9-10-11-12-13+14+15;1-2-3+4-5-6 7+8+9-10+11+12+13+14+15;1-2-3+4-5-6+7+8-9-10+11-12-13+14+15;1-2-3+4-5-6+7+8-9-10-11+12+13-14+15;1-2-3+4-5-6+7-8+9+10-11-12-13+14+15;1-2-3+4-5-6+7-8+9-10+11-12+13-14+15;1-2-3+4-5-6+7-8+9-10-11+12+13+14-15;1-2-3+4-5-6+7-8-9+10+11+12-13-14+15;1-2-3+4-5-6+7-8-9+10+11-12+13+14-15;1-2-3+4-5-6-7+8+9+10-11-12+13-14+15;1-2-3+4-5-6-7+8+9-10+11+12-13-14+15;1-2-3+4-5-6-7+8+9-10+11-12+13+14-15;1-2-3+4-5-6-7+8-9+10+11+12-13+14-15;1-2-3+4-5-6-7-8+9+10+11+12+13-14-15;1-2-3-4 5+6 7+8-9-10-11-12-13+14+15;1-2-3-4 5+6 7-8+9-10-11-12+13-14+15;1-2-3-4 5+6 7-8-9+10-11+12-13-14+15;1-2-3-4 5+6 7-8-9+10-11-12+13+14-15;1-2-3-4 5+6 7-8-9-10+11+12-13+14-15;1-2-3-4 5+6+7+8 9-10+11-12-13-14-15;1-2-3-4 5+6+7+8+9+10+11+12-13+14-15;1-2-3-4 5+6+7-8-9+10-11+12+13+14+15;1-2-3-4 5+6-7+8+9-10-11+12+13+14+15;1-2-3-4 5+6-7+8-9+10+11-12+13+14+15;1-2-3-4 5+6-7-8+9+10+11+12-13+14+15;1-2-3-4 5-6 7+8 9+10-11+12-13+14+15;1-2-3-4 5-6 7+8 9-10+11+12+13-14+15;1-2-3-4 5-6+7+8+9-10+11-12+13+14+15;1-2-3-4 5-6+7+8-9+10+11+12-13+14+15;1-2-3-4 5-6+7-8+9+10+11+12+13-14+15;1-2-3-4 5-6-7+8 9+10-11-12-13+14-15;1-2-3-4 5-6-7+8 9-10+11-12+13-14-15;1-2-3-4 5-6-7+8+9+10+11+12+13+14-15;1-2-3-4+5 6-7 8+9+10+11+12-13-14+15;1-2-3-4+5 6-7 8+9+10+11-12+13+14-15;1-2-3-4+5+6 7-8 9+10+11-12-13+14+15;1-2-3-4+5+6 7-8 9+10-11+12+13-14+15;1-2-3-4+5+6 7-8 9-10+11+12+13+14-15;1-2-3-4+5+6 7-8-9-10-11-12-13+14-15;1-2-3-4+5+6+7+8+9+10-11-12-13+14-15;1-2-3-4+5+6+7+8+9-10+11-12+13-14-15;1-2-3-4+5+6+7+8-9+10+11+12-13-14-15;1-2-3-4+5+6+7-8-9-10-11+12-13+14+15;1-2-3-4+5+6-7+8-9-10+11-12-13+14+15;1-2-3-4+5+6-7+8-9-10-11+12+13-14+15;1-2-3-4+5+6-7-8+9+10-11-12-13+14+15;1-2-3-4+5+6-7-8+9-10+11-12+13-14+15;1-2-3-4+5+6-7-8+9-10-11+12+13+14-15;1-2-3-4+5+6-7-8-9+10+11+12-13-14+15;1-2-3-4+5+6-7-8-9+10+11-12+13+14-15;1-2-3-4+5-6 7+8 9-10-11-12+13-14+15;1-2-3-4+5-6 7+8+9+10-11+12+13+14+15;1-2-3-4+5-6+7+8-9+10-11-12-13+14+15;1-2-3-4+5-6+7+8-9-10+11-12+13-14+15;1-2-3-4+5-6+7+8-9-10-11+12+13+14-15;1-2-3-4+5-6+7-8+9+10-11-12+13-14+15;1-2-3-4+5-6+7-8+9-10+11+12-13-14+15;1-2-3-4+5-6+7-8+9-10+11-12+13+14-15;1-2-3-4+5-6+7-8-9+10+11+12-13+14-15;1-2-3-4+5-6-7+8+9+10-11+12-13-14+15;1-2-3-4+5-6-7+8+9+10-11-12+13+14-15;1-2-3-4+5-6-7+8+9-10+11+12-13+14-15;1-2-3-4+5-6-7+8-9+10+11+12+13-14-15;1-2-3-4+5-6-7-8-9-10-11+12+13+14+15;1-2-3-4-5 6+7 8+9-10+11-12-13-14+15;1-2-3-4-5 6+7 8+9-10-11+12-13+14-15;1-2-3-4-5 6+7 8-9+10+11-12-13+14-15;1-2-3-4-5 6+7 8-9+10-11+12+13-14-15;1-2-3-4-5+6 7+8-9-10+11-12-13-14-15;1-2-3-4-5+6 7-8+9+10-11-12-13-14-15;1-2-3-4-5+6+7+8+9-10-11-12-13+14+15;1-2-3-4-5+6+7+8-9+10-11-12+13-14+15;1-2-3-4-5+6+7+8-9-10+11+12-13-14+15;1-2-3-4-5+6+7+8-9-10+11-12+13+14-15;1-2-3-4-5+6+7-8+9+10-11+12-13-14+15;1-2-3-4-5+6+7-8+9+10-11-12+13+14-15;1-2-3-4-5+6+7-8+9-10+11+12-13+14-15;1-2-3-4-5+6+7-8-9+10+11+12+13-14-15;1-2-3-4-5+6-7+8 9-10-11-12-13-14-15;1-2-3-4-5+6-7+8+9+10+11-12-13-14+15;1-2-3-4-5+6-7+8+9+10-11+12-13+14-15;1-2-3-4-5+6-7+8+9-10+11+12+13-14-15;1-2-3-4-5+6-7-8-9-10+11-12+13+14+15;1-2-3-4-5-6 7+8 9+10+11+12-13-14-15;1-2-3-4-5-6+7+8+9+10+11-12-13+14-15;1-2-3-4-5-6+7+8+9+10-11+12+13-14-15;1-2-3-4-5-6+7-8-9+10-11-12+13+14+15;1-2-3-4-5-6+7-8-9-10+11+12-13+14+15;1-2-3-4-5-6-7+8+9-10-11-12+13+14+15;1-2-3-4-5-6-7+8-9+10-11+12-13+14+15;1-2-3-4-5-6-7+8-9-10+11+12+13-14+15;1-2-3-4-5-6-7-8+9+10+11-12-13+14+15;1-2-3-4-5-6-7-8+9+10-11+12+13-14+15;1-2-3-4-5-6-7-8+9-10+11+12+13+14-15",
        //                1);
        //        totalTime += test(
        //                "S9",
        //                "1 2+3 4-5 6-7+8+9;1 2+3+4-5-6-7+8-9;1 2+3-4 5+6+7+8+9;1 2+3-4+5-6+7-8-9;1 2-3+4+5 6-7 8+9;1 2-3+4+5+6-7-8-9;1 2-3-4-5+6-7-8+9;1 2-3-4-5-6+7+8-9;1+2-3 4-5 6+7 8+9;1-2 3-4-5 6-7+8 9;1-2-3 4+5+6+7+8+9",
        //                c);

    }
}
