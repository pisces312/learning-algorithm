/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pisces312
 */
public class MathTool {
     /**
     * 采用分解素数的方法求因数之和,包括了1和自身!!!!
     * What's fast is the sumOfDivisors part; it extracts prime factors and then computes the sum of divisors from those primes. Hints to that algorithm are in the FAQ of this site. This is Alvaro's version, which is more readable than mine was.
    The algorithm counts n as a factor of n; to compensate, the comparison is done against n+n.
    For the selection of numbers that cannot be written as the sum of two abundant numbers, there is a choice of algorithm. One can search for a combination of abundant numbers to make each number, as I did, or one can generate the numbers that can be written as (...) by running through all combinations of abundant numbers. Perhaps the latter is faster :(

     * @param n
     * @return
     */
    public  static int sumOfDivisors(int n) {
        int prod = 1;
        for (int k = 2; k * k <= n; ++k) {
            int p = 1;
            while (n % k == 0) {//整除
                p = p * k + 1;
                n /= k;
            }
            prod *= p;
        }
        if (n > 1) {
            prod *= 1 + n;
        }
        return prod;
    }
    //计算十进制所有位之和
    public static long getDecBitSum(String num){
        long sum=0l;
        for(int i=0;i<num.length();i++){
            sum+=(num.charAt(i)-'0');
        }
        return sum;
    }
    public static boolean isPalindromic(String num) {
        int len = num.length() / 2;
        for (int i = 0; i < len; i++) {
            if (num.charAt(i) != num.charAt(num.length() - i - 1)) {
                return false;
            }
        }
        return true;

    }
    public static long factorial(int n){
        long result=1;
        for(int i=2;i<=n;i++){
            result*=i;
        }
        return result;

    }
}
