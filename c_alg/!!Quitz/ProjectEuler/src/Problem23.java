
public class Problem23 {

    /** Creates a new instance of Abundant */
    int limit = 28123;
    int[] abNums = new int[limit + 1];//存储过剩数,减少循环
//    int[] nums = new int[limit + 1];//用于求和
    boolean[] abundant = new boolean[limit + 1];

    public int getAnswer() {
        int nAb = 0;
        //找出所有过剩数
        for (int n = 1; n <= limit; n++) {
//            nums[n] = n;
            abundant[n] = isAbundant(n);
            if (abundant[n]) {
                abNums[nAb++] = n;
            //System.out.println("" + n);
            }
        }

        int sum = 0, j;
//        for (int i = 1; i <= limit; i++) {
//            for (j = 1; j < i; j++) {
//                if (abundant[j] && abundant[i - j]) {//是两个abundend number的和的情况
//                    break;
//                }
//            }
//            if (j == i) {
//                sum += i;
////                System.out.println(i);
//            }
//        }
        //
        for (int n = 1; n <= limit; n++) {
            int iAb = 0;

            if (n - abNums[iAb] < 12) {
                sum += n;
            } else {
                boolean flag = true;
                while (iAb < nAb && abNums[iAb] < n) {
                    if (abundant[n - abNums[iAb]]) {
                        flag = false;
                        break;
                    }
                    iAb++;
                }
                if (flag) {
                    sum += n;
                }
            }

        }
//        int sum = 0;
//        for (int i = 1; i <= limit; i++)
//            sum += nums[i];
//        return sum;


        return sum;
    }

    private boolean isAbundant(int n) {
        return (sumOfDivisors(n) > (n<<1));
    }

    /**
     * 采用分解素数的方法求因数之和,包括了1和自身!!!!
     * What's fast is the sumOfDivisors part; it extracts prime factors and then computes the sum of divisors from those primes. Hints to that algorithm are in the FAQ of this site. This is Alvaro's version, which is more readable than mine was.
    The algorithm counts n as a factor of n; to compensate, the comparison is done against n+n.
    For the selection of numbers that cannot be written as the sum of two abundant numbers, there is a choice of algorithm. One can search for a combination of abundant numbers to make each number, as I did, or one can generate the numbers that can be written as (...) by running through all combinations of abundant numbers. Perhaps the latter is faster :(

     * @param n
     * @return
     */
    public  int sumOfDivisors(int n) {
        int prod = 1;
        for (int k = 2; k * k <= n; ++k) {
            int p = 1;
            while (n % k == 0) {//整除
                p = p * k + 1;
                n /= k;
            }
            prod *= p;
        }
        if (n > 1) {
            prod *= 1 + n;
        }
        return prod;
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
//        System.out.println(new Problem23().sumOfDivisors(12));
        System.out.println("Answer: " + (new Problem23()).getAnswer());
        long stop = System.currentTimeMillis();
        System.out.println("Time used: " + (stop - start) + "ms");
    }
}