
public class Problem31 {

    final static int SUM = 200;
    final static int COINS = 7;
    static long nway[][] = new long[SUM + 1][COINS + 1];
    static int coins[] = {1, 2, 5, 10, 20, 50, 100, 200};

    public static void initialise() {

        for (int i = 0; i <= SUM; i++) {
            for (int j = 0; j <= COINS; j++) {
                nway[i][j] = -1;
            }
        }

        nway[0][0] = 1;
    }

    public static long f(int sum, int j) {
        if (j < 0 || sum < 0) {
            return 0;
        }

        if (nway[sum][j] > -1) {
            return nway[sum][j];
        }

//System.out.println (s + "\t" + (s-coins[j]) + "\t" + j);
        return (f(sum, j - 1) + f(sum - coins[j], j));
    }

    public static void main(String[] args) {
        initialise();
        System.out.println("No. of ways to change 200: " + f(SUM, COINS));
    }
}