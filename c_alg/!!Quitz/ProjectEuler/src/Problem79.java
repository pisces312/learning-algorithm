
/**
 * Problem 79
A common security method used for online banking is to ask the user for three random characters from a passcode. For example, if the passcode was 531278, they may asked for the 2nd, 3rd, and 5th characters; the expected reply would be: 317.

The text file, keylog.txt, contains fifty successful login attempts.

Given that the three characters are always asked for in order, analyse the file so as to determine the shortest possible secret passcode of unknown length.
 *
 * 05 Nov 2007 01:57 am
nikgyu   (Java)

i haven't read all replies, but i found a simple solution:
determine all digits that occur after a digit in the xyz-codes and you get:
 * 概率的方法，即一个数后面的数越多，说明它越往前！！！
7=[0, 1, 2, 3, 6, 8, 9],
3=[0, 1, 2, 6, 8, 9],
1=[0, 2, 6, 8, 9],
6=[0, 2, 8, 9],
2=[0, 8, 9],
8=[0, 9],
9=[0]
0 is always the last digit

neat!

/n

 * @author pisces312
 */
public class Problem79 {

    static int[] log = {319, 680,
        180, 690,
        129, 620,
        762, 689,
        762, 318,
        368, 710,
        720, 710,
        629, 168,
        160, 689,
        716, 731,
        736, 729,
        316, 729,
        729, 710,
        769, 290,
        719, 680,
        318, 389,
        162, 289,
        162, 718,
        729, 319,
        790, 680,
        890, 362,
        319, 760,
        316, 729,
        380, 319,
        728, 716
    };

    public static void main(String[] args) {

        //结果就保存在s中，初始化s为第一个密码
        String s = String.valueOf(log[0]);
        for (int i = 0; i < log.length; i++) {
            String s2 = String.valueOf(log[i]);

            if (s.charAt(0) == s2.charAt(2)) // prepend s2加到结果的前面
            {
                s = s2 + s.substring(1);
            } else if (s.charAt(s.length() - 1) == s2.charAt(0)) // append s2加到结果后面
            {
                s += s2.substring(0, 2);
            } else if (s.substring(0, 2).equals(s2.substring(1))) // prepend first digit of s2
            {
                s = s2.charAt(0) + s;
            } else if (s.substring(s.length() - 2).equals(s2.substring(0, 2))) // append last digit of s2
            {
                s += s2.charAt(2);
            } else // insert middle digit of s2
            {
                s = s.replace(s2.charAt(0) + "" + s2.charAt(2), s2);
            }
        }
        System.out.println(s);
//        Console.WriteLine(s);
    }
}