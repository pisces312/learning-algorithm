
public class Problem39 {

    private static int[] perimeter = new int[1000];

    public static void main(String args[]) {
        long begin = System.currentTimeMillis();

        int a, b, c;
        int total;

        for (int m = 2; m < 22; m++) {
            for (int n = m % 2 + 1; n < m; n += 2) {
                if (gcd(m, n) == 1) {
                    int m2 = m * m;
                    int n2 = n * n;

                    a = m2 - n2;
                    b = 2 * m * n;
                    c = m2 + n2;

                    total = a + b + c;
//                    if(total<1000)
//                    perimeter[total]++;

                    for (int loop = total; loop < 1000; loop += total) {
                        perimeter[loop]++;
                    }

                }
            }
        }

        int max = -1;
        int maxIndex = -1;

        for (int loop = 0; loop < 1000; loop++) {
            if (perimeter[loop] > max) {

                max = perimeter[loop];
                maxIndex = loop;

            }
        }

        System.out.println("Maximum perimeter: " + maxIndex);
        System.out.println((System.currentTimeMillis() - begin) + "ms");

    }

    private static int gcd(int a, int b) {
        int n;
        do {
            n = a % b;
//        int temp=a;
            a = b;
            b = n;
        } while (n != 0);
        return a;
//        if (a == 1 || b == 1) {
//            return 1;
//        } else if (a % b == 0) {
//            return b;
//        } else if (b % a == 0) {
//            return a;
//        } else if (a > b) {
//            return gcd(a - (a / b) * b, b);
//        } else {
//            return gcd(a, b - (b / a) * a);
//        }

    }
}