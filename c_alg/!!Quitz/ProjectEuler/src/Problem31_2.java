
public class Problem31_2 {

    private static int[] coinValue = {200, 100, 50, 20, 10, 5, 2};
    private static int[] coinAmount = {0, 0, 0, 0, 0, 0, 0};

    public static void main(String args[]) {

        int coinTotal = 200;
        int whichCoin = 6;//从最后一种货币类型开始，这里为2p，不考虑1p
        int solutions = 1;//统计所有可行方案

        while (coinAmount[0] != 1) {

            coinAmount[whichCoin]++;

            while (getTotal() > coinTotal) {

                coinAmount[whichCoin] = 0;
                coinAmount[--whichCoin]++;

            }

            solutions++;

            whichCoin = 6;

        }

        System.out.println("Solutions: " + solutions);

    }
//从记录每种货币数量的数组中计算当前的总价
    private static int getTotal() {

        int total = 0;

        for (int loop = 0; loop < coinAmount.length; loop++) {
            total += coinValue[loop] * coinAmount[loop];
        }

        return total;

    }
}