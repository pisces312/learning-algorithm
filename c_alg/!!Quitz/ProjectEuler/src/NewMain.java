
import java.math.BigInteger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;

/**
 *
 * @author pisces312
 */
public class NewMain {

    /**
     * Problem 63
    The 5-digit number, 16807=7^5, is also a fifth power. Similarly, the 9-digit number, 134217728=8^9, is a ninth power.

    How many n-digit positive integers exist which are also an nth power?
    5^19 1^! 2^1 3^1 4^1 5^1 6^1 7^1 8^1 9^1 7^5 8^9

     */
    static void problem63() {
        BigInteger d = new BigInteger("1");
        BigInteger end = new BigInteger("2");
        BigInteger big;
        for (big = new BigInteger("2"); big.compareTo(end) <= 0; big = big.add(d)) {

            int p = 2;
            BigInteger mul = new BigInteger(big.toString());
            mul = mul.multiply(big);
            while (mul.toString().length() < p) {
//                System.out.println(big + "^" + p + "=" + mul);
                mul = mul.multiply(big);
                p++;
            }
            if (p == mul.toString().length()) {
                System.out.println(big + "^" + p + "=" + mul);
            }
//                System.out.println(big + "^" + p + "=" + mul+"  abc");

//            if (p == mul.toString().length()) {
//                System.out.println(big + "^" + p + "=" + mul+"  abc");
//            } else {
//                do {
//                    mul = mul.multiply(big);
//                    p++;
//                } while (mul.toString().length() != p);
//                if (p == mul.toString().length()) {
//                    System.out.println(big + "^" + p + "=" + mul);
//                }
//            }

        }
//        System.out.println(big.pow(10).toString());
    }

    /**
     * Problem 3
    The prime factors of 13195 are 5, 7, 13 and 29.
    What is the largest prime factor of the number 600851475143 ?
     */
    static void problem3() {
//        BigInteger num=new BigInteger("13195");
        BigInteger num = new BigInteger("317584931803");
//                BigInteger num = new BigInteger("600851475143");
//
        BigInteger end = num.divide(new BigInteger("2"));
//        BigInteger d = new BigInteger("1");
        BigInteger max = BigInteger.ZERO;

        for (BigInteger big = new BigInteger("2"); big.compareTo(end) <= 0; big = big.add(BigInteger.ONE)) {
            BigInteger temp = num.mod(big);
            if (temp.compareTo(BigInteger.ZERO) == 0) {
//                System.out.println("is zero");
//                System.out.println("");
                //
                BigInteger div = new BigInteger("2");
                BigInteger end2 = big.divide(div);
                for (; div.compareTo(end2) < 0; div = div.add(BigInteger.ONE)) {
                    if (big.mod(div).compareTo(BigInteger.ZERO) == 0) {
                        break;
                    }
                }
                if (div.compareTo(end2) == 0) {
                    System.out.println(big + " is prime");
//                    if (big.compareTo(max) > 0) {
                    max = big;
//                    }
                }
            //
//                if(big.isProbablePrime(10)){
//                    System.out.println(big+" is prime");
//                    if(big.compareTo(max)>0){
//                        max=big;
//                    }
//                }
            }
        }
        System.out.println(max);
//        BigInteger[] result=num.divideAndRemainder(big);


    }

    static void problem3_2() {
        long it = 600851475143l;
//        long it = 317584931803l;
        int n = ((int) Math.sqrt(it)) + 1;
        boolean isprime[] = new boolean[n + 1];

        Arrays.fill(isprime, true);
        isprime[2] = true;
        isprime[3] = true;
        for (int i = 4; i < n; i += 2) {
            isprime[i] = false;
        }
        for (int i = 3; i < n; i += 2) {
            for (int j = i + i; j < n; j += i) {
                isprime[j] = false;
            }
        }
        long out = 0;
        for (int i = 2; i < n; ++i) {
            if (isprime[i] && (it % i) == 0) {
                out = Math.max(i, out);
            }
        }

        System.out.println(out);

    }

    /**
     * Problem 36    
    The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.    
    Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.    
    (Please note that the palindromic number, in either base, may not include leading zeros.)
     * //不包含0前缀
     */
    static void problem36() {
        long sum = 0l;
        for (Integer i = 1; i < 1000000; i++) {
            if (MathTool.isPalindromic(i.toString()) && MathTool.isPalindromic(Integer.toBinaryString(i))) {
                System.out.print(i + "  ");
                System.out.println(Integer.toBinaryString(i));
                sum += i;
            }

        }
        System.out.println(sum);
    }

    /**
     * Problem 34
    145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.
    Find the sum of all numbers which are equal to the sum of the factorial of their digits.
    Note: as 1! = 1 and 2! = 2 are not sums they are not included.
     * 即不包括单独的1和2，至少两位数
     * 9!=362880

    9999999 is an easy upper limit to come up with. 7 times 9! is less than 9999999.

    Another, more complex, method would be to create a tree starting with the resulting least significant digit and the possible factorials that could make that digit. It is a fairly small tree, but with many branches all greater than the number formed by the digits.

    For example, how can the least digit be 9?
    With the following additional digit sets:
    {6,1,1,1}
    {6,2,1}
    {4,4,1}
    {4,2,2,1}
    {4,2,1,1,1}
    {4,1,1,1,1,1}
    {2,1,1,1,1,1,1,1}
    {1,1,1,1,1,1,1,1,1}

    (0 could be used instead of 1)

    See how quickly the branches grow beyond the easy upper bound already established at the top?
     */
    public static void problem34() {



        int[] factorials = new int[10];
        for (int i = 0; i < factorials.length; i++) {
            factorials[i] = (int) MathTool.factorial(i);
//            System.out.println(factorials[i]);
        }
        //计算上界
        long begin = 9;
        int a = factorials[9];
        long sum = a;
        while (begin < sum) {
            begin = begin * 10 + 9;
            sum += a;
        }


        begin = sum;
        sum = 0;
        for (int i = 3; i <= begin; i++) {
            String num = String.valueOf(i);
            int bitSum = 0;
            for (int j = 0; j < num.length(); j++) {
                bitSum += factorials[num.charAt(j) - '0'];
            }
            if (bitSum == i) {
                sum += i;
                System.out.println(i);
            }
        }
        System.out.println(sum);
    //不存在这样的两位数
//        for(int i=1;i<10;i++){
//            for(int j=1;j<10;j++){
//                int p=10*i+j;
//                if(factorials[i]+factorials[j]==p){
//                    System.out.print(factorials[i]);
//                    System.out.println("+"+factorials[j]+"="+p);
//                }
//            }
//        }
    //
    //
//        for (int i = 0; i < 10; i++) {
//            for (int j =0; j < 10; j++) {
//                for (int k = 1; k < 10; k++) {
//                    int p = 100 * i + 10 * j + k;
//                    if (factorials[i] + factorials[j] + factorials[k] == p) {
//
//                        System.out.println(factorials[i] + "+" + factorials[j] + "+" + factorials[k] + "=" + p);
//                    }
//                }
//            }
//        }
    //
//        for(int i=1;i<factorials.length;i++){
//            System.out.println(factorials[i]);
//        }

    }

    /**
     * Problem 56
    A googol (10^100) is a massive number: one followed by one-hundred zeros; 100^100 is almost unimaginably large: one followed by two-hundred zeros. Despite their size, the sum of the digits in each number is only 1.

    Considering natural numbers of the form, ab, where a, b  100, what is the maximum digital sum?


     */
    public static void problem56() {
        BigInteger a = new BigInteger("2");
//        BigInteger b=new BigInteger("2");
        BigInteger end = new BigInteger("100");
        long max = 0l;
        for (; a.compareTo(end) <= 0; a = a.add(BigInteger.ONE)) {
            for (int b = 2; b <= 100; b++) {
//                String str = a.pow(b).toString();
                long temp = MathTool.getDecBitSum(a.pow(b).toString());
                if (temp > max) {
                    max = temp;
                }
            }
//            for(;b.compareTo(end)<=0;b.add(BigInteger.ONE)){
//
//            }

        }
        System.out.println(max);


    }
    //改进
    public static void problem56_2() {
        BigInteger a = new BigInteger("2");
//        BigInteger b=new BigInteger("2");
        BigInteger end = new BigInteger("100");
        long max = 0l;
        for (; a.compareTo(end) <= 0; a = a.add(BigInteger.ONE)) {
            BigInteger p = a;
            for (int b = 2; b <= 100; b++) {
                p = p.multiply(a);
//                String str = a.toString();
//                String str=a.pow(b).toString();
                long temp = MathTool.getDecBitSum(p.toString());
                if (temp > max) {
                    max = temp;
                }
            }
//            for(;b.compareTo(end)<=0;b.add(BigInteger.ONE)){
//
//            }

        }
        System.out.println(max);


    }
    //获得循环小数的循环节长度,只要知道分母即可！！！对于非循环小数返回0
    public static int repeating_decimal_length(String num) {
        BigInteger number = new BigInteger(num);
        BigInteger n2 = new BigInteger("2");
        BigInteger n5 = new BigInteger("5");
        BigInteger n9 = new BigInteger("9");
        while (number.mod(n2).compareTo(BigInteger.ZERO) == 0) {
            number = number.divide(n2);
        }
        while (number.mod(n5).compareTo(BigInteger.ZERO) == 0) {
            number = number.divide(n5);
        }
        if (number.compareTo(BigInteger.ONE) == 0) {
            return 0;
        }
        BigInteger i = new BigInteger("9");
        while (i.mod(number).compareTo(BigInteger.ZERO) != 0) {
            i = i.multiply(BigInteger.TEN).add(n9);
        }
        return i.toString().length();
    }
//    public static void testWithTime(){
//
//    }
    /**
     * Problem 53
    There are exactly ten ways of selecting three from five, 12345:
    
    123, 124, 125, 134, 135, 145, 234, 235, 245, and 345
    
    In combinatorics, we use the notation, 5C3 = 10.
    
    In general,
    
    nCr =  n!
    
    r!(nr)! ,where r  n, n! = n(n1)...321, and 0! = 1. 
    
    It is not until n = 23, that a value exceeds one-million: 23C10 = 1144066.
    
    How many values of  nCr, for 1  n  100, are greater than one-million?
    
    
     */
    public static void problem53() {
        //1不用
        final BigInteger[] power = new BigInteger[100 + 1];
        power[0] = BigInteger.ONE;
        BigInteger i = new BigInteger("1");
        final BigInteger end = new BigInteger("100");
        for (; i.compareTo(end) <= 0; i = i.add(BigInteger.ONE)) {
            power[i.intValue()] = power[i.intValue() - 1].multiply(i);
//            System.out.println(power[i.intValue()]);
        }
        BigInteger n = new BigInteger("23");

//        BigInteger max=BigInteger.ZERO;
        final BigInteger max = new BigInteger("1000000");
        int count = 0;
        final BigInteger n2 = new BigInteger("2");
        for (; n.compareTo(end) <= 0; n = n.add(BigInteger.ONE)) {
//            BigInteger end2 = n.divide(n2).add(BigInteger.ONE);
            for (i = n2; i.compareTo(n) < 0; i = i.add(BigInteger.ONE)) {
                BigInteger t = power[n.intValue()].divide(power[i.intValue()]).divide((power[n.intValue() - i.intValue()]));
                if (t.compareTo(max) > 0) {
//                    if (i.compareTo(t) != 0) {//奇数情况
//                        count++;
//                    } else {
                    count++;
//                    }
                    System.out.println(t);

                }
            }
        }
        System.out.println(count);
//        BigInteger b=new BigInteger("2");



    }
    //改进
    public static void problem53_2() {
        //1不用
        final BigInteger[] power = new BigInteger[100 + 1];
        power[0] = BigInteger.ONE;
        BigInteger i = BigInteger.ONE;
        final BigInteger end = new BigInteger("100");
        for (; i.compareTo(end) <= 0; i = i.add(BigInteger.ONE)) {
            power[i.intValue()] = power[i.intValue() - 1].multiply(i);
        }
        final BigInteger max = new BigInteger("1000000");
        int count = 0;
        for (int n = 23; n <= 100; n++) {
            boolean isNEven = (n % 2 == 0);//对n为偶数做特殊处理，根据对称性，只求一半即可！
            int end2 = n / 2;
//            BigInteger end2 = n.divide(n2).add(BigInteger.ONE);
            for (int j = 2; j <= end2; j++) {
                BigInteger t = power[n].divide(power[j]).divide((power[n - j]));
                if (t.compareTo(max) > 0) {
                    if (isNEven && j == end2) {
                        count++;
                    } else {
                        count += 2;
                    }
//                    System.out.println(t);

                }
            }
        }
        System.out.println(count);
//        BigInteger b=new BigInteger("2");



    }

    /**
     * 16 Nov 2004 02:43 am
    euler   (PHP)

    You can significantly simplify this problem...

    First of all we note that C(n,r) are the terms in Pascal's triangle. So C(n,r)=C(n-1,r-1)+C(n-1,r).

    1
    1 1
    1 2 1
    1 3 3 1
    1 4 6 4 1
    1 5 10 10 5 1

    For example, C(5,3)=10, and C(4,2)+C(4,3)=4+6=10

    Next we realise that we don't need to keep track of the actual sum of each value. Once a value has reached one million, we can set it back so that it never exceeds it again. It won't give that actual values of C(n,r), but it will allow us to count the number of elements that exceed one million.

    采用杨辉三角形
     */
    public static void problem53_3() {
        int len = 100;
        BigInteger[][] yh = new BigInteger[len][len];
        for (int i = 0; i < len; i++) {
            yh[0][i] = BigInteger.ONE;
            yh[i][0] = BigInteger.ONE;
        }
        int c = 0;
        BigInteger max = new BigInteger("1000000");
        for (int i = 1; i < len; i++) {
//            boolean isNEven=(i%2==0);
            for (int j = 1; j <= len - i; j++) {
                yh[i][j] = yh[i - 1][j].add(yh[i][j - 1]);
                if (yh[i][j].compareTo(max) > 0) {
                    c++;
                }

            }
        }
        System.out.println(c);
    }

    /**
    Problem 52
    It can be seen that the number, 125874, and its double, 251748, contain exactly the same digits, but in a different order.
    Find the smallest positive integer, x, such that 2x, 3x, 4x, 5x, and 6x, contain the same digits.
    有很多这样的数，必须是可构成循环小数的分母
     *
     * For this problem I didn't even use a computer. I just remembered that the repeating sequence of digits in the decimal representation of 1/7 has the desired property.

    1/7 = 0.142857 142857 142857 ...

    2 * 142857 = 285714
    3 * 142857 = 428571
    4 * 142857 = 571428
    5 * 142857 = 714285
    6 * 142857 = 857142
    but
    7 * 142857 = 999999
     *
     *
     * The first number in x must be 1. If it were 2, then the number would be:
     * 2abcd
    And the number multiplied by 6 would be:
    12efgh
    This means that the two numbers cannot possibly contain the same digits. Also, the number cannot be less than 100. If it were, there could not possibly be 6 different combinations of it digits, only 2 (ab, ba). Also, the second digit has to be less than or equal to 6, as does the 3rd, 4th, 5th, etc. So you only have to search for numbers in the ranges:
    100 - 166
    1000 - 1666
    10000 - 16666
    And so on.
     * 第一个位数必须是1
     **/
    public static boolean hasSameDigit(int[] digits, long num) {
        boolean[] flags = new boolean[digits.length];
        Arrays.fill(flags, false);
        int i;
        while (num != 0) {
            int n = (int) (num % 10);
            num /= 10;
            for (i = 0; i < digits.length; i++) {
                if (digits[i] == n) {
                    flags[i] = true;
                    break;
                }
            }
            if (i == digits.length) {
                return false;
            }
        }
        for (i = 0; i < flags.length; i++) {
            if (flags[i] == false) {
                return false;
            }
        }
        return true;
    }

    public static boolean hasSameDigit(LinkedList<Integer> digits, long num) {
        boolean[] flags = new boolean[digits.size()];
        Arrays.fill(flags, false);
        int i;
        while (num != 0) {
            int n = (int) (num % 10);
            num /= 10;
            i = 0;
            for (Integer p : digits) {
                if (p == n) {
                    flags[i] = true;
                    break;
                }
                i++;
            }
            if (i == digits.size()) {
                return false;
            }
        }
        for (i = 0; i < flags.length; i++) {
            if (flags[i] == false) {
                return false;
            }
        }
        return true;
    }

    public static void problem52() {
        LinkedList<Integer> list = new LinkedList<Integer>();
        for (Integer x = 2;; x++) {
            list.clear();
            Integer num = x, last = null;
            while (num != 0) {
                list.add(num % 10);
                last = num;
                num /= 10;
            }
            if (last != 1) {//第一位必须为1的约束
                continue;
            }
            int p = x;
            for (num = 2; num < 7; num++) {
                p += x;
                if (!hasSameDigit(list, p)) {
                    break;
                }
            }
            if (num == 7) {
                System.out.println(x);
                break;
            }
        }

    }

    public static void problem52_2() {
        LinkedList<Integer> list = new LinkedList<Integer>();
        Integer begin = 100, end = 166, x;
        boolean isFound = false;
        do {
            for (x = begin; x < end; x++) {
                list.clear();
                Integer num = x;
                while (num != 0) {
                    list.add(num % 10);
                    num /= 10;
                }
                int p = x;
                for (num = 2; num < 7; num++) {
                    p += x;
                    if (!hasSameDigit(list, p)) {
                        break;
                    }
                }
                if (num == 7) {//找到
                    System.out.println(x);
                    isFound = true;
                    break;
                }
            }
            if (isFound) {
                break;
            }
            begin *= 10;
            end = end * 10 + 6;
        } while (true);


    }

    /**
     * Problem 23
    A perfect number is a number for which the sum of its proper divisors is exactly equal to the number.
     * For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.        A number whose proper divisors are less than the number is called deficient and a number whose proper divisors exceed the number is called abundant.
    As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.
    Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
     */
    public static long getDivisors(int num) {
        long sum = 1;
//        System.out.print(1);
        for (int i = 2; i < num; i++) {
            if (num % i == 0) {
                sum += i;
            }
        }
//        long sum = 1,end=(long) Math.sqrt(num);
////        System.out.print(1);
//        for (int i = 2; i < end; i++) {
//            if (num % i == 0) {
////                System.out.print("+"+i);
//                sum += i;
//                sum+=(num/i);
//            }
//        }
//        if(num%end==0){
//            sum+=end;
//        }
//        System.out.println("="+sum);
        return sum;
    }

    public static void problem23() {
        int size = 28123, j;
        boolean[] flags = new boolean[size + 1];
//        for(int i=0;i<=size;i++){
//            flags[i]=false;
//        }
        Arrays.fill(flags, false);
        long sum = 0;
        //先求过剩数
        for (int i = 12; i <= size; i++) {
//            if (sumOfDivisors(i) > i) {
            if (getDivisors(i) > i) {
                flags[i] = true;

            }
        }
        for (int i = 1; i <= size; i++) {
            for (j = 1; j < i; j++) {
                if (flags[j] && flags[i - j]) {//是两个abundend number的和的情况
                    break;
                }
            }
            if (j == i) {
                sum += i;
//                System.out.println(i);
            }
        }
        System.out.println(sum);

    }

    /**
     * Problem 40
    An irrational decimal fraction is created by concatenating the positive integers:
    0.123456789101112131415161718192021...
    It can be seen that the 12th digit of the fractional part is 1.
    If dn represents the nth digit of the fractional part, find the value of the following expression.
    d1  d10  d100  d1000  d10000  d100000  d1000000
     */
    public static void problem40() {

        int last, power = 1;
//        for (n = 10; n <= 15; n++) {
//        for (n = 499; n <= 505; n++) {
        for (int n = 1; n <= 1000000; n *= 10) {
            System.out.println("n=" + n);
            if (n < 10) {
                System.out.println("result=1");
                power *= n;
                continue;
            }
            int begin = 1, end = 9, c = 1, t = n;
            while (true) {
                last = t;
                t -= ((end - begin + 1) * c);
//            System.out.println(n);
                if (t == 0) {
                    power *= 9;
                    System.out.println("result=" + 9);
                    break;

                } else if (t < 0) {
//                System.out.println(n);
                    t = last;
                    System.out.println(t);
                    int r = t % c;
                    System.out.println("rest=" + r + " c=" + c);
                    int q = t / c;
                    if (r == 0) {
                        q--;
                        r = c;//余数为0时必须变为模的大小！！ 余数位1，2，3依次对应一个三位数的最左一位，次左一位，最右一位
                    }
                    int num = begin + q;
                    System.out.println("num=" + num);
//??????????
                    r = c - r;
//                    r = c - r-1;
                    while (r > 0) {
                        num /= 10;
                        r--;
                    }
                    power *= (num % 10);
                    System.out.println("result=" + (num % 10));
                    break;
                }
                begin *= 10;
                end = end * 10 + 9;
                c++;
            }
        }
        System.out.println(power);

    }

    /**
     * Problem 29
    Consider all integer combinations of ab for 2  a  5 and 2  b  5:
    22=4, 23=8, 24=16, 25=32
    32=9, 33=27, 34=81, 35=243
    42=16, 43=64, 44=256, 45=1024
    52=25, 53=125, 54=625, 55=3125
    If they are then placed in numerical order, with any repeats removed, we get the following sequence of 15 distinct terms:
    4, 8, 9, 16, 25, 27, 32, 64, 81, 125, 243, 256, 625, 1024, 3125
    How many distinct terms are in the sequence generated by ab for 2  a  100 and 2  b  100?
     */
    public static void problem29() {
        BigInteger a = new BigInteger("2");
        BigInteger end = new BigInteger("100");
//        HashSet<BigInteger> list=new HashSet<BigInteger>();
        LinkedList<BigInteger> list = new LinkedList<BigInteger>();
        int c = 0;
        for (; a.compareTo(end) <= 0; a = a.add(BigInteger.ONE)) {
            for (int b = 2; b <= 100; b++) {
                BigInteger result = a.pow(b);
                if (!list.contains(result)) {
                    c++;
//                    System.out.println(result);
                    list.add(result);
                }
            }
        }
        System.out.println(c);
    }
    //使用hash表
    public static void problem29_2() {
        BigInteger a = new BigInteger("2");
        BigInteger end = new BigInteger("100");
        HashSet<BigInteger> list = new HashSet<BigInteger>();
//        LinkedList<BigInteger> list = new LinkedList<BigInteger>();
        int c = 0;
        for (; a.compareTo(end) <= 0; a = a.add(BigInteger.ONE)) {
            for (int b = 2; b <= 100; b++) {
                BigInteger result = a.pow(b);
                if (!list.contains(result)) {
                    c++;
//                    System.out.println(result);
                    list.add(result);
                }
            }
        }
        System.out.println(c);
    }

    /**
     * Problem 30
    Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:
    1634 = 14 + 64 + 34 + 44
    8208 = 84 + 24 + 04 + 84
    9474 = 94 + 44 + 74 + 44
    As 1 = 14 is not a sum it is not included.
    The sum of these numbers is 1634 + 8208 + 9474 = 19316.
    Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.
     */
    public static void problem30() {

//        System.out.println(begin);
//        System.out.println(sum);
//        System.out.println();
        int[] power = new int[10];


        for (int i = 0; i < power.length; i++) {
            power[i] = (int) Math.pow(i, 5);
        }

        //计算上界
        long begin = 9;
        int a = power[9];
        long sum = a;
        while (begin < sum) {
            begin = begin * 10 + 9;
            sum += a;
        }
        begin = sum;//用sum的值较小！！！
        sum = 0;
        for (int i = 2; i < begin; i++) {
            String num = String.valueOf(i);
            long bitSum = 0;
            for (int j = 0; j < num.length(); j++) {
                bitSum += power[num.charAt(j) - '0'];
            }
            if (bitSum == i) {
                sum += i;
//                System.out.println(i);
            }
        }
        System.out.println(sum);
    }

    /**
     * Problem 19
    You are given the following information, but you may prefer to do some research for yourself.
    1 Jan 1900 was a Monday.
    Thirty days has September,
    April, June and November.
    All the rest have thirty-one,
    Saving February alone,
    Which has twenty-eight, rain or shine.
    And on leap years, twenty-nine.
    A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
    How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
     * 所有月中第一天是星期天的次数
     *
     * 手算方法如下
     * RudyPenteado   (Assembler)

    I decided to solve this one by hand also:
即每个月第一天按从周一到周末的顺序改变
    Sunday Monday Tuesday Wednesday Thursday Friday Saturday
    0 1 2 3 4 5 6
    Those are modulo 7 remainders.

    Normal year started in a specific
    remainder 0,1,2,3,4,5,6
    # of Suns # of Suns
    in Year in 4 years
    J F M A M J J A S O N D
    ---------------------------------------------
    0 3 3 6 1 4 6 2 5 0 3 5 2 2,2,2,2 8
    1 4 4 0 2 5 0 3 6 1 4 6 2 2,2,1,2 7
    2 5 5 1 3 6 1 4 0 2 5 0 2 2,1,3,1 7
    3 6 6 2 4 0 2 5 1 3 6 1 ok 1 1,3,1,1 6
    4 0 0 3 5 1 3 6 2 4 0 2 3 3,1,1,3 8
    5 1 1 4 6 2 4 0 3 5 1 3 1 1,1,2,2 6
    6 2 2 5 0 3 5 1 4 6 2 4 1 1,2,2,1 6

    Lap year started in a specific
    remainder 0,1,2,3,4,5,6
    # of Sundays
    in the Year
    J F M A M J J A S O N D
    -------------------------------------
    0 3 4 0 2 5 0 3 6 1 4 6 3
    1 4 5 1 3 6 1 4 0 2 5 0 2
    2 5 6 2 4 0 2 5 1 3 6 1 1 Ex:
    3 6 0 3 5 1 3 6 2 4 0 2 2 L2=1
    4 0 1 4 6 2 4 0 3 5 1 3 ok 2
    5 1 2 5 0 3 5 1 4 6 2 4 1
    6 2 3 6 1 4 6 2 5 0 3 5 1

    The numbers in the quartets are obtained
    using the 1st 3 consecutive years from
    the top table and the last from the
    lower table
    ----------------------------------------
    1901 to 2000 is a sequence of 25 groups
    of 4 years like 'nor,nor,nor,lap'
    1901 started in a monday (remainder 1)
    Each consecutive in the 5th remainder
    (4 for each year + 1 extra for the last
    year being lap)

    Year start # of
    at Months

    1901 2 7
    1905 0 8
    1909 5 6
    1913 3 6 48
    1917 1 7
    1921 6 6
    1925 4 8

    1929 2 7
    1933 0 8
    1937 5 6
    1941 3 6 48
    1945 1 7
    1949 6 6
    1953 4 8

    1957 2 7
    1961 0 8
    1965 5 6
    1969 3 6 48
    1973 1 7
    1977 6 6
    1981 4 8

    1985 2 7
    1999 0 8 27
    1993 5 6
    1997 3 6

    Total = 171


     */
    /**
     * 采用一句话的方法
     * Why would one just compute 1200/7 to find the number of first days of the month that fell on a Sunday in 100 years?
    There are 1200 first days of the month in 100 years, as there are 1200 months. One in every 7 days is a Sunday. There is enough regularity to make things essentially nonrandom. Of course this argument gets one an approximate answer only.
     */
    public static void problem19_2() {
        System.out.println(1200 / 7);
    }

    /**
     * 判断闰年
     * @param y
     * @return
     */
    public static boolean isLeapYear(int y) {
        if ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0) {
            return true;
        }
        return false;
    }
    //获得某年某月的天数
    public static int getMonthDay(int y, int m) {
        switch (m) {
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            case 2:
                if (isLeapYear(y)) {
                    return 29;
                }
                return 28;
            default:
                return 31;

        }
    }

    public static void problem19() {
        int sum = 1 + 365;
        int c = 0;
        //从1901开始！！！
        for (int year = 1901; year <= 2000; year++) {

            for (int m = 1; m <= 12; m++) {
                if (sum % 7 == 0) {
                    c++;
                }
                sum += getMonthDay(year, m);
            }


        }
        System.out.println(c);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        long begin = System.currentTimeMillis();
//        problem34();
        problem19();
//        problem52_2();
//        int[] digits={1,2,3,4,5};
//        System.out.println(hasSameDigit(digits, 54213));
//        problem53_3();
//        System.out.println(repeating_decimal_length("983"));
//        System.out.println(repeating_decimal_length("2"));
//        problem56();
//        problem56_2();
        long end = System.currentTimeMillis();
        System.out.println(end - begin + "ms");

//        System.out.println(MathTool.getDecBitSum("1234"));
//        BigInteger a=new BigInteger("1234");
//        System.out.println(a.bitLength());

    //产生真随机数
//        SecureRandom random=new SecureRandom();
//        for(int i=0;i<10;i++)
//            System.out.println(random.nextDouble());
//        problem34();
//        System.out.println(isPalindromic("123456654321"));
//        problem3_2();
    // TODO code application logic here
//        long n=317584931803;
//        problem3();
//        long it = 317584931803l;

//        problem63();
//        BigInteger end = new BigInteger("123456");
//        System.out.println(end.bitLength());
//        System.out.println(end.bitCount());
    }
}
