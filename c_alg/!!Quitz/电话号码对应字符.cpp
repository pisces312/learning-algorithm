//3.2电话号码对应英语单词
//
//文章分类:C++编程
//方法一、
//神奇的双重while循环。
//注：显然可以利用这种方法，打印出任意一个N维数组的任意组合。
//
//方法二、递归（实质就是DFS）
//实例代码:
//C++代码
#include<iostream>
#include<cstring>
using namespace std;

//键盘数字上的字符
char c[10][10] = {
    "", "", "ABC", "DEF",
    "GHI", "JKL", "MNO",
    "PQRS", "TUV", "WXYZ"
};
//0到9每个数组键上字符的个数
int total[] = {0, 0, 3, 3, 3, 3, 3, 4, 3, 4};
const int TelLength = 3; //电话号码的位数
int number[TelLength];//每位电话号码的数字
int answer[TelLength]; //每位数字上字符的下标

//方法一
void OutputTelephone(int number[]) {
    while (true) {
        int k = TelLength - 1;
        //一次循环一次从第k个键向前移动一位
        while (k >= 0) {
            for (int i = 0; i < TelLength; i++)
                cout << c[number[i]][answer[i]] << " ";
            cout << endl;
            if (answer[k] < total[number[k]] - 1) {
                //第k个键移动
                answer[k]++;
                break; //这里只要有一个键的一个位置发生一次变化，就退出，打印一次。
            } else {
                //第k个键移动完毕，回到0，开始遍历上一个键。
                //此时，继续内部循环。下次循环，处理上一个键。
                answer[k] = 0;
                k--;
            }

        }
        if (k < 0)
            break;

    }
}

//方法2
void RecursiveOutput(int number[], int answer[], int index) {
    if (index == TelLength) {
        for (int i = 0; i < TelLength; i++)
            cout << c[number[i]][answer[i]] << " ";
        cout << endl;
        return;
    }
    //上下来回反弹的都以递归啊，类似于dfs
    for (answer[index] = 0; answer[index] < total[number[index]]; answer[index]++) {
        RecursiveOutput(number, answer, index + 1);
    }

}

int main() {
    int number[] = {3, 4, 5};
    OutputTelephone(number);
    RecursiveOutput(number, answer, 0);
    return 0;
}
