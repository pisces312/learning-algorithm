#include   <stdio.h>
#include   <errno.h>
#include   <stdlib.h>
#include   <time.h>
#include   <assert.h>

typedef   struct   node {
    int   data;
    struct   node   *next;
} slink;

void   print_slink(const   slink*   head) {
    while(head) {
        printf( "%d ",   head-> data);
        head   =   head-> next;
    }
    printf( "\n ");
}


void   swap(int*   a,   int*   b) {
    int   tmp   =   *a;
    *a   =   *b;
    *b   =   tmp;
}

//将不带头结点的单链表head进行由小到大排序
//主程序调用时，tail=NULL;
static   slink   *my_qsort(slink   *head,   slink**   last) {
    *last   =   head;
    if   (head   ==   NULL)   return   NULL;//没有元素
    if   (head-> next   ==   NULL)   return   head;//一个元素

    slink   *tmp;
    slink   *pivot   =   head-> next;
    slink   *left   =   head;
    slink   *left_tail   =   head;
    slink   *right   =   pivot;
    slink   *right_tail   =   pivot;
    slink   *curr   =   pivot-> next;

    left_tail-> next   =   NULL;
    right_tail-> next   =   NULL;

    if   (head-> data   >=   pivot-> data) {
        swap(&head-> data,   &pivot-> data);
    }

    while(curr) {
        if   (curr-> data   <   pivot-> data) {
            left_tail-> next   =   curr;
            left_tail   =   curr;
        } else {
            right_tail-> next   =   curr;
            right_tail   =   curr;
        }
        //next
        tmp   =   curr-> next;
        curr-> next   =   NULL;
        curr   =   tmp;
    }

    left   =   my_qsort(left,   &tmp);     //对前一部分进行快速排序
    right   =   my_qsort(right,   last);                     //对后一部分进行快速排序
    tmp-> next   =   right;
    return   left;
}


slink   *create(int   N) {
    //创建链表
    int   i;
    slink*   head;
    slink*   tmp;
    slink*   pre;

    head   =   (slink   *)malloc(sizeof(slink));
    head-> data   =   rand();
    pre   =   head;
    for   (i   =   1;   i   <   N;   ++i) {
        tmp   =   (slink   *)malloc(sizeof(slink));
        tmp-> data   =   rand();
        pre-> next   =   tmp;
        pre   =   tmp;
    }
    pre-> next   =   NULL;

    return   head;
}
//end may be NULL
void QuickSort(slink** head,slink* end){
    if(*head==NULL||(*head)->next==NULL) return;//无元素或只有一个元素的情况


    slink* pivot=*head;
//    slink* p=pivot->next;

//    slink* leftHead=pivot;
//    slink* rightHead=pivot->next;
    slink* leftTail=pivot;//i
    slink* rightTail=pivot->next;//rightHead;//j

    slink* tmp;
    slink* old=pivot;
    while(rightTail!=end){
        if(rightTail->data<pivot->data){
//            old->next=leftHead;
//            old=leftTail;
//swap两个指针
            old=leftTail;

            tmp=leftTail;
            leftTail=leftTail->next;
            tmp->next=rightTail;

            tmp=leftTail->next;
            leftTail->next=rightTail->next;
            rightTail->next=tmp;
        }
        rightTail=rightTail->next;
    }
    old->next=pivot;

    tmp=pivot->next;
    pivot->next=leftTail->next;
    leftTail->next=tmp;



    *head=leftTail;





    QuickSort(head,pivot);
    QuickSort(&pivot->next,end);
//    QuickSort(leftHead);
}

int   main() {
    slink   *head=NULL;
    slink   *tail=NULL;
    int   n=10;

int t=time(NULL);
//1289272017
t=1289272017;
printf("%d\n",t);

    srand(t);
//    srand(100ul);

//        printf( "Input   n:\n ");
//        scanf( "%d ",   &n);
//    if   (n   <   2)   break;
    head   =   create(n);

    print_slink(head);


    QuickSort(&head,NULL);
    //head   =   my_qsort(head,   &tail);
    print_slink(head);

    return   0;
}
