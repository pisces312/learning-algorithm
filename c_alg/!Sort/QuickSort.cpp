#include <iostream>

using namespace std;
int x[10]={5,3,9,7,1,4,2,8,0,6};
int comps=0;
int randint(int a,int b) {
    return a+rand()%(b-a);
}
void swap(int a,int b) {
    int temp=x[a];
    x[a]=x[b];
    x[b]=temp;
}
//3-1
void quicksort(int l, int u) {
    int i, m;
    if (l >= u) return;
    swap(l, randint(l, u));//随机选择第一个标准
    m = l;//初始为第一个位置
    for (i = l+1; i <= u; i++)
        if (x[i] < x[l])//都与l位置上的元素比较
            swap(++m, i);//m记录了比标准小的元素的个数，最后m表示了标准应插入的位置
    //??不同于常规的快速算法！！
    swap(l, m);
    quicksort(l, m-1);
    quicksort(m+1, u);
}
void printArray(int *x,int len) {
    for (int i=0;i<len;i++) {
        cout<<x[i]<<" ";
    }
    cout<<endl;
}
//3-2
void quicksort2(int l, int u) {
    int i, m;
    if (l >= u) return;
    swap(l, randint(l, u));
    m = l;
    for (i = l+1; i <= u; i++) {
        comps++;//加入比较计数
        if (x[i] < x[l])
            swap(++m, i);
    }
    swap(l, m);
    quicksort2(l, m-1);
    quicksort2(m+1, u);
}
//3-3
//3-2
void quicksort3(int l, int u) {
    int i, m;
    if (l >= u) return;
    swap(l, randint(l, u));
    m = l;
    comps+=u-l;//比较计数提到循环外
    for (i = l+1; i <= u; i++) {
        if (x[i] < x[l])
            swap(++m, i);
    }
    swap(l, m);
    quicksort3(l, m-1);
    quicksort3(m+1, u);
}
//EXAMPLE 3-4 . Quicksort skeleton reduced to counting
//???不精确？？
void quickcount(int l, int u) {
    int m;
    if (l >= u) return;
    m = randint(l, u);
    comps += u-l;
    quickcount(l, m-1);
    quickcount(m+1, u);
}
//EXAMPLE 3-5 . Quicksort skeleton with single size argument
void qc(int n) {
    int m;
    if (n <= 1) return;
    m = randint(1, n);
    comps += n-1;
    qc(m-1);
    qc(n-m);
}
//EXAMPLE 3-6 . Quicksort skeleton implemented as a function
int cc(int n) {
    int m;
    if (n <= 1) return 0;
    m = randint(1, n);
    return n-1 + cc(m-1) + cc(n-m);
}
//EXAMPLE 3-7 . Quicksort average comparisons as pseudocode
float c(int n) {
    if (n <= 1) return 0;
    int sum = 0;
    for (int m = 1; m <= n; m++)
        sum += n-1 + c(m-1) + c(n-m);
    return sum/n;
}
//EXAMPLE 3-8 . Quicksort calculation with dynamic programming
//相当于同时计算多组值快速排序的比较次数，即从大小为1的数组到
//N的N个数组的排序，效率高
void dynamicQC(int N) {
    int *t=new int[N];
    t[0] = 0;
    int n;
    for (n = 1; n <= N; n++) {
        int sum = 0;
        for (int i = 1; i <= n; i++)
            sum += n-1 + t[i-1] + t[n-i];
        t[n] = sum/n;
    }
    printArray(t,N);
}
//EXAMPLE 3-9 . Quicksort calculation with code moved out of the loop
void dynamicQC2(int N) {
    int *t=new int[N];
    t[0] = 0;
    int n;
    for (n = 1; n <= N; n++) {
        int sum = 0;
        for (int i = 1; i <= n; i++)
            sum += t[i-1] + t[n-i];
        t[n] = n-1 +  sum/n;
    }
    printArray(t,N);
}
//3-10利用对称性
void dynamicQCUsingSym(int N) {
    int *t=new int[N];
    t[0] = 0;
    int n;
    for (n = 1; n <= N; n++) {
        int sum = 0;
        for (int i = 0; i < n; i++)
            sum += 2 * t[i];
        t[n] = n-1 +  sum/n;
    }
    printArray(t,N);
}
//EXAMPLE 3-11 . Quicksort calculation with the inner loop removed
void dynamicQCUsingSym2(int N) {
    int *t=new int[N];
    t[0] = 0;
    int n;
    int sum = 0;
    for (n = 1; n <= N; n++) {
        sum += 2 * t[n-1];
        t[n] = n-1 +  sum/n;
    }
    printArray(t,N);
}
//EXAMPLE 3-12 . Quicksort calculation—final version
void qcFinal(int N) {
    int sum = 0,t = 0,n;
    for (n = 1; n <= N; n++) {
        sum += 2*t;
        t = n-1 + sum/n;
        cout<<t<<endl;
    }

}
int main() {
//    cout << "Hello world!" << endl;
    cout<<"原始数组\n";
    printArray(x,10);
    //
//    quicksort(0,9);
    //
//    quicksort2(0,9);

//      quicksort3(0,9);
//quickcount(0,9);

//qc(10);
//comps=cc(10);
//    float aComps=c(10);
//    cout<<aComps<<endl;
//dynamicQC(10);
//    printArray(x,10);
//    dynamicQC2(10);
//    cout<<comps<<endl;
//    dynamicQCUsingSym(10);
//    dynamicQCUsingSym2(10);
    qcFinal(10);
    //

    return 0;
}
