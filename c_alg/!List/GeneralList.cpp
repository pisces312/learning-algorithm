//---------------------------------------------------------------------------

#pragma hdrstop

#include <iostream.h>

enum Boolean {False, True};

class GeneralList;

class GeneralListNode{
    friend class GeneralList;
private:
    Boolean tag;

    union{
        char data;
        GeneralListNode *dlink;
    };
    GeneralListNode *link;
};

class GeneralList{
    friend class GeneralListNode;
private:
    GeneralListNode *first;

public:
    GeneralListNode *CreateNode(char* data, int& start){
        switch(*(data+start)){
            case 0:{
                return 0;
            }
            case '(':{
                GeneralListNode *node = new GeneralListNode();
                node->tag = True;
                start++;
                node->dlink = CreateNode(data, start);
                node->link = CreateNode(data, start);
                return node;
            }
            case ')':{
                start++;
                return 0;
            }
            default:{
                GeneralListNode *node = new GeneralListNode();
                node->tag = False;
                node->data = *(data+start);
                start++;
                node->link = CreateNode(data, start);
                return node;
            };
        };
    };

    GeneralList(char* data){
        int start = 0;
        first = CreateNode(data,start);
    };

    void deleteNode(GeneralListNode *node){
        if (!node)
            return;
        if (node->tag)
            deleteNode(node->dlink);
        deleteNode(node->link);
        delete node;
    };

    ~GeneralList(){
        if (first)
            deleteNode(first);
    };


    void printNode(GeneralListNode *node){
        if (node){
            if (node->tag){
                cout << '(';
                printNode(node->dlink);
                cout << ')';
            } else {
                cout << node->data;
            }
            printNode(node->link);
        };
    };

    void print(){
        printNode(first);
        cout << endl;
    };

    GeneralListNode* copyNode(GeneralListNode* source){
        GeneralListNode *node = 0;
        if (source){
            node = new GeneralListNode();
            node->tag = source->tag;
            if (node->tag)
                node->dlink = copyNode(source->dlink);
            else
                node->data = source->data;
            node->link = copyNode(source->link);  
        }
        return node;
    };

    void copy(GeneralList* source){
        deleteNode(first);
        first = copyNode(source->first); 
    };

    Boolean operator== (const GeneralList& source){
        return equal(source.first, this->first);
    };

    Boolean equal(GeneralListNode *node1, GeneralListNode *node2){
        if (node1 == node2)
            return True;  
        if ((node1 && (!node2)) || ((!node1) && node2))
            return False;
        if (node1->tag != node2->tag)
            return False;
        if (node1->tag) {
            if (!equal(node1->dlink, node2->dlink))
                return False; 
        } else {
            if (node1->data != node2->data)
                return False;
        }
        return equal(node1->link, node2->link);  
    };

    int depth(){
        return nodeDepth(first);
    };

    int nodeDepth(GeneralListNode* node){
        if (!node){
            return 1;
        } else {
            int m = 0;
            while (node){
                if (node->tag)  {
                    int n = nodeDepth(node->dlink);
                    if (n>m)
                        m = n;
                };
                node = node->link;
            }
            return m + 1;
        };
    };



};


//---------------------------------------------------------------------------

#pragma argsused
int main(int argc, char* argv[])
{
    //create general list
    GeneralList *list = new GeneralList("((ab(abcd)((((d)((ef((ab(c)g))h))i))j)(klmn)))");

    cout << GeneralList("(a(((b(ccc(ddddd))))c)").depth() << endl;

    list->print();

    GeneralList *list2 = new GeneralList("");
    list2->copy(list);
    list2->print();

    if (*list == *list2){
        cout << "Equal" << endl;
    }

    delete list;
    delete list2;

    getchar();
    return 0;
}
//---------------------------------------------------------------------------
 