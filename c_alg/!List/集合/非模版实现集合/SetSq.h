#include<iostream.h>
typedef double dataType;
class SetSq
{
	dataType *set;
	int len;
	int size;
public:
	SetSq(int n=0);
	SetSq(SetSq& s);
	~SetSq();
	bool InsertSet(dataType n);
	bool DeleteSet(dataType n);
	bool FindSet(dataType n)const;
	bool ModifySet(dataType a,dataType b);
	bool isFull()const
	{
		return len==size;
	}
	bool isEmpty()const
	{
		return len<=size;
	}
	int LenthSet()const
	{
		return len;
	}
	void OutputSet()const;
	friend SetSq UnionSet(SetSq& t1,SetSq& t2);
	SetSq UnionSet(SetSq& t);
	SetSq InterseSet(SetSq& t);
	friend SetSq InterseSet(SetSq& t1,SetSq& t2);
	void SortSet();
	friend bool operator==(SetSq& t1,SetSq& t2);
};

SetSq::SetSq(int n)
{
	set=new dataType[n];
	len=0;
	size=n;
}
SetSq::SetSq(SetSq& s)
{
	len=s.len;
	size=s.size;
	set=new dataType[size];
	for(int i=0;i<s.len;i++)
		set[i]=s.set[i];
}
SetSq::~SetSq()
{
	delete []set;
	set=NULL;
	len=size=0;
}

bool SetSq::InsertSet(dataType n)
{
	if(!isFull())
	{
		if(FindSet(n)) 
			return false;
		set[len++]=n;
		return true;
	}
	return false;
}

bool SetSq::DeleteSet(dataType n)
{
	for(int i=0;i<len;i++)
		if(set[i]==n) break;
	if(i<len)
	{
		set[i]=set[len-1];
		len--;
		return true;
	}
	return false;
}

bool SetSq::FindSet(dataType n)const
{
	for(int i=0;i<len;i++)
		if(set[i]==n) return true;
		return false;
}

bool SetSq::ModifySet(dataType a,dataType b)
{
	for(int i=0;i<len;i++)
		if(set[i]==a) break;
		if(i<len)
		{
			set[i]=b;
			return true;
		}
		return false;
}
void SetSq::OutputSet()const
{
	for(int i=0;i<len;i++)
		cout<<set[i]<<" ";
	cout<<endl;
}
SetSq SetSq::UnionSet(SetSq& t)
{
	SetSq s(len+t.len);
	for(int i=0;i<len;i++)
		if(!s.FindSet(set[i])) s.InsertSet(set[i]);
	for(i=0;i<t.len;i++)
		if(!s.FindSet(t.set[i])) s.InsertSet(t.set[i]);
	return s;
}
SetSq UnionSet(SetSq& t1,SetSq& t2)
{
	SetSq s(t1.len+t2.len);
	for(int i=0;i<t1.len;i++)
		if(!s.FindSet(t1.set[i])) s.InsertSet(t1.set[i]);
	for(i=0;i<t2.len;i++)
		if(!s.FindSet(t2.set[i])) s.InsertSet(t2.set[i]);
	return s;
}
SetSq SetSq::InterseSet(SetSq& t)
{
	SetSq s(len>t.len?t.len:len);
	for(int i=0;i<len;i++)
		if(t.FindSet(set[i])) s.InsertSet(set[i]);
	return s;
}
SetSq InterseSet(SetSq& t1,SetSq& t2)
{
	SetSq s(t1.len>t2.len?t2.len:t1.len);
	for(int i=0;i<t1.len;i++)
		if(t2.FindSet(t1.set[i])) s.InsertSet(t1.set[i]);
	return s;
}
void SetSq::SortSet()
{
	dataType x;
	for(int i=0;i<len;i++)
	{
		x=set[i];
		for(int j=i-1;j>=0;j--)
			if(x<set[j])
				set[j+1]=set[j];
			else break;
			set[j+1]=x;
	}
}
bool operator==(SetSq& t1,SetSq& t2)
{
	if(t1.len!=t2.len) return false;
	for(int i=0;i<t1.len;i++)
		if(!t1.FindSet(t2.set[i])) return false;
	for(i=0;i<t1.len;i++)
		if(!t2.FindSet(t1.set[i])) return false;
	return true;
}

