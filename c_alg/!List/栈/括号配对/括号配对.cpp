#include"LinkStack.h"
bool func1(LinkStack<char> &s)
{
	if(s.get()=='[') 
		cout<<"期望\"]\"\n";
	else if(s.get()=='{')
		cout<<"期望\"}\"\n";
	else
		cout<<"期望\"(\"\n";
	return false;
}
bool func2(LinkStack<char> &s)
{
	if(s.get()=='(') 
		cout<<"期望\")\"\n";
	else if(s.get()=='{')
		cout<<"期望\"}\"\n";
	else
		cout<<"期望\"[\"\n";
	return false;
}
bool func3(LinkStack<char> &s)
{
	if(s.get()=='[') 
		cout<<"期望\"]\"\n";
	else if(s.get()=='(')
		cout<<"期望\")\"\n";
	else
		cout<<"期望\"{\"\n";
	return false;
}
void main()
{
	LinkStack<char> s;
	char *p="((())";
	bool tag=true;
	while(*p!='\0'&&tag)
	{
		switch(*p)
		{
		case '(':
		case '[':
		case '{':s.push(*p);break;
		case ')':if(s.get()=='(') 
				 {
					 tag=true;
					 s.pop();
				 }
			else tag=func1(s);break;
		case ']':if(s.get()=='[')
				 {
					 s.pop();
					 tag=true;
				 }
			else tag=func2(s);break;
		case '}':if(s.get()=='{')
				 {
					 s.pop();
					 tag=true;
				 }
			else tag=func3(s);break;
		}
		p++;
	}
	if(!s.isEmpty()||!tag)
		cout<<"表达式不正确!\n";
	else
		cout<<"表达式正确!\n";
}