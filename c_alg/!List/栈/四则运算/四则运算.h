#include"LinkStack.h"
#include<string.h>
#include<math.h>
char* toPostfix(char *expstr)
{
	LinkStack<char> s1;
	char *poststr=new char[strlen(expstr)+1];
	char out=expstr[0];
	int i=0,j=0;
	//if(out=='+'||out=='-')
	//{
	//	poststr[j++]=
	while(expstr[i]!='\0')
	{
	//	cout<<"expstr["<<i<<"]="<<expstr[i];
		switch(expstr[i])
		{
		case '+':
		case '-':
			while(!s1.isEmpty()&&s1.get()!='(')
				poststr[j++]=s1.pop();
			s1.push(expstr[i++]);
			break;
		case '*':
		case '/':
			while(!s1.isEmpty()&&(s1.get()=='*'||s1.get()=='/'))
				 poststr[j++]=s1.pop();
			s1.push(expstr[i++]);
			break;
		case '(':
			s1.push(expstr[i++]);break;
		case ')':
			out=s1.pop();
			while(!s1.isEmpty()&&out!='(')
			{
				poststr[j++]=out;
				out=s1.pop();
			}
			i++;
			break;
		default:
			while((expstr[i]>='0'&&expstr[i]<='9'&&expstr[i]!='\0')||(expstr[i]=='.'))
				poststr[j++]=expstr[i++];
			poststr[j++]=' ';
			break;
		}
		/*poststr[j]='\0';
		cout<<"poststr="<<poststr<<"\t";
		cout<<s1;*/
	}
	
	while(!s1.isEmpty())
		poststr[j++]=s1.pop();
	poststr[j]='\0';	
	return poststr;
}
double calc(char *poststr)
{
	LinkStack<double> s2;
	char *p=poststr;
	double x,y,z,s;
	int i=0,l;
	while(p[i]!='\0')
	{
		s=0;
		if(p[i]>='0'&&p[i]<='9')
		{	
			z=0;
			l=1;
			
			while(p[i]!=' '&&p[i]!='.')
			{
				z=z*10+p[i]-'0';
				i++;
			}
			if(p[i]=='.')
			{
				i++;
				while(p[i]!=' ')
					s=s+(p[i++]-'0')*pow(10,-(l++));				
			//	cout<<s<<endl;
			}
			s2.push(z+s);
		}
		else 
		{
			if(p[i]!=' ')
			{
				y=s2.pop();
				x=s2.pop();
				switch(p[i])
				{
				case '+':z=x+y;break;
				case '-':z=x-y;break;
				case '*':z=x*y;break;
				case '/':z=(double)x/(double)y;break;
				}
				s2.push(z);
			}
		}
		i++;
	}	
	return s2.pop();
}


char* Postfix(char *expstr)
{
	LinkStack<char> s1;
	char *poststr=new char[strlen(expstr)+1];
	char out;
	int i=0,j=0;
	while(expstr[i]!='\0')
	{
	//	cout<<"expstr["<<i<<"]="<<expstr[i];
		switch(expstr[i])
		{
		case '+':
		case '-':
			while(!s1.isEmpty()&&s1.get()!='(')
				poststr[j++]=s1.pop();
			s1.push(expstr[i++]);
			break;
		case '*':
		case '/':
			while(!s1.isEmpty()&&s1.get()=='*'||s1.get()=='/')
				 poststr[j++]=s1.pop();
			s1.push(expstr[i++]);
			break;
		case '(':
			s1.push(expstr[i++]);break;
		case ')':
			out=s1.pop();
			while(!s1.isEmpty()&&out!='(')
			{
				poststr[j++]=out;
				out=s1.pop();
			}
			i++;
			break;
		default:
			while(expstr[i]>='0'&&expstr[i]<='9'&&expstr[i]!='\0')
				poststr[j++]=expstr[i++];
			poststr[j++]=' ';
			break;
		}
		/*poststr[j]='\0';
		cout<<"poststr="<<poststr<<"\t";
		cout<<s1;*/
	}	
	while(!s1.isEmpty())
		poststr[j++]=s1.pop();
	poststr[j]='\0';	
	return poststr;
}