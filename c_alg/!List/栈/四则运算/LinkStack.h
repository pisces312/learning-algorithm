#include"LSNode.h"
template<class T>
class LinkStack
{
	public:
		LSNode<T> *top;
		LinkStack();
		~LinkStack();
		bool isEmpty()const;
		bool push(T k);
		T pop();
		T get();
		friend ostream& operator<<(ostream& out,LinkStack<T> &s);
};
template<class T>
LinkStack<T>::LinkStack()
{
	top=NULL;
}
template<class T>
LinkStack<T>::~LinkStack()
{
	LSNode<T> *p=top,*q;
	while(p!=NULL)
	{
		q=p;
		p=p->next;
		delete q;
	}
	top=NULL;
}
template<class T>
bool LinkStack<T>::isEmpty()const
{
	return top==NULL;
}
template<class T>
bool LinkStack<T>::push(T k)
{
	LSNode<T> *q=new LSNode<T>(k);
	q->next=top;
	top=q;
	return true;
}
template<class T>
T LinkStack<T>::pop()
{
	T k;
	if(!isEmpty())
	{
		k=top->data;
		LSNode<T> *p=top;
		top=top->next;
		delete p;
	}
	return k;
}
template<class T>
T LinkStack<T>::get()
{
	if(!isEmpty())
		return top->data;
	return 0;
}
template<class T>
ostream& operator<<(ostream& out,LinkStack<T> &s)
{
	LSNode<T> *p=s.top;
	while(p!=NULL)
	{
		cout<<p->data<<" ";
		p=p->next;
	}
	cout<<endl;
	return out;
}

