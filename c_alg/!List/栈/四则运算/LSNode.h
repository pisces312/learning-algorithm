#include<iostream.h>
template<class T>
class LSNode
{
	public:
		T data;
		LSNode<T> *next;
		LSNode(T n)
		{
			data=n;
			next=NULL;
		}
};