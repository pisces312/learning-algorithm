#include<iostream.h>
typedef char dataType;
class SeqStack
{
private:
	char *pt;
	int size;
	int top;
public:
	SeqStack(int n=0);
	~SeqStack();
	bool isEmpty()
	{
		return (pt==NULL||top==-1);
	}
	bool isFull()
	{
		return (top>=size||pt==NULL);
	}
	bool push(const dataType n);
	dataType pop();
	dataType get();
	friend ostream &operator<<(ostream &out,SeqStack &s);
};
SeqStack::SeqStack(int n)
{
	pt=NULL;
	if(n>0)
		pt=new dataType[n];
	size=n;
	top=-1;
}
SeqStack::~SeqStack()
{
	delete []pt;
	size=0;
	top=-1;
}
bool SeqStack::push(dataType n)
{
	if(!isFull())
	{
		pt[++top]=n;
		return true;
	}
	else
	{
		cerr<<"栈已满"<<n<<"值无法入栈!\n";
		return false;
	}
}
dataType SeqStack::pop()
{
	if(!isEmpty())	return pt[top--];
	else
	{
		cerr<<"栈已空!\n";
		return -1;
	}
}
dataType SeqStack::get()
{
	if(!isEmpty())
		return pt[top];
	else
		cerr<<"栈已空，无法获得栈顶数据元素值!\n";
	return -1;
}
ostream& operator<<(ostream& out,SeqStack &s)
{
	for(int i=0;i<=s.top;i++)
		cout<<s.pt[i];
	cout<<endl;
	return out;
}


