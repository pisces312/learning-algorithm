#include"TLNode.h"
#include<stdlib.h>
class TRQ
{
public:
	TLNode *head;
	TRQ();
	TRQ(int n);
	~TRQ();
	int TRQ::length()const;
	bool isEmpty() const
	{
		return head==NULL;
	}
	bool isFull() const
	{
		return false;
	}
	bool isExist(int i) const;
	void output() const;
	void InQueue(int n);
	void DeQueue();
};

TRQ::TRQ()
{
	head=new TLNode;
	head->prior=head;
	head->next=head;
}
TRQ::TRQ(int n)
{
	head=new TLNode;
	TLNode *p=head;
	if(n<0)
	{
		cout<<"输入错误!\n";
		exit(1);
	}
	for(int i=0;i<n;)
	{
		p->next=new TLNode;
		p->next->prior=p;
		p=p->next;
		p->data=++i;
		cout<<i<<"入队!\n";
	}
	p->next=head;
	head->prior=p;
}

void TRQ::InQueue(int n)
{
	TLNode *s=new TLNode;
	s->data=n;
	head->prior->next=s;
	head->prior=s;	
	s->next=head;
	cout<<n<<"入队!\n";
}
void TRQ::DeQueue()
{
	TLNode *t=head->next;
	t->next->prior=t->prior;
	head->next=t->next;
	cout<<t->data<<"出队!\n";
	delete t;
}
TRQ::~TRQ()
{
	TLNode *p=head->next,*t;
	while(p!=head)
	{
		t=p;
		p=t->next;
		delete t;
	}
	head=NULL;
}
bool TRQ::isExist(int i) const
{
	if(i<1||i>length()) 
	{
		cout<<"输入错误! 结点"<<i<<"不存在!\n";
		return false;
	}
	return true;
}

int TRQ::length()const
{
	TLNode *p=head->next;
	int i=0;
	while(p!=head)
	{
		i++;
		p=p->next;
	}
	return i;
}
void TRQ::output() const
{
	TLNode *p=head->next;
	cout<<"Queue: ";
	if(head->next==head)
	{
		cout<<"空队列\n";
	}
	while(p!=head)
	{
		cout<<p->data;
		p=p->next;
		if(p!=head) cout<<"->";
	}
	cout<<endl;
}