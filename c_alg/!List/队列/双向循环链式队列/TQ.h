#include"TLNode.h"
#include<stdlib.h>
class TQ
{
public:
	TLNode *head;
	TQ();
	TQ(int n);
	~TQ();
	int TQ::length()const;
	bool isEmpty() const
	{
		return head==NULL;
	}
	bool isFull() const
	{
		return false;
	}
	bool isExist(int i) const;
	TLNode* index(int i) const;
	int get(int i) const;
	bool set(int i,int n);
	TLNode* insertbefore(TLNode* p,int n);
	TLNode* insertafter(TLNode* p,int n);
	bool insertbefore(int i,int n);
	bool insertafter(int i,int n);
	bool remove(TLNode* p);
	bool remove(int i);
	void reverse();
	void reverseoutput() const;
	void output() const;
	void output(TLNode *p) const;
};

TQ::TQ()
{
	head=new TLNode;
}
TQ::TQ(int n)
{
	head=new TLNode;
	TLNode *p=head;
	if(n<0)
	{
		cout<<"�������!\n";
		exit(1);
	}
	for(int i=0;i<n;)
	{
		p->next=new TLNode;
		p->next->prior=p;
		p=p->next;
		p->data=++i;
	}
}
TQ::~TQ()
{
	TLNode *p=head,*t;
	while(p!=NULL)
	{
		t=p;
		p=t->next;
		delete t;
	}
	head=NULL;
}
bool TQ::isExist(int i) const
{
	if(i<1||i>length()) 
	{
		cout<<"�������! ���"<<i<<"������!\n";
		return false;
	}
	return true;
}

int TQ::length()const
{
	TLNode *p=head->next;
	int i=0;
	while(p!=NULL)
	{
		i++;
		p=p->next;
	}
	return i;
}
TLNode* TQ::index(int i) const
{
	if(!isExist(i)) exit(1);
	TLNode *p=head->next;
	int j=0;
	while(p!=NULL&&j<i-1)
	{
		j++;
		p=p->next;
	}
	return p;
}
int TQ::get(int i) const
{
	if(!isExist(i)) exit(1);
	TLNode *p=head->next;
	int j=0;
	while(p!=NULL&&j<i-1)
	{
		j++;
		p=p->next;
	}
	return p->data;
}
bool TQ::set(int i,int n)
{
	if(!isExist(i)) 
	{
		cout<<"����ʧ��!\n";
		return false;
	}
	TLNode *p=head->next;
	int j=0;
	while(p!=NULL&&j<i-1)
	{
		j++;
		p=p->next;
	}
	p->data=n;
	return true;
}
TLNode* TQ::insertafter(TLNode* p,int n)
{
	TLNode *q=new TLNode(n);
	q->prior=p;
	q->next=p->next;
	if(p->next!=NULL)
	{
		p->next->prior=q;
	}
	p->next=q;
	return q;
}
TLNode* TQ::insertbefore(TLNode* p,int n)
{
	return insertafter(p->prior,n);
}
bool TQ::insertbefore(int i,int n)
{
	insertbefore(index(i),n);
	return true;
}
bool TQ::insertafter(int i,int n)
{
	insertafter(index(i),n);
	return true;
}
bool TQ::remove(TLNode* p)
{
	TLNode *t=p->next;
	p->prior->next=t;
	if(t!=NULL)
	{
		t->prior=p->prior;	
	}
	delete p;
	return true;
}
bool TQ::remove(int i)
{
	return remove(index(i));
}
void TQ::output(TLNode *p) const
{
	cout<<"Two Link: ";
	if(head->next==NULL)
	{
		cout<<"�ձ�\n";
	}
	while(p!=NULL)
	{
		cout<<p->data;
		p=p->next;
		if(p!=NULL) cout<<"->";
	}
	cout<<endl;
}
void TQ::output() const
{
	output(head->next);
}
void TQ::reverse()
{
	TLNode *p=head->next,*q=NULL,*t=NULL;
	while(p!=NULL)
	{
		q=p->next;
		p->next=t;
		p->prior=q;
		t=p;
		p=q;
	}
	head->next=t;
}
void TQ::reverseoutput() const
{
	TLNode *p=head;
	while(p->next!=NULL)
	{
		p=p->next;
	}
	cout<<"Reversed Two Link: ";
	while(p->prior!=NULL)
	{
		cout<<p->data;
		p=p->prior;
		if(p->prior!=NULL) cout<<"->";
	}
	cout<<endl;
}




