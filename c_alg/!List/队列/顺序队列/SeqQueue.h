#include<iostream.h>
#include"LinkStack.h"
typedef char dataType;
class SeqQueue
{
public:
	dataType *table;
	int size,front,rear;
public:
	SeqQueue(int n=0);
	SeqQueue(SeqQueue &s);
	~SeqQueue();
	bool isEmpty();
	bool isFull();
	bool enQueue(const dataType k);
	dataType deQueue();
	friend ostream& operator<<(ostream* out,SeqQueue &q);
	void Reverse();
	void Output();
	
};
SeqQueue::SeqQueue(SeqQueue &s)
{
	size=s.size;
	table=new dataType[s.size];
	for(int i=0;i<size;i++)
		table[i]=s.table[i];
	front=s.front;
	rear=s.rear;
}

void SeqQueue::Output()
{
	SeqQueue s(*this);
	while(!s.isEmpty())
		cout<<s.deQueue()<<" ";
	cout<<endl;
	if(isEmpty()) cout<<"队列已空!\n";
}
void SeqQueue::Reverse()
{
	int n;
	LinkStack<dataType> s;
	while(!isEmpty())
	{
		n=deQueue();
		s.push(n);
	}
	while(!s.isEmpty())
	{
		n=s.pop();
		enQueue(n);
	}
}


SeqQueue::SeqQueue(int n)
{
	size=n+1;
	table=new dataType[size];
	front=rear=0;
}
bool SeqQueue::isEmpty()
{
	return front==rear;
}
bool SeqQueue::isFull()
{
	return front==(rear+1)%size;
	//wrong!! return front==rear%size+1;
}
SeqQueue::~SeqQueue()
{
	delete[] table;
	size=0;
	front=0;
	rear=0;
}
bool SeqQueue::enQueue(const dataType n)
{
	if(!isFull())
	{
		table[rear]=n;
		rear=(rear+1)%size;
		return true;
	}
	cout<<"队列已满\n";
	return false;
}
dataType SeqQueue::deQueue()
{
	dataType n=0;
	if(!isEmpty())
	{
		n=table[front];
		front=(front+1)%size;
	}
	return n;
}
ostream& operator<<(ostream& out,SeqQueue &q)
{

	SeqQueue s(q);
	while(!s.isEmpty())
		cout<<s.deQueue()<<" ";
	cout<<endl;
	if(q.isEmpty()) cout<<"队列已空!\n";
	return out;
	
	
	
	/*int i=q.front;
	while(!q.isEmpty()&&i<(q.rear+q.size)%q.size)
	{
		cout<<q.table[i]<<" ";
		i=(i+1)%q.size;
	}
	if(i==q.front) cout<<"队列为空!\n";
	cout<<endl;
	return out;*/
}