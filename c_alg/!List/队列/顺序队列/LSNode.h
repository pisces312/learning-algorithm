#include<iostream.h>
template<class T>
class LSNode
{
	public:
		T data;
		LSNode<T> *next;
		LSNode(T& k,LSNode<T> *nextnode=NULL)
		{
			data=k;
			next=nextnode;
		}
		~LSNode()
		{}
};