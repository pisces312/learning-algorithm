#include"SRLNode.h"
#include<stdlib.h>
class SRQ
{
public:
	SRLNode *rear;
	SRQ();
	SRQ(int n);
	~SRQ();
	int SRQ::length()const;
	bool isEmpty() const
	{
		return rear->next==rear;
	}
	bool isFull() const
	{
		return false;
	}
	bool output() const;
	void InQueue(int n);
	bool DeQueue();
	void SetEmpty();
};

void SRQ::InQueue(int n)
{
	SRLNode *s=new SRLNode(n),*t;
	t=rear->next;
	rear->next=s;
	rear=s;
	rear->next=t;
	cout<<n<<"入队!\n";
}
bool SRQ::DeQueue()
{

	SRLNode *t=rear->next;	
	if(t!=rear)
	{
		rear->next=t->next;
		
		cout<<t->next->data<<"出队!\n";
		delete t;
		return true;
	}
	cout<<"队列已空!\n";
	return false;
}
SRQ::SRQ()
{
	rear=new SRLNode;
	rear->next=rear;
}
SRQ::SRQ(int n)
{
	rear=new SRLNode;
	rear->next=rear;	
	SRLNode *p=rear;
	if(n<0)
	{
		cout<<"输入错误!\n";
		exit(1);
	}
	for(int i=0;i<n;)
	{
		p->next=new SRLNode;
		p=p->next;
		p->data=++i;		
	}
	p->next=rear;
	rear=p;
}
SRQ::~SRQ()
{
	SRLNode *p=rear->next,*t;
	while(p!=rear)
	{
		t=p;
		p=t->next;
		delete t;
	}
	delete rear;
}
int SRQ::length()const
{
	SRLNode *p=rear->next->next;
	int i=0;
	while(p!=rear->next)
	{
		i++;
		p=p->next;
	}
	return i;
}

void SRQ::SetEmpty()
{
	int l=length();
	if(l==0) cout<<"队列已空!\n";
	for(int i=0;i<l;i++)
		DeQueue();
}
		
bool SRQ::output() const
{
	SRLNode *p=rear->next->next;
	if(rear->next==rear)
	{
		cout<<"空队列\n";
		return false;	
	}
	cout<<"Queue: ";
	while(p!=rear->next)
	{
		cout<<p->data;
		p=p->next;
		if(p!=rear->next) cout<<"->";
	}
	cout<<endl;
	return true;
}