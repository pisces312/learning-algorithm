#include<iostream.h>
typedef int dataType;
class SeqQueue
{
public:
	dataType *table;
	int size,front,rear;
	bool tag;
	SeqQueue(int n=0);
	~SeqQueue();
	bool isEmpty();
	bool isFull();
	bool enQueue(const dataType k);
	bool deQueue();
	dataType get();
	friend ostream& operator<<(ostream* out,SeqQueue &q);
};
SeqQueue::SeqQueue(int n)
{
	table=new dataType[n];
	size=n;
	front=rear=0;
	tag=false;
}
bool SeqQueue::isEmpty()
{
	return front==rear&&!tag;
}
bool SeqQueue::isFull()
{
	return front==rear&&tag;
}
SeqQueue::~SeqQueue()
{
	delete[] table;
	size=0;
	front=0;
	rear=0;
}
bool SeqQueue::enQueue(const dataType n)
{
	if(front==rear&&!tag)
	{
		cout<<"rear="<<rear<<" "<<"front="<<front<<endl;
		table[rear++]=n;
		cout<<n<<"入队!\n\n";
	return true;
	}
	if(!isFull())
	{
		table[rear]=n;
		cout<<"rear="<<rear<<" "<<"front="<<front<<endl;
		rear=(rear+1)%size;
		tag=true;
		cout<<n<<"入队!\n\n";

		return true;		
	}
	cout<<"队列已满,无法插入!\n";
	return false;
}
bool SeqQueue::deQueue()
{
	cout<<"rear="<<rear<<" "<<"front="<<front<<endl;
	if(!isEmpty())
	{
		cout<<table[front++]<<"出队!\n\n";
		tag=false;
		return true;
	}
	return false;
}
dataType SeqQueue::get()
{
	if(!isEmpty())
		return table[front];
	return -1;
}
ostream& operator<<(ostream& out,SeqQueue &q)
{
	cout<<"rear="<<q.rear<<" "<<"front="<<q.front<<endl;
	int i=q.front,end=q.rear;
//	cout<<q.table[q.rear]<<endl;
//	i=0;
	end=q.rear+q.size-q.front;
	//end=9;
	while(!q.isEmpty()&&i<end)
	{
		cout<<q.table[i]<<" ";
	//	i=(i+1)%q.size;
		i++;
	}
	cout<<endl;
	return out;
}