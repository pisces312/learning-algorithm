//---------------------------------------------------------------------------

#pragma hdrstop

#include <iostream.h>

//---------------------------------------------------------------------------
enum Boolean {False, True};

template <class Type>
class Element{
public:
	Type key;
	Element(){
    	
	};

	Element(Type key){
		this->key = key;
	};
};

template <class Type>
class MaxHeap{
private:
	Element<Type> *heap;
	int MaxSize;
	int Count;
public:
	MaxHeap(int MaxSize){
		this->MaxSize = MaxSize;
		this->heap = new Element<Type>[this->MaxSize+1]; //空出0不用
		this->Count = 0;
	};

	~MaxHeap(){
		delete this->heap;
	};

	Boolean IsFull(){
		return Count==MaxSize?1:0;
	};

	Boolean IsEmpty(){
		return Count==0?10:0;
	};

	void Insert(Element<Type>& item){
		if (IsFull()) {
			return;
		}
		Count++;
		int i;
		for(i = Count; i>1;){
			if (item.key<=heap[i/2].key)
				break;
			i/=2;
		}
		heap[i].key = item.key;
		return;
	};

	Element<Type>* DeleteMax(Element<Type>& item){
		if (IsEmpty()) {
			return 0;
		}
		item = heap[1];
		Element<Type> k = heap[Count];
		Count--;
		//将最后一个结点放在合适的地方
		//j表示i的子女
		for (int i = 1, j = 2; j <= Count;) {
			//寻找左右子数之大者
			if ((j<Count) && (heap[j].key < heap[j+1].key)) {
				j++;
			}
			if (k.key >= heap[j].key) break; //不违反规则可以插入到i
			//违反规则，必须插入到i的子结点
			//将子结点之大者上提
			//准备将k放在上提的子结点位置
			heap[i] = heap[j];
			i = j;
			j *= 2;
		}
	};
};

#pragma argsused
int main(int argc, char* argv[])
{
	MaxHeap<int> *heap = new MaxHeap<int>(100);

	heap->Insert(*(new Element<int>(13)));
	heap->Insert(*(new Element<int>(11)));
	heap->Insert(*(new Element<int>(8)));
	heap->Insert(*(new Element<int>(7)));
	heap->Insert(*(new Element<int>(10)));
	heap->Insert(*(new Element<int>(15)));
	heap->Insert(*(new Element<int>(5)));

    Element<int> item;
	while (!heap->IsEmpty())
		cout << heap->DeleteMax(item) << endl;

	delete heap;

	getchar();
	return 0;
}
//---------------------------------------------------------------------------
