#include"treeChildBrotherNode.h"
class cbTree
{
public:
	cbTreeNode* root;
	cbTree(int n);
	bool isEmpty();
	void setRootData(const dataType str);
	void attachChild(const dataType str);
	void attachBrother(const dataType str);
	void attachChildSub(cbTree &t);
	void attachBrotherSub(cbTree &t);
	void printTree(cbTreeNode *p,int n=1);
};
void cbTree::printTree(cbTreeNode *p,int n)
{
	if(p==NULL) return;
	printTree(p->child,n+1);
	for(int i=0;i<n-1;++i)
		cout<<"   ";
	cout<<p->data<<endl;
	printTree(p->brother,n+1);
}
void cbTree::attachChildSub(cbTree &t)
{
	if(isEmpty()||root->child!=NULL) return;
	else
	{
		root->child=t.root;
		t.root=NULL;
	}
}
void cbTree::attachBrotherSub(cbTree &t)
{
	if(isEmpty()||root->brother!=NULL) return;
	else
	{
		root->brother=t.root;
		t.root=NULL;
	}
}
void cbTree::attachBrother(const dataType str)
{
	if(isEmpty()||root->brother!=NULL) return;
	else
		root->brother=new cbTreeNode(str);
	return;
}
void cbTree::attachChild(const dataType str)
{
	if(isEmpty()||root->child!=NULL) return;
	else
		root->child=new cbTreeNode(str);
}
cbTree::cbTree(int n)
{
	root=new cbTreeNode[n];
}
bool cbTree::isEmpty()
{
	return root==NULL;
}
void cbTree::setRootData(const dataType str)
{
	if(!isEmpty())
		root->data=str;
	else
		root=new cbTreeNode(str);
}
