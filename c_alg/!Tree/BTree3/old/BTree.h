#ifndef BTREE_H
#define BTREE_H
#include<iostream>
using namespace std;
template<class T>
class BTreeNode {
    public:
    BTreeNode<T> *parent;
    T* key;//????m+1
    int keyNum;//目前存储的关键字个数
    int maxKeyNum;
    BTreeNode(int m) {//传入的是阶数，即m
        if (m<2) {
            exit(1);
        }

        parent=NULL;
        childPtr=new BTreeNode<T>*[m];
        memset(childPtr,0,m*sizeof(BTreeNode<T>*));//指针设置为空
        //
        keyNum=0;
        maxKeyNum=m-1;
        //多一个节点用于插入时作为溢出，方便插入!!
        key=new T[m];//大小在初始时固定，db中每个索引节点占一个块
        phyPtr=new int*[maxKeyNum];
    }
    BTreeNode<T> **childPtr;
    int **phyPtr;//指向物理地址
    //必须保证插入后小于等于最大关键字数!!
    void insertKey(int pos,const T &k,BTreeNode<T>* ptr) {
        for (int i=keyNum-1;i>=pos;i--) {
            key[i+1]=key[i];//假设后一个是空的
            childPtr[i+2]=childPtr[i+1];
//childPtr[j]
        }
        key[pos]=k;
        //右邻指针pos+1
        childPtr[pos+1]=ptr;
        keyNum++;
    }
    int findFirstIndexLargerThan(const T &x) {
        int i=0;
        for (;i<keyNum&&key[i]<x;i++) {
//            cout<<key[i]<<endl;
        }

        if (i<keyNum&&key[i]>=x) {
            return i;
        }
        return -1;
    }
    //???
    //p从0开始
    void moveKeyTo(BTreeNode<T> *p,int startPos) {
//        p->keyNum=maxKeyNum+1-startPos;
//        int size=sizeof(T);
//        int len=size*p->keyNum;
//        cout<<len<<endl;
//        memcpy(p->key,key+startPos*size,len);
//        keyNum=startPos;
//
        for (int i=startPos,j=0,n=maxKeyNum;i<=n;i++,j++) {
            p->key[j]=key[i];
            p->childPtr[j]=childPtr[i];
        }
        p->keyNum=maxKeyNum+1-startPos;
        keyNum=startPos-1;
    }


    void test() {
        for (int i=0;i<maxKeyNum;i++) {
            cout<<childPtr[i]<<endl;
        }
    }
    void print() {
        cout<<"=====node=====\n";
        cout<<"| "<<childPtr[0]<<" | ";
        for (int i=0;i<keyNum;i++) {
            cout<<key[i]<<" | "<<childPtr[i]<<" | ";
        }
        cout<<"\n==============\n";
    }
    void printKey() {
        cout<<"=====node=====\n";
        for (int i=0;i<keyNum;i++) {
            cout<<key[i]<<" ";
        }
        cout<<"\n==============\n";
    }
};


//B+树索引节点
//template<class T>
//class BTreeIndexNode:public BTreeNode<T> {
//    public:
//    BTreeIndexNode(int n){
//        key=new T[n];
//        childPtr=new BTreeNode<T>*[n];
//    }
//    BTreeIndexNode() {
//        key=NULL;
//    }
//    BTreeNode<T> **childPtr;
//    //
//
//};
//template<class T>
//class BTreeLeafNode:public BTreeNode<T> {
//    public:
//
//    BTreeLeafNode(int n) {
//        key=new T[n];
//        childPtr=new int*[n];
//    }
//    BTreeLeafNode() {
//        key=NULL;
//    }
//
//    int **phyPtr;//指向物理地址
//};
template<class T>
struct Triple {
    BTreeNode<T> *r;
    int i;//节点中关键字序号
    bool tag;
};
template<class T>
class BTree {
    public:
//    static Triple<T> nullResult=new Triple<T>;
    BTreeNode<T> *root;//根指针
    BTreeNode<T> *minLeafNode;//关键字最小的叶节点

    int m;//子树个数
    BTree(int m2) {
        m=m2;
        root=new BTreeNode<T>(m);//每个索引节点最多m-1个关键字
        //for test
//        root->insertKey(0,2);
//        root->insertKey(1,7);
//        root->key[0]=2;
//        root->key[1]=7;
//        root->key[1]=4;
//        root->key[2]=7;
//        root->keyNum=2;


    }

    ~BTree() {
        //dtor
    }
    //搜索，用于插入，删除，更新，判断是否存在该关键字
    //结果中的下标停留在比x大的第一个元素位置上
    Triple<T>* search(const T&x) {
        Triple<T> *result=new Triple<T>;//NULL;

        //用于数据库时，每个节点都应从磁盘读取，如使用getNode
        //q指向父节点
        BTreeNode<T> *p=root,*q=NULL;
        int i=-1;
        while (p!=NULL) {
            i=0;
            p->key[p->keyNum]=10000;
            while (p->key[i]<x) i++;
            if (p->key[i]==x) {
                result->r=p;
                result->i=i;
                result->tag=false;
                return result;
            }
            q=p;
            p=p->childPtr[i];
        }
//        for (;p!=NULL;p=p->childPtr[i]) {
////            int index;
//            int n=p->keyNum;
//            for (;i<n&&p->key[i]<x;i++) {
//                cout<<p->key[i]<<endl;
//            }
//            if (i==n) {
//                i--;
//            }
//
////            if (i==0) {
////                cout<<"find value "<<x<<" in index "<<i<<endl;
//////                index=i;
//////                result=new Triple<T>;//(p,j,true);
////                result->r=p;
////                result->i=i;
////                result->tag=true;//已经存在
//////                break;
////                return result;
////            }
//
//            if (p->key[i]>=x) {
//                cout<<"find value "<<x<<" in index "<<i<<endl;
////                index=i;
////                result=new Triple<T>;//(p,j,true);
//                result->r=p;
//                result->i=i;
//                result->tag=true;//已经存在
////                break;
//                return result;
//            }
//            q=p;
//        }
        result->r=q;
        result->i=i;
        result->tag=true;
        return result;
    }
    Triple<T>* search2(const T&x) {
        Triple<T> *result=new Triple<T>;//NULL;

        //用于数据库时，每个节点都应从磁盘读取，如使用getNode
        //q指向父节点
        BTreeNode<T> *p=root,*q=NULL;
        int i=0;
        for (;p!=NULL;p=p->childPtr[i]) {
//            int index;
            int n=p->keyNum;
            for (;i<n&&p->key[i]<x;i++) {
                cout<<p->key[i]<<endl;
            }
            if (i==n) {
                i--;
            }
            if (p->key[i]==x) {
                cout<<"find value "<<x<<" in index "<<i<<endl;
//                index=i;
//                result=new Triple<T>;//(p,j,true);
                result->r=p;
                result->i=i;
                result->tag=true;//已经存在
//                break;
                return result;
            }
            q=p;
        }
        result->r=q;
        result->i=i;
        result->tag=false;
        return result;
    }
//搜索，用于插入，删除，更新，判断是否存在该关键字
    //结果中的下标停留在比x大的第一个元素位置上
    Triple<T>* findPosToInsert(const T&x) {
        Triple<T> *result=new Triple<T>;//NULL;

        //用于数据库时，每个节点都应从磁盘读取，如使用getNode
        //q指向父节点
        BTreeNode<T> *p=root,*q=NULL;
        if (p!=NULL&&p->keyNum==0) {
            result->r=p;
            result->i=0;
            result->tag=true;//已经存在
            return result;
        }
//        int index=0;
        int i=0;
        for (;p!=NULL;) {
            int index=p->findFirstIndexLargerThan(x);
            if (index>=0) {
                result->r=p;
                result->i=index;
                result->tag=true;//已经存在
//                break;
                return result;
            }
            if (i==0) {
                q=p;
                p=q->childPtr[i];
                i++;
            } else if (i<p->keyNum) {
                p=q->childPtr[i];
                i++;
            } else {
                i=0;
                q=p;
                p=q->childPtr[i];
                i++;
            }


        }
//        result->r=q;
//        result->i=index;
        result->tag=false;
        return result;
    }
    Triple<T>* findPosToInsert2(const T&x) {
        Triple<T> *result=new Triple<T>;//NULL;

        //用于数据库时，每个节点都应从磁盘读取，如使用getNode
        //q指向父节点
        BTreeNode<T> *p=root,*q=NULL;
        int i=0;
        while (p!=NULL) {
            for (i=0;i<p->keyNum;i++) {
                if (p->key[i]==x) {
                    result->r=p;
                    result->i=i;
                    result->tag=true;
                    return result;
                } else if (p->key[i]>x) {
                    break;
                }


            }
            q=p;
            p=p->childPtr[i];
        }
        result->r=q;
        result->i=i;
        result->tag=false;
        return result;
    }

    //插入一个关键字
    //这里
    bool insert(const T&key) {
        Triple<T> *loc=findPosToInsert2(key);
//        Triple<T> *loc=findPosToInsert(key);
//        if (loc->tag) {//如果找到，则说明不用插入，插入失败
////            cout<<"have already exist!\n";
//            cout<<"没有找到插入的位置!\n";
//            return false;
//        }
        //ap为插入关键字x的右邻指针
        BTreeNode<T> *p=loc->r,*q=NULL,*ap=NULL,*t;
        T k=key;
        int j=loc->i;
        while (true) {
            //节点关键字个数未超出
            if (p->keyNum<m-1) {
                cout<<p->keyNum<<"<"<<(m-1)<<endl;
                p->insertKey(j,k,ap);
                return true;
            }
            //分裂
            int s=(m+1)/2;
            p->insertKey(j,k,ap);
            //建立新节点
            q=new BTreeNode<T>(m);
            p->moveKeyTo(q,s+1);
            cout<<"-------\n";
            //            p->print();
            //            q->print();
            //(key,ap)形成向上插入二元组
            //即使用分裂出来的节点的最小的关键字移动到上一层
//            k=q->key[0];//p->key[s];
            k=p->key[s];//!!!!!!是中间的元素上去，！！
            ap=q;//上一层的指针指向这个新节点
            //
            t=p->parent;
            if (t!=NULL) {//原来节点有父节点
                //则将分裂后复制上去的节点填到父节点中
                j=t->findFirstIndexLargerThan(k);
                if (j==-1) {
                    cout<<"j="<<j<<endl;
                    return false;
                }
                //将分裂的节点的父节点也设置为原节点的，因为他们在同一层，
                //有共同的父节点
                q->parent=t;
                //原节点上移一层，因为复制到父节点后可能继续分裂
                p=t;

            } else {//分裂到最顶层，需要将根分裂，并建立新的根
                root=new BTreeNode<T>(m);
                root->key[0]=k;
                root->keyNum=1;
                //因为根至少两个指针
                root->childPtr[0]=p;//指向原节点
                root->childPtr[1]=ap;//指向新节点
                q->parent=p->parent=root;
                return true;
            }

        }

        //        const T &key=x
        return false;

    }
    void printTree() {
        //        BTreeNode<T> *p=root,*q;

        printNode(root);
    }
    void printNode(BTreeNode<T> *node) {
        if (node==NULL)
            return;
        node->printKey();
        for (int i=0;i<=node->keyNum;i++) {
            printNode(node->childPtr[i]);
        }


    }
    //    bool remove(T& x);
    protected:
    private:
};

#endif // BTREE_H
