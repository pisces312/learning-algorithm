#include <iostream>
#include"Btree.h"
#include<string>
using namespace std;
void testInteger() {
    //    BTreeNode<int> node(4);
//node.test();
    BTree<int> tree(4);
//    tree.root->insertKey(0,2,NULL);
//    tree.root->insertKey(1,7,NULL);
//    int v1=8;
//    Triple<int> *result=tree.search(v1);
//    if (result!=NULL) {
//        cout<<"=======\n";
//        cout<<result->i<<endl;
//        cout<<result->tag<<endl;
//        cout<<"=======\n";
//    }
    tree.insert(21);
//    tree.printTree();
    tree.insert(7);
//    tree.printTree();
    tree.insert(8);
//    tree.printTree();
    tree.insert(1);
//    tree.printTree();
//    tree.printTree();
    tree.insert(9);
//    tree.printTree();
    tree.insert(3);
//    tree.printTree();
    tree.insert(6);
//    tree.printTree();
    tree.insert(0);
//    tree.printTree();
    tree.insert(10);
//    tree.insert(10);
//    tree.insert(10);
    tree.insert(13);
//    tree.printTree();
    tree.insert(100);
//    tree.printTree();
    tree.insert(16);
//    tree.printTree();
    tree.insert(20);
    tree.insert(15);
    tree.insert(54);
    tree.insert(36);
    tree.printTree();
//    Triple<int> *result=tree.findPosToInsert(16,tree.root);
//    cout<<result->tag<<endl;
//    cout<<result->i<<endl;
//    result->r->printKey();
//    cout<<flag<<endl;
//    tree.root->print();
//    int a=4;
//    int &b=a;
//    int c=b;
//    cout<<c<<endl;
//    cout << "Hello world!" << endl;
}
void testString() {
    //!正确
    BTree<string> tree(4);
    tree.insert("z");
    tree.insert("a");
    tree.insert("c");
    tree.insert("y");
    tree.insert("a");
    tree.printTree();
}
void testRemoveRightAdjust() {
    int m=3;
    BTree<int> tree(m);
    tree.insert(10);
    tree.insert(30);
    tree.insert(40);
    tree.insert(50);
    tree.insert(55);
    tree.insert(58);
    tree.printTree();
    tree.remove(10);
    tree.remove(50);
    tree.remove(58);
    tree.printTree();
}

void testRemove() {
    int m=3;
//    int x=30;
    BTree<int> tree(m);
    tree.insert(10);
    tree.insert(30);
    tree.insert(40);
//    tree.remove(x);
////    tree.printTree();
    tree.insert(50);
    tree.insert(55);
    tree.insert(58);
//////    tree.printTree();
//!!出错
    tree.insert(60);
    //
    //
    //
    tree.insert(65);
    tree.insert(70);
    tree.insert(75);
    tree.insert(80);

//    tree.printTree();

//    tree.remove(x);
//    x=80;
//    tree.remove(x);
//    cout<<"````````````````\n";
    tree.printTree();
    tree.remove(10);
////        tree.printTree();
//???出错
    tree.remove(50);
//    tree.remove(58);
//    tree.remove(30);
////    tree.printTree();
//    tree.remove(75);
//    tree.remove(65);
////    cout<<"````````````````\n";
////tree.root->printKey();
    tree.printTree();
}
int main() {
//    testInteger();
//!不正确
//    BTree<char *> tree(4);
//    tree.insert("z");
//    tree.insert("a");
//    tree.insert("c");
//    tree.insert("y");
//    tree.insert("a");
//    tree.printTree();
//
//    testString();
//!删除有问题
//    testRemove();
    testInteger();
//testRemoveRightAdjust();


    return 0;
}
