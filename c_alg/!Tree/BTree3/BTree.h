#ifndef BTREE_H
#define BTREE_H
#include<iostream>
using namespace std;
//!!对模板类型最好重载“=、<、>"
template<class T>
class BTreeNode {
    public:
    BTreeNode<T> *parent;
    T* key;//????m+1
    int keyNum;//目前存储的关键字个数
    int maxKeyNum;
    BTreeNode(int m) {//传入的是阶数，即m
        if (m<2) {
            exit(1);
        }

        parent=NULL;
        //实际最多m个指针，m-1个关键字，这里存储关键字的数组大小与指针数组大小相同
        //!里存储m+1个指针，有一个作为溢出使用
        //指针大小
        int ptrSize=m+1;
        childPtr=new BTreeNode<T>*[ptrSize];
        memset(childPtr,0,ptrSize*sizeof(BTreeNode<T>*));//指针设置为空
        //
        keyNum=0;
        maxKeyNum=m-1;
        //!!多一个节点用于插入时作为溢出，方便插入!!
        key=new T[m];//大小在初始时固定，db中每个索引节点占一个块
        //这里未用
        phyPtr=new int*[maxKeyNum];
    }
    BTreeNode<T> **childPtr;
    int **phyPtr;//指向物理地址
    //必须保证插入后小于等于最大关键字数!!
    void insertKey(int pos,const T &k,BTreeNode<T>* ptr) {
        for (int i=keyNum-1;i>=pos;i--) {
            key[i+1]=key[i];//假设后一个是空的
            childPtr[i+2]=childPtr[i+1];
//            childPtr[i+1]=childPtr[i];
//childPtr[j]
        }
        key[pos]=k;
        //右邻指针pos+1
//        if(pos
        childPtr[pos+1]=ptr;
        keyNum++;
    }
//    int findFirstIndexLargerThan(const T &x) {
//        int i=0;
//        for (;i<keyNum&&key[i]<x;i++) {
////            cout<<key[i]<<endl;
//        }
//
//        if (i<keyNum&&key[i]>=x) {
//            return i;
//        }
//        return -1;
//    }
    //???
    //p从0开始
    //移动到一个新的节点中
    void moveKeyTo(BTreeNode<T> *p,int startPos) {
//        p->keyNum=maxKeyNum+1-startPos;
//        int size=sizeof(T);
//        int len=size*p->keyNum;
//        cout<<len<<endl;
//        memcpy(p->key,key+startPos*size,len);
//        keyNum=startPos;
//
        int i,j,n;
        for (i=startPos,j=0,n=maxKeyNum;i<=n;i++,j++) {
            p->key[j]=key[i];
            p->childPtr[j]=childPtr[i];
            if (childPtr[i]) {
                p->childPtr[j]->parent=p;
            }
        }
        //!!否则造成少复制一个指针，指针比关键字要多复制一个
        p->childPtr[j]=childPtr[i];
        if (childPtr[i]) {
            p->childPtr[j]->parent=p;
        }
        p->keyNum=maxKeyNum+1-startPos;
        keyNum=startPos-1;
    }


    void test() {
        for (int i=0;i<maxKeyNum;i++) {
            cout<<childPtr[i]<<endl;
        }
    }
    void print() {
        cout<<"=====node=====\n";
        cout<<"| "<<childPtr[0]<<" | ";
        for (int i=0;i<keyNum;i++) {
            cout<<key[i]<<" | "<<childPtr[i]<<" | ";
        }
        cout<<"\n==============\n";
    }
    //j为相应父节点中的关键字位置
    void printKey() {
        cout<<"node: ";
        for (int i=0;i<keyNum;i++) {
            cout<<key[i]<<" ";
        }
        cout<<endl;
//        cout<<"\n==============\n";
    }
    //j为相应父节点中的关键字位置
    void printKey(int j) {
        cout<<"=====node=====\n";
        cout<<"parent="<<parent->key[j]<<endl;
        for (int i=0;i<keyNum;i++) {
            cout<<key[i]<<" ";
        }
        cout<<"\n==============\n";
    }
};


template<class T>
struct Triple {
    BTreeNode<T> *r;
    int i;//节点中关键字序号
    bool tag;
};
template<class T>
class BTree {
    public:
//    static Triple<T> nullResult=new Triple<T>;
    BTreeNode<T> *root;//根指针
    BTreeNode<T> *minLeafNode;//关键字最小的叶节点

    int m;//子树个数
    BTree(int m2) {
        m=m2;
        root=new BTreeNode<T>(m);//每个索引节点最多m-1个关键字
    }

    ~BTree() {
        //dtor
    }
    //搜索，用于插入，删除，更新，判断是否存在该关键字
    //结果中的下标停留在比x大的第一个元素位置上
    //!!!有问题
    Triple<T>* findPosToInsert(const T&x,BTreeNode<T> *begin) {
        Triple<T> *result=new Triple<T>;//NULL;

        //用于数据库时，每个节点都应从磁盘读取，如使用getNode
        //q指向父节点
        BTreeNode<T> *p=begin,*q=NULL;
        int i=0,ptrIndex=0;
        while (p!=NULL) {
            i=0;
            while (i<p->keyNum&&p->key[i]<x) i++;
            if (p->key[i]==x) {//不允许重复的索引
                result->r=p;
                result->i=i;
                result->tag=true;
                return result;
            }

//!!正确
//            for (i=0;i<p->keyNum;i++) {
//                if (p->key[i]==x) {
//                    result->r=p;
//                    result->i=i;
//                    result->tag=true;
//                    return result;
//                } else if (p->key[i]>x) {
//                    ptrIndex=i;
//                    break;
//                }
//            }
            q=p;
            p=p->childPtr[i];
        }
        result->r=q;
        result->i=i;
        result->tag=false;
        return result;
    }

    //插入一个关键字
    //这里
    //!!有问题，父节点
    bool insert(const T& key) {
        Triple<T> *loc=findPosToInsert(key,root);

//        Triple<T> *loc=findPosToInsert(key);
        if (loc->tag) {//如果找到，则说明不用插入，插入失败
            cout<<"have already exist!\n";
//            cout<<"没有找到插入的位置!\n";
            return false;
        }
        //ap为插入关键字x的右邻指针
        BTreeNode<T> *p=loc->r,*q=NULL,*ap=NULL,*t;
        T k=key;
        int j=loc->i;
        cout<<key<<endl;
        int s=m/2;//(m+1)/2;
        while (true) {
            //节点关键字个数未超出
            if (p->keyNum<m-1) {
//                cout<<p->keyNum<<"<"<<(m-1)<<endl;
                p->insertKey(j,k,ap);
                return true;
            }
            //!分裂?????
            //！区分子女数和关键字数的最小要求，移到while外

////int s=(m+1)/2-1;

            p->insertKey(j,k,ap);

//            if(key==60){
//                cout<<endl;
//            }
            //建立新节点
            q=new BTreeNode<T>(m);


            p->moveKeyTo(q,s+1);
            //!!连续多次分裂，会导致父母节点错误
            //在moveKeyTo中实现
//            for (int i=0;i<=q->keyNum;i++) {
//                if (q->childPtr[i]!=NULL)
//                    q->childPtr[i]->parent=q;
//            }
//            q->childPtr

//            cout<<"-------\n";
            //            p->print();
            //            q->print();
            //(key,ap)形成向上插入二元组
            //即使用分裂出来的节点的最小的关键字移动到上一层
//            k=q->key[0];//p->key[s];
            k=p->key[s];//!!!!!!是中间的元素上去，！！
            ap=q;//上一层的指针指向这个新节点
            //
            t=p->parent;
            if (t!=NULL) {//原来节点有父节点
                printTree();
                j=0;
                //???找合适的插入关键字的位置
                while (j<t->keyNum&&t->key[j]<k) j++;
                //将分裂的节点的父节点也设置为原节点的，因为他们在同一层，
                //有共同的父节点

                q->parent=t;
//                t->childPtr[j]=p;
//                t->childPtr[j+1]=ap;
//                q->parent=p->parent;
                //原节点上移一层，因为复制到父节点后可能继续分裂
                p=t;

            } else {//分裂到最顶层，需要将根分裂，并建立新的根
                printTree();
                root=new BTreeNode<T>(m);
                root->key[0]=k;
                root->keyNum=1;
                //因为根至少两个指针
                root->childPtr[0]=p;//指向原节点
                root->childPtr[1]=ap;//指向新节点
                q->parent=root;
                p->parent=root;
//                root=t;

//
//
//                root=new BTreeNode<T>(m);
//                root->key[0]=k;
//                root->keyNum=1;
//                //因为根至少两个指针
//                root->childPtr[0]=p;//指向原节点
//                root->childPtr[1]=ap;//指向新节点
//                q->parent=p->parent=root;
                return true;
            }

        }

        //        const T &key=x
        return false;

    }
//    bool remove(const T& x){
//
//    }
    void printNode(BTreeNode<T> *node) {
        if (node==NULL)
            return;
//            cout<<"{ ";
//        if (node->parent!=NULL&&node->parent->keyNum>0) {
//            cout<<"parent ";
//            node->parent->printKey();
//            cout<<"----------------------\n";
//        }
        node->printKey();
//        cout<<"}\n";



        for (int i=0;i<=node->keyNum;i++) {
//            cout<<node->key[i]<<"child "<<i<<" ";
            printNode(node->childPtr[i]);
        }
//        printNode(node->childPtr[i]);


    }
    void printNode(BTreeNode<T> *node,int j) {
        if (node==NULL)
            return;
        node->printKey(j);
        int i=0;
        for (;i<node->keyNum;i++) {
//            cout<<node->key[i]<<"child "<<i<<" ";
            printNode(node->childPtr[i],i);
        }
        printNode(node->childPtr[i],i-1);


    }
    void printTree() {
        //        BTreeNode<T> *p=root,*q;
        cout<<"BTree dump==========\n";

        printNode(root);//,0);
        cout<<"======dump finished====\n";
    }

    /*
    由于节点内部空间连续分布，删除后需要压缩
    这里包含节点的减少
    */
//    void compressNodeAfterDel(BTreeNode<T> *p,int startPos){
//
//        for(int i=startPos,n=p->keyNum;i<n;i++){
//            p->key[i]=p->key[i+1];
////            p->childPtr[i]=
//
//        }
//    }
//!压缩
    void delAndCompressNodeOneKey(BTreeNode<T> *p,int posToDel) {
        //!只删除该位置关键字和右侧的指针，不删除左侧的！！！
        //???删除时，如果只剩一个，
        int i=posToDel;//???
        for (int n=p->keyNum;i<n;i++) {
            p->key[i]=p->key[i+1];
            p->childPtr[i+1]=p->childPtr[i+2];
        }
//        p->childPtr[i]=p->childPtr[i+1];
        p->keyNum--;

    }
    //右节点合并到左节点
    //j是父节点中要移动的关键字的下标
    void merge(BTreeNode<T> *left,BTreeNode<T> *parent,BTreeNode<T> *right,int j) {

        int len=left->keyNum;
//!父节点下移一个关键字添加到左节点
        left->key[len]=parent->key[j];

        //???!!!问题，如果left为空，右节点的第一个指针作为左节点的
        //第一个指针
        left->childPtr[len+1]=right->childPtr[0];
        //
        int i=0;
        for (int n=right->keyNum;i<n;i++) {
            //!要+1！！
            left->key[len+i+1]=right->key[i];
            //从右子树的第二个指针复制，第一个已在前面赋值过
            left->childPtr[len+i+2]=right->childPtr[i+1];
//            left->childPtr[left->keyNum+i+2]=right->childPtr[i+1];
        }
        left->childPtr[len+i+2]=right->childPtr[i+1];
        delAndCompressNodeOneKey(parent,j);
        left->keyNum=len+right->keyNum+1;//加上右节点的个数和父节点下移的节点
        delete right;
    }
    /*
    min是最小阶数
    j是父节点指向left的指针的下标)??
    */

    void leftAdjust(BTreeNode<T> *leftChild,BTreeNode<T> *parent,int min,int j) {
//                cout<<"1\n";
//        printTree();
        BTreeNode<T> *rightChild=parent->childPtr[j+1];//右兄弟,j+1表示该元素对应的右子树
        if (rightChild->keyNum>min-1) {//向上移动一个后保证不低于最小关键字个数

            leftChild->key[leftChild->keyNum]=parent->key[j];
            leftChild->keyNum++;//!!该语句位置
            leftChild->childPtr[leftChild->keyNum]=rightChild->childPtr[0];

            //
            parent->key[j]=rightChild->key[0];
            delAndCompressNodeOneKey(rightChild,0);
            //
//            rightChild->childPtr[0]=rightChild->childPtr[1];
        } else {//右节点不足最小节点数要求，需要与左节点合并
            //????
            merge(leftChild,parent,rightChild,j);//???
        }


    }

    //j是父节点中指向rightChild的指针下标
    void rightAdjust(BTreeNode<T> *rightChild,BTreeNode<T> *parent,int min,int j) {
        cout<<"rightAdjust\n";
        BTreeNode<T> *leftChild=parent->childPtr[j-1];
        //左兄弟空间还够, 不用合并, 仅做调整
        if (leftChild->keyNum>min-1) {//向上移动一个后保证不低于最小关键字个数
            //insertKey中数量增一
            //保存第一个关键字右侧指针，避免insertKey中移动指针覆盖原值
//            BTreeNode<T> *t=rightChild->childPtr[0];
//
//            rightChild->insertKey(0,parent->key[j-1],rightChild->childPtr[0]);
//!正确
            rightChild->childPtr[rightChild->keyNum+1]=rightChild->childPtr[rightChild->keyNum];
            for (int i=rightChild->keyNum;i>=1;i--) {
                rightChild->key[i]=rightChild->key[i-1];//向后移动一位
                rightChild->childPtr[i]=rightChild->childPtr[i-1];
            }
            rightChild->key[0]=parent->key[j-1];
//更改第一个指针
            rightChild->childPtr[0]=leftChild->childPtr[leftChild->keyNum];
            rightChild->keyNum++;//!!该语句位置
            //
            //
            parent->key[j-1]=leftChild->key[leftChild->keyNum-1];
            delAndCompressNodeOneKey(leftChild,leftChild->keyNum-1);
            //
//            rightChild->childPtr[0]=rightChild->childPtr[1];
        } else {//右节点不足最小节点数要求，需要与左节点合并
//            merge(rightChild,parent,leftChild,j);//???
            cout<<"right merge\n";
//???!!
            merge(leftChild,parent,rightChild,j-1);
        }

    }

    bool remove(const T& x) {
        Triple<T>* loc=findPosToInsert(x,root);
        if (!loc->tag) {
            cout<<"未找到\n";
            return false;

        }
        cout<<"remove "<<x<<endl;
        BTreeNode<T> *p=loc->r,*q,*s;
        int j=loc->i;
        //!
        s=p->childPtr[j+1];//!!!从右指针向下
        p->printKey();
//        s->printKey();
        //1节点的删除，分两种情况
        //非叶节点的删除
        if (s!=NULL) {//说明是非叶节点
            s->printKey();
            q=p;
            //!子树的最小关键字一定在右子树的最左侧！！
            while (s!=NULL) {
                q=s;
                s=s->childPtr[0];//最左
            }
            //!debug
            if (x==75) {
                cout<<"删除后用子树中最小的关键字来代替被删的\n";
                printNode(p);
                cout<<p->key[j]<<endl;
                cout<<q->key[0]<<endl;
                q->parent->printKey();
            }
            //
            //删除后用子树中最小的关键字来代替被删的
            p->key[j]=q->key[0];

            //删除子树中的最小关键字
            //!??
            delAndCompressNodeOneKey(q,0);
            if (x==75) {
                cout<<"after compress\n";
                cout<<q->key[0]<<endl;
            }
            p=q;//一定不为空//p=q=s 定位在被删除的叶节点
            if (x==75) {
                cout<<"parent ??\n";
                //???????????
                q->parent->printKey();
//                        p->printKey();
            }
        } else {//作为叶节点，直接删
            delAndCompressNodeOneKey(p,j);
        }
        cout<<"phrase 1\n";
        printTree();
        //以上正确
        //2调整，从下往上
        int d=(m+1)/2;
//        int d=m/2;//(m+1)/2;
//!
//        s=root->childPtr[0];
        while (true) {
            //小于最小值
            if (x==75) {
                cout<<endl;

            }
            if (p->keyNum<d-1) {
                j=0;
                q=p->parent;
                if (x==75) {
                    cout<<"parent 80\n";
                    q->printKey();
//                        p->printKey();
                }
                //找到p的父节点中指向p的指针下标
                while (j<=q->keyNum&&q->childPtr[j]!=p) {
                    j++;
                }
//                printTree();
                if (j==0) {
                    leftAdjust(p,q,d,j);
                } else {
                    if (x==75) {
                        cout<<"debug \n";
                        q->printKey();
                        p->printKey();
                    }

                    rightAdjust(p,q,d,j);
                }
//                printTree();
                p=q;
//                q->printKey();
//                root->printKey();
                if (p==root) {

//                    printTree();
                    break;
                }
            } else {
                break;
            }

        }
        //根结点为空时，用第一个子节点替换
        if (root->keyNum==0) {
//            printTree();

            p=root->childPtr[0];
            delete root;
            root=p;
//            root->parent=NULL;
        }

//        bool flag=NULL;
        return true;
    }


    protected:
    private:
};

#endif // BTREE_H
