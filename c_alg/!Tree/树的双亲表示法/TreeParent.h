#include"TPNode.h"
#include"iomanip.h"
#include"BTree.h"
class treeParent
{
public:
	TPNode* table;
	int end,size;
	treeParent(int n);
	treeParent(elemtype n);
	treeParent(treeParent &t);
	void setRoot(elemtype n);
	bool isFull()
	{
		return end+1==size;
	}
	int getSize()
	{
		return size;
	}
	int index(elemtype data);
	int attachChild(elemtype root,elemtype data);
	void copyTree(treeParent &t1,treeParent &t2);
	void copyNode(TPNode &p1,TPNode &p2);
	void printTree();
	void preOrderInitiate(elemtype n);
	void preOrder(elemtype n);
	void postOrder(elemtype n);
	void postOrderInitiate(elemtype n);
	BTree T2B();
	void btreeCreate(BTreeNode *p,TPNode *t);
};
BTree treeParent::T2B()
{
	BTree bt;
	bt.root=new BTreeNode;
	BTreeNode *p=bt.root;
	bt.setRootData(table[0].data);
	btreeCreate(p,&table[0]);		
	return bt;
}
void treeParent::btreeCreate(BTreeNode *p,TPNode *t)
{
	if(t==NULL) return;
	int a=index(t->data),count=1;	
	for(int i=a;i<size;i++)
		if(table[i].parent==a)
		{	
			if(count==1)
			{
				p->left=new BTreeNode(table[i].data);
				count++;
				btreeCreate(p->left,&table[i]);
			}
			else 
			{			
				p->right=new BTreeNode(table[i].data);
				count=1;
			}
		}
		
}


	
	
void treeParent::postOrder(elemtype n)
{
	int t=index(n);
	if(t<0) return;
	cout<<"�������˫����: ";
	postOrderInitiate(n);
	cout<<n<<endl;
}
void treeParent::postOrderInitiate(elemtype n)
{
	int t=index(n);	
	for(int i=t;i<end+1;i++)
		if(table[i].parent==t) 
		{			
			postOrderInitiate(table[i].data);
			cout<<table[i].data<<" ";			
		}
}

void treeParent::preOrder(elemtype n)
{
	int t=index(n);
	if(t<0) return;
	cout<<"�������˫����: "<<n<<" ";
	preOrderInitiate(n);
	cout<<endl;
}

void treeParent::preOrderInitiate(elemtype n)
{
	int t=index(n);	
	for(int i=t;i<end+1;i++)
		if(table[i].parent==t) 
		{
			cout<<table[i].data<<" ";
			preOrderInitiate(table[i].data);
		}
}
		

treeParent::treeParent(treeParent &t)
{
	int l=t.getSize();
	table=new TPNode[l];
	for(int i=0;i<l;i++)
		copyNode(table[i],t.table[i]);
	end=t.end;
	size=t.size;
}
void treeParent::copyTree(treeParent &t1,treeParent &t2)
{
	int l=getSize();
	for(int i=0;i<l;i++)
		copyNode(t1.table[i],t2.table[i]);
	t1.end=t2.end;
	t1.size=t2.size;
}
int treeParent::attachChild(elemtype root,elemtype data)
{
	
	if(isFull())
	{
		int l=getSize();
		treeParent t(*this);				
		table=new TPNode[l+1];
		for(int i=0;i<l;i++)
			copyNode(table[i],t.table[i]);
		end=t.end;
		size=t.size+1;
	}
	table[end+1].data=data;
	table[end+1].parent=index(root);
	return ++end;
	
}
void treeParent::copyNode(TPNode &p1,TPNode &p2)
{	
	p1.data=p2.data;
	p1.parent=p2.parent;
}
treeParent::treeParent(elemtype n)
{
	table=new TPNode[1];
	table[0].data=n;
	table[0].parent=-1;
	end=0;
	size=1;
}
treeParent::treeParent(int n)
{
	table=new TPNode[n];
	end=-1;
	size=n;
}
int treeParent::index(elemtype data)
{
	int l=getSize();
	for(int i=0;i<l;i++)
		if(table[i].data==data) return i;
	return -1;
}
void treeParent::setRoot(elemtype n)
{
	table[0].data=n;
	table[0].parent=-1;
	if(end<0)	
	{
		end=0;
		size=1;
	}
}
void treeParent::printTree()
{
	cout<<"  NUM  DATA  PARENT\n";
	for(int i=0;i<end+1;i++)
		cout<<setw(4)<<i<<setw(6)<<table[i].data<<setw(7)<<table[i].parent<<endl;
}



