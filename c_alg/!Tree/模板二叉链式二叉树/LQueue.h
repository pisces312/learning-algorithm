#include"LQNode.h"
template<class T>
class LQueue
{
	public:
		LQNode<T> *front,*rear;
		LQueue();
		~LQueue();
		bool isEmpty() const;
		bool enQueue(T n);
		T deQueue();
		friend ostream& operator<<(ostream& out,LQueue<T> &q);
};
template<class T>
LQueue<T>::LQueue()
{
	front=rear=NULL;
}
template<class T>
LQueue<T>::~LQueue()
{
	LQNode<T> *p=front,*q;
	while(p!=NULL)
	{
		q=p;
		p=p->next;
		delete q;
	}
	front=rear=NULL;
}
template<class T>
bool LQueue<T>::isEmpty()const
{
	return front==NULL&&rear==NULL;
}
template<class T>
bool LQueue<T>::enQueue(T n)
{
	LQNode<T> *p=new LQNode<T>(n);
	if(!isEmpty())
		rear->next=p;
	else
		front=p;
	rear=p;
	return true;
}
template<class T>
T LQueue<T>::deQueue()
{
	T n;
	LQNode<T> *p=front;
	if(!isEmpty())
	{
		n=front->data;
		front=front->next;
		delete p;
		if(front==NULL) rear=NULL;
	}
	return n;
}
template<class T>
ostream& operator<<(ostream& out,LQueue<T> &q)
{
	LQNode<T> *p=q.front;
	while(p!=NULL)
	{
		cout<<p->data<<" ";
		p=p->next;
	}
	cout<<endl;
	return out;
}

