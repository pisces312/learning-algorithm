#include"BTreeNode.h"
#include"LinkStack.h"
#include"LQueue.h"
#include"SSStr.h"
template<class T>
class BTree
{

	BTreeNode<T>* preCreate(char *str);
	BTreeNode<T>* tableCreate(char *str);
	BTreeNode<T>* layerCreate(char *str,int i=1);
	BTreeNode<T>* preinCreate(SSStr str1,SSStr str2);

public:
	BTreeNode<T> *root;
	BTree(char *str="");
	BTree(const BTree &t);
	BTree(BTreeNode<T> *p);
	~BTree();
	void destroyTree(BTreeNode<T> *p=NULL);
	void preOrder(BTreeNode<T> *p=NULL);
	void inOrder(BTreeNode<T> *p=NULL);
	void postOrder(BTreeNode<T> *p=NULL);
	void OutputLeaf(BTreeNode<T> *p=NULL);
	int BTreeLeafCount(BTreeNode<T> *p=NULL);
	int BTreeCount(BTreeNode<T> *p=NULL);
	void Property(BTreeNode<T> *p,int &n0,int &n2);
	void preInitiate(char *str="");
	void tableInitiate(char *str="");
	int BTreeDepth(BTreeNode<T> *p);
	bool FindNode(BTreeNode<T> *p,T str);	
	int CountNode(BTreeNode<T> *p,T str);
	int NodeLevel(BTreeNode<T> *p,T str);
	T MaxValue(BTreeNode<T> *p);
	void PrintTable2(BTreeNode<T> *p);
	T* PrintTable(BTreeNode<T> *p);
	
	void LayerOrder(BTreeNode<T> *p);
	void inOrderStack();                
	void setRootData(const T str);
	bool isEmpty();
	bool attachLeft(const T str);
	bool attachRight(const T str);
	bool attachLeftSub(BTree &t);

	bool attachRightSub(BTree &t);
	void copyTree(BTreeNode<T> *p,BTreeNode *&newTreePtr);
	BTree& operator=(const BTree &t);
	bool detachLeftSub(BTree &leftTree);			//把删掉的左子树赋给leftTree
	bool detachRightSub(BTree &rightTree);
	BTree getLeftSub();
	BTree getRightSub();
	
	void layerInitiate(char *str);
	
	void preinInitiate(char *str1,char *str2);
	void preOrderStack();
	BTreeNode<T>* inpostCreate(SSStr poststr,SSStr instr);
	void inpostInitiate(char *poststr,char *instr);
	void printTree(BTreeNode<T> *p,int n=1);
	
};
template<class T>
void BTree<T>::printTree(BTreeNode<T> *p,int n)
{
	if(p==NULL) return;
	printTree(p->left,n+1);
	for(int i=0;i<n-1;++i)
		cout<<"   ";
	cout<<p->data<<endl;
	printTree(p->right,n+1);
}
template<class T>
void BTree<T>::inpostInitiate(char *poststr,char *instr)
{	
	if(root!=NULL)
		destroyTree();
	root=inpostCreate(poststr,instr);
}
template<class T>
BTreeNode<T>* BTree<T>::inpostCreate(SSStr poststr,SSStr instr)
{
	BTreeNode<T> *p=NULL;
	int n=poststr.length();
	if(n>0)
	{
		char ch=poststr.GetChar(n);
		int k=instr.IndexChar(ch);
		if(k>0)
		{
			p=new BTreeNode(ch);
			SSStr postsub=poststr.GetSub(1,k-1);		//取左子树的后序串
			SSStr insub=instr.GetSub(1,k-1);		//取左子树的中序串
			p->left=inpostCreate(postsub,insub);		//建左子树

			postsub=poststr.GetSub(k,n-k);
			insub=instr.GetSub(k+1,n-k);
			p->right=inpostCreate(postsub,insub);
		}
	}
	return p;
}
template<class T>
void BTree<T>::preOrderStack()
{	
	LinkStack<BTreeNode<T>*> s;
	BTreeNode<T> *p=root;
	if(p==NULL) return;
	cout<<"非递归先序遍历二叉树: ";
	while(!(p==NULL&&s.isEmpty()))
	{
		while(p!=NULL)
		{
			cout<<p->data<<" ";
			s.push(p);
			p=p->left;
		}
		p=s.pop();
		p=p->right;		
	}
	cout<<endl;
}
template<class T>
void BTree<T>::preinInitiate(char *str1,char *str2)
{
	if(root!=NULL)
		destroyTree();
	root=preinCreate(str1,str2);
}
template<class T>
BTreeNode<T>* BTree<T>::preinCreate(SSStr prestr,SSStr instr)
{
	BTreeNode<T> *p=NULL;
	int n=prestr.length();
	if(n>0)
	{
		char ch=prestr.GetChar(1);
		int k=instr.IndexChar(ch);
		if(k>0)
		{
			p=new BTreeNode(ch);
			SSStr presub=prestr.GetSub(2,k-1);		//取左子树的先序串
			SSStr insub=instr.GetSub(1,k-1);		//取左子树的中序串
			p->left=preinCreate(presub,insub);		//建左子树

			presub=prestr.GetSub(k+1,n-k);
			insub=instr.GetSub(k+1,n-k);
			p->right=preinCreate(presub,insub);
		}
	}
	return p;
}
template<class T>
void BTree<T>::layerInitiate(char *str)
{
	if(root!=NULL)
		destroyTree(root);
	root=layerCreate(str);
}
template<class T>
BTreeNode<T>* BTree<T>::layerCreate(char *str,int i)
{
	BTreeNode<T> *p=NULL;
	static int len=strlen(str);
	if(i-1<len)
	{
		p=new BTreeNode(str[i-1]);
		p->left=layerCreate(str,2*i);
		p->right=layerCreate(str,2*i+1);
	}
	return p;
}
template<class T>
BTree<T> BTree<T>::getLeftSub()
{
	BTreeNode<T> *subTreePtr;
	if(isEmpty()) return BTree<T>();
	copyTree(root->left,subTreePtr);
	return BTree<T>(subTreePtr);
}
template<class T>
BTree<T> BTree<T>::getRightSub()
{
	BTreeNode<T> *subTreePtr;
	if(isEmpty()) return BTree<T>();
	copyTree(root->right,subTreePtr);
	return BTree<T>(subTreePtr);
}
template<class T>
bool BTree<T>::detachLeftSub(BTree &leftTree)
{
	if(isEmpty()) return false;
	leftTree=BTree(root->left);
	root->left=NULL;
	return true;
}
template<class T>
bool BTree<T>::detachRightSub(BTree &rightTree)
{
	if(isEmpty()) return false;
	rightTree=BTree(root->right);
	root->right=NULL;
	return true;
}
template<class T>
BTree<T>& BTree<T>::operator=(const BTree &t)
{
	if(this!=&t)					//如果两个棵相同则不执行
	{
		destroyTree(root);
		copyTree(t.root,root);
	}
	return *this;
}
template<class T>
void BTree<T>::copyTree(BTreeNode<T> *p,BTreeNode<T> *&newTreePtr)
{
	if(p!=NULL)
	{
		newTreePtr=new BTreeNode(p->data);
		copyTree(p->left,newTreePtr->left);
		copyTree(p->right,newTreePtr->right);
	}
	else newTreePtr=NULL;         //空树
}
template<class T>
bool BTree<T>::attachLeftSub(BTree &t)
{
	if(isEmpty()||root->left!=NULL) return false;
	else
	{
		root->left=t.root;
		t.root=NULL;                   //  为什么必须加??????
	}
	return true;
}
template<class T>
bool BTree<T>::attachRightSub(BTree &t)
{
	if(isEmpty()||root->right!=NULL) return false;
	else
	{
		root->right=t.root;
		t.root=NULL;
	}
	return true;
}
template<class T>
bool BTree<T>::attachRight(const T str)
{
	if(isEmpty()||root->right!=NULL) return false;
	else
		root->right=new BTreeNode(str);
	return true;
}
template<class T>
bool BTree<T>::attachLeft(const T str)
{
	if(isEmpty()||root->left!=NULL) return false;
	else
		root->left=new BTreeNode(str);
	return true;
}
template<class T>
bool BTree<T>::isEmpty()
{
	return root==NULL;
}
template<class T>
void BTree<T>::setRootData(const T str)
{
	if(!isEmpty())
		root->data=str;
	else
		root=new BTreeNode(str);
}
template<class T>
void BTree<T>::inOrderStack()
{
	cout<<"中序非递归遍厉二叉树: ";
	BTreeNode<T> *p=root;
	LinkStack<BTreeNode<T>*> s;
	while(p!=NULL||!s.isEmpty())
	{
		if(p!=NULL)
		{
			s.push(p);
			p=p->left;
		}
		else
		{
			p=s.pop();
			cout<<p->data<<" ";				
			p=p->right;
		}
	}
	cout<<endl;
}
template<class T>
void BTree<T>::LayerOrder(BTreeNode<T> *p)
{
	cout<<"层次遍历二叉树: ";
	LQueue<BTreeNode<T>*> q;
	q.enQueue(p);
	while(!q.isEmpty())
	{
		p=q.deQueue();
		cout<<p->data<<" ";
		if(p->left!=NULL)
			q.enQueue(p->left);
		if(p->right!=NULL)
			q.enQueue(p->right);
	}
	cout<<endl;
}
template<class T>
void BTree<T>::PrintTable2(BTreeNode<T> *p)
{
	static n=0;
	if(p==NULL) return;
	else
	{
		cout<<p->data;
		if(p->left!=NULL||p->right!=NULL)
		{
			cout<<'(';
			if(p->left==NULL)
				cout<<'#';
			PrintTable2(p->left);
			if(p->right!=NULL)
				cout<<',';
			else
				cout<<",#";
			PrintTable2(p->right);			
			cout<<')';
			n++;
		}
	}	
	if(n==BTreeDepth(root)+1)
	{
		n=0;
		cout<<endl;
	}
}
template<class T>
T* BTree<T>::PrintTable(BTreeNode<T> *p)
{
	static count=BTreeCount(root);
	static T* str=new T[count*5];
	static i=0,n=0;
	int l=strlen(str);
	if(i>=l) 
	{
		i=n=0;
	}
	if(p==NULL) return str;
	else
	{
		str[i++]=p->data;
		n++;
		if(p->left!=NULL||p->right!=NULL)
		{
			str[i++]='(';
			if(p->left==NULL)
				str[i++]='#';
			PrintTable(p->left);
			if(p->right!=NULL)
				str[i++]=',';
			else
			{
				str[i++]=',';
				str[i++]='#';
			}
			PrintTable(p->right);			
			str[i++]=')';
		}
	}
	if(n==BTreeCount())
		str[i]='\0';
	return str;
}
template<class T>
T BTree<T>::MaxValue(BTreeNode<T> *p)
{
	if(p==NULL) return '\0';
	else
	{
		T k1,k2;
		k1=MaxValue(p->left);
		k2=MaxValue(p->right);
		k1=k1>k2?k1:k2;
		return k1>p->data?k1:p->data;
	}
}
template<class T>
int BTree<T>::NodeLevel(BTreeNode<T> *p,T str)
{
	if(p==NULL) return 0;
	if(p->data==str) return 1;
	else
	{
		int c1=NodeLevel(p->left,str);
		if(c1>=1) return c1+1;
		int c2=NodeLevel(p->right,str);
		if(c2>=1) return c2+1;
		return 0;
	}
}
template<class T>
bool BTree<T>::FindNode(BTreeNode<T> *p,T str)
{
	if(p==NULL) return false;
	if(p->data==str)
	{
		return true;
	}
	else
	{
		if(FindNode(p->left,str)) return true;
		if(FindNode(p->right,str)) return true;
		return false;
	}
}
template<class T>
int BTree<T>::CountNode(BTreeNode<T> *p,T str)
{
	if(p==NULL) return 0;
	if(p->data==str) 
		return CountNode(p->left,str)+CountNode(p->right,str)+1;
	return CountNode(p->left,str)+CountNode(p->right,str);
}
template<class T>
int BTree<T>::BTreeDepth(BTreeNode<T> *p)
{
	if(p==NULL) return 0;
	else
	{
		int dep1=BTreeDepth(p->left);
		int dep2=BTreeDepth(p->right);
		if(dep1>dep2) return ++dep1;
		else return ++dep2;
	}
}
template<class T>
BTreeNode<T>* BTree<T>::tableCreate(char *str)
{
	BTreeNode<T> *p=NULL;
	static int i=0;
	static int l=strlen(str);
	if(i>=l) i=0;
	if((str[i]>='A'&&str[i]<='Z')||(str[i]>='a'&&str[i]<='z'))
	{
		p=new BTreeNode<T>(str[i++]);
		if(str[i]=='(')
		{
			i++;
			p->left=tableCreate(str);
			i++;
			p->right=tableCreate(str);
			i++;
		}
	}
	if(str[i]=='#'||str[i]=='.')
		i++;
	return p;
}
template<class T>
void BTree<T>::tableInitiate(char *str)
{
	destroyTree(root);
	if(str!="")
		root=tableCreate(str);
}
template<class T>
void BTree<T>::preInitiate(char *str)
{
	destroyTree(root);
	if(str!="")
		root=preCreate(str);
}
template<class T>
BTreeNode<T>* BTree<T>::preCreate(char *str)
{
	BTreeNode<T> *p=NULL;
	static int i=0;						
	static int l=strlen(str);
	if(i>=l) i=0;					//防止第二次调用时i不用0开始增加
	if(str[i]!='.')
	{
		p=new BTreeNode<T>(str[i++]);
		p->left=preCreate(str);
		p->right=preCreate(str);
	}
	else i++;
	return p;
}
template<class T>
void BTree<T>::Property(BTreeNode<T> *p,int &n0,int &n2)
{
	if(root==NULL)
	{
		n0=-1;
		n2=-1;
		return;
	}
	if(p!=NULL)
	{
		if(p->left==NULL&&p->right==NULL)
			n0++;
		if(p->left!=NULL&&p->right!=NULL)
			n2++;
		Property(p->left,n0,n2);
		Property(p->right,n0,n2);
	}
}
template<class T>
int BTree<T>::BTreeLeafCount(BTreeNode<T> *p)
{
	if(p==NULL) p=root;
	return root->BTreeLeafCount(p);
}
template<class T>
int BTree<T>::BTreeCount(BTreeNode<T> *p)
{
	if(p==NULL) p=root;
	return root->BTreeCount(p);
}
template<class T>
void BTree<T>::OutputLeaf(BTreeNode<T> *p)
{
	if(p==NULL) p=root;
	root->OutputLeaf(p);
	cout<<endl;
}
template<class T>
BTree<T>::BTree(char *str)
{
	root=NULL;						//?????
	if(str!="")
		root=preCreate(str);
}
template<class T>
BTree<T>::BTree(BTreeNode<T> *p)
{
	root=p;
}
template<class T>
BTree<T>::BTree(const BTree &t)
{
	copyTree(t.root,root);
}
template<class T>
BTree<T>::~BTree()
{
	destroyTree(root);
	root=NULL;
}
template<class T>
void BTree<T>::destroyTree(BTreeNode<T> *p)
{
	if(p!=NULL)
	{
		destroyTree(p->left);
		destroyTree(p->right);
		delete p;
	}
}
template<class T>
void BTree<T>::preOrder(BTreeNode<T> *p)
{
	if(p==NULL) p=root;
	cout<<"先序遍历二叉树: ";
	root->preOrder(p);
	cout<<endl;
}
template<class T>
void BTree<T>::inOrder(BTreeNode<T> *p)
{
	if(p==NULL) p=root;
	cout<<"中序遍历二叉树: ";
	root->inOrder(p);
	cout<<endl;
}
template<class T>
void BTree<T>::postOrder(BTreeNode<T> *p)
{
	if(p==NULL) p=root;
	cout<<"后序遍历二叉树: ";
	root->postOrder(p);
	cout<<endl;
}
