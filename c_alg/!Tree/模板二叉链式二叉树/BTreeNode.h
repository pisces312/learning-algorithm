#include<iostream.h>
#include<string.h>
#include<math.h>
template<class T>
class BTreeNode
{
public:
	T data;
	BTreeNode *left,*right;
	BTreeNode(T ch);
	~BTreeNode(){};
	void preOrder(BTreeNode *p);
	void inOrder(BTreeNode *p);
	void postOrder(BTreeNode *p);
	void OutputLeaf(BTreeNode *p);
	int BTreeLeafCount(BTreeNode *p);
	int BTreeCount(BTreeNode *p);
	void visit(BTreeNode *p);
};
template<class T>
void BTreeNode<T>::preOrder(BTreeNode *p)
{
	if(p!=NULL)
	{
		cout<<p->data<<" ";
		preOrder(p->left);
		preOrder(p->right);
	}
}
template<class T>
void BTreeNode<T>::inOrder(BTreeNode *p)
{
	if(p!=NULL)
	{
		inOrder(p->left);
		cout<<p->data<<" ";
		inOrder(p->right);
	}
}
template<class T>
void BTreeNode<T>::postOrder(BTreeNode *p)
{
	if(p!=NULL)
	{
		postOrder(p->left);
		postOrder(p->right);
		cout<<p->data<<" ";
	}
}
template<class T>
int BTreeNode<T>::BTreeCount(BTreeNode *p)
{
	if(p==NULL) return 0;
	else return BTreeCount(p->left)+BTreeCount(p->right)+1;
}
template<class T>
int BTreeNode<T>::BTreeLeafCount(BTreeNode *p)
{	
	if(p==NULL) 		
		return 0;
	else if(p->left==NULL&&p->right==NULL) 
		return 1;
	else 
		return BTreeLeafCount(p->left)+BTreeLeafCount(p->right);
}
template<class T>
void BTreeNode<T>::OutputLeaf(BTreeNode *p)
{
	if(p==NULL) return;		//函数无返回值时可直接用return返回
	if(p->left==NULL&&p->right==NULL) 
		cout<<p->data<<" ";
	else 
	{
		OutputLeaf(p->left);
		OutputLeaf(p->right);
	}
}
template<class T>
BTreeNode<T>::BTreeNode(T ch)
{
	data=ch;
	left=NULL;
	right=NULL;
}