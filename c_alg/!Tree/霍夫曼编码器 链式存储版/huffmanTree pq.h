#include<iostream>
#include<string>
#include<queue>
#include<vector>
#include<fstream>
#include<algorithm>
#include<iomanip>
using namespace std;
static const int MAX=256;
class hNode
{
	char data;
	int freq;
	string code;
	hNode *left,*right,*parent;
public:
	friend class huffman;
	friend class compare;
	hNode(char data='\n')
	{
		this->data=data;
		freq=0;
		left=right=parent=NULL;
	}
};
class compare
{
public:
	bool operator()(const hNode *&c1,const hNode *&c2) const
	{
		return (*c1).freq>(*c2).freq;
	}
};
class huffman
{
	priority_queue<hNode*,vector<hNode*>,compare> pq;
	hNode* nodeArray[MAX],*root;
	int nodenum;
	string encodestr,decodestr;
	void printTreeTraverse(hNode *p,int n=1);
public:
	friend class hNode;
	huffman();
	void createQueue(string &str);
	void createTree();
	void calculateCode();
	void printTree();
	void encode(string &str);
	void decode(string &str);
	void saveTofile();		
};
huffman::huffman()
{
	for(int i=0;i<MAX;i++)
		nodeArray[i]=new hNode(char(i));
	nodenum=0;
	root=new hNode;
}
void huffman::saveTofile()
{		
	ofstream fout("code.txt");
	if(encodestr!="")
		fout<<"编码字符串为:"<<encodestr<<endl;
	if(decodestr!="")
		fout<<"解码字符串为:"<<decodestr<<endl;
}
void huffman::decode(string &str)
{
	decodestr="";
	int i,k,len,j=0,strlen=str.length();
	bool have;
	cout<<"霍夫曼编码为"<<str<<"\n对应的字符串为:";
	while(j<strlen)
	{
		have=false;
		for(i=0;i<MAX;i++)
		{			
			if(nodeArray[i]->freq>0)
			{
				int t=0;
				len=nodeArray[i]->code.length();
				for(k=0;k<len;k++)
					if(nodeArray[i]->code[k]!=str[j+t]) break;
					else t++;
				if(k==len)
				{
					have=true;
					j+=t;
					decodestr+=nodeArray[i]->data;
				}
			}
		}
		if(have==false)
		{
			cout<<"无法解码!\n";
			exit(0);
		}
	}
	if(have) cout<<decodestr;
	cout<<endl;
}
void huffman::encode(string &str)
{
	encodestr="";
	for(unsigned i=0;i<str.length();i++)
		if(nodeArray[str[i]]->freq>0)
			encodestr+=nodeArray[str[i]]->code;
		else 
		{
			cout<<str[i]<<"无法编码!\n";
			exit(0);
		}
	cout<<str<<"的霍夫曼编码是:"<<encodestr<<endl;
}
void huffman::printTree()
{
	cout<<"霍夫曼树的凹入表示法如下:\n";
	printTreeTraverse(root);
}
void huffman::printTreeTraverse(hNode *p,int n)
{
	if(p==NULL) return;
	printTreeTraverse(p->left,n+1);
	for(int i=0;i<n-1;i++)
		cout<<setw(5)<<" ";
	if(p->data=='\n') cout<<"○";
	else if(p->data==' ') cout<<"空格";
	else cout<<p->data;
	cout<<endl;
	printTreeTraverse(p->right,n+1);
}
void huffman::calculateCode()
{
	int total=0,i;
	string code;
	hNode *p;
	for(i=0;i<MAX;i++)
	{
		code="";
		p=nodeArray[i];
		if(p->freq>0)
		{
			cout<<char(i)<<"的霍夫曼编码是:";
			do
			{
				code+=p->code;
				p=p->parent;
			}while(p!=NULL);
			reverse(code.begin(),code.end());
			nodeArray[i]->code=code; 
			cout<<setw(nodenum)<<code<<"   出现次数为:"<<nodeArray[i]->freq<<endl;
			total=code.length()*(nodeArray[i]->freq)+total;
		}	
	}
	cout<<"所需编码的大小"<<total<<endl;
}
void huffman::createTree()
{
	hNode *left,*right,*sum=NULL;
	while(pq.size()>1)
	{
		left=pq.top();
		pq.pop();
		left->code="0";
		right=pq.top();
		pq.pop();
		right->code="1";
		sum=new hNode;
		sum->freq=left->freq+right->freq;
		sum->left=left;
		sum->right=right;
		left->parent=right->parent=sum;
		pq.push(sum);
	}
	if(sum==NULL) 
	{
		sum=pq.top();
		pq.pop();
		sum->code="0";
	}		
	root=sum;
}
void huffman::createQueue(string &str)
{
	hNode *p;
	unsigned i;
	for(i=0;i<str.size();i++)
		nodeArray[str[i]]->freq++;
	for(i=0;i<MAX;i++)
	{
		p=nodeArray[i];
		if(p->freq>0)
		{
			pq.push(p);
			nodenum++;
		}
	}
}