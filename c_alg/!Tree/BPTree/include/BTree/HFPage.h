#ifndef HFPAGE_H
#define HFPAGE_H


class HFPage
{
    public:
        HFPage();
        virtual ~HFPage();
    protected:
    //定义一个槽结构，由偏移和长度组成
    struct Slot {
        short   offset;
        short   length;    // equals EMPTY_SLOT if slot is not in use
    };
    private:
};

#endif // HFPAGE_H
