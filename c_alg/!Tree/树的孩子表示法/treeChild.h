#include"treeChildNode.h"
#include"iomanip.h"
template<class T>
class treeChild
{
public:
	TCNode<T>* table;
	int end,size;
	treeChild(int n=1);
	treeChild(T n);
	treeChild(treeChild &t);
	void setRoot(T n);
	bool isFull(){return end+1==size;}
	int getSize(){return size;}
	int indexParent(T data);
	int attachChild(T root,T data);
	void copyTree(treeChild<T> &t1,treeChild<T> &t2);
	void copyNode(TCNode<T> &p1,TCNode<T> &p2);
	void printTree();
};		
template<class T>
treeChild<T>::treeChild(treeChild<T> &t)
{
	int l=t.getSize();
	table=new TCNode<T>[l];
	for(int i=0;i<l;i++)
		copyNode(table[i],t.table[i]);
	end=t.end;
	size=t.size;
}
template<class T>
void treeChild<T>::copyTree(treeChild<T> &t1,treeChild<T> &t2)
{
	int l=getSize();
	for(int i=0;i<l;i++)
		copyNode(t1.table[i],t2.table[i]);
	t1.end=t2.end;
	t1.size=t2.size;
}
template<class T>
int treeChild<T>::indexParent(T data)
{
	int l=getSize();
	for(int i=0;i<l;i++)
		if(table[i].data==data) return i;
	return -1;
}

template<class T>
int treeChild<T>::attachChild(T root,T data)
{
	
	if(isFull())
	{
		int l=getSize();
		treeChild<T> t(*this);				
		table=new TCNode<T>[l+1];
		for(int i=0;i<l;i++)
			copyNode(table[i],t.table[i]);
		end=t.end;
		size=t.size+1;
	}
	table[end+1].data=data;
	table[indexParent(root)].link.insertEnd(data);	
	return ++end;
	
}
template<class T>
void treeChild<T>::copyNode(TCNode<T> &p1,TCNode<T> &p2)
{	
	p1.data=p2.data;
	for(int i=1;i<=p2.link.length();i++)
	{
		p1.link.set(i,p2.link.get(i));
	}
}
template<class T>
treeChild<T>::treeChild(T n)
{
	table=new TCNode<T>[1];
	table[0].data=n;
	end=0;
	size=1;
}

template<class T>
treeChild<T>::treeChild(int n)
{
	if(n==0)
		table=NULL;
	else
	{
		table=new TCNode<T>[n];
	}
	end=-1;
	size=n;
}
template<class T>
void treeChild<T>::setRoot(T n)
{
	if(table==NULL)
	{
		table=new TCNode<T>[1];
		end=0;
		size=1;
	}
	else if(end<0)
	{
		end=0;
		size=1;
	}
	table[0].data=n;		
}
template<class T>
void treeChild<T>::printTree()
{
	cout<<"  NUM  DATA  CHILD\n";

	for(int i=0;i<end+1;i++)
	{	
		cout<<setw(4)<<i<<setw(6)<<table[i].data;
		table[i].link.output();
	}
		
}





