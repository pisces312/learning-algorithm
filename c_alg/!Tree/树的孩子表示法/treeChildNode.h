#include<iostream.h>
#include"SLink.h"
template<class T>
class TCNode
{
public:
	T data;
	SLink<char> link;	
	TCNode();
	TCNode(T n);
};
template<class T>	
TCNode<T>::TCNode()
{
	link.head=new SLNode<T>('\0');
}
template<class T>
TCNode<T>::TCNode(T n)
{
	data=n;
	link.head=new SLNode<T>('\0');
}
