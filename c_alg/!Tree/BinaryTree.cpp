//---------------------------------------------------------------------------

#pragma hdrstop

#include <iostream.h>
#include <Stack.cpp>
#include <Queue.cpp>


enum Action{GOLEFT, GORIGHT, VISITNODE, GOPARENT};

class Tree;
class InorderIterator;
class PostorderIterator;

class TreeNode{
    friend class Tree;
	friend class InorderIterator;
	friend class PostorderIterator;

public:
	char data;
	TreeNode *leftChild, *rightChild;
	TreeNode(char data){
		this->data = data;
        this->leftChild = this->rightChild = 0;
    };

    ~TreeNode(){
		if (leftChild)
			delete leftChild;
		if (rightChild)
			delete rightChild;
    };
};

struct TreeNodeData{
    TreeNode *node;
    int data;
};

class Tree{
    friend class TreeNode;
    friend class InorderIterator;
    friend class PostorderIterator;
private:
public:
	TreeNode *root;
    Tree(char* data, int len){
        root = createNode(data, 0, len);
    };

	TreeNode* MinParent(TreeNode* node, TreeNode* p, TreeNode* q, Boolean *HasP, Boolean *HasQ){
		if (!node) {
			*HasP = *HasQ = False;
			return 0;
		} else {
			*HasP = *HasQ = False;
			Boolean LeftHasP, LeftHasQ, RightHasP, RightHasQ;
            TreeNode *LeftMinParent, *RightMinParent;
			if (node==p)
				*HasP = True;
			else if (node==q)
				*HasQ = True;
			LeftMinParent = MinParent(node->leftChild, p, q, &LeftHasP, &LeftHasQ);
			if ((*HasP && LeftHasQ) || (*HasQ && LeftHasP)) {
				*HasP = *HasQ = True; return node;
			} else if (LeftHasP && LeftHasQ) {
				*HasP = *HasQ = True; return LeftMinParent;
			} else {
				RightMinParent = MinParent(node->rightChild, p, q, &RightHasP, &RightHasQ);
				if ((*HasP && RightHasQ) || (*HasQ && RightHasP)) {
					*HasP = *HasQ = True; return node;
				} else if (RightHasP && RightHasQ) {
					*HasP = *HasQ = True; return RightMinParent;
				} else {
					*HasP = *HasP || LeftHasP || RightHasP;
					*HasQ = *HasQ || LeftHasQ || RightHasQ;
					return node;
				}
			};
		}
	};

	TreeNode* GetMinParent(TreeNode *p, TreeNode* q){
		Boolean HasP = 0, HasQ = 0;
		TreeNode* result = MinParent(root, p, q, &HasP, &HasQ);
		if (HasP && HasQ)
			return result;
		else
			return 0;
	};

    TreeNode* createNode(char* data, int i, int len){
        if (i>=len){
            return 0;
        } else if (*(data+i)==' ') {
            return 0;
        } else {
            TreeNode *node = new TreeNode(*(data+i));
            node->leftChild = createNode(data, 2*i+1, len);
            node->rightChild = createNode(data, 2*i+2, len);
            return node;   
        };
    };

    ~Tree(){
        if (root)
            delete root;
    };

    void inorder(){
        cout << "中序:";
        inorder(root);
        cout << endl;    
    };

    void inorder(TreeNode *node){
        if (!node)
            return;
        inorder(node->leftChild);
        cout << node->data;
        inorder(node->rightChild); 
    };

    void preorder(){
        cout << "前序:";
        preorder(root);
        cout << endl;
    };

    void preorder(TreeNode *node){
        if (!node)
            return;
        cout << node->data;
        preorder(node->leftChild);
        preorder(node->rightChild); 
    };

    void postorder(){
        cout << "后序:";
        postorder(root);
        cout << endl;
    };

    void postorder(TreeNode *node){
        if (!node)
            return;
        postorder(node->leftChild);
        postorder(node->rightChild);
        cout << node->data;
    };

    void nonrecInoder(){
        cout << "中序:";
        Stack<TreeNode*> s;
        TreeNode* current = root;
        while(1){
            while(current){
                s.push(current);
                current = current->leftChild;
            }
            if(!s.isEmpty()){
                current = *s.pop();
                cout << current->data;
                current = current->rightChild;
            } else {
                break;
            }
        };
        cout << endl;
    };

    Boolean IsFullTree(){
        if (!root) return True;
        TreeNodeData nodeData;
        TreeNode* node;
        Queue<TreeNodeData> queue;
        int seqNo = 0;
        nodeData.node = root;
        nodeData.data = 1;
        queue.Add(nodeData);
        while (!queue.IsEmpty()){
            nodeData = *(queue.Delete());
            seqNo++;
            if (nodeData.data != seqNo) return False;
            node = nodeData.node;
            if (node->leftChild) {
              nodeData.node = node->leftChild;
              nodeData.data = seqNo * 2;
              queue.Add(nodeData);
            };
            if (node->rightChild) {
              nodeData.node = node->rightChild;
              nodeData.data = seqNo * 2 + 1;
              queue.Add(nodeData);
            };
        };
        seqNo++;
        Boolean tag = True;
        while (seqNo != 1){
            if (seqNo % 2 != 0) {
                tag = False;
                break;
            };
            seqNo = seqNo / 2;
        };
        if (tag)
            cout << "满二叉树" << endl;
        return True;
    };

    void StackInorder(){
      cout << "中序:";
      TreeNode *p = root;
      Stack<TreeNode*> s;
      while(p || !s.isEmpty()){
        while (p){
          s.push(p); p = p->leftChild;
        };
        if (!s.isEmpty()) {
          p = *s.pop();
          cout << p->data;
          p = p->rightChild;
        };
      }
      cout << endl;
	};

    void StackPostOrder(){
      TreeNode *p = root;
      Stack<TreeNodeData> s;
      TreeNodeData nodeData;
      while(p || !s.isEmpty()){
        while (p && (p->data!='G')){
          nodeData.node = p;
          nodeData.data = 0;
          s.push(nodeData); 
          p = p->leftChild;
        };
        if (p) {
          while (!s.isEmpty()){
            nodeData = *s.pop();
            cout << nodeData.node->data << endl;
          };
          break;
        };
        nodeData = *s.pop();
        if (!nodeData.data){
          nodeData.data = 1;
          s.push(nodeData);
          p = nodeData.node->rightChild;
        } else {
          //
        };
      }
    };

    void levelOrder(){
      if (!root) return;
      cout << "层次:";
      int front = 0, rear = 1, last = 1, level = 0;
      Queue<TreeNode*> queue;
      TreeNode* node;
      node = root;
      queue.Add(node);
      while (!queue.IsEmpty()){
        node = *queue.Delete();
        cout << "level " << level << " :" << node->data << endl;
        if (node->leftChild){queue.Add(node->leftChild); rear++;};
        if (node->rightChild){queue.Add(node->rightChild); rear++;};
        front++;
        if (front == last){
            last = rear;
            level++;
        }
      }; 
    };

    void findNode(char x){
      cout << "后序查找:";
      TreeNode *p = root;
      Stack<TreeNodeData> s;
      TreeNodeData nodeData;
      
      while(p || !s.isEmpty()){
        if (p){
          if (p->data == x){
            cout << "OK " << endl;
            while(!s.isEmpty()){
                nodeData = *s.pop();
                cout << "Parent " << nodeData.node->data << endl;
            };
            break;
          };
          nodeData.node = p;
          nodeData.data = 0;
          s.push(nodeData); p = p->leftChild;
        } else {
          nodeData = *s.pop();
          if (!nodeData.data){
            nodeData.data = 1;
            s.push(nodeData);
            p = nodeData.node->rightChild; 
          } else {
            //空遍历
          }
        };
      }
      cout << endl;
    };

    
    void StackPreorder(){
      cout << "前序:";
      TreeNode *p = root;
      Stack<TreeNode*> s;
      while(p || !s.isEmpty()){
        if (p){
          cout << p->data;
          if (p->rightChild){
            s.push(p->rightChild); 
          };
          p = p->leftChild;
        } else {
          p = *s.pop();
        };
      }
      cout << endl;
	};
};

class InorderIterator{
    friend class Tree;
    friend class TreeNode;
private:
    Tree *tree;
    TreeNode *current;
    Stack<TreeNode*> stack;
public:
    InorderIterator(Tree *tree){
        this->tree = tree;
        current = tree->root;
    };

    char* Next(){
        while (current){
            stack.push(current);
            current = current->leftChild;
        };
        if (!stack.isEmpty()){
            current = *stack.pop();
            char *result = &(current->data);
			current = current->rightChild;
			return result;
        } else {
            return 0;
        }
    };
};

struct StackItem{
    TreeNode *node;
    Boolean RCVisited;
};

class PostorderIterator{
    friend class Tree;
    friend class TreeNode;
private:
    Tree *tree;
    TreeNode *current;
    Stack<StackItem> stack;
public:
    PostorderIterator(Tree *tree){
        this->tree = tree;
        current = tree->root;
    };

    char* Next(){
        while(1){
            while (current){
                StackItem item;
                item.node = current;
                item.RCVisited = False;
                stack.push(item);
                current = current->leftChild;
            };
            if (!stack.isEmpty()){
                StackItem item = *stack.pop();
                current = item.node;
                if (!item.RCVisited){
                    item.RCVisited = True;
                    stack.push(item);
                    current = current->rightChild; 
                } else {
                    char *result = &(current->data);
                    current = 0;
                    return result; 
                };
            } else {
                return 0;
            }
        };
    };
};

class ThreadedTree;

class ThreadedNode{
	friend class ThreadedTree;
public:
	ThreadedNode(char data){
		this->data = data;
		this->LeftThread = False;
		this->RightThread = False;
		this->LeftChild = 0;
        this->RightChild = 0;
	};
	Boolean LeftThread;
	ThreadedNode* LeftChild;
	char data;
	Boolean RightThread;
	ThreadedNode* RightChild;
};

class ThreadedTree{
private:
    ThreadedNode *previous;
public:
	ThreadedNode *root;

	ThreadedTree(){
		root = new ThreadedNode(' ');
		root->LeftThread = True;
		root->LeftChild = root;
		root->RightThread = False;
        root->RightChild = root;
	};

    void BuildThread(){
       this->previous = root;
       BuildNodeThread(root);
    };

    void BuildNodeThread(ThreadedNode *node){
       if (node->LeftChild)
          BuildNodeThread(node->LeftChild);
       else {
          node->LeftThread = True;
          node->LeftChild = previous;
       };
       if (previous && (!previous->RightChild)){
          previous->RightThread = True;
          previous->RightChild = node;
       };
       previous = node;
       if ((node!=root) && (node->RightChild))
          BuildNodeThread(node->RightChild);
    };

	void InitSampleTree(){
		ThreadedNode *A = new ThreadedNode('A');
		ThreadedNode *B = new ThreadedNode('B');
		ThreadedNode *C = new ThreadedNode('C');
		ThreadedNode *D = new ThreadedNode('D');
		ThreadedNode *E = new ThreadedNode('E');
		ThreadedNode *F = new ThreadedNode('F');
		ThreadedNode *G = new ThreadedNode('G');
		ThreadedNode *H = new ThreadedNode('H');
		ThreadedNode *I = new ThreadedNode('I');

		root->LeftThread = False;
		root->LeftChild = A;

		A->LeftChild = B;
		A->RightChild = C;

		B->LeftChild = D;
		B->RightChild = E;

		C->LeftChild = F;
		C->RightChild = G;

		D->LeftChild = H;
		D->RightChild = I;

		E->LeftThread = True;
		E->LeftChild = B;
		E->RightThread = True;
		E->RightChild = A;

		F->LeftThread = True;
		F->LeftChild = A;
		F->RightThread = True;
		F->RightChild = C;

		G->LeftThread = True;
		G->LeftChild = C;
		G->RightThread = True;
		G->RightChild = root;

		H->LeftThread = True;
		H->LeftChild = root;
		H->RightThread = True;
		H->RightChild = D;

		I->LeftThread = True;
		I->LeftChild = D;
		I->RightThread = True;
		I->RightChild = B;
	};

	void InsertLeft(ThreadedNode *parent, ThreadedNode *child){
		child->LeftThread = parent->LeftThread;
		child->LeftChild = parent->LeftChild;
		child->RightThread = True;
		child->RightChild = parent;
		parent->LeftThread = False;
		parent->LeftChild = child;
		if (!child->LeftThread) {
			ThreadedNode *temp = child->LeftChild;
			while (!temp->RightThread){
				temp = temp->RightChild;
			}
			temp->RightChild = child;
		}
	};

	void InsertRight(ThreadedNode *parent, ThreadedNode *child){
		child->RightThread = parent->RightThread;
		child->RightChild = parent->RightChild;
		child->LeftThread = True;
		child->LeftChild = parent;
		parent->RightThread = False;
		parent->RightChild = child;
		if (!child->RightThread) {
			ThreadedNode *temp = child->RightChild;
			while (!temp->LeftThread){
				temp = temp->LeftChild;
			}
			temp->LeftChild = child;
		}
	};

	void DeleteNode(ThreadedNode* node){
		if ((!node->LeftThread) && (node->LeftChild!=root)) {
			DeleteNode(node->LeftChild);
		}
		if ((!node->RightThread) && (node->RightChild!=root)) {
			DeleteNode(node->RightChild);
		}
		delete node;
	};

	void Inorder(){
        cout << "中序:";
		ThreadedNode *p = root;
		while(True){
            if (p->RightThread)
                p = p->RightChild;
            else {
                p = p->RightChild;
                while (!p->LeftThread)
                    p = p->LeftChild;
            }
            if (p == root)
                break;
            else
                cout << p->data;
		}
        cout << endl;
	};


    ThreadedNode* Parent(ThreadedNode *node){
		ThreadedNode *parent;

        //检查是否为左结点，如果是，则右结点的最右的后继为Parent
        parent = node;
		while (!parent->RightThread){
			parent = parent->RightChild;
		}
		parent = parent->RightChild;
        
        if (parent->LeftChild != node) {
           //检查是否为左结点，如果是，则左结点的最左的前继为Parent
			parent = node;
			while(!parent->LeftThread){
                parent = parent->LeftChild;
			};
			parent = parent->LeftChild;
        };
        
        return parent; 
    };

	ThreadedNode* Parent(ThreadedNode *node, Action* NextAction){
		/*
		  假设node!=0 返回node.parent
		  if node = parent.LeftChild then nextAction = GORIGHT
		  if node = parent.RightChild then nextAction = VISITNODE
		  先判断是不是左子女，
		  如果是左子女，那边最右子结点的中序后驱就一定是父结点
		  因为最右子结点是父结点的中序遍历的最后一个结点
		  遍历完这个结点后就应该是遍历父结点
		  如果最右子结点的中序后驱就不是父结点
		  那么就一定是右子女
		  右子女的中序结点就是最左子女的前驱结点
		  因为最左子女是父结点的右子数的第一个遍历的结点
		  遍历这个结点之前就是父结点
		  需要注意的是root,如果遍历到root，
		  就表示node无后续，故node一定是右结点
		*/
		ThreadedNode *parent = node;
		while (!parent->RightThread){
			parent = parent->RightChild;
		}
		parent = parent->RightChild;
		if (parent==root) {
        	*NextAction = VISITNODE;
		} else if (parent->LeftChild == node) {
        	*NextAction = GORIGHT;           
		} else {
            *NextAction = VISITNODE;
		}
		if (*NextAction == VISITNODE) {
			parent = node;
			while(!parent->LeftThread){
                parent = parent->LeftChild;
			};
			parent = parent->LeftChild;
		}
		return parent;
	};

	void Postorder(){
		cout << "后序:";
		Action NextAction = GOLEFT;
		ThreadedNode *current = root->LeftChild;
		while (current!=root){
			switch(NextAction){
				case GOLEFT:{
					if (!current->LeftThread)
						current = current->LeftChild;
					else
						NextAction = VISITNODE;
					break;
				}
				case GORIGHT:{
					if (!current->RightThread) {
						current = current->RightChild;
                        NextAction = GOLEFT;
					} else {
                        NextAction = VISITNODE;
					}
					break;
				}
				case VISITNODE:{
					cout << current->data;
					current = Parent(current, &NextAction);
					break; 
				}
			}

		}
        cout << endl;
	};

	void Preorder(){
		cout << "前序:";
        ThreadedNode *node = root->LeftChild;
        while (node != root){
            //沿左子树向下
            while (!node->LeftThread){
                cout << node->data;
                node = node->LeftChild;
            };
            //输出，准备右转
            cout << node->data;
            //沿右线索向上
            while (node->RightThread) node = node->RightChild;
            //右转
            node = node->RightChild;
        };
		cout << endl;
	};

	~ThreadedTree(){
		DeleteNode(root);
	};
};

//---------------------------------------------------------------------------
void printAllPath(TreeNode* root){
    TreeNodeData stack[100];
    int top = -1;
    TreeNode *p = root;
    while (p || (top>=0)){
        while (p){
            top++;
            stack[top].node = p;
            stack[top].data = 0;
            p = p->leftChild;
        };
        if (stack[top].data) {
            if (!stack[top].node->leftChild && !stack[top].node->rightChild){
                for (int i = 0; i<=top; i++)
                    cout << stack[i].node->data;
                cout << endl;
            }
            top--;
        } else {
            stack[top].data = 1;
            p = stack[top].node->rightChild;
        };
    };
};


TreeNode *pre;

int NavigateSortNode(TreeNode* node){
    if (!node) return 1;
    if (!NavigateSortNode(node->leftChild)) return 0;
    if ((pre) && (pre->data > node->data)) return 0;
    pre = node;
    return NavigateSortNode(node->rightChild);
};


int IsSortTree(TreeNode* root){
  pre = 0;
  return NavigateSortNode(root); 
};

TreeNode* MinParent(Tree* tree, TreeNode* p, TreeNode* q){
  int foundP = 0, foundQ = 0, popP = 0, popQ = 0;
  TreeNodeData stack[100];
  int top = -1;
  TreeNode* n = tree->root;
  while (n || (top>=0)){
    while (n){
        top++;  stack[top].node = n; stack[top].data = 0;
        foundP = foundP || (n == p);
        foundQ = foundQ || (n == q);
        if (foundP && foundQ)
            break;
        n = n->leftChild;
    }
    if ((!foundP || !foundQ) && (stack[top].data==0)){
        stack[top].data = 1; n = stack[top].node->rightChild;
    } else {
        n = stack[top].node; top--;
        popP = popP || (n == p);
        popQ = popQ || (n == q);
        if (popP && popQ)
            return stack[top].node;
        else if (popP && foundQ)
            return q;
        else if (popQ && foundP)
            return p;
        else
            n = 0;
    };
  }
  return 0;
};

TreeNode* buildNode(char* pre, char* in, int *root, int low, int high, int *flag){
    if ((!flag) || (low > high)) return 0;
    (*root)++;
    TreeNode *node = new TreeNode(pre[*root]);
    int i;
    for (i = low; (i <= high) && (in[i]!=pre[*root]); i++);
    if (in[i]!=pre[*root]) {
        *flag = 0;
        return 0;
    }
    node->leftChild = buildNode(pre, in, root, low, i - 1, flag);
    node->rightChild = buildNode(pre, in, root, i + 1, high, flag); 
    return node;
}

Tree* buildTree(char* pre, char* in, int n){
    Tree* tree = new Tree("", 0);
    int root = -1;
    int flag = 1;
    tree->root = buildNode(pre, in, &root, 0, n-1, &flag);
    return tree;
}

#pragma argsused
int main(int argc, char* argv[])
{

	Tree *tree = new Tree("ABCDEFGHI", 9);

	cout << tree->GetMinParent(tree->root, tree->root->leftChild)->data << endl;
	cout << tree->GetMinParent(tree->root, tree->root->rightChild)->data << endl;
	cout << tree->GetMinParent(tree->root->leftChild, tree->root->rightChild)->data << endl;
	cout << tree->GetMinParent(tree->root->leftChild->leftChild, tree->root->rightChild->leftChild)->data << endl;
	cout << tree->GetMinParent(tree->root->leftChild->leftChild, tree->root->leftChild->rightChild)->data << endl;
	cout << tree->GetMinParent(tree->root->rightChild->leftChild, tree->root->rightChild->rightChild)->data << endl;

	cout << MinParent(tree, tree->root, tree->root->leftChild)->data << endl;
	cout << MinParent(tree, tree->root, tree->root->rightChild)->data << endl;
	cout << MinParent(tree, tree->root->leftChild, tree->root->rightChild)->data << endl;
	cout << MinParent(tree, tree->root->leftChild->leftChild, tree->root->rightChild->leftChild)->data << endl;
	cout << MinParent(tree, tree->root->leftChild->leftChild, tree->root->leftChild->rightChild)->data << endl;
	cout << MinParent(tree, tree->root->rightChild->leftChild, tree->root->rightChild->rightChild)->data << endl;

	//delete tree;

	//getchar();

	tree = new Tree("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 26);

	printAllPath(tree->root);

	getchar();

	return 0;

    if (IsSortTree(tree->root)){
        cout << "Is sorted tree" << endl;
    };

    delete tree;

    tree = new Tree("4261357", 7);
    if (IsSortTree(tree->root)){
        cout << "Is sorted tree" << endl;
    };

	//getchar();

    //delete tree;

    //return 0;

    tree = buildTree("ABDHIECFG", "HDIBEAFCG", 9);

	tree->inorder();
	tree->preorder();
	tree->postorder();
    cout << "不用递归" << endl;
    tree->StackInorder();
    tree->StackPreorder();
    tree->StackPostOrder();
    tree->findNode('E');
    tree->levelOrder();


	//tree->nonrecInoder();

    if (tree->IsFullTree()){
      cout << "完全树" << endl;
    };






	{
	  cout << "中序:";
	  InorderIterator iter(tree);
      char* c;
	  for (c=iter.Next(); c; c=iter.Next()){
		  cout << *c;
	  }
	  cout << endl;
	}

	{
	  cout << "后序:";
	  PostorderIterator iter(tree);
	  char* c;
	  for (c=iter.Next(); c; c=iter.Next()){
		  cout << *c;
	  }
	  cout << endl;
	}

	cout << "中序线索树" << endl;
	ThreadedTree *ttree = new ThreadedTree();
	ttree->InitSampleTree();
	ttree->Inorder();
	ttree->Postorder();
	ttree->Preorder();

    delete ttree;

	delete tree;

	ttree = new ThreadedTree();

	ThreadedNode *A = new ThreadedNode('A');
	ThreadedNode *B = new ThreadedNode('B');
	ThreadedNode *C = new ThreadedNode('C');
	ThreadedNode *D = new ThreadedNode('D');
	ThreadedNode *E = new ThreadedNode('E');
	ThreadedNode *F = new ThreadedNode('F');
	ThreadedNode *G = new ThreadedNode('G');
	ThreadedNode *H = new ThreadedNode('H');
	ThreadedNode *I = new ThreadedNode('I');

	/*ttree->InsertLeft(ttree->root, A);
	ttree->InsertLeft(A, B);
	ttree->InsertLeft(B, H);
	ttree->InsertLeft(B, D);
	ttree->InsertRight(D, I);
	ttree->InsertRight(B, E);
	ttree->InsertRight(A, G);
	ttree->InsertRight(A, C);
	ttree->InsertLeft(C, F);*/
    
    ttree->root->LeftChild = A;
    ttree->root->LeftThread = False;
    A->LeftChild = B;
    A->RightChild = C;
    B->LeftChild = D;
    B->RightChild = E;
    C->LeftChild = F;
    C->RightChild = G;
    D->LeftChild = H;
    D->RightChild = I;

    ttree->BuildThread();


	cout << "构造后的线索树" << endl;
	ttree->Inorder();
    ttree->Postorder();
	ttree->Preorder();

    cout << "A.Parent = " << ttree->Parent(A)->data << endl;
    cout << "B.Parent = " << ttree->Parent(B)->data << endl;
    cout << "C.Parent = " << ttree->Parent(C)->data << endl;
    cout << "D.Parent = " << ttree->Parent(D)->data << endl;
    cout << "E.Parent = " << ttree->Parent(E)->data << endl;
    cout << "F.Parent = " << ttree->Parent(F)->data << endl;
    cout << "G.Parent = " << ttree->Parent(G)->data << endl;
    cout << "H.Parent = " << ttree->Parent(H)->data << endl;
    cout << "I.Parent = " << ttree->Parent(I)->data << endl;

    delete ttree;

	getchar();
	return 0;
}
//---------------------------------------------------------------------------
 