#include"BTreeNode.h"
#include"LinkStack.h"
#include"LQueue.h"
class BTree
{
public:
	BTreeNode *root;
	BTree(char *str="");
	~BTree();
	void Destroy(BTreeNode *p=NULL);
	void PreOrder(BTreeNode *p=NULL);
	void InOrder(BTreeNode *p=NULL);
	void PostOrder(BTreeNode *p=NULL);
	void OutputLeaf(BTreeNode *p=NULL);
	int BTreeLeafCount(BTreeNode *p=NULL);
	int BTreeCount(BTreeNode *p=NULL);
	void Property(BTreeNode *p,int &n0,int &n2);
	void PreInitiate(char *str="");
	void TableInitiate(char *str="");
	int BTreeDepth(BTreeNode *p);
	bool FindNode(BTreeNode *p,char str);	
	int CountNode(BTreeNode *p,char str);
	int NodeLevel(BTreeNode *p,char str);
	char MaxValue(BTreeNode *p);
	void PrintTable2(BTreeNode *p);
	char* PrintTable(BTreeNode *p);
	BTreeNode *PreCreate(char *str);
	BTreeNode *TableCreate(char *str);
	void LayerOrder(BTreeNode *p);
	void InOrder2();                    //wrong
	void setRootData(const char& str);
};

void BTree::vist(BTreeNode *p)
{
	cout<<p->data<<" ";
}
void BTree::InOrder2()
{
	BTreeNode *p=root;
	LinkStack<BTreeNode*> s;
	bool done=false;
	while(!done)
	{
		if(p!=NULL)
		{
			s.push(p);
			p=p->left;
		//	cout<<s.get()->data<<" ";				
		}
		else
		{
			if(s.isEmpty())
				done=true;
			else
			{
				cout<<s.get()->data<<" ";				
				s.pop();
				p=p->right;
			}
		}
	}
}
void BTree::LayerOrder(BTreeNode *p)
{
	cout<<"层次遍历: ";
	LQueue<BTreeNode*> q;
	q.enQueue(p);
	while(!q.isEmpty())
	{
		p=q.deQueue();
		cout<<p->data<<" ";
		if(p->left!=NULL)
			q.enQueue(p->left);
		if(p->right!=NULL)
			q.enQueue(p->right);
	}
	cout<<endl;
}
void BTree::PrintTable2(BTreeNode *p)
{
	static n=0;
	if(p==NULL) return;
	else
	{
		cout<<p->data;
		if(p->left!=NULL||p->right!=NULL)
		{
			cout<<'(';
			if(p->left==NULL)
				cout<<'#';
			PrintTable2(p->left);
			if(p->right!=NULL)
				cout<<',';
			else
				cout<<",#";
			PrintTable2(p->right);			
			cout<<')';
			n++;
		}
	}	
	if(n==BTreeDepth(root)+1)
	{
		n=0;
		cout<<endl;
	}
}
char* BTree::PrintTable(BTreeNode *p)
{
	static count=BTreeCount(root);
	static char* str=new char[count*5];
	static i=0,n=0;
	int l=strlen(str);
	if(i>=l) 
	{
		i=n=0;
	}
	if(p==NULL) return str;
	else
	{
		str[i++]=p->data;
		n++;
		if(p->left!=NULL||p->right!=NULL)
		{
			str[i++]='(';
			if(p->left==NULL)
				str[i++]='#';
			PrintTable(p->left);
			if(p->right!=NULL)
				str[i++]=',';
			else
			{
				str[i++]=',';
				str[i++]='#';
			}
			PrintTable(p->right);			
			str[i++]=')';
		}
	}
	if(n==BTreeCount())
		str[i]='\0';
	return str;
}
char BTree::MaxValue(BTreeNode *p)
{
	if(p==NULL) return '\0';
	else
	{
		char k1,k2;
		k1=MaxValue(p->left);
		k2=MaxValue(p->right);
		k1=k1>k2?k1:k2;
		return k1>p->data?k1:p->data;
	}
}
int BTree::NodeLevel(BTreeNode *p,char str)
{
	if(p==NULL) return 0;
	if(p->data==str) return 1;
	else
	{
		int c1=NodeLevel(p->left,str);
		if(c1>=1) return c1+1;
		int c2=NodeLevel(p->right,str);
		if(c2>=1) return c2+1;
		return 0;
	}
}
bool BTree::FindNode(BTreeNode *p,char str)
{
	if(p==NULL) return false;
	if(p->data==str)
	{
		return true;
	}
	else
	{
		if(FindNode(p->left,str)) return true;
		if(FindNode(p->right,str)) return true;
		return false;
	}
}

int BTree::CountNode(BTreeNode *p,char str)
{
	if(p==NULL) return 0;
	if(p->data==str) 
		return CountNode(p->left,str)+CountNode(p->right,str)+1;
	return CountNode(p->left,str)+CountNode(p->right,str);
}



int BTree::BTreeDepth(BTreeNode *p)
{
	if(p==NULL) return 0;
	else
	{
		int dep1=BTreeDepth(p->left);
		int dep2=BTreeDepth(p->right);
		if(dep1>dep2) return ++dep1;
		else return ++dep2;
	}
}
BTreeNode* BTree::TableCreate(char *str)
{
	BTreeNode *p=NULL;
	static int i=0;
	static int l=strlen(str);
	if(i>=l) i=0;
	if((str[i]>='A'&&str[i]<='Z')||(str[i]>='a'&&str[i]<='z'))
	{
		p=new BTreeNode(str[i++]);
		if(str[i]=='(')
		{
			i++;
			p->left=TableCreate(str);
			i++;
			p->right=TableCreate(str);
			i++;
		}
	}
	if(str[i]=='#'||str[i]=='.')
		i++;
	return p;
}
void BTree::TableInitiate(char *str)
{
	Destroy(root);
	root=NULL;
	if(str!="")
		root=TableCreate(str);
}
void BTree::PreInitiate(char *str)
{
	Destroy(root);
	root=NULL;
	if(str!="")
		root=PreCreate(str);
}
BTreeNode *BTree::PreCreate(char *str)
{
	BTreeNode *p=NULL;
	static int i=0;						
	static int l=strlen(str);
	if(i>=l) i=0;					//防止第二次调用时i不用0开始增加
	if(str[i]!='.')
	{
		p=new BTreeNode(str[i++]);
		p->left=PreCreate(str);
		p->right=PreCreate(str);
	}
	else i++;
	return p;
}
void BTree::Property(BTreeNode *p,int &n0,int &n2)
{
	if(root==NULL)
	{
		n0=-1;
		n2=-1;
		return;
	}
	if(p!=NULL)
	{
		if(p->left==NULL&&p->right==NULL)
			n0++;
		if(p->left!=NULL&&p->right!=NULL)
			n2++;
		Property(p->left,n0,n2);
		Property(p->right,n0,n2);
	}
}
int BTree::BTreeLeafCount(BTreeNode *p)
{
	if(p==NULL) p=root;
	return root->BTreeLeafCount(p);
}
int BTree::BTreeCount(BTreeNode *p)
{
	if(p==NULL) p=root;
	return root->BTreeCount(p);
}
void BTree::OutputLeaf(BTreeNode *p)
{
	if(p==NULL) p=root;
	root->OutputLeaf(p);
	cout<<endl;
}


BTree::BTree(char *str)
{
	root=NULL;
	if(str!="")
		root=PreCreate(str);
}
BTree::~BTree()
{
	Destroy(root);
	root=NULL;
}
void BTree::Destroy(BTreeNode *p)
{
	if(p!=NULL)
	{
		Destroy(p->left);
		Destroy(p->right);
		delete p;
	}
}


void BTree::PreOrder(BTreeNode *p)
{
	if(p==NULL) p=root;
	cout<<"先序遍历二叉树: ";
	root->PreOrder(p);
	cout<<endl;
}
void BTree::InOrder(BTreeNode *p)
{
	if(p==NULL) p=root;
	cout<<"中序遍历二叉树: ";
	root->InOrder(p);
	cout<<endl;
}
void BTree::PostOrder(BTreeNode *p)
{
	if(p==NULL) p=root;
	cout<<"后序遍历二叉树: ";
	root->PostOrder(p);
	cout<<endl;
}