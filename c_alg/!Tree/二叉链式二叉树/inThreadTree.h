#include"ThreadTreeNode.h"
#include"string.h"
class ThreadTree
{
public:
	ThreadTreeNode *root;
	bool isThread;
	ThreadTree(dataType *str="");
	void inThread();
	void inThread(ThreadTreeNode *p);
	ThreadTreeNode *inNext(ThreadTreeNode *p);
	void inOrder();
	ThreadTreeNode *inLast(ThreadTreeNode *p);
	ThreadTreeNode *preNext(ThreadTreeNode *p);
	void preOrder();
	ThreadTreeNode *postLast(ThreadTreeNode *p);
	void postOrder();
	ThreadTreeNode* preCreate(dataType *str);
	void preInitiate(dataType *str);
	void destroyTree(ThreadTreeNode *p);
};
void ThreadTree::destroyTree(ThreadTreeNode *p)
{
	if(p!=NULL)
	{
		destroyTree(p->left);
		destroyTree(p->right);
		delete p;
	}
}
ThreadTreeNode* ThreadTree::preNext(ThreadTreeNode *p)
{
	if(!isThread) return NULL;
	if(!p->ltag)
		p=p->left;
	else
	{
		if(!p->rtag)
			p=p->right;
		else
		{
			while(p->rtag&&p->right!=NULL)
				p=p->right;
			p=p->right;
		}
	}
	return p;
}
void ThreadTree::preOrder()
{
	if(!isThread) return;
	ThreadTreeNode *p=root;
	if(p!=NULL)
	{
		cout<<"先序遍历中序线索二叉树: ";
		do
		{
			cout<<p->data<<" ";
			p=preNext(p);
		}while(p!=NULL);
		cout<<endl;
	}
}
ThreadTreeNode* ThreadTree::postLast(ThreadTreeNode *p)
{
	if(!isThread) return NULL;
	if(!p->rtag)
		p=p->right;
	else
	{
		while(p->ltag&&p->left!=NULL)
			p=p->left;
		p=p->left;
	}
	return p;
}


void ThreadTree::preInitiate(dataType *str)
{
	if(root!=NULL) 
	{
//		destroyTree(root);
		root=NULL;
	}
	if(str!="")
		root=preCreate(str);
	isThread=false;
	
}
ThreadTree::ThreadTree(dataType *str)
{
	preInitiate(str);
	isThread=false;
}
ThreadTreeNode* ThreadTree::preCreate(dataType *str)
{
	ThreadTreeNode *p=NULL;
	static int i=0;						
	static int l=strlen(str);
	if(i>=l) i=0;					//防止第二次调用时i不用0开始增加
	if(str[i]!='.')
	{
		p=new ThreadTreeNode(str[i++]);
		p->left=preCreate(str);
		p->right=preCreate(str);
	}
	else i++;
	return p;
}
void ThreadTree::inThread()
{
	inThread(root);
	isThread=true;
}
void ThreadTree::inThread(ThreadTreeNode *p)
{
	static ThreadTreeNode *front=NULL;
	if(p!=NULL)
	{
		inThread(p->left);
		if(p->left==NULL)
		{
			p->ltag=true;
			p->left=front;
		}
		if(p->right==NULL)
		p->rtag=true;
		if(front!=NULL&&front->rtag)
			front->right=p;
		front=p;
		inThread(p->right);
	}
}
ThreadTreeNode* ThreadTree::inLast(ThreadTreeNode *p)
{
	if(!isThread) return NULL;
	if(p->ltag)
		p=p->left;
	else
	{
		p=p->left;
		while(!p->rtag)
			p=p->right;
	}
	return p;
}
ThreadTreeNode* ThreadTree::inNext(ThreadTreeNode *p)
{
	if(!isThread) return NULL;
	if(p->rtag)
		p=p->right;
	else
	{
		p=p->right;
		while(!p->ltag)
			p=p->left;
	}
	return p;
}
void ThreadTree::inOrder()
{
	if(!isThread) return;
	ThreadTreeNode *p=root;
	if(p!=NULL)
	{
		cout<<"中序遍历中序线索二叉树: ";
		while(!p->ltag)
			p=p->left;
		do
		{
			cout<<p->data<<" ";
			p=inNext(p);
		}while(p!=NULL);
		cout<<endl;
	}
}
