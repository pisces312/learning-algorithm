#include"ThreadTreeNode.h"
class ThreadTree
{
public:
	ThreadTreeNode *root;
	ThreadTree(dataType *str="");
	ThreadTreeNode* create(char *str);
	void inThread();
	void inThread(ThreadTreeNode *p);
	ThreadTreeNode *inNext(ThreadTreeNode *p);
	void inOrder();
	ThreadTreeNode *inLast(ThreadTreeNode *p);
	ThreadTreeNode *preNext(ThreadTreeNode *p);
	void preOrder();
	ThreadTreeNode *postLast(ThreadTreeNode *p);
	void postOrder();
	ThreadTreeNode* preCreate(dataType *str);
	void preInitiate(dataType *str);
};/*

void THreadTree::preInitiate(dataType *str)
{
	if(str!="")
		root=preCreate(str);
}
ThreadTree::ThreadTree(dataType *str="")
{
	root=preInitiate(str);
}
ThreadTreeNode* ThreadTree::preCreate(dataType *str)
{
	ThreadTreeNode *p=NULL;
	static int i=0;						
	static int l=strlen(str);
	if(i>=l) i=0;					//防止第二次调用时i不用0开始增加
	if(str[i]!='.')
	{
		p=new BTreeNode(str[i++]);
		p->left=preCreate(str);
		p->right=preCreate(str);
	}
	else i++;
	return p;
}
void ThreadTree::inThread()
{
	inThread(root);
}
void ThreadTree::inThread(ThreadTreeNode *p)
{
	static ThreadTreeNode *front=NULL;
	if(p!=NULL)
	{
		inThread(p->left);
		if(p->left==NULL)
		{
			p->ltag=true;
			p->left=front;
		}
		if(p->right==NULL)
		p->rtag=true;
		if(front!=NULL&&front->rtag)
			front->right=p;
		cout<<p->data<<" ";
		front=p;
		inThread(p->right);
	}
}
ThreadTreeNode* ThreadTree::inLast(ThreadTreeNode *p)
{
	if(p->ltag)
		p=p->left;
	else
	{
		p=p->left;
		while(!p->rtag)
			p=p->right;
	}
	return p;
}
ThreadTreeNode* ThreadTree::inNext(ThreadTreeNode *p)
{
	if(p->rtag)
		p=p->right;
	else
	{
		p=p->right;
		while(!p->ltag)
			p-p->left;
	}
	return p;
}
void ThreadTree::inOrder()
{
	ThreadTreeNode *p=root;
	if(p!=NULL)
	{
		cout<<"中序遍历中序线索二叉树: ";
		while(!p->ltag)
			p=p->left;
		do
		{
			cout<<p->data<<" ";
			p=inNext(p);
		}while(p!=NULL);
		cout<<endl;
	}
}
*/