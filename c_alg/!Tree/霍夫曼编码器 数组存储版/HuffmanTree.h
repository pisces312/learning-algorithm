#include"HuffmanTreeNode.h"
#include<fstream>
#define maxvalue 32768;
class hTree
{
public:
	string encodestr,decodestr;
	int nodenum,size;
	hTNode* table;
	friend class hTNode;
	hTree(int n=0);
	void create();
	void encode(string &str);
	void decode(string &str);
	void output();
	hTree(string &str);
	void save();
	void printTreeTraverse(int i,int n=1);
	void printTree();
	int getNodenum();
};
void hTree::printTree()
{
	cout<<"霍夫曼树的凹入表示如下:\n";
	printTreeTraverse(size-1);
	cout<<endl;
}
int hTree::getNodenum()
{
	return nodenum;
}
void hTree::printTreeTraverse(int i,int n)
{
	if(i!=-1)
	{		
		printTreeTraverse(table[i].left,n+1);
		for(int j=0;j<n;j++)
			cout<<"   ";		
		cout<<table[i].data<<endl;		
		printTreeTraverse(table[i].right,n+1);
	}
}			
void hTree::save()
{		
	ofstream fout("code.txt");
	if(encodestr!="")
		fout<<"编码字符串为:"<<encodestr<<endl;
	if(decodestr!="")
		fout<<"解码字符串为:"<<decodestr<<endl;
}
void hTree::decode(string &str)
{
	cout<<"霍夫曼编码为"<<str<<"\n对应的字符串为:";
	int i,j=0,k,len,strlen=str.length();
	bool have;
	while(j<strlen)
	{
		have=false;
		for(i=0;i<nodenum;i++)
		{
			int t=0;
			len=table[i].bit.length();
			for(k=0;k<len;k++)
				if(table[i].bit[k]!=str[j+t]) break;
				else t++;
			if(k==len)
			{
				have=true;
				j+=t;
				decodestr+=table[i].data;				
			}
		}
		if(have==false)
		{
			cout<<"无法解码!";
			break;
		}
	}
	if(have) cout<<decodestr;
	cout<<endl;
}
hTree::hTree(string &str)
{
	int strlen=str.length(),i;
	int ascii[224];
	nodenum=0;
	for(i=0;i<224;i++)
		ascii[i]=0;
	for(i=0;i<strlen;i++)
		ascii[str[i]-32]++;
	table=new hTNode[224];
	for(i=0;i<224;i++)
		if(ascii[i]!=0) 
		{			
			table[nodenum].weight=ascii[i];
			table[nodenum++].data=i+32;
		}
}
hTree::hTree(int n)
{
	if(n==0) table=NULL;
	else
	{
		nodenum=n;
		table=new hTNode[2*n-1];	
		for(int i=0;i<n;i++)
		{
			cout<<"请输入第"<<i+1<<"个结点的权值: ";
			cin>>table[i].weight;
		}
	}
}
void hTree::encode(string &str)
{
	int i,j,strlen=str.length();
	for(i=0;i<strlen;i++)
		for(j=0;j<nodenum;j++)
		{
			if(table[j].data==str[i])
					encodestr+=table[j].bit;
		}
	cout<<str<<"的霍夫曼编码是"<<encodestr<<endl;
}
void hTree::output()
{
	if(table==NULL) return;
	cout<<"num  char    weight     parent    flag      left     right\n";
	size=2*nodenum-1;
	for(int i=0;i<size;i++)
	{
		cout<<setw(4)<<i<<setw(4)<<table[i].data;
		table[i].outputNode();
	}
}
void hTree::create()
{
	if(table==NULL) return;
	int i,j,w1,w2,x1,x2;
	for(i=0;i<nodenum-1;i++)
	{
		w1=w2=maxvalue;
		x1=x2=0;
		for(j=0;j<nodenum+i;j++)
		{
			if(table[j].weight<w1&&table[j].flag==false)
			{
				w2=w1;
				x2=x1;
				w1=table[j].weight;
				x1=j;
			}
			else if(table[j].weight<w2&&table[j].flag==false)
			{
				w2=table[j].weight;
				x2=j;
			}
		}
			table[x1].parent=table[x2].parent=nodenum+i;
			table[x1].flag=table[x2].flag=true;
			table[nodenum+i].weight=table[x1].weight+table[x2].weight;
			table[nodenum+i].left=x1;
			table[nodenum+i].right=x2;	
	}
	for(i=0;i<nodenum;i++)
	{
		int child=i,parent=table[child].parent;
		while(parent!=-1)
		{
			if(table[parent].left==child)
				table[i].bit+='0';
			else
				table[i].bit+='1';	
			child=parent;
			parent=table[child].parent;
		}
		string temp=table[i].bit;
		for(int k=0,j=table[i].bit.length()-1;j>=0;j--)
			table[i].bit[k++]=temp[j];			
		cout<<table[i].data<<"的Huffman编码为:";
			cout<<table[i].bit<<endl;
	}
}
