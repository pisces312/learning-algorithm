#include<iostream>
#include<iomanip>
#include<string>
#define width 10
using namespace std;
class hTNode
{
	int weight,parent,left,right;
	char data;
	bool flag;
	string bit;
public:
	friend class hTree;
	hTNode(char d='*',int w=0,int pa=-1,int l=-1,int r=-1);
	void outputNode();
};
void hTNode::outputNode()
{
	cout<<setw(width)<<weight<<setw(width)<<parent<<setw(width)<<flag<<setw(width)<<left<<setw(width)<<right<<endl;
}
hTNode::hTNode(char d,int w,int pa,int l,int r)
{
	data=d;
	weight=w;
	flag=false;
	parent=pa;
	left=l;
	right=r;
}
