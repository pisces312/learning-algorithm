#include<iostream>
#include<iomanip>
#include<algorithm>
using namespace std;
const int maxweight=10000;
struct edgeNode
{
	int begin,end;
	double weight;
};
class arcNode
{
public:
	int adjvex;
	double weight;
	arcNode *nextarc;
	arcNode(int end=-1,double w=maxweight)
	{
		adjvex=end;
		nextarc=NULL;
		weight=w;
	}
};
class vNode
{
public:
	char data;
	arcNode* firstarc;
	vNode(char v='\0')
	{
		data=v;
		firstarc=NULL;
	}
};
class adjGraph
{
	vNode* table;
	int vexnum,arcnum;
	int kruskalfind(int father[],int v);
	void createHeapBig(edgeNode edge[],int en,int h);
public:
	adjGraph(int n);
	~adjGraph()
	{
		delete []table;	
	}
	void output();
	void createGraphEnd(int v,char *vertex,int e,edgeNode edge[]);
	void addEdgeEnd(edgeNode &edge);
	bool isExist(int begin,int end);
	double prim(int i=0);
	double getWeight(int begin,int end);		
	void heapSort(edgeNode edge[],int n);
	double kruskal(edgeNode edge[]);	
	void setEdge(edgeNode& e1,edgeNode& e2);
};
void adjGraph::createHeapBig(edgeNode edge[],int en,int h)//h为开始下标，n为元素个数
{
	int i=h,j=2*h+1;
	edgeNode temp;
	setEdge(temp,edge[i]);
	while(j<en)
	{
		if(j<en-1&&edge[j].weight<edge[j+1].weight) j++;
		if(temp.weight>=edge[j].weight)
			break;
		else
		{
			setEdge(edge[i],edge[j]);
			i=j;
			j=2*j+1;
		}
	}
	setEdge(edge[i],temp);	
}
void adjGraph::heapSort(edgeNode edge[],int n)
{
	for(int i=(n-1)/2;i>=0;i--)
		createHeapBig(edge,n,i);
	for(i=n-1;i>0;i--)
	{
		swap(edge[0],edge[i]);
		createHeapBig(edge,i,0);
	}
}
int adjGraph::kruskalfind(int father[],int v)
{
	while(father[v]>0)
		v=father[v];
	return v;
}
double adjGraph::kruskal(edgeNode edge[])     
{
	cout<<"克鲁斯卡尔方法生成最小生成树\n";
	int i,vf1,vf2,en=arcnum/2;
	double sum=0;
	heapSort(edge,en);
	int *father=new int[vexnum];	
	for(i=0;i<vexnum;i++)	
		father[i]=0;
	for(i=0;i<vexnum-1;i++)
	{		
		vf1=kruskalfind(father,edge[i].begin);
		vf2=kruskalfind(father,edge[i].end);
		if(vf1!=vf2)
		{
			father[vf2]=vf1;
			sum+=edge[i].weight;
			cout<<table[edge[i].begin].data<<"->"<<table[edge[i].end].data<<"权值"<<edge[i].weight<<endl;
		}
	}
	return sum;
}
void adjGraph::setEdge(edgeNode& e1,edgeNode& e2)
{
	e1.begin=e2.begin;
	e1.end=e2.end;
	e1.weight=e2.weight;
}
double adjGraph::getWeight(int begin,int end)
{
	if(!isExist(begin,end)) return maxweight;
	arcNode *p=table[begin].firstarc;
	while(p!=NULL)
	{
		if(p->adjvex==end)
			return p->weight;
		p=p->nextarc;
	}
	return maxweight;
}
double adjGraph::prim(int i)
{  
	double sum=0;
	cout<<"普里姆方法生成最小生成树\n";
	edgeNode *closedge=new edgeNode[vexnum];
	edgeNode *t=new edgeNode[vexnum];
	for(int j=i;j<vexnum;j++)
	{
		closedge[j].begin=0;
		if(j!=i)
		{	
			closedge[j].end=i;
			closedge[j].weight=getWeight(i,j);
		}
	}
	closedge[i].end=0;
	closedge[i].weight=0;
	for(j=1;j<vexnum;j++)
	{
		for(i=0;i<vexnum;i++)
			setEdge(t[i],closedge[i]);
		int k=0;
		double min=0;
		heapSort(t,vexnum);
		while(t[k].weight==0) k++;
		min=t[k].weight;
		for(k=0;k<vexnum;k++)
			if(closedge[k].weight==min) i=k;
		cout<<table[closedge[i].end].data<<"->"<<table[i].data<<"权值"<<closedge[i].weight<<endl;
		sum+=closedge[i].weight;
		closedge[i].weight=0;
		for(k=0;k<vexnum;k++)
		{
			if(getWeight(i,k)<closedge[k].weight)
			{
				closedge[k].end=i;
				closedge[k].weight=getWeight(i,k);
			}
		}
	}
	return sum;
}
bool adjGraph::isExist(int begin,int end)
{
	arcNode *p=table[begin].firstarc;
	while(p!=NULL)
	{
		if(p->adjvex==end)
			return true;
		p=p->nextarc;
	}
	return false;
}
void adjGraph::addEdgeEnd(edgeNode &edge)
{
	int begin=edge.begin,end=edge.end;
	double weight=edge.weight;
	if(isExist(begin,end)) 
	{
		cout<<"从顶点"<<table[begin].data<<"到顶点"<<table[end].data<<"的边已存在"<<endl;
		return;
	}
	arcNode *p=table[begin].firstarc;
	if(p==NULL)
		table[begin].firstarc=new arcNode(end,weight);
	else
	{
		while(p->nextarc!=NULL)
			p=p->nextarc;
		p->nextarc=new arcNode(end,weight);
	}
	p=table[end].firstarc;
	if(p==NULL)
		table[end].firstarc=new arcNode(begin,weight);
	else
	{
		while(p->nextarc!=NULL)
			p=p->nextarc;
		p->nextarc=new arcNode(begin,weight);
	}
	arcnum+=2;
}
void adjGraph::createGraphEnd(int v,char *vertex,int e,edgeNode edge[])
{
	vexnum=v;
	for(int i=0;i<vexnum;i++)
	{
		table[i].data=vertex[i];	
		table[i].firstarc=NULL;
	}
	for(i=0;i<e;i++)
		addEdgeEnd(edge[i]);
}
adjGraph::adjGraph(int n)
{
	table=new vNode[n];
	vexnum=n;
	arcnum=0;
}
void adjGraph::output()
{
	cout<<"无向图带权邻接表存储\n";
	cout<<"下标   顶点   邻接表\n";
	for(int i=0;i<vexnum;i++)
	{	
		cout<<setw(3)<<i<<setw(6)<<table[i].data<<setw(3)<<" ";
		arcNode *p=table[i].firstarc;
		while(p!=NULL)
		{
			cout<<setw(3)<<p->adjvex;
			p=p->nextarc;
		}
		cout<<endl;
	}
}