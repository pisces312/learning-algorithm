#include "BTree.h"
#include <stdlib.h>

#pragma warning(default: 4200)

/// 树节点构造函数，一开始要给m_key和m_point分配适合的内存空间以存放关键码和指针
/// @param  keylength          关键码长度
/// @param  order              树节点的度数
/// @param  datalen            指针的长度
BTreeNode::BTreeNode()
{
	m_key=m_point=NULL;
    m_parent=NULL;
    m_pre=m_next=NULL;
}


BTreeNode::~BTreeNode(){
	free(m_key);
    free(m_point);
    m_key=NULL;
    m_point=NULL;
	m_parent=NULL;
	m_pre=NULL;
	m_next=NULL;
	m_count=0;
	isLeaf=false;
	}

/// 初始化一个树节点，为各个属性赋值
HRESULT BTreeNode::BnodeInit(
		unsigned int keylen,
	    unsigned int datalen,
		unsigned int order,
		bool  leaf
		){
	
	m_key=malloc(keylen*(order+1));
	memset(m_key,0,keylen*(order+1));
    
	m_point=malloc(datalen*(order+1));
    memset(m_point,NULL,datalen*(order+1));

	if(m_key==NULL||m_point==NULL)
		return E_OUTOFMEMORY;
	m_count=0;
    isLeaf=leaf;
	return S_OK;
	}

bool BTreeNode::get_isLeaf()
{
	return isLeaf;
	}
BTreeNode *BTreeNode::get_pare()
{
	return m_parent;
	}
void *BTreeNode::get_key()
{
	return m_key;
	}
void *BTreeNode::get_point()
{
	return m_point;
	}
BTreeNode *BTreeNode::get_pre()
{
	return m_pre;
	}
BTreeNode *BTreeNode::get_next()
{
	return m_next;
	}
void BTreeNode::set_count(unsigned int count)
{
	m_count=count;
	}
void BTreeNode::set_leaf(bool leaf)
	{
	isLeaf=leaf;
	}
void BTreeNode::set_pare(BTreeNode* pare)
	{
	m_parent=pare;
	}
void BTreeNode::set_pre(BTreeNode* pre)
	{
	m_pre=pre;
	}
void BTreeNode::set_next(BTreeNode* next)
	{
	m_next=next;
	}

/// B+树构造函数
BTree::BTree()
{
	m_pRoot=NULL;
    m_fIsInitialized=false;
}

/*
BTree::~BTree()
{
    Uninitialize();
}
*/
/// Initialize:         初始化B+树
/// @param  uOrder      B+树的阶数
/// @param  uKeyLength  键值的长度
/// @param  uDataLength 数据的长度
/// @return 如果成功，返回S_OK；否则，返回失败原因
HRESULT BTree::Initialize(
    unsigned int uOrder,
	unsigned int uKeyLength,
	unsigned int uDataLength
    )
{
    if (m_fIsInitialized)
        return E_FAIL;

    if (0 == uOrder)
        return E_INVALIDARG;

    HRESULT hr = S_OK;
    m_uOrder = uOrder;

    // 一开始创建一个类型为叶节点的根结点
	m_pRoot = new BTreeNode();
    if (NULL == m_pRoot)
    {
        hr = E_OUTOFMEMORY;
        goto end;
    }
	//current
	//初始化根结点
	m_pRoot->BnodeInit(uKeyLength,uDataLength,m_uOrder,true);
	current.r=m_pRoot;
	current.i=0;
	current.tag=0;
end:
    if (SUCCEEDED(hr))
    {
        m_fIsInitialized = true;
    }
    else
    {
        Uninitialize();
    }
    return hr;
}
/*
/// Uninitialize: 释放B+树,对书中的每个节点用delete释放，
///delete前要调用各节点自己的析构函数以释放m_key和m_point所指向的资源。
///要通过遍历释放树中的所有节点的资源。。。。。未完待续
void BTree::Uninitialize()
{
    
}
*/
/// Search:在B树中查找键值为pKey指向值的叶节点，
/// 若找到，返回所在节点位置和节点内偏移；若没找到，返回新索引条目应插入的位置
/// @param  pKey        键值
/// @param  uKeyLength  键值的长度
Triple BTree::Search(
		const void *pKey
        )
{
	Triple result;
	BTreeNode *p=m_pRoot;
	char *val=(char *)pKey,*t=NULL;
	int i;
	
	while(!p->get_isLeaf()){ //在内节点中查找，到叶节点时跳出
		t=(char *)p->get_key();
		i=0;
		
		while(memcmp(t,val,m_uKeyLength)<0&&(i+1)<=p->get_count()){//key[i+1]>=pkey时或节点内所有键值小于pkey时跳出
			t=t+m_uKeyLength;
			i=i+1;
			}
		if(memcmp(t,val,m_uKeyLength)==0&&(i+1)<=p->get_count()){//内节点中找到该键值，向下追溯到叶节点的相应位置
			p=(BTreeNode *)p->get_point()+i+1;
			while(!p->get_isLeaf())//到达叶节点时跳出
				p=(BTreeNode *)p->get_point();
			}
		p=(BTreeNode *)p->get_point()+i;//该节点中没有找到，向子树中查找
		}
	//到达叶节点
	result.r=p;
	i=0;
	t=(char *)p->get_key();
	while(memcmp(t,val,m_uKeyLength)<0&&(i+1)<=p->get_count()){
		t=t+m_uKeyLength;
		i=i+1;
		}
	if(memcmp(t,val,m_uKeyLength)==0&&(i+1)<=p->get_count()){
		result.i=i+1;
		result.tag=0;
		return result;
		}
	result.i=i;
	result.tag=1;
	return result;
}

/// InsertKey:                      在节点中指定位置插入(key,p)二元组，在Insert中调用，至于节点是否分裂留给Insert查看
/// @param  pNode                   指向要插入的节点
/// @param  loc                     节点中要插入的位置k[loc],k[loc+1]之间
/// @param  pKey                    指向要插入的键值
/// @param  datelen                 插入不同类型节点指针类型不同
/// @param  kp                      键值对应的指针
/// @return 如果节点存在，返回S_OK；不存在，返回S_FALSE；否则，返回失败原因
void BTree::InsertKey(
		BTreeNode  *pNode,
		unsigned int  loc,
        unsigned int datelen,
        void *pKey,
        void  *kp
		)
{
   char *key=(char *)pKey,*point=(char *)kp;
   int i=pNode->get_count();
   char *p=(char *)pNode->get_key()+m_uKeyLength*i;
   char *q=(char *)pNode->get_point()+datelen*(i+1);
   while((i+1)>loc){
       memcpy(p+m_uKeyLength,p,m_uKeyLength);
       memcpy(q+datelen,q,datelen);
	   p-=m_uKeyLength;
	   q-=datelen;
	   i-=1;
	   }
   memcpy(p,key,m_uKeyLength);
   memcpy(q,point,datelen);
   pNode->set_count(pNode->get_count()+1);
}

/// Move:                      将pNode的key[s...m]和point[start,s...m]移到qNode的key[1...m-s]和point[0...s-1]中
///                            修改p,q的count，在Insert中调用
/// @param  pNode              移出节点
/// @param  qNode              移入节点
/// @param  start              q的起始指针
/// @param  s，m               界定范围
/// @param  datelen            不同类型节点指针类型不同
void BTree::Move(
	BTreeNode  *pNode,
	BTreeNode  *qNode,
	void  *start,
	int   s,
	unsigned int mloc,
	unsigned int datelen
	)
{
    char *k1=(char *)pNode->get_key()+(s-1)*m_uKeyLength;
	char *p1=(char *)pNode->get_point()+s*datelen;
	
	char *k2=(char *)qNode->get_key(),*p2=(char *)qNode->get_point();
	
	memcpy(p2, (char *)start,datelen);
	p2+=datelen;
	int i=s;
	while(i<=mloc){
		memcpy(k2,k1,m_uKeyLength);
		memcpy(p2,p1,datelen);
        k1+=m_uKeyLength;
		k2+=m_uKeyLength;
        p1+=datelen;
		p2+=datelen;
		}
	pNode->set_count(s-1);//注意，调用move时还要根据是内节点/叶节点分裂的不同情况，对pNode->count加1
	qNode->set_count(mloc-s+1);
}

/// Insert:             插入节点,当节点中关键码达到m_uOrder-1时，分裂节点
/// @param  pKey        键值
/// @param  uKeyLength  键值的长度
/// @param  pData       插入关键码的右邻指针
/// @param  uDataLength 指针的长度
/// @return 如果成功，返回S_OK；否则，返回失败原因
HRESULT BTree::Insert(
    void *pKey,
    unsigned int uKeyLength,
    void *pData,
    unsigned int uDataLength
    )
{
    if (!m_fIsInitialized)
        return E_FAIL;
	if(uKeyLength>m_uKeyLength||uDataLength>m_uDataLen)
        return E_INVALIDARG;
    Triple location=Search(pKey);
	if(location.tag==0)       //已经存在该键值，插入不成功
		return E_FAIL;
	HRESULT hr = S_OK;

	BTreeNode *p=location.r,*q,*t; //插入叶节点
	unsigned int j=location.i;//节点内的位置
	void *k=pKey,*ap=pData; //k分裂时向上插入的键值
                            //ap分裂时向上插入的指针
	
	while(1){ //由于可能的分裂，将导致多次插入
		if(p->get_count()<(m_uOrder-1)){ //节点中关键码未超出，直接插入即可
			InsertKey(p,j,m_uDataLen,k,ap);
			return hr;
			}
        
        int s=(m_uOrder+1)/2; //准备分裂，s是要向上插入的键值的位置
		InsertKey(p,j,sizeof(BTreeNode *),k,ap);
        q=new BTreeNode();
		
		if(p->get_isLeaf()){ //叶节点的分裂
			q->BnodeInit(m_uKeyLength,m_uDataLen,m_uOrder,true);
            Move(p,q,NULL,s,m_uOrder,m_uDataLen);
			p->set_next(q);
			q->set_pre(p);
			}
		else{ //内部节点分裂
			q->BnodeInit(m_uKeyLength,sizeof(BTreeNode *),m_uOrder,false);
			Move(p,q,(BTreeNode *)p->get_point()+s,s+1,m_uOrder,sizeof(BTreeNode *));
			p->set_count(p->get_count()-1);
			}

		k=(char *)p->get_key()+m_uKeyLength*(s-1);//准备向上插入的二元组(k,ap)
        ap=q;
		
		if(p->get_pare()!=NULL){//原来的p不为根
			t=p->get_pare();
			char *t1=(char *)t->get_key();
			j=0;

			while(memcmp(t1,k,m_uKeyLength)<0&&(j+1)<=t->get_count()){//key[j+1]>k时或节点内所有键值小于pkey时跳出
			    t1=t1+m_uKeyLength;
			    j=j+1;
			  }
			q->set_pare(p->get_pare());
			p=t;
			}
		else{ //原来的p是根结点，要产生新的根
			m_pRoot=new BTreeNode();
			m_pRoot->BnodeInit(m_uKeyLength,sizeof(BTreeNode *),m_uOrder,false);
			m_pRoot->set_count(1);
			memcpy((char *)m_pRoot->get_key(),k,m_uKeyLength);
			char *t2=(char *)m_pRoot->get_point();
			memcpy(t2,(char *)p,sizeof(BTreeNode *));
            memcpy(t2+sizeof(BTreeNode *),(char *)ap,sizeof(BTreeNode *));
			q->set_pare(m_pRoot);
			p->set_pare(m_pRoot);
			return hr;
			}
		}
}

/// GetFirst:                       定位第一个节点
/// @param  pKeyBuffer              返回第一个节点的键值
/// @param  uKeyBufferLength        键值Buffer的长度
/// @param  pDataBuffer             返回第一个节点的数据
/// @param  uDataBufferLength       数据Buffer的长度
/// @return 如果节点存在，返回S_OK；不存在，返回S_FALSE；否则，返回失败原因
HRESULT BTree::GetFirst(
    void *pKeyBuffer,
    unsigned int uKeyBufferLength,
    void *pDataBuffer,
    unsigned int uDataBufferLength
    )
{
    if(!m_fIsInitialized)
        return E_FAIL;
	if(uKeyBufferLength<m_uKeyLength||uDataBufferLength<m_uDataLen)//缓冲区大小不够
        return E_INVALIDARG;
	if(m_pRoot->get_count()==0) //空树，没有节点存在
		return S_FALSE;
    HRESULT hr=S_OK;

	BTreeNode *p=m_pRoot;
	char *data;
	while(!p->get_isLeaf())
		p=(BTreeNode *)p->get_point();
    //到达叶节点，找到第一个索引条目
	current.r=p;
	current.i=1;
	data=(char *)p->get_point()+m_uDataLen;
	memcpy(pKeyBuffer,p->get_key(),m_uKeyLength);
    memcpy(pDataBuffer,data,m_uDataLen);
	return hr;
}

/// GetNext:                        定位下一节点，在调用GetFirst之后调用
/// @param  pKeyBuffer              返回第一个节点的键值
/// @param  uKeyBufferLength        键值Buffer的长度
/// @param  pDataBuffer             返回第一个节点的数据
/// @param  uDataBufferLength       数据Buffer的长度
/// @return 如果节点存在，返回S_OK；不存在，返回S_FALSE；否则，返回失败原因
HRESULT BTree::GetNext(
    void *pKeyBuffer,
    unsigned int uKeyBufferLength,
    void *pDataBuffer,
    unsigned int uDataBufferLength
    )
{
    if (!m_fIsInitialized)
        return E_FAIL;
    if(uKeyBufferLength<m_uKeyLength||uDataBufferLength<m_uDataLen)//缓冲区大小不够
        return E_INVALIDARG;
    HRESULT hr=S_OK;
	
	BTreeNode *p=current.r;
	if(p==NULL)
		return S_FALSE;
	if(current.i==p->get_count()){//已达到当前节点中最后一个索引
		if(p->get_next()==NULL)//当前节点是最后一个节点
			return S_FALSE;
		current.r=p->get_next();//走向下一个节点
		current.i=1;
		}
	else //还在当前节点中，向后读一个索引
		current.i+=1;
	char *key=(char *)(current.r)->get_key()+m_uKeyLength*(current.i-1);
	memcpy(pKeyBuffer,key,m_uKeyLength);
	char *data=(char *)(current.r)->get_point()+m_uDataLen*(current.i);
	memcpy(pDataBuffer,data,m_uDataLen);
	return hr;
}

/// FindFirst:                      查找符合条件的第一个节点
/// @param  pKey                    查找的键值
/// @param  uKeyLength              要查找的键值的长度
/// @param  eCompare                查找的是（等于/大于/...）的键值
/// @param  pKeyBuffer              返回第一个节点的键值
/// @param  uKeyBufferLength        键值Buffer的长度
/// @param  pDataBuffer             返回第一个节点的数据
/// @param  uDataBufferLength       数据Buffer的长度
/// @return 如果节点存在，返回S_OK；不存在，返回S_FALSE；否则，返回失败原因
HRESULT BTree::FindFirst(
    const void *pKey,
    unsigned int uKeyLength,
    BTreeFindCompare eCompare,
    void *pKeyBuffer,
    unsigned int uKeyBufferLength,
    void *pDataBuffer,
    unsigned int uDataBufferLength
    )
{
    if (!m_fIsInitialized)
        return E_FAIL;
    return E_NOTIMPL;
}

/// FindNext:                       查找符合条件的下一个节点，在FindFirst之后调用
/// @param  pKeyBuffer              返回下一个节点的键值
/// @param  uKeyBufferLength        键值Buffer的长度
/// @param  pDataBuffer             返回下一个节点的数据
/// @param  uDataBufferLength       数据Buffer的长度
/// @return 如果节点存在，返回S_OK；不存在，返回S_FALSE；否则，返回失败原因
HRESULT BTree::FindNext(
    void *pKeyBuffer,
    unsigned int uKeyBufferLength,
    void *pDataBuffer,
    unsigned int uDataBufferLength
    )
{
    if (!m_fIsInitialized)
        return E_FAIL;
    return E_NOTIMPL;
}

