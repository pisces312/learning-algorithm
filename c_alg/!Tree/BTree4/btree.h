#ifndef BTREE_H
#define BTREE_H

#include <windows.h>

/// 用于B+树查找
enum BTreeFindCompare
{
    eEqual,
    eGreater,
    eLess,
    eGreaterOrEqual,
    eLessOrEqual
};

class BTreeNode;

///
///搜索结果结构
///
struct Triple
{
	BTreeNode    *r;  //所在或应插入的B树叶节点
	unsigned int i;   //节点中的插入位置
	int          tag; //搜索成功标志
};

///
///  B+树
///
class BTree
{
public:
    BTree();
    ~BTree();
    /// Initialize:         初始化B+树
    /// @param  uOrder      B+树的阶数
    /// @param  uKeyLength  键值的长度
    /// @param  uDataLength 数据的长度
    /// @return 如果成功，返回S_OK；否则，返回失败原因
    HRESULT Initialize(
        unsigned int uOrder,
		unsigned int uKeyLength,
		unsigned int uDataLength
        );

    /// Uninitialize: 释放B+树
    void Uninitialize();

    /// Insert:             插入节点
    /// @param  pKey        键值
    /// @param  uKeyLength  键值的长度
    /// @param  pData       数据
    /// @param  uDataLength 数据的长度
    /// @return 如果成功，返回S_OK；否则，返回失败原因
    HRESULT Insert(
        void *pKey,
        unsigned int uKeyLength,
        void *pData,
        unsigned int uDataLength
        );
    
    /// GetFirst:                       定位第一个节点
    /// @param  pKeyBuffer              返回第一个节点的键值
    /// @param  uKeyBufferLength        键值Buffer的长度
    /// @param  pDataBuffer             返回第一个节点的数据
    /// @param  uDataBufferLength       数据Buffer的长度
    /// @return 如果节点存在，返回S_OK；不存在，返回S_FALSE；否则，返回失败原因
    HRESULT GetFirst(
        void *pKeyBuffer,
        unsigned int uKeyBufferLength,
        void *pDataBuffer,
        unsigned int uDataBufferLength
        );

    /// GetNext:                        定位下一节点，在调用GetFirst之后调用
    /// @param  pKeyBuffer              返回第一个节点的键值
    /// @param  uKeyBufferLength        键值Buffer的长度
    /// @param  pDataBuffer             返回第一个节点的数据
    /// @param  uDataBufferLength       数据Buffer的长度
    /// @return 如果节点存在，返回S_OK；不存在，返回S_FALSE；否则，返回失败原因
    HRESULT GetNext(
        void *pKeyBuffer,
        unsigned int uKeyBufferLength,
        void *pDataBuffer,
        unsigned int uDataBufferLength
        );

    /// FindFirst:                      查找符合条件的第一个节点
    /// @param  pKey                    查找的键值
    /// @param  uKeyLength              要查找的键值的长度
    /// @param  eCompare                查找的是（等于/大于/...）的键值
    /// @param  pKeyBuffer              返回第一个节点的键值
    /// @param  uKeyBufferLength        键值Buffer的长度
    /// @param  pDataBuffer             返回第一个节点的数据
    /// @param  uDataBufferLength       数据Buffer的长度
    /// @return 如果节点存在，返回S_OK；不存在，返回S_FALSE；否则，返回失败原因
    HRESULT FindFirst(
        const void *pKey,
        unsigned int uKeyLength,
        BTreeFindCompare eCompare,
        void *pKeyBuffer,
        unsigned int uKeyBufferLength,
        void *pDataBuffer,
        unsigned int uDataBufferLength
        );
	

    /// FindNext:                       查找符合条件的下一个节点，在FindFirst之后调用
    /// @param  pKeyBuffer              返回下一个节点的键值
    /// @param  uKeyBufferLength        键值Buffer的长度
    /// @param  pDataBuffer             返回下一个节点的数据
    /// @param  uDataBufferLength       数据Buffer的长度
    /// @return 如果节点存在，返回S_OK；不存在，返回S_FALSE；否则，返回失败原因
    HRESULT FindNext(
        void *pKeyBuffer,
        unsigned int uKeyBufferLength,
        void *pDataBuffer,
        unsigned int uDataBufferLength
        );

private:
    bool            m_fIsInitialized;           // 是否初始化
    BTreeNode       *m_pRoot;                   //根结点，总是在变化的，最初是一个叶子节点
    unsigned int    m_uOrder;                   // 树的阶数
    unsigned int    m_uKeyLength;               // 键值的长度
	unsigned int    m_uDataLen;                //叶节点指针的长度
	Triple          current;                  //维护遍历B+树时的遍历到的当前位置，在GetFirst()时置第一个索引
	                                           //在FindFisrt时置满足条件的第一个索引

    /// Search:                       在B树中搜索键值为给定值的索引条目，在FindFirst或Insert中调用
    /// @param  uKeyLength              给定键值的长度
	Triple Search(
		const void *pKey
		);
	
	/// InsertKey:                      在节点中指定位置插入(key,p)二元组，在Insert中调用，至于节点是否分裂留给Insert查看
    /// @param  pNode                   指向要插入的节点
	/// @param  loc                     节点中要插入的位置k[loc],k[loc+1]之间
    /// @param  pKey                    指向要插入的键值
    /// @param  kp                      插入键值的地址
    /// @return 如果节点存在，返回S_OK；不存在，返回S_FALSE；否则，返回失败原因
    void InsertKey(
		BTreeNode  *pNode,
		unsigned int  loc,
		unsigned int datelen,
        void *pKey,
        void  *kp
		);

	/// Move:                      将pNode的key[s+1...m]和point[start,s+1...m]移到qNode的key[0...s-1]和point[0...s-1]中，修改p,q的count，在Insert中调用
    /// @param  pNode              移出节点
	/// @param  qNode              移入节点
    /// @param  start              q的起始指针
    /// @param  s，m               界定范围
	/// @param  datelen            不同类型节点指针类型不同
	void Move(
	   BTreeNode  *pNode,
	   BTreeNode  *qNode,
	   void  *start,
	   int   s,
	   unsigned int mloc,
	   unsigned int datelen
	);
}; 

///
///B树节点
///
class BTreeNode
{
public:
	
	/// 树节点构造函数，一开始要给m_key和m_point分配适合的内存空间以存放关键码和指针
    
	BTreeNode();

	~BTreeNode();
	/// BnodeInit:                 初始化B树节点
	/// @param  keylength          关键码长度
	/// @param  order              树节点的度数
    /// @param  datalen            指针的长度，可控制创建的节点类型：叶节点分裂时创建新的叶节点
	///                            内节点分裂时，创建新的内部节点
	/// @param  leaf               是否叶节点
    HRESULT BnodeInit(
        unsigned int keylen,
	    unsigned int datalen,
		unsigned int order,
		bool  leaf
		);
    unsigned int get_count();
	bool         get_isLeaf();
	BTreeNode    *get_pare();
	void         *get_key();
    void         *get_point();
	BTreeNode    *get_pre();
	BTreeNode    *get_next();
	

	void   set_count(unsigned int count);
	void   set_leaf(bool leaf);
	void   set_pare(BTreeNode* pare);
	void   set_pre(BTreeNode* pre);
	void   set_next(BTreeNode* next);
private:
	unsigned int    m_count;  //当前节点中的索引条目数
    bool            isLeaf;  //是否叶节点
    BTreeNode       *m_parent;//指向双亲节点
	BTreeNode       *m_pre;  //（叶节点中）指向前一个叶节点
	BTreeNode       *m_next;//（叶节点中）指向后一个叶节点
	void            *m_key;  //关键码数组
    void            *m_point; //指向子树或索引键值对应元组的物理地址,
	                        //point数组中的每个元素可以是任意类型的指针
};

#endif
