#include <stdio.h>
#include "btree.h"
#include <assert.h>
#include <vector>
#include <algorithm>

#define BTREE_ORDER     4
#define TEST_NODE_COUNT 40

void CreateTestBTree(BTree &btree)
{
    HRESULT hr;
    int key;
    int data;

	hr = btree.Initialize(BTREE_ORDER, sizeof(int), sizeof(int));
    assert(S_OK == hr);
    
    std::vector<int> vint;
    for (int i = 1; i <= TEST_NODE_COUNT; ++i)
    {
        vint.push_back(i);
    }
    std::random_shuffle(vint.begin(), vint.end());

    for (std::vector<int>::iterator iter = vint.begin(); iter != vint.end(); ++iter)
    {
        key = *iter;
        data = key*10;
		hr = btree.Insert(&key, sizeof(key), &data, sizeof(data));
        assert(S_OK == hr);
    }
}

void TestBTreeWalk()//遍历所有的索引条目
{
    BTree btree;
    HRESULT hr;
    int key;
    int data;

    CreateTestBTree(btree);
    hr = btree.GetFirst(&key, sizeof(key), &data, sizeof(data));
    assert(S_OK == hr);
    assert(1 == key);
    assert(key*10 == data);
    for (int i = 2; i <= TEST_NODE_COUNT; ++i)
    {
        hr = btree.GetNext(&key, sizeof(key), &data, sizeof(data));
        assert(S_OK == hr);
        assert(i == key);
        assert(key*10 == data);
    }
    hr = btree.GetNext(&key, sizeof(key), &data, sizeof(data));
    assert(S_FALSE == hr);

    btree.Uninitialize();
}
/*
void TestBTreeFind()//查找满足各种条件的索引条目
{
    BTree btree;
    HRESULT hr;
    int key;
    int data;
    int findKey;

    CreateTestBTree(btree);
    for (int i = 1; i <= TEST_NODE_COUNT; ++i)
    {
        // 查找等于
        findKey = i;
        hr = btree.FindFirst(&findKey, sizeof(findKey), eEqual, &key, sizeof(key), &data, sizeof(data));
        assert(S_OK == hr);
        assert(findKey == key);
        assert(key*10 == data);
        hr = btree.GetNext(&key, sizeof(key), &data, sizeof(data));
        assert(S_FALSE == hr);

        // 查找大于等于
        findKey = i;
        hr = btree.FindFirst(&findKey, sizeof(findKey), eGreaterOrEqual, &key, sizeof(key), &data, sizeof(data));
        assert(S_OK == hr);
        assert(findKey == key);
        assert(key*10 == data);
        for (int j = i+1; j <= TEST_NODE_COUNT; ++j)
        {
            hr = btree.FindNext(&key, sizeof(key), &data, sizeof(data));
            assert(S_OK == hr);
            assert(j == key);
            assert(key*10 == data);
        }
        hr = btree.GetNext(&key, sizeof(key), &data, sizeof(data));
        assert(S_FALSE == hr);

        // 查找小于等于
        findKey = i;
        hr = btree.FindFirst(&findKey, sizeof(findKey), eLessOrEqual, &key, sizeof(key), &data, sizeof(data));
        assert(S_OK == hr);
        assert(findKey == key);
        assert(key*10 == data);
        for (int j = i-1; j >= 1; --j)
        {
            hr = btree.FindNext(&key, sizeof(key), &data, sizeof(data));
            assert(S_OK == hr);
            assert(j == key);
            assert(key*10 == data);
        }
        hr = btree.GetNext(&key, sizeof(key), &data, sizeof(data));
        assert(S_FALSE == hr);
    }

    btree.Uninitialize();
}
*/
int main(int argc, char *argv[])
{
    TestBTreeWalk();
    //TestBTreeFind();
    return 0;
}