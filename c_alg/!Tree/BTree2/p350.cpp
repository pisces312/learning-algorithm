#include "iostream.h"
#ifndef MAXKEY
#define MAXKEY 32767
#endif
const int MaxM=100;
template <class Type> class Mtree;
template <class Type> class Btree;
template <class Type> class Mnode {					// B_树结点类定义
    public:
    void insertkey(int,Type,Mnode<Type>*);
    Mnode() {
        n=0;
        parent=NULL;
        for (int i=0;i<=MaxM;i++) ptr[i]=NULL;
    }
    void printKey() {
        cout<<"=====node=====\n";
        for (int i=1;i<=n;i++) {
            cout<<key[i]<<" ";
        }
        cout<<"\n==============\n";
    }
    private:
    int n;									//当前结点中关键码个数
    Mnode<Type> *parent;						//双亲结点指针
    Type key[MaxM+1];								//key[MaxM]为监视哨兼工作单元, key[0]未用
    Mnode<Type> *ptr[MaxM+1];						//子树结点指针数组, ptr[m]未用
    friend ostream& operator <<(ostream& strm, Mnode<Type>& mn);
    friend class Mtree<Type>;
    friend class Btree<Type>;
};

template <class Type>
class Triple {								//搜索结果
    public:
    Mnode<Type> *r;							//B_树结点地址
    int i;
    char tag;							//结点中关键码序号及搜索成功标志
    friend ostream& operator <<(ostream& strm, Triple<Type>& tp);
};

template <class Type>
class Mtree {
    public:
    Mnode<Type> *root;
    Mtree(int d=3) {
        root=NULL;
        m=d;
    };
    void insertkey(Mnode<Type>*,int,Type,Mnode<Type>*);
    Triple<Type>  Search ( const Type & );
    friend ostream& operator <<(ostream& strm, Mtree<Type>& mt);
//		protected:
    void move ( Mnode<Type>*p, Mnode<Type>*q, int s, int m );
    void print(ostream& strm,Mnode<Type> *p);

    int m;
};

template <class Type>
Triple<Type>  Mtree<Type>::Search ( const Type & x ) {
    //用关键码x搜索驻留在磁盘上的m路搜索树。各结点格式为n, A[0], (k[1],A[1]),……,  (K[n],A[n]), n < m。
    //函数返回一个类型为Triple (r, i, tag)的对象。tag=1, 表示x 在结点r找到, 是该结点的K[i];  tag=0, 表
    //示没有找到x, 这时可以插入的结点是r, 插入到该结点的K[i]与K[i+1]之间。
    Triple<int> result;							//存放结果的工作单元
    GetNode ( root );						//从磁盘上读取位于根root的结点
    Mnode <Type> *p = root, *q = NULL;			//p是扫描指针, q是p的父结点指针
    int i;
    while ( p != NULL ) {						//继续搜索
        i = 0;
        p->key[(p->n)+1] = MAXKEY;
        while ( p->key[i+1] < x ) i++;				//在结点内顺序搜索
        if ( p->key[i+1] == x ) {					//搜索成功, 本结点有x
            result.r = p;
            result.i = i+1;
            result.tag = 0;
            return result;					//返回结果, 返回主程序
        }
        q = p;
        p = p->ptr[i];					//本结点无x, q记下当前结点, p下降到相应子树			 GetNode (p);							//从磁盘上读取p结点
    }
    result.r = q;
    result.i = i;
    result.tag = 1;
    return result;							//x可能落入的区间[ Ki, Ki+1 )
}
template <class Type>
class Btree : public Mtree<Type> {		//B_树的类定义
    public:
    //Search从Mtree继承;
    //插入关键码x
    int Insert ( const Type & x ) {

        //将关键码x插入到一个驻留在磁盘上的m阶B-树中。
        if (!this->root) {
            this->root=new Mnode<Type>;
            insertkey ( this->root, 1, x, NULL );				//K, ap插入到j位置, 且p->n加1
            PutNode (this->root);
            return 1;				    //输出结点到磁盘, 返回调用程序
        }
        Triple<Type> loc = Search (x);				//在树中搜索x的插入位置
        if ( !loc.tag ) return 0;						//x已经存在于树中, 不再插入
        Mnode<Type> *p = loc.r,  *q;				//r是关键码x要插入的结点地址
        Mnode<Type> *ap = NULL,  *t;				//ap是插入关键码x的右邻指针
        Type K = x;
        int j = loc.i;					//(K,ap)形成插入二元组
        while (1) {
            if ( p->n < this->m-1) {						//结点中关键码个数未超出，还可容下新关键码
                insertkey ( p, j+1, K, ap );				//K, ap插入到j位置, 且p->n加1
                PutNode (p);
                return 1;				//输出结点到磁盘, 返回调用程序
            }
            int s = (this->m+1)/2;						//准备分裂结点, s是(m/2(位置
            insertkey ( p, j+1, K, ap );					//先插入, 结点中p->n达到m
            q = new Mnode<Type>;					//建立新结点q
            move ( p, q, s, this->m );						//将 p的key[s+1..m]和ptr[s..m]移到q的key[1..s-1]
            //和ptr[0..s-1],  p->n改为s-1, q->n改为s-1
            K = p->key[s];
            ap = q;		//(K,ap)形成向上插入二元组
            if ( p->parent != NULL ) {				//从下向上进行调整: 原来的p不为根
                t = p->parent;
                GetNode (t);				//从磁盘上读取p的双亲结点
                j = 0;
                t->key[(t->n)+1] = MAXKEY;			//在双亲结点内顺序搜索插入位置, 设监视哨
                while ( t->key[j+1] < K ) j++;			//搜索, 找到 >K的关键码停
                ap->parent = p->parent;				//新结点的双亲指针赋值
                PutNode (p);
                PutNode (ap);				//输出结点到磁盘
                p = t;							//p上升到父结点, 向上调整
            } else {								//原来的p是根, 则要产生新的根
                this->root = new Mnode<Type>;
                this->root->n = 1;
                this->root->parent = NULL;		//创建新根
                this->root->key[1] = K;
                this->root->ptr[0] = p;
                this->root->ptr[1] = ap;
                ap->parent = p->parent = this->root;
                PutNode (p);
                PutNode (ap);
                PutNode (this->root);	//输出结点到磁盘
                return 1;
            }
        }
    }
    void printTree() {
        //        BTreeNode<T> *p=root,*q;
        cout<<"BTree dump==========\n";

        printNode(this->root);
        cout<<"---------------------\n\n";
    }
    void printNode(Mnode<Type> *node) {
        if (node==NULL)
            return;
        node->printKey();
        for (int i=0;i<=node->n;i++) {
//            cout<<"child "<<i<<" ";
            printNode(node->ptr[i]);
        }


    }
    int Remove ( const Type & x ) {
        //从驻留磁盘上的m阶B-树上删除x。
        Triple<Type> loc = Search (x);			//在树中搜索x
        if ( loc.tag ) return 0;					//x不在B-树中, 返主
        Mnode<Type> *p = loc.r, *q, *s;			//p是关键码x所在结点
        int j = loc.i;						//j是关键码在结点中的位置
        if ( p->ptr[j] != NULL ) {				//若p非叶结点
            s = p->ptr[j];
            GetNode (s);
            q = p;		//读取磁盘上的s结点
            while ( s != NULL ) {
                q = s;     //找大于x但最接近于x的最小关键码(q是叶结点)
                s = s->ptr[0];
            }
            p->key[j] = q->key[1];				//用最小关键码填补
            compress ( q, 1 );					//在q结点中关键码与指针前移填补key[1], q->n减1
            p = q;						//下一步处理q结点中的删除
        } else compress ( p, j );					//p是叶结点, 关键码与指针前移填补key[j], p->n减1
        int d = (this->m+1)/2;						//结点容纳关键码的下限
        if (!(p==this->root))
            while (1) {
                if ( p->n < d-1 ) {					//需要调整
                    j = 0;
                    q = p->parent;			//在q中找指向p的指针
                    GetNode (q);
                    while ( j <= q->n && q->ptr[j] != p ) j++;
                    if ( !j ) LeftAdjust ( p, q, d, j );		//p是q的最左子女, 与其右兄弟与双亲结点做调整
                    else
                        if (j==q->n) RightAdjust ( p, q, d, j );			//p是q的最右子女, 与其左兄弟与双亲结点做调整
                        else													//p是中间,选择一个较简单的合并方法
                            if ( (q->ptr[j+1]->n) > d-1 ) LeftAdjust(p,q,d,j);
                            else
                                RightAdjust ( p, q, d, j );
                    p = q;						//继续向上做结点调整工作
                    if ( p == this->root ) break;
                } else break;						//不需要进行调整, 跳出循环
            }
        if ( !this->root->n ) {					//当根结点为空时删根结点
            p = this->root->ptr[0];
            delete this->root;
            this->root = p;
            if (this->root) this->root->parent = NULL;
        }
        return 1;
    }


    void LeftAdjust ( Mnode<Type> *p, Mnode<Type> *q, const int d, const int j ) {
        Mnode<Type> *p1 = q->ptr[j+1];			//p的右兄弟
        if ( p1->n > d-1 ) {					//右兄弟空间还够, 不用合并, 仅做调整
            ( p->n ) ++;
            p->key[p->n] = q->key[j+1];			//双亲结点相应关键码下移
            q->key[j+1] = p1->key[1];				//右兄弟最小关键码上移到双亲结点
            p->ptr[p->n] = p1->ptr[0];			//右兄弟最左指针左移
            compress ( p1, 0 );
        } else merge ( p, q, p1,j );				//p与p1合并, 保留p结点
    }
    void compress ( Mnode<Type> *p, const int j ) {
        for ( int i=j; i<p->n; i++ ) {			//左移
            p->key[i] = p->key[i+1];
            p->ptr[i] = p->ptr[i+1];
        }
        p->n --;							//结点中元素个数减1
    }


    void merge ( Mnode<Type> *p, Mnode<Type> *q, Mnode<Type> *p1, const int j) {
        p->key[(p->n)+1] = q->key[j+1];			//从双亲结点下降一个关键码
        p->ptr[(p->n)+1] = p1->ptr[0];			//从右兄弟结点左移一个指针
        for ( int k=1; k<=p1->n; k++ ) {				//从右兄弟结点
            p->key[(p->n)+k+1] = p1->key[k];		//关键码从p1到p左移
            p->ptr[(p->n)+k+1] = p1->ptr[k];		//指针从p1到p左移
        }
        compress ( q, j+1 );					//双亲结点q中值与指针左移
        p->n = p->n + p1->n + 1;
        delete p1;
    }


    void RightAdjust ( Mnode<Type> *p, Mnode<Type> *q, const int d, const int j )
//此程序与LeftAdjust功能基本相同，但与LeftAdjust是对称的：左右指针互换，前端与后端互换。
    {
        Mnode<Type> *p1 = q->ptr[j-1];			//p的左兄弟
        if ( p1->n > d-1 ) {					//左兄弟空间还够, 不用合并, 仅做调整
            ( p->n ) ++;
            for (int i=p->n; i>=1;i--) {
                p->key[i]=p->key[i-1];
                p->ptr[i]=p->ptr[i-1];
            }
            p->key[1]=q->key[j];
            q->key[j]=p1->key[p1->n];
            p->ptr[0]=p1->ptr[p1->n];
            (p1->n)--;
        } else merge ( p1, q, p,j-1 );				//p1与p合并, 保留p结点
    }
//    int Remove ( const Type& x );					//删除关键码x
//    private:
//    void LeftAdjust ( Mnode<Type> *p, Mnode<Type> *q, const int d, const int j );
//    void RightAdjust ( Mnode<Type> *p, Mnode<Type> *q, const int d, const int j );
//    void compress ( Mnode<Type> *p, const int j );
//    void merge ( Mnode<Type> *p, Mnode<Type> *q, Mnode<Type> *p1, int j) ;

};
template <class Type>
void GetNode(Mnode<Type> *p) {
    //理论上应该从磁盘读入结点p,现在这里可以为空
};
template <class Type>
void PutNode(Mnode<Type> *p) {
    //理论上应该从磁盘写结点p,现在这里可以为空
};

template <class Type>
void Mtree<Type>::print(ostream& strm,Mnode<Type> *p) {
    if (p) {
        strm<<*p;
        if (p->n) {
            strm<<"(";
            for (int i=0;i<=p->n;i++)
                print(strm,p->ptr[i]);

            strm<<")";
        }
    }
}
template <class Type>
ostream& operator <<(ostream& strm, Mtree<Type>& mt) {
    mt.print(strm,mt.root);
    return strm;
}
template <class Type>
ostream& operator <<(ostream& strm, Mnode<Type>& mn) {
    strm<<"[";
    for (int i=1;i<=mn.n;i++) {
        if (i!=1) strm<<',';
        strm<<mn.key[i];
    }
    strm<<"]";
    return strm;

}
template <class Type>
ostream& operator <<(ostream& strm, Triple<Type>& tp) {
    if (tp.tag) strm<<"Key Not Found";
    else
        strm<<"Key Found at :"<<*tp.r<<" No. "<<tp.i<<" key";
    return strm;
}
template <class Type>
void Mnode<Type>::
insertkey(int i,Type K,Mnode<Type>* q) {
    for (int j=n;j>=i;j--) {
        key[j+1]=key[j];
        ptr[j+1]=ptr[j];
    }
    key[i]=K;
    ptr[i]=q;
    n++;
};
template <class Type>
void Mtree<Type>::
insertkey(Mnode<Type>* p,int i,Type K,Mnode<Type>* q) {
    p->insertkey(i,K,q);
}
template <class Type>
void Mtree<Type>::move ( Mnode<Type>*p, Mnode<Type>*q, int s, int m )
//将 p的key[s+1..m]和ptr[s..m]移到q的key[1..s-1]和ptr[0..s-1]
//p->n改为s, q->n改为m-s
{
    int j=0;
    q->ptr[0]=p->ptr[s];
    if (q->ptr[0])
        q->ptr[0]->parent=q;
    for (int i=s+1;i<=m;i++) {
        j++;
        q->key[j]=p->key[i];
        q->ptr[j]=p->ptr[i];
        if (q->ptr[j])
            q->ptr[j]->parent=q;
    }
    q->n=m-s;
    p->n=s-1;
}
//
