#include <iostream.h>
#include "String.h"
#include"BinaryTree.h"
#define MaxNum 100
int result[MaxNum];
void combin(int n, int k) {
    for (int i=n;i>=k;i--) {
        result[k]=i;
        if (k>1) {
            combin(i-1, k-1);
        } else {
            for (int j=result[0];j>0;j--)
                cout<<result[j];
            cout<<endl;
        }
    }
}
//合并有序表
void unionList(int a[],int aLen,int b[],int bLen) {
    int* c=new int[aLen+bLen];
    int i=0,j=0;
    int p=0;
    for (;i<aLen,j<bLen;) {
        if (a[i]<b[j]) {
            c[p++]=a[i];
            i++;
        } else {
            c[p++]=b[j];
            j++;
        }
    }
    if (i!=aLen) {
        for (;i<aLen;i++) {
            c[p++]=a[i];
        }
    }
    if (j!=bLen) {
        for (;j<bLen;j++) {
            c[p++]=b[j];
        }
    }
	//打印
	for(i=0;i<aLen+bLen;i++){
		cout<<c[i]<<endl;
	}
}


//计算最大公约数
int GCD(int divident,int divisor){
	int t;
	if(divident<divisor){
		t=divident;
		divident=divisor;
		divisor=t;
	}
	int remainder=divident%divisor;
	while(remainder!=0){
		divident=divisor;
		divisor=remainder;
		remainder=divident%divisor;

	}
	return divisor;
}
//循环移位,时间复杂度O(n),空间复杂度O(1)
void moveCycle(int x[],int n,int p){
	int i,j,m=GCD(n,p);
	for(int k=0;k<m;k++){
		int t=x[k];
		i=k;
		j=(n+i-p)%n;
		while(j!=k){
			x[i]=x[j];
			i=j;
			j=(n+j-p)%n;
		}
		x[i]=t;

	}
}

//对于不是互质的n和p的循环移位 XXXX错误
void moveCycle2(int x[],int n,int p){
	int count=0;
		int i=0,j=0,t=0;
		while(count<n){
			j=(i+p)%n;
			t=x[j];
			x[j]=x[i];
			i=j;
			count++;

		}
}

int main() {
    //char* treeStr="dbeac^f";
    //char* treeStr="dbegc^f";
    //char* treeStr="gfic^hj^e^^^^^^d";
char* treeStr="ABCDEFGH";
    BinaryTree tree;
    tree.root=tree.layerCreate(treeStr);
    tree.printTree(tree.root);	

	int x[]={1,2,3,4,5,6};
	int p=1;
	int n=6;
 moveCycle(x,n,p);
//moveCycle2(x,n,p);
 for(int i=0;i<n;i++){
	 cout<<x[i]<<endl;
 }


/*tree.deleteAllLeaf(tree.root);
tree.printTree(tree.root);	

	cout<<tree.totalNum()<<endl;
	*/
	//cout<<tree.width(tree.root)<<endl;
	//cout<<tree.sum2(tree.root)<<endl;
	//cout<<tree.sum1(tree.root)<<endl;
//tree.preOrderStack2(tree.root);

	//tree.preOrder1(tree.root);
	//tree.preOrder2(tree.root);
//tree.doubleOrder(tree.root);

//tree.swapChild(tree.root);
//tree.printTreeDrive(tree.root);


    //cout<<tree.sum0(tree.root)<<endl;
	
	
	/*tree.middleOrderStack(tree.root);	
	tree.preOrderStack(tree.root);
    tree.postOrderStack(tree.root);
    tree.levelOrderQueue(tree.root);
	*/
    




	/*int a[]={2,4,7,11,15,20};
	int b[]={1,3,5,8};
	unionList(a,6,b,4);
*/






    /* BinaryTree tree;
     tree.root=tree.layerCreate(treeStr);
     tree.printTreeDrive(tree.root);
     /*if (tree.isBinSortTree(tree.root)==true) {
         cout<<"是二叉排序树\n";
     } else {
         cout<<"不是二叉排序树\n";
     }*/
    /*int bs=0;
    tree.isBinSortTree2(tree.root,bs);
    if (bs) {
        cout<<"是二叉排序树\n";
    } else {
        cout<<"不是二叉排序树\n";
    }*/


    //BinTreeNode node('a');
    /*char* treeStr="abcdefg";
    BinaryTree tree;
    tree.root=tree.layerCreate(treeStr);
    tree.printTreeDrive(tree.root);
    BinTreeNode *toFind=tree.root->leftChild->leftChild;
    int level=tree.searchNode(tree.root,toFind);
    //int level=tree.searchNode(tree.root,tree.root->rightChild);
    cout<<"节点"<<toFind->data<<"在第"<<level<<"层"<<endl;
    */


    /*char* treeStr="abcdef";
    BinaryTree tree;
        tree.root=tree.layerCreate(treeStr);
        tree.printTreeDrive(tree.root);
        tree.Traverse(tree.root);*/


    /*   char* treeStr="abcdefg";
       BinaryTree tree;
       tree.root=tree.layerCreate(treeStr);
       tree.printTreeDrive(tree.root);
       int level=tree.level(tree.root,tree.root->leftChild);
       cout<<level<<endl;

    */


//    result[0]=3;
//    combin(5, 3);


//    char* str="abcabaa";
//    String S("abcaabbabcabaacbacba", 20);
//    String pat(str, 7);
//    int index=S.FastFind(pat);
//    if(index==-1){
//        cout<<"没有找到!"<<endl;
//    }else{
//        cout<<index<<endl;
//    }
    return 0;
}
