#include "BinTreeNode.h"
#include <stack>
#include <queue>
using namespace std;
class BinaryTree {
public :
    BinTreeNode* root;

//删除所有叶结点,注意参数类型为引用的指针！！
	void deleteAllLeaf(BinTreeNode *&p){
		if(p==NULL)
			return;
		if(p->leftChild==NULL&&p->rightChild==NULL){
			cout<<p->data<<endl;
			delete p;
			p=NULL;
		}else{
			deleteAllLeaf(p->leftChild);
			deleteAllLeaf(p->rightChild);
		}
	}

//计算总结点数
	int totalNum(){
		BinTreeNode *p=root;
		int sum=0;
		if(p==NULL)
			return 0;
		stack<BinTreeNode*> s;		
		s.push(p);
		while(!s.empty()){
			p=s.top();
			s.pop();
			sum++;
			if(p->rightChild!=NULL) s.push(p->rightChild);
			if(p->leftChild!=NULL) s.push(p->leftChild);
		}
		return sum;
	}

	//计算树的宽度，即在二叉树的各层上，具有节点数最多的那一层上的节点数
	void levelNum(BinTreeNode *p,int* a,int h){
		if(p!=NULL){
			a[h]++;
			levelNum(p->leftChild,a,h+1);
			levelNum(p->rightChild,a,h+1);
		}

	}
	int width(BinTreeNode *p){		
		int n=totalNum();
		//多出一个元素，让数组从1开始
		int* a=new int[n+1];
		int h=0,i,wid;
		for(i=0;i<=n;i++) a[i]=0;
		levelNum(p,a,h);
		wid=a[0];
		for(i=1;i<=n;i++)
			if(a[i]>wid) wid=a[i];
			return wid;
	}
    
    //递归计算二叉树高度
    int height(BinTreeNode* t) {
        if (t==NULL)
            return 0;
        int hl=height(t->leftChild);
        int hr=height(t->rightChild);
        return 1+(hl>hr?hl:hr);
		
    }
	//递归计算节点的平衡因子，并返回非叶子节点的数目
    int calcBF(BinTreeNode* t) {
        if (t==NULL)
            return 0;
        cout<<"节点"<<t->data<<"的平衡因子是"<<height(t->leftChild)-height(t->rightChild)<<endl;
		
        if (t->leftChild==NULL&&t->rightChild==NULL) {
            return 0;
        }
        return 1+calcBF(t->leftChild)+calcBF(t->rightChild);
    }
	
    //判断二叉排序树法一
    bool isBinSortTree(BinTreeNode* t) {
        //cout<<"test\n";
        if (t==NULL)
            return true;
        if (t->leftChild!=NULL) {
            if (t->data<t->leftChild->data||!isBinSortTree(t->leftChild))
                return false;
        }
        if (t->rightChild!=NULL) {
            if (t->data>t->rightChild->data||!isBinSortTree(t->rightChild))
                return false;
        }
        return true;
    }
    //判断二叉排序树法二
    void isBinSortTree2(BinTreeNode* t,int &bs) {
        if (t==NULL)
            return;
        if ((t->leftChild==NULL||t->data>t->leftChild->data)&&(t->rightChild==NULL||t->data<t->rightChild->data)) {
            bs=1;
            isBinSortTree2(t->leftChild,bs);
            if (bs)
                isBinSortTree2(t->rightChild,bs);
			
        } else
            bs=0;
    }
	
	
    //非递归中序遍历
    void middleOrderStack(BinTreeNode* p) {
        cout<<"中序遍历\n";
        //	BinTreeNode* p=b;
        stack<BinTreeNode*> s;
        while (p!=NULL||!s.empty()) {
            while (p!=NULL) {
                s.push(p);
                p=p->leftChild;
				
            }
            if (!s.empty()) {
                p=s.top();//stack类中pop无返回值
                s.pop();
                cout<<p->data<<" ";
                p=p->rightChild;
				
            }
        }
        cout<<endl;
    }
    //非递归后续遍历
    void postOrderStack(BinTreeNode* p) {
        cout<<"后序遍历\n";
        stack<BinTreeNode*> s;
        do {
            while (p!=NULL) {
                p->tag=false;//表示未访问左子树
                s.push(p);
                p=p->leftChild;
            }
            //访问完左子树才能访问自身，对于叶节点同样的道理，只不过其子女均为空
            while (!s.empty()&&s.top()->tag) {
                p=s.top();
                s.pop();
                cout<<p->data<<" ";
				
            }
            if (!s.empty()) {
                p=s.top();
                p->tag=true;
                p=p->rightChild;
				
            }
        } while (!s.empty());
        cout<<endl;
    }
	//非递归前序遍历法一，较繁琐
    void preOrderStack(BinTreeNode *p) {
        cout<<"前序遍历\n";
        stack<BinTreeNode*> s;
        while (p!=NULL||!s.empty()) {
            while (p!=NULL) {
                s.push(p);
                cout<<p->data<<" ";
                p=p->leftChild;
				
            }
            if (!s.empty()) {
                p=s.top();//stack类中pop无返回值
                s.pop();
				
                p=p->rightChild;
				
            }
        }
        cout<<endl;
    }
	
	
    //非递归层次遍历
    void levelOrderQueue(BinTreeNode *p) {
        cout<<"层序遍历\n";
        queue<BinTreeNode*> q;
        q.push(p);
		
        while (!q.empty()) {
            p=q.front();
            q.pop();
            cout<<p->data<<" ";
            if (p->leftChild!=NULL) {
                q.push(p->leftChild);
            }
            if (p->rightChild!=NULL) {
                q.push(p->rightChild);
            }
        }
        cout<<endl;
    }
	
	
	
	//第二种方法查找指定节点所在层次
	bool level2(BinTreeNode* root,BinTreeNode* p,int& n) {
		n++;
		//cout<<n<<endl;
		if (root==NULL||p==NULL) {
			n--;
			//没有找到
			return false;
		}
		if (root==p) {        
			return true;
		}
		//在左子树找到则不找右子树
		if (level2(root->leftChild,p,n))
			return true;
		if( level2(root->rightChild,p,n))
			return true;
		//左右子树都没找到
		n--;
		return false;
	}
	int searchNode(BinTreeNode* root,BinTreeNode* p) {
		int n=0;
		level2(root,p,n);
		return n;
	}
	
	
	
	//按层次创建
	BinTreeNode* layerCreate(char *str, int i=1) {
		BinTreeNode *p=NULL;
		int len=strlen(str);
		//static int len=strlen(str);
		if (i-1<len) {
			//没有树的情况用"^"表示
			if (str[i-1]=='^') {
				return NULL;
			} else {
				p=new BinTreeNode(str[i-1]);
				p->leftChild=layerCreate(str, 2*i);
				p->rightChild=layerCreate(str, 2*i+1);
			}
		}
		return p;
	}
	
	
	//打印树
	void printTree(BinTreeNode *p,int n=0) {
		if (p==NULL) return;
		printTree(p->leftChild,n+1);
		for (int i=0;i<n;++i)
			cout<<"   ";
		cout<<p->data<<endl;
		printTree(p->rightChild,n+1);
	}
	
	
	//获得指定节点所在的层次
	int  level(BinTreeNode* root,BinTreeNode *p) {
		if (root==NULL)
			return 0;
		//是根节点，则是第一层
		if (root==p)
			return 1;
		int left=level(root->leftChild,p);
		int right=level(root->rightChild,p);
		if (left)
			return 1+left;
		if (right)
			return 1+right;
		return 0;
	}
	
	
	
	//统计二叉树叶结点个数,即度为0的结点个数
	int sum0(BinTreeNode *p){
		if(p==NULL)
			return 0;
		if(p->leftChild==NULL&&p->rightChild==NULL) return 1;
		return sum0(p->leftChild)+sum0(p->rightChild);
	}
//统计二叉树度为1的结点个数
	int sum1(BinTreeNode *p){
		if(p==NULL)
			return 0;
		if((p->leftChild!=NULL&&p->rightChild==NULL)||(p->leftChild==NULL&&p->rightChild!=NULL)) 
			return 1+sum1(p->leftChild)+sum1(p->rightChild);
		return sum1(p->leftChild)+sum1(p->rightChild);
	}

	//统计二叉树度为2的结点个数
	int sum2(BinTreeNode *p){
		if(p==NULL)
			return 0;
		if(p->leftChild!=NULL&&p->rightChild!=NULL) 
			return 1+sum2(p->leftChild)+sum2(p->rightChild);
		return sum2(p->leftChild)+sum2(p->rightChild);
	}

	

	//交换每个结点的左右子树
	void swapChild(BinTreeNode *p){
		BinTreeNode *t;
		if(p==NULL) return;
		if(p->leftChild!=NULL||p->rightChild!=NULL){
			t=p->leftChild;
			p->leftChild=p->rightChild;
			p->rightChild=t;
			swapChild(p->leftChild);
			swapChild(p->rightChild);
		}
	}
	
	
	
	
	//双序遍历!!叶子结点被连续访问两次
	void doubleOrder(BinTreeNode *p){
		if(p!=NULL){
			cout<<p->data<<endl;
			doubleOrder(p->leftChild);
			cout<<p->data<<endl;
			doubleOrder(p->rightChild);
		}
	}
	
	
	//纯递归前序遍历
	void preOrder2(BinTreeNode *p){
		if(p!=NULL){
			cout<<p->data<<endl;
			preOrder2(p->leftChild);
			preOrder2(p->rightChild);
		}
	}
	
	//消去第二个递归的前序遍历
	void preOrder1(BinTreeNode *p){
		while(p!=NULL){
			cout<<p->data<<endl;
			preOrder1(p->leftChild);
			p=p->rightChild;
		}
	}
	//第二种方法实现非递归的前序遍历
	void preOrderStack2(BinTreeNode *p){
		//BinTreeNode* t;
		if(p==NULL)
			return;
		stack<BinTreeNode*> s;		
		s.push(p);
		while(!s.empty()){
			p=s.top();
			s.pop();
			cout<<p->data<<endl;
			if(p->rightChild!=NULL) s.push(p->rightChild);
			if(p->leftChild!=NULL) s.push(p->leftChild);
		}
	}
	
	
	
	};