//
// File:   BinTreeNode.h
// Author: pisces
//
// Created on 2007��12��17��, ����8:57
//

#ifndef _BINTREENODE_H
#define	_BINTREENODE_H



#endif	/* _BINTREENODE_H */
class BinTreeNode{
    friend class BinaryTree;
public :
    BinTreeNode* leftChild;
    char data;
    BinTreeNode* rightChild;
//    BinTreeNode(){};
    BinTreeNode();
    BinTreeNode(char ch){
        data=ch;
        leftChild=NULL;
        rightChild=NULL;
    }
};
