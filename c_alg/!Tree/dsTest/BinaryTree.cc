#include"BinaryTree.h"
#include"BinTreeNode.h"
#include<string.h>
BinTreeNode* BinaryTree::layerCreate(char *str, int i) {
    BinTreeNode *p=NULL;
    static int len=strlen(str);
    if(i-1<len) {
        p=new BinTreeNode(str[i-1]);
        p->leftChild=layerCreate(str, 2*i);
        p->rightChild=layerCreate(str, 2*i+1);
    }
    return p;
}