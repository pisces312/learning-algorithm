#
# Gererated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc.exe
CCC=g++.exe
CXX=g++.exe
FC=

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=build/Debug/Cygwin-Windows

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/F_/My_Programs/seuc++/dsTest/BinaryTree.o \
	${OBJECTDIR}/_ext/F_/My_Programs/seuc++/dsTest/dsTest.o \
	${OBJECTDIR}/_ext/F_/My_Programs/seuc++/dsTest/String.o

# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS} dist/Debug/Cygwin-Windows/dstest.exe

dist/Debug/Cygwin-Windows/dstest.exe: ${OBJECTFILES}
	${MKDIR} -p dist/Debug/Cygwin-Windows
	${LINK.cc} -o dist/Debug/Cygwin-Windows/dstest ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/_ext/F_/My_Programs/seuc++/dsTest/BinaryTree.o: /cygdrive/F/My\ Programs/seuc++/dsTest/BinaryTree.cc 
	${MKDIR} -p ${OBJECTDIR}/_ext/F_/My_Programs/seuc++/dsTest
	$(COMPILE.cc) -g -o ${OBJECTDIR}/_ext/F_/My_Programs/seuc++/dsTest/BinaryTree.o F\:/My\ Programs/seuc++/dsTest/BinaryTree.cc

${OBJECTDIR}/_ext/F_/My_Programs/seuc++/dsTest/dsTest.o: /cygdrive/F/My\ Programs/seuc++/dsTest/dsTest.cc 
	${MKDIR} -p ${OBJECTDIR}/_ext/F_/My_Programs/seuc++/dsTest
	$(COMPILE.cc) -g -o ${OBJECTDIR}/_ext/F_/My_Programs/seuc++/dsTest/dsTest.o F\:/My\ Programs/seuc++/dsTest/dsTest.cc

${OBJECTDIR}/_ext/F_/My_Programs/seuc++/dsTest/String.o: /cygdrive/F/My\ Programs/seuc++/dsTest/String.cc 
	${MKDIR} -p ${OBJECTDIR}/_ext/F_/My_Programs/seuc++/dsTest
	$(COMPILE.cc) -g -o ${OBJECTDIR}/_ext/F_/My_Programs/seuc++/dsTest/String.o F\:/My\ Programs/seuc++/dsTest/String.cc

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf:
	${RM} -r build/Debug
	${RM} dist/Debug/Cygwin-Windows/dstest.exe

# Subprojects
.clean-subprojects:
