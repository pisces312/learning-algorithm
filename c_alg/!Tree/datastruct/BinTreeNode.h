#include<iostream.h>
class BinTreeNode{
public :
    BinTreeNode* leftChild;
    char data;
    BinTreeNode* rightChild;
	bool tag;
//    BinTreeNode(){};
    BinTreeNode();
    BinTreeNode(char ch){
        data=ch;
        leftChild=NULL;
        rightChild=NULL;
		tag=false;
    }
};
