#include "BinTreeNode.h"
#include <stack>
#include <queue>
using namespace std;
class BinaryTree {
public :
    BinTreeNode* root;
//    BinaryTree(){};
    

    //递归计算节点的平衡因子，并返回非叶子节点的数目
    //递归计算高度
    int height(BinTreeNode* t) {
        if (t==NULL)
            return 0;
        int hl=height(t->leftChild);
        int hr=height(t->rightChild);
        return 1+(hl>hr?hl:hr);

    }
    int Traverse(BinTreeNode* t) {
        if (t==NULL)
            return 0;
        cout<<"节点"<<t->data<<"的平衡因子是"<<height(t->leftChild)-height(t->rightChild)<<endl;

        if (t->leftChild==NULL&&t->rightChild==NULL) {
            return 0;
        }
        return 1+Traverse(t->leftChild)+Traverse(t->rightChild);
    }

    //判断二叉排序树
    bool isBinSortTree(BinTreeNode* t) {
        //cout<<"test\n";
        if (t==NULL)
            return true;
        if (t->leftChild!=NULL) {
            if (t->data<t->leftChild->data||!isBinSortTree(t->leftChild))
                return false;
        }
        if (t->rightChild!=NULL) {
            if (t->data>t->rightChild->data||!isBinSortTree(t->rightChild))
                return false;
        }
        return true;
    }
    //法二
    void isBinSortTree2(BinTreeNode* t,int &bs) {
        if (t==NULL)
            return;
        if ((t->leftChild==NULL||t->data>t->leftChild->data)&&(t->rightChild==NULL||t->data<t->rightChild->data)) {
            bs=1;
            isBinSortTree2(t->leftChild,bs);
            if (bs)
                isBinSortTree2(t->rightChild,bs);

        } else
            bs=0;
    }


    //非递归中序遍历
    void middleOrderStack(BinTreeNode* p) {
        cout<<"中序遍历\n";
        //	BinTreeNode* p=b;
        stack<BinTreeNode*> s;
        while (p!=NULL||!s.empty()) {
            while (p!=NULL) {
                s.push(p);
                p=p->leftChild;

            }
            if (!s.empty()) {
                p=s.top();//stack类中pop无返回值
                s.pop();
                cout<<p->data<<" ";
                p=p->rightChild;

            }
        }
        cout<<endl;
    }
    //非递归后续遍历
    void postOrderStack(BinTreeNode* p) {
        cout<<"后序遍历\n";
        stack<BinTreeNode*> s;
        do {
            while (p!=NULL) {
                p->tag=false;//表示未访问左子树
                s.push(p);
                p=p->leftChild;
            }
            //访问完左子树才能访问自身，对于叶节点同样的道理，只不过其子女均为空
            while (!s.empty()&&s.top()->tag) {
                p=s.top();
                s.pop();
                cout<<p->data<<" ";

            }
            if (!s.empty()) {
                p=s.top();
                p->tag=true;
                p=p->rightChild;

            }
        } while (!s.empty());
        cout<<endl;
    }
//非递归前序遍历法一，较繁琐
    void preOrderStack(BinTreeNode *p) {
        cout<<"前序遍历\n";
        stack<BinTreeNode*> s;
        while (p!=NULL||!s.empty()) {
            while (p!=NULL) {
                s.push(p);
                cout<<p->data<<" ";
                p=p->leftChild;

            }
            if (!s.empty()) {
                p=s.top();//stack类中pop无返回值
                s.pop();

                p=p->rightChild;

            }
        }
        cout<<endl;
    }


    //非递归层次遍历
    void levelOrderQueue(BinTreeNode *p) {
        cout<<"层序遍历\n";
        queue<BinTreeNode*> q;
        q.push(p);

        while (!q.empty()) {
            p=q.front();
            q.pop();
            cout<<p->data<<" ";
            if (p->leftChild!=NULL) {
                q.push(p->leftChild);
            }
            if (p->rightChild!=NULL) {
                q.push(p->rightChild);
            }
        }
        cout<<endl;
    }



//第二种方法查找指定节点所在层次
	bool level2(BinTreeNode* root,BinTreeNode* p,int& n) {
    n++;
    //cout<<n<<endl;
    if (root==NULL||p==NULL) {
        n--;
        //没有找到
        return false;
    }
    if (root==p) {        
        return true;
    }
    //在左子树找到则不找右子树
    if (level2(root->leftChild,p,n))
        return true;
    if( level2(root->rightChild,p,n))
		return true;
	//左右子树都没找到
	n--;
	return false;
}
int searchNode(BinTreeNode* root,BinTreeNode* p) {
    int n=0;
    level2(root,p,n);
    return n;
}



//按层次创建
BinTreeNode* layerCreate(char *str, int i=1) {
    BinTreeNode *p=NULL;
    static int len=strlen(str);
    if (i-1<len) {
        //没有树的情况用"^"表示
        if (str[i-1]=='^') {
            return NULL;
        } else {
            p=new BinTreeNode(str[i-1]);
            p->leftChild=layerCreate(str, 2*i);
            p->rightChild=layerCreate(str, 2*i+1);
        }
    }
    return p;
}


 //打印树递归主体
void printTree(BinTreeNode *p,int n) {
    if (p==NULL) return;
    printTree(p->leftChild,n+1);
    for (int i=0;i<n;++i)
        cout<<"   ";
    cout<<p->data<<endl;
    printTree(p->rightChild,n+1);
}
//打印树驱动器
void printTreeDrive(BinTreeNode *p) {
    printTree(p,0);
}

//获得指定节点所在的层次
int  level(BinTreeNode* root,BinTreeNode *p) {
    if (root==NULL)
        return 0;
    //是根节点，则是第一层
    if (root==p)
        return 1;
    int left=level(root->leftChild,p);
    int right=level(root->rightChild,p);
    if (left)
        return 1+left;
    if (right)
        return 1+right;
    return 0;
}



//统计二叉树叶结点个数,即度为0的结点个数
int sum0(BinTreeNode *p){
if(p==NULL)
return 0;
else if(p->leftChild==NULL&&p->rightChild==NULL) return 1;
return sum0(p->leftChild)+sum0(p->rightChild);
}


};