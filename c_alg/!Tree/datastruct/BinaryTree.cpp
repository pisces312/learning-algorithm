#include"BinaryTree.h"
#include<string.h>
bool BinaryTree::level2(BinTreeNode* root,BinTreeNode* p,int& n) {
    n++;
    //cout<<n<<endl;
    if (root==NULL||p==NULL) {
        n--;
        //没有找到
        return false;
    }
    if (root==p) {        
        return true;
    }
    //在左子树找到则不找右子树
    if (level2(root->leftChild,p,n))
        return true;
    if( level2(root->rightChild,p,n))
		return true;
	//左右子树都没找到
	n--;
	return false;
}
int BinaryTree::searchNode(BinTreeNode* root,BinTreeNode* p) {
    int n=0;
    level2(root,p,n);
    return n;
}
BinTreeNode* BinaryTree::layerCreate(char *str, int i) {
    BinTreeNode *p=NULL;
    static int len=strlen(str);
    if (i-1<len) {
        //没有树的情况用"^"表示
        if (str[i-1]=='^') {
            return NULL;
        } else {
            p=new BinTreeNode(str[i-1]);
            p->leftChild=layerCreate(str, 2*i);
            p->rightChild=layerCreate(str, 2*i+1);
        }
    }
    return p;
}
void BinaryTree::printTree(BinTreeNode *p,int n) {
    if (p==NULL) return;
    printTree(p->leftChild,n+1);
    for (int i=0;i<n;++i)
        cout<<"   ";
    cout<<p->data<<endl;
    printTree(p->rightChild,n+1);
}
void BinaryTree::printTreeDrive(BinTreeNode *p) {
    printTree(p,0);
}
int  BinaryTree::level(BinTreeNode* root,BinTreeNode *p) {
    if (root==NULL)
        return 0;
    //是根节点，则是第一层
    if (root==p)
        return 1;
    int left=level(root->leftChild,p);
    int right=level(root->rightChild,p);
    if (left)
        return 1+left;
    if (right)
        return 1+right;
    return 0;
}