#include"ThreadTreeNode.h"
#include"LinkStack.h"
#include"string.h"
class TBTree
{
public:
	TBTNode *root,*head;
	bool isThread;
	void threadTree();
	TBTNode *addthread(TBTNode *bt,TBTNode *pre);
	TBTree(dataType *str);
	TBTNode *preNext(TBTNode *p);
	void preOrder();
	void postOrder();
	TBTNode* preCreate(dataType *str);
	void preInitiate(dataType *str);
	void preOrderStack();
};
void TBTree::preOrderStack()
{	
	LinkStack<TBTNode*> s;
	TBTNode *p=root;
	if(p==NULL) return;
	cout<<"非递归先序遍历二叉树: ";
	while(!(p==NULL&&s.isEmpty()))
	{
		while(p!=NULL)
		{
			cout<<p->data<<" ";
			s.push(p);
			p=p->left;
		}
		p=s.pop();
		p=p->right;		
	}
	cout<<endl;
}
TBTNode* TBTree::preNext(TBTNode *p)
{
	if(!p->ltag)
		p=p->left;
	else
	{
		cout<<p->data<<" ";
		if(!p->rtag)
		{
			p=p->right;
			cout<<p->data<<" ";
		}
		else
		{
			while(p->rtag&&p->right!=NULL)
				p=p->right;
			
			p=p->right;
		}
	}
	return p;
}
void TBTree::preOrder()
{
	TBTNode *p=root;
	if(p!=NULL)
	{
		cout<<"先序遍历中序线索二叉树: ";
		do
		{
			cout<<p->data<<" ";
			p=preNext(p);
		}while(p!=NULL);
		cout<<endl;
	}
}
void TBTree::preInitiate(dataType *str)
{
	if(root!=NULL) 
		root=NULL;	
	if(str!="")
		root=preCreate(str);
	isThread=false;	
}
TBTree::TBTree(dataType *str)
{	
	head=new TBTNode;
	root=head->left;
	preInitiate(str);
	isThread=false;
}
TBTNode* TBTree::preCreate(dataType *str)
{
	TBTNode *p=NULL;
	static int i=0;						
	static int l=strlen(str);
	if(i>=l) i=0;					//防止第二次调用时i不用0开始增加
	if(str[i]!='.')
	{
		p=new TBTNode(str[i++]);
		p->left=preCreate(str);
		p->right=preCreate(str);
	}
	else i++;
	return p;
}
void TBTree::threadTree()
{
	TBTNode *pre;
	//head->right=head;
	//head->rtag=true;
	if(head->left==NULL)
	{
		head->left=head;
		head->ltag=true;
	}
	else
	{
		pre=head;
		pre=addthread(root,pre);
		pre->right=head;
		pre->rtag=true;
		head->right=pre;
		head->rtag=true;
	}	
}
TBTNode* TBTree::addthread(TBTNode *bt,TBTNode *pre)
{
	LinkStack<TBTNode*> s;
	TBTNode *p=bt;
	while(!(p==NULL&&s.isEmpty()))
	{
		while(p!=NULL)
		{
			s.push(p);
			p=p->left;
			if(s.isEmpty()) return pre;
			else p=s.pop();
			if(p->left==NULL)
			{
				p->ltag=true;
				p->left=pre;
			}
			else p->ltag=false;
			cout<<pre->data<<" ";
			if(pre->right==NULL)
			{
				pre->rtag=true;
				pre->right=p;
				cout<<p->data<<" ";
			}
			else pre->rtag=false;
			pre=p;
			p=p->right;
		}
	}
	return pre;
}