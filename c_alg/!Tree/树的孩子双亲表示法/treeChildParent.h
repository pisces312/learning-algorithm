#include"treeChildParentNode.h"
#include"iomanip.h"
template<class T>
class cpTree
{
public:
	cpTreeNode<T>* table;
	int end,size;
	cpTree(int n=1);
	cpTree(T n);
	cpTree(cpTree &t);
	void setRoot(T n);
	bool isFull(){return end+1==size;}
	int getSize(){return size;}
	int indexParent(T data);
	int attachChild(T root,T data);
	void copyTree(cpTree<T> &t1,cpTree<T> &t2);
	void copyNode(cpTreeNode<T> &p1,cpTreeNode<T> &p2);
	void printTree();
};		
template<class T>
cpTree<T>::cpTree(cpTree<T> &t)
{
	int l=t.getSize();
	table=new cpTreeNode<T>[l];
	for(int i=0;i<l;i++)
		copyNode(table[i],t.table[i]);
	end=t.end;
	size=t.size;
}
template<class T>
void cpTree<T>::copyTree(cpTree<T> &t1,cpTree<T> &t2)
{
	int l=getSize();
	for(int i=0;i<l;i++)
		copyNode(t1.table[i],t2.table[i]);
	t1.end=t2.end;
	t1.size=t2.size;
}
template<class T>
int cpTree<T>::indexParent(T data)
{
	int l=getSize();
	for(int i=0;i<l;i++)
		if(table[i].data==data) return i;
	return -1;
}

template<class T>
int cpTree<T>::attachChild(T root,T data)
{
	
	if(isFull())
	{
		int l=getSize();
		cpTree<T> t(*this);				
		table=new cpTreeNode<T>[l+1];
		for(int i=0;i<l;i++)
			copyNode(table[i],t.table[i]);
		end=t.end;
		size=t.size+1;
	}
	int parent=indexParent(root);
	table[end+1].data=data;	
	table[end+1].parent=parent;
	table[parent].link.insertEnd(data);	
	return ++end;
	
}
template<class T>
void cpTree<T>::copyNode(cpTreeNode<T> &p1,cpTreeNode<T> &p2)
{	
	p1.data=p2.data;
	p1.parent=p2.parent;
	for(int i=1;i<=p2.link.length();i++)
		p1.link.set(i,p2.link.get(i));
}
template<class T>
cpTree<T>::cpTree(T n)
{
	table=new cpTreeNode<T>[1];
	table[0].data=n;
	end=0;
	size=1;
}

template<class T>
cpTree<T>::cpTree(int n)
{
	if(n==0)
		table=NULL;
	else
	{
		table=new cpTreeNode<T>[n];
	}
	end=-1;
	size=n;
}
template<class T>
void cpTree<T>::setRoot(T n)
{
	if(table==NULL)
	{
		table=new cpTreeNode<T>[1];
		end=0;
		size=1;
	}
	else if(end<0)
	{
		end=0;
		size=1;
	}
	table[0].data=n;
}
template<class T>
void cpTree<T>::printTree()
{
	cout<<"  NUM  DATA PARENT  CHILD\n";

	for(int i=0;i<end+1;i++)
	{	
		cout<<setw(4)<<i<<setw(6)<<table[i].data<<setw(6)<<table[i].parent;
		table[i].link.output();
	}
		
}