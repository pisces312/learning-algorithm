#include<iostream.h>
#include"SLink.h"
template<class T>
class cpTreeNode
{
public:
	T data;
	int parent;
	SLink<char> link;	
	cpTreeNode();
	cpTreeNode(T n);
};
template<class T>	
cpTreeNode<T>::cpTreeNode()
{
	link.head=new SLNode<T>('\0');
	parent=-1;
}
template<class T>
cpTreeNode<T>::cpTreeNode(T n)
{
	data=n;
	parent=-1;
	link.head=new SLNode<T>('\0');
}
