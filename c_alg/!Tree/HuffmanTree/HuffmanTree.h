#include"HuffmanTreeNode.h"
#define maxvalue 32768;
class hTree
{
public:
	hTNode* table;	
	int nodenum;
	hTree(int n=0);
	void create();
	void encode();
	void output();
	hTree(int *a,int size);
};

hTree::hTree(int n)
{
	if(n==0) table=NULL;
	else
	{
		nodenum=n;
		table=new hTNode[2*n-1];	
		for(int i=0;i<n;i++)
		{
			cout<<"请输入第"<<i+1<<"个结点的权值: ";
			cin>>table[i].weight;
		}
	}
}
hTree::hTree(int *a,int size)
{
	nodenum=size;
	table=new hTNode[2*nodenum-1];
	for(int i=0;i<nodenum;i++)
		table[i].weight=a[i];
}
void hTree::encode()
{
	for(int i=0;i<nodenum;i++)
	{
		table[i].start=nodenum;
		table[i].bit=new int[nodenum+1];
		int child=i,parent=table[child].parent;
		while(parent!=-1)
		{
			if(table[parent].left==child)
				table[i].bit[table[i].start--]=0;
			else
				table[i].bit[table[i].start--]=1;
			child=parent;
			parent=table[child].parent;
		}
		cout<<setw(4)<<table[i].weight<<"的Huffman编码为:";
		for(int j=table[i].start+1;j<=nodenum;j++)
			cout<<table[i].bit[j]<<" ";
		cout<<endl;
	}
}
void hTree::output()
{
	if(table==NULL) return;
	cout<<"  Num    weight     parent    flag      left     right\n";
	int size=2*nodenum-1;
	for(int i=0;i<size;i++)
	{
		cout<<setw(4)<<i;
		table[i].outputNode();
	}
}
void hTree::create()
{
	if(table==NULL) return;
	int i,j,w1,w2,x1,x2;
	for(i=0;i<nodenum-1;i++)
	{
		w1=w2=maxvalue;
		x1=x2=0;
		for(j=0;j<nodenum+i;j++)
		{
			if(table[j].weight<w1&&table[j].flag==false)
			{
				w2=w1;
				x2=x1;
				w1=table[j].weight;
				x1=j;
			}
			else if(table[j].weight<w2&&table[j].flag==false)
			{
				w2=table[j].weight;
				x2=j;
			}
		}
			table[x1].parent=table[x2].parent=nodenum+i;
			table[x1].flag=table[x2].flag=true;
			table[nodenum+i].weight=table[x1].weight+table[x2].weight;
			table[nodenum+i].left=x1;
			table[nodenum+i].right=x2;	
	}	
}
