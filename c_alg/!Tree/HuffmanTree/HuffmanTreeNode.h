#include<iostream.h>
#include<iomanip.h>
#define width 10
class hTNode
{
public:
	int weight,parent,left,right;
	bool flag;
	int *bit;
	int start;
	hTNode(int w=0,int p=-1,int l=-1,int r=-1);
	void outputNode();
};
void hTNode::outputNode()
{
	cout<<setw(width)<<weight<<setw(width)<<parent<<setw(width)<<flag<<setw(width)<<left<<setw(width)<<right<<endl;
}
hTNode::hTNode(int w,int p,int l,int r)
{
	weight=w;
	flag=false;
	parent=p;
	left=l;
	right=r;
}
