#include"HuffmanTreeNode.h"
#define maxvalue 32768;
class hTree
{

	hTNode* table;
	string s,encodestr;
	int nodenum;
public:
	friend class hTNode;
	hTree(int n=0);
	void create();
	void encode(string &str);
	void decode(string &str);
	void output();
	hTree(const char *str,int size);
	hTree(string &str);
};
void hTree::decode(string &str)
{
	cout<<"霍夫曼编码为"<<str<<"\n对应的字符串为:";
	int j=0;
	while(j<str.length())		
		for(int i=0;i<nodenum;i++)
		{
			int t=0;
			for(int k=table[i].start+1;k<=nodenum;k++)
				if(char(table[i].bit[k]+'0')!=str[j+t]) break;
				else t++;
			if(k>nodenum)
			{
				j+=t;
				table[i].output();
			}		
		}
	cout<<endl;
}

hTree::hTree(string &str)
{
	s=str;
	encodestr="";
	int ascii[224];
	nodenum=0;
	for(int i=0;i<224;i++)
		ascii[i]=0;
	for(i=0;i<str.length();i++)
		ascii[str[i]-32]++;
	table=new hTNode[224];
	for(i=0;i<224;i++)
		if(ascii[i]!=0) 
		{			
			table[nodenum].weight=ascii[i];
			table[nodenum++].data=i+32;
		}
}
hTree::hTree(const char *str,int size)
{
	int ascii[224];
	nodenum=0;
	for(int i=0;i<224;i++)
		ascii[i]=0;
	for(i=0;i<size;i++)
		ascii[str[i]-32]++;
	table=new hTNode[224];
	for(i=0;i<224;i++)
		if(ascii[i]!=0) 
		{			
			table[nodenum].weight=ascii[i];
			table[nodenum++].data=i+32;
		}
}
hTree::hTree(int n)
{
	if(n==0) table=NULL;
	else
	{
		nodenum=n;
		table=new hTNode[2*n-1];	
		for(int i=0;i<n;i++)
		{
			cout<<"请输入第"<<i+1<<"个结点的权值: ";
			cin>>table[i].weight;
		}
	}
}
void hTree::encode(string &str)
{
	for(int i=0;i<str.length();i++)
	{
		for(int j=0;j<nodenum;j++)
		{
			if(table[j].data==str[i])
				for(int k=table[j].start+1;k<=nodenum;k++)
					encodestr+=(table[j].bit[k]+'0');
		}
	}
	cout<<str<<"的霍夫曼编码是"<<encodestr<<endl;
}
void hTree::output()
{
	if(table==NULL) return;
	cout<<"num  char    weight     parent    flag      left     right\n";
	int size=2*nodenum-1;
	for(int i=0;i<size;i++)
	{
		cout<<setw(4)<<i<<setw(4)<<table[i].data;
		table[i].outputNode();
	}
}
void hTree::create()
{
	if(table==NULL) return;
	int i,j,w1,w2,x1,x2;
	for(i=0;i<nodenum-1;i++)
	{
		w1=w2=maxvalue;
		x1=x2=0;
		for(j=0;j<nodenum+i;j++)
		{
			if(table[j].weight<w1&&table[j].flag==false)
			{
				w2=w1;
				x2=x1;
				w1=table[j].weight;
				x1=j;
			}
			else if(table[j].weight<w2&&table[j].flag==false)
			{
				w2=table[j].weight;
				x2=j;
			}
		}
			table[x1].parent=table[x2].parent=nodenum+i;
			table[x1].flag=table[x2].flag=true;
			table[nodenum+i].weight=table[x1].weight+table[x2].weight;
			table[nodenum+i].left=x1;
			table[nodenum+i].right=x2;	
	}
	for(i=0;i<nodenum;i++)
	{
		table[i].start=nodenum;
		table[i].bit=new int[nodenum+1];
		int child=i,parent=table[child].parent;
		while(parent!=-1)
		{
			if(table[parent].left==child)
				table[i].bit[table[i].start--]=0;
			else
				table[i].bit[table[i].start--]=1;
			child=parent;
			parent=table[child].parent;
		}
		cout<<table[i].data<<"的Huffman编码为:";
		for(j=table[i].start+1;j<=nodenum;j++)
			cout<<table[i].bit[j]<<" ";
		cout<<endl;
	}
}
