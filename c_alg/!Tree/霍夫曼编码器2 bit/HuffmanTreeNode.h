#include<iostream>
#include<iomanip>
#include<string>
#define width 10
using namespace std;
class hTNode
{

	int weight,parent,left,right;
	char data;
	bool flag;
	int *bit;
	int start;
public:
	friend class hTree;
	hTNode(char d=' ',int w=0,int p=-1,int l=-1,int r=-1);
	void outputNode();
	void outputCode(int num);
	void output();
};
void hTNode::output()
{
	cout<<data;
}
void hTNode::outputCode(int num)
{
	for(int i=start+1;i<=num;i++)
		cout<<bit[i];
}
void hTNode::outputNode()
{
	cout<<setw(width)<<weight<<setw(width)<<parent<<setw(width)<<flag<<setw(width)<<left<<setw(width)<<right<<endl;
}
hTNode::hTNode(char d,int w,int p,int l,int r)
{
	data=d;
	weight=w;
	flag=false;
	parent=p;
	left=l;
	right=r;
}
