#include<iostream>
using namespace std;
typedef double sortType;
void swap(sortType& x,sortType& y)
{
	sortType temp=x;
	x=y;
	y=temp;
}
void display(sortType a[],int n)
{
	for(int i=0;i<n;i++)
		cout<<a[i]<<" ";
	cout<<endl;
}
void createHeap(sortType a[],int n,int h)//h为开始下标，n为元素个数
{
	int i=h,j=2*h+1;
	sortType temp=a[h];
	while(j<n)
	{
		if(j<n-1&&a[j]<a[j+1]) j++;//如果左孩子小于右孩子 移到右孩子
		if(temp>=a[j])//如果根节点大于孩子中大的 则完成
			break;
		else
		{
			a[i]=a[j];
			i=j;
			j=2*j+1;
		}
	}
	a[i]=temp;
}
void heapSort(sortType a[],int n)
{
	int i;
	for(i=(n-1)/2;i>=0;i--)
		createHeap(a,n,i);
	for(i=n-1;i>0;i--)
	{
		swap(a[0],a[i]);
		createHeap(a,i,0);
	}
}
void quickSort(sortType a[],int low,int high)
{
	int i=low,j=high;
	sortType temp=a[low];//设定第一个元素为基准值
	while(i<j)
	{
		while(i<j&&temp<a[j]) j--;//退出循环或者因为i=j或者因为temp>a[j]
		if(i<j)
		{
			a[i]=a[j];
			i++;
		}
		while(i<j&&a[i]<temp) i++;
		if(i<j)
		{
			a[j]=a[i];
			j--;
		}
		a[i]=temp;
		if(low<i) quickSort(a,low,i-1);
		if(i<high) quickSort(a,j+1,high);
	}
}

void bubbleSort3(sortType table[],int n)//????
{
	int i,j,index=0;
	bool exChange=true;
	for(i=0;i<n-1&&exChange;i++)
	{
		exChange=false;
		j=index;
		for(j=0;j<n-i-1;j++)
			if(table[j]>table[j+1])
			{
				swap(table[j],table[j+1]);
				exChange=true;
			}
		if(!exChange&&table[j]<table[j+1])
			index++;		
	}
}
void bubbleSort2(sortType table[],int n)//加入判断是否交换的标记exChange
{
	bool exChange=true;
	for(int i=0;i<n-1&&exChange;i++)//若本趟没有交换则说明元素已经全部排好序
	{
		exChange=false;
		for(int j=0;j<n-i-1;j++)
			if(table[j]>table[j+1])
			{
				swap(table[j],table[j+1]);
				exChange=true;
			}
	}
}
void bubbleSort1(sortType table[],int n)
{
	for(int i=0;i<n-1;i++)   
		for(int j=0;j<n-i-1;j++) //!! j<n-i-1
			if(table[j]>table[j+1])swap(table[j],table[j+1]);
}

void selectSort(sortType table[],int n)
{
	for(int i=0;i<n-1;i++)//可以为i<n
	{
		int min=i;
		for(int j=i;j<n;j++)
			if(table[j]<table[min])
				min=j;
			if(i!=min)
				swap(table[i],table[min]);
	}
}
void insertSort(sortType a[],int n)  //直接插入排序
{
	sortType temp;
	for(int i=0;i<n-1;i++)
	{
		temp=a[i+1];//保存将要排序的数
		int j=i;
		while(j>=0&&a[j]>temp)
		{
			a[j+1]=a[j];
			j--;
		}
		a[j+1]=temp;
	}
}
void shellSort(sortType a[],int n,int d[],int numOfd)
{
	int i,j,m,k,span;
	sortType temp;
	for(m=0;m<numOfd;m++)
	{
		span=d[m];
		for(k=0;k<span;k++)
		{
			for(i=k;i<n-span;i+=span)
			{
				temp=a[i+span];
				j=i;
				while(j>-1&&temp<=a[j])
				{
					a[j+span]=a[j];
					j-=span;
				}
				a[j+span]=temp;
			}
		}
	}
}

		
