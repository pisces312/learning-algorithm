#include<iostream>
using namespace std;
template<class T>
class LQNode
{
public:
	T data;
	LQNode<T>* next;
	LQNode(T n=0)
	{
		data=n;
		next=NULL;
	}
};
