#include<iostream>
#include<string>
#include<cmath>
using namespace std;
typedef char dataType;
class BTreeNode
{
public:
	dataType data;
	BTreeNode *left,*right;
	BTreeNode(dataType ch='\0');
	~BTreeNode(){};
	void PreOrder(BTreeNode *p);
	void InOrder(BTreeNode *p);
	void PostOrder(BTreeNode *p);
	void OutputLeaf(BTreeNode *p);
	int BTreeLeafCount(BTreeNode *p);
	int BTreeCount(BTreeNode *p);
	void visit(BTreeNode *p);
};
void BTreeNode::PreOrder(BTreeNode *p)
{
	if(p!=NULL)
	{
		cout<<p->data<<" ";
		PreOrder(p->left);
		PreOrder(p->right);
	}
}

void BTreeNode::InOrder(BTreeNode *p)
{
	if(p!=NULL)
	{
		InOrder(p->left);
		cout<<p->data<<" ";
		InOrder(p->right);
	}
}
void BTreeNode::PostOrder(BTreeNode *p)
{
	if(p!=NULL)
	{
		PostOrder(p->left);
		PostOrder(p->right);
		cout<<p->data<<" ";
	}
}
int BTreeNode::BTreeCount(BTreeNode *p)
{
	if(p==NULL) return 0;
	else return BTreeCount(p->left)+BTreeCount(p->right)+1;
}
int BTreeNode::BTreeLeafCount(BTreeNode *p)
{	
	if(p==NULL) 		
		return 0;
	else if(p->left==NULL&&p->right==NULL) 
		return 1;
	else 
		return BTreeLeafCount(p->left)+BTreeLeafCount(p->right);
}
void BTreeNode::OutputLeaf(BTreeNode *p)
{
	if(p==NULL) return;		//函数无返回值时可直接用return返回
	if(p->left==NULL&&p->right==NULL) 
		cout<<p->data<<" ";
	else 
	{
		OutputLeaf(p->left);
		OutputLeaf(p->right);
	}
}
BTreeNode::BTreeNode(dataType ch)
{
	data=ch;
	left=NULL;
	right=NULL;
}