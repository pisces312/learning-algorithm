#include"SeqList.h"
#include"sort.h"
#include"BTree.h"
int seqSearch(SeqList &a,double s)
{
	int i=0;
	while(i<a.size&&a.table[i]!=s) i++;
	if(a.table[i]==s) return i;
	return -1;
}
int binarySearch2(SeqList &a,int low,int high,double s)//递归方法
{
		int mid=(low+high)/2;
		if(a.table[mid]==s) return mid;
		if(low==high) return -1;
		if(a.table[mid]<s)
			return binarySearch2(a,mid+1,high,s);
		else
			return binarySearch2(a,low,mid-1,s);
		return -1;
}
int binarySearch1(SeqList &a,double s)
{
	quickSort(a.table,0,a.size-1);//必须先排序
	int low=0,high=a.size-1,mid;
	while(low<=high)
	{		
		mid=(low+high)/2;
		if(a.table[mid]==s) return mid;  //返回的是在已排好序的序列中的位置
		if(a.table[mid]<s) low=mid+1;
		else high=mid-1;
	}
	return -1;
}
