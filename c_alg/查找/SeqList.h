#include<iostream>
//#include<stdlib>
#include<iomanip>
using namespace std;
class SeqList
{
public:
	double* table;
	int size;
	int len;
public:
	SeqList(double a[],int n);
	SeqList(int n=0);
	~SeqList();
	bool isEmpty() const;
	bool isFull() const;
	int length() const;
	double get(int i) const;
	bool set(int i,double k);
	bool insert(int i,double k);
	bool insert(double k);
	int search(double k);
	bool remove(double k);
	void output();
	bool create(int n);
};
SeqList::SeqList(double a[],int n)
{
	table=new double[n];
	for(int i=0;i<n;i++)
		table[i]=a[i];
	size=len=n;
}
SeqList::SeqList(int n)
{
	table=new double[n];
	size=n;
	len=0;
}
SeqList::~SeqList()
{
	delete []table;
}
bool SeqList::isEmpty() const
{
	return len==0;
}
bool SeqList::isFull() const
{
	return len>=size;
}
int SeqList::length() const
{
	return len;
}
double SeqList::get(int i) const
{
	if(i>0&&i<=len)
		return table[i-1];
	return -1;
}
bool SeqList::set(int i,double k)
{
	if(i>0&&i<=len+1)
	{
		table[i-1]=k;
		if(len==0) len++;
		return true;
	}
	return false;
}
int SeqList::search(double k)
{
	int i=1;
	while(i<=length()&&get(i)!=k)
	i++;
	if(i<=length())
		return i;
	else return 0;
}
bool SeqList::insert(int i,double k)
{
	if(!isFull())
	{
		if(i<=0) i=1;
		if(i>len) i=len+1;
		for(int j=len-1;j>=i-1;j--)
			table[j+1]=table[j];
		table[i-1]=k;
		len++;
		return true;
	}
	else
	{
		cerr<<"顺序表已满!\n";
		return false;
	}
}
bool SeqList::insert(double k)
{
	return insert(length()+1,k);
}
bool SeqList::remove(double k)
{
	if(!isEmpty())
	{
		int i=search(k);
		for(int j=i;j<length();j++)
			set(j,get(j+1));
		len--;
		return true;
	}
	else
	{
		cout<<"顺序表为空,无法删除值!\n";
		return false;
	}
}
void SeqList::output()
{
	int l=0;
	for(int i=0;i<len;i++)
	{
		cout<<setw(4)<<table[i]<<" ";
		if(++l%10==0) cout<<endl;
		
	}
	cout<<endl;
}
bool SeqList::create(int n)
{
	for(int i=1;i<=n;i++)
		if(!insert(i)) return false;
		else continue;
	return true;
}
	