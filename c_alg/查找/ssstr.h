#include<iostream>
#include<string>
using namespace std;
class SSStr
{
	char *table;
	int len;
public:
	SSStr(char *str);
	SSStr(int n);
	SSStr();
	SSStr(char ch);
	SSStr(const SSStr &s);
	~SSStr();
	int BFSearchSub(SSStr &s,int start =1);
	int BFSearchSub(char *str,int start=1);
	int length()
	{
		return len;
	}
	
	bool SetChar(int i,char ch);
	bool isCorrect(int i,int n=0);
	char GetChar(int i);
	
	void Catch(char *str);	
	void Catch(SSStr &s);	
	SSStr toUpcase(SSStr &s1);
	SSStr& operator=(char *str);
	SSStr& operator=(SSStr &s);
	bool operator==(char *str);
	bool operator==(SSStr &s);
	SSStr GetSub(int i,int n);
	int IndexChar(char ch,int start=1);
	SSStr InsertAfter(SSStr &s,int i);
	SSStr InsertBefore(SSStr &s,int i);
	friend ostream& operator<<(ostream &out,SSStr &s);
	SSStr operator+(SSStr &s2);
	SSStr& operator+=(SSStr &s2);
	int KMP(SSStr &s,int start =1);
	int KMP(char *s,int start =1);
	void KMPNext(int* next);
	void KMPNextval(int* nextval,int* next);
};
int SSStr::KMP(SSStr &s,int start)
{
	
	if(!isCorrect(start)) return 0;
	int i=start,j=1;
	int* next=new int[s.len+1],* nextval=new int[s.len+1];
	s.KMPNext(next);
	s.KMPNextval(nextval,next);
	while(i<=len&&j<=s.len)
	{
		if(table[i-1]==s.table[j-1])
		{
			i++;
			j++;
		}
		else
		{
			j=nextval[j]+1;
			i++;
		}		
	}
	if(j>s.len) 
		return i-s.len;
	return 0;
}
int SSStr::KMP(char *str,int start)
{
	SSStr s(str);
	if(!isCorrect(start)) return 0;
	int i=start,j=1;
	int* next=new int[s.len+1],* nextval=new int[s.len+1];
	s.KMPNext(next);
	s.KMPNextval(nextval,next);
	while(i<=len&&j<=s.len)
	{
		if(table[i-1]==s.table[j-1])
		{
			i++;
			j++;
		}
		else
		{
			j=nextval[j]+1;
			i++;
		}		
	}
	if(j>s.len) 
		return i-s.len;
	return 0;
}
void SSStr::KMPNextval(int* nextval,int* next)
{
	for(int j=1;j<len+1;j++)
	{
		if(GetChar(j)==GetChar(next[j])) nextval[j]=next[next[j]];
		else nextval[j]=next[j];
	}
//	for(int i=1;i<len+1;i++)
//			cout<<nextval[i]<<" ";
//	cout<<endl;
}
void SSStr::KMPNext(int* next)
{
	next[1]=0;
	next[2]=1;
	for(int j=3;j<len+1;j++)
		for(int k=j-1;k>1;k--)
			if(GetSub(1,k-1)==GetSub(j-k+1,k-1))
			{
				next[j]=k;
				break;
			}
			else
				next[j]=1;
//	for(int i=1;i<len+1;i++)
//			cout<<next[i]<<" ";
//	cout<<endl;
}

int SSStr::BFSearchSub(SSStr &s,int start)
{
	if(!isCorrect(start)) return 0;
	int i=start-1,j=0;
	while(i<len&&j<s.len)
	{
		if(table[i]==s.table[j])
		{
			i++;
			j++;			
		}
		else
		{
			j=0;
			i=i-j+1;
		}
		
	}
	if(j==s.len) return i-s.len+1;
	return 0;
}
int SSStr::BFSearchSub(char *str,int start)
{
	if(!isCorrect(start)) return 0;
	int i=start-1,j=0;
	int l=strlen(str);
	while(i<len&&j<l)
	{
		if(table[i]==str[j])
		{
			i++;
			j++;			
		}
		else
		{
			j=0;
			i=i-j+1;
		}
		
	}
	if(j==l) return i-l+1;
	return 0;
}
SSStr& SSStr::operator+=(SSStr &s2)
{
	SSStr s1=*this;
	int n1=s1.len,n2=s2.len;
	len=n1+n2;
	table=new char[len+1];
	strcpy(table,s1.table);
	for(int i=0;i<n2;i++)
		table[n1+i]=s2.table[i];
	table[len]='\0';
	return *this;
}
SSStr SSStr::operator+(SSStr &s2)
{
	SSStr temp=*this;
	temp+=s2;
	return temp;
}
SSStr SSStr::InsertBefore(SSStr &s,int i)
{
	if(i==1) return s+*this;
	return GetSub(1,i-1)+s+GetSub(i,len-i+1);
}
SSStr SSStr::InsertAfter(SSStr &s,int i)
{
	return GetSub(1,i)+s+GetSub(i+1,len-i);
}
int SSStr::IndexChar(char ch,int start)
{
	if(!isCorrect(start)) return 0;
	for(;start<=len;start++)
		if(table[start-1]==ch) return start;
		return 0;
}

bool SSStr::operator==(char *str)
{
	return strcmp(table,str)==0;
}
bool SSStr::operator==(SSStr &s)
{
	return strcmp(table,s.table)==0;
}
SSStr& SSStr::operator=(SSStr &s)
{
	delete []table;
	len=s.len;
	table=new char[len+1];
	strcpy(table,s.table);
	return *this;
}
SSStr& SSStr::operator=(char *str)
{
	len=strlen(str);
	table=new char[len+1];
	strcpy(table,str);
	return *this;
}


SSStr SSStr::toUpcase(SSStr &s1)
{
	SSStr s2(s1);
	for(int i=0;i<s2.len;i++)
	{
		char ch=s2.GetChar(i+1);
		if(ch>='a'&&ch<='z')
			s2.SetChar(i+1,ch-('a'-'A'));
	}
	return s2;
}
void SSStr::Catch(char *str)
{
	char *p=this->table;
	int l1=len,l2=strlen(str);
	len=l1+l2;
	table=new char[len+1];
	strcpy(table,p);
	for(int i=0;i<l2;i++)
		table[l1+i]=str[i];
	table[len]='\0';
}
void SSStr::Catch(SSStr &s)
{
	char *p=this->table;
	int l1=len,l2=s.len;
	len=l1+l2;
	table=new char[len+1];
	strcpy(table,p);
	for(int i=0;i<l2;i++)
		table[l1+i]=s.table[i];
	table[len]='\0';
}
ostream& operator<<(ostream &out,SSStr &s)
{
	out<<s.table;
	return out;
}
char SSStr::GetChar(int i)
{
	if(isCorrect(i))
		return table[i-1];
	return NULL;
}

bool SSStr::isCorrect(int i,int n)
{
	return (i+n-1<=len)&&i>0&&i<=len&&(n>=0)&&(n<=len);
}
bool SSStr::SetChar(int i,char ch)
{
	if(isCorrect(i))
	{
		table[i-1]=ch;
		return true;
	}
	return false;
}
SSStr::SSStr(char *str)
{
	len=strlen(str);
	table=new char[len+1];
	strcpy(table,str);
}
SSStr::SSStr(char ch)
{
	len=1;
	table=new char[2];
	table[0]=ch;
	table[1]='\0';
}
SSStr::SSStr(const SSStr &s)
{
	len=s.len;
	table=new char[len+1];
	strcpy(table,s.table);
}
SSStr::SSStr(int n)
{
	if(n<0) 
	{
		len=0;
		table=new char[1];
		table[0]='\0';
	}
	else
	{
		len=n;
		table=new char[len+1];
		table[0]='\0';	//长度为n 最后一个元素下标为n-1
	}
}
SSStr::SSStr()
{
	len=0;
	table=new char[1];	//SSStr str;的构造函数 不加() 
	table[0]='\0';
}
SSStr::~SSStr()
{
	delete []table;
	len=0;
}

SSStr SSStr::GetSub(int i,int n)
{
	SSStr temp;
	if(!isCorrect(i)&&n<1) return temp;
	if(n>len) n=len-i+1;
	SSStr sub(n);	
	for(int j=0;j<n;j++)
		sub.table[j]=table[(i++)-1];
	sub.table[n]='\0';
	return sub;
}
