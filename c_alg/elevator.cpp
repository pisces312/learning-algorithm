#include<iostream>
using namespace std;
void order(int* seq,int len)
{
	int i,j;
	for(i=0;i<len-1;i++)
	{
		for(j=0;j<len-i-1;j++)
		{
			if(seq[j]>seq[j+1])
			{
				int temp=seq[j];
				seq[j]=seq[j+1];
				seq[j+1]=temp;
			}
		}
	}
}
int getIndex(int *seq,int len,int begin)
{
	int index=-1;
	for(int i=0;i<len;i++)
	{
		if(seq[i]==begin)
		{
			index=i;
			break;
		}
	}
	//cout<<"index="<<index<<endl;
	return index;
}
void ElevatorD(int *seq,int len,int begin)
{
	int index,i;
	index=getIndex(seq,len,begin);
	if(index==-1)
	{
		cout<<"起始柱面号输入错误!\n";
		exit(1);
	}
	cout<<"电梯降序调度为:";
	for(i=index;i>=0;i--)
	{
		if(i==0&&index==len-1)
		{
			cout<<seq[i];
			break;
		}
		cout<<seq[i]<<"->";
	}
	for(i=index+1;i<len;i++)
	{
		if(i==len-1)
		{
			cout<<seq[i];
			break;
		}
		cout<<seq[i]<<"->";
	}
	
	cout<<endl;	
}
void ElevatorA(int *seq,int len,int begin)
{
	int index,i;
	index=getIndex(seq,len,begin);
	if(index==-1)
	{
		cout<<"起始柱面号输入错误!\n";
		exit(1);
	}
	cout<<"电梯升序调度为:";
	for(i=index;i<len;i++)
	{
		if(i==len-1&&index==0)
		{
			cout<<seq[i];
			break;
		}
		cout<<seq[i]<<"->";
	}
	for(i=index-1;i>=0;i--)
	{
		if(i==0)
		{
			cout<<seq[i];
			break;
		}
		cout<<seq[i]<<"->";
	}
	cout<<endl;	
}
void main()
{
	int* seq;
	int len,i;
	cout<<"输入访问次数:";
	cin>>len;


	seq=new int[len];
	cout<<"输入访问的柱面号:";
	for(i=0;i<len;i++)
	{
		cin>>seq[i];
	}
	cout<<"输入起始柱面号:";
	int begin;
	cin>>begin;

	order(seq,len);
	char choose;
	do{
		cout<<"选择升序或降序(输入'a'为升序，输入'd'为降序):";
		
		cin>>choose;
	
		switch(choose)
		{
			case 'a':
				ElevatorA(seq,len,begin);	
				break;
			case 'd':		
				ElevatorD(seq,len,begin);	
				break;
		}
	}while(choose!='a'&&choose!='d');
}
		



