#include<stdio.h>
#include<vector>
using namespace std;
/**
假设 n=a+b 其中 a 是已经找到的最大的那一节数,
b 是比 n 小的已经解决了的金链问题,由于 b 已经解决,
因此 b 的拆分能够表示从1,2,3,...b-1,b 的所有金链节数,
而再大一些的数就不能够表示了,比如 b+1,
所以必须要 a 参加进来,如果 n 是奇数,可令 a=b+1,
这样 n=2b+1,所以 b=(n-1)/2,a=(n+1)/2,
这样就找到了最大的一节的节数 a ,
然后对 b=(n-1)/2继续应用如上的办法,即可解决问题.
如果 n 是偶数,可令 a=b ,这样虽然 a 本身不能表示出 b+1,
但是可以从 b 的拆分中拿出一个1来(这个1是必须存在的,
因为要表示从1,2,3,...b-1,b的所有数)与 a 组成 a+1 也就
是 b+1.所以 n=a+b=2a=2b,a=b=n/2.这样也找到了 n 为偶数
时最大的一节金链的节数.
**/
void BreakGoldenChain(int n,vector<int>& c) {
    if(n==0) {
        return;
    }
    int a;
    if((n&0x01)) { //odd
        a=(n+1)/2;
    } else {
        a=n/2;
    }
    c.push_back(a);
    BreakGoldenChain(n-a,c);
}
int main() {

    vector<int> v;
    for(int j=1; j<16; ++j) {
        BreakGoldenChain(j,v);
        for(int i=0; i<v.size(); ++i) {
            printf("%d ",v[i]);
        }
        printf("\n");
        v.clear();
    }
}
