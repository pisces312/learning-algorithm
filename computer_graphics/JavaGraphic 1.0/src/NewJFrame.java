import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;
/*
 * NewJFrame.java
 *
 * Created on 2007年3月27日, 上午8:11
 */

/**
 *
 * @author  pisces312
 */
public class NewJFrame extends javax.swing.JFrame {
    /** Creates new form NewJFrame */
    Graphics g;
    Point p1,p2;
    Point[] p;
    Vector<Point> points=new Vector();
    public NewJFrame() {
        initComponents();
        p1=new Point(20,50);
        p2=new Point(250,100);
        g=this.getGraphics();
    }
    public void paint(Graphics g) {
        drawBack();
//        this.BresenHam(p1,p2);
//        this.BresenHam(p2,p1);
//        this.BresenHam2(p1,p2);
//        this.BresenHam3(p1,p2);
    }
    void drawPixel(int x,int y) {
        g.fillRect(x,y,1,1);
    }
    void drawPixel(Point p){
        g.fillRect(p.x,p.y,1,1);
    }
    void drawBack(){
//        g.setColor(Color.BLACK);
        g.setColor(Color.WHITE);
        this.paintComponents(g);
        g.fillRect(0,0,getWidth(),getHeight()-50);
//        g.fillRect(0,0,getWidth(),getHeight());
//        jButton1.pai.paint(g);
        
//        this.paintAll(g);
//        jButton1.paintComponents(g);
//        jButton2.paintComponents(g);
    }
    void getPixel(Point point){
//        this.get
//        g.getClipBounds(new Rectangle(point));
    }
    void midpointellipse(int a,int b,Point p){
        int x=0,y=b;
        double d=b*b +a*a*(-b+0.25);
//        drawPixel(x,y);
       while( b*b*x<a*a*y){
           drawPixel(x+p.x,y+p.y);
           drawPixel(-x+p.x,y+p.y);
           drawPixel(x+p.x,-y+p.y);
           drawPixel(-x+p.x,-y+p.y);
            if (d<0){
                d+=b*b*(2*x+3); 
                x++;  
           }
           else{
               d+=(b*b*(2*x+3)+2*a*a*(1-y));
               x++;  y--;
            } 
            
	}
        d=b*b*(x+0.5)*(x+0.5)+a*a*(y-1)*(y-1)-a*a*b*b;
        while(y>0){
           drawPixel(x+p.x,y+p.y);
           drawPixel(-x+p.x,y+p.y);
           drawPixel(x+p.x,-y+p.y);
           drawPixel(-x+p.x,-y+p.y);
            if (d<0) { 
                d+=2*b*b*(x+1)+a*a*(-2*y+3);
                x++;  y--;
            }
            else{
                d+= a*a*(-2*y+3);  y--; 
            }
        }
	   

    }
    void drawBezier(){
        generateBezier(p,50);
    }
    
    
//    void Bezier(Point[] p,int n,double t){
//        double x=0,y=0;
//        double b;
//        for(int i=0;i<p.length;i++){
//            b=B(i,n,t);
//            x+=(p[i].x*b);
//            y+=(p[i].y*b);
//            
//        }
//        drawPixel((int)x,(int)y);
////        return new Point((int)x,(int)y);
//    }
    void generateBezier(Point[] p,int n){
        for(int i=0;i<n;i++){
//            Bezier(p,p.length-1,(double)(i*1.0/n));
            this.midpointLine(Bezier(p,p.length-1,(double)(i*1.0/n)),Bezier(p,p.length-1,(double)((i+1)*1.0/n)));
        }
        
    }
    Point Bezier(Point[] p,int n,double t){
        double x=0,y=0;
        double b;
        for(int i=0;i<p.length;i++){
            b=B(i,n,t);
            x+=(p[i].x*b);
            y+=(p[i].y*b);
            
        }
        drawPixel((int)x,(int)y);
        return new Point((int)x,(int)y);
    }
    
    double B(int i,int n,double t){
        int j;
        double sum=C(i,n);
//        System.out.print("c="+sum);
        for(j=1;j<=i;j++){
            sum*=t;
        }
        for(j=1;j<=n-i;j++){
            sum*=(1-t);
        }
        return sum;
        
    }
    double C(int i,int n){
        int j,sum=1;
        //相当于已经除了i!
        for(j=i+1;j<=n;j++){
            sum*=j;
        }
        for(j=2;j<=n-i;j++){
            sum/=j;
        }
        return sum;
    }
    void drawRect(Point p1,Point p2){
//        drawBack();
        Point t1=new Point(p2.x,p1.y),t2=new Point(p1.x,p2.y);
        
        
        
//        this.midpointLine(p1,p2);
        midpointLine(p1,t1);
        midpointLine(p1,t2);
        midpointLine(p2,t1);
        midpointLine(p2,t2);
//        dda_Line(p1,t1);
//        dda_Line(p1,t2);
//        dda_Line(p2,t1);
//        dda_Line(p2,t2);
        
    }
    
    
    
    
    
    
    void dda_Line(Point p1,Point p2) {
//        drawBack();
        double dx,dy,x,y;
        //k为较长的一侧的长度
        int k=Math.abs(p2.x-p1.x),i=Math.abs(p2.y-p1.y);
        
        if(i>k){
            k=i;
            dx=(double)(p2.x-p1.x)/k;
            dy=p2.y-p1.y<0?-1:1;
        } else{
            dx=p2.x-p1.x<0?-1:1;
            dy=(double)(p2.y-p1.y)/k;
        }
        //四舍五入
        x=p1.x+0.5;
        y=p1.y+0.5;
//        g.setColor(new Color(255,255,255));
        for(i=1;i<=k;i++){
            drawPixel((int)x,(int)y);
            x+=dx;
            y+=dy;
        }
    }
    void midpointLine(Point p1,Point p2) {
        int a=p1.y-p2.y,b=p2.x-p1.x,d1 = 0,d2 = 0,d = 0,x=p1.x,y=p1.y;
        int dx,dy;
        if(a>0)
            dy=-1;
        else if(a<0)
            dy=1;
        else
            dy=0;
        
        if(b>0){
            dx=1;
            //b=a保证45度斜线能画出
            if(a<=0&&b+a>=0){
                //1
                d=a+a+b;
                d1=a+a;
                d2=a+b+a+b;
                do{
                    drawPixel(x,y);
                    if(d<0){
                        x+=dx;
                        y+=dy;
                        d+=d2;
                    }else{
                        x+=dx;
                        d+=d1;
                    }
                }while(x<p2.x);
                
            } else if(a<0&&b+a<0){
                //2
                d=a+b+b;
                d1=b+b;
                d2=a+b+b+a;
                do{
                    drawPixel(x,y);
                    if(d>0){
                        x+=dx;
                        y+=dy;
                        d+=d2;
                    }else{
                        y+=dy;
                        d+=d1;
                    }
                }while(y<p2.y);
                
            } else if(a>0&&b-a<=0&&b>0){
                //7
                d=a-b-b;
                d1=-b-b;
                d2=a-b+a-b;
                do{
                    drawPixel(x,y);
                    if(d<0){
                        x+=dx;
                        y+=dy;
                        d+=d2;
                    }else{
                        y+=dy;
                        d+=d1;
                    }
                }while(y>p2.y);
                
            } else if(a>=0&&b-a>0&&b>0){
                d=a+a-b;
                d1=a+a;
                d2=a-b+a-b;
                do{
                    drawPixel(x,y);
                    if(d>0){
                        x+=dx;
                        y+=dy;
                        d+=d2;
                    }else{
                        x+=dx;
                        d+=d1;
                    }
                }while(x<p2.x);
                
            }
            
            
            
            
        } else if(b<0){
            dx=-1;
            
            if(a<0&&b-a>=0){
                //3
                d=b+b-a;
                d1=b+b;
                d2=b+b-a-a;
                do{
                    drawPixel(x,y);
                    if(d<0){
                        x+=dx;
                        y+=dy;
                        d+=d2;
                    }else{
                        y+=dy;
                        d+=d1;
                    }
                }while(y<p2.y);
                
            } else if(a<=0&&b-a<0){
                //4
                d=b-a-a;
                d1=-a-a;
                d2=b+b-a-a;
                do{
                    drawPixel(x,y);
                    if(d>0){
                        x+=dx;
                        y+=dy;
                        d+=d2;
                    }else{
                        x+=dx;
                        d+=d1;
                    }
                }while(x>p2.x);
                
            } else if(a>=0&&a+b<=0){
                //5
                d=-b-a-a;
                d1=-a-a;
                d2=-a-b-a-b;
                do{
                    drawPixel(x,y);
                    if(d<0){
                        x+=dx;
                        y+=dy;
                        d+=d2;
                    }else{
                        x+=dx;
                        d+=d1;
                    }
                }while(x>p2.x);
                
            } else if(a>0&&a+b>0){
                //6
                d=-b-b-a;
                d1=-b-b;
                d2=-a-b-a-b;
                do{
                    drawPixel(x,y);
                    if(d>0){
                        x+=dx;
                        y+=dy;
                        d+=d2;
                    }else{
                        y+=dy;
                        d+=d1;
                    }
                }while(y>p2.y);
                
            }
        } else{
            if(a<0){
                do{
                    drawPixel(x,y);
                    y+=dy;
                }while(y<p2.y);
            } else{
                do{
                    drawPixel(x,y);
                    y+=dy;
                }while(y>p2.y);
            }
        }
    }
    void BresenHam(Point p1,Point p2){
        int i,dx,dy,e;
//        int x1=p1.x ,x2=p2.x,y1=p1.y,y2=p2.y;
        Point t1=new Point(p1),t2=new Point(p2);
//        this.drawBack();
//        g.setColor(Color.WHITE);
        if(Math.abs(t1.x-t2.x)>(t2.y-t1.y)){
            if(t1.x>t2.x){
                Point t=t2;
                t2=t1;
                t1=t;
            }
            dx=t2.x-t1.x;
            dy=t2.y-t1.y;
            e=2*dy-dx;
            for(i=0;i<=dx;i++){
//                System.out.println("draw");
                this.drawPixel(t1.x,t1.y);
                if(e>0){
                    t1.y++;
                    e=e+2*dy-2*dx;
                }
                e=e+2*dx;
                t1.x++;
            }
        } else{
            if(t1.y>t2.y){
                Point t=t2;
                t2=t1;
                t1=t;
            }
            dx=t2.x-t1.x;
            dy=t2.y-t1.y;
            e=dx+dx-dy;
            for(i=0;i<=dy;i++){
                this.drawPixel(t1.x,t1.y);
                if(e>0){
                    t1.x++;
                    e=e-dy-dy;
                    e=e+dy+dy;
                    t1.y++;
                }
            }
        }
    }
    
    
    
    
    
    
    
    void BresenHam2(Point p1,Point p2){
        int i,dx,dy,e,sign=1;
        int x,y;
//        int x1=p1.x ,x2=p2.x,y1=p1.y,y2=p2.y;
        Point t1=new Point(p1),t2=new Point(p2);
//        this.drawBack();
//        g.setColor(Color.WHITE);
        x=t1.x;
        y=t1.y;
        dx=t2.x-t1.x;
        dy=t2.y-t1.y;
        if(Math.abs(dx)>=(dy)){
            
            if(dx<0){
                dx*=-1;
                sign=-1;
            }
            e=2*dy-dx;
            
            for(i=1;i<=dx;i++){
                this.drawPixel(x,y);
                x+=sign;
                if(e>0){
                    y++;
                    e=e+2*dy-2*dx;
                } else{
                    e=e+2*dy;
                    
                }
            }
        } else{
            if(dy<0){
                dy*=-1;
                sign=-1;
            }
            
            e=dx+dx-dy;
            for(i=1;i<=dy;i++){
                this.drawPixel(x,y);
                y+=sign;
                if(e>0){
                    x++;
//                    e=e-dy-dy;
                    e=e+2*dx-2*dy;
                } else{
//                    e=e+dy+dy;
                    e=e+dx+dx;
                    
                }
            }
        }
    }
    
    
    
    
    
    void BresenHam3(Point p1,Point p2){
        int i,dx,dy,e;
        int x,y;
        dx=p2.x-p1.x;
        dy=p2.y-p1.y;
        e=dy+dy-dx;
        x=p1.x;
        y=p1.y;
        for(i=0;i<dx;i++){
            this.drawPixel(x,y);
            x++;
            if(e>0){
                y++;
                e=e+dy+dy-dx-dx;
            } else
                e=e+dy+dy;
        }
    }
    
    
    void midpointCircle(Point p,int r) {
        int x=0,y=r,d=1-r;
        do{
            drawPixel(x+p.x,y+p.y);
            drawPixel(-x+p.x,-y+p.y);
            drawPixel(-x+p.x,y+p.y);
            drawPixel(x+p.x,-y+p.y);            
            drawPixel(y+p.x,x+p.y);
            drawPixel(-y+p.x,-x+p.y);
            drawPixel(-y+p.x,x+p.y);
            drawPixel(y+p.x,-x+p.y);
            if(d<0){
                d+=x+x+3;
                x++;
            } else{
                d+=x-y+x-y+5;
                x++;
                y--;                
            }
        }while(x<y);
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" 生成的代码 ">//GEN-BEGIN:initComponents
    private void initComponents() {
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("\u8ba1\u7b97\u673a\u56fe\u5f62\u5b66\u6f14\u793a-\u7f51\u7edc041 \u502a\u529b");
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                formMousePressed(evt);
            }
        });
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                formMouseDragged(evt);
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                formMouseMoved(evt);
            }
        });

        jButton1.setText("\u7ed8\u5236");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });

        jButton2.setText("\u5237\u65b0");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1)
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addContainerGap(174, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(263, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap())
        );
        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked
// TODO 将在此处添加您的处理代码：
        this.drawBack();
//        jButton1.setVisible(true);
//        jButton2.setVisible(true);
    }//GEN-LAST:event_jButton2MouseClicked
    
    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
// TODO 将在此处添加您的处理代码：
//        p=new Point[2];
//        p[0]=new Point(100,50);
//        
//        p[1]=new Point(50,100);
//        p[2]=new Point(60,60);
//        p[3]=new Point(20,60);
        if(points.size()==0)
            return;
        p=null;
        p=new Point[points.size()];
        for(int i=0;i<p.length;i++){
            p[i]=new Point(points.get(i));
        }
//        this.drawBack();
        g.setColor(Color.WHITE);
        this.drawBezier();
          points.removeAllElements();
        
    }//GEN-LAST:event_jButton1MouseClicked
    
    private void formMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMousePressed
// TODO 将在此处添加您的处理代码：
        
        
        p1=getMousePosition();
//        drawBack();
//        g.setColor(Color.BLACK);
//        this.midpointellipse(50,20,this.getMousePosition());
        
        
//synchronized(this){
        //int i;
        //long time=System.currentTimeMillis();
      //  for(i=0;i<1000;i++){
    //    this.dda_Line(p1,p2);
        ////this.midpointLine(p1,p2);
            //this.BresenHam3(p1,p2);
        //}
  //      time=System.currentTimeMillis()-time;
//        g.drawString("绘制"+i+"次从点("+p1.x+","+p1.y+")到("+p2.x+","+p2.y+")的直线共耗时 "+time+"ms",10,40);
        //}


//        this.dda_Line()
//        g.setColor(Color.WHITE);
//        this.drawBezier();
        
//        System.currentTimeMillis();
        
//        Point t=this.getMousePosition();
//        points.add(t);
//        g.setColor(Color.WHITE);
//        this.drawPixel(t);
//        g.drawString("P"+points.size(),t.x,t.y);
    }//GEN-LAST:event_formMousePressed
    
    private void formMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseDragged
// TODO 将在此处添加您的处理代码：
         p2=getMousePosition();
         drawBack();
         g.setColor(Color.BLACK);
         this.midpointellipse(Math.abs(p1.x-p2.x),Math.abs(p1.y-p2.y),p1);
//         midpointCircle(p1,(int)Point.distance(p1.x,p1.y,p2.x,p2.y));
         
         //g.setColor(Color.WHITE);
//              this.BresenHam2(p1,p2);
         //this.BresenHam3(p1,p2);
//         this.BresenHam(p1,p2);
        //this.dda_Line(p1,p2);///!!!!
         //this.midpointLine(p1,p2);
        
         
//         this.drawRect(p1,p2);
        
        
        
//        System.out.println("move mouse");
    }//GEN-LAST:event_formMouseDragged
    
    private void formMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseMoved
// TODO 将在此处添加您的处理代码：
//        p2=this.getMousePosition();
//        this.dda_Line(p1,p2);
    }//GEN-LAST:event_formMouseMoved
    
    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
// TODO 将在此处添加您的处理代码：
//        p1=this.getMousePosition();
//        System.out.println(p1.x+","+p1.y);
    }//GEN-LAST:event_formMouseClicked
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NewJFrame().setVisible(true);
            }
        });
    }
    
    // 变量声明 - 不进行修改//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    // 变量声明结束//GEN-END:variables
    
}
