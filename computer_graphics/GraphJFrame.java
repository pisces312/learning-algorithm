import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JFrame;
/*
 * RoseJFrame.java
 *
 * Created on 2007年10月9日, 下午11:32
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author Administrator
 */
public class GraphJFrame extends JFrame implements ComponentListener,ItemListener{
    Graphics g;
    /** Creates a new instance of RoseJFrame */
    public GraphJFrame() {
        //调用父类构造函数
        super("图形绘制");
        //设置窗体大小
        setSize(800,600);
        //设置默认关闭窗口执行的动作
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        //设置布局管理器
        setLayout(new FlowLayout());
        //设置为可见，默认不可见
        setVisible(true);
        
        //设置Container的背景
        getContentPane().setBackground(Color.WHITE);
        addComponentListener(this);
    }    
    //计算阿基米德曲线 获得一个点，将计算得到的横纵坐标由其返回
    void Archimedes(Point p,double a,double angle){
        p.setLocation(a*angle*Math.cos(angle),a*angle*Math.sin(angle));
    }
    //四叶玫瑰线
    //a是玫瑰叶顶端到中心的距离
    void Rose4sin(Point p,double a,double angle){
        double sin2=Math.sin(2*angle);
        p.setLocation(a*sin2*Math.cos(angle),a*sin2*Math.sin(angle));
    }
    //a是玫瑰叶顶端到中心的距离
    void Rose4cos(Point p,double a,double angle){
        double sin2=Math.cos(2*angle);
        p.setLocation(a*sin2*Math.cos(angle),a*sin2*Math.sin(angle));
    }
    //三叶玫瑰线
    //a是玫瑰叶顶端到中心的距离
    void Rose3cos(Point p,double a,double angle){
        double sin2=Math.cos(3*angle);
        p.setLocation(a*sin2*Math.cos(angle),a*sin2*Math.sin(angle));
    }
    //a是玫瑰叶顶端到中心的距离
    void Rose3sin(Point p,double a,double angle){
        double sin2=Math.sin(3*angle);
        p.setLocation(a*sin2*Math.cos(angle),a*sin2*Math.sin(angle));
    }
    void heartIn(Point p,double a,double angle){
        p.setLocation(a*Math.pow(Math.cos(angle),3),a*Math.pow(Math.sin(angle),3));
    }
    void heartOut(Point p,double a,double angle){
        double c=a-a*Math.cos(angle);
        p.setLocation(c*Math.cos(angle),c*Math.sin(angle));
    }
    //伯努利
//    void Bsin(Point p,double a,double angle){
//        double sin2=Math.sin(3*angle);
//        p.setLocation(a*sin2*Math.cos(angle),a*sin2*Math.sin(angle));
//    }
//    void Bsin(Point p,double a,double angle){
//        double sin2=Math.sin(3*angle);
//        p.setLocation(a*sin2*Math.cos(angle),a*sin2*Math.sin(angle));
//    }
    //在这里画图
    void drawGraph(int anchorX,int anchorY,double begin,double end,double dAngle,Color color){
        
        g.setColor(color);
        int a=15;
        Point p=new Point();
        while(begin<end){
            begin+=dAngle;
            Archimedes(p,a,begin);
//            g.fillRect(anchorX+p.x,anchorY+p.y,1,1);
//            Rose4sin(p,200,begin);
            g.fillRect(anchorX+p.x,anchorY+p.y,1,1);
//            Rose4cos(p,200,begin);
//            Rose3cos(p,200,begin);
//            g.fillRect(anchorX+p.x,anchorY+p.y,1,1);
//            Rose3sin(p,200,begin);
//            heartIn(p,200,begin);
//            heartOut(p,200,begin);
//            g.fillRect(anchorX+p.x,anchorY+p.y,1,1);
        }
        
    }
    //画坐标
    void drawCoordinate(int oX,int oY,Color color){
        g.setColor(color);
        g.drawLine(oX,0,oX,oY*2);
        g.drawLine(0,oY,oX*2,oY);
    }
    public void paint(Graphics g){
        int x0,y0;
        this.g=g;
        //绘制Container的背景
        super.paint(g);
        x0=getWidth()/2;
        y0=getHeight()/2;
        //画坐标轴
        drawCoordinate(x0,y0,Color.BLACK);
        
        //角度增量
        double dAngle=Math.PI/1024;
//        double dAngle=Math.PI/128;
        double maxAngle=5*Math.PI;
//        double maxAngle=200*Math.PI;
        drawGraph(x0,y0,0,maxAngle,dAngle,Color.BLUE);
        
     
        
    }
    
    
    public void componentResized(ComponentEvent componentEvent) {
    }
    
    public void componentMoved(ComponentEvent componentEvent) {
    }
    
    public void componentShown(ComponentEvent componentEvent) {
    }
    
    public void componentHidden(ComponentEvent componentEvent) {
    }
    
    public void itemStateChanged(ItemEvent itemEvent) {
    }
    public static void main(String[] args) {
        new GraphJFrame();
    }
    
    
}

