#ifndef CHINESECHESS_H_INCLUDED
#define CHINESECHESS_H_INCLUDED
#include "tools.h"
/**
下过中国象棋的朋友都知道，双方的“将”和“帅”相隔遥远，并且它们不能照面。在象棋 残局中，许多高手能利用这一规则走出精妙的杀招。假设棋盘上只有“将”和“帅”二子（为了下面叙述方便，我们约定用A 表示“将”，B 表示“帅”）
输出A 、B 所有合法位置。要求在代码中只能使用一个变量

1 2 3
4 5 6
7 8 9

1 2 3
4 5 6
7 8 9
即A、B不能在同一列
**/
void chessMethod1() {
    for (int i=1;i<10;i++) {
        for (int j=1;j<10;j++) {
            if (i%3!=j%3) {
                printf("%d %d\n",i,j);
            }

        }
    }

}
//
#define HALF_BITS_LENGTH 4
// 这个值是记忆存储单元长度的一半，在这道题里是4bit
#define FULLMASK 0xff
// 这个数字表示一个全部bit的mask，在二进制表示中，它是11111111。
#define LMASK (FULLMASK << HALF_BITS_LENGTH)
// 这个宏表示左bits的mask，在二进制表示中，它是11110000。
#define RMASK (FULLMASK >> HALF_BITS_LENGTH)
// 这个数字表示右bits的mask，在二进制表示中，它表示00001111。
#define RSET(b, n) (b = ((LMASK & b) ^ n))
// 这个宏，将b的右边设置成n
#define LSET(b, n)  (b = ((RMASK & b) ^ (n << HALF_BITS_LENGTH)))
// 这个宏，将b的左边设置成n
#define RGET(b) (RMASK & b)
// 这个宏得到b的右边的值
#define LGET(b) ((LMASK & b) >> HALF_BITS_LENGTH)
// 这个宏得到b的左边的值
#define GRIDW 3
//使用一个变量
void chessMethod2() {
    unsigned char b;
//每个将的活动位置为9
    for (LSET(b, 1); LGET(b) <= 9; LSET(b, (LGET(b) + 1)))

        for (RSET(b, 1); RGET(b) <= 9; RSET(b, (RGET(b) + 1)))

            if (LGET(b) % GRIDW != RGET(b) % GRIDW)

                printf("A = %d, B = %d\n", LGET(b), RGET(b));
}
//????排列组合
//共81种排列方法
void chessMethod3() {
    BYTE i = 81;
    while (i--)    {
        //???
        //i/9取前九格
        //i%9取后九个格
        if (i / 9 % 3 == i % 9 % 3)
            continue;
        printf("A = %d, B = %d\n", i / 9 + 1, i % 9 + 1);

    }
}
void chessMethod4() {
    struct {
//!限制位宽
        unsigned char a:4;

        unsigned char b:4;

    } i;
//    struct {
//        unsigned char a;
//
//        unsigned char b;
//
//    } j;
//    cout<<sizeof(i)<<endl;
//    cout<<sizeof(j)<<endl;



    for (i.a = 1; i.a <= 9; i.a++)

        for (i.b = 1; i.b <= 9; i.b++)

            if (i.a % 3 != i.b % 3)

                printf("A = %d, B = %d\n", i.a, i.b);
}
#endif // 中国象棋将帅问题_H_INCLUDED
