#ifndef JUDGELISTINTERSECTION_H_INCLUDED
#define JUDGELISTINTERSECTION_H_INCLUDED
//#include <map>
#include <set>
/**

相交，即两个链表中的某个节点同时指向一个节点，多对一（不存在分开的情况）
**/
namespace JudgeListIntersection {
    struct Node {
        int data;
        Node* next;
    };
    /**
    基本方法
    **/
    Node* isListIntersect(Node* p1,Node* p2) {
        while (p1!=NULL) {
            Node* t=p2;
            while (t!=NULL) {
                if (p1->next==t->next) {
                    return p1->next;
                }
                t=t->next;

            }
            p1=p1->next;

        }
        return NULL;
    }
    //
    /**
    解法2
    利用计数方法
    可以用hash或set，利用其中一个链表建立，另一个容器检查
    **/
    Node* isListIntersectCount(Node* p1,Node* p2) {
        set <Node*> s;
        while(p1!=NULL){
            s.insert(p1);
            p1=p1->next;
        }
        set<Node*>::const_iterator itr;
        while(p2!=NULL){
            itr=s.find(p2);
            if(itr!=s.end()){
                return *itr;

            }
            p2=p2->next;

        }
        return NULL;


    }
        void printNodes(Node *p){
//        Node *p=node;
        while(p!=NULL){
            cout<<p->data<<" ";
            p=p->next;
        }
        cout<<endl;
    }
    //
    /**
    解法3
    ！！！！
    把第二个链表接在第一个链表后面，如果得到的链表有环，则说明两个链表相交
    传入一个表头，从其遍历一遍又回到起点，则说明存在环
    **/
    Node* hasCircle(Node *node){
        Node* head=node;
        Node* p=node;
        while(p!=NULL){
            if(p==node){
                break;
            }
            p=p->next;
        }
        return p;
    }
    /**
    这里假设传来的是头指针
    !如果有环，则第二个链表的表头一定在环上!!
    **/
    Node *isListIntersectAppend(Node* p1,Node* p2){
        Node *p=p1;
        assert(p!=NULL);
        while(p->next!=NULL){
            p=p->next;
        }
        //!接上,这里对原表进行了修改
        p->next=p2;
        Node* r=hasCircle(p2);
        //!再把表断开
        p->next=NULL;
        return r;


    }
    //
    //
    /**
    解法4
    两个没有环的链表相较于某一节点，那么该节点之后的所有节点都是两个链表锁共有。
    ！！如果两链表没有环，且相交于一点，则最后一个节点一定是共有的
    ！该方法只能判断是否相交，但无法找到相交点

    **/
    Node * getLastNode(Node* p){
        //这里保证链表至少有一个节点
        assert(p!=NULL);
        while(p->next!=NULL){
            p=p->next;
        }
        return p;
    }
    bool isListIntersectLast(Node* p1,Node* p2){
        return getLastNode(p1)==getLastNode(p2);

    }
    void testJudgeListIntersection() {
                //产生一个链表
        int size=4;
        Node *nodes=new Node[size];
        nodes[0].data=3;//=new Node;//{3,nodes[1]};
        nodes[1].data=6;
        nodes[2].data=2;
        nodes[3].data=1;
        for(int i=0;i<size-1;i++){
            nodes[i].next=&nodes[i+1];
        }
        nodes[size-1].next=NULL;
        //
        //让两个链表同时指向nodes[2]
        Node *nodes2=new Node[2];
        nodes2[0].data=6;//=new Node;//{3,nodes[1]};
        nodes2[0].next=&nodes2[1];
        nodes2[1].data=8;
        nodes2[1].next=&nodes[2];
        //
        printNodes(nodes);
        cout<<endl;
        printNodes(nodes2);
        //
        //
        assert(isListIntersect(nodes,nodes2)!=NULL);
        assert(isListIntersectCount(nodes,nodes2)!=NULL);
        assert(isListIntersectAppend(nodes,nodes2)!=NULL);
        assert(isListIntersectLast(nodes,nodes2));


    }
};


#endif // JUDGELISTINTERSECTION_H_INCLUDED
