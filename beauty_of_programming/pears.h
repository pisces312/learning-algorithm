#ifndef PEARS_H_INCLUDED
#define PEARS_H_INCLUDED

/*
* Copyright (c) 2010,东南大学数据库课题组
* All rights reserved.
*
* 文件名称：
* 摘    要：简要描述本文件的内容
*
* 当前版本：1.0
* 作    者：倪力
* 完成日期：2010年
*/
namespace ProgrammingPears {
//使用快速排序选择第k个最小的元素,k从0开始
//数组中数据被修改,返回的是下标
//11.9
int selectKMin(int *x,int l, int u, int k);
int selectKMin2(int *x,int l, int u, int k);
//p18 2.8
//是否存在n个元素中取k个不超过t
bool IsKArrayLeNum(int *x,int n,int t,int k);
bool IsKArrayLeNum2(int *x,int n,int t,int k);
//4.5 even:x/2,odd:3*x+1
void HailStone(int x);

//舍九法验证加法a+b==r
//加数的数字总和与和数的数字总和模9相等
bool ValidatePlus(int a,int b,int r);
//利用72法则计算以rate利率投资几年后翻番
int GetYearsOfDoubleUsing72Rule(double rate);

int FindMaxUsingDivide(int* x,int n);
//x一定要含有n+1个元素的空间
int FindMaxUsingSential(int* x,int n);

//
double CalcPolynomialValue1(double* a,int n,double x);
double CalcPolynomialValue2(double* a,int n,double x);

//TODO
void RotateMatrix(int* x,int row,int col);


////////////////////////////////////////
//p100:a<10,b<10
inline byte CompressTwoNum2Byte(byte a,byte b);
inline void UncompressByte2TwoNum(byte c,byte& a,byte& b);

inline byte CompressTwoNum2Byte2(byte a,byte b);
inline void UncompressByte2TwoNum2(byte c,byte& a,byte& b);
///////////////////////////
extern bool uppertable[256];
#define isupper2(c)  (uppertable(c))

//对每个字符对应的表元素用每个位代表一个状态
extern char bigtable[256];
const char UPPER=0x1;
const char LOWER=0x2;
const char DIGIT=0x4;
#define isupper3(c)  (bigtable(c)&UPPER)
#define isalnum3(c)  (bigtable(c)&(DIGIT|LOWER|UPPER))
///////////////////////////
//p101 共享空间存储
//x为第一个方阵中的点位置的数据，
//需要len*len的方阵存储任意两点之间的距离
//共享空间的两个方阵必须有相同的边长
//设左下角的三角矩阵为第一个方阵的区域
int* ShareMatrix(int* x,int* y,int len);
//获得共享方阵中第一个方阵的距离
int GetFirstMatrixDistance(int* matrix,int len,int i,int j);
int GetSecondMatrixDistance(int* matrix,int len,int i,int j);
/**
C++标准库对堆的支持
**/
void testCPPLibForHeap();


////////////////////////////
void test_selectKMin();
void test_selectKMin3();
void testShareMatrix();
void testCompress();
void testFindMaxUsingSential();
void testCalcPolynomialValue();
void test();
}
#endif // PEARS_H_INCLUDED
