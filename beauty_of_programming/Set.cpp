/*
* Copyright (c) 2010,东南大学数据库课题组
* All rights reserved.
*
* 文件名称：
* 摘    要：简要描述本文件的内容
*
* 当前版本：1.0
* 作    者：倪力
* 完成日期：2010年
*/
#include "Set.h"
#include "tools.h"
#include "Random.h"

namespace Set {
void testSet(IntSetAbstract* s,int* x,int n) {
    WATCH_TIME_BEGIN;
    for(int i=0; i<n; ++i) {
        s->insert(x[i]);
    }
    WATCH_TIME_END("");


    int len=s->size();
//    printf("%d\n",len);
    int *v=new int[len];
    s->report(v);
    if(len<50) printArray(v,len);
//    bool* flag=new bool[n];
//    memset(flag,false,sizeof(bool)*n);
//    for(int i=0;i<n;++i){
//
//    }

    delete[] v;
}
void test() {
    int n=1000;
    int maxelements=n;
    int maxvalue=n-1;
    int* x=new int[n];
    Random::createRandomPositiveData(x,n);
//    Random::createRandomPositiveData(x,n,123,true);
    if(n<50) printArray(x,n);

    IntSetAbstract* s=NULL;

    IntSetSTL sSTL(maxelements,maxvalue);
    IntSetBitVec sBit(maxelements,maxvalue);
    IntSetArr sArr(maxelements,maxvalue);

    IntSetList sList(maxelements,maxvalue);
    IntSetList2 sList2(maxelements,maxvalue);

    IntSetBST sBST(maxelements,maxvalue);
    IntSetBST2 sBST2(maxelements,maxvalue);

    IntSetBins sBin(maxelements,maxvalue);
    IntSetBins2 sBin2(maxelements,maxvalue);
//
    s=&sSTL;
    testSet(s,x,n);

    s=&sBit;
    testSet(s,x,n);

    s=&sArr;
    testSet(s,x,n);

    s=&sList;
    testSet(s,x,n);

    s=&sList2;
    testSet(s,x,n);

    s=&sBST;
    testSet(s,x,n);
    s=&sBST2;
    testSet(s,x,n);
    s=&sBin;
    testSet(s,x,n);
    s=&sBin2;
    testSet(s,x,n);




    delete[] x;
}
}
