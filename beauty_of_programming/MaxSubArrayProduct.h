#ifndef MAXSUBARRAYPRODUCT_H_INCLUDED
#define MAXSUBARRAYPRODUCT_H_INCLUDED
#include <float.h>
#include <iostream>
#include <cmath>
using namespace std;
/**
给定一个长度为N的整数数组，只允许用乘法，不能用除法，计算任意（N-1）个数的组合乘积中最大的一组
**/

namespace MaxSubArrayProduct {
    /**
    法一
    如果使用除法
    先计算所有的乘积，再比较除以每个元素后的大小(即去除一个元素)
    **/
    double getMaxSubArrayProduct(double *a,int size) {
        if (size<1) {
            throw size;
        }
        double p=a[0];
        for (int i=1;i<size;i++) {
            p*=a[i];
        }
        double t=p/a[0];

        for (int i=1;i<size;i++) {
            double s=p/a[i];
            if (s>t) {
                t=s;
            }
        }
        return t;
    }
    /**
    法二
    s表示前i个元素的积,即下标代表元素的个数，从1开始，比原始数组大1
    t表示后N-i个元素的积
    分成三部分
    s    a[i]   t
    **/
    double getMaxSubArrayProduct2(double *a,int size) {
        if (size<1) {
            throw size;
        }
        double* s=new double[size+1];
        double* t=new double[size+1];
        s[0]=1;
        t[size]=1;
        for (int i=1,j=size-1;i<=size;i++,j--) {
            s[i]=s[i-1]*a[i-1];
            t[j]=t[j+1]*a[j];
        }
        double max=s[size-1],p;
        for (int i=1;i<=size;i++) {
            p=s[i-1]*t[i];
            if (p>max) {
                max=p;
            }

        }
        return max;
    }
    /**
    法三
    分析
    其实，还可以通过分析，进一步减少解答问题的计算量。假设 N 个整数的
    乘积为 P，针对 P 的正负性进行如下分析（其中，AN-1 表示 N-1 个数的组合，PN-1
    表示 N-1 个数的组合的乘积）：
    1.  P为0
    那么，数组中至少包含有一个0。假设除去一个0之外，其他N-1个数的乘
    积为Q，根据Q的正负性进行讨论：
    Q为0
    说明数组中至少有两个0，那么N-1个数的乘积只能为0，返回0；
    Q为正数
    返回Q，因为如果以0替换此时AN-1中的任一个数，所得到的PN-1为0，必然
    小于Q；
    Q为负数
    如果以0替换此时AN-1中的任一个数，所得到的PN-1为0，大于Q，乘积最大
    值为0。
    2.  P为负数
    根据“负负得正”的乘法性质，自然想到从N个整数中去掉一个负数，使得
    PN-1为一个正数。而要使这个正数最大，这个被去掉的负数的绝对值必须
    是数组中最小的。我们只需要扫描一遍数组，把绝对值最小的负数给去掉
    就可以了。
    3.  P为正数
    类似P为负数的情况，应该去掉一个绝对值最小的正数值，这样得到的PN-1
    就是最大的。
    上面的解法采用了直接求 N个整数的乘积P，进而判断 P的正负性的办法，
    但是直接求乘积在编译环境下往往会有溢出的危险 （这也就是本题要求不使用除
    法的潜在用意☺），事实上可做一个小的转变，不需要直接求乘积，而是求出数
    组中正数（+）、负数（-）和 0 的个数，从而判断 P 的正负性，其余部分与以
    上面的解法相同。
    在时间复杂度方面，由于只需要遍历数组一次，在遍历数组的同时就可得
    到数组中正数（+）、负数（-）和 0 的个数，以及数组中绝对值最小的正数和
    负数，时间复杂度为 O（N）。


    **/
    double getMaxSubArrayProduct3(double *a,int size) {
        if (size<1) {
            throw size;
        }
        double p=a[0];
        int countZero=0;
        //保存0最后一次出现的位置
        int lastZeroPos=-1;
        double minPositive=DBL_MAX;
        //存正绝对值
        //!DBL_MIN不是最小的负数，而是绝对值最小的正数
        double minAbsoluteNegative=DBL_MAX;
        //
        //
        if(a[0]==0){
            countZero=1;
            lastZeroPos=0;
        }else if(a[0]<0){
            minAbsoluteNegative=fabs(a[0]);

        }else{
            minPositive=a[0];
        }

        for (int i=1;i<size;i++) {
            p*=a[i];
            //下面是统计相关数据
            if (a[i]==0) {
                countZero++;
                lastZeroPos=i;
            } else if (a[i]<0) {
                if (fabs(a[i])<minAbsoluteNegative) {
                    minAbsoluteNegative=fabs(a[i]);
                    cout<<"max="<<minAbsoluteNegative<<endl;
                }
            } else {
                if (a[i]<minPositive) {
                    minPositive=a[i];
                    cout<<"min="<<minPositive<<endl;
                }
            }
        }

        //1 总乘积为0
        if (p==0) {
            //Q=0
            if (countZero>1) {
                return 0;
            }
            //下面是只有一个0的情况
            double q=1;
            int i;
            for(i=0;i<lastZeroPos;i++){
                q*=a[i];
            }
            for(i++;i<size;i++){
                q*=a[i];
            }
            //Q>0
//            if(q>0){
//                return q;
//            }
            //Q<0
//            return 0;
            return q>0?q:0;


        }else if(p<0){//p<0
            return -p/minAbsoluteNegative;
        }
        //p>0

        return p/minPositive;

    }


    void testMaxSubArrayProduct() {
        //显然，含有1，所以最大乘积为所有元素的积
        //27090
        double a[]={2,5,43,1,7,9};
        double a2[]={2,-5,43,1,-7,-9};
        printf("%f\n",getMaxSubArrayProduct(a,6));
        printf("%f\n",getMaxSubArrayProduct2(a2,6));
        printf("%f\n",getMaxSubArrayProduct3(a2,6));

    }
};


#endif // MAXSUBARRAYPRODUCT_H_INCLUDED
