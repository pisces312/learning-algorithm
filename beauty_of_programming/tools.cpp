#include "tools.h"
int FindMaxInteger(int* x,int n){
    int s=INT_MIN;
    for(int i=0;i<n;++i){
        if(x[i]>s){
            s=x[i];
        }
    }
    return s;

}
bool isSorted(int* x,int n) {
    for (int i = 1; i < n; i++)
        if (x[i-1] > x[i])
            return false;
    return true;
}
void printArray(int* keys,int n,int limits) {
    printArrayRange(keys,0,n-1,limits);
}
void printArrayRange(int* keys,int b,int e,int limits) {
    if(limits>0) {
        e=limits+b-1;
    }
    for(int i=b; i<=e; ++i) {
        printf("%d ",keys[i]);
    }
    printf("\n");
}
void createSeqArray(int* keys,int n) {
    for(int i=0; i<n; ++i) {
        keys[i]=i;
    }
}

void swap(int* x,int i, int j, int k) { /* swap x[i..i+k-1] with x[j..j+k-1] */
    int t;
    while (k-- > 0) {
        swap(x,i,j);
        i++;
        j++;
    }

}
void reverse(int* arr,int b,int e) {
    for (; b<e; b++,e--) {
        std::swap(arr[b],arr[e]);
    }

}
int GetSumOfEveryDigit(int a) {
    int sum=0;
    while(a) {
        sum+=(a%10);
        a/=10;
    }
    return 0;
}
void printMatrix(int* x,int row,int col) {
    for(int i=0; i<row; ++i) {
        for(int j=0; j<col; ++j) {
            printf("%d ",*(x+i*row+j));
        }
        printf("\n");
    }
}
