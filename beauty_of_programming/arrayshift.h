#ifndef ARRAYSHIFT_H_INCLUDED
#define ARRAYSHIFT_H_INCLUDED
#include "tools.h"
namespace ArrayShift {

typedef void (*ArrayShiftAlg)(int*,int,int);
#define testArrayShift(F,A) \
{memcpy(keys,org,sizeof(int)*n);\
    WATCH_TIME_BEGIN\
    (F)(keys,n,k);\
    WATCH_TIME_END(#F)\
    if(n<50) printArray(keys,n);\
    assertArrayShift(org,n,k,keys,(A));\
    printf("-----------------------%s pass!\n",#F);}

//验证
void assertArrayShift(int* org,int n,int k,int* keys,bool isRight=true) {
    if(isRight) {
        for(int i=0; i<n; ++i) {
            assert(keys[(i+k)%n]==org[i]);
        }
    } else {
        for(int i=0; i<n; ++i) {
            assert(keys[(i+n-k)%n]==org[i]);
        }
    }

}
/* Alg 2: Juggling (dolphin) rotation */

int gcd(int i, int j) {
    int t;
    while (i != 0) {
        if (j >= i)
            j -= i;
        else {
            t = i;
            i = j;
            j = t;
        }
    }
    return j;
}

///////////////////////////////////////////////////////
/**
基本方法
速度很慢
**/
void arrayRightShiftBasic(int *arr,int N,int K) {
    //循环的位数大于数组长度，则取余
    K%=N;
    while (K--) {
        int t=arr[N-1];
        for (int i=N-1; i>0; i--)
            arr[i]=arr[i-1];
        arr[0]=t;
    }
}
void arrayLeftShiftBasicImp(int *x,int n,int k) {
    int t;
    int nextPos;
    int j;
    //k可能是n的数倍，不能采用减法，只能用取模的方法
    for(int i=0; i<k; ++i) {
        t=x[i];
        nextPos=i;
        do {
            j=nextPos;
            nextPos=(j+k)%n;
            x[j]=x[nextPos];
        } while(i!=nextPos);
        x[j]=t;
    }
}
/////////////////////////////////////////
/**
ab=(b'a')'
abc=(c'(ab)')'=(c'b'a')'
右移K位的过程就是把数组的两部分（移位时中间间隔的位置）交换一下
e.g.
abcd1234
1. 逆序排列abcd abcd1234->dcba1234
2. 逆序排列1234 dcba1234->dcba4321
3. 全部逆序dcba4321->1234abcd
**/
void arrayRightShiftReverse(int *arr,int N,int K) {
    K%=N;
    reverse(arr,0,N-K-1);
    reverse(arr,N-K,N-1);
    reverse(arr,0,N-1);
}
//??
//结合求最大公约数的结构
void arrayLeftShiftGCDReverse(int* x, int n,int rotdist)
{
	if (rotdist == 0 || rotdist == n)
		return;
    int p;
	int i = p = rotdist;
	int j = n - p;
	while (i != j) {
		/* invariant:
			x[0  ..p-i  ] is in final position
			x[p-i..p-1  ] = a (to be swapped with b)
			x[p  ..p+j-1] = b (to be swapped with a)
			x[p+j..n-1  ] in final position
		*/
		if (i > j) {
			swap(x,p-i, p, j);
			i -= j;
		} else {
			swap(x,p-i, p+j-i, i);
			j -= i;
		}
	}
	swap(x,p-i, p, i);
}
////////////////////////////////////////////////////

//杂技算法，取最大公约数
//用减法代替取模
void arrayJuggleLeftShiftUsingMinus(int *x,int n,int rotdist) {
    int j, k, t;
    int cycles = gcd(rotdist, n);
    for (int i = 0; i < cycles; i++) {
        /* move i-th values of blocks */
        t = x[i];
        j = i;
        for (;;) {
            k = j + rotdist;
            if (k >= n)
                k -= n;
            if (k == i)
                break;
            x[j] = x[k];
            j = k;
        }
        x[j] = t;
    }
}

void arrayJuggleLeftShiftUsingMod(int *x,int n,int rotdist) {
    int j, k, t;
    int cycles = gcd(rotdist, n);
    for (int i = 0; i < cycles; i++) {
        t = x[i];
        j = i;
        for (;;) {
            k = (j + rotdist) % n;
            if (k == i)
                break;
            x[j] = x[k];
            j = k;
        }
        x[j] = t;
    }
}
void testSuite(int n,int k) {
    int* keys=new int[n];
    int* org=new int[n];
    createSeqArray(keys,n);
    memcpy(org,keys,sizeof(int)*n);
    if(n<50) printArray(keys,n);
    /**
    第一类方法：移动的方法
    **/
    //最基本方法
    testArrayShift(arrayRightShiftBasic,true);
    ////优化：采用步长长度移动
    testArrayShift(arrayLeftShiftBasicImp,false);
    //使用最大公约数的杂技算法
    testArrayShift(arrayJuggleLeftShiftUsingMod,false);
    ////优化：使用减法
    testArrayShift(arrayJuggleLeftShiftUsingMinus,false);


    /**
        第二类方法：分治的方法
        **/
    testArrayShift(arrayRightShiftReverse,true);
    //采用gcd
    testArrayShift(arrayLeftShiftGCDReverse,false);



    delete[] keys;
    delete[] org;
}
};
#endif // ARRAYSHIFT_H_INCLUDED
