#include<windows.h>
#include "tools.h"
#include "chinesechess.h"
#include "CPU使用率.h"
#include "piesort.h"
#include "findfault.h"
#include "maxgcd.h"
#include "fraction.h"
#include "understand.h"
#include "fractorial.h"
#include "MaxSubArraySum.h"
#include "MaxSubArraySumTwoD.h"
#include "arrayshift.h"
#include "FindMaxInQueue.h"
#include "RemoveNodeFromList.h"
#include "JudgeListIntersection.h"
#include "MaxSubArrayProduct.h"
#include "PointInTriangle.h"
#include "FindTwoNum.h"
#include "BinarySearch.h"
#include "Sort.h"
#include "stringalg.h"
#include "MaxCommonSequence.h"
#include "pears.h"
#include "Random.h"
#include "Set.h"
#include "PriorityQueue.h"
#include "MaxLenSeq.h"
using namespace std;
int fn1() {
//    printf( "next.\n" );
    return 0;
}

//有一组数字，从1到n，中减少了一个数，顺序也被打乱，放在一个n-1的数组里
//请找出丢失的数字，最好能有程序，最好算法比较快
int test1() {
    const int N=10;
    int a[N-1]= {3,9,4,5,2,6,1,8,10}, missing=N;
    for(int i=1; i<N; i++)missing+=i-a[i-1];//1+2+…+n-数组中所有的
    printf("Missing Number: %d\n", missing);

    return 0;
}

/*
有一个整数n
写一个函数f(n)，返回0到n之间出现的"1"的个数。
比如f(13)=6,现在f(1)=1, 问最大的f(n)=n的n是什么？为什么
*/
//递归版本
int f(int n) {
    if(n==0) return 0;
    if (n<10) return 1;
//得到位数
    int d = log10(n);
    int x=pow((double)10,d);
    if (n == 10*x-1) return (d+1)*x;
//取最高位的数
    int a = n / x;;
//去掉最高位后的数
    int y = n - x*a;
//根据最高位的数的值求解
    if (a==1)
        return f(x-1) + 1 + y + f(y);

    return a*f(x-1) + x + f(y);

}
void testF() {
    printf("%f\n",log10(1));
    printf("%f\n",log10(10));
    printf("%d\n",f(10));
    printf("%d\n",f(100));
    printf("%d\n",f(11));
}
///////////////////////////////////////////////////////
/**
给一个n长的数组，判断它是否为一个1, 2, ..., n的全排列，要求在线形时间，常数空间内实现.
**/
// This c++ function tests if the numbers stored in a[0..len-1] is a permutation of 1..len and guarantees infected array does not change after invocation.
bool testPerm (int *a, int len) {
    int i, p;
    bool result = true;
//非正数的情况直接为false
    for (i = 0; i < len; ++i) {
        if (a[i] <= 0) return false;
    }
    for (i = 0; i < len; ++i) {
        if (a[i] > 0) {
            p = i;
            while (a[p] >= 1 && a[p] <= len) {
                a[p] = -a[p];// 每次检查完一个数，就将它置负
                p = -a[p]-1;
            }
            //跳出则表示一个环的结束
            if (p != i) break;
        }
    }
    for (i = 0; i < len; ++i) {
        if (a[i] > 0)
            result = false;
        else
            a[i] = -a[i];// 修改原来的数组，再修改回来，这样就不影响了
    }
    return result;
}
void testTestPerm() {
    int a[5]= {2,5,4,1,3};
    printf("%d\n",testPerm(a,5));

    int b[5]= {2,5,4,1,1};
    printf("%d\n",testPerm(b,5));
}
void testStrangeString(){
    string s("hello");
    //s.push_back('\0');
    s.push_back('\0');
    //or s.insert(s.end(),'\0');
    s+="fucing world!";
    cout<<s<<endl;
    const char* cs = s.c_str();
}
///////////////////////////////////////////////////
/**
insertSortWithBisearch  n=100000        clicks=6146     time=6.146e+009ns
insertSort3     n=100000        clicks=7781     time=7.781e+009ns
insertSort1     n=100000        clicks=35323    time=3.5323e+010ns
**/
int main() {
   // Sort::testSort(insertSort1);
    //Sort::testAlgSelectSort();
//    using namespace Random;
    Sort::testAllSort();
    //Sort::testForShellSortSeq();
//    Sort::testAlgSelectSort();

//    String::testKMP();
//    struct st{
//    int i;
//    short s;
//    char c;
//    };
//    printf("%d\n",sizeof(st));
//    MaxLenSeq::test();
//    ProgrammingPears::test_selectKMin3();
//    Search::test2();
//    MaxCommonSequence::testFindMaxIncreaseDecreaseSeq();
//    MaxCommonSequence::test();
//    testTestPerm();


//    test1();
//    String::testRemoveDupCh();

//    char* a=new char[1];
//    memset(a,0,1000);

    //printf("%s\n",cs);
    /*
    for(int i=0;i<s.size();i++)
    {
    	cout<<((int)cs[i])<<" ";
    	cout<<endl;
    }
    */


//    _onexit( fn1 );

//    strcpy(a,"abc");
//    printf("%s\n",a);
//    int a;
//    int *p=&a;
//    delete p;
//    int* a=new int;
//    delete a;
//    delete a;

//    String::testRemoveNum();
//    ProgrammingPears::testCPPLibForHeap();
//    Search::testbinarySearchUsingHeapArray();

//    using namespace PriorityQueue;
//    const int	n = 10;
//    int	i, v[n];
//    if (1) { // Generate and sort
//        for (i = 0; i < n; i++)
//            v[i] = n-i;
//        pqsort(v, n);
//        for (i = 0; i < n; i++)
//            cout << v[i] << "\n";
//    } else { // Insert integers; extract with 0
//        priqueue<int> pq(100);
//        while (cin >> i)
//            if (i == 0)
//                cout << pq.extractmin() << "\n";
//            else
//                pq.insert(i);
//    }
//    PriorityQueue::pqsort();
//    PriorityQueue::test();
//    Set::test();
//    Random::test();
//    ProgrammingPears::testShareMatrix();
//    ProgrammingPears::testCompress();
//    ProgrammingPears::testCalcPolynomialValue();
//    ProgrammingPears::test();
//    GCD::test();
//    MaxCommonSequence::test();
//    String::test();
//    Sort::testAllSort();
//    Sort::testCountingSort();
//    Search::test();
//    FindTwoNum::test();
//    printf("%d\n",INT_MIN);
//    MaxSubArraySum::testFindFixedLenSubArraySumCloseToNum();
//MaxSubArraySum::testFindSubArraySumCloseToNum();
//    MaxSubArraySum::testSuite(10);
//    ArrayShift::testSuite(10000,2000);
//    ArrayShift::testSuite(10,2);
//    PointInTriangle::testPointInTriangle();
//    MaxSubArrayProduct::testMaxSubArrayProduct();
//    JudgeListIntersection::testJudgeListIntersection();
//    RemoveNodeFromList::testRemoveNodeFromList();
//    testPriorityQueue();
//    testArrayShift();
//    testMaxSubArraySum2D();
//    testMaxSubArraySum();
//    testCountZero();
//    testFindTheNumber();
//    testFraction();
//    testGCD();
    //
//    int array[]={2,5,7,3,9};
//    int size=5;
//    divArray(array,size);
    //
    //
//    testPieSort();
//    chessMethod1();
//    testFindFault();

//chessMethod3();
//chessMethod4();
//chessMethod2();
//    WaitForSingleObject(hThread,100000);
//    testThread();

//    cpuSineRateEx(cosFun,1,0,0.05);
//    cpuSineRateEx(sinFun,1,0,0.05);
//    cpuSineRate();
// getCPUTickCount();
//test();

//    cpuUsageRateMethod1(0.1);
//    cpuUsageRateMethod2(0.2);
//    cout << "Hello world!" << endl;
    return 0;
}
//inline size_t getCPUTickCount(){
//    asm{
//        rdtsc;
//    }
//}

long /*WINAPI*/  MyThreadProc(LPVOID pParam) {
//    ofstream out("hello.txt";
//    out<<"It is now in MyThreadProc!"<<endl;
    cout<<"hello,world"<<endl;
    return 1;
}
void testThread() {
//CreateThread(
//    int i;
    unsigned long * p=NULL;
    HANDLE hThread=CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)MyThreadProc,NULL,0,p);
    //为了显示
    WaitForSingleObject(hThread,1000);
}
/**
返回一个数组中所有元素被第一个元素除的结果
!循环的次序，一定是从后向前，否则，第一个元素在第一次计算后就变为1！！！
**/
void divArray(int * pArray,int size) {
    for (int i=size-1; i>=0; i--) {
        pArray[i]/=pArray[0];
        cout<<pArray[i]<<endl;
    }
}



//}
