/*
* Copyright (c) 2010,东南大学数据库课题组
* All rights reserved.
*
* 文件名称：
* 文件标识：见配置管理计划书
* 摘    要：简要描述本文件的内容
*
* 当前版本：1.0
* 作    者：倪力
* 完成日期：
*/
#include "stringalg.h"
//#include <string.h>
#include <stdio.h>
#include "tools.h"
#include <algorithm>
namespace String {
void fail(const char* pat,int patLen,int* f) {
    f[0]=-1;
    for(int j=1; j<patLen; ++j) {//[0...j]中的最大k，前后不重叠、交叉
        int i=f[j-1];//i在前，j在后
        while(i>=0&&pat[i+1]!=pat[j]) i=f[i];//不断向两边扩展，前后匹配的长度越来越小
        if(pat[j]==pat[i+1])
            f[j]=i+1;//i=f[j-1]
        else
            f[j]=-1;
//        while((i>=0)&&((*(pat+j)!=*(pat+i+1)))) i=f[i];
//        if(*(pat+j)==*(pat+i+1))
//            f[j]=i+1;
//        else
//            f[j]=-1;
    }
//    for(int i=0;i<patLen;++i){
//        printf("%d ",f[i]);
//    }
//    printf("\n");
}
int kmp(const char* pat,const int patLen,const char* s,const int sLen) {
    if(patLen<=0||sLen<=0) {
        return -1;
    }
    int *f=new int[patLen];
    fail(pat,patLen,f);

    int i=0,j=0;
    while(i<sLen&&j<patLen) {
        if(s[i]==pat[j]) {
            ++i;
            ++j;
        } else {
            if(j==0)
                ++i;
            else
                j=f[j-1]+1;
        }
    }
    if(j<patLen) {//没有找到
        return -1;
    }
    delete[] f;
    return i-patLen;
}
/* trim:  remove trailing blanks, tabs, newlines */
int trim(char s[]) {
    int n;

    for (n = strlen(s)-1; n >= 0; n--)
        if (s[n] != ' ' && s[n] != '\t' && s[n] != '\n')
            break;
    s[n+1] = '\0';
    return n;
}
/* reverse:  reverse string s in place */
void reverse(char s[]) {
    int c, i, j;

    for (i = 0, j = strlen(s)-1; i < j; i++, j--) {
        //swap前后交换，c为临时变量
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}
//将整型转换为字符串
/* itoa:  convert n to characters in s */
void itoa(int n, char s[]) {
    int i, sign;

    if ((sign = n) < 0)  /* record sign */
        n = -n;          /* make n positive */
    i = 0;
    do {      /* generate digits in reverse order */
        s[i++] = n % 10 + '0';  /* get next digit */
    } while ((n /= 10) > 0);    /* delete it */
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);//最后翻转
}
/*  Stores a string representation of integer n
    in s[], using a numerical base of b. Will handle
    up to base-36 before we run out of digits to use.
    最多支持36进制,b为进制
     */

void itoa2(int n, char s[], int b) {
    static char digits[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int i, sign;

    if ( b < 2 || b > 36 ) {
        fprintf(stderr, "EX3_5: Cannot support base %d\n", b);
        return;
//        exit(-1);
    }

    if ((sign = n) < 0)
        n = -n;
    i = 0;
    do {
        s[i++] = digits[n % b];
    } while ((n /= b) > 0);
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);
}
/* atoi:  convert s to integer */
int atoi(char s[]) {
    int n=0;
    for (int i = 0; s[i] >= '0' && s[i] <= '9'; ++i)
        n = 10 * n + (s[i] - '0');
    return n;
}
/* squeeze:  delete all c from s
从数组中删除指定字符
仅使用了原始的空间，没有使用新数组
*/
void squeeze(char s[], char c) {
    int i, j;

    for (i = j = 0; s[i] != '\0'; i++)
        if (s[i] != c)
            s[j++] = s[i];
    s[j] = '\0';
}
/* strcat:  concatenate t to end of s; s must be big enough */
void strcat(char s[], char t[]) {
    int i, j;

    i = j = 0;
    while (s[i] != '\0') /* find end of s */
        i++;
    while ((s[i++] = t[j++]) != '\0') /* copy t */
        ;
}
char * __cdecl strcpy(char * dst, const char * src) {
    char * cp = dst;
    while( *cp++ = *src++ )
        ;/* Copy src over dst */
    return( dst );
}

char * __cdecl strcat (char * dst, const char * src) {
    char * cp = dst;
    while( *cp )
        cp++; /* find end of dst */
    while( *cp++ = *src++ ) ; /* Copy src to end of dst */
    return( dst ); /* return dst */
}
int strlen(const char *s) { /* added by RJH; source: K&R p99 */
    int n;

    for (n = 0; *s != '\0'; s++) {
        n++;
    }
    return n;
}
int commonlen(char *p, char *q) {
    int i = 0;
    while (*p && (*p++ == *q++))
        ++i;
    return i;
}
int strcmp1(const char *a,const char *b) {
    while(*a && *b && *a==*b) {//找共同的前缀
        ++a;
        ++b;
    }
    if(*a && *b)//都没有到头
        return (*a-*b);
    else if(*a && '\0'==b)//b已经到头,a大
        return 1;
    else if(*b && '\0'==*a)
        return -1;
    return 0;
}
int strcmp2(const char *s, const char *t) { /* added by RJH; source: K&R p106 */
    for (; *s == *t; s++, t++)
        if (*s == '\0')
            return 0;
    return *s - *t;
}
int strcmp3(const char *dest, const char *source) {
    assert((NULL != dest) && (NULL != source));
    while (*dest && *source && (*dest == *source)) {
        ++dest;
        ++source;
    }
    return *dest - *source;
    /*如果dest > source,则返回值大于0，如果dest = source,则返回值等于0，如果dest < source ,则返回值小于0。*/
}
int pstrcmp(char **p, char **q) {
    return strcmp(*p, *q);
}
int pstrcmpWrapper(const void* p,const void* q) {
    return strcmp(*(const char**)p, *(const char**)q);
}
/**
    判断t是否在s的末尾
    **/
int strend(char *s, char *t) {
    int Result = 0;
    int s_length = 0;
    int t_length = 0;

    /* get the lengths of the strings */
    s_length = strlen(s);
    t_length = strlen(t);

    /*
    t在s的末尾，则t的大小一定小于等于s
    check if the lengths mean that the string t could fit at the string s */
    if (t_length <= s_length) {
        /* advance the s pointer to where the string t would have to start in string s */
        s += s_length - t_length;

        /* and make the compare using strcmp */
        if (0 == strcmp(s, t)) {
            Result = 1;
        }
    }

    return Result;
}
//
/**
赋值ct中的n个字符赋值到s所指向的字符数组中
**/
char *liw_strncpy(char *s, const char *ct, size_t n) {
    char *p;

    p = s;
    for (; n > 0 && *ct != '\0'; --n)
        *p++ = *ct++;
    for (; n > 0; --n)
        *p++ = '\0';
    return s;
}
/**
连接ct中的n个字符
**/
char *liw_strncat(char *s, const char *ct, size_t n) {
    char *p;

    p = s;
    while (*p != '\0')
        ++p;
    for (; n > 0 && *ct != '\0'; --n)
        *p++ = *ct++;
    *p = '\0';
    return s;
}

int liw_strncmp(const char *cs, const char *ct, size_t n) {
    while (n > 0 && *cs == *ct && *cs != '\0') {
        ++cs;
        ++ct;
        --n;
    }
    if (n == 0 || *cs == *ct)
        return 0;
    if (*(unsigned char *) cs < *(unsigned char *) ct)
        return -1;
    return 1;
}




////////////////////////////////////////////////////////
//求最大子字符串
void MaxCommonSubString(const char *a,const char* b,char* r) {
    int lenA=strlen(a),lenB=strlen(b);
    const char *pA,*pB,*pA2,*pB2;
    const char* pAEnd=a+lenA;
    const char* pBEnd=b+lenB;
    const char* pCommon=NULL;
    int maxLen=0;
    int count;
    for(pA=a; pA<pAEnd-maxLen; ++pA) {
        for(pB=b; pB<pBEnd-maxLen; ++pB) {
            if(*pB==*pA) {
                pA2=pA+1;
                pB2=pB+1;
            }
            //至少有一个字符相同，统计相同的个数
            for(count=1; (pA2<pAEnd)&&(pB2<pBEnd)&&(*pA2==*pB2); ++count,++pA2,++pB2);
            if(count>maxLen) {
                maxLen=count;
                pCommon=pA;
            }


        }
    }
    memcpy(r,pCommon,maxLen);
    *(r+maxLen)='\0';
}
void MaxCommonSubStringSuffixArray(const char *a,const char* b,char* r) {
    int i, n = 0, maxi=0,maxlen = 0;
    char* aEnd;
//    int lenA=strlen(a);
//    PRINT_INT(lenA);
//    int lenB=strlen(b);
    int len=strlen(a)+strlen(b)+2;

    char* c=new char[len];
    i=0;
//    printf("%s\n",a);
//    printf("%c\n",a[11]);
    while(*a) {
        c[i++]=*a++;
    }
    aEnd=c+i;
    c[i++]='\0';
//    do{
//        c[i++]=*a++;
//    }while(*a);
//    printf("%s\n",c);
    while(*b) {
        c[i++]=*b++;
    }
    c[i++]='\0';
//    do{
//        c[i++]=*b++;
//    }while(*b);
//    printf("%s\n",c);
//    for(i=0; i<len; ++i) {
//        printf("%c",c[i]);
//    }
//    printf("\n");
//for(i=0; i<len; ++i) {
//        printf("%s\n",p[i]);
//    }

    char** p=new char*[len];

    i=0;
    while(i<len) {
        p[i++]=c++;
    }
    std::qsort(p, len, sizeof(char*), pstrcmpWrapper);

//    for(i=0; i<len; ++i) {
//        printf("%s\n",p[i]);
//    }


    for (i = 1; i < len; i++) {
        if((p[i-1]<aEnd&&p[i]>aEnd)||(p[i-1]>aEnd&&p[i]<aEnd)) {//考虑两种情况
            if ((n=commonlen(p[i-1], p[i])) > maxlen) {
                maxlen = n;
                maxi = i;
            }
        }
    }
//
    if(maxlen)
        memcpy(r,p[maxi],maxlen);
    *(r+maxlen)='\0';


    delete[] p;
    delete[] c;

}

void FindMaxDupSubString(const char* a,char *r) {
    int i, j, n = 0, maxi,maxj, maxlen = -1;
    int len=strlen(a);
    for(i=0; i<len; ++i) {
        for(j=i+1; j<len; ++j) {
            if((n=commonlen(const_cast<char*>(a+i),const_cast<char*>(a+j)))>maxlen) {
                maxlen=n;
                maxi=i;
                maxj=j;
            }
        }
    }
    memcpy(r,a+maxi,maxlen);
    *(r+maxlen)='\0';
//    qsort(a, n, sizeof(char *), pstrcmp);
//    for (i = 0; i < n-M; i++)
//        if (comlen(a[i], a[i+M]) > maxlen) {
//            maxlen = comlen(a[i], a[i+M]);
//            maxi = i;
//        }
}

void FindMaxDupSubStringSuffixArray(const char* a,char *r) {
    int i, n = 0, maxi,maxlen = 0;
    const char* pa=a;
//    printf("%s\n",a);
    int len=strlen(a);
    const char** p=new const char*[len];
    i=0;
    while(*pa) {
        p[i++]=pa++;
    }
    std::qsort(const_cast<char**>(p), len, sizeof(const char*), pstrcmpWrapper);

//    for(i=0; i<len; ++i) {
//        printf("%s\n",p[i]);
//    }


    for (i = 1; i < len; i++)
        if ((n=commonlen(const_cast<char*>(p[i-1]), const_cast<char*>(p[i]))) > maxlen) {
            maxlen = n;
            maxi = i;
        }

    memcpy(r,p[maxi],maxlen);
    *(r+maxlen)='\0';


    delete[] p;
}
//do not use extra space
const char* RemoveNumFrom(char* str) {
    char* p=str;//point to en character
    char* q=str;//point to digital character
    while(*p!='\0'&&*q!='\0') {
//        printf("%s\n",str);
        while(*q!='\0'&&(*q<'1'||*q>'9')) {
            ++q;
        }
        if(*q!='\0') {//find a num
            p=q+1;
            while(*p!='\0'&&*p>='1'&&*p<='9') {
                ++p;
            }
            if(*p!='\0') { //find a en
                *q=*p;
                *p='1';//replace by a num
                ++q;
            } else {
                *q='\0';
            }
        }
    }
    return str;
}
const char* RemoveNumFrom2(char* str) {
    if(!str)
        return NULL;
    char* p = str;
    char* q = str-1;
    while(*p) {
        if(*p < '0' || *p> '9') {
            if(q>=str) {
                *q = *p;
                *p = '1';
                q++;
            }
        } else {
            if(q<str) {
                q=p;
            }
        }
        ++p;
    }
    if(q>=str)
        *q='\0';
    return str;
}

const char* RemoveNumFrom3(char* str) {
    if(!str)
        return NULL;
    char* p = str;//以找字母优先，
    char* q = str;//rember digit pos
    while(*p) {
        if(*p < '0' || *p> '9') {//find non-digit
            *q++ = *p;//可能将一个位置的字母赋值到同一个位置，允许
        }
        ++p;
    }
    *q='\0';
    return str;
}

//for en
const char* RemoveDupCh(char *str) {
    if(!str)
        return NULL;
    char table[255]= {};
    char* p = str;
    char* q = str;
    while(*p) {
        if(!table[*p]) {//find non-dup
            *q++ = *p;//可能将一个位置的字母赋值到同一个位置，允许
        }
        ++table[*p];
        ++p;
    }
    *q='\0';
    return str;
}
//use extra space
const char* RemoveNumFrom(const char* str,char* res) {
    const char* src=res;
    const char* p=str;
    while(*p) {
        if(*p<'1'||*p>'9') {
            *res++=*p;
        }
        ++p;
    }
    *res='\0';
    return src;
}
void testRemoveNum() {
    char str[]="afda23jfkad23415gagad";
//    char str[]="4123879124";
//    char str[]="afda23jfkad";
//    printf("%s\n",RemoveNumFrom(str));
    char temp[1024];
//    printf("%s\n",RemoveNumFrom(str,temp));

//printf("%s\n",RemoveNumFrom2(str));

//    printf("%s\n",RemoveNumFrom3(str));
}


void test_ncpy(const char *str) {
    const int MAX_BUF=1024;
    char std_buf[MAX_BUF];
    char liw_buf[MAX_BUF];

    memset(std_buf, 0x42, sizeof(std_buf));
    strncpy(std_buf, str, sizeof(std_buf));

    memset(liw_buf, 0x42, sizeof(liw_buf));
    liw_strncpy(liw_buf, str, sizeof(liw_buf));

    if (memcmp(std_buf, liw_buf, sizeof(std_buf)) != 0) {
        fprintf(stderr, "liw_strncpy failed for <%s>\n", str);
        exit(EXIT_FAILURE);
    }
}

void test_ncat(const char *first, const char *second) {
    const int MAX_BUF=1024;
    char std_buf[MAX_BUF];
    char liw_buf[MAX_BUF];

    memset(std_buf, 0x69, sizeof(std_buf));
    strcpy(std_buf, first);
    strncat(std_buf, second, sizeof(std_buf) - strlen(std_buf) - 1);

    memset(liw_buf, 0x69, sizeof(liw_buf));
    strcpy(liw_buf, first);
    liw_strncat(liw_buf, second, sizeof(liw_buf) - strlen(liw_buf) - 1);

    if (memcmp(std_buf, liw_buf, sizeof(std_buf)) != 0) {
        fprintf(stderr, "liw_strncat failed, <%s> and <%s>\n",
                first, second);
        exit(EXIT_FAILURE);
    }
}

void test_ncmp(const char *first, const char *second) {
    size_t len;
    int std_ret, liw_ret;

    if (strlen(first) < strlen(second))
        len = strlen(second);
    else
        len = strlen(first);
    std_ret = strncmp(first, second, len);
    liw_ret = liw_strncmp(first, second, len);
    if ((std_ret < 0 && liw_ret >= 0) || (std_ret > 0 && liw_ret <= 0) ||
            (std_ret == 0 && liw_ret != 0)) {
        fprintf(stderr, "liw_strncmp failed, <%s> and <%s>\n",
                first, second);
        exit(EXIT_FAILURE);
    }
}

void testNStringOpr() {
    test_ncpy("");
    test_ncpy("a");
    test_ncpy("ab");
    test_ncpy("abcdefghijklmnopqrstuvwxyz");     /* longer than MAX_BUF */

    test_ncat("", "a");
    test_ncat("a", "bc");
    test_ncat("ab", "cde");
    test_ncat("ab", "cdefghijklmnopqrstuvwxyz"); /* longer than MAX_BUF */

    test_ncmp("", "");
    test_ncmp("", "a");
    test_ncmp("a", "a");
    test_ncmp("a", "ab");
    test_ncmp("abc", "ab");

    printf("All tests pass.\n");
}
void testStrcat() {
    char testbuff[128];

    const char *test[] = {
        "",
        "1",
        "12",
        "123",
        "1234"
    };

    size_t numtests = sizeof test / sizeof test[0];
    size_t thistest;
    size_t inner;

    for (thistest = 0; thistest < numtests; thistest++) {
        for (inner = 0; inner < numtests; inner++) {
            strcpy(testbuff, test[thistest]);
            strcat(testbuff, test[inner]);

            printf("[%s] + [%s] = [%s]\n", test[thistest], test[inner], testbuff);
        }
    }

//  return 0;
}
void testStrend() {
    char *s1 = "some really long string.";
    char *s2 = "ng.";
    char *s3 = "ng";

    if (strend(s1, s2)) {
        printf("The string (%s) has (%s) at the end.\n", s1, s2);
    } else {
        printf("The string (%s) doesn't have (%s) at the end.\n", s1, s2);
    }
    if (strend(s1, s3)) {
        printf("The string (%s) has (%s) at the end.\n", s1, s3);
    } else {
        printf("The string (%s) doesn't have (%s) at the end.\n", s1, s3);
    }


}
void testRemoveDupCh() {
    char str[]="aadfasdds";//adfs
    char str1[]="aaaaaaaaaaaaaaaaa";//adfs
    printf("%s\n",RemoveDupCh(str));
    printf("%s\n",RemoveDupCh(str1));

}
void test() {
    char s[100];
    itoa(-100,s);
    printf("%s\n",s);

    const char* a="agadkfjlaew";
    const char* b="fjla";
    char r[100];
//    MaxCommonSubString(a,b,r);
    MaxCommonSubStringSuffixArray(a,b,r);
    printf("%s\n",r);


//    memset(r,0,100);
//    FindMaxDupSubString("abcdefg fdabdbacb",r);
//    FindMaxDupSubStringSuffixArray("abcdefg fdabdbacb",r);
//    printf("%s\n",r);
}

void testKMP(){
    char pat[]="abcda";
    int patLen=sizeof(pat)/sizeof(pat[0])-1;
    char s[]="abjcdabcdkabcdag";
    int sLen=sizeof(s)/sizeof(s[0])-1;
    int ret=kmp(pat,patLen,s,sLen);
    printf("%d\n",ret);
}
}
