#ifndef SET_H_INCLUDED
#define SET_H_INCLUDED

/*
* Copyright (c) 2010,东南大学数据库课题组
* All rights reserved.
*
* 文件名称：
* 摘    要：简要描述本文件的内容
*
* 当前版本：1.0
* 作    者：倪力
* 完成日期：2010年
*/
#include <set>
/**
**/
namespace Set {
using namespace std;
class IntSetAbstract {
//protected:
//    IntSetAbstract(int maxelements, int maxval) { }
public:
    virtual ~IntSetAbstract() {};
    virtual int size()=0;
    virtual void insert(int t)=0;
    //TODO
    virtual bool find(int t) {
        return true;
    }
    virtual void report(int *v)=0;
};
//封装STL中的set类
class IntSetSTL :public IntSetAbstract {
private:
    set<int> S;
public:
    IntSetSTL(int maxelements, int maxval) { }
    ~IntSetSTL() {}
    int size() {
        return S.size();
    }
    void insert(int t) {
        S.insert(t);
    }
    void report(int *v) {
        int j = 0;
        set<int>::iterator i;
        for (i = S.begin(); i != S.end(); ++i)
            v[j++] = *i;
    }
};

//使用位图作为集合
//!只能用于非负整数
class IntSetBitVec :public IntSetAbstract {
private:
    enum { BITSPERWORD = 32, SHIFT = 5, MASK = 0x1F };
    int	n, hi, *x;
    void set(int i)  {
        x[i>>SHIFT] |=  (1<<(i & MASK));
    }
    void clr(int i)  {
        x[i>>SHIFT] &= ~(1<<(i & MASK));
    }
    int  test(int i) {
        return x[i>>SHIFT] &   (1<<(i & MASK));
    }
public:
    IntSetBitVec(int maxelements, int maxval) {
        hi = maxval;
        x = new int[1 + hi/BITSPERWORD];
        for (int i = 0; i < hi; i++)
            clr(i);
        n = 0;
    }
    ~IntSetBitVec() {
        delete[] x;
    }
    int size() {
        return n;
    }
    void insert(int t) {
        if(t<0) {
            return;
        }
        if (test(t))
            return;
        set(t);
        n++;
    }
    void report(int *v) {
        int j=0;
        for (int i = 0; i < hi; i++)
            if (test(i))
                v[j++] = i;
    }
};

//使用数组的实现

class IntSetArr :public IntSetAbstract {
private:
    int	n, *x;
public:
    IntSetArr(int maxelements, int maxval) {
        x = new int[1 + maxelements];/* sentinel at x[n] */
        n=0;
        //哨卫，比最大值要大一！！
        x[0] = maxval+1;//每次插入向后移动
    }
    ~IntSetArr() {
        delete[] x;
    }
    int size() {
        return n;
    }
    //这里是插入排序
    void insert(int t) {
        int i, j;

        for (i = 0; x[i] < t; i++)//这里顺序查找,可以改为二分查找
            ;
        if (x[i] == t)
            return;
        for (j = n; j >= i; j--)//向后移动
            x[j+1] = x[j];
        x[i] = t;
        n++;
    }
    void report(int *v) {
        for (int i = 0; i < n; i++)
            v[i] = x[i];
    }
};

//使用链表的实现
class IntSetList :public IntSetAbstract {
private:
    int	n;
    struct node {
        int val;
        node *next;
        node(int i, node *p) {
            val = i;
            next = p;
        }
    };
    node *head, *sentinel;
    node *rinsert(node *p, int t) {
        if (p->val < t) {
            p->next = rinsert(p->next, t);
        } else if (p->val > t) {
            p = new node(t, p);
            n++;
        }
        return p;
    }
public:
    IntSetList(int maxelements, int maxval) {
        sentinel = head = new node(maxval+1, 0);
        n = 0;
    }
    ~IntSetList() { //TODO
    }
    int size() {
        return n;
    }
    void insert(int t) {
        head = rinsert(head, t);
    }
    void insert2(int t) {
        node *p;
        if (head->val == t)
            return;
        if (head->val > t) {
            head = new node(t, head);
            n++;
            return;
        }
        for (p = head; p->next->val < t; p = p->next)
            ;
        if (p->next->val == t)
            return;
        p->next = new node(t, p->next);
        n++;
    }
    void insert3(int t) {
        node **p;
        for (p = &head; (*p)->val < t; p = &((*p)->next))
            ;
        if ((*p)->val == t)
            return;
        *p = new node(t, *p);
        n++;
    }
    void report(int *v) {
        int j = 0;
        for (node *p = head; p != sentinel; p = p->next)
            v[j++] = p->val;
    }
};

// Change from new per node to one new at init
// Factor of 2.5 on VC 5.0, 6% on SGI CC
class IntSetList2 :public IntSetAbstract {
private:
    int	n;
    struct node {
        int val;
        node *next;
    };
    node *head, *sentinel, *freenode;
public:
    IntSetList2(int maxelements, int maxval) {
        sentinel = head = new node;
        sentinel->val = maxval+1;
        //提前分配节点空间
        freenode = new node[maxelements];
        n = 0;
    }
    ~IntSetList2() { //TODO
    }
    int size() {
        return n;
    }
    void insert(int t) {
        node **p;
        for (p = &head; (*p)->val < t; p = &((*p)->next))
            ;
        if ((*p)->val == t)
            return;
        freenode->val = t;
        freenode->next = *p;
        *p = freenode++;
        n++;
    }
    void report(int *v) {
        int j = 0;
        for (node *p = head; p != sentinel; p = p->next)
            v[j++] = p->val;
    }
};

//使用二分搜索树的实现
class IntSetBST :public IntSetAbstract {
private:
    int	n, *v, vn;
    struct node {
        int val;
        node *left, *right;
        node(int v) {
            val = v;
            left = right = 0;
        }
    };
    node *root;
//    node* sential;
    node *rinsert(node *p, int t) {
        if (p == 0) {
            p = new node(t);
            n++;
        } else if (t < p->val) {
            p->left = rinsert(p->left, t);
        } else if (t > p->val) {
            p->right = rinsert(p->right, t);
        } // do nothing if p->val == t
        return p;
    }
    void traverse(node *p) {
        if (p == 0)
            return;
        traverse(p->left);
        v[vn++] = p->val;
        traverse(p->right);
    }
public:
    IntSetBST(int maxelements, int maxval) {
        root = 0;
        n = 0;
//        sential=new node
    }
    ~IntSetBST() { //TODO
    }
    int size() {
        return n;
    }
    void insert(int t) {
        root = rinsert(root, t);
    }
    void report(int *x) {
        v = x;
        vn = 0;
        traverse(root);
    }
};

//预先分配节点，使用哨卫
class IntSetBST2 :public IntSetAbstract {
private:
    int	n, *v, vn;
    struct node {
        int val;
        node *left, *right;
    };
    node *root, *freenode, *sentinel;
    node *rinsert(node *p, int t) {
        if (p == sentinel) {
            p = freenode++;
            p->val = t;
            p->left = p->right = sentinel;
            n++;
        } else if (t < p->val) {
            p->left = rinsert(p->left, t);
        } else if (t > p->val) {
            p->right = rinsert(p->right, t);
        } // do nothing if p->val == t
        return p;
    }
    void traverse(node *p) {
        if (p == sentinel)
            return;
        traverse(p->left);
        v[vn++] = p->val;
        traverse(p->right);
    }
public:
    IntSetBST2(int maxelements, int maxval) {
        root = sentinel = new node;  // 0 if using insert1
        n = 0;
        freenode = new node[maxelements];//提前分配空间
    }
    ~IntSetBST2() { //TODO
    }
    int size() {
        return n;
    }
//    void insert1(int t) {
//        root = rinsert(root, t);
//    }
    void insert(int t) {
        sentinel->val = t;
        node **p = &root;
        while ((*p)->val != t)
            if (t < (*p)->val)
                p = &((*p)->left);
            else
                p = &((*p)->right);
        if (*p == sentinel) {
            *p = freenode++;
            (*p)->val = t;
            (*p)->left = (*p)->right = sentinel;
            n++;
        }
    }
    void report(int *x) {
        v = x;
        vn = 0;
        traverse(root);
    }
};

//使用箱的实现
class IntSetBins :public IntSetAbstract {
private:
    int	n, bins, maxval;
    struct node {
        int val;
        node *next;
        node(int v, node *p) {
            val = v;
            next = p;
        }
    };
    node **bin, *sentinel;
    node *rinsert(node *p, int t) {
        if (p->val < t) {
            p->next = rinsert(p->next, t);
        } else if (p->val > t) {
            p = new node(t, p);
            n++;
        }
        return p;
    }
public:
    IntSetBins(int maxelements, int pmaxval) {
        bins = maxelements;
        maxval = pmaxval;
        bin = new node*[bins];
        sentinel = new node(maxval+1, 0);
        for (int i = 0; i < bins; i++)
            bin[i] = sentinel;
        n = 0;
    }
    ~IntSetBins() { //TODO
    }
    int size() {
        return n;
    }
    void insert(int t) {
        int i = t / (1 + maxval/bins);  //! CHECK !
        bin[i] = rinsert(bin[i], t);
    }
    void report(int *v) {
        int j = 0;
        for (int i = 0; i < bins; i++)
            for (node *p = bin[i]; p != sentinel; p = p->next)
                v[j++] = p->val;
    }
};

//使用箱的实现
class IntSetBins2:public IntSetAbstract {
private:
    int	n, bins, maxval,binshift;
    struct node {
        int val;
        node *next;
    };
    node **bin, *sentinel, *freenode;
    node *rinsert(node *p, int t) {
        if (p->val < t) {
            p->next = rinsert(p->next, t);
        } else if (p->val > t) {
            freenode->val = t;
            freenode->next = p;
            p = freenode++;
            n++;
        }
        return p;
    }
public:
    IntSetBins2(int maxelements, int pmaxval) {
        maxval = pmaxval;
        binshift=1;
        int goal=maxval/maxelements;
        for(int i=2; i<goal; i<<=1) {
            ++binshift;
        }
        bins = 1+(maxval>>binshift);


        freenode = new node[maxelements];
        bin = new node*[bins];

        sentinel = new node;
        sentinel->val = maxval+1;

        for (int i = 0; i < bins; i++)
            bin[i] = sentinel;
        n = 0;
    }
    ~IntSetBins2() { //TODO
    }
    int size() {
        return n;
    }

    void insert(int t) {
        node **p;
        //除法开销大
//        int i = t / (1 + maxval/bins);
        for (p = &bin[t>>binshift]; (*p)->val < t; p = &((*p)->next))
            ;
        if ((*p)->val == t)
            return;
        freenode->val = t;
        freenode->next = *p;
        *p = freenode++;
        n++;
    }
    void report(int *v) {
        int j = 0;
        for (int i = 0; i < bins; i++)
            for (node *p = bin[i]; p != sentinel; p = p->next)
                v[j++] = p->val;
    }
};

void test();
}
#endif // SET_H_INCLUDED
