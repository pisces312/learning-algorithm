#ifndef MAXSUBARRAYSUM2D_H_INCLUDED
#define MAXSUBARRAYSUM2D_H_INCLUDED
#include <limits.h>
#include <assert.h>
#include <algorithm>
#include <iomanip>
/**
都从1开始
**/
namespace MaxSubArraySumTwoD {
    //逻辑矩形
    const int row=5;
    const int col=5;
    //实际
    //!测试
    int array[row][col]={
//        1,4,6,3,-1,
//        4,9,6,12,0,
//        -5,1,2,7,5,
//        2,8,5,89,3,
//        -74,8,3,6,5
//
//        {0,0,0,0,0,0},
//        {0,1,4,6,3,-1},
//        {0,4,9,6,12,0},
//        {0,-5,1,2,7,5},
//        {0,2,8,5,89,3},
//        {0,-74,8,3,6,5}
        //

        {1,4,6,3,-1},
        {4,9,6,12,0},
        {-5,1,2,7,5},
        {2,8,5,89,3},
        {-74,8,3,6,5}
    };

    //部分和比实际大一圈，边界值，便于计算
    //!计算部分和
    int PS[row+1][col+1];
    void initPS() {
        //边界值，即等于该位置上的元素值
        for (int i=0;i<=row;i++) {
            PS[i][0]=0;//array[i][0];
        }
        for (int i=0;i<=col;i++) {
            PS[0][i]=0;//array[0][i];
        }
        for (int i=1;i<=row;i++) {
            for (int j=1;j<=col;j++) {
                PS[i][j]=PS[i-1][j]+PS[i][j-1]-PS[i-1][j-1]+array[i-1][j-1];
            }
        }
        //!输出原始数组
        //print
        for (int i=0;i<row;i++) {
            for (int j=0;j<col;j++) {
                cout<<setw(6)<<array[i][j]<<" ";;
            }
            cout<<endl;
        }
        cout<<endl;
        //print
        for (int i=0;i<=row;i++) {
            for (int j=0;j<=col;j++) {
                cout<<setw(6)<<PS[i][j]<<" ";;
            }
            cout<<endl;
        }
    }
    //!打印二维数组
    void print2DArray(int *array,int row,int col) {
        int t=0;
        for (int i=0;i<row;i++,t+=row) {
            for (int j=0;j<col;j++) {
                cout<<setw(6)<<array[t+j];
            }
            cout<<endl;
        }
    }
    //!通用的部分和计算
    int * createPartSum(int *array,int row,int col) {
        row++;
        col++;
        int size=row*col;
        int *PS=new int[size];

        //边界值，即等于该位置上的元素值
        for (int i=0;i<size;i+=row) {
            PS[i]=0;
        }
        for (int i=1;i<col;i++) {
            PS[i]=0;//array[0][i];
        }
        int t=row;
        for (int i=1;i<row;i++,t+=row) {

            for (int j=1;j<col;j++) {
                PS[t+j]=PS[t-row+j]+PS[t+j-1]-PS[t-row+j-1]+array[t-row-i+j];
//正确
//array[(i-1)*(row-1)+j-1];

            }
        }
//        //!输出原始数组
//        //print
//        t=0;
//
//        for (int i=0;i<(row-1);i++,t+=(row-1)) {
//            for (int j=0;j<(col-1);j++) {
//                cout<<setw(6)<<array[t+j]<<" ";;
//            }
//            cout<<endl;
//        }
//        cout<<endl;
//        //print
//        t=0;
//        for (int i=0;i<row;i++,t+=row) {
//            for (int j=0;j<col;j++) {
//                cout<<setw(6)<<PS[t+j]<<" ";;
//            }
//            cout<<endl;
//        }
        return PS;
    }


    /**
    计算任意矩形区域的数值和
    基于部分和PS：从左上角算起的矩形区域
    二维数组以一维数组存储（矩形）
    下标
    前两个参数为行
    后两个参数为列
    **/
    int sumOfRect(int i_min,int i_max,int j_min,int j_max) {
//!将二维数组中的坐标转换为ps中的，+1

//        return PS[i_max][j_max]-PS[i_min-1][j_max]-PS[i_max][j_min-1]+PS[i_min-1][j_min-1];
        return PS[i_max+1][j_max+1]-PS[i_min][j_max+1]-PS[i_max+1][j_min]+PS[i_min][j_min];

    }
    /**
    n  行数
    m 列数
    O(N^2 * N^2 * Sum)
    基本方法
    **/
    int maxSubArraySum2DBasic() {
        int minRow=-1,maxRow=-1,minCol=-1,maxCol=-1;
        int max=INT_MIN;
        //x轴上的范围
        for (int i_min=0;i_min<row;i_min++) {
            for (int i_max=0;i_max<row;i_max++) {
                for (int j_min=0;j_min<col;j_min++) {
                    for (int j_max=0;j_max<col;j_max++) {
                        //法一
//                        max=std::max(max,sumOfRect(i_min,i_max,j_min,j_max));
//法二
                        int t=sumOfRect(i_min,i_max,j_min,j_max);
                        if (t>max) {
                            minRow=i_min;
                            maxRow=i_max;
                            minCol=j_min;
                            maxCol=j_max;
                            max=t;
                        }
                    }
                }
            }
        }
        cout<<"("<<minRow<<","<<minCol<<") ("<<maxRow<<","<<maxCol<<")\n";
        return max;
    }
    //
    //
    //
    //!解法二
    //
    //!返回指定范围内一列的数值和
    int BC(int rowBegin,int rowEnd,int col) {
        return sumOfRect(rowBegin,rowEnd,col,col);
//        return PS[rowEnd+1][col+1]

    }
    int maxSubArraySum2DOpt() {
        int max=INT_MIN;
        int colMaxIndex=col-1;
        for (int i=0;i<row;i++) {
            for (int j=0;j<col;j++) {
                int start=BC(i,j,colMaxIndex);
                //!从i开始的
                int all=start;
                for (int k=colMaxIndex-1;k>=0;k--) {
                    if (start<0) {
                        start=0;
                    }
                    start+=BC(i,j,k);
                    all=std::max(start,all);
                }
                max=std::max(max,all);
            }

        }

        return max;

    }
    void testMethod1() {
        //解法一
        MaxSubArraySumTwoD::initPS();
        cout<<MaxSubArraySumTwoD::sumOfRect(0,1,0,1)<<endl;
        //第一行0至2列的矩形和
        cout<<MaxSubArraySumTwoD::sumOfRect(0,0,0,2)<<endl;
        cout<<MaxSubArraySumTwoD::maxSubArraySum2DBasic()<<endl;
    }
    void testMethod2() {
        MaxSubArraySumTwoD::initPS();
        cout<<BC(0,4,0)<<endl;
        cout<<maxSubArraySum2DOpt()<<endl;
    }
    //!自动生成
    void test() {
        int array[]={
            1,4,6,3,-1,
            4,9,6,12,0,
            -5,1,2,7,5,
            2,8,5,89,3,
            -74,8,3,6,5
        };
        int row,col;
        row=col=5;
        int *PS=createPartSum(array,row,col);
        print2DArray(PS,row+1,col+1);
    }

};
void testMaxSubArraySum2D() {

    //解法二
//    int row=5;
//    int col=5;
//    int* array=new int[row*col];
//    MaxSubArraySumTwoD::testMethod2();
    MaxSubArraySumTwoD::test();


}
#endif // MAXSUBARRAYSUM2D_H_INCLUDED
