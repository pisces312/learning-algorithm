#include "Sort.h"
#include "tools.h"
#include "Random.h"
#include <vector>
#include <list>
#include <stack>
namespace Sort {
/////////////////////////////////////////////
#define TEST2(F) {\
 memcpy(keys,org,sizeof(int)*n);\
testCore2((F),keys,n,#F);}

#define testCore2(F,K,N,S) {\
    if((N)<100) printArray((K),(N));\
    int start = clock();\
    (F)((K),(0),((N)-1));\
    int clicks = clock() - start;\
    printf("%s\tn=%d\tclicks=%d\ttime=%gns\n",\
           (S),\
           (N), clicks,\
           1e9*clicks/((float) CLOCKS_PER_SEC));\
    if((N)<100) printArray((K),(N));\
    assertSorted((K),(N));}

//typedef void (*SortFunc)(int* keys,int n);


#define TEST_SHELLSORT(F,H,L) {\
 memcpy(keys,org,sizeof(int)*n);\
testCore3((F),keys,n,H,L,#F);}

#define testCore3(F,K,N,H,L,S) {\
    if((N)<100) printArray((K),(N));\
    int start = clock();\
    (F)((K),(0),(N-1),(H),L);\
    int clicks = clock() - start;\
    printf("%s\tn=%d\tclicks=%d\ttime=%gns\n",\
           (S),\
           (N), clicks,\
           1e9*clicks/((float) CLOCKS_PER_SEC));\
    if((N)<100) printArray((K),(N));\
    assertSorted((K),(N));}


int partition(int q[],int low,int high) {
//    using namespace std;
    int i=low;
    int j=high+1;
    int p=i;
    while(1) {
        while(q[++i]<q[p]);
        while(q[--j]>q[p]);
        if(i>=j) break;
        swap(q,i,j);
//        swap(q[i],q[j]);
    }
    swap(q,j,p);
//    swap(q[j],q[p]);
    return j;
}
//non recursive version
void quickSort(int* x,int n) {
    using namespace std;
    stack<int> s1;
    stack<int>s2;
    int low=0;
    int high=n-1;
    s1.push(low);
    s2.push(high);
    int tl,th,p;
    while(!s1.empty() && !s2.empty()) {
        tl=s1.top();
        th=s2.top();
        s1.pop();
        s2.pop();
        if(tl>=th) continue;
        p=partition(x,tl,th);
        s1.push(tl);
        s1.push(p+1);
        s2.push(p-1);
        s2.push(th);
    }
}

void quickSort2(int* q,int n) {
    int low=0;
    int high=n-1;

    int top=0,i,j,p;
    //
    int c=high-low;

    int stackSize=0;
    while(c) {
        c>>=1;
        ++stackSize;
    }
    stackSize<<=1;
//    printf("%d\n",stackSize);


    int *a=new int[stackSize];
    if(a==NULL) return;
    a[top++]=low;
    a[top++]=high;
    while(top>0) {//[j...p...i]
        i=a[--top];//出栈，第一次就是处理全部
        j=a[--top];
        while(j<i) {
            p=partition(q,j,i);
            if(p-j<i-p) {//后部大于前部时
                //先分割前部,后部进栈
                a[top++]=p+1;
                a[top++]=i;
                i=p-1;
            } else {
                //先分割后部,前部进栈
                a[top++]=j;
                a[top++]=p-1;
                j=p+1;
            }
        }
    }
    delete[] a;
}

/////////////////////////////////////////////////////////////
//在内部随机选择pivot
int partition2(int q[],int low,int high) {
    swap(q,low, Random::randint(low, high));
    int i=low;
    int j=high+1;
    while(1) {
        while(q[++i]<q[low]);
        while(q[--j]>q[low]);
        if(i>=j) break;
        swap(q,i,j);
    }
    swap(q,j,low);
    return j;
}
//消除尾递归
//使栈的深度降到O(lgn)
void qsortFinal2(int* x,int i, int j) {
    int m;
    while(i<j) {
        m=partition2(x,i,j);
        if(m-i<j-m) {
            qsortFinal2(x,i, m);
            i=m+1;
        } else {
            qsortFinal2(x,m+1,j);
            j=m-1;
        }
    }
}





/**
  * 计数排序，用于分布均匀的非负整数排序
  * @param p
  * @param maxValue出现的最大值
  * @return
  */
int * countingSort(int * p,int n,int maxValue) {
    int * q=new int[maxValue+1];//统计
    memset(q,0,sizeof(int)*(maxValue+1));
    int * re=new int[n];//存储结果
    memset(re,0,sizeof(int)*n);
    for(int i=0; i<n; ++i)
        ++q[p[i]];
    for(int i=1; i<=maxValue; ++i)
        q[i]+=q[i-1];
    for(int i=n-1; i>=0; --i) {
        re[q[p[i]]-1]=p[i];
        q[p[i]]--;
    }
    delete[] q;
    return re;
}

/**
Algorithms in C
**/
inline bool less(int a,int b) {
    return a<b;
}
inline void exch(int& a,int& b) {
    int t=a;
    a=b;
    b=t;
}
inline void compexch(int& a,int& b) {
    if(less(b,a))
        exch(a,b);
}
void selectSort2(int* a,int l,int r) {
    int min;
    for(int i=l; i<r; ++i) {
        min=i;
        for(int j=i+1; j<=r; ++j) {
            if(less(a[j],a[min]))
                min=j;

        }
        exch(a[i],a[min]);

    }
}

void insertion(int *a,int l,int r) {
    int i;
    //find smallest first as the sentinel
    for(i=r; i>l; --i) {
        compexch(a[i-1],a[i]);
    }
    //reduce exchange to assignment
    for(i=l+2; i<=r; ++i) {
        int j=i;
        int v=a[i];
        while(less(v,a[j-1])) {
            a[j]=a[j-1];
            --j;
        }
        a[j]=v;
    }
}
/**
Step1
1st line /> the unsorted numbers are here ,
2nd line /> comparing first 2 numbers of the 1st line (from left) and just copy the rest
3rd line /> skip the 1st no of line 2 (from left) then compare the 3rd and 4th numbers
4th line /> skip the 1st & 2nd nos of line 3 (from left) then compare the 4th and 5th numbers
Step2
1st line /> the unsorted numbers are here ,
2nd line /> comparing first 2 numbers of the 1st line (from right) and just copy the rest
3rd line /> skip the 1st no of line 2 (from right) then compare the 3rd and 4th numbers
4th line /> skip the 1st & 2nd nos of line 3 (from right) then compare the 4th and 5th numbers
EX:
numbers : 5 2 3 4 1
* Step1
1L: 5 2 3 4 1
2L: 2 5 3 4 9
3L: 2 3 5 4 1
4L: 2 3 4 5 1
5L: 2 3 4 1 5 < -- step 2 starts with this line
* Step 2
1L: 2 3 4 1 5
2L: 2 3 4 1 5
3L: 2 3 1 4 5
4L: 2 1 3 4 5
5L: 1 2 3 4 5 < -- The Result
if you noticed, The bubles are like this
*S1*
L1: (_ _) _ _ _
L2: _ (_ _) _ _
L3: _ _ (_ _) _
L4: _ _ _ (_ _)
L5: _ _ _ _ _
*S2*
L1: _ _ _ (_ _)
L2: _ _ (_ _) _
L3: _ (_ _) _ _
L4: (_ _) _ _ _
L5: _ _ _ _ _ < THE RESULT
NOTICE : the resault must end in left side .!
**/
void shakerSort(int *items, int count) {
    int i;
    bool exchange;
    do {
        exchange = 0;
        for(i = count - 1; i > 0; --i) {
            if(less(items[i],items[i-1])) {
                exch(items[i],items[i-1]);
                exchange = 1;
            }
        }

        for(i = 1; i < count; ++i) {
            if(less(items[i],items[i-1])) {
                exch(items[i],items[i-1]);
                exchange = 1;
            }
        }
    } while(exchange); /* sort until no exchanges */
}
//typedef int (*ShellSortIncFun)(int);
/**
按M'=3*M+1递归产生
1,4,13,40,121,364...
**/
void shellSort3(int*a ,int l,int r) {
    int i,j,h=1;
    for(; h<=(r-1)/9; h=3*h+1);
    for(; h>0; h/=3) {
        for(i=l+h; i<=r; ++i) {
            int j=i;
            int v=a[i];
            while(j>=l+h&&less(v,a[j-h])) {
                a[j]=a[j-h];
                j-=h;
            }
            a[j]=v;
        }
    }
}
/**
1,8,23,77,281,1073,4193,16577
比kunuth 序列快
**/
void shellSort4(int*a ,int l,int r) {
    int i=0,j;

    const int size=8;
    int h[size]= {1,8,23,77,281,1073,4193,16577};
    int k=size-1;

//    int h[8]={16577,4193,1073,281,77,23,8,1};
    for(int n=r-l+1; h[k]>n; --k);
    //h=pow(4,i+1)+3*pow(2,i)+1;
    //printf("%d\n",h[k]);
    for(; k>=0; --k) {
        for(i=l+h[k]; i<=r; ++i) {
            int j=i;
            int v=a[i];
            while(j>=l+h[k]&&less(v,a[j-h[k]])) {
                a[j]=a[j-h[k]];
                j-=h[k];
            }
            a[j]=v;
        }
    }
}
/**
1,8,23,77,281,1073,4193,16577
**/
void shellSortCustomSeq(int*a ,int l,int r,int* h,int size) {
    int i=0,j;
    int k=size-1;
    for(int n=r-l+1; h[k]>n; --k);
    for(; k>=0; --k) {
        for(i=l+h[k]; i<=r; ++i) {
            int j=i;
            int v=a[i];
            while(j>=l+h[k]&&less(v,a[j-h[k]])) {
                a[j]=a[j-h[k]];
                j-=h[k];
            }
            a[j]=v;
        }
    }
}

void testForShellSortSeq(){
    int n=100000;
    int* keys=new int[n];
    int* org=new int[n];
    Random::createRandomPositiveData(keys,n);
    memcpy(org,keys,sizeof(int)*n);
//    printArray(keys,n);
    ////////////////////////////
    int a[]={1,4,13,40,121,364,1093,3280,9841,29524};
    TEST_SHELLSORT(shellSortCustomSeq,a,10);
    int a2[]={1,8,23,77,281,1073,4193,16577};
    TEST_SHELLSORT(shellSortCustomSeq,a2,8);

    //bad
    int a3[]={1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384};
    TEST_SHELLSORT(shellSortCustomSeq,a3,15);

//最优
    int a4[]={1,2,3,4,6,9,8,12,18,27,16,24,36,54,81,32,48,72,
    108,162,243,64,96,144,216,324,486,729,128,192,288,432,648};
    TEST_SHELLSORT(shellSortCustomSeq,a4,sizeof(a4)/sizeof(int));
}
void testAlgSelectSort() {
    int n=100000;
    int* keys=new int[n];
    int* org=new int[n];
    Random::createRandomPositiveData(keys,n);
    memcpy(org,keys,sizeof(int)*n);
//    printArray(keys,n);
    ////////////////////////////



//    TEST2(selectSort2);
//    TEST2(insertion);
//    TEST(shakerSort);
//    TEST_SHELLSORT(shellSortCustomSeq,)
    TEST2(shellSort3);
    TEST2(shellSort4);
}

void testCountingSort() {
    int a[]= {1,5,3,7,3,5,8,3,8,4,34,100,0};
    int an=sizeof(a)/sizeof(int);
    int maxValue=FindMaxInteger(a,an);
    int *r=countingSort(a,an,maxValue);
    printArray(r,an,0);
    delete[] r;

}
}

