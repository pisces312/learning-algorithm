#ifndef FRACTORIAL_H_INCLUDED
#define FRACTORIAL_H_INCLUDED
#include <assert.h>
/**
1. 给定一个整数N，那么N的阶乘N！末尾有多少个0呢？例如：N＝10，N！＝3 628 800，
N！的末尾有两个0。
2. 求N！的二进制表示中最低位1的位置。
核心思想:
首先考虑，如果 N！=  K×10M，且 K不能被 10 整除，那么 N！末尾有 M个 0。再考虑
对 N！进行质因数分解，N！=（2^x）×（3^y）×（5^z）…，由于 10 = 2×5，所以 M只跟 X和 Z
相关，每一对 2 和5 相乘可以得到一个 10，于是 M = min（X, Z）。不难看出 X大于等于 Z，
因为能被 2整除的数出现的频率比能被 5 整除的数高得多，所以把公式简化为 M = Z。
**/
/**
【问题 1 的解法一】
要计算 Z，最直接的方法，就是计算 i（i =1, 2, …, N）的因式分解中 5 的指数，然后求
和：
**/
namespace CountZero {
    int countZero1(const int& N) {
        int ret = 0;
        for (int i = 1,j; i <= N; i++) {
            j = i;
            while (j % 5 ==0) {
                ret++;
                j /= 5;
            }
        }
        return ret;
    }
    /**
      【问题 1 的解法二】

    !!公式：Z = [N/5] +[N/5^2] +[N/5^3] + …
    （不用担心这会是一个无穷的运算，因为总存在一个 K，使得 5K > N，[N/5K]=0。）
    ！！Z表示N！中质因数为5的个数！！
    //
    !!即N/5计算出至少含有一个5的个数，N/5^2计算出至少含有两个5的个数
    //
    公式中，[N/5]表示不大于 N 的数中5的倍数贡献一个 5，[N/5^2]表示不大于 N 的数中 5^2的倍数再贡献一个 5
    如：25=5*5，要贡献两个5，即还要加上25/5^2=1
    **/
    int countZero2(int N) {
        //这里的N用作临时变量
        int ret = 0;
        while (N) {
            //法一
//            ret += N / 5;
//            N /= 5;
            //
            N /= 5;
            ret += N;

        }
        return ret;
    }
    /**
    问题2的解法
    首先来看一下一个二进制数除以 2的计算过程和结果是怎样的。
    把一个二进制数除以 2，实际过程如下：
    判断最后一个二进制位是否为 0，若为 0，则将此二进制数右移一位，即为商值（为什
    么）；反之，若为 1，则说明这个二进制数是奇数，无法被 2 整除（这又是为什么）。
    所以，这个问题实际上等同于求 N！含有质因数 2 的个数。即答案等于 N！含有质因数
    2 的个数加1。
    !!即存在2的质因数说明可以被2整除，即说明整除前是偶数，二进制的末尾是0
    **/
    /**
    【问题 2 的解法一】
    由于 N!  中含有质因数 2的个数，等于  N/2 + N/4 + N/8 + N/16 + …1
    !位置从右向左，从0开始
    **/
    int lowestOnePos(int N) {
        int Ret = 0;
        while (N) {
            N >>= 1;
            Ret += N;
        }
        return Ret;
    }
    /**
    计算阶乘N！中的质因数factor的个数
    通用公式
    **/
    int countFractorialPrimeFactor(int N,int factor) {
        int ret = 0;
        while (N) {
            N /= factor;
            ret += N;

        }
        return ret;
    }
    /**
    【问题 2 的解法二】
    N！含有质因数 2 的个数，还等于 N减去 N 的二进制表示中 1 的数目。我们还可以通过
    这个规律来求解。
    下面对这个规律进行举例说明，假设  N = 11011，那么 N!中含有质因数 2的个数为  N/2
    + N/4 + N/8 + N/16 + …
    即：  1101 + 110 + 11 + 1
    =（1000 + 100 + 1）
    +（100 + 10）
    +（10 + 1）
    + 1
    =（1000 + 100+ 10 + 1）+（100 + 10 + 1）+ 1
    = 1111 + 111 + 1
    =（10000 -1）+（1000 - 1）+（10-1）+（1-1）
     = 11011- (N二进制表示中 1 的个数 )
    **/
    //循环的次数等于x中包含的1位的数目
    int countBits(unsigned int x) {
        int   pop = 0;
        while (x) {
            pop++;
            x = x & (x - 1);
        }
        return pop;
    }
    int lowestOnePos2(const int &N) {
        return N-countBits(N);
    }
    /**
    判断是否是2的方幂
    若n是2的方幂，设为1000...
    n-1=01111...
    n&(n-1)=00000...
    得证
    **/
    bool is2Power(int n){
        return n>0&&((n&(n-1))==0);
    }
};
void testCountZero() {
    assert(CountZero::countZero1(10)==2);
    assert(CountZero::countZero2(10)==2);
    //1010
    assert(CountZero::lowestOnePos(3)==1);
    assert(CountZero::countFractorialPrimeFactor(10,5)==2);
    assert(CountZero::lowestOnePos2(3)==1);
    assert(CountZero::is2Power(64));


}
#endif // FRACTORIAL_H_INCLUDED
