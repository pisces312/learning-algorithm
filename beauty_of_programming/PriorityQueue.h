#ifndef PRIORITYQUEUE_H_INCLUDED
#define PRIORITYQUEUE_H_INCLUDED

/*
* Copyright (c) 2010,东南大学数据库课题组
* All rights reserved.
*
* 文件名称：
* 摘    要：简要描述本文件的内容
*
* 当前版本：1.0
* 作    者：倪力
* 完成日期：2010年
*/
// define and implement priority queues
namespace PriorityQueue{
template<class T>
class priqueue {
private:
    int	n, maxsize;
    T	*x;
    void swap(int i, int j) {
        T t = x[i];
        x[i] = x[j];
        x[j] = t;
    }
public:
    priqueue(int m) {
        maxsize = m;
        x = new T[maxsize+1];//多申请一个，从1开始
        n = 0;
    }
    void insert(T t) {
        int i, p;
        x[++n] = t;
        for (i = n; i > 1 && x[p=i/2] > x[i]; i = p)
            swap(p, i);
    }
    T extractmin() {
        int i, c;
        T t = x[1];
        x[1] = x[n--];
        for (i = 1; (c=2*i) <= n; i = c) {
            if (c+1<=n && x[c+1]<x[c])
                c++;
            if (x[i] <= x[c])
                break;
            swap(c, i);
        }
        return t;
    }
};

// sort with priority queues (heap sort is strictly better)

template<class T>
void pqsort(T v[], int n) {
    priqueue<T> pq(n);
    int i;
    for (i = 0; i < n; i++)
        pq.insert(v[i]);
    for (i = 0; i < n; i++)
        v[i] = pq.extractmin();
}

void test();
}
#endif // PRIORITYQUEUE_H_INCLUDED
