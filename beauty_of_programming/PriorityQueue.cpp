/*
* Copyright (c) 2010,东南大学数据库课题组
* All rights reserved.
*
* 文件名称：
* 摘    要：简要描述本文件的内容
*
* 当前版本：1.0
* 作    者：倪力
* 完成日期：2010年
*/
#include "PriorityQueue.h"
#include <iostream>
namespace PriorityQueue {
using namespace std;
void test() {

    const int	n = 10;
    int	i, v[n];
    if (1) { // Generate and sort
        for (i = 0; i < n; i++)
            v[i] = n-i;
        pqsort(v, n);
        for (i = 0; i < n; i++)
            cout << v[i] << "\n";
    } else { // Insert integers; extract with 0
        priqueue<int> pq(100);
        while (cin >> i)
            if (i == 0)
                cout << pq.extractmin() << "\n";
            else
                pq.insert(i);
    }
}

}
