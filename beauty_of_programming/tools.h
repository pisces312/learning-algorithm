#ifndef TOOLS_H_INCLUDED
#define TOOLS_H_INCLUDED

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <memory.h>
#include <limits.h>
#include <iostream>
#include<cmath>
#include <algorithm>

#define PASS() printf("%s------pass!\n",__FUNCTION__)
#define WATCH_TIME_BEGIN {int start = clock();
#define WATCH_TIME_END(S) start = clock() - start;\
    printf("%s\tclicks=%d\ttime=%gns\n",(S),start,1e9*start/((float) CLOCKS_PER_SEC));}

#define PRINT_INT(i) printf("%d\n",(i))
#define PRINT_STRING(s) printf("%s\n",(s))
typedef unsigned char byte;
typedef unsigned char uint4;
typedef unsigned char uint8;
/* swap x[i..i+k-1] with x[j..j+k-1] */
void swap(int* x,int i, int j, int k);

inline bool isEven(int x) {
    return (x&0x1)?false:true;
}

inline int compareInt(const void* x,const void* y) {
    return *(int*)x-*(int*)y;
}
int FindMaxInteger(int* x,int n);
//inline int randint(int l, int u) {
//    return l + (RAND_MAX*rand() + rand()) % (u-l+1);
//}

void reverse(int* arr,int b,int e);
//获得一个数字中的每位数的总和
int GetSumOfEveryDigit(int x);
#endif // TOOLS_H_INCLUDED
