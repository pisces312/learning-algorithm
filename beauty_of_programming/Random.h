#ifndef RANDOM_H_INCLUDED
#define RANDOM_H_INCLUDED

/*
* Copyright (c) 2010,东南大学数据库课题组
* All rights reserved.
*
* 文件名称：
* 摘    要：简要描述本文件的内容
*
* 当前版本：1.0
* 作    者：倪力
* 完成日期：2010年
*/
#include <stdlib.h>
#include <time.h>
#include <list>
namespace Random {
inline int bigrand() {
    return RAND_MAX*rand() + rand();
}

inline int randint(int l, int u) {
    return l + bigrand() % (u-l+1);
}
//[0,n-1]之间的k个不同的随机数
void GenRandomShuffle(int* x,int m, int n);
void GenRandomKnuthOrdered(int* x,int m, int n);
void GenRandomKnuthAscOrdered(int* x,int m, int n);
void GenRandomSets(int* x,int m, int n);
void GenRandomSetsFloyd(int* x,int m, int n);

//从总个数未知的容器中随机选择一个
typedef std::list<int> IntList;
//返回行号
int RandSelectFromUnkownLen(const IntList& c,int& value);

/////////////////////////////////////////
//tools
void createRandomPositiveData(int* keys,int n,time_t seed=0,bool isSetSeed=false);
int createRandomData(int* keys,int n,time_t seed=0,bool isSetSeed=false) ;

void test();
}
#endif // RANDOM_H_INCLUDED
