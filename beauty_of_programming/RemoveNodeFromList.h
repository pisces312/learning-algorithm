#ifndef REMOVENODEFROMLIST_H_INCLUDED
#define REMOVENODEFROMLIST_H_INCLUDED
//#include <limits.h>
#include <assert.h>
//#include <algorithm>
//#include <list>
/**
无头单链表的删除，所为无头，即没有指针指向要删除的节点的前一个节点！！
**/
namespace RemoveNodeFromList {
    struct Node {
        int data;
        Node* next;
    };
    //删除节点ｎｏｄｅ
    /**
    !直接交换数据！
    **/
    void removeNode(Node* node) {
        assert(node!=NULL);
        Node* pNext=node->next;
        if (pNext!=NULL) {
            node->next=pNext->next;
            node->data=pNext->data;
            delete pNext;
        }

    }
    void printNodes(Node *p){
//        Node *p=node;
        while(p!=NULL){
            cout<<p->data<<" ";
            p=p->next;
        }
        cout<<endl;
    }
    void testRemoveNodeFromList() {
        //产生一个链表
        int size=4;
        Node *nodes=new Node[size];
        nodes[0].data=3;//=new Node;//{3,nodes[1]};
        nodes[1].data=6;
        nodes[2].data=2;
        nodes[3].data=1;
        for(int i=0;i<size-1;i++){
            nodes[i].next=&nodes[i+1];
        }
        nodes[size-1].next=NULL;
        //
        //
        Node *p=&nodes[0];
        printNodes(p);
        cout<<endl;
        //
//        p=&nodes[0];
        removeNode(nodes+1);
        printNodes(p);

//        head->data=3;

//    RemoveNodeFromList::removeNode
    }
};



#endif // REMOVENODEFROMLIST_H_INCLUDED
