/*
* Copyright (c) 2010,东南大学数据库课题组
* All rights reserved.
*
* 文件名称：
* 摘    要：简要描述本文件的内容
*
* 当前版本：1.0
* 作    者：倪力
* 完成日期：2010年
*/
#include "Random.h"
#include "tools.h"
#include <set>
namespace Random {
using namespace std;
/////////////////////////////////////////////////////////
//输出0~n-1范围内的m个随机整数有序列表，不允许重复,m<n
void GenRandomKnuthOrdered(int* x,int m, int n) {
    for (int i = 0,t=m; i < n; i++)
        /* select m of remaining n-i */
        if ((bigrand() % (n-i)) < m) {
            x[t-m]=i;
            m--;
        }
}
//void GenRandomKnuthDecOrdered(int* x,int m, int n) {
//    if(m>0) {
//        if((bigrand()%n)<m) {
//
//            GenRandomKnuthDecOrdered(x,m-1,n-1);
////            PRINT_INT(n-1);
//            x[m-1]=n-1;
//        } else {
//            GenRandomKnuthDecOrdered(x,m,n-1);
//        }
//
//
//    }
//}
void GenRandomKnuthAscOrdered(int* x,int m, int n) {
    if(m>0) {
        if((bigrand()%n)<m) {
            x[m-1]=n-1;
            GenRandomKnuthAscOrdered(x,m-1,n-1);
        } else {
            GenRandomKnuthAscOrdered(x,m,n-1);
        }
    }
}
//TODO产生数组[0,n-1]的所有m元子集
void GenSubset(int n,int m,int a) {
//    if(m>0) {
//        for(int i=0; i<n; ++i) {
////            int t=i;
//            PRINT_INT(i);
//            GenSubset(m-1,n-1);
//            printf("\n");
//        }
//        if((bigrand()%n)<m) {
//            x[m-1]=n-1;
//            GenRandomKnuthAscOrdered(x,m-1,n-1);
//        } else {
//            GenRandomKnuthAscOrdered(x,m,n-1);
//        }
//    }
}

//输出是随机的
void GenRandomSets(int* x,int m, int n) {
    set<int> S;

    while (S.size() < m) {
        S.insert(bigrand() % n);
    }
    int j=0;
    for (set<int>::iterator i = S.begin(); i != S.end(); ++i) {
        x[j++]=*i;
    }
//        cout << *i << "\n";
}
//[0,n-1]之间的k个不同的随机数
void GenRandomShuffle(int* x,int m, int n) {
    int i;
    for (i = 0; i < n; i++)
        x[i] = i;
    for (i = 0; i < m; i++) {
        swap(x,i,randint(i,n-1));
    }
//    sort(x, x+m);//可以对前m个排序
}

void GenRandomSetsFloyd(int* x,int m, int n) {
    set<int> S;
    int j,t;
    for (j = n-m; j < n; j++) {
        t = bigrand() % (j+1);//[0,j]
        if (S.find(t) == S.end())
            S.insert(t); // t not in S
        else
            S.insert(j); // t in S,上一次 t的范围[0,j-1]，不可能包含j
    }
    j=0;
    for (set<int>::iterator i= S.begin(); i != S.end(); ++i) {
        x[j++]=*i;
    }
//        cout << *i << "\n";
}


////////////////////////////////////////////////////////////
int createRandomData(int* keys,int n,time_t seed,bool isSetSeed) {
    if(!isSetSeed)
        time(&seed);
    srand(seed);
    for(int i=0; i<n; ++i) {
        keys[i]=rand()%n;
        if((rand()&0x1)!=0) {
            keys[i]=-keys[i];
        }
    }
    return seed;
}
void createRandomPositiveData(int* keys,int n,time_t seed,bool isSetSeed) {
    if(!isSetSeed)
        time(&seed);
    srand(seed);
    for(int i=0; i<n; ++i) {
        keys[i]=rand()%n;
    }
}
int RandSelectFromUnkownLen(const IntList& c,int& value) {
    IntList::const_iterator itr=c.begin();
    if(itr==c.end()) {
        return -1;
    }
    //只有一行的情况，就选这一行
    value=*itr;
    int i=0;
    ++itr;
    while(itr!=c.end()) {
        ++i;
        if((rand()%(i+1))==i) {//1/n的概率
            value=*itr;
            break;
        }
        ++itr;
    }
    if(itr==c.end()) {//如果输入结束没有找到，则选第一个
        return 0;
    }
    return i;

}
void test() {
    int n=10;
    int* x=new int[n];
    int k=5;
    time_t seed;
    time(&seed);
    srand(seed);

//    GenRandomKnuthDecOrdered(x,k,n);
//    printArray(x,k);

//    GenSubset(6,2);

    GenRandomKnuthAscOrdered(x,k,n);
    printArray(x,k);

    GenRandomShuffle(x,k,n);
    printArray(x,k);

    GenRandomKnuthOrdered(x,k,n);
    printArray(x,k);

    GenRandomSets(x,k,n);
    printArray(x,k);

    GenRandomSetsFloyd(x,k,n);
    printArray(x,k);


    IntList c;
    c.push_back(2);
    c.push_back(1);
    c.push_back(5);
    c.push_back(3);
    c.push_back(0);
    c.push_back(9);
    c.push_back(7);
    int value;
    int i=RandSelectFromUnkownLen(c,value);
    if(i>=0) {
        printf("%d %d\n",i,value);
    }
    delete[] x;
}
}
