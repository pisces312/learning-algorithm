#ifndef STRINGALG_H_INCLUDED
#define STRINGALG_H_INCLUDED

/*
* Copyright (c) 2010,东南大学数据库课题组
* All rights reserved.
*
* 文件名称：
* 文件标识：见配置管理计划书
* 摘    要：简要描述本文件的内容
*
* 当前版本：1.0
* 作    者：倪力
* 完成日期：
*/
#include <stdlib.h>
namespace String {
///////////////////////////////////////////////////////
//basic string operations
// trim:  remove trailing blanks, tabs, newlines
int trim(char s[]);
void reverse(char s[]);
void itoa(int num, char s[]);
int atoi(char s[]) ;
char * __cdecl strcpy(char * dst, const char * src);
char * __cdecl strcat (char * dst, const char * src);
int strcmp1(const char *a,const char *b);
int strcmp2(const char *a,const char *b);
int strcmp3(const char *dest, const char *source);
int strlen(const char *s);

/**
KMP
**/
int kmp(const char* pat,const int patLen,const char* s,const int sLen);


/**
    判断t是否在s的末尾
    **/
int strend(char *s, char *t);
//#define MAX_BUF 16
/**
赋值ct中的n个字符赋值到s所指向的字符数组中
**/
char *liw_strncpy(char *s, const char *ct, size_t n);
/**
连接ct中的n个字符
**/
char *liw_strncat(char *s, const char *ct, size_t n);
int liw_strncmp(const char *cs, const char *ct, size_t n);
void itoa2(int n, char s[], int b);
void squeeze(char s[], char c);
void strcat(char s[], char t[]);
int pstrcmp(char **p, char **q);
//返回两个字符串中共同部分的长度，从第一个字符开始比较
int commonlen(char *p,char*q);

///////////////////////////////////////////////////
//do not use extra space
const char* RemoveNumFrom(char* str);
const char* RemoveNumFrom2(char* str);
const char* RemoveNumFrom3(char* str);
//use extra space
const char* RemoveNumFrom(const char* str,char* res);

const char* RemoveDupCh(char *str);
///////////////////////////////////////////////////
//求两个字符串的最大子公共字符串
void MaxCommonSubString(const char *a,const char* b,char* r);
void MaxCommonSubStringSuffixArray(const char *a,const char* b,char* r);

/////////////////////////////////////////////////////
//在一个语句中找出最长的重复子字符串
void FindMaxDupSubString(const char* a,char *r);
void FindMaxDupSubStringSuffixArray(const char* a,char *r);
/////////////////////////////////////////////////////
void test();
void testRemoveNum();
void test_ncpy(const char *str) ;
void test_ncat(const char *first, const char *second);

void test_ncmp(const char *first, const char *second);
void testNStringOpr();
void testStrcat() ;
void testStrend();
void testRemoveDupCh();
void testKMP();
}
#endif // STRINGALG_H_INCLUDED
