#ifndef BUYDRINKS_H_INCLUDED
#define BUYDRINKS_H_INCLUDED

/**
饮料供货
**/
class Drink {
    public:
    string name;
    int volume;
    //最大数量
    int maxNumber;
    double H;
    int buyNumber;
    Drink(        string n,
                  int v,
                  int c,
                  double h,
                  int b=0) {
        name=n;
        volume=v;
        maxNumber=c;
        H=h;
        buyNumber=b;
    }


};
Drink drinks[]={
    Drink("可乐",32,100,0.8),
    Drink("王老吉",4,100,0.5),
    Drink("果粒橙",16,100,0.6)
};
#define MAX_V 128
#define DRINK_TYPE_NUM 3
//多一个，包含了容量0
int opt[MAX_V+1][DRINK_TYPE_NUM+1];
int cal(int v,int t) {
    opt[0][t]=0;
    for (int i=1;i<=v;i++) {
        opt[i][t]=-INFINITY;
    }
    //
    for (int j = t - 1; j >= 0; j--) {
        for (int i = 0; i <= v; i++) {
            opt[i][j] = -INFINITY;
            for (int k = 0; k <= drinks[j].maxNumber; k++) {        // 遍历第j种饮料选取数量k
//                cout<<k<<endl;
                if (i <k * drinks[j].volume) {
                    break;
                }
                int x = opt[i - k * drinks[j].volume][j + 1];
                //!
                if (x>(-INFINITY)) {
//                    cout<<"abc\n";
//                if (x != -INFINITY) {
                    x += drinks[j].H * k;
                    if (x > opt[i][j]) {
                        opt[i][j] = x;
                        cout<<i<<" "<<j<<" "<<k<<" "<<drinks[j].name<<endl;
//                        cout<<"abc\n";
                    }
                }
            }
        }
    }
    return opt[v][0];
}
void testBuyDrinks() {
//cout<<(-1111>-INFINITY)<<endl;
//cout<<-INFINITY<<endl;
    cal(MAX_V,DRINK_TYPE_NUM);
    for (int j=0;j<=4;j++) {
        if (opt[MAX_V][j]>=0)
            cout<<opt[MAX_V][j]<<" ";
    }
    cout<<endl;
//}
//    for (int i=0;i<=MAX_V;i++) {
//        for (int j=0;j<=4;j++) {
//            if (opt[i][j]>=0)
//                cout<<opt[i][j]<<" ";
//            else {
//                cout<<"      ";
//            }
//        }
//        cout<<endl;
//    }


}


#endif // BUYDRINKS_H_INCLUDED
