/*
* Copyright (c) 2010,东南大学数据库课题组
* All rights reserved.
*
* 文件名称：
* 摘    要：简要描述本文件的内容
*
* 当前版本：1.0
* 作    者：倪力
* 完成日期：2010年
*/
#include "tools.h"
#include "Random.h"
#include "Sort.h"
#include <vector>
namespace ProgrammingPears {
//使用快速排序选择第k个最小的元素
int selectKMin(int *x,int l, int u, int k) {
    if (l >= u)
        return -1;
    swap(x,l, Random::randint(l, u));
    int t = x[l];
    int i = l;
    int j = u+1;
    int temp;
    for (;;) {
        do i++;
        while (i <= u && x[i] < t);
        do j--;
        while (x[j] > t);
        if (i > j)
            break;
        temp = x[i];
        x[i] = x[j];
        x[j] = temp;
    }
    swap(x,l, j);
    //递归是函数的最后一个操作，可以转换成while循环，见selectKMin2
    if (j < k)
        selectKMin(x,j+1, u, k);
    else if (j > k)
        selectKMin(x,l, j-1, k);
    return k;//直到j==k结束
}

//使用快速排序选择第k个最小的元素,返回的是下标
//非递归情况
int selectKMin2(int *x,int l, int u, int k) {

    int t;
    int i;
    int j;
    int temp;
    while(l < u) {

        swap(x,l, Random::randint(l, u));
        i = l;
        j = u+1;
        t = x[l];

        for (;;) {
            do i++;
            while (i <= u && x[i] < t);
            do j--;
            while (x[j] > t);
            if (i > j)
                break;
            temp = x[i];
            x[i] = x[j];
            x[j] = temp;
        }
        swap(x,l, j);
        if (j < k) {
            l=j+1;
        } else if (j > k) {
            u=j-1;
        } else { //直到j==k结束
            return k;
        }
    }
    if(l==u&&k==0) {
        return 0;
    }
    return -1;

}
//默认以最左侧的为pivot
int Patition(int* x,const int l,const int u) {
    //printf("pivot=%d\n",t);
    swap(x,l, Random::randint(l, u));
    int i=l+1,j=u;//跳过第一个元素
    //字母l下标为pivot
    while(true) {
        while (i <= j && x[i] < x[l]) {
            ++i;
        }
        while (x[j] > x[l]) { //这里不需要判断i<=j//l相当于一个哨卫
            --j;
        }
        if (i > j)
            break;
        swap(x,i,j);
//        printArray(x,u-l+1,0);
    }
    swap(x,l,j);//!!!交换的是pivot！！
//    printArray(x,u-l+1,0);
//    printf("%d %d\n",t,x[j]);
    return j;
}
//这里k从1开始,返回第k小的下标
int selectKMin3(int *x,int l, int u, int k) {
    int j;
    while(l < u) {
        j=Patition(x,l,u);
        if (j < k-1) {
            l=j+1;
        } else if (j > k-1) {
            u=j-1;
        } else { //直到j==k结束
            return j;
        }
    }
//    if(l==u&&k==1) {//???
////    if(l==u){
//        return 0;
//    }
    return -1;
}
bool IsKArrayLeNum(int *x,int n,int t,int k) {
    Sort::quickSortFinal(x,n);
    int sum=0;
    for(int i=0,len=k-1; i<=len; ++i) {
        sum+=x[i];
    }
    return sum<=t;
}
bool IsKArrayLeNum2(int *x,int n,int t,int k) {
    int sum=0;
    for(int i=0,len=selectKMin2(x,0,n-1,k-1); i<=len; ++i) {
        sum+=x[i];
    }
    return sum<=t;
}
void HailStone(int x) {
    while(x!=1) {
        printf("%d ",x);
        if(isEven(x)) {
            x>>=1;
        } else {
            x=3*x+1;
        }

    }
    printf("\n");
}
bool ValidatePlus(int a,int b,int r) {
    return ((GetSumOfEveryDigit(a)+GetSumOfEveryDigit(b))%9)==(GetSumOfEveryDigit(r)%9);

}

int GetYearsOfDoubleUsing72Rule(double rate) {
    //return 72/(rate*100);
    return ceill(0.72/rate);
}
int FindMaxUsingDivide(int* x,int n) {
    if(n==1) {
        return x[0];
    }
    return std::max(x[n-1],FindMaxUsingDivide(x,n-1));
}
int FindMaxUsingSential(int* x,int n) {
    int i=0;
    int max=INT_MIN;
    while(i<n) {
        max=x[i];
        x[n]=max;
        ++i;
        while(x[i]<max) ++i;
    }
    return max;
}

double CalcPolynomialValue1(double* a,int n,double x) {
    double y=a[0];
    double t=1;
    for(int i=1; i<n; ++i) {
        t*=x;
        y+=(a[i]*t);
    }
    return y;
}

double CalcPolynomialValue2(double* a,int n,double x) {
    double y=a[n-1];
    for(int i=n-2; i>=0; --i) {
        y=y*x+a[i];
    }
    return y;
}
void RotateMatrix(int* x,int row,int col) {
//    struct Element{
//        int row,col,e;
//    };
    int n=row*col;
    int* rowIds=new int[n];
    int* colIds=new int[n];
//    for(int )


    delete[] rowIds;
    delete[] colIds;
}


byte CompressTwoNum2Byte(byte a,byte b) {
    return 10*a+b;
}
void UncompressByte2TwoNum(byte c,byte& a,byte& b) {
    b=c%10;
    a=c/10;
}

byte CompressTwoNum2Byte2(byte a,byte b) {
    return (a<<4)+b;
}
void UncompressByte2TwoNum2(byte c,byte& a,byte& b) {
    b=(c&0xf);
    a=(c>>4);
}
int* ShareMatrix(int* x,int* y,int len) {
    int n=len*len;
    int* c=new int[n];
    int* row,*col;
//    int* rend;
    int i,j;
    for(row=c,i=0; i<len; row+=len,++i) {
//        rend=row+len;
        for(col=row,j=0; j<i; ++col,++j) {
            *col=abs(x[i]-x[j]);
        }
        *col=0;
        ++col;
        for(j=i+1; j<len; ++col,++j) {
            *col=abs(y[i]-y[j]);
        }
    }
//    int i,j;
//    for(i=0; i<len; ++i) {
//        for(j=0;j<i;++j){
//            c[i][j]=abs(x[i]-x[j]);
//        }
//        c[i][j]=0;
//        for(j=i+1; j<len; ++j) {
//            c[i][j]=abs(y[i]-y[j]);
//        }
//    }

    return c;
}
int GetFirstMatrixDistance(int* matrix,int len,int i,int j) {
    if(i<j) {
        int t=i;
        i=j;
        j=t;
    }
    return *(matrix+len*i+j);

}
int GetSecondMatrixDistance(int* matrix,int len,int i,int j) {
    if(i>j) {
        int t=i;
        i=j;
        j=t;
    }
    return *(matrix+len*i+j);
}
//double GetHighPrecisonSum(double* d,int n){
//
//}

//////////////////////////////////////////////////////////////////////////
void testShareMatrix() {
    int x[]= {1,2,3,4,5};
    int y[]= {4,12,7,2,93};
    int n=5;
    int* c=ShareMatrix(x,y,n);

    printMatrix(c,n,n);

    PRINT_INT(GetFirstMatrixDistance(c,n,0,3));
    PRINT_INT(GetSecondMatrixDistance(c,n,2,3));
    PRINT_INT(GetSecondMatrixDistance(c,n,3,2));


    delete[] c;

}
void testCompress() {
    byte a=4;
    byte b=3;

    byte c;

    c=CompressTwoNum2Byte(a,b);
    PRINT_INT(c);
    UncompressByte2TwoNum(c,a,b);
    PRINT_INT(a);
    PRINT_INT(b);

    c=CompressTwoNum2Byte2(a,b);
    PRINT_INT(c);
    UncompressByte2TwoNum2(c,a,b);
    PRINT_INT(a);
    PRINT_INT(b);


}
void testCalcPolynomialValue() {
    int n=10;
    double a[]= {1,3,2,6,3,7,3,5,8,0};
    int x=2;
    printf("%f,%f\n",CalcPolynomialValue1(a,n,x),CalcPolynomialValue2(a,n,x));
//    assert(CalcPolynomialValue1(a,n,x)==CalcPolynomialValue2(a,n,x));
}
void test_selectKMin() {
    int n=8;
    int x[]= {3,5,2,8,1,4,7,9};
    int k=4;
    selectKMin2(x,0,n-1,k);
    printArray(x,n);
    assert(x[k]==5);

    k=1;
    selectKMin2(x,0,n-1,k);
    printArray(x,n);
    assert(x[k]==2);


    k=2;
    selectKMin2(x,0,n-1,k);
    assert(x[k]==3);
}
void test_selectKMin3() {


    //int x[]= {3,5,2,8,1,4,7,6};
    int x[]= {8,7,6,5,4,3,2,1};
    int n=sizeof(x)/sizeof(int);
    int* tmp=new int[n];
    memcpy(tmp,x,n*sizeof(int));



    int k=4;
    int i=selectKMin3(x,0,n-1,k);
    printArray(x,n);
    printf("%d\n",x[i]);
    assert(x[i]==4);
    memcpy(x,tmp,n*sizeof(int));

    k=1;
    i=selectKMin3(x,0,n-1,k);
    printArray(x,n);
    printf("%d\n",x[i]);
    assert(x[i]==1);
    memcpy(x,tmp,n*sizeof(int));


    k=2;
    i=selectKMin3(x,0,n-1,k);
    printf("%d\n",x[i]);
    assert(x[i]==2);
    memcpy(x,tmp,n*sizeof(int));
}
void test_IsKArrayLeNum2() {
//    test_selectKMin();
    int n=8;
    int k=4;
    int x1[]= {3,5,2,8,1,4,7,9};
    assert(IsKArrayLeNum(x1,n,10,k)==true);

    n=10;
    k=8;
    int x2[]= {9,8,7,6,5,4,3,2,1,0};
    assert(IsKArrayLeNum(x2,n,28,k)==true);

}
void testHailStone() {
    HailStone(10);
    HailStone(100);
}
void testFindMaxUsingSential() {
    int n=8;
    int x1[9]= {3,5,2,8,1,4,7,9};
    PRINT_INT(FindMaxUsingSential(x1,n));

}

void print_ivec(std::vector<int>::iterator begin, std::vector<int>::iterator end) {
    for(; begin != end; ++begin)
        std::cout << *begin << '\t';
    std::cout << std::endl;
}
void testCPPLibForHeap() {
    int n=8;
    int x1[8]= {3,5,2,8,1,4,7,9};
    printArray(x1,n);
    std::make_heap(x1,x1+n);//默认最大堆
    printArray(x1,n);
    //将最大值放在堆尾
    std::pop_heap(x1,x1+n);
    printArray(x1,n-1);//此时要减少一个长度

    x1[7]=10;
    std::push_heap(x1,x1+n);
    printArray(x1,n);

    std::sort_heap(x1,x1+n);
    printArray(x1,n);
    ///////////////////////////
    using namespace std;
    int a[] = {1, 12, 15, 20, 30};
    vector<int> ivec(a, a + sizeof(a) / sizeof(a[0]));
    print_ivec(ivec.begin(), ivec.end());
    make_heap(ivec.begin(), ivec.end(), greater<int>());
    print_ivec(ivec.begin(), ivec.end());
    pop_heap(ivec.begin(), ivec.end());
    ivec.pop_back();
    print_ivec(ivec.begin(), ivec.end());
    ivec.push_back(99);
    push_heap(ivec.begin(), ivec.end());
    print_ivec(ivec.begin(), ivec.end());
    sort_heap(ivec.begin(), ivec.end());
    print_ivec(ivec.begin(), ivec.end());
}
void test() {
    testFindMaxUsingSential();
//     int n=8;
//    int k=4;
//    int x1[]= {3,5,2,8,1,4,7,9};
//    PRINT_INT(FindMaxUsingDivide(x1,n));
//    assert(ValidatePlus(1234,4321,5555));
//    PRINT_INT(GetYearsOfDoubleUsing72Rule(0.08));
//    testHailStone();
//    test_IsKArrayLeNum2();
}
}
