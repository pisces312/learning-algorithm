#ifndef CPU_USAGE_H_INCLUDED
#define CPU_USAGE_H_INCLUDED
#include <windows.h>
#include<cmath>
#include<time.h>
/**
cpu可能会自动调节频率
**/
// Pauses for a specified number of milliseconds.
void sleep( clock_t wait ) {
    clock_t goal;
    goal = wait + clock();
    while ( goal > clock() )
        ;
}
//
//
double sinFun(double r) {
    return sin(r*3.1415926);
}
double cosFun(double r) {
    return cos(r*3.1415926);
}
//!??调节比率
//双核cpu，不成线性比例
/**
假设这段代码要运行的CPU 是P4 2.4Ghz （2.4 * 10 的9 次方个时钟周期每秒）。现代
CPU 每个时钟周期可以执行两条以上的代码，那么我们就取平均值两条，于是让（2 400 000
000 * 2 ）/5=960 000 000  （循环/秒），也就是说CPU 1 秒钟可以运行这个空循环960 000 000
次。
???? /5
**/

void cpuUsageRateMethod1(double rate) {
    //cpu 2*2.1
    //现代cpu每个时钟周期执行两条以上的指令，取平均值2
    //2.1*10e9*numberPerClock
    //!Intel Core 2 Duo CPU T8100
    size_t max=2000000000*rate;
    //休眠的ms
    size_t sleepTime=10;
    for (;;) {
        for (size_t i=0;i<max;i++);
        Sleep(sleepTime);
    }
}
//
void cpuUsageRateMethod2(double rate) {
    //busyTime和idleTime占的比例不同决定CPU的使用率
    size_t busyTime=10;
    size_t idleTime=busyTime/rate-busyTime;
    size_t startTime=0;
    while (true) {
        startTime=GetTickCount();
        //
        while ((GetTickCount()-startTime)<=busyTime);
        //
        Sleep(idleTime);
    }
}

void cpuSineRate() {
    //步进
    const double SPLIT = 0.04;
    const int COUNT = 100;
    const double PI = 3.14159265;
    const int INTERVAL = 300;
    unsigned int busySpan[COUNT];  //array of busy times
    unsigned int idleSpan[COUNT];  //array of idle times
    int half = INTERVAL / 2;
    double radian = 0.0;
//初始化一个缓存
    for (int i = 0; i < COUNT; i++)    {
        busySpan[i] = (DWORD)(half + (sin(PI * radian) * half));
        idleSpan[i] = INTERVAL - busySpan[i];
        radian += SPLIT;
    }
    unsigned int startTime = 0;
    int j = 0;
    while (true)    {
        j = j % COUNT;
        startTime = GetTickCount();
        while ((GetTickCount() - startTime) <= busySpan[j]) ;
        Sleep(idleSpan[j]);
        j++;
    }

}
void cpuSineRateEx(double (*fun)(double),double max,double initValue=0.0,double step=0.01) {
    //步进
    const double SPLIT = step;
    const int COUNT = 200;
    double maxValue=max;

//    const double PI = 3.14159265;
    const int INTERVAL = 300;
    unsigned int busySpan[COUNT];  //array of busy times
    unsigned int idleSpan[COUNT];  //array of idle times
    unsigned int half = INTERVAL / 2;

    double radian = initValue;
//初始化一个缓存
    for (int i = 0; i < COUNT; i++,radian += SPLIT)    {
        busySpan[i] = half+fun(radian)/maxValue*half;
        idleSpan[i] = INTERVAL - busySpan[i];
    }

    unsigned int startTime = 0;
    int j = 0;
    while (true)   {
        j = j % COUNT;
        //!法一标准c
        startTime = clock();//GetTickCount();
        while ((/*GetTickCount()*/clock() - startTime) <= busySpan[j]) ;
        //!法二windows函数
//        startTime = GetTickCount();
//        while ((GetTickCount()- startTime) <= busySpan[j]) ;
        Sleep(idleSpan[j]);
        j++;
    }

}
//!使用一个CPU
//默认使用cpu0
void cpuSineRateInMultipleCPU(int mask=0x1) {
    unsigned long * p=NULL;

    HANDLE hThread=CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)cpuSineRate,NULL,CREATE_SUSPENDED ,p);
    //!mask的每个位都对应一个cpu，所以最低位代表cpu0，次低位代表cpu1
    SetThreadAffinityMask(hThread,mask);
    ResumeThread(hThread);
//    HANDLE hThread=CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)cpuSineRate,NULL,0,p);
//!防止直接跳出主线程，即相当于设置了运行时间
//    WaitForSingleObject(hThread,100000);
}
void testQueryPerformance() {
    LARGE_INTEGER c;//=(LARGE_INTEGER *)malloc(sizeof((LARGE_INTEGER));
//    long* c=(long*)malloc(sizeof(long));
//!获得系统时间
    QueryPerformanceCounter(&c);
    printf("%d\n",c);
    //!获得系统频率
    QueryPerformanceFrequency(&c);
    printf("%d\n",c);
    //
//    getprocess
}
void multiCPUSineRate(){
        //!同时让两个CPU的使用率呈现正弦曲线
    cpuSineRateInMultipleCPU(0x1);
    cpuSineRateInMultipleCPU(0x2);
    Sleep(100000);
}
#endif // CPU使用率_H_INCLUDED
