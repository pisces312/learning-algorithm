#ifndef FINDFAULT_H_INCLUDED
#define FINDFAULT_H_INCLUDED
#include <map>
//#include<iostream>
using namespace std;
/**
假设一台机器只存一个标号为ID的记录，每份数据保存两个备份
1 在某个时间，如果得到一个数文件ID的列表，是否能够快速地找出这个表中进出现一次的ID
2 如果已经知道只有一台机器死机（也就是说只有一个备份丢失）呢？（同1）
!!如果有两台机器死机呢（假设同一个数据备份不会同时丢失，即丢失的两个ID不相同）
**/
namespace findfault {
    //1
    void findFaultSolOne(int* array,int size) {
//        using namespace std;
//        int *count=new int[size];
//        for(
        map<int, int> idMap;
//
        for (int i=0;i<size;i++) {

            idMap[array[i]]++;
        }
        //
        //
        map<int, int>::iterator it;
        for (it = idMap.begin(); it != idMap.end(); ++it) {
            if (it->second<2) {
                cout<<"ID "<<it->second<<" 故障\n";
            }
//            cout << it->first << " : " << it->second << endl;
        };

    }
    /**
    1
    ID出现次数等于2的机器肯定不是故障的机器，不予考虑
    **/

    void findFaultSol2(int* array,int size) {
//        int *count=new int[size];
//        for(
        map<int, int> idMap;
//
        for (int i=0;i<size;i++) {

            idMap[array[i]]++;
            //!达到两个相同的ID时就删除该ID，最后那个只有一个备份的ID就是有故障的
            if (idMap[array[i]]>=2) {
                idMap.erase(array[i]);
            }
        }

        //
        //
        map<int, int>::iterator it;
        for (it = idMap.begin(); it != idMap.end(); ++it) {
//            if(it->second<2){
//                cout<<"ID "<<it->second<<" 故障\n";
//            }
            cout << it->first << " : " << it->second << endl;
        }
    }
    /**
    1
    采用异或的方式
    **/
    void findFaultXorSol(int* array,int size) {
        int sum=0;
        for (int i=0;i<size;i++) {
            sum^=array[i];
        }
        cout<<sum<<endl;
    }
    /**
    2
    找出两个不同ID的机器故障
    关于A、B不同时，分别计算两类ID能得出A和B的原因：
    根据这两类的分法，A和B将会分别存在于这两类中
    即每个类中只有一个ID只存在一个备份，又还原为了2的第一个问
    **/
    void findFaultXorSol2(int* array,int size) {
        //!显然所有ID的异或结果为A^B，A、B为两个出错的ID，即两个故障的机器。
        //1 A=B，A^B=0无法计算
        //2 A!=B，即A^B!=0，即A、B之间至少有一个位置上为1而另一个对应位置上为0，这样才能保证异或的结果不为0！！！
        //把ID分为两类，
        //1 在这位上为1
        //2 在这位上为0
        int sum=0;
        for (int i=0;i<size;i++) {
            sum^=array[i];
        }
        //
        //
        int mask=0x1;
        while ((sum&mask)==0) {
            mask<<=1;
        }
        int sum1=0,sum2=0;

        for (int i=0;i<size;i++) {
            //!&必须加括号！！！优先级
            if ((mask&array[i])!=0) {
                sum1^=array[i];
            } else {
                sum2^=array[i];
            }

        }
        cout<<sum<<endl;
        cout<<mask<<endl;
        cout<<sum1<<endl;
        cout<<sum2<<endl;

    }

    /**
    解法四
    sum表示预计算的ID的和，即正常情况下（未出故障前）的ID算术和
    针对2的第一问，解决一个ID出错的情况
    **/
    void findFaultInvariantSol1(int *array,int size,int sum) {
        int t=0;
        for (int i=0;i<size;i++) {
            t+=array[i];
        }
        cout<<(sum-t)<<endl;

    }
    /**
    对于2的第二问，两个ID错误，这里考虑所有的两种情况
    !构造第二个方程
    使用乘法
    **/
    void findFaultInvariantSol2(int *array,int size,long sum,long product) {
//    int t=0;

        for (int i=0;i<size;i++) {
            sum-=array[i];
            product/=array[i];
        }
        //x+y=a
        //xy=b
        //(-b+sqrt((b*b)-4*a*c))/2*b
        //(a+sqrt(a*a-4*b))/2
//    long a=sum-t;
//    cout<<a<<endl;
        int x=(sum-(int)sqrt(sum*sum-4*product))/2;
        if (x<0) {
            x=(sum+(int)sqrt(sum*sum-4*product))/2;
        }
        //!+-均可
//    int x=(sum+(int)sqrt(sum*sum-4*product))/2;
        int y=sum-x;
//    cout<<(sum-t)<<endl;
        cout<<x<<endl;
        cout<<y<<endl;


    }
    /**
    对于2的第二问，两个ID错误，这里考虑所有的两种情况
    !构造第二个方程
    构造平法和
    **/
    void findFaultInvariantSol3(int *array,int size,long sum,long sumOfSquares) {
        for (int i=0;i<size;i++) {
            sum-=array[i];
            sumOfSquares-=(array[i]*array[i]);
        }
        //x+y=a
        //xy=b
        //(-b+sqrt((b*b)-4*a*c))/2*b
        //(a+sqrt(a*a-4*b))/2
//    long a=sum-t;
//    cout<<a<<endl;
        cout<<sumOfSquares<<endl;
        int x=(sum-(int)sqrt(2*sumOfSquares-sum*sum))/2;
        if (x<0) {
            x=(sum+(int)sqrt(2*sumOfSquares-sum*sum))/2;
        }
        //!+-均可
//    int x=(sum+(int)sqrt(sum*sum-4*product))/2;
        int y=sum-x;
//    cout<<(sum-t)<<endl;
        cout<<x<<endl;
        cout<<y<<endl;


    }
    void testFindFault() {
        int array3[]={1,2,3,4,5,1,2,3,4};
        int size3=9;
        //求和的使用
//    list<int> ilist(array3, array3 + size3);
//    int ilist_result = accumulate(ilist.begin(), ilist.end(), 0, plus<int>());
//    cout<<ilist_result<<endl;
//
        findFaultInvariantSol1(array3,size3,30);
        //
        //
        int array4[]={1,2,3,4,5,1,2,4};
        int size4=8;
        findFaultInvariantSol2(array4,size4,30,120*120);
        findFaultInvariantSol3(array4,size4,30,110);



    }















//
//
//


};
void testFindFault() {
    //问题1和问题2第一问的数据
    int array[]={1,2,3,4,5,2,3,1,5};
//    int array[]={1,2,3,4,5,2,3,4,5};
    int size=9;
    //
    int array2[]={1,2,3,4,5,3,1,5};
    int size2=8;
//    findfault::findFaultSol2(array,size);
//    findfault::findFaultSolOne(array,size);
//    findfault::findFaultXorSol(array,size);
    findfault::findFaultXorSol2(array2,size2);
    //
    //
    int array3[]={1,2,3,4,5,1,2,3,4};
    int size3=9;
    //求和的使用
//    list<int> ilist(array3, array3 + size3);
//    int ilist_result = accumulate(ilist.begin(), ilist.end(), 0, plus<int>());
//    cout<<ilist_result<<endl;
//
    findfault::findFaultInvariantSol1(array3,size3,30);
    //
    //
    int array4[]={1,2,3,4,5,1,2,4};
    int size4=8;
    findfault::findFaultInvariantSol2(array4,size4,30,120*120);
    findfault::findFaultInvariantSol3(array4,size4,30,110);



}

#endif // FINDFAULT_H_INCLUDED
