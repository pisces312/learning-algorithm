#ifndef FRACTION_H_INCLUDED
#define FRACTION_H_INCLUDED
#include <cmath>
using namespace std;
/**
???????????????pow(10,n);小数部分如何提取
//
思想：
使用float或double来存储小数是不能得到精确值的，解决方法就是用分数形式来表示
所有小数都可以化为整数部分加纯小数
这里只讨论纯小数的情况
一、有限小数X=0.a1a2a3...an
X=(a1a2...an)/10^n
二、无限小数X=0.a1a2...an (b1b2...bn)
X=0.a1a2...an (b1b2...bn)
=>10^n * X=a1a2...an . (b1b2...bn)   //分为了整数部分和小数部分
=>10^n * X=a1a2...an + 0.(b1b2...bn)  //化为整数+纯小数形式
=>X=(a1a2...an + 0.(b1b2...bn))/10^n //将不循环部分和循环部分分开
下面是对循环部分Y=0.(b1b2...bn)的处理
10^m * Y=b1b2...bm . (b1b2...bm)    //写出两个循环节，并把第一部分化为整数
=>10^m * Y=b1b2...bm + 0.(b1b2...bm)  //化为两部分
=>10^m * Y-Y=b1b2...bm  //将Y带入，移项
=> Y=b1b2...bm/(10^m -1 )
X的最终公式为
X=((a1a2...am) * (10^m - 1) + (b1b2...bm)) / ((10^m-1)*10^n)
//
//
分数的化简
A/B=(A/Gcd(A,B))/(B/Gcd(A,B))
即消去最大公约数
**/
namespace fraction {
    inline bool isEven(int x) {
        return (x&0x1)?false:true;
    }
    /**
    四种情况讨论
    1. x,y even 即都能提出公因数2
    f(x,y)=2*f(x/2,y/2)=2*f(x>>1,y>>1)=f(x>>1,y>>1)<<1
    2. x even,y odd , 2一定不在公约数中，可以将x除2变小
    f(x,y)=2*f(x/2,y)=f(x>>1,y)
    3. x odd, y even, 同理
    f(x,y)=2*f(x,y/2)=f(x,y>>1)
    4. x,y odd,用一般方法，可以用减或取余
    f(x,y)=f(y,x-y)
    奇数之间相减，结果一定是偶数，即一奇一偶的情况
    优点：利用移位提高效率
    **/
    int gcdByPrime(int x,int y) {
        //!保证x>=y>0
        if (x<y)
            return gcdByPrime(y,x);
        if (y==0)
            return x;

        if (isEven(x)) {
            if (isEven(y))
                return (gcdByPrime(x>>1,y>>1)<<1);
            return gcdByPrime(x>>1,y);
        }
        if (isEven(y))
            return gcdByPrime(x,y>>1);
        return gcdByPrime(y,x-y);
    }
    //n为小数位数
    void d2Fraction(const double &d,int& x,int& y,const int& n) {
        int integral;

        if (d>=1) {
            integral=floor(d);
        } else {
            integral=0;
        }
        //小数部分
        double f=d-(double)integral;
        cout<<f<<endl;
//求小数位数??
//        int n=0;
//        while (f!=ceil(f)) {
//            n++;
//            f*=10;
//        }


        cout<<n<<endl;
        cout<<"整数部分"<<integral<<endl;
        int B=pow(10.0,n);
        //!不精确
        int A=f*B;
        int gcd=gcdByPrime(A,B);
        A/=gcd;
        B/=gcd;
        cout<<"小数部分的分数表示 A/B="<<A<<"/"<<B<<endl;
    }

};
void testFraction() {
    int x,y;
    fraction::d2Fraction(2.34,x,y,2);
}


#endif // FRACTION_H_INCLUDED
